<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PermissionTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_titles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('permissions', function (Blueprint $table) {
            if (!Schema::hasColumn('permissions', 'permission_titles_id')) {
                $table->unsignedBigInteger('permission_titles_id')->nullable()->after('guard_name');
                $table->foreign('permission_titles_id')
                    ->references('id')
                    ->on('permission_titles')
                    ->onDelete('set null')
                    ->onUpdate('no action');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropForeign(['permission_titles_id']);
            $table->dropColumn('permission_titles_id');
        });

        Schema::dropIfExists('permission_titles');
    }
}
