<?php

use App\Http\Controllers\DisciplinariesController;
use App\Http\Controllers\KpiEvalationFormController;
use App\Http\Controllers\NewsPageController;
use App\Models\OrganogramSetup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('cv_bank_online','OnBoardingController@cv_bank_online');

Route::middleware('Validateurl')->group(function () {
    if (env('COMPANY') == 'CLINIX' || env('COMPANY') == 'HGNHRM') {
        Route::middleware('UserBanned')->get('/', 'NewsPageController@news_dashboard');
        Route::get('dashboard', 'DashboardController@index');
    }else{
        Route::middleware('UserBanned')->get('/', 'DashboardController@index');
        Route::get('news_dashboard' ,[NewsPageController::class,'news_dashboard']);
    }
    Route::get('change-language/{locale}', 'LocalizationController@changeLanguage')->name('changeLanguage');
    Route::resource('employees', EmployeeController::class);
    Route::get('getEmployees', 'EmployeeController@getEmployees')->name('getEmployees');
    Route::post('getEmployees', 'EmployeeController@getEmployees')->name('getEmployees');
    Route::post('getEmployeesTable', 'EmployeeController@getEmployees')->name('getEmployeesTable');
    Route::post('suspend_employee', 'EmployeeController@suspend');
    Route::post('act_inact_employee', 'EmployeeController@act_inact_employee');
    Route::post('terminate_employee', 'EmployeeController@terminate');
    Route::post('restore_employee', 'EmployeeController@restore');
    Route::post('off_roll_employee', 'EmployeeController@off_roll');
    Route::post('on_roll_employee', 'EmployeeController@on_roll');
    // On Boarding
    Route::get('cv_bank_list','OnBoardingController@cv_bank_list')->name('applicant_list');
    Route::get('pdf_download/{id}','OnBoardingController@pdf_download');
    Route::post('call_for_appointment','OnBoardingController@call_for_appointment');
    Route::get('app_schedule', 'OnBoardingController@sch');
    Route::get('appointment', 'OnBoardingController@appointment');
    Route::match(['get', 'post'],'interview_result', 'OnBoardingController@results')->name('Admin.careersapplication.results');
    Route::get('careersapplication/{id}', 'OnBoardingController@show')->name('Admin.careersapplication.show');
    Route::get('careersapplication/download/{id}', 'OnBoardingController@download')->name('Admin.careersapplication.download');
    Route::post('careersapplication/remarks', 'OnBoardingController@remarks')->name('Admin.careersapplication.remarks');
    Route::post('careersapplication/quick', 'OnBoardingController@quick_add')->name('Admin.careersapplication.quick');
    Route::get('appointment/{id}','OnBoardingController@show');

    // Route::get('interview_result', 'OnBoardingController@interview_result');
    // Organogram
    Route::resource('organoGramSetup', OrganogramSetupController::class);
    Route::get('organogram', 'CompanyStructureController@organogram');

    Route::resource('cv_bank', OnBoardingController::class);
    // Employee Join Report
    Route::resource('emp_join_report', EmpJoinReportController::class);
    // Employee Appoint Letter Issuance
    Route::resource('appoint_letter', AppointLetterController::class);
    Route::get('print_letter','AppointLetterController@print_letter');
    Route::get('appoint_letter/{id}/create', 'AppointLetterController@create')->name('appoint_letter.create');

    // Employee Final Settlement Letter
    Route::get('settlement_letter','EmployeeController@finalSettlementLetter');
    // Employee Final Settlement History
    Route::get('settlement_history','EmployeeController@settlement_history');
    // Employee Final Settlement History Print
    Route::get('settlement_history_print/{id}', 'EmployeeController@settlement_history_print');
    //  Employee Final Settlement Record
    Route::post('final_record','EmployeeController@final_record');
    // Department Terminated Employees
    Route::get('get_dept_terminated_employees', 'EmployeeController@getDeptTerminatedEmployees');
    // Comapny Structures
    Route::resource('companyStructures', CompanyStructureController::class);
    // Company Setup
    Route::resource('companysetup', companySetupController::class);
    // Route::get('organogram', 'companySetupController@organogram');
    //City
    Route::resource('city',CityController::class);
    // Banks
    Route::resource('bank' , BanksController::class);
    // ENV Name
    Route::resource('env_name' , EnvNameController::class);
    // ENV Checks
    Route::resource('env_check' , EnvChecksController::class);
    // Final Settlement Setup
    Route::resource('final_settlement_setup' , FinalSettlementSetupController::class);
    // Maschine Management
    Route::resource('machine_management' , MachineManagementController::class);
    // Legal
    Route::resource('legals' , LegalController::class);
    // Leave Types
    Route::resource('leave_types' , LeaveTypesController::class);
    // Audit Log
    Route::resource('audit_logs' , AuditLogController::class);
    // Job Titles
    Route::resource('job_titles', JobTitlesController::class);
    // Training
    Route::resource('training', TrainingController::class);
    // Training Setup
    Route::resource('training_setup', TrainingSetupController::class);
    //Document Management
    Route::resource('document_management', DocumentManagementController::class);
    //Shift Type
    Route::resource('shiftTypes', ShiftTypeController::class);
    Route::get('get_employees_shift', 'ShiftTypeController@get_employees_shift')->name('get_employees_shift');

    //Shift Management
    Route::resource('shiftManagement', ShiftManagementController::class);
    // Route::post('/shiftstore', [ShiftManagementController::class , 'shiftstore']);
    Route::post('shiftstore', 'ShiftManagementController@shiftstore')->name('shiftstore');

    // Expense Administration
    Route::resource('expenseAdministration' , ExpenseAdministrationController::class);
    // Salary Setup
    Route::resource('salarySetup', SalarySetupController::class);
    Route::get('get_dept_employees', 'SalarySetupController@get_department_employees');
    Route::get('get_des_employees', 'SalarySetupController@get_designation_employees');
    Route::get('get_department_employees_from_section', 'SalarySetupController@get_department_employees_from_section');
    Route::get('get_emp_leave', 'SalarySetupController@get_employees_leave');
    Route::get('get_emp_monthly_comp', 'SalarySetupController@get_emp_monthly_comp');
    Route::get('get_department_child', 'SalarySetupController@get_department_child');
    Route::get('get_department_by_type', 'SalarySetupController@get_department_by_type');
    

    Route::get('clear_salary_comp/{id}', 'SalarySetupController@clear_salary_comp');
    // Incentive Employees
    Route::resource('IncentiveEmployee', IncentiveEmployeeController::class);
    // Department Change
    Route::resource('department_change' , DepartmentChangeController::class);
    // Employee Leave Request
    Route::resource('employee_leave_requests' , EmployeeLeaveRequestController::class);
    Route::get('get_leave_balance', 'EmployeeLeaveRequestController@getLeaveBalance');
    Route::get('leave-balance-logs', 'EmployeeLeaveRequestController@getLeaveBalanceLogs');
    Route::get('leave-type-balance-edit', 'EmployeeLeaveRequestController@editLeaveBalance');
    Route::post('leave-type-balance-update', 'EmployeeLeaveRequestController@updateLeaveBalance');
    Route::get('new-emp-leave-balance', 'EmployeeLeaveRequestController@newEmployeeLeaveBalance');
    // Short Leave Request
    Route::resource('short_leave_requests' , ShortLeaveRequestController::class);
    // OverTime Management
    Route::resource('overtime_managements' , OverTimeManagementController::class);
    // Bulk OverTime Management
    Route::resource('bulk_overtime_management' , BulkOverTimeManagementController::class);
    Route::get('check_bulk_overtime', 'BulkOverTimeManagementController@checkOvertime');
    // Shift Change
    Route::resource('shift_change' , ShiftChangeController::class);
    // KPI
    Route::resource('kpis', KpiController::class);
    // Loan Request
    Route::resource('loan_request' , LoanRequestController::class);
    Route::get('loan-payment-erp-detail/{referenceNo}', 'LoanRequestController@loanPaymentErpDetail');
    Route::resource('loan_installments' , LoanInstallmentController::class);
    // Miscellaneous Request
    Route::resource('miscellaneous' , MiscellaneousRequestController::class);
    // Bilk Advance Approval
    Route::resource('bulk_advance_approval' , BulkAdvanceApprovalController::class);
    Route::get('bulk_advance_approved' ,'BulkAdvanceApprovalController@approve_loan');
    // Travel Request
    Route::resource('travel_requests' , TravelRequestController::class);
    // Approvals
    Route::resource('approvals' , ApprovalController::class);
    //Add Salary Request
    Route::resource('addSalary' , AddSalaryController::class);
    // News Page Routes
    Route::get('news_page' ,[NewsPageController::class,'news_page']);
    Route::get('deletenews/{id}' ,[NewsPageController::class,'deletenews']);
    Route::post('updatenewsdeuration' ,[NewsPageController::class,'updatenewsdeuration']);
    Route::post('createheadline' ,[NewsPageController::class,'createheadline']);
    Route::get('headlines_items_delete/{id}' ,[NewsPageController::class,'deleteheadline']);
    Route::get('headlines_items_edit/{id}' ,[NewsPageController::class,'headlines_items_edit']);
    Route::post('news_create' ,[NewsPageController::class,'news_create']);
    Route::get('statusUpdate/{id}', 'ApprovalController@statusUpdate');
    Route::get('newslist_items_delete/{id}',[NewsPageController::class,'newslist_items_delete']);
    Route::get('newslist_items_edit/{id}',[NewsPageController::class,'newslist_items_edit']);
    Route::post('newslist_items_update' ,[NewsPageController::class,'newslist_items_update']);
    Route::get('ChangeNewsStatus/{id}' ,[NewsPageController::class,'ChangeNewsStatus']);
    Route::get('add_headlines',[NewsPageController::class,'add_headlines']);
    // End News Page Routes
    // Disciplinaries
     Route::get('disciplinaries', [DisciplinariesController::class,'index']);
     Route::post('issue_disciplinaries', [DisciplinariesController::class,'issue_disciplinaries']);
     Route::get('disciplinariestable', [DisciplinariesController::class,'disciplinariestable']);
     Route::get('disciplinaries_delete/{id}', [DisciplinariesController::class,'disciplinaries_delete']);
     Route::get('disciplinaries_edit/{id}', [DisciplinariesController::class,'disciplinaries_edit']);
     Route::post('update_disciplinaries', [DisciplinariesController::class,'update_disciplinaries']);
     Route::get('disciplinariesRules', [DisciplinariesController::class,'disciplinariesRules']);
     Route::get('disciplinaries_Rules_delete/{id}', [DisciplinariesController::class,'disciplinaries_Rules_delete']);
     Route::get('disciplinaries_Rules_edit/{id}', [DisciplinariesController::class,'disciplinaries_Rules_edit']);
     Route::get('disciplinaries_Rules_edit/{id}', [DisciplinariesController::class,'disciplinaries_Rules_edit']);
     Route::post('create_disciplinaries_rule', [DisciplinariesController::class,'create_disciplinaries_rule']);
     Route::post('update_disciplinaries_rule', [DisciplinariesController::class,'update_disciplinaries_rule']);
     Route::get('ChangeStatus/{id}',[DisciplinariesController::class,'ChangeStatus']);
     Route::post('add_reply',[DisciplinariesController::class,'add_reply']);
    // End Disciplinaries
    // Role
    Route::resource('roles' , RoleController::class);
    // Permission
    Route::resource('permissions' , PermissionController::class);
    Route::resource('permission-titles', PermissionTitleController::class);
    // User
    Route::resource('users' , UserController::class);
    Route::get('user/{id}', 'UserController@status');
     // Kpis
     Route::resource('kpis', KpiController::class);
     // End Kpis

     Route::get('kpi_evaluation_form',[KpiEvalationFormController::class,'index']);


    // Calculation Groups
    Route::resource('calculationGroups' , CalculationGroupController::class);
    // Company Belongnings
    Route::resource('company_belongings' , CompanyBelongingsController::class);
    Route::resource('issue_belonging' , IssueBelongingsController::class);
    Route::post('returning_belonging/{id}' , 'IssueBelongingsController@returning_belonging');
    // Route::post('return_belonging/{id}' , 'IssueBelongingsController@return_belonging');

    // Payment Codes
    Route::resource('paymentCodes' , PaymentCodesController::class);
    // Employee Payments
    Route::resource('employeePayments' , EmployeePaymentsController::class);
    // Medical Claims
    Route::resource('medicalClaims' , MedicalClaimsController::class);
    // Payroll Processing
    Route::post('payrollProcessing/{id}' , 'PayrollProcessingController@processPayroll');
    Route::get('downloadMultiple' , 'PayrollProcessingController@downloadMultiple');
    Route::post('check-payroll-accrual-jv' , 'PayrollProcessingController@checkPayrollAccrualJV');
    Route::post('payrollAccrualJV' , 'PayrollProcessingController@PayrollAccrualJV');
    Route::delete('delete-payroll-accrual-jv/{payroll}' , 'PayrollProcessingController@deletePayrollAccrualJV');
    Route::get('payroll-accrual-payment' , 'PayrollProcessingController@PayrollAccrualPayment');
    Route::post('payroll-accrual-payment' , 'PayrollProcessingController@storePayrollAccrualPayment');
    Route::get('view-accrual-voucher/{voucher_no}' , 'PayrollProcessingController@accrualPaymentVoucherView')->name('view-accrual-voucher');
    Route::get('accrual-payment-vouchers' , 'PayrollProcessingController@accrualPaymentVouchers');
    Route::post('accrual-payment-voucher' , 'PayrollProcessingController@sendAccrualPaymentVoucher');
    Route::resource('payrollProcessing' , PayrollProcessingController::class);
    Route::get('get-payrolls-erpurls' , 'PayrollProcessingController@payrollsErpUrls');
    Route::post('payrolls-erpurls' , 'PayrollProcessingController@storePayrollsErpUrls');
    Route::get('payroll-employee-columns/{payroll}' , 'PayrollProcessingController@payrollEmployeeColumns');
    Route::post('payroll-employee-columns/{payroll}' , 'PayrollProcessingController@storePayrollEmployeeColumns');
    // Calculation Methods
    Route::resource('calculationMethods' , CalculationMethodsController::class);
    // Overtime Administration
    Route::resource('overtimeAdministration' , OvertimeAdministrationController::class);
    // Job Details Setup
    Route::resource('jobDetailsSetup' , JobDetailsSetupController::class);
    // Data Import
    Route::resource('dataImport' , DataImportController::class);
    // Manage Metadata
    Route::resource('manageMetadata' , ManageMetadataController::class);
    // HR Form Management
    Route::resource('hrFormManagement' , HRFormManagementController::class);
    // Company Payroll
    Route::resource('companyPayroll' , CompanyPayrollController::class);
    // Employee Training Sessions
    Route::resource('employeeTrainingSessions' , EmployeeTrainingSessionController::class);
    // Payroll Columns
    Route::resource('payrollColumns' , PayrollColumnsController::class);
    // Attendance Dashboard
    Route::resource('attendance_dashboard' , AttendanceDashboardController::class);
    // Manual Attendance
    Route::resource('manualAttendance' , ManualAttendanceController::class);
    // Monthly KPI Report
    Route::resource('monthlyKPIReport' , MonthlyKPIController::class);
    // Yearly KPI Report
    Route::resource('yearlyKPIReport' , YearlyKPIController::class);
    // Company Loans
    Route::resource('companyLoans' , CompanyLoansController::class);
    // Loans
    Route::resource('loans' , LoanController::class);
    // Asset Issuance
    Route::resource('assetIssuance' , AssetIssuanceController::class);
    // Entitlement
    Route::resource('entitlement' , EntitlementController::class);
    // Entitlement Assignment
    Route::resource('entitlementAssignment' , EntitlementAssignmentController::class);
    // Key Performance Indicator
    Route::resource('performanceIndicator' , PerformanceIndicatorController::class);
    // Insurance Types
    Route::resource('insuranceTypes' , InsuranceTypeController::class);
    // Insurance Sub Types
    Route::resource('insuranceSubTypes' , InsuranceSubTypeController::class);
    // Salary Slip
    Route::resource('salary_slip' , SalarySlipController::class);
    // Staff Directory
    Route::resource('staff_directory' , StaffDirectoryController::class);
    Route::get('attachment_download/{id}' , 'StaffDirectoryController@attachment_download');
    // Insurance Allocation
    Route::resource('insuranceAllocation' , InsuranceAllocationController::class);
    // Payroll History
    // Route::resource('payrollHistory' , PayrollHistoryController::class);
    // Employee Disciplinary Action
    Route::resource('employee_disciplinary_action' , EmployeeDisciplinaryActionController::class);
    // Job Positions
    Route::resource('job_positions' , JobPositionsController::class);
    // Company Banks
    Route::resource('company_banks' , CompanyBanksController::class);
    // Bulk Shift Management
    Route::resource('bulk_shift_management', BulkShiftController::class);
    // Bulk Time In
    Route::resource('bulk_time_in_department', BulkTimeInDepartmentController::class);
    // Bulk Bulk Attendance Job
    Route::resource('bulk_attendance_job_wise', BulkAttendanceJobController::class);
    // Bulk Time In Employee Controller
    Route::resource('bulk_time_in_employee', BulkTimeInEmployeeController::class);
    // Bulk Time Out
    Route::resource('bulk_time_out', bulkTimeOutController::class);
    Route::resource('bulk_time_out_jsml', bulkTimeOutJsmlController::class);
    // Report Columns
    Route::resource('report_columns', ReportColumnsController::class);
    // Overtime Hourly Pay And Net Salary Column
    Route::resource('overtime_hourly_paycolumn', OvertimeHourlyAndNetSalaryColumnController::class);
    // Consolidated Bank Sheet
    Route::resource('consolidated_bank_sheet', ConsolidatedSheetController::class);
    // Fuel Rate
    Route::resource('fuel_rate', FuelRateController::class);
    // Department Wise Salary Summary
    Route::resource('department_wise_salary', DepartmentWiseSalaryController::class);
    // Salary Componenet History
    Route::resource('salary_comp_history', SalaryComponentHistoryController::class);
    // Payroll Bulk Edit
    Route::resource('payroll_bulk_edit', PayrollBulkEditController::class);
    // Salary Plan
    Route::resource('salary_plan', SalaryPlanController::class);
    // Tax Report
    Route::resource('tax_report', TaxReportController::class);
    // Employee History
    Route::resource('employee_history', EmployeeHistoryController::class);
    Route::get('single_employee_history', 'EmployeeHistoryController@single_employee_history');
    // Attendance Report
    Route::resource('attendance_report', AttendanceReportController::class);
    Route::get('absentessReport', 'AttendanceReportController@index');
    Route::get('missing_checkout_report', 'AttendanceReportController@index');
    // Attendance History
    Route::get('attendance_history', 'AttendanceReportController@index');
    // Attendance Approval Controller
    Route::resource('attendance_approval', AttendanceApprovalController::class);
    // JSML Salary Slip
    // Route::resource('jsml_salary_slip', JSMLSalarySlipController::class);
    // PaySlip Templates
    Route::resource('payslip_template', PayslipTemplateController::class);
    Route::post('payslip_template/{id}' , 'PayslipTemplateController@update');

    // Miscellaneous Request Type
    Route::resource('MiscellaneousRequestType', MiscellaneousRequestTypeController::class);
    Route::resource('fineReversal', FineReversalRequestController::class);

    // Reports
    Route::get('bank_salary_sheet', 'ReportController@bank_salary_sheet');
    Route::get('time_conflict_report', 'ReportController@time_conflict_report');
    Route::get('login-report', 'ReportController@LoginReport');
    Route::get('gross_salary_report', 'ReportController@gross_salary_report');
    Route::get('bank_salary_report', 'ReportController@bank_salary_report');
    Route::get('departmentWiseSalary', 'ReportController@departmentWiseSalary');
    Route::get('designationWiseSalary', 'ReportController@designationWiseSalary');
    Route::get('salaryHistory', 'ReportController@salaryHistory');
    Route::get('departmentWiseSalarySummery', 'ReportController@departmentWiseSalarySummery');
    Route::get('eobiMonthlyReport', 'ReportController@eobiMonthlyReport');
    Route::get('uifMonthlyReport', 'ReportController@uifMonthlyReport');
    Route::get('uifReport', 'ReportController@uifReport');
    Route::get('UIFFile', 'ReportController@UIFFile');
    Route::get('IRP5Report', 'ReportController@IRP5Report');
    Route::get('EMP501 Report', 'ReportController@EMP501 Report');
    Route::get('EMP501 ETI Breakdown Report', 'ReportController@EMP501 ETI Breakdown Report');
    Route::get('EMP201 Breakdown Report', 'ReportController@EMP201 Breakdown Report');
    Route::get('EMP201', 'ReportController@EMP201');
    Route::get('EEA2 Report', 'ReportController@EEA2 Report');
    Route::get('EEA2 Detail Report', 'ReportController@EEA2 Detail Report');
    Route::get('EEA4Report', 'ReportController@EEA4Report');
    Route::get('EEA4DetailReport', 'ReportController@EEA4DetailReport');
    Route::get('DirectiveNumberReport', 'ReportController@DirectiveNumberReport');
    Route::get('COIDAReport', 'ReportController@COIDAReport');
    Route::get('COIDABreakdownReport', 'ReportController@COIDABreakdownReport');
    Route::get('ComponentVarianceReport', 'ReportController@ComponentVarianceReport');
    Route::get('ComponentVarianceTotalsReport', 'ReportController@ComponentVarianceTotalsReport');
    Route::get('PayrollReconReport', 'ReportController@PayrollReconReport');
    Route::get('importView', 'ReportController@importView');
    Route::post('import', 'ReportController@import')->name('import');
    Route::get('eobiMonthlySummery', 'ReportController@eobiMonthlySummery');
    Route::get('pessiMonthlyReport', 'ReportController@pessiMonthlyReport');
    Route::get('pessiMonthlySummery', 'ReportController@pessiMonthlySummery');
    Route::get('providentFundReport', 'ReportController@pessiMonthlyReport');
    Route::get('leave_report', 'ReportController@leaveReport');
    Route::get('annualLeaveEncashmentReport', 'ReportController@annualLeaveEncashmentReport');
    Route::get('department_wise_salary_report', 'ReportController@departmentWiseSalaryReport');
    Route::get('location_wise_report', 'ReportController@locationWiseReport');
    Route::get('loanReport', 'ReportController@loan_report');
    Route::get('graph', 'ReportController@graph');
    Route::get('deductionReport', 'ReportController@deduction_report');
    Route::get('getPayrollsByMonth/{month}', 'ReportController@getPayrollsByMonth');
    Route::get('payrollLedger', 'ReportController@payrollLedger');

    // Gl Settings
    Route::post('glsettings-lockunlock/{id}', 'GlsettingController@lockUnlock');
    Route::get('loan-advance-glsettings', 'GlsettingController@loanAdvanceIndex');
    Route::post('loan-advance-glsettings', 'GlsettingController@StoreLoanAdvance');
    Route::resource('glSettings', GlsettingController::class);
    Route::resource('glAccounts', GlAccountController::class);

    // CurrencyType Setup
    Route::resource('currency-types', CurrencyTypeController::class);

    // Industry Code Setup
    Route::resource('industry-codes', IndustryCodeController::class);

    // Industry Group Setup
    Route::resource('industry-groups', IndustryGroupController::class);

    // getCheckBookByBanks
    Route::get('getCheckBookByBanks/{bank_id}', 'LoanRequestController@getCheckBookByBanks');
    Route::get('getCheckBookByLeafs/{cheque_book_id}', 'LoanRequestController@getCheckBookByLeafs');

    Route::apiResource('taxes', TaxController::class);

    Route::get('employees-present-on-holidays', 'AttendanceReportController@employeesPresentOnHolidays');
});

require __DIR__.'/auth.php';
