<?php

use App\Models\Attendance;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth Controllers
Route::post('login', 'UserController@login');
Route::post('logout', 'UserController@logout');
Route::post('checkIn','AttendanceController@checkIn')->withoutMiddleware("throttle:api");
Route::post('checkOut','AttendanceController@checkOut')->withoutMiddleware("throttle:api");
Route::middleware(['auth:sanctum'])->group(function(){
    Route::post('logout','UserController@logout');
});

// Attendance App Routes
Route::get('search-user', 'AttendanceAppController@SearchUser');
Route::get('get-employees-list', 'AttendanceAppController@GetEmployeesList');
Route::get('machine-details', 'AttendanceAppController@MachineDetails');
Route::post('register-user', 'AttendanceAppController@RegisterUser');
Route::post('get-status', 'AttendanceAppController@GetStatus');
Route::post('check-in-new', 'AttendanceAppController@CheckInApp');
Route::post('check-out-new', 'AttendanceAppController@CheckOutApp');

// Departments
Route::get('getDepartments', 'ApiController@getDepartments');
// Employees
Route::get('getEmployees', 'ApiController@getEmployees');
Route::get('getEmployeeDetails', 'ApiController@getEmployeeDetails');
Route::post('markAttendance', 'ApiController@markAttendance');
// Checks
Route::get('attendance_check_handle', 'ApiController@attendance_check_handle');

// Miscellaneous Request
Route::get('getMiscellaneousRequestType', 'ApiController@getMiscellaneousRequestType');
Route::post('miscellaneousRequest', 'ApiController@miscellaneousRequest');

// Leave Request
Route::get('getLeaveTypes' , 'ApiController@getLeaveTypes');
Route::post('leave_request' , 'ApiController@leaveRequest');
Route::post('shortLeaveRequest', 'ApiController@shortLeaveRequest');

// Loan Request
Route::get('getLoanTypes', 'ApiController@getLoanTypes');
Route::post('loanRequest', 'ApiController@loanRequest');

// Overtime Management Request
Route::post('overtime_request' , 'ApiController@overtimeRequest');
Route::get('overtime_check_holiday', 'ApiController@holiday_check');

// Sgift Change
Route::resource('shift_change' , ShiftChangeController::class);
Route::get('get_employees_shift', 'ShiftChangeController@get_employees_shift');


// Loan Summary
Route::post('loan_summary', 'LoanSummaryController@loanSummary');

// Attendance
Route::post('monthWiseManual', 'ApiController@monthWiseManual');
Route::post('ManualAttendance', 'ApiController@ManualAttendance');

// Salary Department Check
Route::post('salary_dept_check', 'SalaryDeptCheckController@salary_dept_check');
// Department Salary Check
Route::get('dept_salary_check', 'SalaryDeptCheckController@dept_salary_check');
// Department change Request
Route::post('departmentChange', 'ApiController@departmentChange');
// Salary Component
Route::get('getCalculationGroups', 'ApiController@getCalculationGroups');
Route::get('getEmployeeSalaryComponents', 'ApiController@getEmployeeSalaryComponents');
Route::post('storeEmployeeComponents', 'ApiController@storeEmployeeComponents');

// Leave Approvals Request
Route::get('approvals', 'ApiController@Approvals');
Route::post('approvals_status', 'ApiController@approvalStatus');

// Attendance Report
Route::get('attendanceReport', 'ApiController@attendanceReport');
Route::get('getAttendanceReportEmployees', 'ApiController@getAttendanceReportEmployees');

//Salary Sheet
Route::get('downloadSalarySheet', 'ApiController@downloadSalarySheet');

//Attendance Sheet
Route::get('downloadAttendanceSheet', 'ApiController@downloadAttendanceSheet');

