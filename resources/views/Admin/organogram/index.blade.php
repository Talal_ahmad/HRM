@extends('Admin.layouts.master')
@section('title', 'Company Organogram')

@section('content')
<style>
    #chart-container {
    font-family: Arial;
    height: 420px;
    /* border: 1px solid #aaa; */
    overflow: auto;
    text-align: center;
  }
  .orgchart .node .content{
    width: 151px;
    font-size: 12px !important;
    height: 23px !important;
    margin-left: 0 !important;

  }
  .orgchart .node .title{
    line-height: 25px !important;
    text-align: center !important;
  }
  #github-link {
    display: inline-block;
    background-image: url("https://dabeng.github.io/OrgChart/img/logo.png");
    background-size: cover;
    width: 64px;
    height: 64px;
    position: absolute;
    top: 0px;
    left: 0px;
  }

    #chart-container {
      height: 65vh;
    }
    .orgchart .node .title {
      height: 50px;
      text-align: left;
      line-height: 40px;
      width: 200px;
    }
    .orgchart .node .content {
      text-align: left;
      padding: 5px;
    }
    .orgchart .node .content .symbol {
      margin-right: 20px;
    }
    .oci-leader::before, .oci-leader::after {
      background-color: #3d3d3d ;
    }
    .orgchart .node .avatar {
      width: 75px;
      height: 75px;
      border-radius: 50%;
      /* float: left; */
      margin: 5px;
    }
    .orgchart .node .title {background-color:  #3d3d3d !important;}
    .title .avatar{
      position: relative;
      bottom: 50px;
      right: 65px;
      border: 5px solid #6f4fa6;
    }
</style>

    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Company Organogram</h2>
{{-- <img src="{{asset('upload')}}{{user_image(2)}}" alt=""> --}}

                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Setup</a>
                                </li>
                                <li class="breadcrumb-item active">Company Organogram
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="card">
                <div id="chart-container"></div>
                {{-- <a id="github-link" href="http://github.com/dabeng" target="_blank"></a> --}}
            </div>
        </div>
    </section>
@endsection
@php
// dd($Result);
@endphp
@section('scripts')
<script type="text/javascript" src="{{asset('js/jquery.orgchart.js')}}"></script>
<script>
  (function ($) {
  $(function () {
    var datas = '{!! json_encode($Result) !!}';
    
    console.log(datas);
    var ds = JSON.parse(datas);
    // {
    //   id: "1",
    //   name: "Lao Lao",
    //   title: "general manager",
    //   children: [
    //     { id: "2", name: "Bo Miao", title: "department manager" },
    //     {
    //       id: "3",
    //       name: "Su Miao",
    //       title: "department manager",
    //       children: [
    //         { id: "4", name: "Tie Hua", title: "senior engineer" },
    //         {
    //           id: "5",
    //           name: "Hei Hei",
    //           title: "senior engineer",
    //           children: [
    //             { id: "6", name: "Pang Pang", title: "engineer" },
    //             { id: "7", name: "Xiang Xiang", title: "UE engineer" }
    //           ]
    //         }
    //       ]
    //     },
    //     { id: "8", name: "Yu Jie", title: "department manager" },
    //     { id: "9", name: "Yu Li", title: "department manager" },
    //     { id: "10", name: "Hong Miao", title: "department manager" },
    //     { id: "11", name: "Yu Wei", title: "department manager" },
    //     { id: "12", name: "Chun Miao", title: "department manager" },
    //     { id: "13", name: "Yu Tie", title: "department manager" }
    //   ]
    // };
    
      
    var oc = $("#chart-container").orgchart({
      // exportButton: true,
      exportFilename: "MyOrgChart",
      data: ds,
      nodeContent: "title",
      nodeID: "id",
      createNode: function ($node, data) {
        // console.log(data.id);
        // $node.find(".title").append(`
        //   <img class="avatar" src="${user_image(data.id)}" crossorigin="anonymous" />
        // `);
        // $node.find(".content").prepend($node.find(".symbol"));
        $.ajax({
          url: `{{ url('organoGramSetup') }}/${data.id}`, // Replace with your server endpoint for fetching images
          method: 'GET',
          success: function (response) {
            // Use the retrieved image URL to set the node's image
            $node.find(".title").append(`
              <img class="avatar" src="${response.image}" crossorigin="anonymous" />
            `);
            $node.find(".content").prepend($node.find(".symbol"));
          },
          error: function (error) {
            console.error('Error fetching user image:', error);
          }
        });
      }
    });
  });
})(jQuery);

</script> 
@endsection