@extends('Admin.layouts.master')
@section('title', 'Disciplinaries-Index')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Disciplinaries')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.Disciplinaries')</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @can('Issue-New-Disciplinarie')
    <div class="col-12">
        <div class="card mb-1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary mt-2" data-bs-toggle="modal" data-bs-target="#add_modal">@lang('app.Issue_Disciplinaries')<b>+</b></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan
</section>
<section>
    <div class="modal-size-lg d-inline-block">
        <!-- Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Issue_Disciplinaries ')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" action="{{url('issue_disciplinaries')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee_id">@lang('app.Select_Employee')</label>
                                            <select name="employee_id" id="employee_id" class="select2 form-select" placeholder="@lang('app.Select_Employee')" required>
                                                <option value="">@lang('app.Select_Employee')</option>
                                                @foreach($employees as $employee)
                                                <option value="{{$employee->id}}" {{$employee->id ==request('employee') ? 'selected' : ""}}>{{$employee->employee_id." ".$employee->first_name." ".$employee->middle_name." ".$employee->last_name ." ".$employee->employee_code." ".$employee->job_title." ".$employee->department}}</option>
                                                @endforeach
                                            </select>
                                            @error('employee_id')
                                            <div class="error">{{ "employee not selected" }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="warning_rule">@lang('app.Warning_Rule')</label>
                                            <select name="warning_rule" id="warning_rule" class="select2 form-select" required>
                                                <option value="">@lang('app.Select_Type')</option>
                                                @foreach($rules as $rule)
                                                <option value="{{$rule->id}}">{{$rule->rule}}</option>
                                                @endforeach
                                           
                                            </select>
                                            @error('warning_rule')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="warning_type">@lang('app.Warning_Type')</label>
                                            <select name="warning_type" id="warning_type" class="select2 form-select" required>
                                                <option value="">@lang('app.Select_Type')</option>
                                                <option value="Notice">@lang('app.Notice')</option>
                                                <option value="Verbal Warning">@lang('app.Verbal_Warning')</option>
                                                <option value="Written Warning">@lang('app.Written_Warning')</option>
                                                <option value="Final Warning">@lang('app.Final_Warning')</option>
                                                <option value="Dismissal">@lang('app.Dismissal')</option>
                                            </select>
                                            @error('warning_type')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="issuername">@lang('app.Issuer_Name')</label>
                                            <input type="text" name="issuername" id="issuername" class="form-control " placeholder="@lang('app.Issuer_Name')" />
                                        </div>
                                    </div>
                                    @error('issuername')
                                            <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="attachment_image">@lang('app.Image_Attachment')</label>
                                            <input type="file" name="attachment_image" id="attachment_image" class="form-control " placeholder="@lang('app.Image_Attachment')" />
                                        </div>
                                    </div>
                                    @error('attachment_image')
                                            <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="message">@lang('app.Message')</label>
                                        <textarea name="message" id="message" class="form-control" placeholder="@lang('app.Enter_message_its_Optional')"></textarea>
                                    </div>
                                    @error('message')
                                            <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="hidden" name="created_by" id="created_by" value="{{auth()->user()->id}}">

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                            <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')

@endsection