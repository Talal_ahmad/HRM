@extends('Admin.layouts.master')
@section('title', 'Disciplinaries-Table')
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Disciplinaries</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Disciplinaries
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="basic-dataTable">
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <!-- <th>Sr.No</th> -->
                                    <th>Employee Code</th>
                                    <th>Employee Name</th>
                                    <th>Employee Designation</th>
                                    <th>Employee Department</th>
                                    <th>Violated Rule</th>
                                    <th>Warning Type</th>
                                    <th>Description</th>
                                    <th>Issued by</th>
                                    @if (userRoles()!="Self Service")
                                        <th>Reply</th>
                                        <th>Attachment_Image</th>
                                        <th>Action</th>
                                        @can('Auth-Reply')
                                        <th>Add Reply</th>
                                        @endcan
                                    @endif

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($disciplinaries as $key => $disciplinarie)
                                <tr>
                                    <td class="not_include"></td>
                                    <!-- <td>{{$key+1}}</td> -->
                                    <td>{{$disciplinarie->employee_code}}</td>
                                    <td>{{$disciplinarie->first_name." ".$disciplinarie->middle_name." ".$disciplinarie->last_name}}</td>
                                    <td>{{$disciplinarie->designation}}</td>
                                    <td>{{$disciplinarie->employee_department}}</td>
                                    <td>{{$disciplinarie->rule}}</td>
                                    <td>{{$disciplinarie->warning_type}}</td>
                                    <td>{{$disciplinarie->message}}</td>
                                    <td>{{$disciplinarie->Issuer_name}}</td>
                                    @if (userRoles()!="Self Service")
                                        @can('Auth-Reply')
                                        @if(!empty($disciplinarie->reply))
                                            <td><a href="#" class="item-edit font-medium-4 me-50" onclick=disciplinaries_watch_reply({{$disciplinarie->id}})> <p class="badge bg-light-success">Watch-Reply</p></a></td>
                                            @else
                                            <td><p class="badge bg-light-danger">Not-Replied</p></td>
                                            @endif
                                        @endcan    
                                        <td>
                                            <img src="{{asset($disciplinarie->attachment_image)}}" width="50px" height="50px" class="rounded-circle ms-2" alt="" srcset="" onclick="imagePopUp(this.src)">
                                        </td>
                                        @can('Add-or-Delete-Disciplinaries')
                                        <td>
                                            <a href="#" class="item-edit font-medium-4 me-50" onclick=disciplinaries_edit({{$disciplinarie->id}})><i class="far fa-edit"></i></a>
                                            <a href="#" class="font-medium-4 text-danger" onclick=disciplinaries_delete({{$disciplinarie->id}})><i class="far fa-trash-alt"></i></a>
                                        </td>
                                        @endcan
                                        @can('Add Reply To Diciplinaries')
                                            <td>
                                                <a href="#" class="item-edit font-medium-4 me-50" onclick=disciplinaries_reply({{$disciplinarie->id}})><i class="fa-solid fa-reply-all"></i></a>                                   
                                            </td>
                                        @endcan
                                    @endif
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Edit Modal -->
    <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">Edit Disciplinaries</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form" action="{{url('update_disciplinaries')}}" enctype="multipart/form-data" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="employee_id">Select Employee</label>
                                        <select name="employee_id" id="employee_id" class="select2 form-select" placeholder="Select Employee" required>
                                            <option value="">Select Employee</option>
                                            @foreach($employees as $employee)
                                            <option value="{{$employee->id}}">{{$employee->first_name." ".$employee->middle_name." ".$employee->last_name}}</option>
                                            @endforeach
                                        </select>
                                        @error('employee_id')
                                        <div class="error">{{ "employee not selected" }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="warning_type">Warning Type</label>
                                        <select name="warning_type" id="warning_type" class="select2 form-select" required>
                                            <option value="">Select Type</option>
                                            <option value="Notice">Notice</option>
                                            <option value="Verbal Warning">Verbal Warning</option>
                                            <option value="Written Warning">Written Warning</option>
                                            <option value="Final Warning">Final Warning</option>
                                            <option value="Dismissal">Dismissal</option>
                                        </select>
                                        @error('warning_type')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="warning_rule">Warning Rule</label>
                                        <select name="warning_rule" id="warning_rule" class="select2 form-select" required>
                                            <option value="">Select Type</option>
                                            @foreach($rules as $rule)
                                            <option value="{{$rule->id}}">{{$rule->rule}}</option>
                                            @endforeach

                                        </select>
                                        @error('warning_rule')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="attachment_image">New Image_Attachment</label>
                                        <input type="file" name="attachment_image" id="attachment_image" class="form-control " placeholder="Choose New Attachment_Image" />
                                    </div>
                                </div>
                                @error('attachment_image')
                                <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1" id="imagediv">

                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="issuername">Issuer Name</label>
                                        <input type="text" name="issuername" id="issuername" class="form-control " placeholder="Issuer Name" />
                                    </div>
                                </div>
                                @error('issuername')
                                <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <input type="hidden" name="disciplinaries_id" id="disciplinaries_id" class="form-control " placeholder="Choose New Attachment_Image" />

                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="message">Message</label>
                                    <textarea name="message" id="message" class="form-control" placeholder="Enter message its Optional"></textarea>
                                </div>
                                @error('message')
                                <div class="error">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-outline-success">Reset</button>
                        <button type="submit" class="btn btn-primary form_save" id="save">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Edit Modal -->
    <!-- Image Modal -->

    <div class="modal fade" id="ModalImage" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLabel">Record Image</h5>
                </div>
                <div class="modal-body" id="imagemodal">

                </div>
                <div class="modal-body" id="downloadimage">

                </div>
            </div>
        </div>
    </div>
    <!-- End Image Modal -->
     <!-- Reply Modal -->
     <div class="modal fade text-start" id="reply_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">Reply Modal</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form" action="{{url('add_reply')}}" enctype="multipart/form-data" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1" id="replyimagediv">

                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                   
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="reply_warning_rule">Warning Rule</label>
                                        <select name="reply_warning_rule" id="reply_warning_rule" class="select2 form-select" required disabled>
                                            <option value="" selected>Select Type</option>
                                            @foreach($rules as $rule)
                                            <option value="{{$rule->id}}">{{$rule->rule}}</option>
                                            @endforeach

                                        </select>
                                        @error('warning_rule')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="reply_issuername">Issuer Name</label>
                                        <input type="text" name="reply_issuername" id="reply_issuername" class="form-control " placeholder="Issuer Name" disabled/>
                                    </div>
                                </div>
                                @error('issuername')
                                <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="reply_message">Message</label>
                                    <textarea name="reply_message" id="reply_message" class="form-control" disabled></textarea>
                                </div>
                                @error('message')
                                <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="reply">Add Reply</label>
                                    <textarea name="reply" id="reply" class="form-control" placeholder="Add Your Reply"></textarea>
                                </div>
                                @error('message')
                                <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <input type="hidden" name="reply_disciplinaries_id" id="reply_disciplinaries_id" class="form-control "  />

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-outline-success">Reset</button>
                        <button type="submit" class="btn btn-primary form_save" id="save">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
     <!-- End Reply Modal -->
     <!-- Wactch Reply Modal -->
     <div class="modal fade text-start" id="watch_reply_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">Reply Box</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                 <div class="modal-body">
                        <div class="row">                           
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="watch_reply_message">Reply</label>
                                    <textarea name="watch_reply_message" id="watch_reply_message" class="form-control" disabled></textarea>
                                </div>
                            </div>
                        </div>
                    </div>          
            </div>
        </div>
    </div>
     <!-- End Watch Reply Modal -->
</section>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
            ordering: true,
            "columnDefs": [{
                // For Responsive
                className: 'control',
                orderable: false,
                targets: 0
            }],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({
                    class: 'font-small-4 me-50'
                }) + 'Export',
                buttons: [{
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Csv',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Pdf',
                        className: 'dropdown-item',
                        orientation: 'landscape',
                        footer: true,
                        pageSize: 'A4',
                        customize: function(doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();
                            doc.pageMargins = [20, 60, 20, 30];
                            // Set the font size fot the entire document
                            doc.defaultStyle.fontSize = 12;
                            // Set the fontsize for the table header
                            doc.styles.tableHeader.fontSize = 10;
                            // Create a header
                            doc["header"] = function() {
                                return {
                                    columns: [{
                                        alignment: "center",
                                        italics: true,
                                        text: "Disciplinaries Report",
                                        fontSize: 18,
                                        margin: [10, 0]
                                    }, ],
                                    margin: 20
                                };
                            };
                            doc["footer"] = function(page, pages) {
                                return {
                                    columns: [{
                                            alignment: "left",
                                            text: ["Created on: ", {
                                                text: jsDate.toString()
                                            }]
                                        },
                                        {
                                            alignment: "right",
                                            text: [
                                                "page ",
                                                {
                                                    text: page.toString()
                                                },
                                                " of ",
                                                {
                                                    text: pages.toString()
                                                }
                                            ]
                                        }
                                    ],
                                    margin: 20
                                };
                            };
                            var objLayout = {};
                            objLayout["hLineWidth"] = function(i) {
                                return 0.5;
                            };
                            objLayout["vLineWidth"] = function(i) {
                                return 0.5;
                            };
                            objLayout["hLineColor"] = function(i) {
                                return "#aaa";
                            };
                            objLayout["vLineColor"] = function(i) {
                                return "#aaa";
                            };
                            objLayout["paddingLeft"] = function(i) {
                                return 4;
                            };
                            objLayout["paddingRight"] = function(i) {
                                return 4;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        // action: newexportaction,
                        exportOptions: {
                            columns: ':not(.not_include)',
                            // tfoot:true,
                        }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    }
                ],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            }],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">Disciplinaries Table</h6>');
    });

    function disciplinaries_edit(id) {
        $.ajax({
            url: "disciplinaries_edit/" + id,
            success: function(data) {
                $('#employee_id').val(data.employee_id).select2();
                $('#warning_type').val(data.warning_type).select2();
                $('#warning_rule').val(data.warning_rule).select2();
                $('#message').val(data.message);
                $('#disciplinaries_id').val(data.id);
                $('#issuername').val(data.Issuer_name);
                $('#imagediv').html(`<img src="${data.attachment_image}" width="50px" height="50px" class="rounded-circle" alt="" srcset="">`).append();
                $('#add_modal').modal('show');
            }
        });
    }
    function disciplinaries_reply(id){
        $.ajax({
            url: "disciplinaries_edit/" + id,
            success: function(data) {
                $('#reply_warning_rule').val(data.warning_rule).select2();
                $('#reply_message').val(data.message);
                $('#reply_disciplinaries_id').val(data.id);
                $('#reply_issuername').val(data.Issuer_name);
                $('#replyimagediv').html(`<img src="${data.attachment_image}" width="400px" height="400px" class="rounded" alt="" srcset="">`).append();
                $('#reply_modal').modal('show');
            }
        });

    }

    function disciplinaries_delete(id) {
        $.ajax({
            url: "disciplinaries_delete/" + id,
            success: function() {
                Toast.fire({
                    icon: 'success',
                    title: 'Disciplinarie has been Deleted Successfully!'
                })
                location.reload();

            },
            error: function(xhr) {
                Toast.fire({
                    icon: 'error',
                    title: 'An error has been occured! Please Contact Administrator.'
                });
            }
        });

    }

    function imagePopUp(src) {
        $('#imagemodal').html(`<img src="${src}" width="250px" height="250px" class="rounded" alt="" srcset="">`).append();
        $('#downloadimage').html(`<a href="${src}" download="${src}" class="btn btn-primary">Download</a>`).append();
        $('#ModalImage').modal('toggle');


    }
    function disciplinaries_watch_reply(id){
        $.ajax({
            url: "disciplinaries_edit/" + id,
            success: function(data) {
                $('#watch_reply_message').val(data.reply);
              $('#watch_reply_modal').modal('show');
            }
        });
    }
</script>








@endsection