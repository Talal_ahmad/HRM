@extends('Admin.layouts.master')
@section('title', 'Disciplinaries-RuleTable')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Disciplinaries_Rules')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Disciplinaries_Rules_Table')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header row"></div>
        <div class="content-body">
            <section id="basic-dataTable">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include">SR.NO</th>
                                        <th>Rule</th>
                                        <th>Status</th>
                                        <th >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $warning_rules as $key => $rule)
                                    <tr>
                                        <td class="not_include">{{$key+1}}</td>
                                        <td>{{$rule->rule}}</td>
                                        @if($rule->status == 1)
                                        <td> <p class="badge bg-light-success">Active</p></td>
                                         @else
                                        <td><p class="badge bg-light-danger">In-Active</p></td>
                                        @endif
                                        @can('Edit-or-Delete-Disciplinaries-Rules')
                                        <td>
                                        <a href=""  data-bs-toggle="modal" data-bs-target="#edit-modal" class="item-edit font-medium-4 me-2" onclick= disciplinaries_Rules_edit({{$rule->id}})><i data-feather='edit'></i></a>
                                        <a href=""  data-bs-toggle="modal" data-bs-target="#edit-modal" class="font-medium-4 text-danger" onclick= disciplinaries_Rules_delete({{$rule->id}})><i data-feather='trash-2'></i></a>
                                        </td>
                                        @endcan
                                        
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Disciplinaries_Rule')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" action="{{url('create_disciplinaries_rule')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="rule">@lang('app.Rule')</label>
                                        <input type="text" name="rule" id="rule" class="form-control" placeholder="@lang('app.Add_Rule')">
                                    </div>
                                    @error('message')
                                            <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="status">@lang('app.Status')</label>
                                            <select name="status" id="status" class="select2 form-select" placeholder="@lang('app.Select_Employee')" required>
                                                <option value="1">@lang('app.Active')</option>
                                                <option value="0">@lang('app.In_Active')</option>
                                               
                                               
                                            </select>
                                            @error('status')
                                            <div class="error">{{ "employee not selected" }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                            <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Add Modal -->
        <!-- Edit Modal -->
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Disciplinaries_Rule')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" action="{{url('update_disciplinaries_rule')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="editrule">@lang('app.Rule')</label>
                                        <input type="text" name="editrule" id="editrule" class="form-control" placeholder="@lang('app.Add_Rule')">
                                    </div>
                                    @error('message')
                                            <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="editstatus">@lang('app.Status')</label>
                                            <select name="editstatus" id="editstatus" class="select2 form-select" placeholder="@lang('app.Select_Employee')" required>
                                                <option value="1">@lang('app.Active')</option>
                                                <option value="0">@lang('app.In_Active')</option>
                                               
                                               
                                            </select>
                                            @error('status')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                      <input type="hidden" id="editruleid" name="editruleid">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                            <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <!-- End Edit Modal -->
        <!-- Image Modal -->

<div class="modal fade" id="ModalImage" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">@lang('app.Record_Image')</h5>
      </div>
      <div class="modal-body" id="imagemodal">

      </div>
      
    </div>
  </div>
</div>
        <!-- End Image Modal -->
    </section>
@endsection

@section('scripts')
    <script>
$(document).ready(function() {
        $('#dataTable').DataTable({
            ordering: true,
            "columnDefs": [{
                // For Responsive
                className: 'control',
                orderable: false,
                targets: 0
            }, ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 50,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
            {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
        ],
            
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">Disciplinaries Rules Table</h6>');
    });

    function disciplinaries_Rules_edit(id){
        
        $.ajax({
            url: "disciplinaries_Rules_edit/"+id,
             success: function(data){
                console.log(data)
                $('#editruleid').val(data.id);
                $('#editrule').val(data.rule);
                $('#editstatus').val(data.status).select2();
                $('#edit_modal').modal('show');
                
  }});

    }
    function disciplinaries_Rules_delete(id){
        $.ajax({
            url: "disciplinaries_Rules_delete/"+id,
            success: function() {
                Toast.fire({
                    icon: 'success',
                    title: 'Disciplinaries_Rules has been Deleted Successfully!'
                })
                location.reload();

            },
            error: function(xhr) {
                Toast.fire({
                    icon: 'error',
                    title: 'An error has been occured! Please Contact Administrator.'
                });
            }
});
       
    }
    function imagePopUp(src){
        $('#imagemodal').html(`<img src="${src}" width="250px" height="250px" class="rounded" alt="" srcset="">`).append();
        $('#ModalImage').modal('toggle');

    }
</script>








@endsection
