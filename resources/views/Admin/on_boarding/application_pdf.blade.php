<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Application Form</title>
    <head>
        <style>
           td, h1, h2, input, label,th,span{
            text-align: left;
            font-size:70px;
            /* margin: 10px,10px,10px; */
           }
           input{
            text-transform: uppercase;
           }
           .remove_brd{
                border-top: none;
                border-left: none;
                border-right: none;
                border-bottom: 2px solid black; 
            }
            .remove_brd_1{
                border-top: none;
                border-left: none;
                border-right: none;
                border-bottom: none; 
            }
           .flex-container {
                display: inline-flex;
                flex-direction: row;
                /* margin-top: 100px; */
            }
            body{
                margin-top: 100px;
                margin-left: 200px;
                margin-right: 200px;
            }
            th,input{
                margin-bottom: 40px;
            }
        </style>
    </head>
</head>
<body >
    <img src="{{asset('images/company_logo/clinix.png')}}" alt="profile image" style=" width:1400px;" />
    @if (!empty($boarding->title))
    <pre><h1>User Name: {{$boarding->username}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Source: {{$boarding->title}}</h1></pre>
    @else
    <pre><h1>User Name: Online &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Source: Website</h1></pre>
    @endif
    <h1 style="font-size: 180px; text-align:center; margin-bottom:250px;">Application Form for Employment/Internship</h1>
    <div class="flex-container">
        <table class="table">
            <tbody>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Application No.:" readonly><input type="text" class="remove_brd" size="41" value="{{$boarding->id}}"></td>
                </tr>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Post:" readonly><input type="text" class="remove_brd" size="38" value="{{$boarding->post}}"><input style="border: none;width:auto;" type="text" value="Expected Date Of Joining:" readonly><input type="text" class="remove_brd" size="13" value="{{$boarding->doj}}"></td>

                </tr>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Name:" readonly><input type="text" class="remove_brd" size="25" value="{{$boarding->name}}"><input style="border: none;width:auto;" type="text" value="Email:" readonly><input type="text" class="remove_brd" size="46" value="{{$boarding->email}}"></td>

                </tr>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Father Name:" readonly><input type="text" class="remove_brd" size="71" value="{{$boarding->f_name}}"></td>
                </tr>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="DOB:" readonly><input type="text" class="remove_brd" size="38" value="{{$boarding->dob}}"><input style="border: none;width:auto;" type="text" value="Blood Group:" readonly><input type="text" class="remove_brd" size="26" value="{{$boarding->blood}}"></td>
                </tr>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Cnic:" readonly><input type="text" class="remove_brd" size="38" value="{{$boarding->cnic}}"><input style="border: none;width:auto;" type="text" value="Religion:" readonly><input type="text" class="remove_brd" size="29" value="{{$boarding->religion}}"></td>
                </tr>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Marital Status:" readonly><input type="text" class="remove_brd" size="40" value="{{$boarding->marital_status}}"><input style="border: none;width:auto;" type="text" value="No. of Children:" readonly><input type="text" class="remove_brd" size="16" value="{{$boarding->total_child}}"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="flex-container" style="margin-top: 400px; text-align: center; padding-bottom:0px;">
        <a href="#">
            {{-- @if (file_exists(public_path('images/employees/' . $employee->image)))
                <img src="{{asset('images/employees/' . $employee->image)}}" id="account-upload-img" class="me-50" alt="profile image" height="95" width="95" />
            @else --}}
                <img src="{{asset('applicant_images'.'/'.$boarding->image)}}" alt="profile image" style=" height: 900px; width: 900px; margin-left:100px; padding:0px;" />
            {{-- @endif --}}
        </a>
        {{-- <div class="mt-75 ms-1"> --}}
            {{-- @if($employee->status == 'Active') --}}
            {{-- <button class="btn btn-sm btn-primary mb-75" data-bs-toggle="modal" data-bs-target="#employee_image_modal"><i class="fas fa-upload"></i> Upload</button> --}}
            {{-- @endif --}}
        {{-- </div> --}}
    </div>
   
        <table class="table" style="padding-top: 0px;">
            <tbody>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Name & Address of Next of Kins:" readonly><input type="text" class="remove_brd" size="60" value="{{$boarding->kin_address}}"><input style="border: none;width:auto;" type="text" value=" Phone:" readonly><input type="text" class="remove_brd" size="18" value="{{$boarding->kin_phone}}"></td>
                </tr>
            </tbody>
        </table>
        <table class="table">
            <tbody>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Residence Status:" readonly><input type="text" class="remove_brd" size="28" value="{{$boarding->residence_status}}"><input style="border: none;width:auto;" type="text" value="Own Conveyance:" readonly><input type="text" class="remove_brd" size="52" value="{{$boarding->own_conveyance}}"></td>
                </tr>
            </tbody>
        </table>
    <div class="col-md-12 col-12 table-responsive">
        <table class="table">
            <tbody>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Permanent Address:" readonly><input type="text" class="remove_brd" size="69" value="{{$boarding->permanent_addr}}"><input style="border: none;width:auto;" type="text" value=" Phone:" readonly><input type="text" class="remove_brd" size="18" value="{{$boarding->phone_1st}}"></td>
                </tr>
                <tr>
                    <td><input style="border: none;width:auto;" type="text" value="Present Address:" readonly><input type="text" class="remove_brd" size="70" value="{{$boarding->present_addr}}"><input style="border: none;width:auto;" type="text" value=" Phone:" readonly><input type="text" class="remove_brd" size="18" value="{{$boarding->phone_2nd}}"></td>
                </tr>
            </tbody>
        </table>
    </div>
        <table class="setbox" style="border: 1px solid black;">
            <thead>
                <h1>Acacdemic Qualification</h1>
                <tr>
                    <th style="border: 1px solid black;">Degree</th>
                    <th style="border: 1px solid black;">Name of School/ College/ University attended</th>
                    <th style="border: 1px solid black;">Detail Of education</th>
                    <th style="border: 1px solid black;">Year</th>
                    <th style="border: 1px solid black;">Division</th>
                    <th style="border: 1px solid black;">Grade</th>
                </tr>
            </thead>
            <tbody style="border: 1px solid black">
                @if (!empty($boarding->higher_college_school) || !empty($boarding->higher_year))
                    <tr>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->higher}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->higher_college_school}}" readonly></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->higher_detail}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->higher_year}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->higher_div}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->higher_grade}}"></td>
                    </tr>
                @endif
                @if(!empty($boarding->grad_college_school) || !empty($boarding->grad_year))
                    <tr>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;"  type="text" value="{{$boarding->graduation}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->grad_college_school}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->grad_detail}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->grad_year}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->grad_div}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->grad_grade}}"></td>
                    </tr>
                @endif
                @if(!empty($boarding->inter_college_school) || !empty($boarding->inter_year))
                    <tr>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;"  type="text" value="{{$boarding->inter}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->inter_college_school}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->inter_detail}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->inter_year}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->inter_div}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->inter_grade}}"></td>
                    </tr> 
                @endif
                @if(!empty($boarding->matric_college_school) || !empty($boarding->matric_year))
                    <tr>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;"  type="text" value="{{$boarding->matric}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->matric_college_school}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->matric_detail}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->matric_year}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->matric_div}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->matric_grade}}"></td>
                    </tr>
                    @else
                    <h1>The Applicant is under matric.</h1>
                @endif
            </tbody>
        </table>
    <table style="padding-bottom: 100px; margin-left:200px;">
        <thead>
            <h1>Professional Experience</h1>
            <tr>
                <th style="border: 1px solid black;">Sr. No</th>
                <th style="border: 1px solid black;">Name of Organization</th>
                <th style="border: 1px solid black;">Post</th>
                <th style="border: 1px solid black;">Duration</th>
                <th style="border: 1px solid black;">Salary</th>
                <th style="border: 1px solid black;">Reason For Leaving</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($boarding->organization_name_1))
                <tr>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="1" readonly></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->organization_name_1}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->post_name_1}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->duration_1}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->salary_1}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->leaving_reason_1}}"></td>
                </tr>
                @if (!empty($boarding->organization_name_2))
                    <tr>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="2" readonly></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->organization_name_2}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->post_name_2}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->duration_2}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->salary_2}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->leaving_reason_2}}"></td>
                    </tr>
                    @if (!empty($boarding->organization_name_3))
                        <tr>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="3" readonly></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->organization_name_3}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->post_name_3}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->duration_3}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->salary_3}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->leaving_reason_3}}"></td>
                        </tr>
                        @if (!empty($boarding->organization_name_4))
                            <tr>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="4" readonly></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->organization_name_4}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->post_name_4}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->duration_4}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->salary_4}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->leaving_reason_4}}"></td>
                            </tr>
                        @endif
                    @endif
                @endif
                @else
                <h1>No Skills</h1>
            @endif
        </tbody>
    </table>
    <table style="margin-left:200px;">
        <tbody>
            <tr>
                <td><input style="border: none;width:auto;" type="text" value="Acceptable Salary RS:" readonly><input type="text" class="remove_brd" size="41" value="{{$boarding->accept_salary}}"></td>
            </tr>
            <tr>
                <td><input style="border: none;width:auto;" type="text" value="Language known:" readonly><input type="text" class="remove_brd" size="97" value="{{$boarding->lang_known}}"></td>
            </tr>
            <h1>Particulars of Parents, Brothers and sisters</h1>
            <tr>
                <td><input style="border: none;width:auto;" type="text" value="Father:" readonly><input type="text" class="remove_brd" size="47" value="{{$boarding->father_health_status}}"><input style="border: none;width:auto;" type="text" value=" Mother:" readonly><input type="text" class="remove_brd" size="48" value="{{$boarding->mother_health_status}}"></td>
            </tr>
        </tbody>
    </table>
    <table style="margin-left:200px;">
        <thead>
            <tr>
                <th style="border: 1px solid black;">Sr No.</th>
                <th style="border: 1px solid black;">Relation</th>
                <th style="border: 1px solid black;">Name</th>
                <th style="border: 1px solid black;">Age</th>
                <th style="border: 1px solid black;">Qualification</th>
                <th style="border: 1px solid black;">Occupation</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($boarding->father))
                <tr>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="1"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->father}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->father_name}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->f_age}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->f_qualification}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->f_occupation}}"></td>
                </tr>
                @if (!empty($boarding->mother))
                    <tr>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="2"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->mother}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->mother_name}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->m_age}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->m_qualification}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->m_occupation}}"></td>
                    </tr>
                    @if (!empty($boarding->brother_1))
                        <tr>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="3"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->brother_1}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->brother_1_name}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->b_1_age}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->b_1_qualification}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->b_1_occupation}}"></td>
                        </tr>
                        @if (!empty($boarding->brother_2))
                            <tr>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="4"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->brother_2}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->brother_2_name}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->b_2_age}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->b_2_qualification}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->b_2_occupation}}"></td>
                            </tr>
                            @if (!empty($boarding->sister_1))
                                <tr>
                                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="5"></td>
                                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->sister_1}}"></td>
                                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->sister_1_name}}"></td>
                                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->s_1_age}}"></td>
                                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->s_1_qualification}}"></td>
                                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->s_1_occupation}}"></td>
                                </tr>
                                @if (!empty($boarding->sister_2))
                                    <tr>
                                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="6"></td>
                                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->sister_2}}"></td>
                                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->sister_2_name}}"></td>
                                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->s_2_age}}"></td>
                                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->s_2_qualification}}"></td>
                                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="{{$boarding->s_2_occupation}}"></td>
                                    </tr>
                                @endif
                            @endif
                        @endif
                    @endif
                @endif
                @else
                <h1>Not any Particular</h1>
            @endif
        </tbody>
    </table>
    <table class="table table-bordered mt-2 setbox" style="margin-left:200px;">
        <thead>
            <h1>Any Relative/Friend working in Clinix Pharmacy:</h1>
            <th style="border: 1px solid black;">Sr. No</th>
            <th style="border: 1px solid black;">Relation</th>
            <th style="border: 1px solid black;">Name</th>
            <th style="border: 1px solid black;">Department</th>
            <th style="border: 1px solid black;" colspan="2">Designation &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th></th>
        </thead>
        <tbody>
            @if (!empty($boarding->relation_1))
                <tr>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="1" readonly></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->relation_1}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_name_1}}"></td>
                    <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_department_1}}"></td>
                    <td style="border: 1px solid black;" colspan="2"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_designation_1}}"></td>
                    <td></td>
                </tr>
                @if (!empty($boarding->relation_2))
                    <tr>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="2" readonly></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->relation_2}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_name_2}}"></td>
                        <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_department_2}}"></td>
                        <td style="border: 1px solid black;" colspan="2"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_designation_2}}"></td>
                        <td></td>
                    </tr>
                    @if (!empty($boarding->relation_3))
                        <tr>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="3" readonly></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->relation_3}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_name_3}}"></td>
                            <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_department_3}}"></td>
                            <td style="border: 1px solid black;" colspan="2"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_designation_3}}"></td>
                            <td></td>
                        </tr>
                        @if (!empty($boarding->relation_4))
                            <tr>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" type="text" value="4" readonly></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->relation_4}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_name_4}}"></td>
                                <td style="border: 1px solid black;"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_department_4}}"></td>
                                <td style="border: 1px solid black;" colspan="2"><input class="remove_brd_1" style="margin: 0px;" type="text" value="{{$boarding->rel_designation_4}}"></td>
                                <td></td>
                            </tr>
                        @endif
                    @endif
                @endif
                @else
                <h1>Not any relative working at Clinix</h1>
            @endif
        </tbody>
    </table>
    <table class="table">
        <thead>
        </thead>
        <tbody>
            <tr>
                <h1>Medical History</h1>
                <input name="medical_history" type="text" size="118px" style="height: 100px"  value="{{$boarding->medical_history}}">
            </tr>
    <img src="{{asset('company_documents/oath.png')}}" alt="profile image" style=" width:5500px;" />
            
            <tr>
                {{-- @if (!empty($boarding->upload_time))     --}}
                <td class="pos_set"><input style="border: none;width:auto; margin-top:100px;" type="text" value="Application Date:" readonly><input type="text" class="remove_brd" size="20" value="{{$boarding->upload_time}}"></td>
                <td class="pos_set">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input style="border: none;width:auto; margin-top:100px;" type="text" value="Signature"  readonly><input type="text" class="remove_brd" size="20"></td>
                {{-- @endif --}}
            </tr>
        </tbody>
    </table>
</body>
</html>