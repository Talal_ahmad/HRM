@extends('Admin.layouts.master')
@section('title', 'Calendar')
@section('content')
<style>
    .datepicker,
    .datepicker-inline {
        display: none !important;
    }
</style>
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Dashbaord</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">On Boarding</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Interview Schedule</a>
                        </li>
                        <li class="breadcrumb-item active">Calendar
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="calendar" style="margin: 0px 35px 0px 35px;"></div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>

<script>
    $(document).ready(function () {
        var SITEURL = "{{ url('/') }}";

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        setInterval(function(){ // set function to be executed after delay
            var calendar = $("#calendar").fullCalendar({
                editable: true,
                events: "app_schedule", 
                eventLimit: true, // allow "more" link when too many events
                eventSources: [
                    {
                        url: SITEURL + "{{url('app_schedule')}}",
                        type: 'get',
                        allDayDefault:false,
                        color: '#65a9d7',    // an option!
                        textColor: 'black'  // an option!

                    },                   
                ],
                displayEventTime: false,
                editable: true,
                eventRender: function (event, element, view) {
                    if (event.allDay === "true") {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                eventClick: function(date, calEvent, jsEvent, view) {
                    console.log(date);
                    var eventDate = moment(date.start).format("YYYY-MM-DD");
                    location.href = `{{url('call_for_appointment')}}`+'/'+eventDate;	
                },
            });
        }, 1000); //set the delay
    });
</script>
@endsection
