@extends('Admin.layouts.master')
@section('title', 'Applicant List')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Applicant List</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Applicant</a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header row"></div>
        <div class="content-body">
               <!-- Call For Interview Modal -->
               <div class="modal fade" id="appointmentModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title text-center">Call for Interview</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form id="add_form">
                            @csrf
                        <input type="hidden" id="cfa" name="id" value="">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="date">Select Date:</label>
                                <input type="date" class="form-control" id="date" name="adate" style="cursor:text">
                            </div>
                            <div class="form-group">
                                <label for="time">Select Time:</label>
                                <input type="time" class="form-control" id="time" name="atime">
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align:center !important">
                            <input type="submit" class="btn btn-primary form_save" id="appoint-btn" value="Make an Appointment">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <section id="basic-dataTable">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <div class="modal fade text-start" id="employee_image_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title title_new" id="myModalLabel17"></h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="image_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <input type="hidden" id="employee" class="form-control" name="id"  value=/>
                                                <div class="col-12">
                                                    <div class="mb-1 text-center">
                                                        <p>Click button to get applcation <span class="text-danger">PDF</span></p>
                                                        <button type="button" class="btn btn-danger pdf" id="pdf">Download Pdf</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>App.No</th>
                                        <th>Name</th>
                                        <th>Addr.</th>
                                        <th>Phone</th>
                                        <th>User Name</th>
                                        <th>Department</th>
                                        <th>Submitted</th>
                                        <th>Expected Date of Joining</th>
                                        <th>Post</th>
                                        <th>Father</th>
                                        <th>CNIC</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: true,
                ajax: "{{ url('cv_bank_list') }}",
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'id',
                        name: 'id',
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'permanent_addr',
                        name: 'permanent_addr',
                    },
                    {
                        data: 'phone_1st',
                        name: 'phone_1st',
                    },
                    {
                        render: function(data, type, full, meta, row) {
                            if(full.username){
                                return full.username;
                            }
                            else{
                                return 'Online';
                            }
                        }
                    },
                    {
                        render: function(data, type, full, meta, row) {
                            if(full.title){
                                return full.title;
                            }
                            else{
                                return 'Online';
                            }
                        }
                    },
                    {
                        data: 'upload_time',
                        name: 'upload_time',
                    },
                    {
                        data: 'doj',
                        name: 'doj',
                    },
                    {
                        data: 'post',
                        name: 'post',
                    },
                    {
                        data: 'f_name',
                        name: 'f_name',
                    },
                    {
                        data: 'cnic',
                        name: 'cnic',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta, row) {
                            var url = '{{ route('cv_bank.edit', ':id') }}';
                            url = url.replace(':id', full.id);
                            return (
                                
                                @can('CV Bank Edit Button')
                                '<a href="' + url + '"  title="Edit Applicant">' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) 
                                +
                                '</a>'
                                +
                                @endcan
                                @can('CV Bank PDF Button')
                                '<a href="javascript:;" class="pdf" id="pdf" onclick="pdf_download(' + full.id +
                                ')">' +
                                feather.icons['download'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                                +
                                @endcan
                                @can('CV Bank Phone Button')
                                '<br><a href="javascript:;" class="call" id="pdf" onclick="call_for_interview(' + full.id +
                                ')">' +
                                feather.icons['phone'].toSvg({
                                    class: 'font-medium-4 text-success'
                                }) +
                                '</a>'
                                +
                                @endcan
                                @can('CV Bank Delete Button')
                                '<a href="javascript:;"  onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                                +
                                @endcan
                                ''
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('CV Bank Add New Button')
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50  font-small-4'
                        }) + 'Add New',
                        className: 'btn btn-primary',
                        action: function(e, dt, node, config) {
                            window.location.href = '{{ route('cv_bank.index') }}';
                        }
                    }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });            
        });
        $("#add_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('call_for_appointment') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        console.log(response);
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $("#add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Email Sent Successfully Successfully!'
                            })
                        }

                    }
                });
            });
        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "cv_bank/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Application has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        function pdf_download(id) {
            $(document).on('click', '.pdf', function(){
            blockUI();
            var export_type = $(this).attr('id');
            $.ajax({
                url: "pdf_download/" + id,
                type: "GET",
                data: {
                    export_type:export_type,
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                        location.reload();
                        location.reload();
                       
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        }
        function call_for_interview(id) {
            $(document).on('click', '.call', function(){
            $('#appointmentModal').modal('show');
            var export_type = $(this).attr('id');
            $('#cfa').attr('value',id);
        });
        }
    </script>
@endsection
