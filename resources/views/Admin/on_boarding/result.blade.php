@extends('admin.layouts.master')
@section('title', 'Interview Results')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Results</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">On Boarding</a>
                            </li>
                            <li class="breadcrumb-item active">Interview Result
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <div class="box">
            <div class="box-body">
                <table id="datatable1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Phone Number</th>
                            <th>Qualification</th>
                            <th>Area</th>
                            <th>Applied For</th>
                            <th>Interview Date</th>
                            <th>Status</th>
                            {{-- <th>Remarks</th> --}}
                            {{-- <th>Action</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($applications as $key=> $application)
                        <tr>
                            <td>{{$application->name}}</td>
                            <td>{{$application->phone_1st}}</td>
                            @if (!empty($application->higher))
                            <td>{{$application->higher}}</td>
                            @elseif(!empty($application->graduation))
                            <td>{{$application->graduation}}</td> 
                            @elseif(!empty($application->inter))
                            <td>{{$application->inter}}</td>
                            @elseif(!empty($application->matric))
                            <td>{{$application->matric}}</td>
                            @else
                            <td>Under Matric</td>
                            @endif
                            <td>{{$application->permanent_addr}}</td>
                            <td>{{$application->post}}</td>
                            <td>{{date('d-m-Y', strtotime($application->app_date))}}</td>
                            @php 
                                $color='';
                                $remarks='';
                                if($application->app_remarks == 1) {$color= 'green'; $remarks= 'Selected';}
                                // elseif($application->app_remarks == 2) {$color= 'red'; $remarks= 'Rejected';} 
                                // elseif($application->app_remarks == 3) {$color= 'blue'; $remarks= 'Waiting';} 
                                // elseif($application->app_remarks == 4) {$color= '#424242'; $remarks= 'Did Not Appear';}
                            @endphp
                            <td style="color:white;background-color:{{$color}}">
                                {{$remarks}}
                            </td>
                            {{-- <td>
                                {{$application->description}}
                            </td> --}}
                            {{-- <td>
                                <a href="{{route('Admin.careersapplication.show',$application->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i> </a>
                                <a href="{{route('Admin.careersapplication.download',$application->id)}}" class="btn btn-primary"><i class="fa fa-download"></i></a>
                            </td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@endsection
    @section('scripts')
	<script>
		$(function () {
			$('#datatable1').DataTable({
                // "ordering": false,
            });
            $('.date').datepicker({
                autoclose: true,
                format: "dd-mm-yyyy",  
            });
	    });
    </script>
@endsection