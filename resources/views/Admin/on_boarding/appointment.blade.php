@extends('Admin.layouts.master')
@section('title', 'Appointment')
@section('content')
<style>
    .datepicker,
    .datepicker-inline {
        display: none !important;
    }
</style>
<div class="content-header row mt-4">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Appointment</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">On Boarding</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Interview Schedule</a>
                        </li>
                        <li class="breadcrumb-item active">Called Applicant List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-header row"></div>
<div class="content-body">
    <div class="box">
        <div class="box-body">
            @can('Quick Addition of Applicant')
                <div class="text-end">
                    <a class="btn btn-primary quick text-right" onclick="quick_add()">Quick Add +</a>
                </div>
            @endcan
        <!--Quick Add Modal -->
        <div class="modal fade text-start" id="QuickAddModal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Quick Applicant</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_quick_form"  method="post">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Name</label>
                                        <input type="text" id="name" class="form-control remove_brd" placeholder="Enter Name" name="name"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="nic_num">CNIC</label>
                                        <input type="text" id="nic_num" class="form-control remove_brd" placeholder="Enter CNIC No." name="nic_num"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="father_name">Father Name</label>
                                        <input type="text" id="father_name" class="form-control remove_brd" placeholder="Father Name" name="f_name"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="phone_number">Contact Number</label>
                                        <input type="text" id="phone_number" class="form-control remove_brd" placeholder="Phone Number:" name="phone2"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                    <input type="hidden" id="cfa" name="id" value="">
                                        <label class="form-label" for="remarks">Applied For the Post of</label>
                                        <input type="text" id="post_name" class="form-control remove_brd" placeholder="Enter Post Name" name="post_name"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="permanent_addr">Address</label>
                                        <input type="text" id="permanent_addr" class="form-control remove_brd" placeholder="Enter Permanet Address" name="address2">
                                    </div>
                                </div>
                                @if (count(departments()) > 1)
                                    <div class="col-12">
                                        <label class="form-label" for="source">Select Source:</label>
                                        <select name="source" id="source" class="select2 form-select" data-placeholder="Select Source" required>
                                            <option value=""></option>
                                            @foreach (departments() as $department)
                                                <option value="{{$department->id}}">{{$department->title}}</option>
                                            @endforeach
                                        </select>
                                    </div> 
                                    @else
                                    <div class="col-12">
                                        <label class="form-label" for="source">Select Source:</label>
                                        <select name="source" id="source" class="select2 form-select" data-placeholder="Select Source" required>
                                            <option value=""></option>
                                            @foreach (departments() as $department)
                                                <option value="{{$department->id}}" selected>{{$department->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                <p>Add Interview Date and Time:</p>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label for="date">Select Date:</label>
                                        <input type="date" class="form-control" id="date" name="adate" style="cursor:text">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label for="time">Select Time:</label>
                                        <input type="time" class="form-control" id="time" name="atime">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Quick Add Modal -->
        <!--Status Add Modal -->
        <div class="modal fade text-start" id="appointmentModal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Status</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form" action="{{route('Admin.careersapplication.remarks')}}" method="post">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="remarks">Communication Level</label>
                                        <select name="communication" id="communication" class="form-select select2" data-placeholder="Select Communication Level">
                                            <option value=""></option>
                                            <option value="Bad">Bad</option>
                                            <option value="Good">Good</option>
                                            <option value="Very Good">Very Good</option>
                                            <option value="Extra Ordinary">Extra Ordinary</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="remarks">Cleanliness Level</label>
                                        <select name="cleanliness" id="cleanliness" class="form-select select2" data-placeholder="Cleanliness Level">
                                            <option value=""></option>
                                            <option value="Bad">Bad</option>
                                            <option value="Good">Good</option>
                                            <option value="Very Good">Very Good</option>
                                            <option value="Extra Ordinary">Extra Ordinary</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                    <input type="hidden" id="cfa1" name="id">
                                        <label class="form-label" for="remarks">Status</label>
                                        <select name="remarks" id="remarks" class="form-select select2" data-placeholder="Select Status">
                                            <option value=""></option>
                                            <option value="selected">Selected</option>
                                            <option value="rejected">Rejected</option>
                                            <option value="waiting">Waiting</option>
                                            <option value="didnotappear">Did Not Appear</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_gm_manager">Remarks</label>
                                        <input type="text" name="description" class="form-control" placeholder="Remarks/Description">
                                    </div>
                                </div>
                                <p>Schedule Another Interview:</p>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label for="date">Select Date:</label>
                                        <input type="date" class="form-control" id="date" name="adate" style="cursor:text">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label for="time">Select Time:</label>
                                        <input type="time" class="form-control" id="time" name="atime">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Add Modal -->

         <!--View Modal -->
         <div class="modal fade text-start" id="viewModal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Number of Interviews</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <table id="app_t"  class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <td>Total Interviews</td>
                                <th>Full Name</th>
                                <th>Phone Number</th>
                                <th>Qualification</th>
                                <th>Area</th>
                                <th>Applied For</th>
                                <th>Interview Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--End View Modal -->

        <table id="datatable1" class="table table-bordered table-striped table-responsive">
            <thead>
                <tr>
                    <td>Sr.</td>
                    <th>Full Name</th>
                    <th>Phone Number</th>
                    <th>Qualification</th>
                    <th>Area</th>
                    <th>Applied For</th>
                    <th>Interview Date</th>
                    <th>Status</th>
                    <th>View Interviews</th>

                </tr>
            </thead>
            <tbody>
                {{-- @php
                    dd( $appointments);
                @endphp --}}
                @if(!empty($appointments))
                @endif
                @foreach($appointments as $key=> $application)
                <input type="hidden" id="app_id" name="app_id" value="{{$application->id}}">
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$application->name}}</td>
                    <td>{{$application->phone_1st}}</td>
                    <td>{{$application->higher}}</td>
                    <td>{{$application->permanent_addr}}</td>
                    <td>{{$application->post}}</td>
                    <td>{{date('d-m-Y', strtotime($application->app_date))}} {{$application->app_time}}</td>
                    @if($application->app_remarks==null)
                    <td>
                        <span>
                            {{-- <a class="btn btn-success status mb-1 " onclick="view({{$application->id}})">View<i class="fa fa-eye"></i></a> --}}
                            @can('Add Appointment Button')
                                <a class="btn btn-primary addstatus" onclick="status({{$application->id}})">Add +</a>
                            @endcan
                        </span>
                    </td>
                    @else
                    <td>
                        <p class="text-center">Selected</p>
                    </td>
                    @endif
                    <td>
                        <a class="btn btn-success status" onclick="view({{$application->id}})">View<i class="fa fa-eye"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script>
// Add Data
$('#add_quick_form').on('submit' , function(e){
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{route('Admin.careersapplication.quick')}}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            console.log('s');
                            $("#add_modal").modal("hide");
                            $(".select2").val('').trigger('change');
                            Toast.fire({
                                icon: 'success',
                                title: 'Quick Applicant has been Added Successfully!'
                            });
                            setTimeout(function() { 
                                location.reload();
                            }, 2000);
                        }
                    }
                });
            });
    $(document).ready(function () {
        $(function () {
			$('#datatable1').DataTable({
                "ordering": false,
            });
	    });
    });
function status(id) {
        $(document).on('click', '.addstatus', function(){
        $('#appointmentModal').modal('show');
        // var export_type = $(this).attr('id');
        $('#cfa1').val(id);
        // console.log(id);
    });
    }

function quick_add() {
    $(document).on('click', '.quick', function(){
    $('#QuickAddModal').modal('show');
    // var export_type = $(this).attr('id');
    // console.log(id);
});
}
    
function view(id) {
        $(document).on('click', '.status', function(){
        $('#viewModal').modal('show');
        $('#viewModal').on('hidden.bs.modal', function () {
        location.reload();
        })
        rowid = id;
        $.ajax({
                url: "{{url('appointment')}}" + "/" + id,
                type: "GET",
                success: function(response){

                    $.each(response, function(index, value ) {
                        // console.log($tdNumber);$(v).text(i + 1);
                        var tdNumber = (index + 1);
                        var elements = "<tr><td>" + tdNumber + "</td><td>" + value.name + "</td><td>" + value.phone_1st + "</td><td>" + value.cleanliness + "</td><td>" + value.communication + "</td><td>" + value.description + "</td><td>" + value.start +' '+ value.app_time + "</td></tr>";
                        $("#app_t tbody").append(elements);
                    });
                }
            });
        // console.log(id);
    });
    }
</script>
@endsection
