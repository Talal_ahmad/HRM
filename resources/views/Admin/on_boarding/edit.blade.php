@extends('Admin.layouts.master')
@section('title', 'Edit Application Form')

@section('style')
    <style>
        td,input{
            text-transform: uppercase;
        }
        .remove_brd{
            border-top: none;
            border-left: none;
            border-right: none;
        }
        label {
            /* Other styling... */
            clear: both;
            float:left;
            /* margin-right:15px; */
            /* padding: 10px; */
        }
        .setbox td {
        position: relative;
        }

        .setbox td input {
        position: absolute;
        display: block;
        top:0;
        left:0;
        margin: 0;
        height: 100%;
        width: 100%;
        border: none;
        padding: 10px;
        box-sizing: border-box;
        }
        .bro-none{
            border: none;
        }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Edit Application Form</h2>
                        {{-- <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashbaord</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Employees</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">List</a>
                                </li>
                                <li class="breadcrumb-item active">Create
                                </li>
                            </ol>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="horizontal-wizard">
                <div class="bs-stepper employee-horizontal-wizard">
                    <div class="bs-stepper-header" role="tablist">
                        <div class="step" data-target="#personal-modern" role="tab" id="personal-modern-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="user" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Applicant Personal Info</span>
                                    <span class="bs-stepper-subtitle">Edit Applicant Personal Info</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#address-step-modern" role="tab" id="address-step-modern-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="file-text" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Others</span>
                                    <span class="bs-stepper-subtitle">Edit Other Informations</span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="bs-stepper-content">
                        <div id="personal-modern" class="content" role="tabpanel" aria-labelledby="personal-modern-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">Personal Info</h5>
                                <small class="text-muted">Edit Applicant Personal Info.</small>
                                <span>Application Form for Employment/Internship</span>
                            </div>
                            <form class="form col-12">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 15%;">For the Post of:</th>
                                                    <td>
                                                        <input type="text" id="post_name" class="form-control remove_brd" placeholder="Enter Post Name" name="post_name" value="{{$boarding->post}}"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-9 col-12 table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Name:</th>
                                                    <td>
                                                        <input type="text" id="name" class="form-control remove_brd w-auto" placeholder="Enter Name" name="name" value="{{$boarding->name}}"/>
                                                    </td>
                                                    <th>Father Name:</th>
                                                    <td>
                                                        <input type="text" id="father_name" class="form-control remove_brd w-auto" placeholder="Father Name" name="f_name" value="{{$boarding->f_name}}"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Email:</th>
                                                    <td>
                                                        <input type="text" id="email" class="form-control remove_brd w-auto" placeholder="Email" name="email" value="{{$boarding->email}}"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Date of Birth:</th>
                                                    <td>
                                                        <input type="date" id="birth_date" class="form-control remove_brd w-auto " placeholder="Date of Birth" name="birth_date" value="{{$boarding->dob}}"/>
                                                    </td>
                                                    <th>Blood Group:</th>
                                                    <td>
                                                        <select name="blood" id="blood" class="form-select" data-placeholder="Select Blood Group">
                                                            <option value="a+" {{$boarding->blood == "a+"  ? 'selected' : 'a+'}}>A+</option>
                                                            <option value="b+" {{$boarding->blood == "b+"  ? 'selected' : 'b+'}}>B+</option>
                                                            <option value="o+" {{$boarding->blood == "o+"  ? 'selected' : 'o+'}}>O+</option>
                                                            <option value="a-" {{$boarding->blood == "a-"  ? 'selected' : 'a-'}}>A-</option>
                                                            <option value="b-" {{$boarding->blood == "b-"  ? 'selected' : 'b-'}}>B-</option>
                                                            <option value="o-" {{$boarding->blood == "o-"  ? 'selected' : 'o-'}}>O-</option>
                                                            <option value="ab+" {{$boarding->blood == "ab+"  ? 'selected' : 'ab+'}}>AB+</option>
                                                            <option value="ab-" {{$boarding->blood == "ab-"  ? 'selected' : 'ab-'}}>AB-</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Enter CNIC No.:</th>
                                                    <td>
                                                        <input type="text" id="nic_num" class="form-control  w-auto remove_brd" placeholder="Enter CNIC No." name="nic_num" value="{{$boarding->cnic}}"/>
                                                    </td>
                                                    <th>Religion Name:</th>
                                                    <td>
                                                        <select name="religion" id="religion" class="form-select" data-placeholder="Select Religion" value="{{$boarding->religion}}">
                                                            <option value="Islam" {{$boarding->religion == "Islam"  ? 'selected' : 'Islam'}}>Islam</option>
                                                            <option value="Christian" {{$boarding->religion == "Christian"  ? 'selected' : 'Christian'}}>Christian</option>
                                                            <option value="Christian" {{$boarding->religion == "Hindu"  ? 'selected' : 'Hindu'}}>Hindu</option>
                                                            <option value="Christian" {{$boarding->religion == "Sikhism"  ? 'selected' : 'Sikhism'}}>Sikhism</option>
                                                            <option value="Other" {{$boarding->religion == "Other"  ? 'selected' : 'Other'}}>Other</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Marital Status:</th>
                                                    <td>
                                                        Married:
                                                        <input id="married" name="marital_status" type="checkbox" value="married" {{ $boarding->marital_status == "married" ? 'checked' : '' }}>
                                                        Single:
                                                        <input id="single" name="marital_status" type="checkbox" value="single" {{ $boarding->marital_status == "single" ? 'checked' : '' }}>
                                                    </td>
                                                    <th>Number of Children:</th>
                                                    <td>
                                                        <input type="text" id="child" class="form-control w-auto  remove_brd" placeholder="Enter Number of Children" name="child" value="{{$boarding->total_child}}"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <div class="col-md-3 col-12 text-center">
                                    <a href="#">
                                        <img src="{{asset('applicant_images'.'/'.$boarding->image)}}" id="account-upload-img" class="me-50 border" alt="profile image" style="height: 200px; width: 200px;" />
                                    </a>
                                </div>
                               
                                <div class="col-md-12 col-lg-12 table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    Name & Address of Next of Kin:
                                                </th>
                                                <td>
                                                    <input type="text" id="name_addr" class="form-control remove_brd" placeholder="Name & Address (Next of Kin)" name="address1" size="70" value="{{$boarding->kin_address}}"/>
                                                </td>
                                                <th>Phone:</th>
                                                <td>
                                                    <input type="text" id="phone_number" class="form-control remove_brd" placeholder="Enter Phone:" name="phone1" value="{{$boarding->kin_phone}}"/>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    Residence Status:
                                                </th>
                                                <th>
                                                    Own: <input id="residence" name="residence_status" type="checkbox" value="Own" {{ $boarding->residence_status == "Own" ? 'checked' : '' }}>
                                                </th>
                                                <th>
                                                    Rented: <input id="rented" name="residence_status" type="checkbox" value="Rented" {{ $boarding->residence_status == "Rented" ? 'checked' : '' }}>
                                                </th>
                                                <th>|</th>
                                                <th>
                                                    <label class="col-form-label" >Own Conveyance:</label>
                                                </th>
                                                @php
                                                    $own = explode(',',$boarding->own_conveyance);
                                                @endphp
                                                <th>
                                                    None: <input type="checkbox" name="owns[]" value="None" {{ in_array("None",$own) ? 'checked' : '' }}>
                                                </th>
                                                <th>
                                                    Car: <input type="checkbox" name="owns[]" value="Car" {{ in_array("Car",$own) ? 'checked' : '' }}>
                                                </th>
                                                <th>
                                                    Motor Cycle: <input type="checkbox" name="owns[]" value="Motorcycle" {{ in_array("Motorcycle",$own)  ? 'checked' : '' }}>
                                                </th>
                                                <th>
                                                    Bicycle: <input type="checkbox" name="owns[]" value="Bicycle" {{ in_array("Bicycle",$own) ? 'checked' : '' }}>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12 col-12 table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    Permanet Address: <input type="text" id="permanent_addr" class="form-control remove_brd" placeholder="Enter Permanet Address" name="address2"  size="90" value="{{$boarding->permanent_addr}}"/>
                                                </th>
                                                <th>
                                                    Phone: <input type="text" id="phone_number" class="form-control remove_brd" placeholder="Phone Number:" name="phone2" style="width: auto;" size="20" value="{{$boarding->phone_1st}}"/>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Present Address: <input type="text" id="permanent_addr" class="form-control remove_brd" placeholder="Enter Permanet Address" name="address3" size="90" value="{{$boarding->present_addr}}"/>
                                                </th>
                                                <th>
                                                    Phone: <input type="text" id="phone_number" class="form-control remove_brd" placeholder="Phone Number:" name="phone3" size="20" value="{{$boarding->phone_2nd}}"/>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive col-md-12">
                                    <table class="table table-bordered mt-2 setbox">
                                        <thead>
                                            <h1>Acacdemic Qualification</h1>
                                            <th style="width: 255px;">Degree</th>
                                            <th>Name of School/ College/ University attended</th>
                                            <th>Detail Of education</th>
                                            <th>Year</th>
                                            <th>Division</th>
                                            <th>Grade</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="text" value="{{$boarding->higher}}" name="higher_education" id="higher_education" readonly></td>
                                                <td><input class="remove_brd" type="text" name="higher_college_school" value="{{$boarding->higher_college_school}}"></td>
                                                <td><input class="remove_brd" type="text" name="higher_detail" value="{{$boarding->higher_detail}}"></td>
                                                <td><input type="month" id="date" class="form-control date"  name="higher_year" value="{{$boarding->higher_year}}"></td>
                                                <td><input class="remove_brd" type="text" name="higher_div" value="{{$boarding->higher_div}}"></td>
                                                <td><input class="remove_brd" type="text" name="higher_grade" value="{{$boarding->higher_grade}}"></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" value="{{$boarding->graduation}}" name="graduation" id="graduation" readonly></td>
                                                <td><input class="remove_brd" type="text" name="grad_college_school" value="{{$boarding->grad_college_school}}"></td>
                                                <td><input class="remove_brd" type="text" name="grad_detail" value="{{$boarding->grad_detail}}"></td>
                                                <td><input type="month" id="date" class="form-control date"  name="grad_year" value="{{$boarding->grad_year}}"></td>
                                                <td><input class="remove_brd" type="text" name="grad_div" value="{{$boarding->grad_div}}"></td>
                                                <td><input class="remove_brd" type="text" name="grad_grade" value="{{$boarding->grad_grade}}"></td>
                                            </tr>

                                            <td><input type="text" value="{{$boarding->inter}}" name="inter" id="higher_education" readonly></td>
                                                <td><input class="remove_brd" type="text" name="inter_college_school" value="{{$boarding->inter_college_school}}"></td>
                                                <td><input class="remove_brd" type="text" name="inter_detail" value="{{$boarding->inter_detail}}"></td>
                                                <td><input type="month" id="date" class="form-control date"  name="higher_year" value="{{$boarding->inter_year}}"></td>
                                                <td><input class="remove_brd" type="text" name="inter_div" value="{{$boarding->inter_div}}"></td>
                                                <td><input class="remove_brd" type="text" name="inter_grade" value="{{$boarding->inter_grade}}"></td>

                                            <tr>
                                                <td><input type="text" value="{{$boarding->inter}}" name="matric" id="higher_education" readonly></td>
                                                <td><input class="remove_brd" type="text" name="matric_college_school" value="{{$boarding->matric_college_school}}"></td>
                                                <td><input class="remove_brd" type="text" name="matric_detail" value="{{$boarding->matric_detail}}"></td>
                                                <td><input type="month" id="date" class="form-control date"  name="matric_year" value="{{$boarding->matric_year}}"></td>
                                                <td><input class="remove_brd" type="text" name="matric_div" value="{{$boarding->matric_div}}"></td>
                                                <td><input class="remove_brd" type="text" name="matric_grade" value="{{$boarding->matric_grade}}"></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" value="Under Matric" name="under_matric" id="under_matric" readonly></td>
                                                <td><input class="remove_brd" type="text"></td>
                                                <td><input class="remove_brd" type="text"></td>
                                                <td><input class="remove_brd" type="text"></td>
                                                <td><input class="remove_brd" type="text"></td>
                                                <td><input class="remove_brd" type="text"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive col-md-12">
                                    <table class="table table-bordered mt-2 setbox">
                                        <thead>
                                            <h1>Professional Experience</h1>
                                            <th style="width: 255px;">Sr. No</th>
                                            <th>Name of Organization</th>
                                            <th>Post</th>
                                            <th>Duration</th>
                                            <th>Salary</th>
                                            <th>Reason For Leaving</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input class="text-center" type="text" value="1" readonly></td>
                                                <td><input class="remove_brd" type="text" name="organization_name_1" value="{{$boarding->organization_name_1}}"></td>
                                                <td><input class="remove_brd" type="text" name="post_name_1" value="{{$boarding->post_name_1}}"></td>
                                                <td><input class="remove_brd" type="text" name="duration_1" value="{{$boarding->duration_1}}"></td>
                                                <td><input class="remove_brd" type="text" name="salary_1" value="{{$boarding->salary_1}}"></td>
                                                <td><input class="remove_brd" type="text" name="leaving_reason_1" value="{{$boarding->leaving_reason_1}}"></td>
                                            </tr>
                                            <tr>
                                                <td><input class="text-center" type="text" value="2" readonly></td>
                                                <td><input class="remove_brd" type="text" name="organization_name_2" value="{{$boarding->organization_name_2}}"></td>
                                                <td><input class="remove_brd" type="text" name="post_name_2" value="{{$boarding->post_name_2}}"></td>
                                                <td><input class="remove_brd" type="text" name="duration_2" value="{{$boarding->duration_2}}"></td>
                                                <td><input class="remove_brd" type="text" name="salary_2" value="{{$boarding->salary_2}}"></td>
                                                <td><input class="remove_brd" type="text" name="leaving_reason_2" value="{{$boarding->leaving_reason_2}}"></td>
                                            </tr>
                                            <tr>
                                                <td><input class="text-center" type="text" value="3" readonly></td>
                                                <td><input class="remove_brd" type="text" name="organization_name_3" value="{{$boarding->organization_name_3}}"></td>
                                                <td><input class="remove_brd" type="text" name="post_name_3" value="{{$boarding->post_name_3}}"></td>
                                                <td><input class="remove_brd" type="text" name="duration_3" value="{{$boarding->duration_3}}"></td>
                                                <td><input class="remove_brd" type="text" name="salary_3" value="{{$boarding->salary_3}}"></td>
                                                <td><input class="remove_brd" type="text" name="leaving_reason_3" value="{{$boarding->leaving_reason_3}}"></td>
                                            </tr>
                                            <tr>
                                                <td><input class="text-center" type="text" value="4" readonly></td>
                                                <td><input class="remove_brd" type="text" name="organization_name_4" value="{{$boarding->organization_name_4}}"></td>
                                                <td><input class="remove_brd" type="text" name="post_name_4" value="{{$boarding->post_name_4}}"></td>
                                                <td><input class="remove_brd" type="text" name="duration_4" value="{{$boarding->duration_4}}"></td>
                                                <td><input class="remove_brd" type="text" name="salary_4" value="{{$boarding->salary_4}}"></td>
                                                <td><input class="remove_brd" type="text" name="leaving_reason_4" value="{{$boarding->leaving_reason_4}}"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-outline-secondary btn-prev" disabled>
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">Next</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        <div id="address-step-modern" class="content" role="tabpanel" aria-labelledby="address-step-modern-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">Other Informations</h5>
                                <small>Edit Applicant Other Informations</small>
                            </div>
                            <form>
                                @csrf
                                <div class="row">
                                    <div class="table-responsive mb-1 col-md-12">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 215px;">Acceptable Salary RS:</th>
                                                    <td>
                                                        <input type="text" id="accept_salary" class="form-control remove_brd" placeholder="Enter Ammount/Salary" name="accept_salary" value="{{$boarding->accept_salary}}" style="width: 216px;"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Language known</th>
                                                    @php
                                                        $lang = explode(',',$boarding->lang_known)
                                                    @endphp
                                                    <td>
                                                        English: <input id="English" name="lang_known[]" type="checkbox" value="English" {{in_array("English",$lang) ? 'checked' : ''}}>
                                                    </td>
                                                    <td>
                                                        Urdu: <input id="Urdu" name="lang_known[]" type="checkbox" value="Urdu" {{in_array("Urdu",$lang) ? 'checked' : ''}}>
                                                    </td>
                                                    <td>
                                                        Punjabi: <input id="Punjabi" name="lang_known[]" type="checkbox" value="Punjabi" {{in_array("Punjabi",$lang) ? 'checked' : ''}}>
                                                    </td>
                                                    <td>
                                                        Balochi: <input id="Balochi" name="lang_known[]" type="checkbox" value="Balochi" {{in_array("Balochi",$lang) ? 'checked' : ''}}>
                                                    </td>
                                                    <td>
                                                        Sindhi: <input id="Sindhi" name="lang_known[]" type="checkbox" value="Sindhi" {{in_array("Sindhi",$lang) ? 'checked' : ''}}>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive col-md-12">
                                        <table class="table table-bordered mt-2 setbox">
                                            <thead>
                                                <h1>Particulars of Parents, Brothers and sisters</h1>
                                                <tr>
                                                    @php
                                                        $f_status = explode(',',$boarding->father_health_status)
                                                    @endphp
                                                    <td class="bro-none">
                                                        Father:
                                                    </td>
                                                    <td class="bro-none">
                                                        Alive
                                                        <input style="width: fit-content;" type="checkbox" value="Alive" {{in_array("Alive",$f_status)  ? 'checked' : ''}} name="father_health_status[]" id="">
                                                    </td>
                                                    <td class="bro-none">
                                                        Diseased
                                                        <input style="width: fit-content; border-left: none;" value="Diseased" {{in_array("Diseased",$f_status)  ? 'checked' : ''}} type="checkbox" name="father_health_status[]" id="">
                                                    </td>
                                                    <td class="bro-none text-center">|</td>
                                                    @php
                                                        $m_status = explode(',',$boarding->mother_health_status)
                                                    @endphp
                                                    <td class="bro-none">
                                                        Mother:
                                                    </td>
                                                    <td class="bro-none">
                                                        Alive
                                                        <input style="width: fit-content;" type="checkbox" value="Alive" {{in_array("Alive",$f_status)  ? 'checked' : ''}} name="mother_health_status[]" id="">
                                                    </td>
                                                    <td class="bro-none">
                                                        Diseased
                                                        <input style="width: fit-content; border-left: none;" value="Diseased" {{in_array("Diseased",$f_status)  ? 'checked' : ''}} type="checkbox" name="mother_health_status[]" id="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 161px;">Relation</th>
                                                    <th colspan="2">Name</th>
                                                    <th>Age</th>
                                                    <th>Qualification</th>
                                                    <th colspan="2">Occupation</th>
                                                </tr>   
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" name="father" value="{{$boarding->father}}" readonly></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="father_name" value="{{$boarding->father_name}}"></td>
                                                    <td><input class="remove_brd" type="text" name="f_age"  value="{{$boarding->f_age}}"></td>
                                                    <td><input class="remove_brd" type="text" name="f_qualification"  value="{{$boarding->f_qualification}}"></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="f_occupation"  value="{{$boarding->f_occupation}}"></td>
                                                </tr>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" name="mother" value="{{$boarding->mother}}" readonly></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="mother_name" value="{{$boarding->mother_name}}"></td>
                                                    <td><input class="remove_brd" type="text" name="m_age"  value="{{$boarding->m_age}}"></td>
                                                    <td><input class="remove_brd" type="text" name="m_qualification"  value="{{$boarding->m_qualification}}"></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="m_occupation"  value="{{$boarding->m_occupation}}"></td>
                                                </tr>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" name="brother_1" value="{{$boarding->brother_1}}" readonly></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="brother_1_name" value="{{$boarding->brother_1_name}}"></td>
                                                    <td><input class="remove_brd" type="text" name="b_1_age"  value="{{$boarding->b_1_age}}"></td>
                                                    <td><input class="remove_brd" type="text" name="b_1_qualification"  value="{{$boarding->b_1_qualification}}"></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="b_1_occupation"  value="{{$boarding->b_1_occupation}}"></td>
                                                </tr>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" name="brother_2" value="{{$boarding->brother_2}}" readonly></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="brother_2_name" value="{{$boarding->brother_2_name}}"></td>
                                                    <td><input class="remove_brd" type="text" name="b_2_age"  value="{{$boarding->b_2_age}}"></td>
                                                    <td><input class="remove_brd" type="text" name="b_2_qualification"  value="{{$boarding->b_2_qualification}}"></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="b_2_occupation"  value="{{$boarding->b_2_occupation}}"></td>
                                                </tr>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" name="sister_1" value="{{$boarding->sister_1}}" readonly></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="sister_1_name" value="{{$boarding->sister_1_name}}"></td>
                                                    <td><input class="remove_brd" type="text" name="s_1_age"  value="{{$boarding->s_1_age}}"></td>
                                                    <td><input class="remove_brd" type="text" name="s_1_qualification"  value="{{$boarding->s_1_qualification}}"></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="s_1_occupation"  value="{{$boarding->s_1_occupation}}"></td>
                                                </tr>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" name="sister_2" value="{{$boarding->sister_2}}" readonly></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="sister_2_name" value="{{$boarding->sister_2_name}}"></td>
                                                    <td><input class="remove_brd" type="text" name="s_2_age"  value="{{$boarding->s_2_age}}"></td>
                                                    <td><input class="remove_brd" type="text" name="s_2_qualification"  value="{{$boarding->s_2_qualification}}"></td>
                                                    <td colspan="2"><input class="remove_brd" type="text" name="s_2_occupation"  value="{{$boarding->s_2_occupation}}"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive col-md-12">
                                        <table class="table table-bordered mt-2 setbox">
                                            <thead>
                                                <h1>Any Relative/Friend working in Clinix Pharmacy:</h1>
                                                <th>Sr. No</th>
                                                <th>Relation</th>
                                                <th>Name</th>
                                                <th>Designation</th>
                                                <th>Department</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" value="1" readonly></td>
                                                    <td><input class="remove_brd" type="text" name="relation_1" value="{{$boarding->relation_1}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_name_1" value="{{$boarding->rel_name_1}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_designation_1" value="{{$boarding->rel_designation_1}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_department_1" value="{{$boarding->rel_department_1}}"></td>
                                                </tr>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" value="2" readonly></td>
                                                    <td><input class="remove_brd" type="text" name="relation_2" value="{{$boarding->relation_2}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_name_2" value="{{$boarding->rel_name_2}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_designation_2" value="{{$boarding->rel_designation_2}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_department_2" value="{{$boarding->rel_department_2}}"></td>
                                                </tr>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" value="3" readonly></td>
                                                    <td><input class="remove_brd" type="text" name="relation_3" value="{{$boarding->relation_3}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_name_3" value="{{$boarding->rel_name_3}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_designation_3" value="{{$boarding->rel_designation_3}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_department_3" value="{{$boarding->rel_department_3}}"></td>
                                                </tr>
                                                <tr>
                                                    <td><input class="remove_brd" type="text" value="4" readonly></td>
                                                    <td><input class="remove_brd" type="text" name="relation_4" value="{{$boarding->relation_4}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_name_4" value="{{$boarding->rel_name_4}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_designation_4" value="{{$boarding->rel_designation_4}}"></td>
                                                    <td><input class="remove_brd" type="text" name="rel_department_4" value="{{$boarding->rel_department_4}}"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-reponsive col-md-12">
                                        <table class="table">
                                            <thead>
                                            </thead>
                                            <tbody>
                                                <h1>Medical History</h1>
                                                <input  style="width:100%;" rows="8" placeholder="Please give detail of any serious accident, illnes or disabilities." name="medical_history" value="{{$boarding->medical_history}}">
                                                <tr>
                                                    <td>
                                                        <p style="text-align: right;">
                                                            میں حلفاً اقرار کر تاہوں کہ جو معلومات میں نے اس درخواست میں فراہم کی ہیں ، وہ میرے علم کے مطابق درست ہیں۔ اگر یہ معلومات کسی بھی وقت غلط ثابت ہوں تو مجھے بغیر نوٹس کے سروس سے فارغ کیا جا سکتا ہے اور میں اس بارے میں کوئی قانونی چارہ جوئی نہیں کروں گا۔ مزید یہ کہ ٹریننگ کے بعد ایک سال تک کام کرنے کا پابند رہوں گا اور یہ کہ سروس چھوڑنے سے پہلے ایک ماہ کا نوٹس دینے کا پابند ہوں گا، بصورت دیگر ایک ماہ کی تنخواہ سے دستبردار ہوں گا۔
                                                        </p>
                                                        <input type="checkbox" class="from-control" name="agreement" value="Agreed" id="" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th style="width:0;">Date:</th>
                                                    <td>
                                                        <input style="width:auto;" type="date" name="update_at" value="<?= date('Y-m-d', time()); ?>" class="form-control" readonly>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button class="form_save btn btn-success btn-submit">Submit</button>
                            </div>
                            <div class="modal fade text-start" id="employee_image_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title title_new" id="myModalLabel17"></h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="image_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <input type="hidden" id="employee" class="form-control" name="id"  value=/>
                                                <div class="col-12">
                                                    <div class="mb-1 text-center">
                                                        <p>Click button to get applcation <span class="text-danger">PDF</span></p>
                                                        <button type="button" class="btn btn-danger pdf" id="pdf">Download Pdf</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            new Cleave($('#nic_num'), {
                delimiter: '-',
                blocks: [5, 7, 1],
                numericOnly : true
            });
            var bsStepper = document.querySelectorAll('.bs-stepper'),
            horizontalWizard = document.querySelector('.employee-horizontal-wizard');
            // Adds crossed class
            if (typeof bsStepper !== undefined && bsStepper !== null) {
                for (var el = 0; el < bsStepper.length; ++el) {
                bsStepper[el].addEventListener('show.bs-stepper', function (event) {
                    var index = event.detail.indexStep;
                    var numberOfSteps = $(event.target).find('.step').length - 1;
                    var line = $(event.target).find('.step');

                    // The first for loop is for increasing the steps,
                    // the second is for turning them off when going back
                    // and the third with the if statement because the last line
                    // can't seem to turn off when I press the first item. ¯\_(ツ)_/¯

                    for (var i = 0; i < index; i++) {
                    line[i].classList.add('crossed');

                    for (var j = index; j < numberOfSteps; j++) {
                        line[j].classList.remove('crossed');
                    }
                    }
                    if (event.detail.to == 0) {
                    for (var k = index; k < numberOfSteps; k++) {
                        line[k].classList.remove('crossed');
                    }
                    line[0].classList.remove('crossed');
                    }
                });
                }
            }

            if (typeof horizontalWizard !== undefined && horizontalWizard !== null) {
                var numberedStepper = new Stepper(horizontalWizard),
                $form = $(horizontalWizard).find('form');
                $form.each(function () {
                var $this = $(this);
                if("{! env('COMPANY') == 'CIDEX' !}")
                {
                    $this.validate({
                        rules: {
                            'name': { required: true },
                            'post_name': { required: true},
                            'f_name': { required: true},
                            'birth_date': { required: true},
                            // 'nic_num' : { required: true},
                            'religion': { required: true},
                            'owns': { required: true},
                            'lang_known': { required: true},
                            'father_health_status': { required: true},
                            'mother_health_status': { required: true},
                            'agreement': { required: true},
                        }
                    });
                }
                else
                {
                    $this.validate({
                        rules: {
                            'name': { required: true },
                            'birth_date': { required: true},
                            'f_name': { required: true},
                            // 'nic_num' : { required: true},
                            'religion': { required: true},
                            'owns': { required: true},
                            'lang_known': { required: true},
                            'father_health_status': { required: true},
                            'mother_health_status': { required: true},
                            'agreement': { required: true},
                        }
                    });
                }
                });

                $(horizontalWizard)
                .find('.btn-next')
                .each(function () {
                    $(this).on('click', function (e) {
                    var isValid = $(this).parent().siblings('form').valid();
                    if (isValid) {
                        numberedStepper.next();
                    } else {
                        e.preventDefault();
                    }
                    });
                });

                $(horizontalWizard)
                .find('.btn-prev')
                .on('click', function () {
                    numberedStepper.previous();
                });

                $(horizontalWizard)
                .find('.btn-submit')
                .on('click', function (e) {
                    ButtonStatus('.form_save',true);
                    blockUI();
                    e.preventDefault();
                    var isValid = $(this).parent().siblings('form').valid();
                    if (isValid) 
                    {
                        var form = $('form').serialize();
                        $.ajax({
                            url: "{{route('cv_bank.update', $boarding->id)}}",
                            type: "put",
                            data:  form,
                            
                            success: function (response) {
                                ButtonStatus('.form_save',false);
                                $.unblockUI();
                                if(response.errors){
                                    $.each( response.errors, function( index, value ){
                                        Toast.fire({
                                            icon: 'error',
                                            title: value
                                        })
                                    });
                                }
                                else if(response.error_message){
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'Please recheck the detail. The required field is Missing!'
                                    })
                                }
                                else{
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Application has been Updated Successfully!'
                                    });
                        var appl_id = $('#applcant_id').val();

                                    setTimeout(function(){
                                        if (1 == 1) {
                                            // $("#employee_image_modal").modal("show");
                                            // window.location.href = "{{ url('cv_bank_pdf') }}" + "/" + appl_id;
                                            $('#employee_image_modal').modal('show');
                                            $('.title_new').text('Your Applcation Number is: ' + response.data.id);
                                            $('#employee_image_modal').on('hidden.bs.modal', function () {
                                            // location.reload();
                                            window.location.href = "{{url('cv_bank_list')}}";

                                            })
                                        }
                                    }, 2500);
                                }
                                
                            }
                        });
                    }
                });
            }
        });
        $(document).on('click', '.pdf', function(){
            blockUI();
            var export_type = $(this).attr('id');
            $.ajax({
                url: "{{url('cv_bank')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
    </script>
@endsection