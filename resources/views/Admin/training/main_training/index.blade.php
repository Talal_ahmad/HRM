@extends('Admin.layouts.master')
@section('title', 'Training')

@section('content')

<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.training')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.training')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('app.training')
                            </li>
                        </ol>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <div class="content-body">
        <section class="basic-tabs-components">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="my_training_sessions-tab" data-bs-toggle="tab" href="#my_training_sessions" aria-controls="my_training_sessions" role="tab" aria-selected="true">@lang('app.My_Training_Sessions')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="all_training_sessions-tab" data-bs-toggle="tab" href="#all_training_sessions" aria-controls="all_training_sessions" role="tab" aria-selected="false" onclick="allTrainSession()">@lang('app.All_Training_Sessions')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="training_sessions_direct_reports-tab" data-bs-toggle="tab" href="#training_sessions_direct_reports" aria-controls="training_sessions_direct_reports" role="tab" aria-selected="false" onclick="trainSessionDirect()">@lang('app.Training_Sessions_Of_Direct_Reports')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="training_sessions_coordinated-tab" data-bs-toggle="tab" href="#training_sessions_coordinated" aria-controls="training_sessions_coordinated" role="tab" aria-selected="false" onclick="trainSessionCoordinate()">@lang('app.Training_Sessions_Coordinated_By_You')</a>
                </li>
            </ul>
            <div class="tab-content">
                {{-- My Training Sessions --}}
                <div class="tab-pane active" id="my_training_sessions" aria-labelledby="my_training_sessions-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="my_training_session_dataTable">
                                        <thead>
                                            <tr>
                                                <th class="not_include"></th>
                                                <th>Training Sessions</th>
                                                <th class="not_include">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                {{-- All Training Sessions --}}
                <div class="tab-pane" id="all_training_sessions" aria-labelledby="all_training_sessions-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="all_training_session_dataTable">
                                        <thead>
                                            <tr>
                                                <th class="not_include"></th>
                                                <th>Sr.No</th>
                                                <th>Name</th>
                                                <th>Course</th>
                                                <th>Sheduled Time</th>
                                                <th>Delivery Method</th>
                                                <th>Delivery Locations</th>
                                                <th class="not_include"></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                {{-- Training Sessions Of Direct Reports --}}
                <div class="tab-pane" id="training_sessions_direct_reports" aria-labelledby="training_sessions_direct_reports-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="training_session_direct_dataTable">
                                        <thead>
                                            <tr>
                                                <th class="not_include"></th>
                                                <th>Sr.No</th>
                                                <th>Employee</th>
                                                <th>Training Session</th>
                                                <th>Status</th>
                                                <th class="not_include"></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                {{-- Training Sessions Coordinated By You --}}
                <div class="tab-pane" id="training_sessions_coordinated" aria-labelledby="training_sessions_coordinated-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="training_session_coordinated_dataTable">
                                        <thead>
                                            <tr>
                                                <th class="not_include"></th>
                                                <th>Name</th>
                                                <th>Course</th>
                                                <th>Scheduled Time</th>
                                                <th>Status</th>
                                                <th>Daily Method</th>
                                                <th>Delivery Location</th>
                                                <th>Attendance Type</th>
                                                <th class="not_include">Training Certificate Required</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </section>
    </div>
</section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            my_training_session_dataTable = $('#my_training_session_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering : true,
                ajax: {
                    url: "{{ route('training.index') }}",
                    data: {
                        type: 'my_training'
                    },
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'name',
                        name: 'name', 
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
        });
        // OnClick Functions
        function allTrainSession(){
            all_training_session_dataTable = $('#all_training_session_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering : false,
                destroy: true,
                ajax: {
                    url: "{{ route('training.index') }}",
                    data: {
                        type: 'all_training'
                    },
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'id',
                        name: 'id', 
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'cName',
                    },
                    {
                        data: 'scheduled',
                        name: 'scheduled',
                    },
                    {
                        data: 'deliveryMethod',
                        name: 'deliveryMethod',
                    },
                    {
                        data: 'deliveryLocation',
                        name: 'deliveryLocation',
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
        }
        function trainSessionDirect(){
            training_session_direct_dataTable = $('#training_session_direct_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering : false,
                destroy: true,
                ajax: {
                    url: "{{ route('training.index') }}",
                    data: {
                        type: 'training_direct'
                    },
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'id',
                        name: 'id',
                    },
                    {
                        data: 'empName' , render: function(data , type , row){
                            return row.first_name + ' ' + row.last_name;
                        }
                    },
                    {
                        data: 'tName'
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
        }
        function trainSessionCoordinate(){
            training_session_coordinated_dataTable = $('#training_session_coordinated_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering : false,
                destroy: true,
                ajax: {
                    url: "{{ route('training.index') }}",
                    data: {
                        type: 'training_coordinate'
                    },
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'id',
                        name: 'id', 
                    },
                    {
                        data: 'cName',
                    },
                    {
                        data: 'scheduled',
                        name: 'scheduled',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: 'deliveryMethod',
                        name: 'deliveryMethod',
                    },
                    {
                        data: 'deliveryLocation',
                        name: 'deliveryLocation',
                    },
                    {
                        data: 'attendanceType',
                        name: 'attendanceType',
                    },
                    {
                        data: 'requireProof',
                        name: 'requireProof',
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
        }
    </script>
@endsection