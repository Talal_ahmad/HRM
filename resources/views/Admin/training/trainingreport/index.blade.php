@extends('Admin.layouts.master')

@section('title', 'Training Reports')
@section('content')
<section class="content-wrapper container-xxl p-0">
	<div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Training Reports</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Training</a>
                            </li>
                            <li class="breadcrumb-item active">Training Reports
                            </li>
                        </ol>
                    </div>
                </div>
            </div> 
        </div> 
    </div>
    <div class="content-body">
        <section class="basic-tabs-components">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="myTrainingSessions-tab" data-bs-toggle="tab" href="#myTrainingSessions" aria-controls="myTrainingSessions" role="tab" aria-selected="true">My Training Sessions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="allTrainingSessions-tab" data-bs-toggle="tab" href="#allTrainingSessions" aria-controls="allTrainingSessions" role="tab" aria-selected="false">All Training Sessions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="trainingSessionsOfDirectReports-tab" data-bs-toggle="tab" href="#trainingSessionsOfDirectReports" aria-controls="trainingSessionsOfDirectReports" role="tab" aria-selected="false">Training Sessions Of Direct Reports</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="trainingSessionsCoordinatedByYou-tab" data-bs-toggle="tab" href="#trainingSessionsCoordinatedByYou" aria-controls="trainingSessionsCoordinatedByYou" role="tab" aria-selected="false">Training Sessions Coordinated By You</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="myTrainingSessions" aria-labelledby="myTrainingSessions-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="myTrainingSessions_dataTable">
                                        <thead>
                                            <tr>
                                                <th>Training Session</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="allTrainingSessions" aria-labelledby="allTrainingSessions-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="allTrainingSessions_dataTable">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Course</th>
                                                <th>Scheduled Time</th>
                                                <th>Delivery Method</th>
                                                <th>Delivery Location</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="trainingSessionsOfDirectReports" aria-labelledby="trainingSessionsOfDirectReports-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="trainingSessionsOfDirectReports_dataTable">
                                        <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Training Session</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="trainingSessionsCoordinatedByYou" aria-labelledby="trainingSessionsCoordinatedByYou-tab" role="tabpanel">
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pb-2">
                                <table class="table" id="trainingSessionsCoordinatedByYou_dataTable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Course</th>
                                            <th>Scheduled Time</th>
                                            <th>Status</th>
                                            <th>Delivery Method</th>
                                            <th>Delivery Location</th>
                                            <th>Attendance Type</th>
                                            <th>Training Certificate Required</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section> 
            </div>
        </div>
    </section>
</div>
</section>
@endsection
@section('scripts')
<script> 
    var datatable;
    var rowid;
    $(document).ready(function() {
        $('#myTrainingSessions_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering : false,
            ajax: "{{ route('employeeTrainingSessions.index') }}",
            columns: [
            {
                data: 'tname',
                name: 'trainingsessions.name',
            },
            {
                data: 'status',
                name: 'trainingsessions.status',
            },

            
            ],

            "order": [[0, 'asc']],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
            {
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                buttons: [
                {
                    extend: 'print',
                    text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'csv',
                    text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'excel',
                    text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'pdf',
                    text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'copy',
                    text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                }
                ],
                init: function (api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function () {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            },

            ],
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
    });
    $(document).ready(function() {

        $('#allTrainingSessions_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering : false,
            ajax: "{{ route('trainingSessions.index') }}",
            columns: [
            {
                data : 'name' 
            },
            {
                data: 'cname'
            },
            {
                data: 'scheduled'
            },
            {
                data: 'deliveryMethod'
            },
            {
                data: 'deliveryLocation'
            },

            
            ],

            "order": [[0, 'asc']],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
            {
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                buttons: [
                {
                    extend: 'print',
                    text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'csv',
                    text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'excel',
                    text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'pdf',
                    text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'copy',
                    text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                }
                ],
                init: function (api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function () {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            },

            ],
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
    });
    $(document).ready(function() {

        $('#trainingSessionsOfDirectReports_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering : false,
            ajax: "{{ route('employeeTrainingSessions.index') }}",
            columns: [
            {
                data : 'first_name',
                name: 'employees.first_name',
            },
            {
                data: 'tname',
                name: 'trainingsessions.name',
            },
            {
                data: 'status',
                name: 'trainingsessions.status',
            },

            
            ],

            "order": [[0, 'asc']],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
            {
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                buttons: [
                {
                    extend: 'print',
                    text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'csv',
                    text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'excel',
                    text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'pdf',
                    text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'copy',
                    text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                }
                ],
                init: function (api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function () {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            },

            ],
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
    });
    $(document).ready(function() {

        $('#trainingSessionsCoordinatedByYou_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering : false,
            ajax: "{{ route('trainingSessions.index') }}",
            columns: [
            {
                data : 'name'
            },
            {
                data: 'cname'
            },
            {
                data: 'scheduled'
            },
            {
                data: 'status'
            },
            {
                data: 'deliveryMethod'
            },
            {
                data: 'deliveryLocation'
            },
            {
                data: 'attendanceType'
            },
            {
                data: 'requireProof'
            },

            
            ],

            "order": [[0, 'asc']],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
            {
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                buttons: [
                {
                    extend: 'print',
                    text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'csv',
                    text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'excel',
                    text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'pdf',
                    text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                },
                {
                    extend: 'copy',
                    text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                    className: 'dropdown-item',
                    action: newexportaction,
                                exportOptions: { columns: [0,1] }
                }
                ],
                init: function (api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function () {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            },

            ],
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });


    });


$(document).ready(()=>{
    $('#myTrainingSessions div.head-label').html('<h6 class="mb-0">List of My Training Sessions</h6>');
    $('#allTrainingSessions div.head-label').html('<h6 class="mb-0">List of All Training Sessions</h6>');
    $('#trainingSessionsCoordinatedByYou div.head-label').html('<h6 class="mb-0">List of Training Sessions Coordinated By You</h6>');
    $('#trainingSessionsOfDirectReports div.head-label').html('<h6 class="mb-0">List of Training Sessions Of Direct Reports</h6>');

            // Filter form control to default size for all tables
            $('.dataTables_filter .form-control').removeClass('form-control-sm');
            $('.dataTables_length .form-select').removeClass('form-select-sm').removeClass('form-control-sm');
            
        });


    </script>
    @endsection