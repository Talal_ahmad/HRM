@extends('Admin.layouts.master')

@section('title', 'Training Setup')
@section('content')

    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Training_Setup')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.training')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Training_Setup')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    {{-- Course --}}
                    <li class="nav-item">
                        <a class="nav-link active" id="courses-tab" data-bs-toggle="tab" href="#courses"
                            aria-controls="courses" role="tab" aria-selected="true">@lang('app.Courses')</a>
                    </li>
                    {{-- Training Session --}}
                    <li class="nav-item">
                        <a class="nav-link" id="trainingSessions-tab" onclick="trainingSession()" data-bs-toggle="tab"
                            href="#trainingSessions" aria-controls="trainingSessions" role="tab"
                            aria-selected="false">@lang('app.Training_Sessions')</a>
                    </li>
                    {{-- Employee Training Session --}}
                    <li class="nav-item">
                        <a class="nav-link" id="employeeTrainingSessions-tab" onclick="employeeTrainingSession()"
                            data-bs-toggle="tab" href="#employeeTrainingSessions" aria-controls="employeeTrainingSessions"
                            role="tab" aria-selected="false">@lang('app.Employee_Training_Sessions')</a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- Course --}}
                    <div class="tab-pane active" id="courses" aria-labelledby="courses-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="course_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Code</th>
                                                    <th>Name</th>
                                                    <th>Coordinator</th>
                                                    <th>Trainer</th>
                                                    <th>Payment Type</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="modal-size-lg d-inline-block">
                            <!-- Course Add Modal -->
                            <div class="modal fade text-start" id="course_add_modal" tabindex="-1"
                                aria-labelledby="myModalLabel17" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Courses')</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <form class="form" id="courses_add_form">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="code">@lang('app.Code')</label>
                                                            <input type="text" id="code" class="form-control"
                                                                placeholder="@lang('app.Enter_Code')" name="code" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="name">@lang('app.Name')</label>
                                                            <input type="text" id="name" class="form-control"
                                                                placeholder="@lang('app.Enter_Your_Name')" name="name" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="coordinator">@lang('app.Coordinator')</label>
                                                            <select class="form-control select2 form-select" data-placeholder="@lang('app.Select_Coordinator')" name="coordinator"
                                                                id="coordinator" required>
                                                                <option value="">@lang('app.Select_Coordinator')</option>
                                                                @foreach ($employees as $item)
                                                                    <option value="{{ $item->id }}">
                                                                        {{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="trainer">@lang('app.Trainer')</label>
                                                            <input type="text" id="trainer" class="form-control"
                                                                placeholder="@lang('app.Enter_Trainer')" name="trainer" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="trainer_info">@lang('app.Trainer_Details')
                                                                </label>
                                                            <textarea id="trainer_info" class="form-control" cols="20"
                                                                rows="2" placeholder="@lang('app.Enter_Details')"
                                                                name="trainer_info"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="course_info">@lang('app.Course_Details')
                                                                </label>
                                                            <textarea id="course_info" class="form-control" cols="20"
                                                                rows="2" placeholder="@lang('app.Enter_Details')"
                                                                name="course_info"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="paymentType">@lang('app.Payment_Type')
                                                                </label>
                                                            <select class="form-control select2 form-select" name="paymentType"
                                                                id="paymentType" required>
                                                                <option value="">@lang('app.Select_Payment_Type')</option>
                                                                <option value="Company Sponsored">@lang('app.Company_Sponsored')</option>
                                                                <option value="Paid By Employee">@lang('app.Paid_By_Employee')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="currency">@lang('app.Currency')</label>
                                                            <select class="form-control select2 form-select" name="currency" id="currency"
                                                                required>
                                                                <option value="">@lang('app.Select_Currency')</option>
                                                                <option value="PKR Pakistan">PKR Pakistan</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="cost">@lang('app.Cost')</label>
                                                            <input type="text" id="cost" class="form-control"
                                                                placeholder="@lang('app.Enter_Cost')" name="cost" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="status">@lang('app.Status')</label>
                                                            <select class="form-control select2 form-select" name="status" id="status"
                                                                required>
                                                                <option value="">@lang('app.Select_Status')</option>
                                                                <option value="Active">@lang('app.Active')</option>
                                                                <option value="Inactive">@lang('app.In_Active')</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Course Edit Modal -->
                            <div class="modal fade text-start" id="course_edit_modal" tabindex="-1"
                                aria-labelledby="myModalLabel17" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Courses')</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <form class="form" id="courses_edit_form">
                                            @csrf
                                            @method('PUT')
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_code">@lang('app.Code')</label>
                                                            <input type="text" id="edit_code" class="form-control"
                                                                placeholder="@lang('app.Enter_Code')" name="code" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_name">@lang('app.Name')</label>
                                                            <input type="text" id="edit_name" class="form-control"
                                                                placeholder="@lang('app.Enter_Your_Name')" name="name" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="edit_coordinator">@lang('app.Coordinator')</label>
                                                            <select class="form-control select2 form-select" name="coordinator"
                                                                id="edit_coordinator" required>
                                                                <option value="">@lang('app.Select_Coordinator')</option>
                                                                @foreach ($employees as $item)
                                                                    <option value="{{ $item->id }}">
                                                                        {{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="edit_trainer">@lang('app.Trainer')</label>
                                                            <input type="text" id="edit_trainer" class="form-control"
                                                                placeholder="@lang('app.Enter_Trainer')" name="trainer" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_trainer_info">@lang('app.Trainer_Details')
                                                                </label>
                                                            <textarea id="edit_trainer_info" class="form-control"
                                                                cols="20" rows="2" placeholder="@lang('app.Enter_Details')"
                                                                name="trainer_info"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_course_info">@lang('app.Course_Details')
                                                                </label>
                                                            <textarea id="edit_course_info" class="form-control" cols="20"
                                                                rows="2" placeholder="@lang('app.Enter_Details')"
                                                                name="course_info"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_paymentType">@lang('app.Payment_Type')
                                                                </label>
                                                            <select class="form-control select2 form-select" name="paymentType"
                                                                id="edit_paymentType" required>
                                                                <option value="">@lang('app.Select_Payment_Type')</option>
                                                                <option value="Company Sponsored">@lang('app.Company_Sponsored')</option>
                                                                <option value="Paid By Employee">@lang('app.Paid_By_Employee')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="edit_currency">@lang('app.Currency')</label>
                                                            <select class="form-control select2 form-select" name="currency"
                                                                id="edit_currency" required>
                                                                <option value="">@lang('app.Select_Currency')</option>
                                                                <option value="PKR Pakistan">PKR Pakistan</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_cost">@lang('app.Cost')</label>
                                                            <input type="text" id="edit_cost" class="form-control"
                                                                placeholder="@lang('app.Enter_Cost')" name="cost" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_status">@lang('app.Status')</label>
                                                            <select class="form-control select2 form-select" name="status" id="edit_status"
                                                                required>
                                                                <option value="">@lang('app.Select_Status')</option>
                                                                <option value="Active">@lang('app.Active')</option>
                                                                <option value="Inactive">@lang('app.In_Active')</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                <button type="submit" class="btn btn-primary" id="update">@lang('app.Update')</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    {{-- Training Session --}}
                    <div class="tab-pane" id="trainingSessions" aria-labelledby="trainingSessions-tab"
                        role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="trainingSessions_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Name</th>
                                                    <th>Course</th>
                                                    <th>Scheduled Time</th>
                                                    <th>Status</th>
                                                    <th>Delivery Method</th>
                                                    <th>Delivery Location</th>
                                                    <th>Attendance Type</th>
                                                    <th>Training Certificate Required</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="modal-size-lg d-inline-block">
                            <!-- Training Session Add Modal -->
                            <div class="modal fade text-start" id="training_session_add_modal" tabindex="-1"
                                aria-labelledby="myModalLabel18" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel18">@lang('app.Add_Training_Sessions')</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <form class="form" id="training_session_add_form">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="name">@lang('app.Name')</label>
                                                            <input type="text" id="name" class="form-control"
                                                                placeholder="@lang('app.Enter_Your_Name')" name="name" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="course">@lang('app.Course')</label>
                                                            <select class="form-control select2 form-select" name="course" id="course"
                                                                required>
                                                                <option value="">@lang('app.Select_Course')</option>
                                                                @foreach ($courses as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="description">@lang('app.Details')</label>
                                                            <textarea id="description" class="form-control" cols="20"
                                                                rows="2" placeholder="@lang('app.Enter_Details')"
                                                                name="description"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="scheduled">@lang('app.Scheduled_Time')
                                                                </label>
                                                            <input type="date" name="scheduled" id="scheduled"
                                                                class="form-control flatpickr-basic"
                                                                placeholder="YYYY-MM-DD" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="dueDate">@lang('app.Assignment_Due_Date')
                                                                </label>
                                                            <input type="date" class="form-control flatpickr-basic"
                                                                id="dueDate" name="dueDate" placeholder="YYYY-MM-DD" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="deliveryMethod">@lang('app.Delivery_Method')
                                                                </label>
                                                            <select class="form-control select2 form-select" name="deliveryMethod"
                                                                id="deliveryMethod" required>
                                                                <option value="">@lang('app.Select_Delivery_Method')</option>
                                                                <option value="Classroom">@lang('app.Classroom')</option>
                                                                <option value="Self Study">@lang('app.Self_Study')</option>
                                                                <option value="Online">@lang('app.Online')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="deliveryLocation">@lang('app.Delivery_Location')
                                                                </label>
                                                            <input type="text" id="deliveryLocation" class="form-control"
                                                                placeholder="@lang('app.Enter_Delivery_Location')"
                                                                name="deliveryLocation" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="attendanceType">@lang('app.Attendance_Type')
                                                                 </label>
                                                            <select class="form-control select2 form-select" name="attendanceType"
                                                                id="attendanceType" required>
                                                                <option value="">@lang('app.Select_Attendance_Type')</option>
                                                                <option value="Sign Up">@lang('app.Sign_Up')</option>
                                                                <option value="Assign">@lang('app.Assign')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="requireProof">@lang('app.Training_Certificate_Required')
                                                                </label>
                                                            <select class="form-control select2 form-select" name="requireProof"
                                                                id="requireProof" required>
                                                                <option value="Yes">@lang('app.Yes')</option>
                                                                <option value="No">@lang('app.No')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label for="attachment">@lang('app.Attachment')</label>
                                                            <label for="attachment"
                                                                class="btn btn-sm btn-primary mb-75 mr-75 waves-effect waves-float waves-light">@lang('app.Upload')</label>
                                                            <input type="file" id="attachment" name="attachment" hidden
                                                                accept="image/*" />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Training Session Edit Modal -->
                            <div class="modal fade text-start" id="training_session_edit_modal" tabindex="-1"
                                aria-labelledby="myModalLabel18" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel18">@lang('app.Edit_Training_Session')</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <form class="form" id="training_session_edit_form">
                                            @csrf
                                            @method('PUT')
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_name1">@lang('app.Name')</label>
                                                            <input type="text" id="edit_name1" class="form-control"
                                                                placeholder="@lang('app.Enter_Your_Name')" name="name" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_course">@lang('app.Course') </label>
                                                            <select class="form-control select2 form-select" name="course" id="edit_course"
                                                                required>
                                                                <option value="">@lang('app.Select_Course')</option>
                                                                @foreach ($courses as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="edit_description">@lang('app.Details')</label>
                                                            <textarea id="edit_description" class="form-control"
                                                                cols="20" rows="2" placeholder="@lang('app.Enter_Details')"
                                                                name="description"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_scheduled">@lang('app.Scheduled_Time')
                                                                </label>
                                                            <input type="date" name="scheduled" id="edit_scheduled"
                                                                class="form-control flatpickr-basic"
                                                                placeholder="YYYY-MM-DD" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_dueDate">@lang('app.Assignment_Due_Date')
                                                                 </label>
                                                            <input type="date" class="form-control flatpickr-basic"
                                                                id="edit_dueDate" name="dueDate" placeholder="YYYY-MM-DD" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="edit_deliveryMethod">@lang('app.Delivery_Method')</label>
                                                            <select class="form-control select2 form-select" name="deliveryMethod"
                                                                id="edit_deliveryMethod" required>
                                                                <option value="">@lang('app.Select_Delivery_Method')</option>
                                                                <option value="Classroom">@lang('app.Classroom')</option>
                                                                <option value="Self Study">@lang('app.Self_Study')</option>
                                                                <option value="Online">@lang('app.Online')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="edit_deliveryLocation">@lang('app.Delivery_Location')</label>
                                                            <input type="text" id="edit_deliveryLocation"
                                                                class="form-control"
                                                                placeholder="@lang('app.Enter_Delivery_Location')"
                                                                name="deliveryLocation" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="edit_attendanceType">@lang('app.Attendance_Type')</label>
                                                            <select class="form-control select2 form-select" name="attendanceType"
                                                                id="edit_attendanceType" required>
                                                                <option value="">@lang('app.Select_Attendance_Type')</option>
                                                                <option value="Sign Up">@lang('app.Sign_Up')</option>
                                                                <option value="Assign">@lang('app.Assign')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_requireProof">@lang('app.Training_Certificate_Required')
                                                                </label>
                                                            <select class="form-control select2 form-select" name="requireProof"
                                                                id="edit_requireProof" required>
                                                                <option value="Yes">@lang('app.Yes')</option>
                                                                <option value="No">@lang('app.No')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label for="edit_attachment">@lang('app.Attachment')</label>
                                                            <label for="attachment"
                                                                class="btn btn-sm btn-primary mb-75 mr-75 waves-effect waves-float waves-light">@lang('app.Upload')</label>
                                                            <input type="file" id="edit_attachment" name="attachment" hidden
                                                                accept="image/*" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                <button type="submit" class="btn btn-primary" id="update">@lang('app.Update')</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Employee Training Session --}}
                    <div class="tab-pane" id="employeeTrainingSessions"
                        aria-labelledby="employeeTrainingSessions-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="employeeTrainingSessions_datatable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Employee</th>
                                                    <th>Training Session</th>
                                                    <th>Status</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="modal-size-lg d-inline-block">
                            <!-- Employee Training Session Add Modal -->
                            <div class="modal fade text-start" id="employee_training_session_add_modal" tabindex="-1"
                                aria-labelledby="myModalLabel16" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel16">@lang('app.Add_Training_Sessions')</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <form class="form" id="employee_training_session_add_form">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                            <select class="form-control select2 form-select" name="employee" id="employee"
                                                                required>
                                                                <option value="">@lang('app.Select_Employee')</option>
                                                                @foreach ($employees as $item)
                                                                    <option value="{{ $item->id }}">
                                                                        {{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="trainingSession">@lang('app.Training_Sessions')</label>
                                                            <select class="form-control select2 form-select" name="trainingSession[]"
                                                                id="trainingSession" multiple required>
                                                                @foreach ($training_sessions as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="status">@lang('app.Status')</label>
                                                            <select class="form-control select2 form-select" name="status" id="status"
                                                                required>
                                                                <option value="">@lang('app.Select_Status')</option>
                                                                <option value="Scheduled">@lang('app.')</option>
                                                                <option value="Attended">@lang('app.Attended')</option>
                                                                <option value="Not-Attended">@lang('app.Not_Attended')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Employee Training Session Edit Modal -->
                            <div class="modal fade text-start" id="employee_training_session_edit_modal" tabindex="-1"
                                aria-labelledby="myModalLabel16" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel16">@lang('app.Edit_Courses')</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <form class="form" id="employee_training_session_edit_form">
                                            @csrf
                                            @method('PUT')
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="edit_employee">@lang('app.Employee')</label>
                                                            <select class="form-control select2 form-select" name="employee"
                                                                id="edit_employee" required>
                                                                <option value="">@lang('app.Select_Employee')</option>
                                                                @foreach ($employees as $item)
                                                                    <option value="{{ $item->id }}">
                                                                        {{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label"
                                                                for="edit_trainingSession">@lang('app.Training_Sessions')</label>
                                                            <select class="form-control select2 form-select" name="trainingSession"
                                                                id="edit_trainingSession" required>
                                                                <option value="">@lang('app.Select_Training_Session')</option>
                                                                @foreach ($training_sessions as $item)
                                                                    <option value="{{ $item->id }}">
                                                                        {{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="edit_status1">@lang('app.Status')</label>
                                                            <select class="form-control select2 form-select" name="status" id="edit_status1"
                                                                required>
                                                                <option value="">@lang('app.Select_Status')</option>
                                                                <option value="Scheduled">@lang('app.Scheduled')</option>
                                                                <option value="Attended">@lang('app.Attended')</option>
                                                                <option value="Not-Attended">@lang('app.Not_Attended')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                <button type="submit" class="btn btn-primary" id="update">@lang('app.Update')</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            $('#course_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: {
                    url: "{{ route('training_setup.index') }}",
                    data: {
                        type: 'course'
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'code',
                        name: 'courses.code',
                    },
                    {
                        data: 'name',
                        name: 'courses.name',
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                    },
                    {
                        data: 'trainer',
                        name: 'courses.trainer',
                    },
                    {
                        data: 'paymentType',
                        name: 'courses.paymentType',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=courses_edit(' +full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="courses_delete(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Courses',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Courses',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Courses',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Courses',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Courses',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#course_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            // Add Course Data
            $("#courses_add_form").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'course');
                $.ajax({
                    url: "{{ route('training_setup.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        console.log(response);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#courses_add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#course_add_modal").modal("hide");
                            $('#course_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Course has been Added Successfully!'
                            })
                        }

                    }
                });
            });
            // Courses Update Data
            $("#courses_edit_form").on("submit", function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'course');
                $.ajax({
                    url: "{{ url('training_setup') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#course_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            $('#course_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Course has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
            // Add Training Session Data
            $("#training_session_add_form").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'training_session');
                $.ajax({
                    url: "{{ route('training_setup.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        console.log(response);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#training_session_add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#training_session_add_modal").modal("hide");
                            $('#trainingSessions_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Training Session has been Added Successfully!'
                            })
                        }

                    }
                });
            });
            // Training Session Update data
            $("#training_session_edit_form").on("submit", function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'training_session');
                $.ajax({
                    url: "{{ url('training_setup') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#training_session_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            $('#trainingSessions_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Training Session has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
            // Employee Training Session Add Data 
            $("#employee_training_session_add_form").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'employee_training_session');
                $.ajax({
                    url: "{{ route('training_setup.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        console.log(response);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#employee_training_session_add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#employee_training_session_add_modal").modal("hide");
                            $('#employeeTrainingSessions_datatable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Training Session has been Added Successfully!'
                            })
                        }

                    }
                });
            });
            // Employee Training Session Update Data
            $("#employee_training_session_edit_form").on("submit", function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'employee_training_session');
                $.ajax({
                    url: "{{ url('training_setup') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#employee_training_session_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            $('#employeeTrainingSessions_datatable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Training Session has been Updated Successfully!'
                            })
                        }

                    }
                });
            });

            $('#trainingSessions div.head-label').html('<h6 class="mb-0">List of Training Sessions</h6>');
            $('#courses .head-label').html('<h6 class="mb-0">List of Courses</h6>');

            $('#employeeTrainingSessions div.head-label').html('<h6 class="mb-0">List of Employee Training Sessions</h6>');
        });

        // Onclick Functions
        function trainingSession() {
            // Training DataTable
            $('#trainingSessions_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                destroy: true,
                ajax: {
                    url: "{{ route('training_setup.index') }}",
                    data: {
                        type: 'training_session'
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'name',
                        name: 'trainingsessions.name',
                    },
                    {
                        data: 'cname',
                        name: 'courses.name',
                    },
                    {
                        data: 'scheduled',
                        name: 'trainingsessions.scheduled',
                    },
                    {
                        data: 'status',
                        name: 'trainingsessions.status',
                    },
                    {
                        data: 'deliveryMethod',
                        name: 'trainingsessions.deliveryMethod',
                    },
                    {
                        data: 'deliveryLocation',
                        name: 'trainingsessions.deliveryLocation',
                    },
                    {
                        data: 'attendanceType',
                        name: 'trainingsessions.attendanceType',
                    },
                    {
                        data: 'requireProof',
                        name: 'trainingsessions.requireProof',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=training_session_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="training_session_delete(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }

                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#training_session_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
        }

        function employeeTrainingSession() {
            //  Employee TrainingSession DataTable
            $('#employeeTrainingSessions_datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                destroy: true,
                ajax: {
                    url: "{{ route('training_setup.index') }}",
                    data: {
                        type: 'employee_training_session'
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                    },
                    {
                        data: 'tnames',
                        name: 'tnames',
                    },
                    {
                        data: 'status',
                        name: 'employeetrainingsessions.status',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=employee_training_session_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="employee_training_session_delete(' +
                                full.id + ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Employee Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Employee Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Employee Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Employee Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Employee Training Sessions',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#employee_training_session_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
        }


        // Edit Courses
        function courses_edit(id) {
            rowid = id;
            console.log(rowid);
            $.ajax({
                url: "{{ url('training_setup') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'course'
                },
                success: function(response) {
                    console.log(response);
                    
                    $("#edit_code").val(response.code);
                    $("#edit_name").val(response.name);
                    $("#edit_coordinator").val(response.coordinator);
                    $("#edit_trainer").val(response.trainer);
                    $("#edit_trainer_info").val(response.trainer_info);
                    $("#edit_paymentType").val(response.paymentType);
                    $("#edit_currency").val(response.currency).select2();
                    $("#edit_cost").val(response.cost);
                    $("#edit_status").val(response.status);
                    $("#course_edit_modal").modal("show");
                },
            });
        }
        // Courses Delete
        function courses_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "training_setup/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'course'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#course_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Course has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Edit Training Session
        function training_session_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('training_setup') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'training_session'
                },
                success: function(response) {
                    console.log(response.course);
                    $("#edit_name1").val(response.name);
                    $("#edit_course").val(response.course);
                    $("#edit_description").val(response.description);
                    $("#edit_scheduled").val(response.scheduled);
                    $("#edit_dueDate").val(response.dueDate);
                    $("#edit_deliveryMethod").val(response.deliveryMethod);
                    $("#edit_deliveryLocation").val(response.deliveryLocation);
                    $("#edit_attendanceType").val(response.attendanceType);
                    // $("#edit_attachment").val(response.attachment);
                    $("#edit_requireProof").val(response.requireProof);
                    $("#training_session_edit_modal").modal("show");
                },
            });
        }
        // Training Session Delete
        function training_session_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "training_setup/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'training_session'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please ContactAdministrator.'
                                        })
                                    } else {
                                        $('#trainingSessions_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Training Session has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Edit Employee Training Session
        function employee_training_session_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('training_setup') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'employee_training_session',
                },
                success: function(response) {
                    $("#edit_employee").val(response.employee);
                    $("#edit_trainingSession").val(response.trainingSession);
                    $("#edit_status1").val(response.status);
                    $("#employee_training_session_edit_modal").modal("show");
                },
            });
        }
        // Employee Training Session Delete
        function employee_training_session_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "training_setup/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'employee_training_session'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#employeeTrainingSessions_datatable').DataTable().ajax
                                            .reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Employee Training Session has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
