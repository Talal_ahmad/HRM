@extends('Admin.layouts.master')

@section('title', 'Employees')
@section('content')

    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Employees')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashbaord')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Employees')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.List')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="active_employee-tab" data-bs-toggle="tab" href="#active_employee"
                            aria-controls="active_employee" role="tab" aria-selected="true">@lang('app.Employees')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="suspend-tab" data-bs-toggle="tab" href="#suspend" aria-controls="suspend"
                            role="tab" aria-selected="false" onclick="suspend_dataTable()">@lang('app.Suspended')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="terminate-tab" data-bs-toggle="tab" href="#terminate"
                            aria-controls="terminate" role="tab" aria-selected="false"
                            onclick="terminated_dataTable()">@lang('app.Terminated')</a>
                    </li>
                    @if (env('COMPANY') == 'JSML')
                        <li class="nav-item">
                            <a class="nav-link" id="off_roll-tab" data-bs-toggle="tab" href="#off_roll"
                                aria-controls="off_roll" role="tab" aria-selected="false"
                                onclick="off_roll_dataTable()">@lang('app.OFF_Roll_Employees')</a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="active_employee" aria-labelledby="active_employee-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="employee_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>ID</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th class="not_include">First Name</th>
                                                    <th class="not_include">Last Name</th>
                                                    <th>Job Title</th>
                                                    <th>Parent Department</th>
                                                    <th>Sub Unit Branch</th>
                                                    <th>Father Name</th>
                                                    <th>CNIC</th>
                                                    <th>CNIC Exp Date</th>
                                                    <th>Employment Status</th>
                                                    <th>City</th>
                                                    <th>Mobile Phone</th>
                                                    <th>Father CNIC</th>
                                                    <th>Bank Name</th>
                                                    <th>Account Title</th>
                                                    <th>Account Number</th>
                                                    {{-- <th>Supervisor</th> --}}
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Suspended Employees --}}
                    <div class="tab-pane" id="suspend" aria-labelledby="suspend-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="suspend_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>ID</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th class="not_include">First Name</th>
                                                    <th class="not_include">Last Name</th>
                                                    <th>Job Title</th>
                                                    <th>Parent Department</th>
                                                    <th>Sub Unit Branch</th>
                                                    <th>Father Name</th>
                                                    <th>CNIC</th>
                                                    <th>CNIC EXP Date</th>
                                                    <th>Employment Status</th>
                                                    <th>City</th>
                                                    <th>Mobile Phone</th>
                                                    <th>Father CNIC</th>
                                                    <th>Bank Name</th>
                                                    <th>Account Title</th>
                                                    <th>Account Number</th>
                                                    {{-- <th>Supervisor</th> --}}
                                                    <th>Suspension Date</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Terminate Employees --}}
                    <div class="tab-pane" id="terminate" aria-labelledby="terminate-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="terminate_table">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>ID</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th class="not_include">First Name</th>
                                                    <th class="not_include">Last Name</th>
                                                    <th>Job Title</th>
                                                    <th>Parent Department</th>
                                                    <th>Sub Unit Branch</th>
                                                    <th>Father Name</th>
                                                    <th>CNIC</th>
                                                    <th>CNIC EXP Date</th>
                                                    <th>Employment Status</th>
                                                    <th>City</th>
                                                    <th>Mobile Phone</th>
                                                    <th>Father CNIC</th>
                                                    <th>Bank Name</th>
                                                    <th>Account Title</th>
                                                    <th>Account Number</th>
                                                    {{-- <th>Supervisor</th> --}}
                                                    <th>Termination Date</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Off Roll Emoployee --}}
                    <div class="tab-pane" id="off_roll" aria-labelledby="off_roll-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="off_roll_table">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>ID</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee</th>
                                                    <th class="not_include">First Name</th>
                                                    <th class="not_include">Last Name</th>
                                                    <th>City</th>
                                                    <th>Mobile Phone</th>
                                                    <th>CNIC</th>
                                                    <th>CNIC Exp Date</th>
                                                    <th>Department/Branch</th>
                                                    <th>Employment Status</th>
                                                    <th>Job Title</th>
                                                    <th>Bank Name</th>
                                                    <th>Account Title</th>
                                                    <th>Account Number</th>
                                                    {{-- <th>Supervisor</th> --}}
                                                    <th>Off Roll Date</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--Suspend Employee Modal -->
                    <div class="modal fade text-start" id="suspend_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Suspend_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="suspend_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="suspend_date">@lang('app.Suspension_Date')</label>
                                                    <input type="text" id="suspend_date"
                                                        class="form-control flatpickr-basic" name="suspension_date"
                                                        placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Suspend Modal -->

                    <!--Terminate Employee Modal -->
                    <div class="modal fade text-start" id="terminate_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Terminate_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="terminate_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row justify-content-between">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Employee')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" class="form-control" id="name"
                                                            style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Employee_ID')
                                                        </label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" class="form-control" id="ID"
                                                            style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.CNIC')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" class="form-control" id="cnic"
                                                            style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Address')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" class="form-control" id="address"
                                                            style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Employee_Status_Start_Date')
                                                        </label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" name="employee_status_start_date"
                                                            class="form-control flatpickr-basic" placeholder="Select Date"
                                                            style="border: none; border-radius: 0;"
                                                            placeholder="YYYY-MM-DD">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Comment')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" name="terminate_reason"
                                                            class="form-control" placeholder="Type Comments"
                                                            style="border: none; border-radius: 0;">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Termination_Date')
                                                        </label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" name="termination_date"
                                                            class="form-control flatpickr-basic" id="termination_date"
                                                            placeholder="Select Date"
                                                            style="border: none; border-radius: 0;"
                                                            placeholder="YYYY-MM-DD" required = "required">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <label
                                                        class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Department')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" class="form-control" id="department"
                                                            style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label
                                                        class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Designation')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" class="form-control" id="designation"
                                                            style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Date_of_Joining')
                                                        </label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" class="form-control" id="doj"
                                                            style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                @if (env('COMPANY') != 'HGNHRM')
                                                    <div class="row">
                                                        <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Separation_Type')
                                                            </label>
                                                        <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                            <input type="text" name="separation_type"
                                                                class="form-control" placeholder="Enter Type"
                                                                style="border: none; border-radius: 0;">
                                                        </div>
                                                    </div>
                                                @endif
                                                @if (env('COMPANY') == 'HGNHRM')
                                                    <div class="row">

                                                        <label class=" col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Termination_Reason')
                                                            </label>
                                                        <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                            <select name="separation_type" id="separation_type"
                                                                class="form-select" style="border: none; border-radius: 0;">
                                                                <option value="">@lang('app.Select_Termination_Reason')</option>
                                                                <option value="resigned">@lang('app.Resigned')</option>
                                                                <option value="absconded">@lang('app.Absconded')</option>
                                                                <option value="retrenched">@lang('app.Retrenched')</option>
                                                                <option value="dismissed">@lang('app.Dismissed')</option>
                                                                <option value="expired">@lang('app.Contract_Expired')</option>
                                                                <option value="package">@lang('app.Voluntary_Severance_Package')
                                                                </option>
                                                                <option value="disable">@lang('app.Illness/Medically_boarded/Disabled')
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Notice_Period_Status_End_Date')
                                                        </label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" name="notice_period_status_end_date"
                                                            class="form-control flatpickr-basic" placeholder="Select Date"
                                                            style="border: none; border-radius: 0;"
                                                            placeholder="YYYY-MM-DD">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label
                                                        class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Attachments')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="file" name="termination_file"
                                                            class="form-control" style="border: none; border-radius: 0;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Terminate Modal -->

                    <!--Restore Employee Modal -->
                    <div class="modal fade text-start" id="restore_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Restore_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="restore_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="restore_date">@lang('app.Restore_Date')</label>
                                                    <input type="text" id="restore_date"
                                                        class="form-control flatpickr-basic" name="restore_date"
                                                        placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Restore Modal -->

                    {{-- Off Roll Modal --}}
                    <div class="modal fade text-start" id="off_roll_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.OFF_Roll_Employees')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="off_roll_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="restore_date">@lang('app.Off_Roll_Date')</label>
                                                    <input type="text" id="off_roll_date"
                                                        class="form-control flatpickr-basic" name="off_roll_date"
                                                        placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- End Off Roll Modal --}}

                    {{-- On Roll Modal --}}
                    <div class="modal fade text-start" id="on_roll_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.On_Roll_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="on_roll_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="on_roll_date">@lang('app.On_Roll_Date')</label>
                                                    <input type="text" id="on_roll_date"
                                                        class="form-control flatpickr-basic" name="on_roll_date"
                                                        placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- End On Roll Modal --}}

                    {{-- Employees Filter Modal --}}
                    <div class="modal fade text-start" id="employees_filter_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Filter Employee</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="employee_search_form">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-label">Department:</label>
                                                <select name="departmentFilter" onchange="employeeFun()"
                                                    id="departmentFilter" class="select2 form-select"
                                                    data-placeholder="Select Department" required>
                                                    <option value="">Select Department</option>
                                                    @foreach (departments() as $department)
                                                        <option value="{{ $department->id }}">{{ $department->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label" for="employeeFilter">Employee</label>
                                                <select name="employeeFilter" id="employeeFilter"
                                                    class="select2 form-select" data-placeholder="Select Employee">

                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">City:</label>
                                                <select name="empCityFilter" id="empCityFilter"
                                                    class="select2 form-select" data-placeholder="Select City">
                                                    <option value="">Select City</option>
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->name }}">{{ $city->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Job Title:</label>
                                                <select name="empJobTitleFilter" onchange="employeeFun()"
                                                    id="empJobTitleFilter" class="select2 form-select"
                                                    data-placeholder="Select Job Title">
                                                    <option value="">Select Job Title</option>
                                                    @foreach ($jobtitles as $jobtitle)
                                                        <option value="{{ $jobtitle->id }}">{{ $jobtitle->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Employment Status:</label>
                                                <select name="empStatusFilter" onchange="employeeFun()"
                                                    id="empStatusFilter" class="select2 form-select"
                                                    data-placeholder="Select Employment Status">
                                                    <option value="">Select Employment Status</option>
                                                    @foreach ($employmentstatus as $status)
                                                        <option value="{{ $status->id }}">{{ $status->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Pay Grade:</label>
                                                <select name="empPayFilter" onchange="employeeFun()" id="empPayFilter"
                                                    class="select2 form-select" data-placeholder="Select Pay Grade">
                                                    <option value="">Select Pay Grade</option>
                                                    @foreach ($payGrades as $payGrade)
                                                        <option value="{{ $payGrade->id }}">
                                                            {{ $payGrade->min_salary }}-{{ $payGrade->max_salary }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Joining Date From:</label>
                                                <input type="text" name="empJoinFromDateFilter"
                                                    id="empJoinFromDateFilter" class="form-control flatpickr-basic"
                                                    placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Joining Date To:</label>
                                                <input type="text" name="empJoinToDateFilter" id="empJoinToDateFilter"
                                                    class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                    required>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Confirmation Date From:</label>
                                                <input type="text" name="empConFromDateFilter"
                                                    id="empConFromDateFilter" class="form-control flatpickr-basic"
                                                    placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Confirmation Date To:</label>
                                                <input type="text" name="empConToDateFilter" id="empConToDateFilter"
                                                    class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                    required>
                                            </div>
                                            @if (env('COMPANY') == 'CLINIX')
                                                <div class="col-md-12">
                                                    <label class="form-label">Blood Group:</label>
                                                    <select name="empBloodFilter" onchange="employeeFun()"
                                                        id="empBloodFilter" class="select2 form-select"
                                                        data-placeholder="Select Blood Group">
                                                        <option value="">Select Blood Group</option>
                                                        @foreach (departments() as $department)
                                                            <option value="{{ 'AB+' }}">{{ 'AB+' }}
                                                            </option>
                                                            <option value="{{ 'AB-' }}">{{ 'AB-' }}
                                                            </option>
                                                            <option value="{{ 'A+' }}">{{ 'A+' }}
                                                            </option>
                                                            <option value="{{ 'O+' }}">{{ 'O+' }}
                                                            </option>
                                                            <option value="{{ 'O-' }}">{{ 'O-' }}
                                                            </option>
                                                            <option value="{{ 'B+' }}">{{ 'B+' }}
                                                            </option>
                                                            <option value="{{ 'B-' }}">{{ 'B-' }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">Reset</button>
                                        <button type="submit" class="btn btn-primary" id="save">Filter</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var datatable;
        var rowid;
        var env = <?php echo json_encode($env); ?>;
        $(document).ready(function() {
            //For Export Buttons available inside jquery-datatable "server side processing" - start

            // function newexportaction(e, dt, button, config) {
            //     var self = this;
            //     var oldStart = dt.settings()[0]._iDisplayStart;
            //     dt.one('preXhr', function (e, s, data) {
            //         // Just this once, load all data from the server...
            //         data.start = 0;
            //         data.length = 2147483647;
            //         dt.one('preDraw', function (e, settings) {
            //             // Call the original action function
            //             if (button[0].className.indexOf('buttons-copy') >= 0) {
            //                 $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
            //             } else if (button[0].className.indexOf('buttons-excel') >= 0) {
            //                 $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
            //                     $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
            //                     $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
            //             } else if (button[0].className.indexOf('buttons-csv') >= 0) {
            //                 $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
            //                     $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
            //                     $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
            //             } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
            //                 $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
            //                     $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
            //                     $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
            //             } else if (button[0].className.indexOf('buttons-print') >= 0) {
            //                 $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            //             }
            //             dt.one('preXhr', function (e, s, data) {
            //                 // DataTables thinks the first item displayed is index 0, but we're not drawing that.
            //                 // Set the property to what it was before exporting.
            //                 settings._iDisplayStart = oldStart;
            //                 data.start = oldStart;
            //             });
            //             // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
            //             setTimeout(dt.ajax.reload, 0);
            //             // Prevent rendering of the full data to the DOM
            //             return false;
            //         });
            //     });
            //     // Requery the server with the new one-time export settings
            //     dt.ajax.reload();
            // };
            //For Export Buttons available inside jquery-datatable "server side processing" - End
            datatable = $('#employee_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: true,
                ajax: {
                    url: "{{ route('employees.index') }}",
                    data: function(filter) {
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.empCityFilter = $('#empCityFilter').val();
                        filter.empJobTitleFilter = $('#empJobTitleFilter').val();
                        filter.empStatusFilter = $('#empStatusFilter').val();
                        filter.empPayFilter = $('#empPayFilter').val();
                        filter.empJoinFromDateFilter = $('#empJoinFromDateFilter').val();
                        filter.empJoinToDateFilter = $('#empJoinToDateFilter').val();
                        filter.empConFromDateFilter = $('#empConFromDateFilter').val();
                        filter.empConToDateFilter = $('#empConToDateFilter').val();
                        filter.empBloodFilter = $('#empBloodFilter').val();
                        filter.type = "active";
                    }
                },
                columns: [{
                        data: 'responsive_id',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false,
                    },
                    {
                        data: 'id',
                        name: 'employees.id',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        render: function(data, type, full, meta) {
                            var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                            var lastName = full['last_name'] != null ? full['last_name'] : '';
                            return full['first_name'] + ' ' + middleName + ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'job_title',
                        name: 'jobtitles.name',
                    },
                    {
                        render: function(data, type, full, meta) {
                            if (full['parent'] == '' || full['parent'] == null) {
                                return '-';
                            }
                            return full['parent'];
                        },
                        name: 'companystructures.title',
                        searchable: true,
                    },
                    {
                        data: 'title',
                        name: 't2.title',
                    },
                    {
                        data: "father_name",
                        render: function(data, type, row) {
                            return row.father_name;
                        },
                        name: 'employees.father_name',
                    },
                    {
                        data: 'nic_num',
                        name: 'employees.nic_num',
                    },
                    {
                        data: 'cnic_expiry_date',
                        name: 'employees.cnic_expiry_date',
                    },
                    {
                        data: 'employment_status',
                        name: 'employmentstatus.name',
                    },
                    {
                        data: 'city',
                        name: 'employees.city',
                    },
                    {
                        data: 'mobile_phone',
                        name: 'employees.mobile_phone',
                    },
                    {
                        data: 'father_cnic',
                        name: 'employees.father_cnic',
                    },
                    {
                        data: 'bank_name',
                        name: 'employees.bank_name',
                    },
                    {
                        data: 'account_title',
                        name: 'employees.account_title',
                    },
                    {
                        data: 'account_number',
                        name: 'employees.account_number',
                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['super_first_name'] + ' ' + full['super_last_name'];
                    //     },
                    //     name: 'e1.first_name' + 'e1.last_name',
                    //     searchable: false
                    // },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            var url = '{{ route('employees.edit', ':id') }}';
                            url = url.replace(':id', full.id);
                            var view_url = '{{ route('staff_directory.show', ':id') }}';
                            view_url = view_url.replace(':id', full.id);
                            var history_url = '{{ url('single_employee_history') }}?id=';
                            history_url = history_url.replace('?id=', '?id=' + full.id);
                            var btn = '';
                            @can('View Employee')
                                btn = '<a href="' + view_url +
                                    '" class="me-1" title="View Employee">' +
                                    feather.icons['eye'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan

                            @can('Edit Employee')
                                btn += '<a href="' + url + '" class="me-1" title="Edit Employee">' +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan

                            @can('Single Employee History')
                                btn += '<a href="' + history_url +
                                    '" class="me-1" title="Employee History">' +
                                    feather.icons['feather'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan

                            @can('Off Roll Employee')
                                if (env == 'JSML') {
                                    btn +=
                                        '<a href="javascript:;" title="Off Roll Employee" onclick="off_roll_modal(' +
                                        full.id + ')">' +
                                        feather.icons['file-minus'].toSvg({
                                            class: 'font-medium-4 text-danger me-1'
                                        }) +
                                        '</a>';
                                }
                            @endcan

                            @can('Suspend Employee')
                                btn +=
                                    '<a href="javascript:;" title="Suspend Employee" onclick="suspend_modal(' +
                                    full.id + ')">' +
                                    feather.icons['user-minus'].toSvg({
                                        class: 'font-medium-4 text-danger me-1'
                                    }) +
                                    '</a>';
                            @endcan

                            @can('Terminate Employee')
                                btn +=
                                    `<a href="javascript:;" title="Terminate Employee" onclick="terminate_modal(${full.id},'${full.first_name} ${full.last_name}','${full.employee_id}','${full.nic_num}','${full.address1}','${full.title}','${full.job_title}','${full.joined_date}')">` +
                                    feather.icons['user-x'].toSvg({
                                        class: 'font-medium-4 text-danger'
                                    }) +
                                    '</a>';
                            @endcan

                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                iDisplayLength: -1,
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)',
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                orientation: 'landscape',
                                pageSize: 'LEGAL',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Add Employee Button')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50  font-small-4'
                            }) + 'Add Employee',
                            className: 'btn btn-primary',
                            action: function(e, dt, node, config) {
                                window.location.href = '{{ route('employees.create') }}';
                            }
                        },
                    @endcan {
                        text: feather.icons['filter'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Filter',
                        className: 'create-new btn btn-primary ms-1',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#employees_filter_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#active_employee div.head-label').html('<h6 class="mb-0">List of Employees</h6>');
            $('#terminate div.head-label').html('<h6 class="mb-0">List of Terminated Employees</h6>');

            // suspend employee form
            $("#suspend_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('suspend_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#suspend_modal").modal("hide");
                            document.getElementById("suspend_form").reset();
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been Suspended Successfully!'
                            })
                        }

                    }terminate_form
                });
            });

            // Terminate employee form
            $("#terminate_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('terminate_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#terminate_modal").modal("hide");
                            document.getElementById("terminate_form").reset();
                            datatable.ajax.reload();
                            terminated_dataTable();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been Terminated Successfully!'
                            })
                        }

                    }
                });
            });

            // Restore employee form
            $("#restore_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('restore_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#restore_modal").modal("hide");
                            document.getElementById("restore_form").reset();
                            suspend_dataTable();
                            terminated_dataTable();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been Restored Successfully!'
                            })
                        }
                    }
                });
            });

            // Off Roll Employee Form
            $("#off_roll_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('off_roll_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#off_roll_modal").modal("hide");
                            document.getElementById("off_roll_form").reset();
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been Off Rolled Successfully!'
                            })
                        }
                    }
                });
            });

            // On Roll Form
            $("#on_roll_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('on_roll_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#on_roll_modal").modal("hide");
                            document.getElementById("on_roll_form").reset();
                            off_roll_dataTable();
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been On Rolled Successfully!'
                            })
                        }
                    }
                });
            });
        });

        // Suspended Employees DataTable
        function suspend_dataTable() {
            $('#suspend_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('employees.index') }}",
                    data: {
                        type: "suspended"
                    }
                },
                columns: [{
                        data: 'responsive_id',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false

                    },
                    {
                        data: 'id',
                        name: 'employees.id',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        render: function(data, type, full, meta) {
                            return full['first_name'] + ' ' + full['middle_name'] + ' ' + full['last_name'];
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'job_title',
                        name: 'jobtitles.name',
                    },
                    {
                        render: function(data, type, full, meta) {
                            if (full['parent'] == '' || full['parent'] == null) {
                                return '-';
                            }
                            return full['parent'];
                        },
                        name: 'companystructures.title',
                        searchable: true,
                    },
                    {
                        data: 'title',
                        name: 't2.title',
                    },
                    {
                        data: "father_name",
                        render: function(data, type, row) {
                            return row.father_name;
                        },
                        name: 'employees.father_name',
                    },
                    {
                        data: 'nic_num',
                        name: 'employees.nic_num',
                    },
                    {
                        data: 'cnic_expiry_date',
                        name: 'employees.cnic_expiry_date',
                    },
                    {
                        data: 'employment_status',
                        name: 'employmentstatus.name',
                    },
                    {
                        data: 'city',
                        name: 'employees.city',
                    },
                    {
                        data: 'mobile_phone',
                        name: 'employees.mobile_phone',
                    },
                    {
                        data: 'father_cnic',
                        name: 'employees.father_cnic',
                    },
                    {
                        data: 'bank_name',
                        name: 'employees.bank_name',
                    },
                    {
                        data: 'account_title',
                        name: 'employees.account_title',
                    },
                    {
                        data: 'account_number',
                        name: 'employees.account_number',
                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['super_first_name'] + ' ' + full['super_last_name'];
                    //     },
                    //     searchable: false
                    // },
                    {
                        data: 'suspension_date',
                        name: 'employees.suspension_date',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            var history_url = '{{ url('single_employee_history') }}?id=';
                            history_url = history_url.replace('?id=', '?id=' + full.id);
                            var btn = '';
                            @can('Terminate Employee')
                                btn +=
                                    '<a href="javascript:;" title="Terminate Employee" onclick="terminate_modal(' +
                                    full.id + ')">' +
                                    feather.icons['user-x'].toSvg({
                                        class: 'font-medium-4 text-danger me-1'
                                    }) +
                                    '</a>';
                            @endcan

                            @can('Restore Employee')
                                btn +=
                                    '<a href="javascript:;" title="Restore Employee" class="font-medium-1" onclick="restore_modal(' +
                                    full.id + ')">' +
                                    '<i class="fa fa-undo" aria-hidden="true"></i>' +
                                    '</a>';
                            @endcan

                            @can('Single Employee History')
                                btn += '<a href="' + history_url +
                                    '" class="me-1" title="Employee History">' +
                                    feather.icons['feather'].toSvg({
                                        class: 'font-medium-4 ms-1'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: [0, 1]
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                'd-inline-flex');
                        }, 50);
                    }
                }, ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#suspend div.head-label').html('<h6 class="mb-0">List of Suspended Employees</h6>');
        }

        // Terminated Employees DataTable
        function terminated_dataTable() {
            $('#terminate_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // ordering: false,
                destroy: true,
                ajax: "{{ route('employees.index') }}",
                columns: [{
                        data: 'responsive_id',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                    },
                    {
                        data: 'id',
                        name: 'employees.id',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        render: function(data, type, full, meta) {
                            return full['first_name'] + ' ' + full['middle_name'] + ' ' + full['last_name'];
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'job_title',
                        name: 'jobtitles.name',
                    },
                    {
                        render: function(data, type, full, meta) {
                            if (full['parent'] == '' || full['parent'] == null) {
                                return '-';
                            }
                            return full['parent'];
                        },
                        name: 'companystructures.title',
                        searchable: true,
                    },
                    {
                        data: 'title',
                        name: 't2.title',
                    },
                    {
                        data: "father_name",
                        render: function(data, type, row) {
                            return row.father_name;
                        },
                        name: 'employees.father_name',
                    },
                    {
                        data: 'nic_num',
                        name: 'employees.nic_num',
                    },
                    {
                        data: 'cnic_expiry_date',
                        name: 'employees.cnic_expiry_date',
                    },
                    {
                        data: 'employment_status',
                        name: 'employmentstatus.name',
                    },
                    {
                        data: 'city',
                        name: 'employees.city',
                    },
                    {
                        data: 'mobile_phone',
                        name: 'employees.mobile_phone',
                    },
                    {
                        data: 'father_cnic',
                        name: 'employees.father_cnic',
                    },
                    {
                        data: 'bank_name',
                        name: 'employees.bank_name',
                    },
                    {
                        data: 'account_title',
                        name: 'employees.account_title',
                    },
                    {
                        data: 'account_number',
                        name: 'employees.account_number',
                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['super_first_name'] + ' ' + full['super_last_name'];
                    //     },
                    //     searchable: false
                    // },
                    {
                        data: 'termination_date',
                        name: 'employees.termination_date',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            var history_url = '{{ url('single_employee_history') }}?id=';
                            history_url = history_url.replace('?id=', '?id=' + full.id);
                            var btn = '';
                            @can('Restore Employee')
                                btn +=
                                    '<a href="javascript:;" title="Restore Employee" class="font-medium-1" onclick="restore_modal(' +
                                    full.id + ')">' +
                                    '<i class="fa fa-undo" aria-hidden="true"></i>' +
                                    '</a>'
                            @endcan
                            @can('Single Employee History')
                                btn += '<a href="' + history_url +
                                    '" class="me-1" title="Employee History">' +
                                    feather.icons['feather'].toSvg({
                                        class: 'font-medium-4 ms-1'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Terminated Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Terminated Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Terminated Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'landscape',
                            title: 'Terminated Employees',
                            pageSize: 'LEGAL',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Terminated Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                'd-inline-flex');
                        }, 50);
                    }
                }, ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#terminate div.head-label').html('<h6 class="mb-0">List of Terminated Employees</h6>');
        }

        // Off Roll Employee DataTable
        function off_roll_dataTable() {
            $('#off_roll_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // ordering: false,
                destroy: true,
                ajax: {
                    url: "{{ route('employees.index') }}",
                    data: {
                        type: 'off_roll'
                    }
                },
                columns: [{
                        data: 'responsive_id',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                    },
                    {
                        data: 'id',
                        name: 'employees.id',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        render: function(data, type, full, meta) {
                            return full['first_name'] + ' ' + full['middle_name'] + ' ' + full['last_name'];
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'job_title',
                        name: 'jobtitles.name',
                    },
                    {
                        render: function(data, type, full, meta) {
                            if (full['parent'] == '' || full['parent'] == null) {
                                return '-';
                            }
                            return full['parent'];
                        },
                        name: 'companystructures.title',
                        searchable: true,
                    },
                    {
                        data: 'title',
                        name: 't2.title',
                    },
                    {
                        data: "father_name",
                        render: function(data, type, row) {
                            return row.father_name;
                        },
                        name: 'employees.father_name',
                    },
                    {
                        data: 'nic_num',
                        name: 'employees.nic_num',
                    },
                    {
                        data: 'employment_status',
                        name: 'employmentstatus.name',
                    },
                    {
                        data: 'city',
                        name: 'employees.city',
                    },
                    {
                        data: 'mobile_phone',
                        name: 'employees.mobile_phone',
                    },
                    {
                        data: 'father_cnic',
                        name: 'employees.father_cnic',
                    },
                    {
                        data: 'bank_name',
                        name: 'employees.bank_name',
                    },
                    {
                        data: 'account_title',
                        name: 'employees.account_title',
                    },
                    {
                        data: 'account_number',
                        name: 'employees.account_number',
                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['super_first_name'] + ' ' + full['super_last_name'];
                    //     },
                    //     searchable: false
                    // },
                    {
                        data: 'off_roll_date',
                        name: 'employees.off_roll_date',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            var btn = '';
                            @can('On Roll Employee')
                                btn +=
                                    '<a href="javascript:;" title="Restore Employee" class="font-medium-1" onclick="on_roll_modal(' +
                                    full.id + ')">' +
                                    '<i class="fa fa-undo" aria-hidden="true"></i>' +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'landscape',
                            title: 'Off Roll Employees',
                            pageSize: 'LEGAL',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                'd-inline-flex');
                        }, 50);
                    }
                }, ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#off_roll div.head-label').html('<h6 class="mb-0">List of Off Roll Employees</h6>');
        }

        // Suspend Employee Modal
        function suspend_modal(id) {
            $(".modal-body #employee_id").val(id);
            $('#suspend_modal').modal('toggle');
        }

        // Terminate Employee Modal
        function terminate_modal(id, name, ID, cnic, address, department, designation, doj) {
            $(".modal-body #employee_id").val(id);
            $('#name').val(name);
            $('#ID').val(ID);
            $('#cnic').val(cnic);
            $('#address').val(address);
            $('#department').val(department);
            $('#designation').val(designation);
            $('#doj').val(doj);
            $('#terminate_modal').modal('toggle');
        }

        // Restore Employee Modal
        function restore_modal(id) {
            $(".modal-body #employee_id").val(id);
            $('#restore_modal').modal('toggle');
        }

        // Off Role Employee Modal
        function off_roll_modal(id) {
            $(".modal-body #employee_id").val(id);
            $('#off_roll_modal').modal('toggle');
        }

        // On Roll Employee Modal
        function on_roll_modal(id) {
            $(".modal-body #employee_id").val(id);
            $('#on_roll_modal').modal('toggle');
        }

        // Get Employees
        function employeeFun() {
            var dept_id = $('#departmentFilter').val();
            $.ajax({
                url: "{{ url('employees') }}" + "/" + dept_id,
                type: 'GET',
                success: function(response) {
                    $('#employeeFilter').html('');
                    var opt = $('<option>');
                    $('#employeeFilter').append(opt);
                    $.each(response, function(id, curEmp) {
                        var middleName = curEmp.middle_name != null ? curEmp.middle_name : '';
                        var lastName = curEmp.last_name != null ? curEmp.last_name : '';
                        var opt = $('<option>');
                        opt.val(curEmp.id);
                        opt.text(curEmp.employee_id + "-" + curEmp.employee_code + "-" + curEmp
                            .first_name + " " + middleName + " " + lastName + "-" + curEmp
                            .designation + "-" + curEmp.department);
                        $('#employeeFilter').append(opt);
                    });
                }
            });
        }

        $('#employee_search_form').submit(function(e) {
            $('#employees_filter_modal').modal('hide');
            datatable.draw();
            e.preventDefault();
        });
    </script>
@endsection
