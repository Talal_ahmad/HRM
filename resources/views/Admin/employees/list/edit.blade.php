@extends('Admin.layouts.master')

@section('title', 'Edit Employee')
@section('content')
<?php 
$crumbs = explode("/",$_SERVER["REQUEST_URI"]);
$crumbs = array_filter($crumbs);
?>
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Edit Employees</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Employees</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">List</a>
                                </li>
                                <li class="breadcrumb-item active">Edit
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="horizontal-wizard">
                <div class="bs-stepper employee-horizontal-wizard">
                    <div class="bs-stepper-header" role="tablist">
                        <div class="step" data-target="#personal-modern" role="tab" id="personal-modern-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="user" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Personal Info</span>
                                    <span class="bs-stepper-subtitle">Add Personal Info</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#address-step-modern" role="tab" id="address-step-modern-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="map-pin" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Address</span>
                                    <span class="bs-stepper-subtitle">Add Address</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#contact_info" role="tab" id="contact_info-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="phone" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Contact Info</span>
                                    <span class="bs-stepper-subtitle">Add Contact Info</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#job_details" role="tab" id="job_details-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="file-text" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Job Details</span>
                                    <span class="bs-stepper-subtitle">Add Job Details</span>
                                </span>
                            </button>
                        </div>
                        @can('Employee Leave Management')
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#leaves" role="tab" id="leaves-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="file-text" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Leave Management</span>
                                    <span class="bs-stepper-subtitle">Assign Leave Group</span>
                                </span>
                            </button>
                        </div>
                        @endcan
                        @can('Employee CG Management')
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#calculation-group" role="tab" id="calculation-group-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="file-text" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Calculation Group Management</span>
                                    <span class="bs-stepper-subtitle">Assign Calculation Group</span>
                                </span>
                            </button>
                        </div>
                        @endcan
                        @can('Employee Salary Management')
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#salary-setup" role="tab" id="salary-setup-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="file-text" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Salary Setup</span>
                                    <span class="bs-stepper-subtitle">Manage Salary Components</span>
                                </span>
                            </button>
                        </div>
                        @endcan
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#shift" role="tab" id="shift-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="file-text" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Shift Management/Submit</span>
                                    <span class="bs-stepper-subtitle">Assign Shift</span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="bs-stepper-content">
                        <div id="personal-modern" class="content" role="tabpanel" aria-labelledby="personal-modern-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">Personal Info</h5>
                                <small class="text-muted">Enter Employee Personal Info.</small>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="employee_id">Employee ID</label>
                                        @if (env('COMPANY') == 'CLINIX')
                                            <input type="text" id="employee_id" class="form-control" value="{{$employee->employee_id}}" placeholder="Employee ID" name="employee_id"/>
                                        @else
                                            <input type="text" id="employee_id" class="form-control" value="{{$employee->employee_id}}" placeholder="Employee ID" name="employee_id" readonly/>
                                        @endif
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="employee_code">Employee Code</label>
                                        <input type="text" id="employee_code" class="form-control" value="{{$employee->employee_code}}" placeholder="Employee Code" name="employee_code"/>
                                    </div>
                                </div>
                                @if (env('COMPANY') == 'HGNHRM')
                                    <div class="row">
                                        <div class="mb-1 col-4">
                                            <label class="form-label" for="preferred_name">Preferred Name</label>
                                            <input type="text" id="preferred_name" class="form-control" value="{{$employee->preferred_name}}"
                                                placeholder="Preferred Name" name="preferred_name" />
                                        </div>
                                        <div class="mb-1 col-4">
                                            <label class="form-label" for="initials">initials</label>
                                            <input type="text" id="initials" class="form-control" value="{{$employee->initials}}"
                                                placeholder="initials" name="initials" />
                                        </div>
                                        <div class="mb-1 col-4">
                                            <label class="form-label" for="title">Title</label>
                                            <input type="text" id="title" class="form-control" value="{{$employee->title}}"
                                                placeholder="Title" name="title" />
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="first_name">First Name</label>
                                        <input type="text" id="first_name" class="form-control" value="{{$employee->first_name}}" placeholder="First Name" name="first_name" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="middle_name">Middle Name</label>
                                        <input type="text" id="middle_name" class="form-control" value="{{$employee->middle_name}}" placeholder="Middle Name" name="middle_name" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="last_name">Last Name</label>
                                        <input type="text" id="last_name" class="form-control" value="{{$employee->last_name}}" placeholder="Last Name" name="last_name" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="religion">Religion</label>
                                        <select name="religion" id="religion" class="form-select">
                                            <option value="">Select Religion</option>
                                            <option value="muslim" {{$employee->religion == "muslim"  ? 'selected' : ''}}>Muslim</option>
                                            <option value="non_muslim" {{$employee->religion == "non_muslim"  ? 'selected' : ''}}>Non Muslim</option>
                                        </select>
                                    </div>
                                    @if (env('COMPANY') == 'CIDEX')
                                    <div class="mb-1 col-md-12">
                                        <label class="form-label" for="sequence">Sequence</label>
                                        <input type="number" id="sequence" class="form-control" placeholder="Sequence" name="sequence" value="{{$employee->sequence}}" required/>
                                    </div>
                                    @endif
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="father_name">Father Name</label>
                                        <input type="text" id="father_name" class="form-control" value="{{$employee->father_name}}" placeholder="Father Name" name="father_name" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="father_cnic">Father CNIC</label>
                                        <input type="text" id="father_cnic" class="form-control" value="{{$employee->father_cnic}}" placeholder="Father CNIC" name="father_cnic" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="nic_num">CNIC</label>
                                        <input type="text" id="nic_num" class="form-control" value="{{$employee->nic_num}}" placeholder="CNIC" name="nic_num" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="cnic_expiry_date">CNIC EXP Date</label>
                                        <input type="text" id="cnic_expiry_date" class="form-control" value="{{$employee->cnic_expiry_date}}" placeholder="CNIC EXP Date" name="cnic_expiry_date" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="ssn_num">SSN</label>
                                        <input type="text" id="ssn_num" class="form-control" value="{{$employee->ssn_num}}" placeholder="SSN" name="ssn_num" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="driving_license">Driving Liscence No</label>
                                        <input type="text" id="driving_license" class="form-control" value="{{$employee->driving_license}}" placeholder="Driving Liscence No" name="driving_license" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="other_id">Other ID</label>
                                        <input type="text" id="other_id" class="form-control" value="{{$employee->other_id}}" placeholder="Other ID" name="other_id" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="birthday">Date of Birth</label>
                                        <input type="text" name="birthday" id="birthday" value="{{$employee->birthday}}" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="nationality ">Nationality</label>
                                        <select name="nationality" id="nationality" class="form-select">
                                            <option value=""></option>
                                            @foreach ($nationality as $national)
                                                <option value="{{$national->id}}" {{$national->id == $employee->nationality ? 'selected' : ''}}>{{$national->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="gender">Gender</label>
                                        <select name="gender" id="gender" class="form-select">
                                            <option value="Male" {{$employee->gender == "Male"  ? 'selected' : ''}}>Male</option>
                                            <option value="Female" {{$employee->gender == "Female"  ? 'selected' : ''}}>FeMale</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="marital_status">Marital Status</label>
                                        <select name="marital_status" id="marital_status" class="form-select">
                                            <option value="Single" {{$employee->marital_status == "Single"  ? 'selected' : ''}}>Single</option>
                                            <option value="Married" {{$employee->marital_status == "Married"  ? 'selected' : ''}}>Married</option>
                                            <option value="Divorced" {{$employee->marital_status == "Divorced"  ? 'selected' : ''}}>Divorced</option>
                                            <option value="Widowed" {{$employee->marital_status == "Widowed"  ? 'selected' : ''}}>Widowed</option>
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="blood_group">Blood Group</label>
                                        <select name="blood_group" id="blood_group" class="select2 form-select" data-placeholder="Select Blood Group">
                                            <option value=""></option>
                                            <option value="A+" {{$employee->blood_group == "A+"  ? 'selected' : ''}}>A+</option>
                                            <option value="A-" {{$employee->blood_group == "A-"  ? 'selected' : ''}}>A-</option>
                                            <option value="B+" {{$employee->blood_group == "B+"  ? 'selected' : ''}}>B+</option>
                                            <option value="B-" {{$employee->blood_group == "B-"  ? 'selected' : ''}}>B-</option>
                                            <option value="O+" {{$employee->blood_group == "O+"  ? 'selected' : ''}}>O+</option>
                                            <option value="O-" {{$employee->blood_group == "O-"  ? 'selected' : ''}}>O-</option>
                                            <option value="AB+" {{$employee->blood_group == "AB+"  ? 'selected' : ''}}>AB+</option>
                                            <option value="AB-" {{$employee->blood_group == "AB-"  ? 'selected' : ''}}>AB-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    @if (env('COMPANY') != 'HGNHRM')    
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="eobi">EOBI</label>
                                            <input type="text" id="eobi" class="form-control" value="{{$employee->eobi}}" placeholder="EOBI" name="eobi" />
                                        </div>
                                    @endif
                                    @if (env('COMPANY') == 'HGNHRM')    
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="tax_no">Tax No.</label>
                                            <input type="text" id="tax_no" class="form-control" value="{{$employee->tax_no}}" placeholder="Tax No." name="tax_no" />
                                        </div>
                                    @endif
                                    <div class="mb-1 col-6">
                                        <label class="form-label" for="ethnicity">Ethnicity</label>
                                        <select name="ethnicity" id="ethnicity" class="select2 form-select" data-placeholder="Select Ethnicity">
                                            <option value=""></option>
                                            @foreach ($ethnicity as $ethni)
                                                <option value="{{$ethni->id}}" {{$ethni->id == $employee->ethnicity ? 'selected' : ''}}>{{$ethni->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if (env('COMPANY') == 'HGNHRM')
                                    <div class="row">
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="language">Language</label>
                                            <input type="text" id="language" class="form-control" value="{{$employee->language}}"
                                                placeholder="Language" name="language" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="citizenship">citizenship</label>
                                            <input type="text" id="citizenship" class="form-control" value="{{$employee->citizenship}}"
                                                placeholder="Citizenship" name="citizenship" />
                                        </div>
                                    </div>
                                @endif
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-outline-secondary btn-prev" disabled>
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">Next</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        <div id="address-step-modern" class="content" role="tabpanel" aria-labelledby="address-step-modern-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">Address</h5>
                                <small>Enter Employee Address.</small>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="country">Country</label>
                                        <select name="country" id="country" class="select2 form-select" data-placeholder="Select a Country">
                                            <option value=""></option>
                                            @foreach ($countries as $country)
                                                <option value="{{$country->code}}" {{$country->code == $employee->country ? 'selected' : ''}}>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="province">Province</label>
                                        <select name="province" id="province" class="select2 form-select" data-placeholder="Select a Province">
                                            <option value=""></option>
                                            @foreach ($provinces as $province)
                                                <option value="{{$province->id}}" {{$province->id == $employee->province ? 'selected' : ''}}>{{$province->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="postal_code">Postal/Zip Code</label>
                                        <input type="text" id="postal_code" class="form-control" name="postal_code" value="{{$employee->postal_code}}" placeholder="Postal/Zip Code" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="city">City</label>
                                        <select name="city" id="city" class="select2 form-select" data-placeholder="Select city">
                                            <option value=""></option>
                                            @foreach ($cities as $city)
                                                <option value="{{$city->id}}" {{$city->id == $employee->city ? 'selected' : ''}}>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                        {{-- <input type="text" id="city" class="form-control" value="{{$employee->city}}" placeholder="City" name="city" /> --}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="address1">Address 1</label>
                                        <input type="text" id="address1" class="form-control" value="{{$employee->address1}}" placeholder="Address 1" name="address1" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="address2">Address 2</label>
                                        <input type="text" id="address2" class="form-control" value="{{$employee->address2}}" placeholder="Address 2" name="address2" />
                                    </div>
                                    @if (env('COMPANY') == 'HGNHRM')
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="region_description">Region Description</label>
                                            <input type="text" id="region_description" class="form-control" value="{{$employee->region_description}}"
                                                placeholder="Region Description" name="region_description" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_add_1">Postal Address 1</label>
                                            <input type="text" id="postal_add_1" class="form-control" value="{{$employee->postal_add_1}}"
                                                placeholder="Postal Address 1" name="postal_add_1" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_add_2">Postal Address 2</label>
                                            <input type="text" id="postal_add_2" class="form-control" value="{{$employee->postal_add_2}}"
                                                placeholder="Postal Address 2" name="postal_add_2" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_add_3">Postal Address 3</label>
                                            <input type="text" id="postal_add_3" class="form-control" value="{{$employee->postal_add_3}}"
                                                placeholder="Postal Address 3" name="postal_add_3" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_code_1">Postal Code</label>
                                            <input type="text" id="postal_code_1" class="form-control" value="{{$employee->postal_code_1}}"
                                                placeholder="Postal Code" name="postal_code_1" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_province">Postal Province</label>
                                            <input type="text" id="postal_province" class="form-control" value="{{$employee->postal_province}}"
                                                placeholder="Postal Province" name="postal_province" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_country">Postal Country</label>
                                            <input type="text" id="postal_country" class="form-control" value="{{$employee->postal_country}}"
                                                placeholder="Postal Country" name="postal_country" />
                                        </div>
                                    @endif
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">Next</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        <div id="contact_info" class="content" role="tabpanel" aria-labelledby="contact_info-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">Contact Info</h5>
                                <small>Enter Employee Contact Info.</small>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="home_phone">Home Phone</label>
                                        <input type="text" id="home_phone" class="form-control phone-number-mask" name="home_phone" value="{{$employee->home_phone}}" placeholder="Home Phone" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="mobile_phone">Mobile Phone</label>
                                        <input type="text" id="mobile_phone" class="form-control phone-number-mask" name="mobile_phone" value="{{$employee->mobile_phone}}" placeholder="Mobile Phone" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="work_phone">Work Phone</label>
                                        <input type="text" id="work_phone" class="form-control phone-number-mask" name="work_phone" value="{{$employee->work_phone}}" placeholder="Work Phone" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="work_email">Work Email</label>
                                        <input type="email" id="work_email" class="form-control" name="work_email" value="{{$employee->work_email}}" placeholder="Work Email" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-12">
                                        <label class="form-label" for="private_email">Private Email</label>
                                        <input type="email" id="private_email" class="form-control" name="private_email" value="{{$employee->private_email}}" placeholder="Private Email" />
                                    </div>
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">Next</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        <div id="job_details" class="content" role="tabpanel" aria-labelledby="job_details-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">Job Details</h5>
                                <small>Enter Employee Job Details.</small>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="joined_date">Joined Date</label>
                                        <input type="text" name="joined_date" id="joined_date" value="{{$employee->joined_date}}" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="confirmation_date">Confirmation Date</label>
                                        <input type="text" name="confirmation_date" id="confirmation_date" value="{{$employee->confirmation_date}}" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="work_station_id">Work Station ID</label>
                                        <input type="text" id="work_station_id" class="form-control" name="work_station_id" value="{{$employee->work_station_id}}" placeholder="Work Station ID" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="employment_status ">Employement Status </label>
                                        <select name="employment_status" id="employment_status" class="select2 form-select" data-placeholder="Select a Employement Status">
                                            <option value=""></option>
                                            @foreach ($employmentstatus as $status)
                                                <option value="{{$status->id}}" {{$status->id == $employee->employment_status ? 'selected' : ''}}>{{$status->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="job_title ">Job Title</label>
                                        <select name="job_title" id="job_title" class="select2 form-select" data-placeholder="Select a Job Title">
                                            <option value=""></option>
                                            @foreach ($jobtitles as $jobtitle)
                                                <option value="{{$jobtitle->id}}" {{$jobtitle->id == $employee->job_title ? 'selected' : ''}}>{{$jobtitle->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="pay_grade">Pay Grade</label>
                                        <select name="pay_grade" id="pay_grade" class="select2 form-select" data-placeholder="Select a Pay Grade">
                                            <option value=""></option>
                                            @foreach ($paygrades as $paygrade)
                                                <option value="{{$paygrade->id}}" {{$paygrade->id == $employee->pay_grade ? 'selected' : ''}}>{{$paygrade->name}} ({{$paygrade->min_salary}}-{{$paygrade->max_salary}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if (env('COMPANY') == 'HGNHRM')
                                    <div class="row">
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="position_effective_date">Position Effective
                                                Date</label>
                                            <input type="text" id="position_effective_date"
                                                class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" value="{{$employee->position_effective_date}}"
                                                name="position_effective_date" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="tax_status_description">Tax Status
                                                Description</label>
                                            <input type="text" id="tax_status_description" class="form-control" value="{{$employee->tax_status_description}}"
                                                placeholder="Tax Status Description" name="tax_status_description" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="eefunction_description">eefunction
                                                Description</label>
                                            <input type="text" id="eefunction_description" class="form-control" value="{{$employee->eefunction_description}}"
                                                placeholder="eefunction Description" name="eefunction_description" />
                                        </div>

                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="position_filled_since">Position Filled
                                                Since</label>
                                            <input type="text" id="position_filled_since"
                                                class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" value="{{$employee->position_filled_since}}"
                                                name="position_filled_since" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="coida_reference_number">Coida Reference
                                                Number</label>
                                            <input type="text" id="coida_reference_number" class="form-control" value="{{$employee->coida_reference_number}}"
                                                placeholder="Coida Reference Number" name="coida_reference_number" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="uif_exemption_reason">UIF Exemption
                                                Reason</label>
                                            <input type="text" id="uif_exemption_reason" class="form-control" value="{{$employee->uif_exemption_reason}}"
                                                placeholder="UIF Exemption Reason" name="uif_exemption_reason" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="sdl_exemption_reason">SDL Exemption
                                                Reason</label>
                                            <input type="text" id="sdl_exemption_reason" class="form-control" value="{{$employee->sdl_exemption_reason}}"
                                                placeholder="SDL Exemption Reason" name="sdl_exemption_reason" />
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="not_re_employable">Not RE Employable</label>
                                            <select name="not_re_employable" id="not_re_employable" class="form-select">
                                                <option value="{{$employee->not_re_employable}}">{{$employee->not_re_employable}}</option>
                                                <option value="no">No</option>
                                                <option value="yes">Yes</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="not_qualify_employment_tax">Does Not Qualify
                                                For Employment Tax Incentive</label>
                                            <select name="not_qualify_employment_tax" id="not_qualify_employment_tax"
                                                class="form-select">
                                                <option value="{{$employee->not_qualify_employment_tax}}">{{$employee->not_qualify_employment_tax}}</option>
                                                <option value="no">No</option>
                                                <option value="yes">Yes</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="director_for_coida">Director For Coida</label>
                                            <select name="director_for_coida" id="director_for_coida"
                                                class="form-select">
                                                <option value="{{$employee->director_for_coida}}">{{$employee->director_for_coida}}</option>
                                                <option value="false">False</option>
                                                <option value="true">True</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="exclude_for_coida">Exclude For Coida</label>
                                            <select name="exclude_for_coida" id="exclude_for_coida" class="form-select">
                                                <option value="{{$employee->exclude_for_coida}}">{{$employee->exclude_for_coida}}</option>
                                                <option value="false">False</option>
                                                <option value="true">True</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="exclude_from_eea_report">Exclude From EEA
                                                Report</label>
                                            <select name="exclude_from_eea_report" id="exclude_from_eea_report"
                                                class="form-select">
                                                <option value="{{$employee->exclude_from_eea_report}}">{{$employee->exclude_from_eea_report}}</option>
                                                <option value="false">False</option>
                                                <option value="true">True</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-12">
                                            <label class="form-label" for="exclude_from_uif_submission_file">Exclude From
                                                UIF Submission File</label>
                                            <select name="exclude_from_uif_submission_file"
                                                id="exclude_from_uif_submission_file" class="form-select">
                                                <option value="{{$employee->exclude_from_uif_submission_file}}">{{$employee->exclude_from_uif_submission_file}}</option>
                                                <option value="false">False</option>
                                                <option value="true">True</option>
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    {{-- <div class="mb-1 col-md-6">
                                        <label class="form-label" for="department">Department</label>
                                        <select name="department" id="department" class="select2 form-select" data-placeholder="Select a Department">
                                            <option value=""></option>
                                            @foreach ($companystructures as $companystructure)
                                                <option value="{{$companystructure->id}}" {{$companystructure->id == $employee->department ? 'selected' : ''}}>{{$companystructure->title}}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}
                                    <div class="mb-1 col-md-12">
                                        <label class="form-label" for="supervisor ">Supervisor</label>
                                        <select name="supervisor" id="supervisor" class="select2 form-select" data-placeholder="Select Supervisor">
                                            <option value=""></option>
                                            @foreach (employees() as $emp)
                                                <option value="{{$emp->id}}" {{$emp->id == $employee->supervisor ? 'selected' : ''}}>{{$emp->first_name." ".$emp->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="indirect_supervisors">Indirect Supervisors</label>
                                        <select name="indirect_supervisors" id="indirect_supervisors" class="select2 form-select" data-placeholder="Select Indirect Supervisors">
                                            <option value=""></option>
                                            @foreach (employees() as $emp)
                                                <option value="{{$emp->id}}" {{$emp->id == $employee->indirect_supervisors ? 'selected' : ''}}>{{$emp->first_name." ".$emp->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="approver1">First Level Approver</label>
                                        <select name="approver1" id="approver1" class="select2 form-select" data-placeholder="Select First Level Approver">
                                            <option value=""></option>
                                            @foreach (employees() as $emp)
                                                <option value="{{$emp->id}}" {{$emp->id == $employee->approver1 ? 'selected' : ''}}>{{$emp->first_name." ".$emp->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="approver2">Second Level Approver</label>
                                        <select name="approver2" id="approver2" class="select2 form-select" data-placeholder="Select Second Level Approver">
                                            <option value=""></option>
                                            @foreach (employees() as $emp)
                                                <option value="{{$emp->id}}" {{$emp->id == $employee->approver2 ? 'selected' : ''}}>{{$emp->first_name." ".$emp->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="approver3">Third Level Approver</label>
                                        <select name="approver3" id="approver3" class="select2 form-select" data-placeholder="Select Third Level Approver">
                                            <option value=""></option>
                                            @foreach (employees() as $emp)
                                                <option value="{{$emp->id}}" {{$emp->id == $employee->approver3 ? 'selected' : ''}}>{{$emp->first_name." ".$emp->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="passcode">PassCode</label>
                                        <input type="text" id="passcode" class="form-control" name="passcode" value="{{$employee->passcode}}" placeholder="PassCode" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="posting_station">Posting Station</label>
                                        <select name="posting_station" id="posting_station" class="select2 form-select" data-placeholder="Select Posting Station">
                                            <option value=""></option>
                                            @foreach ($cities as $city)
                                                <option value="{{$city->id}}" {{$city->id == $employee->posting_station ? 'selected' : ''}}>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if (env('COMPANY') == 'HGNHRM')
                                    <div class="row">
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="account_type">Account Type</label>
                                            <input type="text" id="account_type" class="form-control" value="{{$employee->account_type}}"
                                                placeholder="Account Type" name="account_type" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="branch_code">Branch Code</label>
                                            <input type="text" id="branch_code" class="form-control" value="{{$employee->branch_code}}"
                                                placeholder="Branch Code" name="branch_code" />
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="bank_id">Bank</label>
                                        <select name="bank_id" id="bank_id" class="select2 form-select" data-placeholder="Select Bank">
                                            <option value=""></option>
                                            @foreach ($banks as $bank)
                                                <option value="{{$bank->id}}" {{$bank->id == $employee->bank_id ? 'selected' : ''}}>{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="account_title">Bank Account Title</label>
                                        <input type="text" id="account_title" class="form-control" name="account_title" value="{{$employee->account_title}}" placeholder="Bank Account Title" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="account_number">Bank Account Number</label>
                                        <input type="text" id="account_number" class="form-control" name="account_number" value="{{$employee->account_number}}" placeholder="Bank Account Number" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="covid_vaccinated">Covid Vaccinated</label>
                                        <select name="covid_vaccinated" id="covid_vaccinated" class="form-select">
                                            <option value="no">No</option>
                                            <option value="yes">Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-12">
                                        <label class="form-label" for="ppc_reg_no">PPC Registeration Number</label>
                                        <input type="text" id="ppc_reg_no" class="form-control" name="ppc_reg_no" value="{{$employee->ppc_reg_no}}" placeholder="PPC Registeration Number" />
                                    </div>
                                </div>
                                <div class="row">
                                    @if (env('COMPANY') != 'HGNHRM')    
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="provident_fund">Provident Fund</label>
                                        <select name="provident_fund" data-placeholder="Provident Fund" id="provident_fund" class="form-select select2">
                                            <option value="no" {{$employee->provident_fund == "no" ? 'selected' : ''}}>No</option>
                                            <option value="yes" {{$employee->provident_fund == "yes" ? 'selected' : ''}}>Yes</option>
                                        </select>
                                    </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="eobi">EOBI Status</label>
                                            <select name="eobi_status" id="eobi_status" data-placeholder="Select EOBI" class="form-select select2">
                                                <option value="no" {{$employee->eobi_status == "no" ? 'selected' : ''}}>No</option>
                                                <option value="yes" {{$employee->eobi_status == "yes" ? 'selected' : ''}}>Yes</option>
                                            </select>
                                        </div>
                                    @else
                                        <div class="mb-1 col-md-12">
                                            <label class="form-label" for="provident_fund">Provident Fund</label>
                                            <select name="provident_fund" data-placeholder="Provident Fund" id="provident_fund" class="form-select select2">
                                                <option value="no" {{$employee->provident_fund == "no" ? 'selected' : ''}}>No</option>
                                                <option value="yes" {{$employee->provident_fund == "yes" ? 'selected' : ''}}>Yes</option>
                                            </select>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-12 mb-1">
                                        <label class="form-label" for="pessi">PESSI</label>
                                        <select name="pessi" id="pessi" data-placeholder="Select PESSI" class="form-select select2">
                                            <option value="no" {{$employee->pessi == "no" ? 'selected' : ''}}>No</option>
                                            <option value="yes" {{$employee->pessi == "yes" ? 'selected' : ''}}>Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">Next</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        @can('Employee Leave Management')
                        <div id="leaves" class="content" role="tabpanel" aria-labelledby="leaves-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">Leave Management</h5>
                                <small>See Leaves Details.</small>
                                <br>
                                <code class="text-danger">Don't Select Any Leave Group Here If you Dont Want to save Employee Leaves Settings as per Joining or Re-Joining Date. If Leaves Table Populated with Data Then On Saving the settings will be saved.</code>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="mb-1 col-12">
                                        <label class="form-label" for="leave_groups">Leave Groups</label>
                                        <select name="leave_groups[]" id="leave_groups" class="select2 form-select" data-placeholder="Select Groups" multiple>
                                            <option value=""></option>
                                            @php
                                                $employee_leave_groups = $employee->leaveGroups()->pluck('leave_group')->toArray();
                                            @endphp
                                            @foreach ($leaveGroups as $leaveGroup)
                                                <option value="{{$leaveGroup->id}}" {{in_array($leaveGroup->id, $employee_leave_groups) ? 'selected' : ''}}>{{$leaveGroup->name}}</option>
                                            @endforeach
                                        </select>
                                        <button type="button" onclick="calculateLeaves()" class="btn btn-sm btn-info">Get Leaves</button>
                                    </div>
                                    <div class="mb-1 col-12">
                                        <p id="leaveCalculationText" class="text-info"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <table class="table table-striped" id="leaves_table">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.No</th>
                                                        <th>Leave Type</th>
                                                        <th>Allowed</th>
                                                        <th>Availed</th>
                                                        <th>Remaining</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">Next</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        @endcan
                        @can('Employee CG Management')
                        <div id="calculation-group" class="content" role="tabpanel" aria-labelledby="calculation-group-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">Calculation Group Management</h5>
                                <small>Manage Calculation Group</small>
                            </div>
                            <form>
                                <div class="add_groups" id="add_groups">
                                    <div data-repeater-list="calculationGroups">
                                        @forelse ($employee->calculationGroups as $calculation_group)
                                            <div data-repeater-item>
                                                <div class="row">
                                                    <input type="hidden" name="id" value="{{$calculation_group->id}}">
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="pay_frequency">Pay Frequency</label>
                                                            <select name="pay_frequency" id="pay_frequency" class="select2 form-select" data-placeholder="Select Pay Frequency" required>
                                                                <option value=""></option>
                                                                @foreach ($payFrequency as $frequency)
                                                                    <option value="{{ $frequency->id }}" {{$calculation_group->pay_frequency == $frequency->id ? 'selected' : ''}}>{{ $frequency->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="currency">Currency</label>
                                                            <select name="currency" id="currency" class="select2 form-select" data-placeholder="Select Currency" required>
                                                                <option value=""></option>
                                                                @foreach ($currencies as $currency)
                                                                    <option value="{{ $currency->id }}" {{$calculation_group->currency == $currency->id ? 'selected' : ''}}>{{ $currency->code }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="calculation_group">Calculation Group</label>
                                                                <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="Select Calculation Group" required>
                                                                    <option value=""></option>
                                                                    @foreach ($calculationGroup as $group)
                                                                        <option value="{{ $group->id }}" {{$calculation_group->deduction_group == $group->id ? 'selected' : ''}}>{{ $group->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="deduction_exemptions">Calculation Exemptions</label>
                                                            <select name="deduction_exemptions" id="deduction_exemptions" class="select2 form-select"
                                                                data-placeholder="Select Calculation Exemptions">
                                                                <option value=""></option>
                                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                                    <option value="{{ $item->id }}" {{$calculation_group->deduction_exemptions == $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="deduction_allowed">Calculation Assigned</label>
                                                            <select name="deduction_allowed" id="deduction_allowed" class="select2 form-select"
                                                                data-placeholder="Select Calculation Assigned">
                                                                <option value=""></option>
                                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                                    <option value="{{ $item->id }}" {{$calculation_group->deduction_allowed == $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-12 mb-51">
                                                        <div class="mb-1">
                                                            <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                <i data-feather="x" class="me-25"></i>
                                                                <span>Delete</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>
                                            @empty
                                            <div data-repeater-item>
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="pay_frequency">Pay Frequency</label>
                                                            <select name="pay_frequency" id="pay_frequency" class="select2 form-select" data-placeholder="Select Pay Frequency" required>
                                                                <option value=""></option>
                                                                @foreach ($payFrequency as $frequency)
                                                                    <option value="{{ $frequency->id }}">{{ $frequency->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="currency">Currency</label>
                                                            <select name="currency" id="currency" class="select2 form-select" data-placeholder="Select Currency" required>
                                                                <option value=""></option>
                                                                @foreach ($currencies as $currency)
                                                                    <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="calculation_group">Calculation Group</label>
                                                                <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="Select Calculation Group" required>
                                                                    <option value=""></option>
                                                                    @foreach ($calculationGroup as $group)
                                                                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="deduction_exemptions">Calculation Exemptions</label>
                                                            <select name="deduction_exemptions" id="deduction_exemptions" class="select2 form-select"
                                                                data-placeholder="Select Calculation Exemptions">
                                                                <option value=""></option>
                                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="deduction_allowed">Calculation Assigned</label>
                                                            <select name="deduction_allowed" id="deduction_allowed" class="select2 form-select"
                                                                data-placeholder="Select Calculation Assigned">
                                                                <option value=""></option>
                                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-12 mb-51">
                                                        <div class="mb-1">
                                                            <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                <i data-feather="x" class="me-25"></i>
                                                                <span>Delete</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>
                                        @endforelse
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-12">
                                            <button class="btn btn-icon btn-info" type="button"
                                                data-repeater-create>
                                                <i data-feather="plus" class="me-25"></i>
                                                <span>Add New</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">Next</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        @endcan
                        @can('Employee Salary Management')
                        <div id="salary-setup" class="content" role="tabpanel" aria-labelledby="salary-setup-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">Salary Setup</h5>
                                <small>Manage Salary Components</small>
                            </div>
                            <form>
                                <div class="row">
                                    <hr>
                                    @foreach($SalaryComponentTypes as $SalaryComponentType)
                                        @if(isset($salary_components[$SalaryComponentType->id]))
                                            <div class="col-12">
                                                <h4>{{$SalaryComponentType->name}}</h4>
                                                <div class="mb-1">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Salary Component</th>
                                                                <th>Amount</th>
                                                                <th>Remarks</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($salary_components[$SalaryComponentType->id] as $component)
                                                            <tr>
                                                                <td>{{$component->name}}</td>
                                                                <td>
                                                                    <input type="hidden" name="components[]" value="{{$component->id}}">
                                                                    <input type="text" class="form-control" name="sc_amount[{{$component->id}}]" value="{{$comp[$component->id]}}">
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="remarks[{{$component->id}}]" value="{{$remarks[$component->id]}}" class="form-control">
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif
                                    @endforeach
                                    <h4>Monthly Components</h4>
                                    <div class="mb-1">
                                        <label class="form-label" for="month">Month</label>
                                        <input type="month" name="applied_month" class="form-control" id="month" value="{{ !empty($applied_month) ? \Carbon\Carbon::parse($applied_month)->format('Y-m') : '' }}">
                                    </div>
                                    <hr>
                                    @foreach($SalaryComponentTypes as $SalaryComponentType)
                                        @if(isset($monthly_salary_components[$SalaryComponentType->id]))
                                            <div class="col-12">
                                                <h4>{{$SalaryComponentType->name}}</h4>
                                                <div class="mb-1">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Salary Component</th>
                                                                <th>Amount</th>
                                                                <th>Remarks</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($monthly_salary_components[$SalaryComponentType->id] as $component)
                                                            <tr>
                                                                <td>{{$component->name}}</td>
                                                                <td>
                                                                    <input type="hidden" name="monthly_components[]" value="{{$component->id}}">
                                                                    <input type="text" class="form-control" name="monthly_sc_amount[{{$component->id}}]" value="{{$month_comp[$component->id]}}">
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="monthly_remarks[{{$component->id}}]" value="{{$month_remarks[$component->id]}}" class="form-control">
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif
                                    @endforeach
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">Next</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        @endcan
                        <div id="shift" class="content" role="tabpanel" aria-labelledby="shift-trigger">
                            @can('Employee Shift Management')
                                <div class="content-header">
                                    <h5 class="mb-0">Shift Management</h5>
                                    <small>Manage Shift</small>
                                </div>
                                <form>
                                    <div class="row">
                                        <div class="mb-1 col-12">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Shift Type</th>
                                                            <th>Work Week</th>
                                                            <th>Shift From</th>
                                                            <th>Shift To</th>
                                                            <th>Late Time Allowed</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody> 
                                                        <tr>
                                                            <td>
                                                                <select name="shift_id" id="shift" class=" form-select">
                                                                    <option value="">Select Shift Type</option>
                                                                    @foreach ($shift_types as $shift)
                                                                        <option value="{{ $shift->id }}">{{ $shift->shift_desc }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select name="work_week_id" id="work_week" class="form-select">
                                                                    <option value="">Select Work Week</option>
                                                                    @foreach ($work_weeks as $week)
                                                                        <option value="{{ $week->id }}">{{ $week->desc }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="date" name="shift_from_date" class="form-control">
                                                            </td>
                                                            <td>
                                                                <input type="date" name="shift_to_date" class="form-control">
                                                            </td>
                                                            @if (env('COMPANY') == 'JSML' || env('COMPANY') == 'AJMAL' || env('COMPANY') == 'RoofLine')    
                                                                <td>
                                                                    <input type="text" id="late_time_allowed" class="form-control"
                                                                        value="15" name="late_time_allowed" readonly />
                                                                </td>
                                                            @else
                                                                <td>
                                                                    <input type="text" id="late_time_allowed" class="form-control"
                                                                        value="11" name="late_time_allowed" readonly />
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endcan
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <button class="form_save btn btn-success btn-submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var restore_date = "{{ $employee->restore_date}}";
        $(document).ready(function(){
            new Cleave($('#nic_num'), {
                delimiter: '-',
                blocks: [5, 7, 1],
                numericOnly : true
            });

            new Cleave($('#father_cnic'), {
                delimiter: '-',
                blocks: [5, 7, 1],
                numericOnly : true
            });

            var bsStepper = document.querySelectorAll('.bs-stepper'),
            horizontalWizard = document.querySelector('.employee-horizontal-wizard');
            // Adds crossed class
            if (typeof bsStepper !== undefined && bsStepper !== null) {
                for (var el = 0; el < bsStepper.length; ++el) {
                bsStepper[el].addEventListener('show.bs-stepper', function (event) {
                    var index = event.detail.indexStep;
                    var numberOfSteps = $(event.target).find('.step').length - 1;
                    var line = $(event.target).find('.step');

                    // The first for loop is for increasing the steps,
                    // the second is for turning them off when going back
                    // and the third with the if statement because the last line
                    // can't seem to turn off when I press the first item. ¯\_(ツ)_/¯

                    for (var i = 0; i < index; i++) {
                    line[i].classList.add('crossed');

                    for (var j = index; j < numberOfSteps; j++) {
                        line[j].classList.remove('crossed');
                    }
                    }
                    if (event.detail.to == 0) {
                    for (var k = index; k < numberOfSteps; k++) {
                        line[k].classList.remove('crossed');
                    }
                    line[0].classList.remove('crossed');
                    }
                });
                }
            }

            if (typeof horizontalWizard !== undefined && horizontalWizard !== null) {
                var numberedStepper = new Stepper(horizontalWizard, {
                    linear: false,
                });
                $form = $(horizontalWizard).find('form');
                $form.each(function () {
                var $this = $(this);
                if("{! env('COMPANY') == 'CIDEX' !}")
                {
                    $this.validate({
                        rules: {
                            employee_id: { required: true },
                            'first_name': { required: true },
                            // 'last_name': { required: true},
                            // 'father_name': { required: true},
                        }
                    });
                }
                else
                {
                    $this.validate({
                        rules: {
                            employee_id: { required: true },
                            // employee_code: { required: true},
                            'first_name': { required: true },
                            // 'last_name': { required: true},
                            // 'father_name': { required: true},
                            // birthday : { required: true},
                            // province: { required: true},
                            // postal_code: { required: true},
                            // city: { required: true},
                            // address1: { required: true},
                            // mobile_phone: { required: true},
                            // private_email: { required: true},
                            // joined_date: { required: true},
                            employment_status: { required: true},
                            job_title: { required: true},
                            department: { required: true},
                            // bank_id: { required: true},
                            // account_title: { required: true},
                            // account_number: { required: true},
                        }
                    });
                }
                });

                $(horizontalWizard)
                .find('.btn-next')
                .each(function () {
                    $(this).on('click', function (e) {
                    var isValid = $(this).parent().siblings('form').valid();
                    if (isValid) {
                        numberedStepper.next();
                    } else {
                        e.preventDefault();
                    }
                    });
                });

                $(horizontalWizard)
                .find('.btn-prev')
                .on('click', function () {
                    numberedStepper.previous();
                });

                $(horizontalWizard)
                .find('.btn-submit')
                .on('click', function (e) {
                    e.preventDefault();
                    var isValid = $(this).parent().siblings('form').valid();
                    if (isValid) 
                    {
                        ButtonStatus('.form_save',true);
                        blockUI();
                        var form = $('form').serialize();
                        $.ajax({
                            url: "{{route('employees.update', $employee->id)}}",
                            type: "PUT",
                            data: form,
                            success: function (response) {
                                ButtonStatus('.form_save',false);
                                $.unblockUI();
                                if(response.code == 200){
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Employee has been Added Successfully!'
                                    });
                                    setTimeout(function(){
                                        if (1 == 1) {
                                            
                                            window.location.href = "{{ url('getEmployees') }}";
                                        } else {
                                            
                                            window.location.href = "{{ url('employees') }}";
                                        }
                                    }, 2500);
                                }
                                else if(response.errors){
                                    $.each( response.errors, function( index, value ){
                                        Toast.fire({
                                            icon: 'error',
                                            title: value
                                        })
                                    });
                                }
                                else if(response.error_message){
                                    Toast.fire({
                                        icon: 'error',
                                        title: response.error_message
                                    })
                                }
                                else{
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                }
                            }
                        });
                    }
                });
            }
            var calculation_process = $('#add_groups');
            $('.add_groups').repeater({
                isFirstItemUndeletable: false,
                show: function() {
                    $(this).slideDown();
                    calculation_process.find('select').next('.select2-container').remove();
                    calculation_process.find('select').select2();
                    // Feather Icons
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                }
            });
        });
        var isProgrammaticChange = false; // Flag to track programmatic changes

        $("#leave_groups").on("change", function() {
            if (!isProgrammaticChange) {
                calculateLeaves();
            }
        });
        function calculateLeaves() {
            blockUI();
            var joining_date = $('#joined_date').val();
            var calculation_date;
            if (restore_date != null && restore_date != '') {
                calculationDateText = 'Restore Date';
                calculation_date = restore_date;
            } 
            else {
                calculationDateText = 'Joining Date';
                calculation_date = joining_date;
            }
            if (calculation_date == null || calculation_date == '') {
                // Set the flag to true before making programmatic changes
                isProgrammaticChange = true;
                $("#leave_groups > option").prop("selected", false);
                // Trigger the change event without causing a loop
                $("#leave_groups").trigger("change");
                // Set the flag back to false
                isProgrammaticChange = false;
                Swal.fire({
                    icon: 'error',
                    title: 'Please Select Calculation (Joining Or Restore) Date First!',
                });
                return false;
            }
            var leave_groups = $('#leave_groups').val();
            $('#leaveCalculationText').text("As Per Selected Leave Groups And Employee "+calculationDateText+" (" + calculation_date + "), Below Is the table Representing Approximate Leaves Information");
            $.ajax({
                url: "{{url('new-emp-leave-balance')}}",
                method: 'GET',
                data : {
                    leave_groups : leave_groups,
                    calculation_date : calculation_date
                },
                success: function(response) {
                    $.unblockUI();
                    if ($('input[name="new_leave_settings"]').length === 0) {
                        var hiddenInput = $('<input>').attr('type', 'hidden').attr('name', 'new_leave_settings').val(JSON.stringify(response));
                        $('form').append(hiddenInput);
                    } else {
                        $('input[name="new_leave_settings"]').val(JSON.stringify(response));
                    }
                    var tableBody = $('#leaves_table tbody');
                    tableBody.empty();
                    for (var i = 0; i < response.length; i++) {
                        var leave = response[i];
                        var row = '<tr>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td>' + leave.leaveType + '</td>' +
                            '<td>' + leave.allowed + '</td>' +
                            '<td>' + leave.availed + '</td>' +
                            '<td>' + leave.remaining + '</td>' +
                            '</tr>';
                        tableBody.append(row);
                    }
                },
                error: function(error) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Error On Fetching Leaves!',
                    });
                }
            });
        }
    </script>
@endsection