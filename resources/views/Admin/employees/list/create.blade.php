@extends('Admin.layouts.master')
@section('title', 'Create Employee')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Add_Employees')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashbaord')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Employees')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.List')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Create')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="horizontal-wizard">
                <div class="bs-stepper employee-horizontal-wizard">
                    <div class="bs-stepper-header" role="tablist">
                        <div class="step" data-target="#personal-modern" role="tab" id="personal-modern-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="user" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">@lang('app.Personal_Info')</span>
                                    <span class="bs-stepper-subtitle">@lang('app.Add_Personal_Info')</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#address-step-modern" role="tab"
                            id="address-step-modern-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="map-pin" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">@lang('app.Address')</span>
                                    <span class="bs-stepper-subtitle">@lang('app.Add_Address')</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#contact_info" role="tab" id="contact_info-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="phone" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">@lang('app.Contact_Info')</span>
                                    <span class="bs-stepper-subtitle">@lang('app.Add_Contact_Info')</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#job_details" role="tab" id="job_details-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="file-text" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">@lang('app.Job_Details')</span>
                                    <span class="bs-stepper-subtitle">@lang('app.Add_Job_Details')</span>
                                </span>
                            </button>
                        </div>
                        @can('Employee Leave Management')
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#leaves" role="tab" id="leaves-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">
                                        <i data-feather="file-text" class="font-medium-3"></i>
                                    </span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">@lang('app.Leave_Management')</span>
                                        <span class="bs-stepper-subtitle">@lang('app.Assign_Leave_Group')</span>
                                    </span>
                                </button>
                            </div>
                        @endcan
                        @can('Employee CG Management')
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#calculation-group" role="tab" id="calculation-group-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">
                                        <i data-feather="file-text" class="font-medium-3"></i>
                                    </span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">@lang('app.Calculation_Group_Management')</span>
                                        <span class="bs-stepper-subtitle">@lang('app.Assign_Calculation_Group')</span>
                                    </span>
                                </button>
                            </div>
                        @endcan
                        @can('Employee Salary Management')
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#salary-setup" role="tab" id="salary-setup-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">
                                        <i data-feather="file-text" class="font-medium-3"></i>
                                    </span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">@lang('app.Salary_Setup')</span>
                                        <span class="bs-stepper-subtitle">@lang('app.Manage_Salary_Components')</span>
                                    </span>
                                </button>
                            </div>
                        @endcan
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#shift" role="tab" id="shift-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="file-text" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">@lang('app.Shift_Management/Submit')</span>
                                    <span class="bs-stepper-subtitle">@lang('app.Assign_Shift')</span>
                                </span>
                            </button>
                        </div>
                        {{-- <div class="" id="go-back">
                            <a href="{{route('employees.index')}}" type="button" class="btn btn-primary btn-next">
                                <i data-feather="arrow-left" class="align-middle"></i>
                                <span class="align-middle d-sm-inline-block d-none">Go Back</span>
                            </a>
                        </div> --}}
                    </div>

                    <div class="bs-stepper-content">
                        <div id="personal-modern" class="content" role="tabpanel"
                            aria-labelledby="personal-modern-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">@lang('app.Personal_Info')</h5>
                                <small class="text-muted">@lang('app.Enter_Employee_Personal_Info').</small>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="employee_id">@lang('app.Employee_ID')</label>
                                        <input type="text" id="employee_id" class="form-control"
                                            placeholder="@lang('app.Employee_ID')" name="employee_id" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="employee_code">@lang('app.Employee_Code')</label>
                                        <input type="text" id="employee_code" class="form-control"
                                            placeholder="@lang('app.Employee_Code')" name="employee_code" />
                                    </div>
                                </div>
                                @if (env('COMPANY') == 'HGNHRM')
                                    <div class="row">
                                        <div class="mb-1 col-4">
                                            <label class="form-label" for="preferred_name">@lang('app.Preferred_Name')</label>
                                            <input type="text" id="preferred_name" class="form-control"
                                                placeholder="@lang('app.Preferred_Name')" name="preferred_name" />
                                        </div>
                                        <div class="mb-1 col-4">
                                            <label class="form-label" for="initials">@lang('app.initials')</label>
                                            <input type="text" id="initials" class="form-control"
                                                placeholder="@lang('app.initials')" name="initials" />
                                        </div>
                                        <div class="mb-1 col-4">
                                            <label class="form-label" for="title">@lang('app.Title')</label>
                                            <input type="text" id="title" class="form-control"
                                                placeholder="@lang('app.Title')" name="title" />
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="first_name">@lang('app.First_Name')</label>
                                        <input type="text" id="first_name" class="form-control"
                                            placeholder="@lang('app.First_Name')" name="first_name" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="middle_name">@lang('app.Middle_Name')</label>
                                        <input type="text" id="middle_name" class="form-control"
                                            placeholder="@lang('app.Middle_Name')" name="middle_name" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="last_name">@lang('app.Last_Name')</label>
                                        <input type="text" id="last_name" class="form-control"
                                            placeholder="@lang('app.Last_Name')" name="last_name" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="religion">@lang('app.Religion')</label>
                                        <select name="religion" id="religion" class="form-select">
                                            <option value="">@lang('app.Select_Religion')</option>
                                            <option value="muslim">@lang('app.Muslim')</option>
                                            <option value="non_muslim">@lang('app.Non_Muslim')</option>
                                        </select>
                                    </div>
                                    @if (env('COMPANY') == 'CIDEX')
                                        <div class="mb-1 col-md-12">
                                            <label class="form-label" for="sequence">@lang('app.Sequence')</label>
                                            <input type="number" id="sequence" class="form-control"
                                                placeholder="@lang('app.Sequence')" name="sequence" required />
                                        </div>
                                    @endif
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="father_name">@lang('app.Father_Name')</label>
                                        <input type="text" id="father_name" class="form-control"
                                            placeholder="@lang('app.Father_Name')" name="father_name" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="father_cnic">@lang('app.Father_CNIC')</label>
                                        <input type="text" id="father_cnic" class="form-control"
                                            placeholder="@lang('app.Father_CNIC')" name="father_cnic" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="nic_num">@lang('app.CNIC')</label>
                                        <input type="text" id="nic_num" class="form-control" placeholder="@lang('app.CNIC')"
                                            name="nic_num" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="cnic_expiry_date">@lang('app.CNIC_EXP')</label>
                                        <input type="text" id="cnic_expiry_date" class="form-control flatpickr-basic"
                                            placeholder="YYYY-MM-DD" / name="cnic_expiry_date" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="ssn_num">@lang('app.SSN')</label>
                                        <input type="text" id="ssn_num" class="form-control" placeholder="@lang('app.SSN')"
                                            name="ssn_num" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="driving_license">@lang('app.Driving_Liscence_No')</label>
                                        <input type="text" id="driving_license" class="form-control"
                                            placeholder="@lang('app.Driving_Liscence_No')" name="driving_license" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="other_id">@lang('app.Other_ID')</label>
                                        <input type="text" id="other_id" class="form-control" placeholder="@lang('app.Other_ID')"
                                            name="other_id" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="birthday">@lang('app.Date_of_Birth')</label>
                                        <input type="text" name="birthday" id="birthday"
                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="nationality ">@lang('app.Nationality')</label>
                                        <select name="nationality" id="nationality" class="select2 form-select">
                                            <option value=""></option>
                                            @foreach ($nationality as $national)
                                                <option value="{{ $national->id }}">{{ $national->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="gender">@lang('app.Gender')</label>
                                        <select name="gender" id="gender" class="form-select">
                                            <option value="Male">@lang('app.Male')</option>
                                            <option value="Female">@lang('app.Female')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="marital_status">@lang('app.Marital_Status')</label>
                                        <select name="marital_status" id="marital_status" class="form-select">
                                            <option value="Single">@lang('app.Single')</option>
                                            <option value="Married">@lang('app.Married')</option>
                                            <option value="Divorced">@lang('app.Divorced')</option>
                                            <option value="Widowed">@lang('app.Widowed')</option>
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="blood_group">@lang('app.Blood_Group')</label>
                                        <select name="blood_group" id="blood_group" class="form-select">
                                            <option value="A+">A+</option>
                                            <option value="A-">A-</option>
                                            <option value="B+">B+</option>
                                            <option value="B-">B-</option>
                                            <option value="O+">O+</option>
                                            <option value="O-">O-</option>
                                            <option value="AB+">AB+</option>
                                            <option value="AB-">AB-</option>
                                        </select>
                                    </div>
                                    @if (env('COMPANY') == 'CLINIX')
                                        <div class="mb-1 col-md-12">
                                            <label class="form-label" for="blood_group">@lang('app.Blood_Group')</label>
                                            <select name="blood_group" id="blood_group" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Blood_Group')">
                                                <option value=""></option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="O+">O+</option>
                                                <option value="O-">O-</option>
                                                <option value="AB+">AB+</option>
                                                <option value="AB-">AB-</option>
                                            </select>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    @if (env('COMPANY') != 'HGNHRM')
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="eobi">EOBI</label>
                                            <input type="text" id="eobi" class="form-control" placeholder="EOBI"
                                                name="eobi" />
                                        </div>
                                    @endif
                                    @if (env('COMPANY') == 'HGNHRM')
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="tax_no">@lang('app.Tax_No').</label>
                                            <input type="text" id="tax_no" class="form-control"
                                                placeholder="@lang('app.Tax_No')." name="tax_no" />
                                        </div>
                                    @endif
                                    <div class="mb-1 col-6">
                                        <label class="form-label" for="ethnicity">@lang('app.Ethnicity')</label>
                                        <select name="ethnicity" id="ethnicity" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Ethnicity')">
                                            <option value=""></option>
                                            @foreach ($ethnicity as $ethni)
                                                <option value="{{ $ethni->id }}">{{ $ethni->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                @if (env('COMPANY') == 'HGNHRM')
                                    <div class="row">
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="language">@lang('app.Language')</label>
                                            <input type="text" id="language" class="form-control"
                                                placeholder="@lang('app.Language')" name="language" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="citizenship">@lang('app.citizenship')</label>
                                            <input type="text" id="citizenship" class="form-control"
                                                placeholder="@lang('app.citizenship')" name="citizenship" />
                                        </div>
                                    </div>
                                @endif
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-outline-secondary btn-prev" disabled>
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">@lang('app.Previous')</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">@lang('app.Next')</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        <div id="address-step-modern" class="content" role="tabpanel"
                            aria-labelledby="address-step-modern-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">@lang('app.Address')</h5>
                                <small>@lang('app.Enter_Employee_Address').</small>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="country">@lang('app.Country')</label>
                                        <select name="country" id="country" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_a_Country')">
                                            <option value=""></option>
                                            @foreach ($countries as $country)
                                                <option value="{{ $country->code }}">{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="province">@lang('app.Province')</label>
                                        <select name="province" id="province" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_a_Province')">
                                            <option value=""></option>
                                            @foreach ($provinces as $province)
                                                <option value="{{ $province->id }}">{{ $province->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="postal_code">@lang('app.Postal/Zip_Code')</label>
                                        <input type="text" id="postal_code" class="form-control" name="postal_code"
                                            placeholder="@lang('app.Postal/Zip_Code')" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="city">@lang('app.city')</label>
                                        {{-- <input type="text" id="city" class="form-control" placeholder="City" name="city" /> --}}
                                        <select name="city" id="city" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_a_City')">
                                            <option value=""></option>
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="address1">@lang('app.Address_1')</label>
                                        <input type="text" id="address1" class="form-control"
                                            placeholder="@lang('app.Address_1')" name="address1" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="address2">@lang('app.Address_2')</label>
                                        <input type="text" id="address2" class="form-control"
                                            placeholder="@lang('app.Address_2')" name="address2" />
                                    </div>
                                    @if (env('COMPANY') == 'HGNHRM')
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="region_description">@lang('app.Region_Description')</label>
                                            <input type="text" id="region_description" class="form-control"
                                                placeholder="@lang('app.Region_Description')" name="region_description" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_add_1">@lang('app.Postal_Address_1')</label>
                                            <input type="text" id="postal_add_1" class="form-control"
                                                placeholder="@lang('app.Postal_Address_1')" name="postal_add_1" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_add_2">@lang('app.Postal_Address_2')</label>
                                            <input type="text" id="postal_add_2" class="form-control"
                                                placeholder="@lang('app.Postal_Address_2')" name="postal_add_2" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_add_3">@lang('app.Postal_Address_3')</label>
                                            <input type="text" id="postal_add_3" class="form-control"
                                                placeholder="@lang('app.Postal_Address_3')" name="postal_add_3" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_code_1">@lang('app.Postal_Code')</label>
                                            <input type="text" id="postal_code_1" class="form-control"
                                                placeholder="@lang('app.Postal_Code')" name="postal_code_1" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_province">@lang('app.Postal_Province')</label>
                                            <input type="text" id="postal_province" class="form-control"
                                                placeholder="@lang('app.Postal_Province')" name="postal_province" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="postal_country">@lang('app.Postal_Country')</label>
                                            <input type="text" id="postal_country" class="form-control"
                                                placeholder="@lang('app.Postal_Country')" name="postal_country" />
                                        </div>
                                    @endif
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">@lang('app.Previous')</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">@lang('app.Next')</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        <div id="contact_info" class="content" role="tabpanel" aria-labelledby="contact_info-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">@lang('app.Contact_Info')</h5>
                                <small>@lang('app.Enter_Employee_Contact_Info').</small>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="home_phone">@lang('app.Home_Phone')</label>
                                        <input type="text" id="home_phone" class="form-control phone-number-mask"
                                            name="home_phone" placeholder="@lang('app.Home_Phone')" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="mobile_phone">@lang('app.Mobile_Phone')</label>
                                        <input type="text" id="mobile_phone" class="form-control phone-number-mask"
                                            name="mobile_phone" placeholder="@lang('app.Mobile_Phone')" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="work_phone">@lang('app.Work_Phone')</label>
                                        <input type="text" id="work_phone" class="form-control phone-number-mask"
                                            name="work_phone" placeholder="@lang('app.Work_Phone')" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="work_email">@lang('app.Work_Email')</label>
                                        <input type="email" id="work_email" class="form-control" name="work_email"
                                            placeholder="@lang('app.Work_Email')" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-12">
                                        <label class="form-label" for="private_email">@lang('app.Private_Email')</label>
                                        <input type="email" id="private_email" class="form-control"
                                            name="private_email" placeholder="@lang('app.Private_Email')" />
                                    </div>
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">@lang('app.Previous')</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">@lang('app.Next')</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        <div id="job_details" class="content" role="tabpanel" aria-labelledby="job_details-trigger">
                            <div class="content-header">
                                <h5 class="mb-0">@lang('app.Job_Details')</h5>
                                <small>@lang('app.Enter_Employee_Job_Details').</small>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="joined_date">@lang('app.Joined_Date')</label>
                                        <input type="text" name="joined_date" id="joined_date"
                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="confirmation_date">@lang('app.Confirmation_Date')</label>
                                        <input type="text" name="confirmation_date" id="confirmation_date"
                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="work_station_id">@lang('app.Work_Station_ID')</label>
                                        <input type="text" id="work_station_id" class="form-control"
                                            name="work_station_id" placeholder="@lang('app.Work_Station_ID')" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="employment_status ">@lang('app.Employment_Status') </label>
                                        <select name="employment_status" id="employment_status"
                                            class="select2 form-select" data-placeholder="@lang('app.Select_Employment_Status')">
                                            <option value=""></option>
                                            @foreach ($employmentstatus as $status)
                                                <option value="{{ $status->id }}">{{ $status->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="job_title ">@lang('app.Job_Titles')</label>
                                        <select name="job_title" id="job_title" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Job_Title')">
                                            <option value=""></option>
                                            @foreach ($jobtitles as $jobtitle)
                                                <option value="{{ $jobtitle->id }}">{{ $jobtitle->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="pay_grade">@lang('app.Pay_Grades')</label>
                                        <select name="pay_grade" id="pay_grade" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_a_Pay_Grade')">
                                            <option value=""></option>
                                            @foreach ($paygrades as $paygrade)
                                                <option value="{{ $paygrade->id }}">{{ $paygrade->name }}
                                                    ({{ $paygrade->min_salary }}-{{ $paygrade->max_salary }})
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if (env('COMPANY') == 'HGNHRM')
                                    <div class="row">
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="position_effective_date">@lang('app.Position_Effective_Date')
                                                </label>
                                            <input type="text" id="position_effective_date"
                                                class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                name="position_effective_date" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="tax_status_description">@lang('app.Tax_Status_Description')
                                                </label>
                                            <input type="text" id="tax_status_description" class="form-control"
                                                placeholder="@lang('app.Tax_Status_Description')" name="tax_status_description" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="eefunction_description">eefunction Description
                                                </label>
                                            <input type="text" id="eefunction_description" class="form-control"
                                                placeholder="eefunction Description" name="eefunction_description" />
                                        </div>

                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="position_filled_since">@lang('app.Position_Filled_Since')
                                                </label>
                                            <input type="text" id="position_filled_since"
                                                class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                name="position_filled_since" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="coida_reference_number">@lang('app.Coida_Reference_Number')
                                                </label>
                                            <input type="text" id="coida_reference_number" class="form-control"
                                                placeholder="@lang('app.Coida_Reference_Number')" name="coida_reference_number" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="uif_exemption_reason">@lang('app.UIF_Exemption_Reason')
                                                </label>
                                            <input type="text" id="uif_exemption_reason" class="form-control"
                                                placeholder="@lang('app.UIF_Exemption_Reason')" name="uif_exemption_reason" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="sdl_exemption_reason">@lang('app.SDL_Exemption_Reason')
                                                </label>
                                            <input type="text" id="sdl_exemption_reason" class="form-control"
                                                placeholder="@lang('app.SDL_Exemption_Reason')" name="sdl_exemption_reason" />
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="not_re_employable">@lang('app.Not_RE_Employable')</label>
                                            <select name="not_re_employable" id="not_re_employable" class="form-select">
                                                <option value="no">@lang('app.No')</option>
                                                <option value="yes">@lang('app.Yes')</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="not_qualify_employment_tax">@lang('app.Does_Not_Qualify_For_Employment_Tax_Incentive')
                                                </label>
                                            <select name="not_qualify_employment_tax" id="not_qualify_employment_tax"
                                                class="form-select">
                                                <option value="no">@lang('app.No')</option>
                                                <option value="yes">@lang('app.Yes')</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="director_for_coida">@lang('app.Director_For_Coida')</label>
                                            <select name="director_for_coida" id="director_for_coida"
                                                class="form-select">
                                                <option value="false">@lang('app.False')</option>
                                                <option value="true">@lang('app.True')</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="exclude_for_coida">@lang('app.Exclude_For_Coida')</label>
                                            <select name="exclude_for_coida" id="exclude_for_coida" class="form-select">
                                                <option value="false">@lang('app.False')</option>
                                                <option value="true">@lang('app.True')</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="exclude_from_eea_report">@lang('app.Exclude_From_EEA_Report')
                                                </label>
                                            <select name="exclude_from_eea_report" id="exclude_from_eea_report"
                                                class="form-select">
                                                <option value="false">@lang('app.False')</option>
                                                <option value="true">@lang('app.True')</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-12">
                                            <label class="form-label" for="exclude_from_uif_submission_file">@lang('app.Exclude_From_UIF_Submission_File')
                                                </label>
                                            <select name="exclude_from_uif_submission_file"
                                                id="exclude_from_uif_submission_file" class="form-select">
                                                <option value="false">@lang('app.False')</option>
                                                <option value="true">@lang('app.True')</option>
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="department">@lang('app.Department')</label>
                                        <select name="department" id="department" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Department')">
                                            <option value=""></option>
                                            @foreach (departments() as $companystructure)
                                                <option value="{{ $companystructure->id }}">
                                                    {{ $companystructure->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="supervisor ">@lang('app.Supervisor')</label>
                                        <select name="supervisor" id="supervisor" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Supervisor')">
                                            <option value=""></option>
                                            @foreach (employees() as $employee)
                                                <option value="{{ $employee->id }}">
                                                    {{ $employee->first_name . ' ' . $employee->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="indirect_supervisors">@lang('app.Indirect_Supervisors')</label>
                                        <select name="indirect_supervisors" id="indirect_supervisors"
                                            class="select2 form-select" data-placeholder="@lang('app.Select_Indirect_Supervisors')">
                                            <option value=""></option>
                                            @foreach (employees() as $employee)
                                                <option value="{{ $employee->id }}">
                                                    {{ $employee->first_name . ' ' . $employee->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="approver1">@lang('app.First_Level_Approver')</label>
                                        <select name="approver1" id="approver1" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_First_Level_Approver')">
                                            <option value=""></option>
                                            @foreach (employees() as $employee)
                                                <option value="{{ $employee->id }}">
                                                    {{ $employee->first_name . ' ' . $employee->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="approver2">@lang('app.Second_Level_Approver')</label>
                                        <select name="approver2" id="approver2" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Second_Level_Approver')">
                                            <option value=""></option>
                                            @foreach (employees() as $employee)
                                                <option value="{{ $employee->id }}">
                                                    {{ $employee->first_name . ' ' . $employee->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="approver3">@lang('app.Third_Level_Approver')</label>
                                        <select name="approver3" id="approver3" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Third_Level_Approver')">
                                            <option value=""></option>
                                            @foreach (employees() as $employee)
                                                <option value="{{ $employee->id }}">
                                                    {{ $employee->first_name . ' ' . $employee->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="passcode">@lang('app.PassCode')</label>
                                        <input type="text" id="passcode" class="form-control" name="passcode"
                                            placeholder="@lang('app.PassCode')" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="posting_station">@lang('app.Posting_Station')</label>
                                        <select name="posting_station" id="posting_station" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Posting_Station')">
                                            <option value=""></option>
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if (env('COMPANY') == 'HGNHRM')
                                    <div class="row">
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="account_type">@lang('app.Account_Type')</label>
                                            <input type="text" id="account_type" class="form-control"
                                                placeholder="@lang('app.Account_Type')" name="account_type" />
                                        </div>
                                        <div class="mb-1 col-6">
                                            <label class="form-label" for="branch_code">@lang('app.Branch_Code')</label>
                                            <input type="text" id="branch_code" class="form-control"
                                                placeholder="@lang('app.Branch_Code')" name="branch_code" />
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="bank_id">@lang('app.Bank')</label>
                                        <select name="bank_id" id="bank_id" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Banks')">
                                            <option value=""></option>
                                            @foreach ($banks as $bank)
                                                <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="account_title">@lang('app.Bank_Account_Title')</label>
                                        <input type="text" id="account_title" class="form-control"
                                            name="account_title" placeholder="@lang('app.Bank_Account_Title')" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="account_number">@lang('app.Bank_Account_Number')</label>
                                        <input type="text" id="account_number" class="form-control"
                                            name="account_number" placeholder="@lang('app.Bank_Account_Number')" />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="covid_vaccinated">@lang('app.Covid_Vaccinated')</label>
                                        <select name="covid_vaccinated" id="covid_vaccinated" class="form-select">
                                            <option value="no">@lang('app.No')</option>
                                            <option value="yes">@lang('app.Yes')</option>
                                        </select>
                                    </div>
                                </div>
                                @if (env('COMPANY') == 'CLINIX')
                                    <div class="row">
                                        <div class="mb-1 col-md-12">
                                            <label class="form-label" for="ppc_reg_no">@lang('app.PPC_Registeration_Number')</label>
                                            <input type="text" id="ppc_reg_no" class="form-control" name="ppc_reg_no"
                                                placeholder="@lang('app.PPC_Registeration_Number')" />
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    @if (env('COMPANY') != 'HGNHRM')
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="provident_fund">@lang('app.Provident_Fund')</label>
                                            <select name="provident_fund" data-placeholder="@lang('app.Provident_Fund')"
                                                id="provident_fund" class="form-select select2">
                                                <option value="no">@lang('app.No')</option>
                                                <option value="yes">@lang('app.Yes')</option>
                                            </select>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label class="form-label" for="eobi">EOBI</label>
                                            <select name="eobi_status" id="eobi" data-placeholder="@lang('app.Select_EOBI')"
                                                class="form-select select2">
                                                <option value="no">@lang('app.No')</option>
                                                <option value="yes">@lang('app.Yes')</option>
                                            </select>
                                        </div>
                                    @else
                                        <div class="mb-1 col-md-12">
                                            <label class="form-label" for="provident_fund">@lang('app.Provident_Fund')</label>
                                            <select name="provident_fund" data-placeholder="@lang('app.Provident_Fund')"
                                                id="provident_fund" class="form-select select2">
                                                <option value="no">@lang('app.No')</option>
                                                <option value="yes">@lang('app.Yes')</option>
                                            </select>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-12 mb-1">
                                        <label class="form-label" for="pessi">PESSI</label>
                                        <select name="pessi" id="pessi" data-placeholder="@lang('app.Select_PESSI')"
                                            class="form-select select2">
                                            <option value="no">@lang('app.No')</option>
                                            <option value="yes">@lang('app.Yes')</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">@lang('app.Previous')</span>
                                </button>
                                <button type="button" class="btn btn-primary btn-next">
                                    <span class="align-middle d-sm-inline-block d-none">@lang('app.Next')</span>
                                    <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                </button>
                            </div>
                        </div>
                        @can('Employee Leave Management')
                            <div id="leaves" class="content" role="tabpanel" aria-labelledby="leaves-trigger">
                                <div class="content-header">
                                    <h5 class="mb-0">@lang('app.Leave_Management')</h5>
                                    <small>@lang('app.See_Leaves_Details').</small>
                                    <br>
                                    <code class="text-danger">@lang('app.Dont')</code>
                                </div>
                                <form>
                                    <div class="row">
                                        <div class="mb-1 col-12">
                                            <label class="form-label" for="leave_groups">@lang('app.Leave_Group')</label>
                                            <select name="leave_groups[]" id="leave_groups" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Groups')" multiple>
                                                <option value=""></option>
                                                @foreach ($leaveGroups as $leaveGroup)
                                                    <option value="{{ $leaveGroup->id }}">{{ $leaveGroup->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-1 col-12">
                                            <p id="leaveCalculationText" class="text-info"></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <table class="table table-striped" id="leaves_table">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr.No</th>
                                                            <th>Leave Type</th>
                                                            <th>Allowed</th>
                                                            <th>Availed</th>
                                                            <th>Remaining</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button type="button" class="btn btn-primary btn-prev">
                                        <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                        <span class="align-middle d-sm-inline-block d-none">@lang('app.Previous')</span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-next">
                                        <span class="align-middle d-sm-inline-block d-none">@lang('app.Next')</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </div>
                            </div>
                        @endcan
                        @can('Employee CG Management')
                            <div id="calculation-group" class="content" role="tabpanel"
                                aria-labelledby="calculation-group-trigger">
                                <div class="content-header">
                                    <h5 class="mb-0">@lang('app.Calculation_Group_Management')</h5>
                                    <small>@lang('app.Manage_Calculation_Group')</small>
                                </div>
                                <form>
                                    <div class="add_groups" id="add_groups">
                                        <div data-repeater-list="calculationGroups">
                                            <div data-repeater-item>
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="pay_frequency">@lang('app.Pay_Frequency')
                                                                </label>
                                                            <select name="pay_frequency" id="pay_frequency"
                                                                class="select2 form-select"
                                                                data-placeholder="@lang('app.Select_Pay_Frequency')" required>
                                                                <option value=""></option>
                                                                @foreach ($payFrequency as $frequency)
                                                                    <option value="{{ $frequency->id }}">
                                                                        {{ $frequency->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="currency">@lang('app.Currency')</label>
                                                            <select name="currency" id="currency"
                                                                class="select2 form-select"
                                                                data-placeholder="@lang('app.Select_Currency')" required>
                                                                <option value=""></option>
                                                                @foreach ($currencies as $currency)
                                                                    <option value="{{ $currency->id }}">
                                                                        {{ $currency->code }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="calculation_group">@lang('app.Calculation_Group')
                                                                </label>
                                                            <select name="calculation_group" id="calculation_group"
                                                                class="select2 form-select"
                                                                data-placeholder="@lang('app.Select_Calculation_Group')" required>
                                                                <option value=""></option>
                                                                @foreach ($calculationGroup as $group)
                                                                    <option value="{{ $group->id }}">{{ $group->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="deduction_exemptions">@lang('app.Calculation_Exemptions')
                                                                </label>
                                                            <select name="deduction_exemptions" id="deduction_exemptions"
                                                                class="select2 form-select"
                                                                data-placeholder="@lang('app.Select_Calculation_Exemptions')">
                                                                <option value=""></option>
                                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="deduction_allowed">@lang('app.Calculation_Assigned')
                                                                </label>
                                                            <select name="deduction_allowed" id="deduction_allowed"
                                                                class="select2 form-select"
                                                                data-placeholder="@lang('app.Select_Calculation_Assigned')">
                                                                <option value=""></option>
                                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-12 mb-51">
                                                        <div class="mb-1">
                                                            <button class="btn btn-outline-danger text-nowrap px-1"
                                                                data-repeater-delete type="button">
                                                                <i data-feather="x" class="me-25"></i>
                                                                <span>@lang('app.Delete')</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-12">
                                                <button class="btn btn-icon btn-info" type="button" data-repeater-create>
                                                    <i data-feather="plus" class="me-25"></i>
                                                    <span>@lang('app.Add_New')</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button type="button" class="btn btn-primary btn-prev">
                                        <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                        <span class="align-middle d-sm-inline-block d-none">@lang('app.Previous')</span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-next">
                                        <span class="align-middle d-sm-inline-block d-none">@lang('app.Next')</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </div>
                            </div>
                        @endcan
                        @can('Employee Salary Management')
                            <div id="salary-setup" class="content" role="tabpanel"
                                aria-labelledby="salary-setup-trigger">
                                <div class="content-header">
                                    <h5 class="mb-0">@lang('app.Salary_Setup')</h5>
                                    <small>@lang('app.Manage_Salary_Components')</small>
                                </div>
                                <form>
                                    <div class="row">
                                        <hr>
                                        @foreach ($SalaryComponentTypes as $SalaryComponentType)
                                            @if (isset($salary_components[$SalaryComponentType->id]))
                                                <div class="col-12">
                                                    <h4>{{ $SalaryComponentType->name }}</h4>
                                                    <div class="mb-1">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Salary Component</th>
                                                                    <th>Amount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($salary_components[$SalaryComponentType->id] as $component)
                                                                    <tr>
                                                                        <td>{{ $component->name }}</td>
                                                                        <td>
                                                                            <input type="hidden" name="components[]"
                                                                                value="{{ $component->id }}">
                                                                            <input type="text" class="form-control"
                                                                                name="sc_amount[{{ $component->id }}]"
                                                                                value="0.00">
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endif
                                        @endforeach
                                        <h4>Monthly Components</h4>
                                        <div class="mb-1">
                                            <label class="form-label" for="month">Month</label>
                                            <input type="month" name="applied_month" id="month"
                                                class="form-control">
                                        </div>
                                        <hr>
                                        @foreach ($SalaryComponentTypes as $SalaryComponentType)
                                            @if (isset($monthly_salary_components[$SalaryComponentType->id]))
                                                <div class="col-12">
                                                    <h4>{{ $SalaryComponentType->name }}</h4>
                                                    <div class="mb-1">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Salary Component</th>
                                                                    <th>Amount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($monthly_salary_components[$SalaryComponentType->id] as $component)
                                                                    <tr>
                                                                        <td>{{ $component->name }}</td>
                                                                        <td>
                                                                            <input type="hidden"
                                                                                name="monthly_components[]"
                                                                                value="{{ $component->id }}">
                                                                            <input type="text" class="form-control"
                                                                                name="monthly_sc_amount[{{ $component->id }}]"
                                                                                value="0.00">
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endif
                                        @endforeach
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button type="button" class="btn btn-primary btn-prev">
                                        <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                        <span class="align-middle d-sm-inline-block d-none">@lang('app.Previous')</span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-next">
                                        <span class="align-middle d-sm-inline-block d-none">@lang('app.Next')</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </div>
                            </div>
                        @endcan
                        <div id="shift" class="content" role="tabpanel" aria-labelledby="shift-trigger">
                            @can('Employee Shift Management')
                                <div class="content-header">
                                    <h5 class="mb-0">@lang('app.Shift_Management')</h5>
                                    <small>@lang('app.Manage_Shift')</small>
                                </div>
                                <form>
                                    <div class="row">
                                        <div class="mb-1 col-12">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>@lang('app.Shift_Type')</th>
                                                            <th>@lang('app.Work_Week')</th>
                                                            <th>@lang('app.Shift_From')</th>
                                                            <th>@lang('app.Shift_To')</th>
                                                            <th>@lang('app.Late_Time_Allowed')</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <select name="shift_id" id="shift"
                                                                    class=" form-select">
                                                                    <option value="">@lang('app.Select_Shift_Type')</option>
                                                                    @foreach ($shift_types as $shift)
                                                                        <option value="{{ $shift->id }}">
                                                                            {{ $shift->shift_desc }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select name="work_week_id" id="work_week"
                                                                    class="form-select">
                                                                    <option value="">@lang('app.Select_Work_Week')</option>
                                                                    @foreach ($work_weeks as $week)
                                                                        <option value="{{ $week->id }}">
                                                                            {{ $week->desc }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="date" name="shift_from_date"
                                                                    class="form-control">
                                                            </td>
                                                            <td>
                                                                <input type="date" name="shift_to_date"
                                                                    class="form-control">
                                                            </td>
                                                            @if (env('COMPANY') == 'JSML' || env('COMPANY') == 'AJMAL' || env('COMPANY') == 'RoofLine')
                                                                <td>
                                                                    <input type="text" id="late_time_allowed"
                                                                        class="form-control" value="15"
                                                                        name="late_time_allowed" readonly />
                                                                </td>
                                                            @else
                                                                <td>
                                                                    <input type="text" id="late_time_allowed"
                                                                        class="form-control" value="11"
                                                                        name="late_time_allowed" readonly />
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endcan
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-primary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">@lang('app.Previous')</span>
                                </button>
                                <button class="form_save btn btn-success btn-submit">@lang('app.Submit')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            new Cleave($('#nic_num'), {
                delimiter: '-',
                blocks: [5, 7, 1],
                numericOnly: true
            });

            new Cleave($('#father_cnic'), {
                delimiter: '-',
                blocks: [5, 7, 1],
                numericOnly: true
            });

            var bsStepper = document.querySelectorAll('.bs-stepper'),
                horizontalWizard = document.querySelector('.employee-horizontal-wizard');
            // Adds crossed class
            if (typeof bsStepper !== undefined && bsStepper !== null) {
                for (var el = 0; el < bsStepper.length; ++el) {
                    bsStepper[el].addEventListener('show.bs-stepper', function(event) {
                        var index = event.detail.indexStep;
                        var numberOfSteps = $(event.target).find('.step').length - 1;
                        var line = $(event.target).find('.step');

                        // The first for loop is for increasing the steps,
                        // the second is for turning them off when going back
                        // and the third with the if statement because the last line
                        // can't seem to turn off when I press the first item. ¯\_(ツ)_/¯

                        for (var i = 0; i < index; i++) {
                            line[i].classList.add('crossed');

                            for (var j = index; j < numberOfSteps; j++) {
                                line[j].classList.remove('crossed');
                            }
                        }
                        if (event.detail.to == 0) {
                            for (var k = index; k < numberOfSteps; k++) {
                                line[k].classList.remove('crossed');
                            }
                            line[0].classList.remove('crossed');
                        }
                    });
                }
            }

            if (typeof horizontalWizard !== undefined && horizontalWizard !== null) {
                var numberedStepper = new Stepper(horizontalWizard, {
                    linear: false,
                });
                $form = $(horizontalWizard).find('form');
                $form.each(function() {
                    var $this = $(this);
                    if ("{! env('COMPANY') == 'CIDEX' !}") {
                        $this.validate({
                            rules: {
                                employee_id: {
                                    required: true
                                },
                                'first_name': {
                                    required: true
                                },
                                // 'last_name': { required: true},
                                // 'father_name': { required: true},
                            }
                        });
                    } else {
                        $this.validate({
                            rules: {
                                employee_id: {
                                    required: true
                                },
                                // employee_code: { required: true},
                                'first_name': {
                                    required: true
                                },
                                // 'last_name': { required: true},
                                // 'father_name': { required: true},
                                // birthday : { required: true},
                                // province: { required: true},
                                // postal_code: { required: true},
                                // city: { required: true},
                                // address1: { required: true},
                                // mobile_phone: { required: true},
                                // private_email: { required: true},
                                // joined_date: { required: true},
                                employment_status: {
                                    required: true
                                },
                                job_title: {
                                    required: true
                                },
                                department: {
                                    required: true
                                },
                                // bank_id: { required: true},
                                // account_title: { required: true},
                                // account_number: { required: true},
                            }
                        });
                    }
                });

                $(horizontalWizard)
                    .find('.btn-next')
                    .each(function() {
                        $(this).on('click', function(e) {
                            var isValid = $(this).parent().siblings('form').valid();
                            if (isValid) {
                                numberedStepper.next();
                            } else {
                                e.preventDefault();
                            }
                        });
                    });

                $(horizontalWizard)
                    .find('.btn-prev')
                    .on('click', function() {
                        numberedStepper.previous();
                    });

                $(horizontalWizard)
                    .find('.btn-submit')
                    .on('click', function(e) {
                        e.preventDefault();
                        var isValid = $(this).parent().siblings('form').valid();
                        if (isValid) {
                            ButtonStatus('.form_save', true);
                            blockUI();
                            var form = $('form').serialize();
                            $.ajax({
                                url: "{{ route('employees.store') }}",
                                type: "post",
                                data: form,
                                success: function(response) {
                                    ButtonStatus('.form_save', false);
                                    $.unblockUI();
                                    if (response.code == 200) {
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Employee has been Added Successfully!'
                                        });
                                        setTimeout(function() {
                                            if (1 == 1) {

                                                window.location.href =
                                                    "{{ url('getEmployees') }}";
                                            } else {

                                                window.location.href =
                                                    "{{ url('employees') }}";
                                            }
                                        }, 2500);
                                    } else if (response.errors) {
                                        $.each(response.errors, function(index, value) {
                                            Toast.fire({
                                                icon: 'error',
                                                title: value
                                            })
                                        });
                                    } else if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: response.error_message
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                }
                            });
                        }
                    });
            }
            var calculation_process = $('#add_groups');
            $('.add_groups').repeater({
                isFirstItemUndeletable: false,
                initEmpty: true,
                show: function() {
                    $(this).slideDown();
                    calculation_process.find('select').next('.select2-container').remove();
                    calculation_process.find('select').select2();
                    // Feather Icons
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                }
            });
        });
        var isProgrammaticChange = false; // Flag to track programmatic changes

        $("#leave_groups").on("change", function() {
            if (!isProgrammaticChange) {
                calculateLeaves();
            }
        });

        function calculateLeaves() {
            blockUI();
            var joining_date = $('#joined_date').val();
            if (joining_date == null || joining_date == '') {
                // Set the flag to true before making programmatic changes
                isProgrammaticChange = true;
                $("#leave_groups > option").prop("selected", false);
                // Trigger the change event without causing a loop
                $("#leave_groups").trigger("change");
                // Set the flag back to false
                isProgrammaticChange = false;
                Swal.fire({
                    icon: 'error',
                    title: 'Please Select Joining Date First!',
                });
                return false;
            }
            var leave_groups = $('#leave_groups').val();
            $('#leaveCalculationText').text("As Per Selected Leave Groups And Selected Joining Date (" + joining_date +
                "), Below Is the table Representing Approximate Leaves Information");
            $.ajax({
                url: "{{ url('new-emp-leave-balance') }}",
                method: 'GET',
                data: {
                    leave_groups: leave_groups,
                    calculation_date: joining_date
                },
                success: function(response) {
                    $.unblockUI();
                    if ($('input[name="new_leave_settings"]').length === 0) {
                        var hiddenInput = $('<input>').attr('type', 'hidden').attr('name', 'new_leave_settings')
                            .val(JSON.stringify(response));
                        $('form').append(hiddenInput);
                    } else {
                        $('input[name="new_leave_settings"]').val(JSON.stringify(response));
                    }
                    var tableBody = $('#leaves_table tbody');
                    tableBody.empty();
                    for (var i = 0; i < response.length; i++) {
                        var leave = response[i];
                        var row = '<tr>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td>' + leave.leaveType + '</td>' +
                            '<td>' + leave.allowed + '</td>' +
                            '<td>' + leave.availed + '</td>' +
                            '<td>' + leave.remaining + '</td>' +
                            '</tr>';
                        tableBody.append(row);
                    }
                },
                error: function(error) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Error On Fetching Leaves!',
                    });
                }
            });
        }
    </script>
@endsection
