@extends('Admin.layouts.master')

@section('title', 'Employees')
@section('content')

    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Employees')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Employees')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.List')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="active_employee-tab" data-bs-toggle="tab" href="#active_employee"
                            aria-controls="active_employee" role="tab" aria-selected="true">@lang('app.Employees')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="suspend-tab" data-bs-toggle="tab" href="#suspend"
                            aria-controls="suspend" role="tab" aria-selected="false"
                            onclick="suspend_dataTable()">@lang('app.Suspended')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="terminate-tab" data-bs-toggle="tab" href="#terminate"
                            aria-controls="terminate" role="tab" aria-selected="false"
                            onclick="terminated_dataTable()">@lang('app.Terminated')</a>
                    </li>
                    @if (env('COMPANY') == 'JSML')    
                        <li class="nav-item">
                            <a class="nav-link" id="off_roll-tab" data-bs-toggle="tab" href="#off_roll"
                                aria-controls="off_roll" role="tab" aria-selected="false"
                                onclick="off_roll_dataTable()">@lang('app.OFF_Roll_Employees')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="register_emp-tab" data-bs-toggle="tab" href="#reg_emp"
                                aria-controls="reg_emp" role="tab" aria-selected="false"
                                onclick="registered_datatable()">@lang('app.Registered_Employees')</a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="active_employee" aria-labelledby="active_employee-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="employee_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>ID</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th class="not_include">First Name</th>
                                                    <th class="not_include">Last Name</th>
                                                    <th>Job Title</th>
                                                    @if (env('COMPANY') == 'CLINIX')
                                                        <th class="not_include">Bank Name/ Account Title/ Account Number</th>
                                                        <th>Bank Name</th>
                                                        <th>Account Title</th>
                                                        <th>Account Number</th>
                                                    @elseif (env('COMPANY') != 'Ajmal Dawakhana')
                                                    @if (env('COMPANY') == 'JSML')
                                                        <th>Department</th>
                                                    @else    
                                                        <th>Parent Department</th>
                                                    @endif
                                                    @endif
                                                    @if (env('COMPANY') == 'Ajmal Dawakhana')
                                                    <th>DEPT./Sub Unit/Branch</th>
                                                    @elseif(env('COMPANY') == 'JSML')
                                                    <th>Section</th>
                                                    @else
                                                    <th>Sub Unit Branch</th>
                                                    @endif
                                                    @if (env('COMPANY') == 'CLINIX')
                                                        <th>Type</th>
                                                        <th>D.O.J.</th>
                                                        <th>Conf. Date</th>
                                                        <th>Active/In-Active</th>
                                                    @endif
                                                    @if (env('COMPANY') == 'HGNHRM')
                                                    <th>ID Number</th>
                                                    @endif
                                                    @if (env('COMPANY') != 'HGNHRM')
                                                    <th>CNIC</th>
                                                    <th>CNIC EXP Date</th>
                                                    @endif
                                                    <th>Employment Status</th>
                                                    @if (env('COMPANY') != 'HGNHRM')
                                                    <th>City</th>
                                                    @endif
                                                    {{-- <th>CNIC Exp Date</th> --}}
                                                    <th>Mobile Phone</th>
                                                    @if (env('COMPANY') == 'CLINIX' || env('COMPANY') == 'HEALTHWISE')    
                                                        <th>Blood Group</th>
                                                        <th>PPC Registeration Number</th>
                                                    @endif
                                                    <th>Father Name</th>
                                                    <th>Father CNIC</th>
                                                    @if (env('COMPANY') == 'CLINIX')
                                                        <th>Parent Department</th>
                                                    @else
                                                        <th>Bank Name</th>
                                                        <th>Account Title</th>
                                                        <th>Account Number</th>
                                                    @endif
                                                    @if(env('COMPANY') == 'JSML')
                                                        @can('Employee Gross Salary')
                                                            <th>Gross Salary</th>
                                                        @endcan
                                                    @endif
                                                    <th>SSN</th>
                                                    <th>Driving Liscence No</th>
                                                    <th>Other ID</th>
                                                    <th>Date Of Birth</th>
                                                    <th>Nationality</th>
                                                    <th>Gender</th>
                                                    <th>Marital Status</th>
                                                    <th>EOBI</th>
                                                    <th>Ethnicity</th>
                                                    <th>Country</th>
                                                    <th>Province</th>
                                                    <th>Postal/Zip Code</th>
                                                    <th>Address 1</th>
                                                    <th>Address 2</th>
                                                    <th>Home Phone</th>
                                                    <th>Work Phone</th>
                                                    <th>Work Email</th>
                                                    <th>Private Email</th>
                                                    @if (env('COMPANY') != 'CLINIX')
                                                    <th>joined Date</th>
                                                    <th>Confirmation Date</th>
                                                    @endif
                                                    <th>Work Station ID</th>
                                                    <th>Pass Code</th>
                                                    <th>Covid Vaccinated</th>
                                                    <th>Provident Fund</th>
                                                    <th>EOBI</th>
                                                    <th>PESSI</th>
                                                    {{-- <th>Supervisor</th> --}}
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Suspended Employees --}}
                    <div class="tab-pane" id="suspend" aria-labelledby="suspend-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="suspend_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>ID</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th class="not_include">First Name</th>
                                                    <th class="not_include">Last Name</th>
                                                    <th>Job Title</th>
                                                    @if (env('COMPANY') != 'Ajmal Dawakhana')
                                                    <th>Parent Department</th>
                                                    @endif
                                                    @if (env('COMPANY') == 'Ajmal Dawakhana')
                                                    <th>DEPT./Sub Unit/Branch</th>
                                                    @else
                                                    <th>Sub Unit Branch</th>
                                                    @endif
                                                    @if (env('COMPANY') == 'HGNHRM')
                                                    <th>ID Number</th>
                                                    @else
                                                    <th>CNIC</th>
                                                    <th>CNIC EXP Date</th>
                                                    @endif
                                                    <th>Employment Status</th>
                                                    @if (env('COMPANY') != 'HGNHRM')
                                                    <th>City</th>
                                                    @endif
                                                    {{-- <th>CNIC Exp Date</th> --}}
                                                    <th>Mobile Phone</th>
                                                    <th>Father Name</th>
                                                    <th>Father CNIC</th>
                                                    <th>Bank Name</th>
                                                    <th>Account Title</th>
                                                    <th>Account Number</th>
                                                    @if(env('COMPANY') == 'JSML')
                                                        @can('Employee Gross Salary')
                                                            <th>Gross Salary</th>
                                                        @endcan
                                                    @endif
                                                    <th>SSN</th>
                                                    <th>Driving Liscence No</th>
                                                    <th>Other ID</th>
                                                    <th>Date Of Birth</th>
                                                    <th>Nationality</th>
                                                    <th>Gender</th>
                                                    <th>Marital Status</th>
                                                    <th>EOBI</th>
                                                    <th>Ethnicity</th>
                                                    <th>Country</th>
                                                    <th>Province</th>
                                                    <th>Postal/Zip Code</th>
                                                    <th>Address 1</th>
                                                    <th>Address 2</th>
                                                    <th>Home Phone</th>
                                                    <th>Work Phone</th>
                                                    <th>Work Email</th>
                                                    <th>Private Email</th>
                                                    <th>joined Date</th>
                                                    <th>Confirmation Date</th>
                                                    <th>Work Station ID</th>
                                                    <th>Pass Code</th>
                                                    <th>Covid Vaccinated</th>
                                                    <th>Provident Fund</th>
                                                    <th>EOBI</th>
                                                    <th>PESSI</th>
                                                    {{-- <th>Supervisor</th> --}}
                                                    <th>Suspension Date</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Terminate Employees --}}
                    <div class="tab-pane" id="terminate" aria-labelledby="terminate-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="terminate_table">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>ID</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th class="not_include">First Name</th>
                                                    <th class="not_include">Last Name</th>
                                                    <th>Job Title</th>
                                                    <th>Parent Department</th>
                                                    @if (env('COMPANY') == 'Ajmal Dawakhana')
                                                    <th>DEPT./Sub Unit/Branch</th>
                                                    @else
                                                    <th>Sub Unit Branch</th>
                                                    @endif
                                                    @if (env('COMPANY') == 'HGNHRM')
                                                    <th>ID Number</th>
                                                    @endif
                                                    @if (env('COMPANY') != 'HGNHRM')
                                                    <th>CNIC</th>
                                                    <th>CNIC EXP Date</th>
                                                    @endif
                                                    <th>Employment Status</th>
                                                    @if (env('COMPANY') != 'HGNHRM')
                                                    <th>City</th>
                                                    @endif
                                                    {{-- <th>CNIC Exp Date</th> --}}
                                                    <th>Mobile Phone</th>
                                                    <th>Father Name</th>
                                                    <th>Father CNIC</th>
                                                    <th>Bank Name</th>
                                                    <th>Account Title</th>
                                                    <th>Account Number</th>
                                                    @if(env('COMPANY') == 'JSML')
                                                        @can('Employee Gross Salary')
                                                            <th>Gross Salary</th>
                                                        @endcan
                                                    @endif
                                                    <th>SSN</th>
                                                    <th>Driving Liscence No</th>
                                                    <th>Other ID</th>
                                                    <th>Date Of Birth</th>
                                                    <th>Nationality</th>
                                                    <th>Gender</th>
                                                    <th>Marital Status</th>
                                                    <th>EOBI</th>
                                                    <th>Ethnicity</th>
                                                    <th>Country</th>
                                                    <th>Province</th>
                                                    <th>Postal/Zip Code</th>
                                                    <th>Address 1</th>
                                                    <th>Address 2</th>
                                                    <th>Home Phone</th>
                                                    <th>Work Phone</th>
                                                    <th>Work Email</th>
                                                    <th>Private Email</th>
                                                    <th>joined Date</th>
                                                    <th>Confirmation Date</th>
                                                    <th>Work Station ID</th>
                                                    <th>Pass Code</th>
                                                    <th>Covid Vaccinated</th>
                                                    <th>Provident Fund</th>
                                                    <th>EOBI</th>
                                                    <th>PESSI</th>
                                                    {{-- <th>Supervisor</th> --}}
                                                    <th>Termination Date</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Off Roll Emoployee --}}
                    <div class="tab-pane" id="off_roll" aria-labelledby="off_roll-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="off_roll_table">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>ID</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Job Title</th>
                                                    <th>Employment Status</th>
                                                    <th>Department/Branch</th>
                                                    <th>City</th>
                                                    <th>Mobile Phone</th>
                                                    <th>CNIC</th>
                                                    {{-- <th>CNIC Exp Date</th> --}}
                                                    <th>Bank Name</th>
                                                    <th>Account Title</th>
                                                    <th>Account Number</th>
                                                    {{-- <th>Supervisor</th> --}}
                                                    @if(env('COMPANY') == 'JSML')
                                                        @can('Employee Gross Salary')
                                                            <th>Gross Salary</th>
                                                        @endcan
                                                    @endif
                                                    <th>Off Roll Date</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Registered Emoployee --}}

                    <div class="tab-pane" id="reg_emp" aria-labelledby="register_emp-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="reg_table">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>ID</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Job Title</th>
                                                    <th>Employment Status</th>
                                                    <th>Department/Branch</th>
                                                    <th>City</th>
                                                    <th>Mobile Phone</th>
                                                    <th>CNIC</th>
                                                    {{-- <th>CNIC Exp Date</th> --}}
                                                    <th>Bank Name</th>
                                                    <th>Account Title</th>
                                                    <th>Account Number</th>
                                                    {{-- <th>Supervisor</th> --}}
                                                    @if(env('COMPANY') == 'JSML')
                                                        @can('Employee Gross Salary')
                                                            <th>Gross Salary</th>
                                                        @endcan
                                                    @endif
                                                    <th>Off Roll Date</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--Suspend Employee Modal -->
                    <div class="modal fade text-start" id="suspend_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Suspend_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="suspend_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="suspend_date">@lang('app.Suspension_Date')</label>
                                                    <input type="text" id="suspend_date"
                                                        class="form-control flatpickr-basic" name="suspension_date"
                                                        placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Suspend Modal -->


                    <!--Active In-active Modal -->
                    <div class="modal fade text-start" id="act_inact_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Active/In-active_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="act_inact_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="act_employee_id" value="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    {{-- <label class="form-label">Active:</label> --}}
                                                    <select name="active_inactive" id="active_inactive" class="select2 form-select" data-placeholder="@lang('app.Select_Status')">
                                                        <option value=""></option>
                                                        <option value="1">@lang('app.Active')</option>
                                                        <option value="0">@lang('app.In_Active')</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Active In-active Modal -->

                    <!--Terminate Employee Modal -->
                    <div class="modal fade text-start" id="terminate_modal" data-bs-focus="false" tabindex="-1" aria-labelledby="myModalLabel17"
                        aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Terminate_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="terminate_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row justify-content-between">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Employee')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                    <input type="text" class="form-control" id="name" style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Employee_ID')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                    <input type="text" class="form-control" id="ID" style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.CNIC')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                    <input type="text" class="form-control" id="cnic" style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Address')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                    <input type="text" class="form-control" id="address" style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Employee_Status_Start_Date')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" name="employee_status_start_date" class="form-control flatpickr-basic" placeholder="Select Date" style="border: none; border-radius: 0;" placeholder="YYYY-MM-DD">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Comment')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" name="terminate_reason" class="form-control" placeholder="Type Comments" style="border: none; border-radius: 0;">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Termination_Date')
                                                        </label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                        <input type="text" name="termination_date" id="termination_date" style="border: none; border-radius: 0;" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Department')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                    <input type="text" class="form-control" id="department" style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Designation')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                    <input type="text" class="form-control" id="designation" style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Date_of_Joining')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                    <input type="text" class="form-control" id="doj" style="border: none; border-radius: 0;" readonly>
                                                    </div>
                                                </div>
                                                @if (env('COMPANY') != 'HGNHRM')
                                                    <div class="row">
                                                        <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Separation_Type')
                                                            </label>
                                                        <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                            <input type="text" name="separation_type"
                                                                class="form-control" placeholder="Enter Type"
                                                                style="border: none; border-radius: 0;">
                                                        </div>
                                                    </div>
                                                @endif
                                                @if (env('COMPANY') == 'HGNHRM')
                                                    <div class="row">

                                                        <label class=" col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Termination_Reason')
                                                            </label>
                                                        <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                            <select name="separation_type" id="separation_type"
                                                                class="form-select" style="border: none; border-radius: 0;">
                                                                <option value="">@lang('app.Select_Termination_Reason')</option>
                                                                <option value="resigned">@lang('app.Resigned')</option>
                                                                <option value="absconded">@lang('app.Absconded')</option>
                                                                <option value="retrenched">@lang('app.Retrenched')</option>
                                                                <option value="dismissed">@lang('app.Dismissed')</option>
                                                                <option value="expired">@lang('app.Contract_Expired')</option>
                                                                <option value="package">@lang('app.Voluntary_Severance_Package')
                                                                </option>
                                                                <option value="disable">@lang('app.Illness/Medically_boarded/Disabled')
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Notice_Period_Status_End_Date')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                    <input type="text" name="notice_period_status_end_date" class="form-control flatpickr-basic" placeholder="Select Date" style="border: none; border-radius: 0;" placeholder="YYYY-MM-DD">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-5 pb-2 fw-bolder col-form-label">@lang('app.Attachments')</label>
                                                    <div class="col-md-7" style="border-left: 1px solid #BCB6B5;">
                                                    <input type="file" name="termination_file" class="form-control"  style="border: none; border-radius: 0;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Terminate Modal -->

                    <!--Restore Employee Modal -->
                    <div class="modal fade text-start" id="restore_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Restore_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="restore_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="restore_date">@lang('app.Restore_Date')</label>
                                                    <input type="text" id="restore_date"
                                                        class="form-control flatpickr-basic" name="restore_date"
                                                        placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Restore Modal -->

                    {{-- Off Roll Modal --}}
                    <div class="modal fade text-start" id="off_roll_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.OFF_Roll_Employees')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="off_roll_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="restore_date">@lang('app.Off_Roll_Date')</label>
                                                    <input type="text" id="off_roll_date"
                                                        class="form-control flatpickr-basic" name="off_roll_date"
                                                        placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- End Off Roll Modal --}}

                    {{-- On Roll Modal --}}
                    <div class="modal fade text-start" id="on_roll_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.On_Roll_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="on_roll_form">
                                    @csrf
                                    <div class="modal-body">
                                        <input type="hidden" name="employee_id" id="employee_id" value="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="on_roll_date">@lang('app.On_Roll_Date')</label>
                                                    <input type="text" id="on_roll_date" class="form-control flatpickr-basic" name="on_roll_date" placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                    
                    {{-- End On Roll Modal --}}

                    {{-- Employees Filter Modal --}}
                    <div class="modal fade text-start" id="employees_filter_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Filter_Employee')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="employee_search_form">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-label">@lang('app.Department'):</label>
                                                <select name="departmentFilter" onchange="employeeFun()"
                                                    id="departmentFilter" class="select2 form-select"
                                                    data-placeholder="@lang('app.Select_Department')" required>
                                                    <option value="">@lang('app.Select_Department')</option>
                                                    @foreach (departments() as $department)
                                                        <option value="{{ $department->id }}">{{ $department->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if (env('COMPANY') == 'JSML')        
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="title">@lang('app.Section')</label>
                                                    <select name="section[]" id="section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                        
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-md-6">
                                                <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                                <select name="employeeFilter" id="employeeFilter"
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">

                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">@lang('app.City'):</label>
                                                <select name="empCityFilter" id="empCityFilter"
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_City')">
                                                    <option value="">@lang('app.Select_City')</option>
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->name }}">{{ $city->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">@lang('app.Job_Titles'):</label>
                                                <select name="empJobTitleFilter" onchange="employeeFun()"
                                                    id="empJobTitleFilter" class="select2 form-select"
                                                    data-placeholder="@lang('app.Select_Job_Title')">
                                                    <option value="">@lang('app.Select_Job_Title')</option>
                                                    @foreach ($jobtitles as $jobtitle)
                                                        <option value="{{ $jobtitle->id }}">{{ $jobtitle->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">@lang('app.Employment_Status'):</label>
                                                <select name="empStatusFilter" onchange="employeeFun()"
                                                    id="empStatusFilter" class="select2 form-select"
                                                    data-placeholder="@lang('app.Select_Employment_Status')">
                                                    <option value="">@lang('app.Select_Employment_Status')</option>
                                                    @foreach ($employmentstatus as $status)
                                                        <option value="{{ $status->id }}">{{ $status->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">@lang('app.Pay_Grade'):</label>
                                                <select name="empPayFilter" onchange="employeeFun()"
                                                    id="empPayFilter" class="select2 form-select"
                                                    data-placeholder="@lang('app.Select_a_Pay_Grade')">
                                                    <option value="">@lang('app.Select_a_Pay_Grade')</option>
                                                    @foreach ($payGrades as $payGrade)
                                                        <option value="{{ $payGrade->id }}">{{ $payGrade->min_salary }}-{{ $payGrade->max_salary }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">@lang('app.Joining_Date_From'):</label>
                                                <input type="text" name="empJoinFromDateFilter" id="empJoinFromDateFilter"
                                                    class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">@lang('app.Joining_Date_To'):</label>
                                                <input type="text" name="empJoinToDateFilter" id="empJoinToDateFilter"
                                                    class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">@lang('app.Confirmation_Date_From'):</label>
                                                <input type="text" name="empConFromDateFilter" id="empConFromDateFilter"
                                                    class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">@lang('app.Confirmation_Date_To'):</label>
                                                <input type="text" name="empConToDateFilter" id="empConToDateFilter"
                                                    class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            @if (env('COMPANY') == 'CLINIX')    
                                                <div class="col-md-12">
                                                    <label class="form-label">@lang('app.Blood_Group'):</label>
                                                    <select name="empBloodFilter" onchange="employeeFun()"
                                                        id="empBloodFilter" class="select2 form-select"
                                                        data-placeholder="@lang('app.Select_Blood_Group')">
                                                        <option value="">@lang('app.Select_Blood_Group')</option>
                                                        @foreach (departments() as $department)
                                                            <option value="{{ "AB+" }}">{{"AB+"}}</option>
                                                            <option value="{{ "AB-" }}">{{"AB-"}}</option>
                                                            <option value="{{ "A+" }}">{{"A+"}}</option>
                                                            <option value="{{ "O+" }}">{{"O+"}}</option>
                                                            <option value="{{ "O-" }}">{{"O-"}}</option>
                                                            <option value="{{ "B+" }}">{{"B+"}}</option>
                                                            <option value="{{ "B-" }}">{{"B-"}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="btn btn-primary" id="save">@lang('app.Filter')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $('#get_data').submit(function(e){
                e.preventDefault();
                $('#date').text($('#joining').val());
                $('#name').text($('#section').val());
            });
            // Javascript to enable link to tab
            var hash = location.hash.replace(/^#/, '');
            // ^ means starting, meaning only match the first hash
            if (hash) {
                if(hash=='off_roll'){
                    jQuery(function(){
                    jQuery('#off_roll-tab').click();
                });
                $("#terminate-tab").css("display", "none");
                $("#active_employee-tab").css("display", "none");
                $("#suspend-tab").css("display", "none");
                $("#register_emp-tab").css("display", "none");
                }
                else if(hash=='terminate'){
                    jQuery(function(){
                    jQuery('#terminate-tab').click();
                    });
                $("#active_employee-tab").css("display", "none");
                $("#off_roll-tab").css("display", "none");
                $("#suspend-tab").css("display", "none");
                $("#register_emp-tab").css("display", "none");

                }
                else if(hash=='active_employee'){
                    jQuery(function(){
                    jQuery('#active_employee-tab').click();
                });
                $("#terminate-tab").css("display", "none");
                $("#off_roll-tab").css("display", "none");
                $("#suspend-tab").css("display", "none");
                $("#register_emp-tab").css("display", "none");
                }
                else if(hash=='reg_emp'){
                    jQuery(function(){
                    jQuery('#register_emp-tab').click();
                });
                $("#terminate-tab").css("display", "none");
                $("#off_roll-tab").css("display", "none");
                $("#suspend-tab").css("display", "none");
                $("#active_employee-tab").css("display", "none");
                }
                $('.nav-tabs a[href="#' + hash + '"]').tab('show');
                $('.nav-tabs a[href="#' + hash + '"]').addClass('active');

            } 
            // Change hash for page-reload
            $('.nav-tabs a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            })
        });
        var datatable;
        var rowid;
        var env = <?php echo json_encode($env); ?>;
        $(document).ready(function() {
            //For Export Buttons available inside jquery-datatable "server side processing" - start

            // function newexportaction(e, dt, button, config) {
            //     var self = this;
            //     var oldStart = dt.settings()[0]._iDisplayStart;
            //     dt.one('preXhr', function (e, s, data) {
            //         // Just this once, load all data from the server...
            //         data.start = 0;
            //         data.length = 2147483647;
            //         dt.one('preDraw', function (e, settings) {
            //             // Call the original action function
            //             if (button[0].className.indexOf('buttons-copy') >= 0) {
            //                 $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
            //             } else if (button[0].className.indexOf('buttons-excel') >= 0) {
            //                 $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
            //                     $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
            //                     $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
            //             } else if (button[0].className.indexOf('buttons-csv') >= 0) {
            //                 $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
            //                     $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
            //                     $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
            //             } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
            //                 $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
            //                     $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
            //                     $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
            //             } else if (button[0].className.indexOf('buttons-print') >= 0) {
            //                 $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            //             }
            //             dt.one('preXhr', function (e, s, data) {
            //                 // DataTables thinks the first item displayed is index 0, but we're not drawing that.
            //                 // Set the property to what it was before exporting.
            //                 settings._iDisplayStart = oldStart;
            //                 data.start = oldStart;
            //             });
            //             // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
            //             setTimeout(dt.ajax.reload, 0);
            //             // Prevent rendering of the full data to the DOM
            //             return false;
            //         });
            //     });
            //     // Requery the server with the new one-time export settings
            //     dt.ajax.reload();
            // };
            //For Export Buttons available inside jquery-datatable "server side processing" - End
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            datatable = $('#employee_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // autoWidth: false,
                // scrollY: "200px",
                // scrollCollapse: true,
                // paging: false,
                ordering: true,
                // columns: [
                //         // { "width": "50%" },
                //         null,
                //         null,
                //         null,
                //         null
                //     ],
                ajax: {
                    url: "{{ route('getEmployeesTable') }}",
                    type: 'POST',
                    data: function (filter){
                            filter.departmentFilter = $('#departmentFilter').val();
                            filter.employeeFilter = $('#employeeFilter').val();
                            filter.empCityFilter = $('#empCityFilter').val();
                            filter.section = $('#section').val();
                            filter.empJobTitleFilter = $('#empJobTitleFilter').val();
                            filter.empStatusFilter = $('#empStatusFilter').val();
                            filter.empPayFilter = $('#empPayFilter').val();
                            filter.empJoinFromDateFilter = $('#empJoinFromDateFilter').val();
                            filter.empJoinToDateFilter = $('#empJoinToDateFilter').val();
                            filter.empConFromDateFilter = $('#empConFromDateFilter').val();
                            filter.empConToDateFilter = $('#empConToDateFilter').val();
                            filter.empBloodFilter = $('#empBloodFilter').val();
                            filter.type = "active";
                        }
                },
                columns: [
                    {
                        data: 'responsive_id',
                        searchable: false,
                        orderable:false
                    },
                    {
                        data: null,
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'id',
                        name: 'employees.id',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        render: function(data, type, full, meta) {
                            var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                            var lastName = full['last_name'] != null ? full['last_name'] : '';
                            return full['first_name'] + ' ' + middleName+ ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'job_title',
                        name: 'jobtitles.name',
                    },
                    @if (env('COMPANY') == 'CLINIX')
                        {
                            render: function(data, type, full, meta) {
                                var bankName = full['bank_name'] != null ? full['bank_name'] : '';
                                var accountTitle = full['account_title'] != null ? full['account_title'] : '';
                                var accountNumber = full['account_number'] != null ? full['account_number'] : '';
                                var bankDetails = bankName + accountTitle + accountNumber;
                                if (bankDetails) {
                                    return bankName + '/ ' + accountTitle+ '/ ' + accountNumber;
                                }
                            },
                            searchable: false
                        },
                        {
                            data: 'bank_name',
                            name: 'employees.bank_name',
                            visible: false,
                        },
                        {
                            data: 'account_title',
                            name: 'employees.account_title',
                            visible: false,
                        },
                        {
                            data: 'account_number',
                            name: 'employees.account_number',
                            visible: false,
                        },
                        @elseif (env('COMPANY') != 'Ajmal Dawakhana')
                        {
                            render: function(data, type, full, meta) {
                                if(full['parent'] == '' || full['parent'] == null){
                                    return '-';    
                                }
                                return full['parent'];
                            },
                            name: 'companystructures.title',
                            searchable: true,
                        },

                    @endif
                    
                    {
                        data: 'title',
                        name: 't2.title',
                    },
                    @if (env('COMPANY') == 'CLINIX')
                    {
                        data: 'type',
                        name: 'companystructures.type',
                    },
                    {
                        data: 'joined_date',
                        name: 'employees.joined_date',
                    },
                    {
                        data: 'confirmation_date',
                        name: 'employees.confirmation_date',
                    },
                    {
                        data: 'active',
                        name: 'employees.active',
                        render: function(data, type, full, meta) {
                            return data == 1 ? 'Active' : 'Inactive';
                        }
                    },
                    @endif
                    {
                        data: 'nic_num',
                        name: 'employees.nic_num',
                    },
                    @if (env('COMPANY') != 'HGNHRM')
                    {
                        data: 'cnic_expiry_date',
                        name: 'employees.cnic_expiry_date',
                    },
                    @endif
                    {
                        data: 'employment_status',
                        name: 'employmentstatus.name',
                    },
                    @if (env('COMPANY') != 'HGNHRM')
                    {
                        data: 'city',
                        name: 'employees.city',
                    },
                    @endif
                    {
                        data: 'mobile_phone',
                        name: 'employees.mobile_phone',
                    },
                    @if (env('COMPANY') == 'CLINIX' || env('COMPANY') == 'HEALTHWISE')    
                        {
                            data: 'blood_group',
                            name: 'employees.blood_group',
                        },
                        {
                            data: 'ppc_reg_no',
                            name: 'employees.ppc_reg_no',
                            visible: false,
                        },
                    @endif
                    {
                        data: "father_name" , render: function(data, type, row) {
                            return row.father_name;
                        },
                    },
                    {
                        data: 'father_cnic',
                        name: 'employees.father_cnic',
                    },
                    @if (env('COMPANY') == 'CLINIX')
                        {
                            render: function(data, type, full, meta) {
                                if(full['parent'] == '' || full['parent'] == null){
                                    return '-';    
                                }
                                return full['parent'];
                            },
                            name: 'companystructures.title',
                            searchable: true,
                        },                    
                    @else
                        {
                            data: 'bank_name',
                            name: 'employees.bank_name',
                        },
                        {
                            data: 'account_title',
                            name: 'employees.account_title',
                        },
                        {
                            data: 'account_number',
                            name: 'employees.account_number',
                        },
                    @endif
                    @if (env('COMPANY') == 'JSML')
                        @can('Employee Gross Salary')
                            {
                                data: 'employees_gross',
                                name: 'employees_gross',
                            },
                        @endcan
                    @endif
                    {
                        data: 'ssn_num',
                        name: 'employees.ssn_num',
                        visible: false,
                    },
                    {
                        data: 'driving_license',
                        name: 'employees.driving_license',
                        visible: false,
                    },
                    {
                        data: 'other_id',
                        name: 'employees.other_id',
                        visible: false,
                    },
                    {
                        data: 'birthday',
                        name: 'employees.birthday',
                        visible: false,
                    },
                    {
                        data: 'nationalityName',
                        name: 'nationality.name',
                        visible: false,
                    },
                    {
                        data: 'gender',
                        name: 'employees.gender',
                        visible: false,
                    },
                    {
                        data: 'marital_status',
                        name: 'employees.marital_status',
                        visible: false,
                    },
                    {
                        data: 'eobi',
                        name: 'employees.eobi',
                        visible: false,
                    },
                    {
                        data: 'cityName',
                        name: 'city.name',
                        visible: false,
                    },
                    {
                        data: 'namecap',
                        name: 'country.namecap',
                        visible: false,
                    },
                    {
                        data: 'provinceName',
                        name: 'province.name',
                        visible: false,
                    },
                    {
                        data: 'postal_code',
                        name: 'employees.postal_code',
                        visible: false,
                    },
                    {
                        data: 'address1',
                        name: 'employees.address1',
                        visible: false,
                    },
                    {
                        data: 'address2',
                        name: 'employees.address2',
                        visible: false,
                    },
                    {
                        data: 'home_phone',
                        name: 'employees.home_phone',
                        visible: false,
                    },
                    {
                        data: 'work_phone',
                        name: 'employees.work_phone',
                        visible: false,
                    },
                    {
                        data: 'work_email',
                        name: 'employees.work_email',
                        visible: false,
                    },
                    {
                        data: 'private_email',
                        name: 'employees.private_email',
                        visible: false,
                    },
                    @if (env('COMPANY') != 'CLINIX')
                    {
                        data: 'joined_date',
                        name: 'employees.joined_date',
                        visible: false,
                    },
                    {
                        data: 'confirmation_date',
                        name: 'employees.confirmation_date',
                        visible: false,
                    },
                    @endif
                    {
                        data: 'work_station_id',
                        name: 'employees.work_station_id',
                        visible: false,
                    },
                    {
                        data: 'passcode',
                        name: 'employees.passcode',
                        visible: false,
                    },
                    {
                        data: 'covid_vaccinated',
                        name: 'employees.covid_vaccinated',
                        visible: false,
                    },
                    {
                        data: 'provident_fund',
                        name: 'employees.provident_fund',
                        visible: false,
                    },
                    {
                        data: 'eobi_status',
                        name: 'employees.eobi_status',
                        visible: false,
                    },
                    {
                        data: 'pessi',
                        name: 'employees.pessi',
                        visible: false,
                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['super_first_name'] + ' ' + full['super_last_name'];
                    //     },
                    //     name: 'e1.first_name' + 'e1.last_name',
                    //     searchable: false
                    // },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [
                    // { width: 500, targets: 3 },
                    // { width: "10px", targets: 0 },
                    // { width: "40px", targets: 1 },
                    // { width: "100px",targets: 2 },
                    // { width: "70px", targets: 3 },
                    // { width: "70px", targets: 4 },
                    // { width: "70px", targets: 5 },
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            var url = '{{ route('employees.edit', ':id') }}';
                            url = url.replace(':id', full.id);
                            var view_url = '{{ route('staff_directory.show', ':id') }}';
                            view_url = view_url.replace(':id', full.id);
                            var history_url = '{{ url('single_employee_history') }}?id=';
                            history_url = history_url.replace('?id=', '?id='+full.id);
                            var btn = '';
                            @can('View Employee')
                                btn = '<a href="' + view_url + '" class="me-1" title="View Employee">' +
                                feather.icons['eye'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>';
                            @endcan

                            @can('Edit Employee')
                                btn += '<a href="' + url + '" class="me-1" title="Edit Employee">' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>';
                            @endcan
                            
                            @can('Single Employee History')
                                btn += '<a href="' + history_url + '" class="me-1" title="Employee History">' +
                                feather.icons['feather'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>';
                            @endcan
                            @if (env('COMPANY') == 'CLINIX')
                                @can('Active Inactive Employees')
                                    btn += `<a href="javascript:;" title="Active/In-active" onclick="active_inactive(${full.id},'${full.first_name} ${full.last_name}','${full.employee_id}','${full.nic_num}','${full.address1}','${full.title}','${full.job_title}','${full.joined_date}')">`+
                                    feather.icons['shield-off'].toSvg({
                                        class: 'font-medium-4 text-danger me-1'
                                    })+
                                    '</a>';
                                @endcan
                            @endif
                            @can('Off Roll Employee')
                                if(env == 'JSML'){
                                    btn += '<a href="javascript:;" title="Off Roll Employee" onclick="off_roll_modal(' +
                                    full.id + ')">' +
                                    feather.icons['file-minus'].toSvg({
                                        class: 'font-medium-4 text-danger me-1'
                                    })
                                    +
                                    '</a>';
                                }
                            @endcan
                            
                            @can('Suspend Employee')
                                btn += '<a href="javascript:;" title="Suspend Employee" onclick="suspend_modal(' +
                                full.id + ')">' +
                                feather.icons['user-minus'].toSvg({
                                    class: 'font-medium-4 text-danger me-1'
                                })
                                +
                                '</a>';
                            @endcan
                            
                            @can('Terminate Employee')
                                btn += `<a href="javascript:;" title="Terminate Employee" onclick="terminate_modal(${full.id},'${full.first_name} ${full.last_name}','${full.employee_id}','${full.nic_num}','${full.address1}','${full.title}','${full.job_title}','${full.joined_date}')">`+
                                feather.icons['user-x'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                })+
                                '</a>';
                            @endcan
                            
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                iDisplayLength: -1,
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)',
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                orientation: 'landscape',
                                pageSize: 'LEGAL',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Employees',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Add Employee Button')    
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50  font-small-4'
                        }) + 'Add Employee',
                        className: 'btn btn-primary',
                        action: function(e, dt, node, config) {
                            window.location.href = '{{ route('employees.create') }}';
                        }
                    },
                    @endcan
                    {
                        text: feather.icons['filter'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Filter',
                        className: 'create-new btn btn-primary ms-1',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#employees_filter_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#active_employee div.head-label').html('<h6 class="mb-0">List of Employees</h6>');
            $('#terminate div.head-label').html('<h6 class="mb-0">List of Terminated Employees</h6>');

            // suspend employee form
            $("#suspend_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('suspend_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#suspend_modal").modal("hide");
                            document.getElementById("suspend_form").reset();
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been Suspended Successfully!'
                            })
                        }

                    }
                });
            });

            // Active In-Active form
            $("#act_inact_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('act_inact_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#act_inact_modal").modal("hide");
                            document.getElementById("suspend_form").reset();
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Active status has been Changed Successfully!'
                            })
                        }

                    }
                });
            });

            // Terminate employee form
            $("#terminate_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('terminate_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#terminate_modal").modal("hide");
                            document.getElementById("terminate_form").reset();
                            datatable.ajax.reload();
                            terminated_dataTable();
                            window.location.href = '{{ route("getEmployees") }}';
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been Terminated Successfully!'
                            })
                        }

                    }
                });
            });

            // Restore employee form
            $("#restore_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('restore_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#restore_modal").modal("hide");
                            document.getElementById("restore_form").reset();
                            suspend_dataTable();
                            terminated_dataTable();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been Restored Successfully!'
                            })
                        }
                    }
                });
            });

            // Off Roll Employee Form
            $("#off_roll_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('off_roll_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#off_roll_modal").modal("hide");
                            document.getElementById("off_roll_form").reset();
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been Off Rolled Successfully!'
                            })
                        }
                    }
                });
            });

            // On Roll Form
            $("#on_roll_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('on_roll_employee') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#on_roll_modal").modal("hide");
                            document.getElementById("on_roll_form").reset();
                            off_roll_dataTable();
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee has been On Rolled Successfully!'
                            })
                        }
                    }
                });
            });
        });

        // Suspended Employees DataTable
        function suspend_dataTable() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#suspend_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('getEmployees') }}",
                    type: 'POST',
                    data: {
                        type: "suspended"
                    }
                },
                columns: [
                    {
                        data: 'responsive_id',
                        searchable: false,
                        orderable:false
                    },
                    {
                        data: null,
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'id',
                        name: 'employees.id',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        render: function(data, type, full, meta) {
                            var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                            var lastName = full['last_name'] != null ? full['last_name'] : '';
                            return full['first_name'] + ' ' + middleName+ ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'job_title',
                        name: 'jobtitles.name',
                    },
                    @if (env('COMPANY') != 'Ajmal Dawakhana')
                    {
                        render: function(data, type, full, meta) {
                            if(full['parent'] == '' || full['parent'] == null){
                                return '-';    
                            }
                            return full['parent'];
                        },
                        name: 'companystructures.title',
                        searchable: true,
                    },
                    @endif
                    {
                        data: 'title',
                        name: 't2.title',
                    },
                    {
                        data: 'nic_num',
                        name: 'employees.nic_num',
                    },
                    @if (env('COMPANY') != 'HGNHRM')
                    {
                        data: 'cnic_expiry_date',
                        name: 'employees.cnic_expiry_date',
                    },
                    @endif
                    {
                        data: 'employment_status',
                        name: 'employmentstatus.name',
                    },
                    @if (env('COMPANY') != 'HGNHRM')
                    {
                        data: 'city',
                        name: 'employees.city',
                    },
                    @endif
                    {
                        data: 'mobile_phone',
                        name: 'employees.mobile_phone',
                    },
                    {
                        data: "father_name" , render: function(data, type, row) {
                            return row.father_name;
                        },
                    },
                    {
                        data: 'father_cnic',
                        name: 'employees.father_cnic',
                    },
                    {
                        data: 'bank_name',
                        name: 'employees.bank_name',
                    },
                    {
                        data: 'account_title',
                        name: 'employees.account_title',
                    },
                    {
                        data: 'account_number',
                        name: 'employees.account_number',
                    },
                    @if (env('COMPANY') == 'JSML')
                        @can('Employee Gross Salary')
                            {
                                data: 'employees_gross',
                                name: 'employees_gross',
                            },
                        @endcan
                    @endif
                    {
                        data: 'ssn_num',
                        name: 'employees.ssn_num',
                        visible: false,
                    },
                    {
                        data: 'driving_license',
                        name: 'employees.driving_license',
                        visible: false,
                    },
                    {
                        data: 'other_id',
                        name: 'employees.other_id',
                        visible: false,
                    },
                    {
                        data: 'birthday',
                        name: 'employees.birthday',
                        visible: false,
                    },
                    {
                        data: 'nationalityName',
                        name: 'nationality.name',
                        visible: false,
                    },
                    {
                        data: 'gender',
                        name: 'employees.gender',
                        visible: false,
                    },
                    {
                        data: 'marital_status',
                        name: 'employees.marital_status',
                        visible: false,
                    },
                    {
                        data: 'eobi',
                        name: 'employees.eobi',
                        visible: false,
                    },
                    {
                        data: 'cityName',
                        name: 'city.name',
                        visible: false,
                    },
                    {
                        data: 'namecap',
                        name: 'country.namecap',
                        visible: false,
                    },
                    {
                        data: 'provinceName',
                        name: 'province.name',
                        visible: false,
                    },
                    {
                        data: 'postal_code',
                        name: 'employees.postal_code',
                        visible: false,
                    },
                    {
                        data: 'address1',
                        name: 'employees.address1',
                        visible: false,
                    },
                    {
                        data: 'address2',
                        name: 'employees.address2',
                        visible: false,
                    },
                    {
                        data: 'home_phone',
                        name: 'employees.home_phone',
                        visible: false,
                    },
                    {
                        data: 'work_phone',
                        name: 'employees.work_phone',
                        visible: false,
                    },
                    {
                        data: 'work_email',
                        name: 'employees.work_email',
                        visible: false,
                    },
                    {
                        data: 'private_email',
                        name: 'employees.private_email',
                        visible: false,
                    },
                    {
                        data: 'joined_date',
                        name: 'employees.joined_date',
                        visible: false,
                    },
                    {
                        data: 'confirmation_date',
                        name: 'employees.confirmation_date',
                        visible: false,
                    },
                    {
                        data: 'work_station_id',
                        name: 'employees.work_station_id',
                        visible: false,
                    },
                    {
                        data: 'passcode',
                        name: 'employees.passcode',
                        visible: false,
                    },
                    {
                        data: 'covid_vaccinated',
                        name: 'employees.covid_vaccinated',
                        visible: false,
                    },
                    {
                        data: 'provident_fund',
                        name: 'employees.provident_fund',
                        visible: false,
                    },
                    {
                        data: 'eobi_status',
                        name: 'employees.eobi_status',
                        visible: false,
                    },
                    {
                        data: 'pessi',
                        name: 'employees.pessi',
                        visible: false,
                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['super_first_name'] + ' ' + full['super_last_name'];
                    //     },
                    //     searchable: false
                    // },
                    {
                        data: 'suspension_date',
                        name: 'employees.suspension_date',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            var url = '{{ route('employees.edit', ':id') }}';
                            url = url.replace(':id', full.id);
                            
                            var history_url = '{{ url('single_employee_history') }}?id=';
                            history_url = history_url.replace('?id=', '?id='+full.id);
                            var view_url = '{{ route('staff_directory.show', ':id') }}';
                            view_url = view_url.replace(':id', full.id);
                            var btn = '';
                            @can('Edit Suspended Employee')
                                btn += '<a href="' + url + '" class="me-1" title="Edit Employee">' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>'
                            @endcan
                            @can('Terminate Employee')
                                btn += '<a href="javascript:;" title="Terminate Employee" onclick="terminate_modal(' +
                                full.id + ')">' +
                                feather.icons['user-x'].toSvg({
                                    class: 'font-medium-4 text-danger me-1'
                                }) +
                                '</a>';
                            @endcan
                            @can('View Employee')
                                btn = '<a href="' + view_url + '" class="me-1" title="View Employee">' +
                                feather.icons['eye'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>'
                            @endcan
                            @can('Restore Employee')
                                btn += '<a href="javascript:;" title="Restore Employee" class="font-medium-1" onclick="restore_modal(' +
                                full.id + ')">' +
                                '<i class="fa fa-undo" aria-hidden="true"></i>' +
                                '</a>';
                            @endcan

                            @can('Single Employee History')
                                btn += '<a href="' + history_url + '" class="me-1" title="Employee History">' +
                                feather.icons['feather'].toSvg({
                                    class: 'font-medium-4 '
                                }) +
                                '</a>';
                            @endcan
                            @can('Terminate Employee')
                                btn += `<a href="javascript:;" title="Terminate Employee" onclick="terminate_modal(${full.id},'${full.first_name} ${full.last_name}','${full.employee_id}','${full.nic_num}','${full.address1}','${full.title}','${full.job_title}','${full.joined_date}')">`+
                                feather.icons['user-x'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                })+
                                '</a>';
                            @endcan
                            @can('Edit Suspended Employee')
                                btn += '<a href="' + url + '" class="me-1" title="Edit Employee">' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4 ms-1'
                                }) +
                                '</a>'
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: [0, 1]
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Suspended Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                'd-inline-flex');
                        }, 50);
                    }
                }, 
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#suspend div.head-label').html('<h6 class="mb-0">List of Suspended Employees</h6>');
        }

        // Registered Employee DataTable
        function registered_datatable(){
            $('#reg_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // ordering: false,
                destroy: true,
                ajax: {
                    url: "{{ route('getEmployees') }}#reg_emp",
                    // data: {

                    //     type: 'off_roll'
                    // }
                    data: function (filter){
                            filter.departmentFilter = $('#departmentFilter').val();
                            filter.employeeFilter = $('#employeeFilter').val();
                            filter.empCityFilter = $('#empCityFilter').val();
                            filter.empJobTitleFilter = $('#empJobTitleFilter').val();
                            filter.empStatusFilter = $('#empStatusFilter').val();
                            filter.empPayFilter = $('#empPayFilter').val();
                            filter.section = $('#section').val();
                            filter.empJoinFromDateFilter = $('#empJoinFromDateFilter').val();
                            filter.empJoinToDateFilter = $('#empJoinToDateFilter').val();
                            filter.empConFromDateFilter = $('#empConFromDateFilter').val();
                            filter.empConToDateFilter = $('#empConToDateFilter').val();
                            filter.empBloodFilter = $('#empBloodFilter').val();
                            filter.type = "reg_emp";
                        }
                },
                columns: [
                    {
                        data: 'responsive_id',
                        searchable: false,
                        orderable:false
                    },
                    {
                        data: null,
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'id',
                        name: 'employees.id',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                        searchable: true,

                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                        searchable: true,

                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['first_name'] + ' ' + full['middle_name']+ ' ' + full['last_name'];
                    //     },
                    //     searchable: false
                    // },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        // searchable: true,
                        // visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        // searchable: true,
                        // visible: false
                    },
                    {
                        data: 'job_title',
                        name: 'jobtitles.name',
                    },
                    {
                        data: 'employment_status',
                        name: 'employmentstatus.name',
                    },
                    {
                        data: 'title',
                        name: 'companystructures.title',
                        // name: 'companystructures.title',
                        // render: function(data, type, full, meta) {
                        //     if(full['parent'] == '' || full['parent'] == null){
                        //         return '-';    
                        //     }
                        //     return full['parent'];
                        // },
                        searchable: false,
                    },
                    {
                        data: 'city',
                        name: 'employees.city',
                    },
                    {
                        data: 'mobile_phone',
                        name: 'employees.mobile_phone',
                    },
                    // {
                    //     data: 'title',
                    //     name: 't2.title',
                    // },
                    // {
                    //     data: "father_name" , render: function(data, type, row) {
                    //         return row.father_name;
                    //     },
                    // },
                    {
                        data: 'nic_num',
                        name: 'employees.nic_num',
                    },
                    // {
                    //     data: 'father_cnic',
                    //     name: 'employees.father_cnic',
                    // },
                    {
                        data: 'bank_name',
                        name: 'employees.bank_name',
                    },
                    {
                        data: 'account_title',
                        name: 'employees.account_title',
                    },
                    {
                        data: 'account_number',
                        name: 'employees.account_number',
                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['super_first_name'] + ' ' + full['super_last_name'];
                    //     },
                    //     searchable: false
                    // },
                    @if (env('COMPANY') == 'JSML')
                        @can('Employee Gross Salary')
                            {
                                data: 'employees_gross',
                                name: 'employees_gross',
                            },
                        @endcan
                    @endif
                    {
                        data: 'off_roll_date',
                        name: 'employees.off_roll_date',
                    },
                    {
                        data: '',
                        searchable: false,
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                   
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'landscape',
                            title: 'Off Roll Employees',
                            pageSize: 'LEGAL',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                'd-inline-flex');
                        }, 50);
                    }
                }, 
                {
                    text: feather.icons['filter'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Filter',
                    className: 'create-new btn btn-primary ms-1',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#employees_filter_modal'
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#off_roll div.head-label').html('<h6 class="mb-0">List of Off Roll Employees</h6>');
        }

        // Terminated Employees DataTable
        function terminated_dataTable() {
            $('#terminate_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // ordering: false,
                destroy: true,
                ajax: {
                    url: "{{ route('getEmployees') }}#terminate",
                    type: 'POST',
                    data: function (filter){
                            filter.departmentFilter = $('#departmentFilter').val();
                            filter.employeeFilter = $('#employeeFilter').val();
                            filter.empCityFilter = $('#empCityFilter').val();
                            filter.empJobTitleFilter = $('#empJobTitleFilter').val();
                            filter.empStatusFilter = $('#empStatusFilter').val();
                            filter.empPayFilter = $('#empPayFilter').val();
                            filter.section = $('#section').val();
                            filter.empJoinFromDateFilter = $('#empJoinFromDateFilter').val();
                            filter.empJoinToDateFilter = $('#empJoinToDateFilter').val();
                            filter.empConFromDateFilter = $('#empConFromDateFilter').val();
                            filter.empConToDateFilter = $('#empConToDateFilter').val();
                            filter.empBloodFilter = $('#empBloodFilter').val();
                            filter.type = "terminate";
                        }
                },
                columns: [
                    {
                        data: 'responsive_id',
                        searchable: false,
                        orderable:false
                    },
                    {
                        data: null,
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'id',
                        name: 'employees.id',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        render: function(data, type, full, meta) {
                            var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                            var lastName = full['last_name'] != null ? full['last_name'] : '';
                            return full['first_name'] + ' ' + middleName+ ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'job_title',
                        name: 'jobtitles.name',
                    },
                    {
                        render: function(data, type, full, meta) {
                            if(full['parent'] == '' || full['parent'] == null){
                                return '-';    
                            }
                            return full['parent'];
                        },
                        name: 'companystructures.title',
                        searchable: true,
                    },
                    {
                        data: 'title',
                        name: 't2.title',
                    },
                    {
                        data: 'nic_num',
                        name: 'employees.nic_num',
                    },
                    @if (env('COMPANY') != 'HGNHRM')
                    {
                        data: 'cnic_expiry_date',
                        name: 'employees.cnic_expiry_date',
                    },
                    @endif
                    {
                        data: 'employment_status',
                        name: 'employmentstatus.name',
                    },
                    @if (env('COMPANY') != 'HGNHRM')
                    {
                        data: 'city',
                        name: 'employees.city',
                    },
                    @endif
                    {
                        data: 'mobile_phone',
                        name: 'employees.mobile_phone',
                    },
                    {
                        data: "father_name" , render: function(data, type, row) {
                            return row.father_name;
                        },
                    },
                    {
                        data: 'father_cnic',
                        name: 'employees.father_cnic',
                    },
                    {
                        data: 'bank_name',
                        name: 'employees.bank_name',
                    },
                    {
                        data: 'account_title',
                        name: 'employees.account_title',
                    },
                    {
                        data: 'account_number',
                        name: 'employees.account_number',
                    },
                    @if (env('COMPANY') == 'JSML')
                        @can('Employee Gross Salary')
                            {
                                data: 'employees_gross',
                                name: 'employees_gross',
                            },
                        @endcan
                    @endif
                    {
                        data: 'ssn_num',
                        name: 'employees.ssn_num',
                        visible: false,
                    },
                    {
                        data: 'driving_license',
                        name: 'employees.driving_license',
                        visible: false,
                    },
                    {
                        data: 'other_id',
                        name: 'employees.other_id',
                        visible: false,
                    },
                    {
                        data: 'birthday',
                        name: 'employees.birthday',
                        visible: false,
                    },
                    {
                        data: 'nationalityName',
                        name: 'nationality.name',
                        visible: false,
                    },
                    {
                        data: 'gender',
                        name: 'employees.gender',
                        visible: false,
                    },
                    {
                        data: 'marital_status',
                        name: 'employees.marital_status',
                        visible: false,
                    },
                    {
                        data: 'eobi',
                        name: 'employees.eobi',
                        visible: false,
                    },
                    {
                        data: 'cityName',
                        name: 'city.name',
                        visible: false,
                    },
                    {
                        data: 'namecap',
                        name: 'country.namecap',
                        visible: false,
                    },
                    {
                        data: 'provinceName',
                        name: 'province.name',
                        visible: false,
                    },
                    {
                        data: 'postal_code',
                        name: 'employees.postal_code',
                        visible: false,
                    },
                    {
                        data: 'address1',
                        name: 'employees.address1',
                        visible: false,
                    },
                    {
                        data: 'address2',
                        name: 'employees.address2',
                        visible: false,
                    },
                    {
                        data: 'home_phone',
                        name: 'employees.home_phone',
                        visible: false,
                    },
                    {
                        data: 'work_phone',
                        name: 'employees.work_phone',
                        visible: false,
                    },
                    {
                        data: 'work_email',
                        name: 'employees.work_email',
                        visible: false,
                    },
                    {
                        data: 'private_email',
                        name: 'employees.private_email',
                        visible: false,
                    },
                    {
                        data: 'joined_date',
                        name: 'employees.joined_date',
                        visible: false,
                    },
                    {
                        data: 'confirmation_date',
                        name: 'employees.confirmation_date',
                        visible: false,
                    },
                    {
                        data: 'work_station_id',
                        name: 'employees.work_station_id',
                        visible: false,
                    },
                    {
                        data: 'passcode',
                        name: 'employees.passcode',
                        visible: false,
                    },
                    {
                        data: 'covid_vaccinated',
                        name: 'employees.covid_vaccinated',
                        visible: false,
                    },
                    {
                        data: 'provident_fund',
                        name: 'employees.provident_fund',
                        visible: false,
                    },
                    {
                        data: 'eobi_status',
                        name: 'employees.eobi_status',
                        visible: false,
                    },
                    {
                        data: 'pessi',
                        name: 'employees.pessi',
                        visible: false,
                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['super_first_name'] + ' ' + full['super_last_name'];
                    //     },
                    //     searchable: false
                    // },
                    {
                        data: 'termination_date',
                        name: 'employees.termination_date',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            var url = '{{ route('employees.edit', ':id') }}';
                            url = url.replace(':id', full.id);

                            var history_url = '{{ url('single_employee_history') }}?id=';
                            history_url = history_url.replace('?id=', '?id='+full.id);
                            var view_url = '{{ route('staff_directory.show', ':id') }}';
                            view_url = view_url.replace(':id', full.id);
                            var btn = '';
                            @can('View Employee')
                                btn = '<a href="' + view_url + '" class="me-1" title="View Employee">' +
                                feather.icons['eye'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>'
                            @endcan
                            @can('Edit Employee')
                                btn = '<a href="' + url + '" class="me-1" title="Edit Employee">' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>'
                            @endcan
                            @can('Restore Employee')
                                btn += '<a href="javascript:;" title="Restore Employee" class="font-medium-1" onclick="restore_modal(' +
                                full.id + ')">' +
                                '<i class="fa fa-undo" aria-hidden="true"></i>' +
                                '</a>'
                            @endcan
                            @can('Single Employee History')
                                btn += '<a href="' + history_url + '" class="me-1" title="Employee History">' +
                                feather.icons['feather'].toSvg({
                                    class: 'font-medium-4 ms-1'
                                }) +
                                '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Terminated Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Terminated Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Terminated Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'landscape',
                            title: 'Terminated Employees',
                            pageSize: 'LEGAL',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Terminated Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                'd-inline-flex');
                        }, 50);
                    }
                }, 
                {
                    text: feather.icons['filter'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Filter',
                    className: 'create-new btn btn-primary ms-1',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#employees_filter_modal'
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#terminate div.head-label').html('<h6 class="mb-0">List of Terminated Employees</h6>');
        }

        // Off Roll Employee DataTable
        function off_roll_dataTable(){
            $('#off_roll_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // ordering: false,
                destroy: true,
                ajax: {
                    url: "{{ route('getEmployees') }}#off_roll",
                    // data: {

                    //     type: 'off_roll'
                    // }
                    data: function (filter){
                            filter.departmentFilter = $('#departmentFilter').val();
                            filter.employeeFilter = $('#employeeFilter').val();
                            filter.empCityFilter = $('#empCityFilter').val();
                            filter.empJobTitleFilter = $('#empJobTitleFilter').val();
                            filter.empStatusFilter = $('#empStatusFilter').val();
                            filter.empPayFilter = $('#empPayFilter').val();
                            filter.section = $('#section').val();
                            filter.empJoinFromDateFilter = $('#empJoinFromDateFilter').val();
                            filter.empJoinToDateFilter = $('#empJoinToDateFilter').val();
                            filter.empConFromDateFilter = $('#empConFromDateFilter').val();
                            filter.empConToDateFilter = $('#empConToDateFilter').val();
                            filter.empBloodFilter = $('#empBloodFilter').val();
                            filter.type = "off_roll";
                        }
                },
                columns: [
                    {
                        data: 'responsive_id',
                        searchable: false,
                        orderable:false
                    },
                    {
                        data: null,
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'id',
                        name: 'employees.id',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                        searchable: true,

                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                        searchable: true,

                    },
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['first_name'] + ' ' + full['middle_name']+ ' ' + full['last_name'];
                    //     },
                    //     searchable: false
                    // },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        // searchable: true,
                        // visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        // searchable: true,
                        // visible: false
                    },
                    {
                        data: 'job_title',
                        name: 'jobtitles.name',
                    },
                    {
                        data: 'employment_status',
                        name: 'employmentstatus.name',
                    },
                    {
                        data: 'title',
                        name: 'companystructures.title',
                        // name: 'companystructures.title',
                        // render: function(data, type, full, meta) {
                        //     if(full['parent'] == '' || full['parent'] == null){
                        //         return '-';    
                        //     }
                        //     return full['parent'];
                        // },
                        searchable: false,
                    },
                    {
                        data: 'city',
                        name: 'employees.city',
                    },
                    {
                        data: 'mobile_phone',
                        name: 'employees.mobile_phone',
                    },
                    // {
                    //     data: 'title',
                    //     name: 't2.title',
                    // },
                    // {
                    //     data: "father_name" , render: function(data, type, row) {
                    //         return row.father_name;
                    //     },
                    // },
                    {
                        data: 'nic_num',
                        name: 'employees.nic_num',
                    },
                    // {
                    //     data: 'father_cnic',
                    //     name: 'employees.father_cnic',
                    // },
                    {
                        data: 'bank_name',
                        name: 'employees.bank_name',
                    },
                    {
                        data: 'account_title',
                        name: 'employees.account_title',
                    },
                    {
                        data: 'account_number',
                        name: 'employees.account_number',
                    },
                    @if (env('COMPANY') == 'JSML')
                        @can('Employee Gross Salary')
                            {
                                data: 'employees_gross',
                                name: 'employees_gross',
                            },
                        @endcan
                    @endif
                    // {
                    //     render: function(data, type, full, meta) {
                    //         return full['super_first_name'] + ' ' + full['super_last_name'];
                    //     },
                    //     searchable: false
                    // },
                    {
                        data: 'off_roll_date',
                        name: 'employees.off_roll_date',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            var url = '{{ route('employees.edit', ':id') }}';
                            url = url.replace(':id', full.id);
                            var btn = '';
                            @can('Edit Off Roll Employee')
                                btn += '<a href="' + url + '" class="me-1" title="Edit Employee">' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>'
                            @endcan
                            @can('On Roll Employee')
                                btn += '<a href="javascript:;" title="On Roll Employee" class="me-1 font-medium-1" onclick="on_roll_modal(' +
                                full.id + ')">' +
                                '<i class="fa fa-undo" aria-hidden="true"></i>' +
                                '</a>';
                            @endcan
                            @can('Terminate Employee')
                                btn += `<a href="javascript:;" title="Terminate Employee" onclick="terminate_modal(${full.id},'${full.first_name} ${full.last_name}','${full.employee_id}','${full.nic_num}','${full.address1}','${full.title}','${full.job_title}','${full.joined_date}')">`+
                                feather.icons['user-x'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                })+
                                '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'landscape',
                            title: 'Off Roll Employees',
                            pageSize: 'LEGAL',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Off Roll Employees',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                'd-inline-flex');
                        }, 50);
                    }
                }, 
                {
                    text: feather.icons['filter'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Filter',
                    className: 'create-new btn btn-primary ms-1',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#employees_filter_modal'
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#off_roll div.head-label').html('<h6 class="mb-0">List of Off Roll Employees</h6>');
        }

        // Suspend Employee Modal
        function suspend_modal(id) {
            $(".modal-body #employee_id").val(id);
            $('#suspend_modal').modal('toggle');
        }
        //Active inactive
        function active_inactive(id) {
            $(".modal-body #act_employee_id").val(id);
            $('#act_inact_modal').modal('toggle');
        }
        // Terminate Employee Modal
        function terminate_modal(id,name,ID,cnic,address,department,designation,doj) {
            $(".modal-body #employee_id").val(id);
            $('#name').val(name);
            $('#ID').val(ID);
            $('#cnic').val(cnic);
            $('#address').val(address);
            $('#department').val(department);
            $('#designation').val(designation);
            $('#doj').val(doj);
            $('#terminate_modal').modal('toggle');
        }

        // Restore Employee Modal
        function restore_modal(id) {
            $(".modal-body #employee_id").val(id);
            $('#restore_modal').modal('toggle');
        }

        // Off Role Employee Modal
        function off_roll_modal(id) {
            $(".modal-body #employee_id").val(id);
            $('#off_roll_modal').modal('toggle');
        }

        // On Roll Employee Modal
        function on_roll_modal(id) {
            $(".modal-body #employee_id").val(id);
            $('#on_roll_modal').modal('toggle');
        }

        // Get Employees
        function employeeFun() {
            var dept_id = $('#departmentFilter').val();
            $.ajax({
                url: "{{ url('employees') }}" + "/" + dept_id,
                type: 'GET',
                success: function(response) {
                    $('#employeeFilter').html('');
                    var opt = $('<option>');
                    $('#employeeFilter').append(opt);
                    $.each(response, function(id, curEmp) {
                        var middleName = curEmp.middle_name != null ? curEmp.middle_name : '';
                        var lastName = curEmp.last_name != null ? curEmp.last_name : '';
                        var opt = $('<option>');
                        opt.val(curEmp.id);
                        opt.text(curEmp.employee_id + "-" + curEmp.employee_code + "-" + curEmp.first_name + " " + middleName + " " + lastName + "-" + curEmp.designation + "-" + curEmp.department);
                        $('#employeeFilter').append(opt);
                    });
                }
            });
        }

        $('#employee_search_form').submit(function(e){
            $('#employees_filter_modal').modal('hide');
            var hash = location.hash.replace(/^#/, '');
            // ^ means starting, meaning only match the first hash
            if (hash) {
                if(hash=='terminate'){
                    jQuery(function(){
                    jQuery('#terminate-tab').click();
                });
                }
                if(hash=='off_roll'){
                    jQuery(function(){
                    jQuery('#off_roll-tab').click();
                });
                }
            }
            datatable.draw();
            e.preventDefault();
        });
    </script>
@endsection
