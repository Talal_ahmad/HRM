@extends('Admin.layouts.master')

@section('title', 'Final Settlement Letter Print')
@section('style')
<style>
    #main_table td {
        padding: 6px 10px;
    }
</style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Final Settlement Letter Print</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item">@lang('app.Employees')
                            </li>
                            <li class="breadcrumb-item active">Final Settlement Print
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <form class="form" id="add_form">
                    @csrf
                    @method('PUT')
                    <div class="card pt-2 pb-2" id="termination_letter_div">
                        <div class="container-fluid">
                            <div class="row ">
                            <div class="col-md-12">
                                 <h2 class="text-center">FINAL SETTLEMENT SHEET</h2>
                            </div>
                            {{-- <div class="col-6">
                                <span style="font-size: x-large;">{{env('COMPANY')}}</span>
                                <img src="{{asset('/images/employees/logo.png')}}"  alt="" srcset="" width="120px" height="120px" class="rounded-pill">
                            </div> --}}
                            </div>
                            <div class="row pb-1">
                                <div class="col-6">
                                    <table class="table-bordered" id="main_table">
                                        <tbody>
                                            <tr>
                                                <td width="400"><span>{{HandleEmpty($data->employee_code)}}</span></td>
                                            </tr>
                                            <tr>
                                                <td width="400"><span>{{$data->first_name}} {{$data->last_name}}</span></td>
                                            </tr>
                                            <tr>
                                                <td width="400"><span>{{$data->nic_num}}</span></td>
                                            </tr>
                                            <tr>
                                                <td width="400"><span>{{$data->job_title}}</span></td>
                                            </tr>
                                            <tr>
                                                <td width="400"><span>{{$data->title}}</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-6 text-end">
                                    @if (!empty($data->image))
                                        <img src="{{asset('images/employees')}}/{{$data->image}}" alt="Image" height="200" width="150" style="border:1px solid black;padding:12px">
                                    @else
                                        <img src="{{asset('images/employees/default_employee.png')}}" id="account-upload-img" style="border:1px solid black;padding:12px" alt="profile image" height="250" width="250">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 text-center" style="border: 1px solid">
                                    <div>
                                        <label class="checkbox-inline">
                                            Resignation
                                            <input type="checkbox" id="resignation" value="Resignation">
                                        </label>
                                        <label class="checkbox-inline ms-1">
                                            Termination
                                            <input type="checkbox" id="termination" value="Termination">
                                        </label>
                                        <label class="checkbox-inline ms-1">
                                            Layoff
                                            <input type="checkbox" id="layoff" value="Layoff">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 text-center" style="border: 1px solid">
                                    <span>
                                        <p><u class="fw-bold" style="font-size: 20px;">Employement Status</u> ({{$data->employment}})</p>
                                    </span>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5px;">
                                <table class="table table-bordered" style="border: 1px solid black;">
                                    <tbody>
                                        <tr>
                                            <td>{{$data->joined_date}}</td>
                                            <td>{{$data->termination_date}}</td>
                                        </tr>
                                        <tr>
                                            <td id="basic_salary">{{$data->basic_salary}}</td>
                                            {{-- <td class="tdmodal"><span class="days_td"></span></td> --}}
                                            <td class="tdmodal"><Span class="days_d_td1"></Span></td>
                                        </tr>
                                        <tr>
                                            <td id="notice_period_value">{{HandleEmpty($data->notice_period_status_end_date)}}</td>
                                            {{-- <td></td> --}}
                                            {{-- @if (env('COMPANY') != 'RoofLine' && env('COMPANY') != 'Ajmal Dawakhana')
                                                <td>{{$basic_salary}}</td>
                                            @else
                                                <td class="tdmodal"><span class="bs_s"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <p>{{!empty($data->terminate_reason) ? $data->terminate_reason : ''}}</p>
                                <input type="hidden" id="emp_id" value="{{$data->id}}">
                            </div>
                            <div class="text-end">
                                <h4 style="text-decoration-line: overline;">HR OFFICER</h4>
                            </div>
                            <div class="row" id="printableContent">
                                <table class="table table-bordered" style="border: 1px solid black;">
                                    <thead class="text-center">
                                        <tr>
                                            <th colspan="2"><u>ALLOWANCES</u></th>
                                            <th><u>Remarks</u></th>
                                            <th colspan="2"><u>DEDUCTIONS</u></th>
                                            <th><u>Remarks</u></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                                <td>{{$data->day_payable}}</td>
                                                <td>{{$data->days_payable_remarks}}</td>
                                                <td>{{$data->unpaid}}</td>
                                                <td>{{$data->unpaid_remarks}}</td>
                                            {{-- @endif                               --}}
                                         </tr>
                                        <tr>
                                            <td>{{$data->leave_encash}}</td>
                                            <td>{{$data->leave_encash_remarks}}</td>
                                            <td>{{$data->extra_leaves}}</td>
                                            <td>{{$data->extra_leaves_remarks}}</td>
                                       </tr>
                                        <tr>
                                            <td>{{$data->overtime_1}}</td>
                                            <td>{{$data->overtime_1_remarks}}</td>
                                            <td>{{$data->notice_period}}</td>
                                            <td>{{$data->notice_period_remarks}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{$data->overtime_2}}</td>
                                            <td>{{$data->overtime_2_remarks}}</td>
                                            <td>{{$data->eobi}}</td>
                                            <td>{{$data->eobi_remarks}}</td>
                                         </tr>
                                        <tr>
                                            <td>{{$data->food}}</td>
                                            <td>{{$data->food_remarks}}</td>
                                            <td>{{$data->tardiness}}</td>
                                            <td>{{$data->tardiness_remarks}}</td>
                                        </tr> 
                                         <tr>
                                            <td>{{$data->arrears}}</td>
                                            <td>{{$data->arrears_remarks}}</td>
                                            <td>{{$data->loan_advance}}</td>
                                            <td>{{$data->loan_advance_remarks}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{$data->others_1}}</td>
                                            <td>{{$data->others_1_remarks}}</td>
                                            <td>{{$data->others_2}}</td>
                                            <td>{{$data->others_2_remarks}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Total</b></td>
                                            <td class="entitlement_all">{{$data->entitlement_net}}</td>
                                            <td></td>
                                            <td><b>Total</b></td>
                                            <td class="deduction_all">{{$data->deduction_net}}</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <table>
                                            <tr>
                                                <td><label for="staticEmail" class="offset-1 col-sm-4 col-form-label">HR Department: </label></td>
                                                <td><input type="text" class="form-control" id="staticEmail" style="border: none; border-bottom: 1px solid black; border-radius: 0; padding: 2px 2px;"></td>
                                                <td><label for="staticEmail" class="offset-1 col-sm-4 col-form-label">Receiving: </label></td>
                                                <td><input type="text" class="form-control" id="staticEmail" style="border: none; border-bottom: 1px solid black; border-radius: 0; padding: 2px 2px;"></td>
                                            </tr>
                                        </table>
                                        {{-- <label for="staticEmail" class="offset-1 col-sm-4 col-form-label">HR Department: </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" id="staticEmail" style="border: none; border-bottom: 1px solid black; border-radius: 0; padding: 2px 2px;">
                                        </div> --}}
                                    </div>
                                </div>
                                {{-- <div class="col-6">
                                    <div class="form-group row">
                                        <label for="staticEmail" class="offset-1 col-sm-4 col-form-label">Receiving: </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" id="staticEmail" style="border: none; border-bottom: 1px solid black; border-radius: 0; padding: 2px 2px;">
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="col-12 text-center">
                                    <table class="table table-bordered" style="border: 1px solid black;">
                                        <thead>
                                            <tr>
                                                <th class="p-0" colspan="3">
                                                    <h3 class="text-center"><u>NET</u></h3>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="p-0">
                                                    <p>Total Allowances: <span id="entitlement_net" class="entitlement_net">{{$data->entitlement_net}}</span></p>
                                                </td>
                                                <td class="p-0">
                                                    <p>Less Deduction: <span id="deduction_net" class="deduction_net">{{$data->deduction_net}}</span></p>
                                                </td>
                                                <td class="p-0">
                                                    <p>Net Payable: <u><span id="net_all">{{$data->net_all}}</span></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    {{-- <p>Net Payable: <input class="net_all" type="text" style="border-width: 0 0 2px 0; outline: none;"></p> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-end">
                        {{-- <button type="submit" class="btn btn-success form_save"><i class="fas fa-save" style="margin-right: 2px"></i></i> Save</button> --}}
                        <button class="btn btn-danger" onclick="printDiv()"><i class="fas fa-print" style="margin-right: 2px"></i></i> Print</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    
    function printDiv() {
        // convertToSpans();
        var divToPrint = document.getElementById("termination_letter_div");
        const WinPrint = window.open("");
        WinPrint.document.write(
            `<!DOCTYPE html>
                    <head>
                    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
                    <style>
                        body {
                            background-color: #fff;
                        }
                        #main_table td {
                            padding: 6px 10px;
                        }
                    </style>
                    </head>
                    <body>
                    ${WinPrint.document.write(divToPrint.innerHTML)}
                    </body>
                    </html>`
        )
        setTimeout(function() {
            WinPrint.print();
            WinPrint.close();
        }, 500);
        WinPrint.focus();
        // convertToInputs();
    }
</script>
@endsection