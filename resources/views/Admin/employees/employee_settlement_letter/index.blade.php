@extends('Admin.layouts.master')

@section('title', 'Final Settlement Letter')
@section('style')
<style>
    #main_table td {
        padding: 6px 10px;
    }
</style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Final_Settlement_Letter')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item">@lang('app.Employees')
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Final_Settlement_Letter')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card p-2">
                    <form class="form" method="GET" action="{{url('settlement_letter')}}">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="title">@lang('app.Department')</label>
                                    <select name="department" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                        <option value="">@lang('app.Select_Department')</option>
                                        @foreach (departments() as $department)
                                        <option value="{{$department->id}}">{{$department->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="title">@lang('app.Employee')</label>
                                    <select name="employee_id" id="employee" data-placeholder="@lang('app.Select_Employee')" class="select2 form-select" required>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="text-end">
                            <button type="submit" class="btn btn-primary">@lang('app.Get_Letter')</button>
                            @can('FInal Settlement Record History')
                            <a class="btn btn-success" href="{{url('settlement_history')}}"><i class="fa fa-arrow-right"></i> @lang('app.FInal_Settlement_Record_History')</a>
                            @endcan
                        </div>
                    </form>
                </div>
                @if(request('employee_id'))
                <form class="form" id="add_form">
                    @csrf
                    @method('PUT')
                    <div class="card pt-2 pb-2" id="termination_letter_div">
                        <div class="container-fluid">
                            <div class="row ">
                            <div class="col-md-12">
                                 <h2 class="text-center">FINAL SETTLEMENT SHEET</h2>
                            </div>
                            </div>
                            <div class="row pb-1">
                                <div class="col-6">
                                    <table class="table-bordered" id="main_table">
                                        <tbody>
                                            <tr>
                                                <td width="400"><span>{{HandleEmpty($data->employee_code)}}</span></td>
                                            </tr>
                                            <tr>
                                                <td width="400"><span>{{$data->first_name}} {{$data->last_name}}</span></td>
                                            </tr>
                                            <tr>
                                                <td width="400"><span>{{$data->nic_num}}</span></td>
                                            </tr>
                                            <tr>
                                                <td width="400"><span>{{$data->job_title}}</span></td>
                                            </tr>
                                            <tr>
                                                <td width="400"><span>{{$data->title}}</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-6 text-end">
                                    @if (!empty($data->image))
                                    <img src="{{asset('images/employees')}}/{{$data->image}}" alt="Image" height="200" width="150" style="border:1px solid black;padding:12px">
                                    @else
                                    <img src="{{asset('images/employees/default_employee.png')}}" id="account-upload-img" style="border:1px solid black;padding:12px" alt="profile image" height="250" width="250">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 text-center" style="border: 1px solid">
                                    <div>
                                        <label class="checkbox-inline">
                                            Resignation
                                            <input type="checkbox" id="resignation" value="Resignation">
                                        </label>
                                        <label class="checkbox-inline ms-1">
                                            Termination
                                            <input type="checkbox" id="termination" value="Termination">
                                        </label>
                                        <label class="checkbox-inline ms-1">
                                            Layoff
                                            <input type="checkbox" id="layoff" value="Layoff">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 text-center" style="border: 1px solid">
                                    <span>
                                        <p><u class="fw-bold" style="font-size: 20px;">Employement Status</u> ({{$data->employment}})</p>
                                    </span>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5px;">
                                <table class="table table-bordered" style="border: 1px solid black;">
                                    <tbody>
                                        <tr>
                                            <td>{{$data->joined_date}}</td>
                                            <td>{{$data->termination_date}}</td>
                                        </tr>
                                        <tr>
                                            <td id="basic_salary">{{$basic_salary}}</td>
                                            {{-- <td class="tdmodal"><span class="days_td"></span></td> --}}
                                            <td class="tdmodal"><Span class="days_d_td1"></Span></td>
                                        </tr>
                                        <tr>
                                            <td id="notice_period_value">{{HandleEmpty($data->notice_period_status_end_date)}}</td>
                                            {{-- <td></td> --}}
                                            {{-- @if (env('COMPANY') != 'RoofLine' && env('COMPANY') != 'Ajmal Dawakhana')
                                                <td>{{$basic_salary}}</td>
                                            @else
                                                <td class="tdmodal"><span class="bs_s"></span></td>
                                            @endif --}}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <p>{{!empty($data->terminate_reason) ? $data->terminate_reason : ''}}</p>
                                <input type="hidden" id="emp_id" value="{{$data->id}}">
                            </div>
                            <div class="text-end">
                                <h4 style="text-decoration-line: overline;">HR OFFICER</h4>
                            </div>
                            <div class="row" id="printableContent">
                                <table class="table table-bordered" style="border: 1px solid black;">
                                    <thead class="text-center">
                                        <tr>
                                            <th colspan="2"><u>ALLOWANCES</u></th>
                                            <th><u>Remarks</u></th>
                                            <th colspan="2"><u>DEDUCTIONS</u></th>
                                            <th><u>Remarks</u></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="text" id="days_payable" class="form-control convert-input-to-span allowance-input"></td>
                                            <td><input type="text" id="days_payable_remarks" class="form-control convert-input-to-span"></td>
                                            <td><input type="text" id="unpaid" class="form-control deduction-input convert-input-to-span"></td>
                                            <td><input type="text" id="unpaid_remarks" class="form-control convert-input-to-span"></td>
                                         </tr>
                                        <tr>
                                            <td><input type="text" id="leave_encash" class="form-control convert-input-to-span allowance-input"></td> 
                                            <td><input type="text" id="leave_encash_remarks" class="form-control convert-input-to-span"></td>
                                            <td><input type="text" id="extra_leaves" class="form-control deduction-input convert-input-to-span"></td>
                                            <td><input type="text" id="extra_leaves_remarks" class="form-control convert-input-to-span"></td>
                                       </tr>
                                        <tr>
                                            <td><input type="text" id="overtime_1" class="form-control convert-input-to-span allowance-input"></td>
                                            <td><input type="text" id="overtime_1_remarks" class="form-control convert-input-to-span"></td>
                                            <td><input type="text" id="notice_period" class="form-control deduction-input convert-input-to-span"></td>
                                            <td><input type="text" id="notice_period_remarks" class="form-control convert-input-to-span"></td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" id="overtime_2" class="form-control convert-input-to-span allowance-input"></td>
                                            <td><input type="text" id="overtime_2_remarks" class="form-control convert-input-to-span"></td>
                                            <td><input type="text" id="eobi" class="form-control deduction-input convert-input-to-span"></td>
                                            <td><input type="text" id="eobi_remarks" class="form-control convert-input-to-span"></td>
                                         </tr>
                                        <tr>
                                            <td><input type="text" id="food" class="form-control convert-input-to-span allowance-input"></td>
                                            <td><input type="text" id="food_remarks" class="form-control convert-input-to-span"></td>
                                            <td><input type="text" id="tardiness" class="form-control deduction-input convert-input-to-span"></td>
                                            <td><input type="text" id="tardiness_remarks" class="form-control convert-input-to-span"></td>
                                        </tr> 
                                         <tr>
                                            <td><input type="text" id="arrears" class="form-control convert-input-to-span allowance-input"></td>
                                            <td><input type="text" id="arrears_remarks" class="form-control convert-input-to-span"></td>
                                            <td><input type="text" id="loan_advance" class="form-control deduction-input convert-input-to-span"></td>
                                            <td><input type="text" id="loan_advance_remarks" class="form-control convert-input-to-span"></td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" id="others_1" class="form-control convert-input-to-span allowance-input"></td>
                                            <td><input type="text" id="others_1_remarks" class="form-control convert-input-to-span"></td>
                                            <td><input type="text" id="others_2" class="form-control deduction-input convert-input-to-span"></td>
                                            <td><input type="text" id="others_2_remarks" class="form-control convert-input-to-span"></td>
                                        </tr>
                                        <tr>
                                            <td><b>Total</b></td>
                                            <td class="entitlement_all"></td>
                                            <td></td>
                                            <td><b>Total</b></td>
                                            <td class="deduction_all"></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <table>
                                            <tr>
                                                <td><label for="staticEmail" class="offset-1 col-sm-4 col-form-label">HR Department: </label></td>
                                                <td><input type="text" class="form-control" id="staticEmail" style="border: none; border-bottom: 1px solid black; border-radius: 0; padding: 2px 2px;"></td>
                                                <td><label for="staticEmail" class="offset-1 col-sm-4 col-form-label">Receiving: </label></td>
                                                <td><input type="text" class="form-control" id="staticEmail" style="border: none; border-bottom: 1px solid black; border-radius: 0; padding: 2px 2px;"></td>
                                            </tr>
                                        </table>
                                        {{-- <label for="staticEmail" class="offset-1 col-sm-4 col-form-label">HR Department: </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" id="staticEmail" style="border: none; border-bottom: 1px solid black; border-radius: 0; padding: 2px 2px;">
                                        </div> --}}
                                    </div>
                                </div>
                                {{-- <div class="col-6">
                                    <div class="form-group row">
                                        <label for="staticEmail" class="offset-1 col-sm-4 col-form-label">Receiving: </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" id="staticEmail" style="border: none; border-bottom: 1px solid black; border-radius: 0; padding: 2px 2px;">
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="col-12 text-center">
                                    <table class="table table-bordered" style="border: 1px solid black;">
                                        <thead>
                                            <tr>
                                                <th class="p-0" colspan="3">
                                                    <h3 class="text-center"><u>NET</u></h3>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="p-0">
                                                    <p>Total Allowances: <span id="entitlement_net" class="entitlement_net"></span></p>
                                                </td>
                                                <td class="p-0">
                                                    <p>Less Deduction: <span id="deduction_net" class="deduction_net"></span></p>
                                                </td>
                                                <td class="p-0">
                                                    <p>Net Payable: <u><span id="net_all"></span></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    {{-- <p>Net Payable: <input class="net_all" type="text" style="border-width: 0 0 2px 0; outline: none;"></p> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-end">
                        <button type="submit" class="btn btn-success form_save"><i class="fas fa-save" style="margin-right: 2px"></i></i> Save</button>
                        <button class="btn btn-danger" onclick="printDiv()"><i class="fas fa-print" style="margin-right: 2px"></i></i> Print</button>
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $(".tdmodal").on('click', function() {
            $('#inputmodal').modal('show');
        });
        $(".ok_btn").on("click", function() {
            $('#inputmodal').modal('hide');
        });
        $("#department").change(function() {
            var optVal = $(this).val();
            if (optVal.length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_terminated_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employee').empty();
                        $('#employee').html('<option value="">Select Employee</option>');
                        $.each(response, function(index, value) {
                            $('#employee').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - ' + value.first_name + ' ' + value.middle_name + ' ' + value.last_name + " - " + value.desigination)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            } else {
                $('#employee').empty();
            }
        });

        $("#add_form").submit(function(e) {
            ButtonStatus('.form_save', true);
            blockUI();
            e.preventDefault();
            var formData = new FormData();
            formData.append('emp_id', $('#emp_id').val());
            formData.append('resignation', $('#resignation').val());
            formData.append('termination', $('#termination').val());
            formData.append('layoff', $('#layoff').val());
            formData.append('day_payable', $('#days_payable').val());
            formData.append('days_payable_remarks', $('#days_payable_remarks').val());
            formData.append('unpaid', $('#unpaid').val());
            formData.append('unpaid_remarks', $('#unpaid_remarks').val());
            formData.append('leave_encash', $('#leave_encash').val());
            formData.append('leave_encash_remarks', $('#leave_encash_remarks').val());
            formData.append('extra_leaves', $('#extra_leaves').val());
            formData.append('extra_leaves_remarks', $('#extra_leaves_remarks').val());
            formData.append('overtime_1', $('#overtime_1').val());
            formData.append('overtime_1_remarks', $('#overtime_1_remarks').val());
            formData.append('notice_period', $('#notice_period').val());
            formData.append('notice_period_remarks', $('#notice_period_remarks').val());
            formData.append('overtime_2', $('#overtime_2').val());
            formData.append('overtime_2_remarks', $('#overtime_2_remarks').val());
            formData.append('eobi', $('#eobi').val());
            formData.append('eobi_remarks', $('#eobi_remarks').val());
            formData.append('food', $('#food').val());
            formData.append('food_remarks', $('#food_remarks').val());
            formData.append('tardiness', $('#tardiness').val());
            formData.append('tardiness_remarks', $('#tardiness_remarks').val());
            formData.append('arrears', $('#arrears').val());
            formData.append('arrears_remarks', $('#arrears_remarks').val());
            formData.append('loan_advance', $('#loan_advance').val());
            formData.append('loan_advance_remarks', $('#loan_advance_remarks').val());
            formData.append('others_1', $('#others_1').val());
            formData.append('others_1_remarks', $('#others_1_remarks').val());
            formData.append('others_2', $('#others_2').val());
            formData.append('others_2_remarks', $('#others_2_remarks').val());
            formData.append('entitlement_net', $('#entitlement_net').text());
            formData.append('deduction_net', $('#deduction_net').text());
            formData.append('net_all', $('#net_all').text());
            formData.append('notice_period_value', $('#notice_period_value').text());
            formData.append('basic_salary', $('#basic_salary').text());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('final_record')}}",
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                responsive: true,
                success: function(response) {
                    ButtonStatus('.form_save', false);
                    $.unblockUI();
                    console.log(response);
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        // $('#add_form')[0].reset();
                        $(".select2").val('').trigger('change')
                        $('#inputmodal').modal('hide');
                        Toast.fire({
                            icon: 'success',
                            title: 'Final Settlement Record has been Added Successfully!'
                        })
                    }

                }
            });
        });
    });

    $(document).ready(function() {
        //this calculates values automatically 
        // sum();
        // $("#gratuity_s,#day_p_a1,#good_a_p1,#miscellaneous_a,#tax_s,#days_ded,#miscellaneous_d,#notice_p").on("keydown keyup", function() {

        //     sum();
        // });
        function updateTotalAllowances() {
        var total = 0;
        $('.allowance-input').each(function() {
            var value = parseFloat($(this).val()) || 0;
            total += value;
        });
        $('.entitlement_all').text(total);
        $('.entitlement_net').text(total);
        updateTotalBoth();
    }

    // Function to calculate and update the total DEDUCTIONS
    function updateTotalDeductions() {
        var total = 0;
        $('.deduction-input').each(function() {
            var value = parseFloat($(this).val()) || 0;
            total += value;
        });
        $('.deduction_all').text(total);
        $('.deduction_net').text(total);
        updateTotalBoth();
    }
    function updateTotalBoth() {
        var totalAllowancesText = $('.entitlement_all').text();
        var totalDeductionsText = $('.deduction_all').text();
        var totalAllowances = parseFloat(totalAllowancesText) || 0;
        var totalDeductions = parseFloat(totalDeductionsText) || 0;
        var totalBoth = totalAllowances - totalDeductions;
        $('#net_all').text(totalBoth);
    }
    // Add input event listeners to the ALLOWANCES and DEDUCTIONS input fields
    $('.allowance-input').on('input', updateTotalAllowances);
    $('.deduction-input').on('input', updateTotalDeductions);
});
function convertToSpans() {
    $('#printableContent .convert-input-to-span').each(function() {
        var spanElement = $('<span>').text($(this).val());
        $(this).replaceWith(spanElement);
    });
}
function convertToInputs() {
    $('#printableContent span').each(function() {
        var inputElement = $('<input>').attr('type', 'text').addClass('convert-input-to-span').addClass('form-control').val($(this).text());
        $(this).replaceWith(inputElement);
    });
}

    function printDiv() {
        convertToSpans();
        var divToPrint = document.getElementById("termination_letter_div");
        const WinPrint = window.open("");
        WinPrint.document.write(
            `<!DOCTYPE html>
                    <head>
                    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
                    <style>
                        body {
                            background-color: #fff;
                        }
                        #main_table td {
                            padding: 6px 10px;
                        }
                    </style>
                    </head>
                    <body>
                    ${WinPrint.document.write(divToPrint.innerHTML)}
                    </body>
                    </html>`
        )
        setTimeout(function() {
            WinPrint.print();
            WinPrint.close();
        }, 1000);
        WinPrint.focus();
        convertToInputs();
    }
</script>
@endsection