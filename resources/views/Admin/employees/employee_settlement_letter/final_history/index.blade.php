@extends('Admin.layouts.master')

@section('title', 'Final Settlement Letter') 
@section('style')
<style>
    #main_table td {
       padding: 6px 10px;
    }
 </style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Final_Settlement_Letter')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item">@lang('app.Employees')
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Final_Settlement_History')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card p-2">
                    <table class="table" id="dataTable">
                        <thead>
                            <tr>
                                <th class="not_include"></th>
                                <th>Sr.No</th>
                                <th>Employee ID</th>
                                {{-- <th>Employee Code</th> --}}
                                <th>Employee</th>
                                <th class="not_include">First Name</th>
                                <th class="not_include">Last Name</th>
                                <th>Department</th>
                                <th class="not_include">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ url('settlement_history') }}",
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'DT_RowIndex',
                        name : 'DT_RowIndex',
                        searchable: false,
                    },
                    {
                        data : 'employee_id',
                        name : 'employees.employee_id', 
                    },   
                    // {
                    //     data: 'employee_code',
                    //     name : 'employees.employee_code',
                    // },
                    {
                        render: function(data, type, full, meta) {
                            var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                            var lastName = full['last_name'] != null ? full['last_name'] : '';
                            return full['first_name'] + ' ' + middleName+ ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'title',
                        name : 'companystructures.title',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            var createUrl = "{{ url('settlement_history_print/:id') }}";
                            createUrl = createUrl.replace(':id', full.id);
                            return (
                                '&nbsp;&nbsp;<a href="'+createUrl+'" class="item-edit" >' +
                                feather.icons['eye'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                        {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    // {
                    //     text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                    //     className: 'create-new btn btn-primary',
                    //     attr: {
                    //         'data-bs-toggle': 'modal',
                    //         'data-bs-target': '#add_modal'
                    //     },
                    //     init: function (api, node, config) {
                    //         $(node).removeClass('btn-secondary');
                    //     }
                    // },
                    {
                        text: feather.icons['arrow-right'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'FInal Settlement Letter',
                        className: 'create-new ms-1  btn btn-primary',
                        action: function(e, dt, node, config) {
                            window.location.href = '{{ url('settlement_letter') }}';
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
        });


           
    </script>
@endsection