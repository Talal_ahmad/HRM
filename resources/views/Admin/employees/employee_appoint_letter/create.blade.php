@extends('Admin.layouts.master')

@section('title', 'Appoint Letter')
@section('style')
<style>
    .no-border{
        border: none;
        border-bottom: 1px solid;
    }
    /* p{
        line-height: 25px !important;
    } */
    .fieldset {
    padding: 25px !important;
    border: 1px solid black !important;
    }
    /* .width{
        width: 70%;
    } */
</style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Employment_Letter')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item">@lang('app.Employees')
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Letter')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="basic-tabs-components">
            <ul class="nav nav-tabs" role="tablist">
                {{-- @if ($template_check_1) --}}
                    <li class="nav-item">
                        <a class="nav-link active" id="template-1-tab" data-bs-toggle="tab" href="#template-1-component" aria-controls="template-1-component" role="tab" aria-selected="true">Template 1</a>
                    </li>
                {{-- @endif --}}
                {{-- @if ($template_check_2)
                    @if (empty($template_check_1))
                        <li class="nav-item">
                            <a class="nav-link active" id="template-2-tab" data-bs-toggle="tab" href="#template-2" aria-controls="template-2" role="tab" aria-selected="false">Template 2</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" id="template-2-tab" data-bs-toggle="tab" href="#template-2" aria-controls="template-2" role="tab" aria-selected="false">Template 2</a>
                        </li>
                    @endif
                @endif --}}
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="template-1-component" aria-labelledby="template-1-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card p-2">
                                    <form class="form" id="get_data">
                                        <div class="row">
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="title">@lang('app.Department')</label>
                                                    <select name="department" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')">
                                                        <option value="">@lang('app.Select_Department')</option>
                                                        @foreach (departments() as $department)
                                                            <option value="{{$department->id}}">{{$department->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="title">@lang('app.Employee')</label>
                                                    <select name="employee" id="employee" data-placeholder="@lang('app.Select_Employee')" class="select2 form-select">
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">@lang('app.Designation'):</label>
                                                    <select name="designationFilter" id="designationFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Designation')" required>
                                                        <option value=""></option>
                                                        @foreach (designation() as $jobTitle)
                                                            <option value="{{$jobTitle->name}}">{{$jobTitle->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="title">@lang('app.Date_of_Joining')</label>
                                                    <input type="text" id="joining" class="form-control flatpickr-basic text-left" placeholder="YYYY-mm-dd" name="date" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-end">
                                            <a href="{{ route('appoint_letter.index') }}" class="btn btn-success">@lang('app.Back')</a>
                                            <button type="submit" class="btn btn-primary" id="save">@lang('app.Submit')</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="card" id="letter">
                                    <div class="fieldset">
                                        <h3 class="text-center">Joining Slip</h3>
                                        <p>Date Of Joining: <span id="date"></span></p>
                                        <div class="text-center p-1">
                                            @if (!empty($company_setup->logo))
                                                <img style="width:200px;" src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
                                            @else
                                                <img src="{{asset('images/company_logo/')}}" alt="Logo">
                                            @endif
                                        </div>
                                        <h3 class="text-center">HUMAN RESOURCE DEPARTMENT</h3>
                                        <p>To:</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Branch Manager</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="dept"></span></p>
                
                                        <div class="mx-2">
                                            <p>Please accommodate Mr/Miss <span id="box1"><input type="text" id="input1" class="no-border"></span><span id="box2"> S/O, D/O <input type="text" id="parent" class="no-border"></span> as <span id="box3">(Desigination) <input type="text" id="desig" class="no-border"></span> in your branch his/her salary package will be <span id="box4"><input type="text" id="salary" class="no-border"></p></span>
                                            <p>Special Note: <span id="box5"><textarea name="" id="special_note" cols="100" rows="1"></textarea></span>.</p>
                                            {{-- <p>This appointment will be effective <span id="box2"><input type="text" id="input2" class="no-border"></span>. Your annualized salary will be <span  id="box3"><input type="text" id="input3" class="no-border"></span>. In addition to your salary, you will be elligible for fringe benefits extended to our Executive, Administrator and Professional staff.</p> --}}
                                        </div>
                                        <br>
                                        <br>
                                        <div class="mx-2">
                                            <p><span>Director HR</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-right">General Manager</span></p>
                                            @if (!empty($company_setup->hr && $company_setup->gm))
                                            <p><span>{{$company_setup->first_name}} {{$company_setup->last_name}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-right">{{$company_setup->gm_fName}} {{$company_setup->gm_lName}}</span></p>
                                            @endif
                                        </div>
                                        @if (env('COMPANY') == 'CLINIX')
                                            <div class="mx-2">
                                                <img style="width: 625px; height:400px" src="{{asset('company_documents/new join slip.png')}}" alt="Logo">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-end">
                                <button class="btn btn-success" id="saveButton"><i class="fas fa-save" style="margin-right: 2px"></i></i>Save</button>
                                <button class="btn btn-danger" onclick="printDiv()"><i class="fas fa-print" style="margin-right: 2px"></i></i>Print</button>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab-pane " id="template-2" aria-labelledby="template-2-tab" role="tabpanel">
                    <section id="basic-datatable">
                        
                    </section>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employee').empty();
                            $('#employee').html('<option value="">Select Employee</option>'); 
                            $.each(response, function(index, value) {
                                $('#employee').append(
                                    $('<option ></option>').val(value.id).html(
                                        value.employee_id + ' - ' + value.first_name +' '+value.middle_name +' '+ value.last_name + ' - ' + value.designation)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employee').empty();
                }
            });
            console.log($('#employee').val());
            $("#save").on("click", function (e) {
                e.preventDefault();

                var rowid = $('#employee').val();
                $('#date').text($('#joining').val());
                $('#dept').text($('#department').find('option:selected').text());
                $('#desig').val($('#designationFilter').val());
                
                // var formData = new FormData(this);
                if (rowid) {
                    blockUI();
                    $.ajax({
                        url: "{{url('appoint_letter')}}" + "/" + rowid,
                        type: "get",
                        // data: formData,
                        processData: false,
                        contentType: false,
                        success: function (response) 
                        {
                            $.unblockUI();

                            if(response.errors){
                                $.each( response.errors, function( index, value ){
                                    Toast.fire({
                                        icon: 'error',
                                        title: value
                                    })
                                });
                            }
                            else if(response.error_message){
                                Toast.fire({
                                    icon: 'error',
                                    title: 'An error has been occured! Please Contact Administrator.'
                                })
                            }else{
                                $('#input1').val(response.employees_data.first_name + (response.employees_data.middle_name ? ' ' + response.employees_data.middle_name : '') + ' ' +response.employees_data.last_name);
                                $('#parent').val(response.employees_data.father_name);
                            }  
                        }
                    });
                }
            });
            $('#saveButton').on('click', function() {
            var data = {
                employee: $('#employee').val(),
                department: $('#department').val(),
                name:$('#input1').val(),
                f_name:$('#parent').val(),
                date_of_print: $('#joining').val(),
                salary: $('#salary').val(),
                note: $('#special_note').val(),
                desigination: $('#desig').val(),
            };
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('appoint_letter.store')}}", // Replace with the actual URL of your server function
                type: 'POST', // or 'GET' depending on how your server handles it
                data: data,
                headers: {
                    'X-CSRF-Token': csrfToken // Include the CSRF token in the headers
                },
                success: function(response){
                    ButtonStatus('.form_save',false);
                    $.unblockUI();
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        Toast.fire({
                            icon: 'success',
                            title: 'Appointment Letter Record has been Added Successfully!'
                        })
                        window.location.href = "{{route('appoint_letter.index')}}";
                    }
                }
            });
        });
        });
        function printDiv(){
            var input1 = $('#input1').val();
            var input2 = $('#parent').val();
            var input3 = $('#salary').val();
            var input4 = $('#special_note').val();
            var input5 = $('#desig').val();
            $('#box1').html(`<span>${input1}</span>`);
            $('#box2').html(`<span> S/O D/O ${input2}</span>`);
            $('#box4').html(`<span> ${input3}</span>`);
            $('#box5').html(`<span> ${input4}</span>`);
            $('#box3').html(`<span> ${input5}</span>`);
            var printContents = document.getElementById('letter').innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;
            window.print();
            
            // Restore the original content after printing
            document.body.innerHTML = originalContents;

            // Reload the page after printing
            location.reload();
        }
    </script>
@endsection