@extends('Admin.layouts.master')

@section('title', 'Appoint Letter')
@section('style')
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Employment_Letter')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item">@lang('app.Employees')
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Employment_Letter')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Name</th>
                                        <th>Father</th>
                                        <th>salary</th>
                                        <th>note</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--start cancel Modal -->
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Cancel_Letter')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="remarks">@lang('app.Remarks')</label>
                                        <input type="text" id="remarks" class="form-control" placeholder="@lang('app.Write_the_reason')" name="remarks" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                            <button type="submit" class="form_save btn btn-danger" id="update">@lang('app.Cancel_the_Letter')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End cancel Modal -->
    </div>
</section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
            responsive: true,
                ajax: "{{ route('appoint_letter.index') }}",
                columns: [
                    // {
                    //     data: 'responsive_id'
                    // },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },

                    {
                        data: 'id',
                        name: 'id',
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'f_name',
                        name: 'f_name',
                    },
                    {
                        data: 'salary',
                        name: 'salary',
                    },
                    {
                        data: 'note',
                        name: 'note',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            var createUrl = "{{ route('appoint_letter.create', ':id') }}";
                            createUrl = createUrl.replace(':id', full.id);
                            return (
                                '&nbsp;&nbsp;<a href="'+createUrl+'" class="item-edit" >' +
                                feather.icons['eye'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                ' '+
                                '<a href="javascript:;" onclick="cancel('+full.id+')">' +
                                feather.icons['x-circle'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'btn btn-primary',
                        action: function (e, dt, node, config)
                        {
                            window.location.href = '{{url("print_letter")}}';
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employee').empty();
                            $('#employee').html('<option value="">Select Employee</option>'); 
                            $.each(response, function(index, value) {
                                $('#employee').append(
                                    $('<option ></option>').val(value.id).html(
                                        value.employee_id + ' - ' + value.first_name +' '+value.middle_name +' '+ value.last_name + ' - ' + value.designation)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employee').empty();
                }
            });

            $("#save").on("click", function (e) {
                blockUI();
                e.preventDefault();
                var rowid = $('#employee').val();
                $('#date').text($('#joining').val());
                $('#box3').text($('#department').find('option:selected').text());

                // var formData = new FormData(this);
                $.ajax({
                    url: "{{url('appoint_letter')}}" + "/" + rowid,
                    type: "get",
                    // data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) 
                    {
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }else{
                            console.log(response.employees_data.father_name);
                            $('#input1').val(response.employees_data.first_name + (response.employees_data.middle_name ? ' ' + response.employees_data.middle_name : '') + ' ' +response.employees_data.last_name);
                            $('#parent').val(response.employees_data.father_name);
                            $('#box3').html(`<span>${response.employees_data.desigination}</span>`);
                        }  
                    }
                });
            });
            $('#saveButton').on('click', function() {
            var data = {
                employee: $('#employee').val(),
                name:$('#input1').val(),
                f_name:$('#parent').val(),
                date_of_print: $('#joining').val(),
                salary: $('#salary').val(),
                note: $('#special_note').val(),
            };
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('appoint_letter.store')}}", // Replace with the actual URL of your server function
                type: 'POST', // or 'GET' depending on how your server handles it
                data: data,
                headers: {
                    'X-CSRF-Token': csrfToken // Include the CSRF token in the headers
                },
                success: function(response){
                    ButtonStatus('.form_save',false);
                    $.unblockUI();
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        Toast.fire({
                            icon: 'success',
                            title: 'Appointment Letter Record has been Added Successfully!'
                        })
                    }
                }
            });
        });
         // Update record
         $("#edit_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{url('appoint_letter')}}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Letter has been cancelled Successfully!'
                            })
                        }
                        
                    }
                });
            });
        });
        function cancel(id) {
            rowid = id;
            $("#edit_modal").modal("show");
        }
        function view(id) {
            rowid = id;
            $.ajax({
                url: "{{url('bank')}}" + "/" + id + "/create",
                type: "get",
                data: { type: 'view' },
                success: function (response) {
                    $("#edit_modal").modal("show");
                },
            });
        }
        function printDiv(){
            var input1 = $('#input1').val();
            var input2 = $('#parent').val();
            var input3 = $('#salary').val();
            var input4 = $('#special_note').val();
            $('#box1').html(`<span>${input1}</span>`);
            $('#box2').html(`<span> S/O D/O ${input2}</span>`);
            $('#box4').html(`<span> ${input3}</span>`);
            $('#box5').html(`<span> ${input4}</span>`);
            var printContents = document.getElementById('letter').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
		}
    </script>
@endsection