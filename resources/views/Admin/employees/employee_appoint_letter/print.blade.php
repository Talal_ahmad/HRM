@extends('Admin.layouts.master')

@section('title', 'Appoint Letter')
@section('style')
<style>
    .no-border{
        border: none;
        border-bottom: 1px solid;
    }
    /* p{
        line-height: 25px !important;
    } */
    .fieldset {
    padding: 25px !important;
    border: 1px solid black !important;
    }
    /* .width{
        width: 70%;
    } */
</style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Employment Letter</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Employees
                            </li>
                            <li class="breadcrumb-item active">Print
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="basic-tabs-components">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="template-1-tab" data-bs-toggle="tab" href="#template-1-component" aria-controls="template-1-component" role="tab" aria-selected="true">Template 1</a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" id="template-2-tab" data-bs-toggle="tab" href="#template-2" aria-controls="template-2" role="tab" aria-selected="false">Template 2</a>
                </li> --}}
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="template-1-component" aria-labelledby="template-1-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card" id="letter">
                                    <div class="fieldset">
                                        <div class="text-end"><u>Sr No: {{$print->id}}</u></div>
                                        <div class="text-center pb-1">
                                            @if (!empty($company_setup->logo))
                                                <img style="width:200px;" src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
                                            @else
                                                <img src="{{asset('images/company_logo/')}}" alt="Logo">
                                            @endif
                                        </div>
                                        <h3 class="text-center">Joining Slip</h3>
                                        <p>Date Of Joining: {{$print->date_of_record}}</p>
                                        <h3 class="text-center">HUMAN RESOURCE DEPARTMENT</h3>
                                        <p>To:</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Branch Manager</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$print->department}}</p>
                                        <div class="mx-2">
                                            <p>Please accommodate Mr/Miss {{$print->name}} S/O, D/O {{$print->f_name}} as {{$print->desigination}} in your branch his/her salary package will be {{$print->salary}}</p>
                                            <p>Special Note: {{$print->note}}.</p>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="mx-2">
                                            <p><span>Director HR</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-right">General Manager</span></p>
                                            @if (!empty($company_setup->hr && $company_setup->gm))
                                            <p><span>{{$company_setup->first_name}} {{$company_setup->last_name}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-right">{{$company_setup->gm_fName}} {{$company_setup->gm_lName}}</span></p>
                                            @endif
                                        </div>
                                        @if (env('COMPANY') == 'CLINIX')
                                            <div class="mx-2">
                                                <img style="width: 625px; height:400px" src="{{asset('company_documents/new join slip.png')}}" alt="Logo">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-end">
                                <a href="{{ route('appoint_letter.index') }}" class="btn btn-success">Back</a>
                                <button class="btn btn-danger" id="printButton" onclick="printDiv()"><i class="fas fa-print" style="margin-right: 2px"></i></i>Print</button>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab-pane " id="template-2" aria-labelledby="template-2-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card p-2" id="letter">
                                    <div class="m-2 fieldset">
                                        <div class="text-center pb-2">
                                            @if (!empty($company_setup->logo))
                                                <img style="width:200px;" src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
                                            @else
                                                <img src="{{asset('images/company_logo/')}}" alt="Logo">
                                            @endif
                                        </div>
                                        <h3 class="text-center mb-2">Joining Slip 2</h3>
                                        <p>Date: {{$print->date_of_record}}</p>
                                        <h3 class="text-center mb-2">HUMAN RESOURCE DEPARTMENT</h3>
                                        <p>To:</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Branch Manager</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$print->department}}</p>
                                        <div class="mx-2">
                                            <p>Please accommodate Mr/Miss {{$print->name}} S/O, D/O {{$print->f_name}} as {{$print->desigination}}</p>
                                            <p>in your branch his/her salary package will be {{$print->salary}}</p>
                                            <p>Special Note: {{$print->note}}.</p>
                                        </div>
                                        <div class="mx-2">
                                            <p><span>Director HR</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-right">General Manager</span></p>
                                            @if (!empty($company_setup->hr && $company_setup->gm))
                                            <p><span>{{$company_setup->first_name}} {{$company_setup->last_name}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-right">{{$company_setup->gm_fName}} {{$company_setup->gm_lName}}</span></p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-end">
                                <button class="btn btn-danger" onclick="printDiv()"><i class="fas fa-print" style="margin-right: 2px"></i></i>Print</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection
@section('scripts')
    <script>
         $(document).ready(function() {
                $('#printButton').click(function() {
                var printContents = document.getElementById('letter').innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
                location.reload();
            });
        });
    </script>
@endsection