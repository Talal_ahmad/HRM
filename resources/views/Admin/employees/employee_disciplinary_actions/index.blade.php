@extends('Admin.layouts.master')
@section('title', 'Employees Disciplinary/Enquiry')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Disciplinary/Enquiry')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Employees')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Disciplinary/Enquiry')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header row"></div>
        <div class="content-body">
            <section id="basic-dataTable">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Title</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Enquiry Start On</th>
                                        <th>Enquiry End On</th>
                                        <th>Inquiry Officer</th>
                                        <th>Designation</th>
                                        <th>Findings</th>
                                        <th>Action Taken</th>
                                        <th>Description</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <div class="modal-size-lg d-inline-block">
                <!-- Add Modal -->
                <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Disciplinary/Enquiry')</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form class="form" id="add_form">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">@lang('app.Title')</label>
                                                <input type="text" name="title" id="title" class="form-control" placeholder="@lang('app.Title')" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <label class="form-label">@lang('app.Employee'):</label>
                                            <select name="employee_id" id="employee" class="select2 form-select filter" data-placeholder="@lang('app.Select_Employee')" required>
                                                <option value="">@lang('app.Select_Employee')</option>
                                                @foreach (employees() as $employee)
                                                    <option value="{{ $employee->id }}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{ $employee->first_name }}
                                                        {{ $employee->last_name }}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="start_date">@lang('app.Inquiry_Start_On')</label>
                                                    <input type="text" name="start_date" id="start_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="end_date">@lang('app.Inquiry_End_On')</label>
                                                    <input type="text" name="end_date" id="end_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="inquiry_officer">@lang('app.Inquiry_Officer')</label>
                                                    <input type="text" name="inquiry_officer" id="inquiry_officer" class="form-control" placeholder="@lang('app.Inquiry_Officer')"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="designation">@lang('app.Designation')</label>
                                                <input type="text" name="designation" id="designation" class="form-control" placeholder="@lang('app.Designation_of_Inquiry_Officer')"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="findings">@lang('app.Findings')</label>
                                                <textarea id="findings" class="form-control" cols="20" rows="2" placeholder="@lang('app.Type_here')" name="findings"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="action_taken">@lang('app.Action_Taken')</label>
                                                <textarea id="action_taken" class="form-control" cols="20" rows="2" placeholder="@lang('app.Type_here')" name="action_taken"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="description">@lang('app.Description')</label>
                                                <textarea id="description" class="form-control" cols="20" rows="2" placeholder="@lang('app.Type_here')" name="description"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="type">@lang('app.Petition/Case_Copy')</label>
                                                <input type="file" id="type" class="form-control" value="" placeholder="" name="attachment1"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="type">@lang('app.Employee_Statement')</label>
                                                <input type="file" id="type" class="form-control" value="" placeholder="" name="employee_statement"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                    <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Edit Modal -->
                <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Disciplinary/Enquiry')</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form class="form" id="edit_form">
                                @csrf
                                @method('PUT')
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_title">@lang('app.Title')</label>
                                                    <input type="text" name="title" id="edit_title" class="form-control" placeholder="@lang('app.Title')" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_employee_id">@lang('app.Employee')</label>
                                                    <select name="employee_id" id="edit_employee_id" class="select2 form-select">
                                                        <option value="">@lang('app.Select_Employee')</option>
                                                        @foreach (employees() as $employee)
                                                        <option value="{{ $employee->id }}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{ $employee->first_name }}
                                                            {{ $employee->middle_name }} {{ $employee->last_name }}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_start_date">@lang('app.Inquiry_Start_On')</label>
                                                    <input type="text" name="start_date" id="edit_start_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_end_date">@lang('app.Inquiry_End_On')</label>
                                                <input type="text" name="end_date" id="edit_end_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_inquiry_officer">@lang('app.Inquiry_Officer')</label>
                                                <input type="text" name="inquiry_officer" id="edit_inquiry_officer" class="form-control" placeholder="@lang('app.Inquiry_Officer')" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_designation">@lang('app.Designation')</label>
                                                <input type="text" name="designation" id="edit_designation" class="form-control" placeholder="@lang('app.Designation_of_Inquiry_Officer')"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_findings">@lang('app.Findings')</label>
                                                <textarea id="edit_findings" class="form-control" cols="20" rows="2" placeholder="@lang('app.Type_here')" name="findings"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_action_taken">@lang('app.Action_Taken')</label>
                                                <textarea id="edit_action_taken" class="form-control" cols="20" rows="2" placeholder="@lang('app.Type_here')" name="action_taken"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_description">@lang('app.Description')</label>
                                                <textarea id="edit_description" class="form-control" cols="20" rows="2" placeholder="@lang('app.Type_here')" name="description"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="editAttachment">@lang('app.Petition/Case_Copy')</label>
                                                <input type="file" id="editAttachment" class="form-control" value="" placeholder="" name="attachment1"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="editStatement">@lang('app.Employee_Statement')</label>
                                                <input type="file" id="editStatement" class="form-control" value="" placeholder="" name="employee_statement"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                    <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Update')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: true,
                ajax: "{{ route('employee_disciplinary_action.index') }}",
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'title',
                        name: 'disciplinary_inquiry.title',
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        data: 'employee',
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + ' ' + middleName + ' ' + lastName;
                        },
                        orderable:false,
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'start_date',
                        name: 'disciplinary_inquiry.start_date',
                    },
                    {
                        data: 'end_date',
                        name: 'disciplinary_inquiry.end_date',
                    },
                    {
                        data: 'inquiry_officer',
                        name: 'disciplinary_inquiry.inquiry_officer',
                    },
                    {
                        data: 'designation',
                        name: 'disciplinary_inquiry.designation',
                    },
                    {
                        data: 'findings',
                        name: 'disciplinary_inquiry.findings',
                    },
                    {
                        data: 'action_taken',
                        name: 'disciplinary_inquiry.action_taken',
                    },
                    {
                        data: 'description',
                        name: 'disciplinary_inquiry.description',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('employee_disciplinary_action.store') }}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Disciplinary/Enquiry has been Added Successfully!'
                            })
                        }
                    }
                });
            })
            $('#edit_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('employee_disciplinary_action') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#edit_form')[0].reset();
                            $("#edit_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Disciplinary/Enquiry has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            $('div.head-label').html('<h6 class="mb-0">List of Employees Disciplinary/Enquiries</h6>');
        });

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('employee_disciplinary_action') }}" + "/" + rowid + "/edit",
                type: "get",
                success: function(response) {
                    console.log(response);
                    $("#edit_title").val(response.title);
                    $("#edit_employee_id").val(response.employee_id).select2();
                    $("#edit_start_date").val(response.start_date);
                    $("#edit_end_date").val(response.end_date);
                    $("#edit_inquiry_officer").val(response.inquiry_officer);
                    $("#edit_designation").val(response.designation);
                    $("#edit_findings").val(response.findings);
                    $("#edit_action_taken").val(response.action_taken);
                    $("#edit_description").val(response.description);
                    $("#edit_modal").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "employee_disciplinary_action/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Disciplinary/Enquiry has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
