@extends('Admin.layouts.master')

@section('title', 'Joining Report')
@section('style')
<style>
    .no-border{
        border: none;
        border-bottom: 1px solid;
    }
    p{
        line-height: 35px !important;
    }
    .fieldset {
    padding: 25px !important;
    border: 1px solid black !important;
    }
    .width{
        width: 70%;
    }
</style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Joining_Report')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item">@lang('app.Employees')
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Joining_Report')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card p-2">
                    <form class="form" id="get_data">
                        <div class="row">
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="title">@lang('app.Department')</label>
                                    <select name="department" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                        <option value="">@lang('app.Select_Department')</option>
                                        @foreach (departments() as $department)
                                            <option value="{{$department->id}}">{{$department->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="title">@lang('app.Employee')</label>
                                    <select name="employee" id="employee" data-placeholder="@lang('app.Select_Employee')" class="select2 form-select" required>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="title">@lang('app.Date_Of_Joining')</label>
                                    <input type="text" id="joining" class="form-control flatpickr-basic text-left" placeholder="@lang('app.Joining_Date')" name="date" required />
                                </div>
                            </div>
                        </div>
                        <div class="text-end">
                            <button type="submit" class="btn btn-primary" id="save">@lang('app.Submit')</button>
                        </div>
                    </form>
                </div>
                <div class="card p-2" id="letter">
                    <div class="m-2 fieldset">
                        <h3 class="text-center mb-2">Regular Staff Members</h3>
                        <p>Date: <span id="date"></span></p>
                        <p>Dear: <span id="name"></span></p>
                        <h3 class="text-center mb-2">Regular </h3>

                        <div class="mx-2">
                        @if (env('COMPANY') == 'HEALTHWISE')
                            <p>I am pleased to formally offer you the position of <span id="box1"><input type="text" id="input1" class="no-border"></span> (title)(department) at HealthWise.</p>
                        @elseif(env('COMPANY') == 'Ajmal Dawakhana')
                            <p>I am pleased to formally offer you the position of <span id="box1"><input type="text" id="input1" class="no-border"></span> (title)(department) at Ajmal Dawakhana Branch.</p>
                        @else
                            <p>I am pleased to formally offer you the position of <span id="box1"><input type="text" id="input1" class="no-border"></span> (title)(department) at {{env('company')}} Branch.</p>
                        @endif
                            <p>Your Appointment will be forwarded to the Board of Trustees for confirmation at their next meeting.</p>
                            <p>This appointment will be effective <span id="box2"><input type="text" id="input2" class="no-border"></span>. Your annualized salary will be <span  id="box3"><input type="text" id="input3" class="no-border"></span>. In addition to your salary, you will be elligible for fringe benefits extended to our Executive, Administrator and Professional staff.</p>
                        </div>
                        {{-- @if (env('COMPANY') == 'HEALTHWISE')
                            <p>Welcome to HealthWise!</p>
                        @elseif(env('COMPANY') == 'Ajmal Dawakhana')
                            <p>Welcome to Ajmal Dawakhana!</p>
                        @else
                            <p>Welcome to Clinix!</p>
                        @endif --}}
                        <p>Welcome to {{env('company')}}!</p>
                        <p>Sincerely,</p>
                        <div class="row d-flex justify-content-between width">
                            <div class="col-md-6">
                                <p>Name:</p>
                            </div>
                            <div class="col-md-6">
                                <p>Signature:</p>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-between width">
                            <div class="col-md-6">
                                <p>Title:</p>
                            </div>
                            <div class="col-md-6">
                                <p>Date:</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-end">
                <button class="btn btn-danger" onclick="printDiv()"><i class="fas fa-print" style="margin-right: 2px"></i></i>Print</button>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employee').empty();
                            $('#employee').html('<option value="">Select Employee</option>'); 
                            $.each(response, function(index, value) {
                                $('#employee').append(
                                    $('<option></option>').val(value.first_name +' '+ value.last_name).html(
                                        value.employee_id + ' - ' + value.first_name +' '+value.middle_name +' '+ value.last_name + ' - ' + value.designation)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employee').empty();
                }
            });
            $('#get_data').submit(function(e){
                e.preventDefault();
                $('#date').text($('#joining').val());
                $('#name').text($('#employee').val());
            });
        });
        function printDiv(){
            var input1 = $('#input1').val();
            var input2 = $('#input2').val();
            var input3 = $('#input3').val();
            $('#box1').html(`<span>${input1}</span>`);
            $('#box2').html(`<span>${input2}</span>`);
            $('#box3').html(`<span>${input3}</span>`);
            var printContents = document.getElementById('letter').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
		}
    </script>
@endsection