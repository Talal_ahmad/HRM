@extends('Admin.layouts.master')
@section('title', 'Employee History')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Employee History Details</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Employees</a>
                                </li>
                                <li class="breadcrumb-item active">Employee History Details
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <div class="card-header">
                                <h4 class="card-title">Current Record</h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <th>Employee ID</th>
                                                        <th>Employee</th>
                                                        <th>City</th>
                                                        <th>Mobile Phone</th>
                                                        <th>Department</th>
                                                        <th>Status</th>
                                                        <th>Employment Status</th>
                                                        <th>Job Title</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>{{$updated_employee->employee_id}}</td>
                                                            <td>{{$updated_employee->first_name}} {{$updated_employee->middle_name}} {{$updated_employee->last_name}}</td>
                                                            <td>{{$updated_employee->city}}</td>
                                                            <td>{{$updated_employee->mobile_phone}}</td>
                                                            <td>{{$updated_employee->dept}}</td>
                                                            <td>{{$updated_employee->status}}</td>
                                                            <td>{{$updated_employee->employment}}</td>
                                                            <td>{{$updated_employee->job}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Changes In <span class="fw-bolder h3">{{$employee_info[0]->first_name}} {{$employee_info[0]->last_name}}</span> Record</h4>
                                <hr style="width:100%", size="3", color=black>
                            </div>
                            <div class="card-body">
                                @if (isset($employee_info[1]) && isset($employee_info[0]))
                                    @if ($employee_info[1]->emp_id !== $employee_info[0]->emp_id)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Employee ID </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->emp_id)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->emp_id}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->first_name !== $employee_info[0]->first_name)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">First Name </span>Updated From <span class="badge bg-light-danger">{{$employee_info[1]->first_name}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->first_name}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->middle_name !== $employee_info[0]->middle_name)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Middle Name </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->middle_name)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->middle_name}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->last_name !== $employee_info[0]->last_name)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Last Name </span>Updated From <span class="badge bg-light-danger">{{$employee_info[1]->last_name}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->last_name}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->marital_status !== $employee_info[0]->marital_status)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Marital Status </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->marital_status)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->marital_status}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->address1 !== $employee_info[0]->address1)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">First Address </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->address1)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->address1}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->address2 !== $employee_info[0]->address2)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Second Address </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->address2)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->address2}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->city !== $employee_info[0]->city)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">City </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->city)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->city}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->country_name !== $employee_info[0]->country_name)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Country </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->country_name)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->country_name}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->province_name !== $employee_info[0]->province_name)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Province </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->province_name)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->province_name}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->mobile_phone !== $employee_info[0]->mobile_phone)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Mobile Phone </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->mobile_phone)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->mobile_phone}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->work_email !== $employee_info[0]->work_email)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Work E-Mail </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->work_email)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->work_email}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->title !== $employee_info[0]->title)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Department </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->title)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->title}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->status !== $employee_info[0]->status)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Status </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->status)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->status}}</span></p>
                                        <p>Restore date: {{$employee_info[0]->restore}}</p>
                                    @endif
                                    @if ($employee_info[1]->emp_status_name !== $employee_info[0]->emp_status_name)
                                        <p class="fw-bolder" class="fw-bolder"><span class="badge bg-light-warning">Employment Status </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->emp_status_name)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->emp_status_name}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->job_title_name !== $employee_info[0]->job_title_name)
                                        <p class="fw-bolder" class="fw-bolder"><span class="badge bg-light-warning">Job Title </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->job_title_name)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->job_title_name}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->pay_grade_name !== $employee_info[0]->pay_grade_name)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Pay Grade </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->pay_grade_name)}}</span> To <span>{{$employee_info[0]->pay_grade_name}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->home_phone !== $employee_info[0]->home_phone)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Home Phone </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->home_phone)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->home_phone}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->work_phone !== $employee_info[0]->work_phone)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Work Phone </span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->work_phone)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->work_phone}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->work_phone !== $employee_info[0]->work_phone)
                                        <p class="fw-bolder">Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->work_phone)}}</span> To <span class="badge bg-light-success">{{$employee_info[0]->work_phone}}</span></p>
                                    @endif
                                    @if ($employee_info[1]->posting_station_name !== $employee_info[0]->posting_station_name)
                                        <p class="fw-bolder"><span class="badge bg-light-warning">Posting Station</span>Updated From <span class="badge bg-light-danger">{{HandleEmpty($employee_info[1]->posting_station_name)}}</span class="badge bg-light-success"> To <span>{{$employee_info[0]->posting_station_name}}</span></p>
                                    @endif
                                    <p class="h5 fw-bolder">Modified At: </p><span class="fw-lighter">{{$employee_info[0]->created_at}}</span>
                                    <p class="h5 fw-bolder">Modified By: </p><span class="fw-lighter">{{HandleEmpty($employee_info[0]->modified_by)}}</span>
                                @else
                                    <p class="h5 fw-bolder text-center">No Changes Made</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection