@extends('Admin.layouts.master')
@section('title', 'Employee History')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Employee_History')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Employees')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Employee_History')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card p-2">
                            <form class="form" action="{{ route('employee_history.index') }}">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">@lang('app.Employee')</label>
                                            <select name="employee_id" id="employee" data-placeholder="@lang('app.Select_Employee')" class="select2 form-select" required>
                                                <option value="">@lang('app.Select_Employee')</option>
                                                @foreach ($employees_name as $emp)
                                                    <option value="{{$emp->employee_id}}" {{!empty(request('employee_id')) ? $emp->employee_id == request('employee_id') ? 'selected' : '' : ''}}>{{$emp->employee_code}} {{$emp->first_name}} {{$emp->middle_name}} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-end">
                                    <button type="submit" class="btn btn-primary">@lang('app.Submit')</button>
                                </div>
                            </form>
                        </div>
                        <div class="card pb-2">
                            <div class="card-header border-bottom p-1" style="user-select: auto;">
                                <div class="head-label" style="user-select: auto;">
                                    <h6 class="mb-0" style="user-select: auto;">@lang('app.Employee_History')</h6>
                                </div>
                            </div>
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Description</th>
                                        <th>Created(GMT)</th>
                                        <th class="not_include">View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            var value_emp = $("#employee").val();
            console.log(value_emp);
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: true,
                ajax:{
                    url: "{{ route('employee_history.index') }}",
                    type: "GET",
                    data: {
                        value_emp:value_emp,
                    }
                }, 

                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'emp_id',
                        name: 'emp_id'
                    },
                    {
                        data: 'name' , render: function(data , type , row){
                            return row.first_name + " " + row.middle_name + " "  + row.last_name; 
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'name' , render: function(data , type , row){
                            return `<p class="badge bg-light-info">${row.first_name} ${row.middle_name} ${row.last_name}</p> Updated!`; 
                        },
                        searchable: false
                    },
                    {
                        data: 'created_at',
                    },
                    {
                        data: 'action' , render: function(data , type , row){
                            let url = "{{ route('employee_history.show', ':id') }}?type=:type";
                            url = url.replace(':id', row.employee_id);
                            url = url.replace(':type', row.created_at);
                            return `
                                <a href="${url}">
                                    <i class="fas fa-lg fa-eye text text-primary"></i>    
                                </a>
                            `;
                        },
                        searchable: false
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
        }); 
    </script>
@endsection
