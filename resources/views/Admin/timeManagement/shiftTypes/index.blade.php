@extends('Admin.layouts.master')
@section('title', 'Shift Types')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Shift_Types')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Time_Management')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Shift_Types')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Shift</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                    <th>Status</th>
                                    <th>Max Hours Per Month</th>
                                    <th>Max Days per month</th>
                                    <th>Total Working Hours</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Shift_Types')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="shift_id">@lang('app.Shift_ID')</label>
                                            <input type="text" id="shift_id" class="form-control"
                                                placeholder="@lang('app.Shift_ID')" name="shift_id" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="active">@lang('app.Status')</label>
                                        <select class="select2 form-select" name="active" id="active"
                                            data-placeholder="Select Status" required>
                                            <option value=""></option>
                                            <option value="1">@lang('app.Active')</option>
                                            <option value="0">@lang('app.In_Active')</option>
                                        </select>
                                    </div>
                                    @if (env('COMPANY') == 'JSML')
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="rotating_order">@lang('app.Rotating_Order')</label>
                                                <input type="text" id="rotating_order" class="form-control"
                                                    placeholder="@lang('app.Rotating_Order')" name="rotating_order" />
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="shift_start_time">@lang('app.Shift_Start_Time')</label>
                                            <input type="text" id="shift_start_time"
                                                class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                name="shift_start_time" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="shift_end_time">@lang('app.Shift_End_Time')</label>
                                            <input type="text" id="shift_end_time"
                                                class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                name="shift_end_time" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="hours_per_month">@lang('app.Max_Working_Hours_for_the_Month')
                                            </label>
                                            <input type="text" id="hours_per_month" class="form-control text-left"
                                                placeholder="" name="hours_per_month" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="days_per_month">@lang('app.Max_Working_Days_for_the_Month')
                                            </label>
                                            <input type="text" id="days_per_month" class="form-control text-left"
                                                placeholder="" name="days_per_month" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="shift_desc">@lang('app.Shift_Description')</label>
                                            <textarea id="shift_desc" class="form-control" cols="20" rows="2" placeholder="@lang('app.Shift_Description')"
                                                name="shift_desc"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <input class="form-check-input" type="checkbox"
                                                id="open_working_hours_checkbox" name="open_working_hours_checkbox"
                                                onchange="toggleOpenWorkingHoursInput()" />
                                            <label class="form-check-label"
                                                for="open_working_hours_checkbox">@lang('app.Open_Working_Hours')
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <input class="form-check-input" type="checkbox" id="is24HourShift"
                                                name="is_24_hour_shift" value="1" />
                                            <label class="form-check-label" for="is24HourShift">Is 24 Hour Shift</label>
                                        </div>
                                    </div>
                                    <div id="open_working_hours_div" style="display: none;">
                                        <div class="col-md-12 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="open_working_hours">@lang('app.No_of_Open_Working_Hours')
                                                </label>
                                                <input type="text" id="open_working_hours"
                                                    class="form-control text-left" placeholder=""
                                                    name="open_working_hours" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-check-inline">
                                            <input class="form-check-input" type="checkbox" id="sandwich_absent"
                                                name="sandwich_absent" value="1">
                                            <label class="form-check-label">@lang('app.Sandwich_Absent')</label>
                                        </div>
                                        @if (env('COMPANY') == 'JSML')
                                            <div class="form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="is_rotating"
                                                    name="is_rotating" value="1">
                                                <label class="form-check-label">@lang('app.Is_Rotating')</label>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger"
                                    data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="btn btn-primary" id="save">@lang('app.Save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Shift_Type')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_shift_id">@lang('app.Shift_ID')</label>
                                            <input type="text" id="edit_shift_id" class="form-control"
                                                placeholder="@lang('app.Shift_ID')" name="shift_id" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="edit_active">@lang('app.Status')</label>
                                        <select class="select2 form-select" name="active" id="edit_active"
                                            data-placeholder="@lang('app.Select_Status')" required>
                                            <option value=""></option>
                                            <option value="1">@lang('app.Active')</option>
                                            <option value="0">@lang('app.In_Active')</option>
                                        </select>
                                    </div>
                                    @if (env('COMPANY') == 'JSML')
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label"
                                                    for="edit_rotating_order">@lang('app.Rotating_Order')</label>
                                                <input type="text" id="edit_rotating_order" class="form-control"
                                                    placeholder="@lang('app.Rotating_Order')" name="rotating_order" />
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label"
                                                for="edit_shift_start_time">@lang('app.Shift_Start_Time')</label>
                                            <input type="text" id="edit_shift_start_time"
                                                class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                name="shift_start_time" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_shift_end_time">@lang('app.Shift_End_Time')</label>
                                            <input type="text" id="edit_shift_end_time"
                                                class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                name="shift_end_time" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_hours_per_month">@lang('app.Max_Working_Hours_for_the_Month')
                                            </label>
                                            <input type="text" id="edit_hours_per_month"
                                                class="form-control text-left" placeholder="" name="hours_per_month" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_days_per_month">@lang('app.Max_Working_Days_for_the_Month')
                                            </label>
                                            <input type="text" id="edit_days_per_month" class="form-control text-left"
                                                placeholder="" name="days_per_month" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_shift_desc">@lang('app.Shift_Description')</label>
                                            <textarea id="edit_shift_desc" class="form-control" cols="20" rows="2" placeholder="@lang('app.Type_here')"
                                                name="shift_desc"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <input class="form-check-input" type="checkbox"
                                                id="open_working_hours_checkbox_edit" name="open_working_hours_checkbox"
                                                onchange="toggleOpenWorkingHoursInputEdit()" />
                                            <label class="form-check-label"
                                                for="open_working_hours_checkbox_edit">@lang('app.Open_Working_Hours')

                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <input class="form-check-input" type="checkbox" id="is24HourShiftEdit"
                                                name="is_24_hour_shift" value="1" />
                                            <label class="form-check-label" for="is24HourShiftEdit">Is 24 Hour
                                                Shift</label>
                                        </div>
                                    </div>
                                    <div id="open_working_hours_div_edit" style="display: none;">
                                        <div class="col-md-12
                                        col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="open_working_hours_edit">@lang('app.No_of_Open_Working_Hours')
                                                </label>
                                                <input type="text" id="open_working_hours_edit"
                                                    class="form-control text-left" placeholder=""
                                                    name="open_working_hours" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-check-inline">
                                            <input class="form-check-input" type="checkbox" id="edit_sandwich_absent"
                                                name="sandwich_absent" value="1">
                                            <label class="form-check-label">@lang('app.Sandwich_Absent')</label>
                                        </div>
                                        @if (env('COMPANY') == 'JSML')
                                            <div class="form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="edit_is_rotating"
                                                    name="is_rotating" value="1">
                                                <label class="form-check-label">@lang('app.Is_Rotating')</label>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger"
                                        data-bs-dismiss="modal">@lang('app.Close')</button>
                                    <button type="submit" class="btn btn-primary"
                                        id="update">@lang('app.Update')</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script>
        function toggleOpenWorkingHoursInput() {
            var checkbox = document.getElementById("open_working_hours_checkbox");
            var openWorkingHoursDiv = document.getElementById("open_working_hours_div");

            if (checkbox.checked) {
                openWorkingHoursDiv.style.display = "block";
            } else {
                openWorkingHoursDiv.style.display = "none";
            }
        }

        function toggleOpenWorkingHoursInputEdit() {
            var checkbox = document.getElementById("open_working_hours_checkbox_edit");
            var openWorkingHoursDiv = document.getElementById("open_working_hours_div_edit");

            if (checkbox.checked) {
                openWorkingHoursDiv.style.display = "block";
            } else {
                openWorkingHoursDiv.style.display = "none";
            }
        }
    </script>

    <script>
        var datatable;
        var rowid;
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('shiftTypes.index') }}",
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'shift_desc',
                        name: 'shift_desc',
                    },
                    {
                        data: 'shift_start_time',
                        name: 'shift_start_time',
                    },
                    {
                        data: 'shift_end_time',
                        name: 'shift_end_time',
                    },
                    {
                        data: 'active',
                        name: 'active',
                        render: function(data, type, row) {
                            return (data === 0) ? 'Inactive' : 'Active';
                        }
                    },
                    {
                        data: 'hours_per_month',
                        name: 'hours_per_month',
                    },
                    {
                        data: 'days_per_month',
                        name: 'days_per_month',
                    },
                    {
                        render: function(data, type, row) {
                            var startTime = moment(row.shift_start_time, 'HH:mm:ss');
                            var endTime = moment(row.shift_end_time, 'HH:mm:ss');

                            // Handle the case where start time is later than end time
                            if (endTime.isBefore(startTime)) {
                                endTime.add(1, 'day'); // Add 1 day to end time
                            }

                            var duration = moment.duration(endTime.diff(startTime));
                            var totalHours = duration.hours();
                            var totalMinutes = duration.minutes();

                            return totalHours + ' hours ' + totalMinutes + ' minutes';
                        }
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }

                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Shift Types</h6>');
            // Add Record
            $("#add_form").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);

                // Add open working hours data if checkbox is checked
                if ($("#open_working_hours_checkbox").prop("checked")) {
                    formData.append("open_working_hours", $("#open_working_hours").val());
                }
                $.ajax({
                    url: "{{ route('shiftTypes.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        console.log(response);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Shift Type has been Added Successfully!'
                            })
                        }

                    }
                });
            });
            // Update record
            $("#edit_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('shiftTypes') }}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Shift Type has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
        });
        // Edit Function
        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('shiftTypes') }}" + "/" + id + "/edit",
                type: "get",
                success: function(response) {
                    $('#edit_form')[0].reset();
                    $("#edit_shift_id").val(response.shift_id);
                    $("#edit_shift_desc").val(response.shift_desc);
                    $("#edit_active").val(response.active).select2();
                    $("#edit_shift_start_time").val(response.shift_start_time);
                    $("#edit_shift_end_time").val(response.shift_end_time);
                    $("#edit_rotating_order").val(response.rotating_order);
                    $("#edit_hours_per_month").val(response.hours_per_month);
                    $("#edit_days_per_month").val(response.days_per_month);
                    $("#open_working_hours_edit").val(response.open_working_hours);
                    if (response.sandwich_absent == 1) {
                        $("#edit_sandwich_absent").prop('checked', true);
                    }
                    if (response.open_working_hours_checkbox == 1) {
                        $("#open_working_hours_checkbox_edit").prop('checked', true);
                    }
                    if (response.is_rotating == 1) {
                        $("#edit_is_rotating").prop('checked', true);
                    }
                    if (response.is_24_hour_shift == 1) {
                        $("#is24HourShiftEdit").prop('checked', true);
                    }
                    $("#edit_modal").modal("show");
                },
            });
        }
        // Delete Function
        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "shiftTypes/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Shift Type has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
