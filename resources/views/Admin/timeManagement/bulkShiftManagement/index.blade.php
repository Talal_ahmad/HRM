@extends('Admin.layouts.master')
@section('title', 'Bulk Shift management')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Bulk_Shift_Management')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Time_Management')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Bulk_Shift_Management')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="multiple-column-form">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">@lang('app.Add_Bulk_Shift_Management')</h4>
                            </div>
                            <div class="card-body">
                                <form class="form" id="bulkForm">
                                    @csrf
                                    <div class="row">
                                        @if (env('COMPANY') == 'JSML')
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="employment_status">@lang('app.Employment_Status')</label>
                                                <select name="employment_status[]" id="employment_status" class="select2 form-select" data-placeholder="@lang('app.Select_Employment_Status')" multiple>
                                                    <option value=""></option>
                                                    @foreach ($employment_status as $employment)
                                                        <option value="{{$employment->id}}" {{!empty(request('employment_status')) ? in_array($employment->id,request('employment_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#employment_status')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#employment_status')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <label class="form-label">@lang('app.Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}">{{ $department->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">@lang('app.Section')</label>
                                                <select name="section[]" id="section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                    
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                            <select name="employeeFilter[]" id="employeeFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Employee')" multiple disabled>
    
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#employeeFilter')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#employeeFilter')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                        @else
                                        <div class="col-md-6 col-12">
                                            <label class="form-label">@lang('app.Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}">{{ $department->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                            <select name="employeeFilter[]" id="employeeFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Employee')" multiple disabled>
    
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#employeeFilter')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#employeeFilter')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="first-name-column">@lang('app.Shift_Type')</label>
                                                <select name="shift_id" id="shift_id" class="select2 form-select" data-placeholder="@lang('app.Select_Shift_Type')">
                                                    <option value=""></option>
                                                    @foreach ($shift_type as $shift)
                                                        <option value="{{ $shift->id }}">{{ $shift->shift_desc }}</option> 
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="last-name-column">@lang('app.Work_Week')</label>
                                                <select name="work_week_id" id="work_week" class="select2 form-select" data-placeholder="@lang('app.Select_Work_Week')">
                                                    <option value=""></option>
                                                    @foreach ($work_week as $week)
                                                        <option value="{{ $week->id }}">{{ $week->desc }}</option> 
                                                    @endforeach
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="city-column">@lang('app.Shift_From')</label>
                                                <input type="text" id="shift_from" class="form-control flatpickr-basic text-left" placeholder="@lang('app.Shift_From')" name="shift_from_date"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="country-floating">@lang('app.Shift_To')</label>
                                                <input type="text" id="shift_to" class="form-control flatpickr-basic text-left" placeholder="@lang('app.Shift_Date')" name="shift_to_date"/>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="company-column">@lang('app.Late_Time_Allowed')</label>
                                                @if (env('COMPANY') == 'JSML' || env('COMPANY') == 'AJMAL' || env('COMPANY') == 'RoofLine')
                                                    <input type="text" id="late_time_allowed" class="form-control" value="15" name="late_time_allowed" readonly/>
                                                @else
                                                    <input type="text" id="late_time_allowed" class="form-control" value="11" name="late_time_allowed" readonly/>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <a href="{{url('shiftManagement')}}" class="btn btn-outline-danger">@lang('app.Cancel')</a>
                                            <button type="submit" class="btn btn-primary ms-1" id="save">@lang('app.Save')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
        @if (env('COMPANY') != 'JSML')        
                $("#departmentFilter").change(function() {
                    $("#employeeFilter").prop( "disabled", false );
                    department = $(this).val();
                    employment_status = $('#employment_status').val();
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: department,
                            employment_status: employment_status,
                        },
                        success: function(response) {
                            $('#employeeFilter').empty();
                            $('#employeeFilter').html(
                                '<option value="">Select Employee</option>');
                            $.each(response, function(index, value) {
                                $('#employeeFilter').append(
                                    $('<option></option>').val(value.id).html(
                                        value.employee_id + '-' + value.employee_code + '-' + value.first_name + ' ' + value.last_name)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                });
            @else
                $("#departmentFilter").change(function() {
                    var optVal = $(this).val();
                    if (optVal.length > 0) {
                        $.ajax({
                            type: "GET",
                            url: "{{ url('get_department_child') }}",
                            data: {
                                department_id: optVal,
                            },
                            success: function(response) {
                                $('#section').empty();
                                $('#section').html('<option value="">Select Section</option>'); 
                                $.each(response, function(index, value) {
                                    
                                    console.log(value.id);
                                    $('#section').append(
                                        $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                            value.title)
                                    );
                                });
                            },
                            error: function() {
                                alert('Error occured');
                            }
                        });
                    } else {
                        $('#section').empty();
                    }
                });
                $("#section").change(function() {
                    var optVal = $(this).val();
                    $("#employeeFilter").prop( "disabled", false );
                    employment_status = $('#employment_status').val();
                    if (optVal.length > 0) {
                        $.ajax({
                            type: "GET",
                            url: "{{ url('get_department_employees_from_section') }}",
                            data: {
                                department_id: optVal,
                                employment_status: employment_status,

                            },
                            success: function(response) {
                                $('#employeeFilter').empty();
                                $('#employeeFilter').html(
                                    '<option value="">Select Employee</option>');
                                $.each(response, function(index, value) {
                                    $('#employeeFilter').append(
                                        $('<option></option>').val(value.id).html(
                                        value.employee_id + '-' + value.employee_code + '-' + value.first_name + ' ' + value.last_name+ ' ' + value.department)
                                    );
                                });
                            },
                            error: function() {
                                alert('Error occured');
                            }
                        });
                    } else {
                        $('#employee').empty();
                    }
                });
            @endif
            $("#bulkForm").submit(function(e) {
                e.preventDefault();
                let formData = new FormData(this);
                $.ajax({
                    url: "{{ route('bulk_shift_management.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else {
                            $('#bulkForm')[0].reset();
                            Toast.fire({
                                icon: 'success',
                                title: 'Bulk Shift Management has been Added Successfully!'
                            })
                            setTimeout(function(){
                                window.location.reload();
                            }, 2000);
                        }

                    }
                });
            });
        });
    </script>
@endsection