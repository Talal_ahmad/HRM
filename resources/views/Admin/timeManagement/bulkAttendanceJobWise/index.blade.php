@extends('Admin.layouts.master')
@section('title', 'Bulk Attendance Job Wise')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Bulk_Attendance')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Time_Management')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Bulk_Attendance')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">@lang('app.Bulk_Attendance_Job_Title_Wise')</h4>
                        </div>
                        <div class="card-body">
                            <form class="form" id="add_form">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">@lang('app.Select_Job_Title')</label>
                                            <select name="job" id="type" class="select2 form-select" data-placeholder="@lang('app.Select_Designation')" required>
                                                <option value=""></option>
                                                @foreach (designation() as $jobtitle)
                                                    <option value="{{$jobtitle->id}}">{{$jobtitle->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">@lang('app.Select_Date')</label>
                                            <input type="text" id="type" class="form-control flatpickr-basic text-left" placeholder="YYYY-MM-DD" name="date" required/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">@lang('app.Time_In')</label>
                                            <input type="text" id="type"
                                            class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                            name="time_in" required />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">@lang('app.Time_Out')</label>
                                            <input type="text" id="type"
                                            class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                            name="time_out" required />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary me-1">@lang('app.Mark_Attendance')</button>
                                        <button type="reset" class="btn btn-outline-secondary">@lang('app.Reset')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $('#add_form').on('submit' , function(e){
                e.preventDefault();
                $.ajax({
                    url: "{{route('bulk_attendance_job_wise.store')}}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response){
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#add_form")[0].reset();
                            Toast.fire({
                                icon: 'success',
                                title: 'Bulk Attendance has been Marked Successfully!'
                            })
                            setTimeout(function(){
                                window.location.reload();
                            }, 2000);
                        }
                    }
                });
        });
    </script>
@endsection