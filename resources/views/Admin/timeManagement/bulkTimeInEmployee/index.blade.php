@extends('Admin.layouts.master')
@section('title', 'Bulk Time In')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Bulk Time In</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Bulk Time In</a>
                                </li>
                                <li class="breadcrumb-item active">Time Management
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="content-body">
        <section class="form-control-repeater">
            <div class="row">
                <!-- Employee Repeater -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Employees</h4>
                        </div>
                        <div class="card-body">
                            <form action="#" class="invoice-repeater" id="add_form">
                                @csrf
                                <div data-repeater-list="participant">
                                    <div data-repeater-item>
                                        <div class="row d-flex align-items-end">
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="type">Select Employee</label>
                                                    <select name="employee_id" id="type" class="select2 form-select">
                                                        <option value="">Select Employee</option>
                                                        @foreach ($employees as $employee)
                                                            <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->last_name}}</option> 
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="type">Time In</label>
                                                    <input type="text" id="type"
                                                    class="form-control flatpickr-date-time text-left" placeholder="HH:MM"
                                                    name="time_in" required />
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="type">Time Out</label>
                                                    <input type="text" id="type"
                                                    class="form-control flatpickr-date-time text-left" placeholder="HH:MM"
                                                    name="time_out" required />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-2 col-12 ">
                                                <div class="mb-1">
                                                    <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                        <i data-feather="x" class="me-25"></i>
                                                        <span>Delete</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                            <i data-feather="plus" class="me-25"></i>
                                            <span>Add New</span>
                                        </button>
                                        <button class="btn btn-icon btn-success" type="submit">
                                            <span>Submit</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $(function () {
                'use strict';
                // form repeater jquery
                    $('.invoice-repeater').repeater({
                        show: function () {
                        $(this).slideDown();
                        $(this).find('select').next('.select2-container').remove();
                        $(this).find('select').select2({
                            placeholder: "Select Employee"
                        });
                        $(this).find('input').next('.flatpickr-time').remove();
                        // Feather Icons
                        if (feather) {
                            feather.replace({ width: 14, height: 14 });
                        }
                        },
                        hide: function (deleteElement) {
                        if (confirm('Are you sure you want to delete this element?')) {
                            $(this).slideUp(deleteElement);
                        }
                        }
                    });
                });
        });
        $('#add_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: "{{ route('bulk_time_in_employee.store') }}",
                type: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(response) {
                    console.log(response);
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#add_form')[0].reset();
                        Toast.fire({
                            icon: 'success',
                            title: 'Bulk Time In has been Added Successfully!'
                        })
                    }

                }
            });
        });   
    </script>
@endsection