@extends('Admin.layouts.master')
@section('title', 'Bulk Time In Department Wise')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Bulk_Time_In')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Home')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.Time_Management')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Bulk_Time_In')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card mb-1">
                    <!--Search Form -->
                    <div class="card-body">
                        <form id="search_form" action="{{ route('bulk_time_in_department.index') }}">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <label class="form-label">@lang('app.Department'):</label>
                                    <select name="departmentFilter" id="departmentFilter" class="select2 form-select"
                                        data-placeholder="@lang('app.Select_Department')" required>
                                        <option value="">@lang('app.Select_Department')</option>
                                        @foreach (departments() as $department)
                                            <option value="{{ $department->id }}">{{ $department->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 col-12">
                                    <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                    <select name="employeeFilter" id="employeeFilter" class="select2 form-select"
                                        data-placeholder="@lang('app.Select_Employee')">

                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-end">
                                    <a href="{{route('bulk_time_in_department.index')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                    <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <form action="" id="add_form">
                    @csrf
                    <div class="card">
                        <div class="card-header border-bottom p-1" style="user-select: auto;">
                            <div class="head-label" style="user-select: auto;">
                                <h6 class="mb-0" style="user-select: auto;">@lang('app.Add_Bulk_Time_In')</h6>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>Time In</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($employees) && count($employees) > 0)
                                        @foreach ($employees as $employee)
                                            <tr>
                                                <td>{{$employee->employee_id}}</td>
                                                <td>{{$employee->employee_code}}</td>
                                                <td>{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}</td>
                                                <td>
                                                    <input type="hidden" id="department" class="form-control" name="department" value="{{$employee->department}}"/>
                                                    <input type="hidden" id="employee" class="form-control" name="employees[]" value="{{$employee->id}}"/>
                                                    <input type="text" id="fp-date-time" class="form-control flatpickr-date-time" placeholder="YYYY-MM-DD HH:MM" name="date_time[{{$employee->id}}]" required/>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="3">No Record Found</td>
                                        </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary ms-1" id="save">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(e){
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + '-' + value.employee_code + '-' + value.first_name +' '+ value.last_name)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            // Add Bulk Record
            $('#add_form').submit(function(e){
                e.preventDefault();
                $.ajax({
                    url: "{{ route('bulk_time_in_department.store') }}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Time In has been Added Successfully!'
                            })
                            setTimeout(function(){
                                window.location.href = "{{ url('bulk_time_in_department') }}";
                            }, 2000);
                        }
                    }
                });
            }); 
        });
    </script>
@endsection