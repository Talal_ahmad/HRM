@extends('Admin.layouts.master')
@section('title', 'Bulk Time Out')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Bulk_Time_Out')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Bulk_Time_Out')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Time_Management')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <!--Search Form -->
                        <div class="card-body">
                            <form id="search_form" action="{{ route('bulk_time_out_jsml.index') }}" method="GET">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <label class="form-label"
                                            for="departmentFilter">@lang('app.Filter_By_Department')</label>
                                        <select name="departmentFilter" id="departmentFilter"
                                            class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Department')" required>
                                            <option value=""></option>
                                            <option value="all"
                                                {{ request('departmentFilter') == 'all' ? 'selected' : '' }}>
                                                All Departments</option>
                                            @foreach (departments() as $department)
                                                <option value="{{ $department->id }}"
                                                    {{ $department->id == request('departmentFilter') ? 'selected' : '' }}>
                                                    {{ $department->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label"
                                                for="title">@lang('app.Filter_By_Section')</label>
                                            <select name="section[]" id="section"
                                                data-placeholder="@lang('app.Select_Section')"
                                                class="select2 form-select" multiple required>

                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary"
                                                    type="button"
                                                    onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label"
                                            for="employeeFilter">@lang('app.Filter_By_Employee'):</label>
                                        <select name="employeeFilter" id="employeeFilter"
                                            class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Employee')">
                                            <option value=""></option>
                                            @foreach (employees() as $item)
                                                <option value="{{ $item->id }}"
                                                    {{ $item->id == request('employeeFilter') ? 'selected' : '' }}>
                                                    {{ $item->employee_id . ' - ' . $item->employee_code . ' - ' . $item->first_name . ' ' . $item->last_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="fromDate">@lang('app.From_Date'):</label>
                                        <input type="text" id="fromDate" class="form-control flatpickr-basic"  name="fromDate" value="{{empty(request('fromDate')) ? date('Y-m-d') : request('fromDate')}}" required/>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="toDate">@lang('app.To_Date'):</label>
                                        <input type="text" id="toDate" class="form-control flatpickr-basic"  name="toDate" value="{{empty(request('toDate')) ? date('Y-m-d') : request('toDate')}}" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{route('bulk_time_out_jsml.index')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <form action="" id="add_form">
                        @csrf
                        <div class="card">
                            <div class="card-header border-bottom p-1" style="user-select: auto;">
                                <div class="head-label" style="user-select: auto;">
                                    <h6 class="mb-0" style="user-select: auto;">@lang('app.Add_Bulk_Time_Out')</h6>
                                </div>
                            </div>
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>Time In</th>
                                        <th>Time Out</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($employees) && count($employees) > 0)
                                        @foreach ($employees as $employee)
                                        <tr>
                                            <td>{{$employee->employee_id}}</td>
                                            <td>{{$employee->employee_code}}</td>
                                            <td>{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}</td>
                                            {{-- <td>{{$employee->in_time}}</td> --}}
                                            <td><input type="datetime-local" id="fp-in-date-time" class="form-control" placeholder="YYYY-MM-DD HH:MM" value="{{$employee->in_time}}" name="in_time[{{$employee->id}}]"/></td>
                                            <td>
                                                <input type="hidden" class="form-control" name="attendance_ids[]" value="{{$employee->id}}"/>
                                                <input type="datetime-local" id="fp-date-time" class="form-control" placeholder="YYYY-MM-DD HH:MM" value="{{$employee->out_time}}" name="out_time[{{$employee->id}}]"/>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="5">No Record Found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-12 text-end">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@php
    $departmentFilter = request('departmentFilter');
    $section = request('section');
@endphp
@section('scripts')
    <script type="text/javascript">
        var departmentFilter = @json($departmentFilter);
        var section = @json($section);
        var employee = @json(request('employeeFilter'));
        $(document).ready(function() {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>');
                            $.each(response, function(index, value) {
                                var selected = (section ? section : []).includes('' +
                                    value.id + '') ? 'selected' : '';
                                $('#section').append(
                                    $('<option ' + selected + '></option>').val(
                                        value.id).html(
                                        value.title)
                                );
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Department Sections Fetched Successfully!'
                            });
                        },
                        error: function() {
                            Toast.fire({
                                icon: 'error',
                                title: 'Error Fetching Department Sections!'
                            });
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $("#section").change(function() {
                var optVal = $(this).val();
                if(optVal.length > 0){
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_employees_from_section') }}",
                        data: {
                            department_id: optVal,
                            type: 'attendance_report',
                        },
                        success: function(response) {
                            $('#employeeFilter').empty();
                            $('#employeeFilter').html(
                                '<option value="">Select Employee</option>');
                            $.each(response, function(index, value) {
                                var selected = employee == value.id ? 'selected' : '';
                                $('#employeeFilter').append(
                                    $('<option ' + selected + '></option>').val(
                                        value.id).html(
                                        value.employee_id + ' - ' + value
                                        .employee_code + ' - ' + value.first_name +
                                        ' ' + value.middle_name + ' ' + value
                                        .last_name + ' - ' + value.designation +
                                        ' - ' + value.department)
                                );
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Sections Employees Fetched Successfully!'
                            });
                        },
                        error: function() {
                            Toast.fire({
                                icon: 'error',
                                title: 'Error Fetching Sections Employees!'
                            });
                        }
                    });
                }
                else{
                    $('#employeeFilter').empty();
                    $('#employeeFilter').html('<option value="">Select Employee</option>');
                }
            });
            $('#departmentFilter').trigger("change");
            // Add Form
            $('#add_form').on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('bulk_time_out_jsml.store') }}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#add_modal").modal("hide");
                            Toast.fire({
                                icon: 'success',
                                title: 'Bulk Time Out has been added Successfully!'
                            })
                            setTimeout(function(){
                                window.location.href = "{{ url('bulk_time_out_jsml') }}";
                            }, 2000);
                        }
                    }
                });
            });
        });
    </script>
@endsection
