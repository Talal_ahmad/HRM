@extends('Admin.layouts.master')
@section('title', 'Bulk Attendance')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Bulk_Attendance')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Bulk_Attendance')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Time_Management')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <!--Search Form -->
                        <div class="card-body">
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{Session::get('message')}}</div>
                            @else
                            <div class="alert alert-success">{{Session::get('success_message')}}</div>
                            @endif
                            <form id="search_form" action="{{ route('bulk_attendance_job_wise.create') }}" method="GET">
                                <div class="row">
                                    <div class="col-md-5 col-12">
                                        <label class="form-label" for="employee_id">@lang('app.Employees')</label>
                                        <select name="employee_id" id="employee_id" class="select2 form-select"
                                            data-placeholder="@lang('app.Select_Employee')">
                                            <option value=""></option>
                                            @foreach(employees() as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id. ' - '.$employee->employee_code.' - '.$employee->first_name.'  '.$employee->last_name.' - '.$employee->desigination.' ('.$employee->department.' )'}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" for="from_date">@lang('app.From_Date')</label>
                                        <input type="date" name="from_date" id="from_date" class="form-control" required/>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" for="to_date">@lang('app.To_Date')</label>
                                        <input type="date" name="to_date" id="to_date" class="form-control" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{route('bulk_time_out.index')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <form action="{{route('bulk_attendance_job_wise.update', !empty($employ) ? $employ->id : '')}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="card">
                            <div class="card-header border-bottom p-1" style="user-select: auto;">
                                <div class="head-label" style="user-select: auto;">
                                    <h6 class="mb-0" style="user-select: auto;">@lang('app.Add_Bulk_Attendance')</h6>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row mt-2">
                                    @if (!empty($employ))
                                    <div class="col-md-6 col-lg-4 col-12 d-flex justify-content-between">
                                        <h4 style="font-weight: bold">@lang('app.Employees'):</h4>
                                        <h5>{{$employ->employee_id.' - '.$employ->employee_code.' - '.$employ->first_name.' '.$employ->middle_name.' '.$employ->last_name}}</h5>
                                    </div>
                                    <div class="offset-lg-2 col-md-6 col-lg-4 col-12 d-flex justify-content-between">
                                        <h4 style="font-weight: bold">@lang('app.Department'):</h4>
                                        <h5>{{$employ->title}}</h5>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Date</th>
                                            <th>Shift</th>
                                            <th>Work Week</th>
                                            <th>Shift Start</th>
                                            <th>Shift End</th>
                                            <th>Time In</th>
                                            <th>Time Out</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($dates) && count($dates) > 0)
                                            @foreach ($dates as $key => $date)
                                            @php
                                                $in_datetime = $date->date.' '.$date->shift_start_time;
                                                $out_datetime = $date->date.' '.$date->shift_end_time;
                                                if($out_datetime < $in_datetime)
                                                {
                                                    $new_date = date("Y-m-d", strtotime("+1 day", strtotime($date->date)));
                                                    $out_datetime = $new_date.' '.$date->shift_end_time;
                                                }
                                            @endphp
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$date->date}}</td>
                                                <td>{{$date->shift_name}}</td>
                                                <td>{{$date->work_week}}</td>
                                                <td>{{$date->shift_start_time}}</td>
                                                <td>{{$date->shift_end_time}}</td>
                                                <td>
                                                    <input type="hidden" class="form-control" name="dates[]" value="{{$date->date}}"/>
                                                    <input type="text" id="fp-date-time" name="time_in[{{$date->date}}]" class="form-control flatpickr-date-time" value="{{$in_datetime}}" placeholder="YYYY-MM-DD HH:MM" />
                                                </td>
                                                <td>
                                                    <input type="text" id="fp-date-time" name="time_out[{{$date->date}}]" class="form-control flatpickr-date-time" value="{{$out_datetime}}" placeholder="YYYY-MM-DD HH:MM" />
                                                </td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="text-center" colspan="8">No Record Found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if(env('COMPANY') == 'JSML')
                            <div class="col-12">
                                <div class="mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="od" value="1"/>
                                        <label class="custom-control-label">OD</label>
                                        <input type="checkbox" class="custom-control-input" name="ret" value="1"/>
                                        <label class="custom-control-label">RET</label>
                                    </div> 
                                </div>
                            </div>
                        @endif
                        @if (isset($dates) && count($dates) > 0)
                            <button class="btn btn-primary">Submit</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            // Add Form
            $('#add_form').on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('bulk_time_out.store') }}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#add_modal").modal("hide");
                            Toast.fire({
                                icon: 'success',
                                title: 'Bulk Time Out has been added Successfully!'
                            })
                            setTimeout(function(){
                                window.location.href = "{{ url('bulk_time_out') }}";
                            }, 2000);
                        }
                    } 
                });
            });
        });
    </script>
@endsection
