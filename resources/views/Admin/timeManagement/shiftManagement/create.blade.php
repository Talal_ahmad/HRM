@extends('Admin.layouts.master')
@section('title', 'Shift management')

@section('content')
<style>
    td {
        padding: 10px 8px !important;
    }
    
</style>
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Shift Management</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Time Management</a>
                                </li>
                                <li class="breadcrumb-item active">Shift Management
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <!--Search Form -->
                        <div class="card-body">
                            <form id="search_form" method="GET" action="{{ route('shiftManagement.create') }}">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <label class="form-label">Department:</label>
                                        <select name="departmentFilter" id="departmentFilter" class="select2 form-select"
                                            data-placeholder="Select Department" required>
                                            <option value=""></option>
                                            <option value="all">All</option>
                                            @foreach (departments() as $department)
                                                <option value="{{ $department->id }}">{{ $department->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if (env('COMPANY') == 'JSML')        
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">Section</label>
                                                <select name="section[]" id="section" data-placeholder="Select Section" class="select2 form-select" multiple required>
                                                    
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#section')">Select All</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#section')">Deselect All</button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="employeeFilter">Employee</label>
                                        <select name="employeeFilter" id="employeeFilter" class="select2 form-select"
                                            data-placeholder="Select Employee">

                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{route('shiftManagement.create')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <form action="" id="add_form">
                        @csrf
                        <div class="card">
                            <div class="card-header border-bottom p-1">
                                <div class="head-label">
                                    <h6 class="mb-0">List of Shift Types</h6>
                                </div>
                                <div class="dt-action-buttons text-end">
                                    <a class="dt-button create-new btn btn-primary" href="{{route('shiftManagement.index')}}"><i class="fas fa-arrow-left me-1"></i>Go Back</a>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Shift Type</th>
                                                <th>Work Week</th>
                                                <th>Shift From</th>
                                                <th>Shift To</th>
                                                <th>Late Time Allowed</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($employees) > 0)
                                                @foreach ($employees as $key => $employee)
                                                <input type="hidden" id="employee" name="employees[]" value="{{$employee->id}}"/>
                                                <tr>
                                                    <td>{{ $employee->employee_id .' - '.$employee->first_name }} {{ $employee->last_name }}</td>
                                                    <td>
                                                        <select name="shift_id[{{$employee->id}}]" id="shift_id_{{ $key }}" class=" form-select" required>
                                                            <option value="">Select Shift Type</option>
                                                            @if (env('COMPANY') == 'CLINIX')
                                                                @foreach ($shift_type[$employee->id] as $shift)
                                                                    <option value="{{ $shift->id }}">{{ $shift->shift_desc }}
                                                                    </option>
                                                                @endforeach
                                                            @else
                                                                @foreach ($shift_type as $shift)
                                                                    <option value="{{ $shift->id }}">{{ $shift->shift_desc }}
                                                                    </option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="work_week_id[{{$employee->id}}]" id="work_week_{{ $key }}" class="form-select" required>
                                                            <option value="">Select Work Week</option>
                                                            @foreach ($work_week as $week)
                                                                <option value="{{ $week->id }}">{{ $week->desc }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="date" id="shift_from" name="shift_from_date[{{$employee->id}}]" class="form-control" required>
                                                    </td>
                                                    <td>
                                                        <input type="date" id="shift_from" name="shift_to_date[{{$employee->id}}]" class="form-control" required>
                                                    </td>
                                                    @if (env('COMPANY') == 'JSML' || env('COMPANY') == 'AJMAL' || env('COMPANY') == 'RoofLine')    
                                                        <td>
                                                            <input type="text" id="late_time_allowed" class="form-control"
                                                                value="15" name="late_time_allowed[{{$employee->id}}]" readonly />
                                                        </td>
                                                    @else
                                                        <td>
                                                            <input type="text" id="late_time_allowed" class="form-control"
                                                                value="11" name="late_time_allowed[{{$employee->id}}]" readonly />
                                                        </td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td class="text-center" colspan="6">No Record Found!</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @if (count($employees) > 0)
                            <div class="d-flex justify-content-end p-1">
                                <button type="submit" class="btn btn-primary" id="save">Submit</button>
                            </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
    </section>
    </div>
    </section>
@endsection
@section('scripts')
    <script>
        var dataTable;
        $(document).ready(function() {
        @if (env('COMPANY') == 'JSML')        
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
                $("#section").change(function() {
                    var optVal = $(this).val();
                    if (optVal.length > 0) {
                        $.ajax({
                            type: "GET",
                            url: "{{ url('get_dept_employees') }}",
                            data: {
                                department_id: optVal,
                                type: 'component_by_month'
                            },
                            success: function(response) {
                                $('#employeeFilter').empty();
                                $('#employeeFilter').html(
                                    '<option value="">Select Employee</option>');
                                $.each(response, function(index, value) {
                                    $('#employeeFilter').append(
                                        $('<option></option>').val(value.id).html(
                                        value.employee_id + '-' + value.employee_code + '-' + value.first_name + ' ' + value.last_name+ ' ' + value.department)
                                    );
                                });
                            },
                            error: function() {
                                alert('Error occured');
                            }
                        });
                    } else {
                        $('#employee').empty();
                    }
                });
            @else
                $("#departmentFilter").change(function() {
                    var optVal = $(this).val();
                    if (optVal.length > 0) {
                        $.ajax({
                            type: "GET",
                            url: "{{ url('get_dept_employees') }}",
                            data: {
                                department_id: optVal,
                            },
                            success: function(response) {
                                $('#employeeFilter').empty();
                                $('#employeeFilter').html(
                                    '<option value="">Select Employee</option>');
                                $.each(response, function(index, value) {
                                    $('#employeeFilter').append(
                                        $('<option></option>').val(value.id).html(
                                        value.employee_id + '-' + value.employee_code + '-' + value.first_name + ' ' + value.last_name+ ' ' + value.department)
                                    );
                                });
                            },
                            error: function() {
                                alert('Error occured');
                            }
                        });
                    } else {
                        $('#employee').empty();
                    }
                });
            @endif
            $('div.head-label').html('<h6 class="mb-0">List of Employee Current Shift</h6>');
        });

        // Add Data
        $('#add_form').submit(function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{ route('shiftManagement.store') }}",
                type: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: response.error_message
                        })
                    } else {
                        $('#add_form')[0].reset();
                        Toast.fire({
                            icon: 'success',
                            title: 'Current Shift has been Added Successfully!'
                        })
                    }
                }
            });
        });
    </script>
@endsection
