@extends('Admin.layouts.master')
@section('title', 'Shift Management')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Shift_Management')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Time_Management')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Shift_Management')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="currentShifts-tab" data-bs-toggle="tab" href="#currentShifts"
                            aria-controls="currentShifts" role="tab" aria-selected="true">@lang('app.Current_Shifts')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="prevShifts()" id="prevShifts-tab" data-bs-toggle="tab"
                            href="#prevShifts" aria-controls="prevShifts" role="tab" aria-selected="false">@lang('app.Previous_Shifts')
                            </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="shiftManagement()" id="shiftManagementHistory-tab"
                            data-bs-toggle="tab" href="#shiftManagementHistory" aria-controls="shiftManagementHistory"
                            role="tab" aria-selected="false">@lang('app.Shift_Management_History')</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="currentShifts" aria-labelledby="currentShifts-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card mb-1">
                                        <!--Search Form -->
                                        <div class="card-body">
                                            <form id="current_search_form">
                                                <div class="row g-1 mb-md-1">

                                                    <div class="col-md-6">
                                                        <label class="form-label">@lang('app.Filter_By_Department'):</label>
                                                        <select name="curDeptFilter" id="curDeptFilter"
                                                            class="select2 form-select filter"
                                                            data-placeholder="@lang('app.Select_Department')">
                                                            <option value="">@lang('app.Select_Department')</option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{ $department->id }}">
                                                                    {{ $department->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    @if (env('COMPANY') == 'JSML')        
                                                        <div class="col-md-6 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="title">@lang('app.Section')</label>
                                                                <select name="section[]" id="section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                                    
                                                                </select>
                                                                <div class="button-container mt-1">
                                                                    <button class="btn btn-sm btn-primary" type="button"
                                                                        onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                                    <button class="btn btn-sm btn-danger" type="button"
                                                                        onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div style="margin-top:35px" class="col-md-12 text-md-end">
                                                        <button class="btn btn-primary">@lang('app.Filter')</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card pb-2">
                                        <table class="table" id="current_shift_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>St.No</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Designation</th>
                                                    @if (env('COMPANY') == 'JSML')        
                                                        <th>Section</th>
                                                    @else
                                                        <th>Department</th>
                                                    @endif
                                                    <th>Shift ID</th>
                                                    <th>Work Week</th>
                                                    <th>Shift From</th>
                                                    <th>Shift To</th>
                                                    <th>Late Time Allowed</th>
                                                    <th>Added On</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Edit Modal -->
                        <div class="modal fade text-start" id="current_shift_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">Edit Current Shift</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="current_shift_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="shift_id">Shift Type</label>
                                                        <select name="shift_id" id="shift_id" class="select2 form-select" data-placeholder="Select Shift Type" required>
                                                            <option value=""></option>
                                                            @foreach ($shift_type as $shift)
                                                                <option value="{{ $shift->id }}">
                                                                    {{ $shift->shift_desc }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label class="form-label" for="work_week">Work Week</label>
                                                    <select name="work_week_id" id="work_week" class="select2 form-select" data-placeholder="Select Work Week" required>
                                                        <option value=""></option>
                                                        @foreach ($work_week as $week)
                                                            <option value="{{ $week->id }}">{{ $week->desc }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="shift_from">Shift From</label>
                                                        <input type="text" id="shift_from"
                                                            class="form-control flatpickr-basic text-left"
                                                            placeholder="Shift From" name="shift_from_date" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="shift_to">Shift To</label>
                                                        <input type="text" id="shift_to"
                                                            class="form-control flatpickr-basic text-left"
                                                            placeholder="Shift Date" name="shift_to_date" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="time_allowed">Late Time
                                                            Allowed</label>
                                                        <input type="text" id="time_allowed" class="form-control"
                                                            name="late_time_allowed" readonly required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="update">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="prevShifts" aria-labelledby="prevShifts-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card mb-1">
                                        <!--Search Form -->
                                        <div class="card-body">
                                            <form id="prev_search_form">
                                                <div class="row g-1 mb-md-1">
                                                    <div class="col-md-6">
                                                        <label class="form-label">Filter By Department:</label>
                                                        <select name="prevDeptFilter" id="prevDeptFilter"
                                                            class="select2 form-select filter"
                                                            data-placeholder="Select Department">
                                                            <option value=""></option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{ $department->id }}">
                                                                    {{ $department->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    @if (env('COMPANY') == 'JSML')        
                                                        <div class="col-md-6 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="title">Section</label>
                                                                <select name="section[]" id="section1" data-placeholder="Select Section" class="select2 form-select" multiple required>
                                                                    
                                                                </select>
                                                                <div class="button-container mt-1">
                                                                    <button class="btn btn-sm btn-primary" type="button"
                                                                        onclick="selectAll('#section1')">Select All</button>
                                                                    <button class="btn btn-sm btn-danger" type="button"
                                                                        onclick="deselectAll('#section1')">Deselect All</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div style="margin-top:35px" class="col-md-6 text-md-end">
                                                        <button class="btn btn-primary">Filter</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card pb-2">
                                        <table class="table" id="prev_shift_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Designation</th>
                                                    <th>Department</th>
                                                    <th>Shift ID</th>
                                                    <th>Work Week</th>
                                                    <th>Shift From</th>
                                                    <th>Shift To</th>
                                                    <th class="not_include">Late Time Allowed</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="tab-pane" id="shiftManagementHistory" aria-labelledby="shiftManagementHistory-tab"
                        role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card mb-1">
                                        <!--Search Form -->
                                        <div class="card-body">
                                            <form id="history_search_form">
                                                <div class="row g-1 mb-md-1">
                                                    <div class="col-md-6">
                                                        <label class="form-label">Filter By Department:</label>
                                                        <select name="historyDeptFilter" id="historyDeptFilter"
                                                            class="select2 form-select filter"
                                                            data-placeholder="Select Department">
                                                            <option value=""></option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{ $department->id }}">
                                                                    {{ $department->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    @if (env('COMPANY') == 'JSML')        
                                                        <div class="col-md-6 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="title">Section</label>
                                                                <select name="section[]" id="section2" data-placeholder="Select Section" class="select2 form-select" multiple required>
                                                                    
                                                                </select>
                                                                <div class="button-container mt-1">
                                                                    <button class="btn btn-sm btn-primary" type="button"
                                                                        onclick="selectAll('#section2')">Select All</button>
                                                                    <button class="btn btn-sm btn-danger" type="button"
                                                                        onclick="deselectAll('#section2')">Deselect All</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div style="margin-top:35px" class="col-md-6 text-md-end">
                                                        <button class="btn btn-primary">Filter</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card pb-2">
                                        <table class="table" id="shift_history_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Designation</th>
                                                    <th>Department</th>
                                                    <th>Shift ID</th>
                                                    <th>Work Week</th>
                                                    <th>Shift From</th>
                                                    <th>Shift To</th>
                                                    <th class="not_include">Late Time Allowed</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var datatable;
        var rowid;
        $(document).ready(function() {
            $("#curDeptFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });

            $("#prevDeptFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section1').empty();
                            $('#section1').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section1').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section1').empty();
                }
            });

            $("#historyDeptFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section2').empty();
                            $('#section2').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section2').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section2').empty();
                }
            });

            

            $('#current_shift_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: true,
                ajax: {
                    url: "{{ route('shiftManagement.index') }}",
                    data: function(filter) {
                        filter.curDeptFilter = $('#curDeptFilter').val();
                        filter.section = $('#section').val();
                        filter.type = 'current_shift';
                    },
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        // data: 'first_name',
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name: '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + " " + middleName + " " + lastName;
                        },
                        searchable: false,
                        // orderable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'designation',
                        name: 'jobtitles.name'
                    },
                    {
                        data: 'empDep',
                        name: 'companystructures.title'
                    },
                    {
                        data: 'shiftDesc',
                        name: 'shift_type.shift_desc'
                    },
                    {
                        data: 'workDesc',
                        name: 'work_week.desc'
                    },
                    {
                        data: 'shift_from_date',
                    },
                    {
                        data: 'shift_to_date',
                    },
                    {
                        data: 'late_time_allowed',
                    },
                    {
                        data: 'added_on',
                        name: 'shift_management.created_at',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        @if(env('COMPANY') != 'JSML')
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=current_shift_edit(' +
                                full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>'
                            );
                        }
                        @endif
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @if(env('COMPANY') != 'JSML')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add Shift',
                            className: 'btn btn-primary',
                            action: function(e, dt, node, config) {
                                window.location.href = '{{ route('shiftManagement.create') }}';
                            }
                        }
                    @endif
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Employee Current Shift</h6>');
        });
        // Onclick Functions
        function prevShifts() {
            $('#prev_shift_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                destroy: true,
                ajax: {
                    url: "{{ route('shiftManagement.index') }}",
                    data: function(filter) {
                        filter.prevDeptFilter = $('#prevDeptFilter').val();
                        filter.section1 = $('#section1').val();
                        filter.type = 'prev_shift';
                    },
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'employee_id',
                        data: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        data: 'employees.employee_code'
                    },
                    {
                        data: 'employee',
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name: '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + " " + middleName + " " + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'designation',
                        name: 'jobtitles.name'
                        
                    },
                    {
                        data: 'empDep',
                        name: 'companystructures.title'
                    },
                    {
                        data: 'shiftDesc',
                        name: 'shift_type.shift_desc'
                    },
                    {
                        data: 'workDesc',
                        name: 'work_week.desc'
                    },
                    {
                        data: 'shift_from_date',
                    },
                    {
                        data: 'shift_to_date',
                    },
                    {
                        data: 'late_time_allowed',
                    },

                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },

                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Employee Previous Shift</h6>');
        }

        function shiftManagement() {
            $('#shift_history_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                destroy: true,
                ajax: {
                    url: "{{ route('shiftManagement.index') }}",
                    data: function(filter) {
                        filter.historyDeptFilter = $('#historyDeptFilter').val();
                        filter.section2 = $('#section2').val();
                        filter.type = 'shift_history';
                    },
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        data: 'employee',
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name: '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + " " + middleName + " " + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'designation',
                        name: 'jobtitles.name'
                    },
                    {
                        data: 'empDep',
                        name: 'companystructures.title'
                    },
                    {
                        data: 'shiftDesc',
                        name: 'shift_type.shift_desc'
                    },
                    {
                        data: 'workDesc',
                        name: 'work_week.desc'
                    },
                    {
                        data: 'shift_from_date',
                    },
                    {
                        data: 'shift_to_date',
                    },
                    {
                        data: 'late_time_allowed',
                    },

                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },

                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Employee Shift Management History</h6>');
        }
        // Edit Current Shift
        function current_shift_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('shiftManagement') }}" + "/" + id + "/edit",
                type: "GET",
                success: function(response) {
                    $("#shift_id").val(response.shift_id).select2();
                    $("#work_week").val(response.work_week_id).select2();
                    $("#shift_from").val(response.shift_from_date);
                    $("#shift_to").val(response.shift_to_date);
                    $("#edit_shift_desc").val(response.shift_desc);
                    $("#time_allowed").val(response.late_time_allowed);
                    $("#current_shift_edit_modal").modal("show");
                },
            });
        }
        // Update Current Shift
        $('#current_shift_edit_form').submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'current_shift');
            $.ajax({
                url: "{{ url('shiftManagement') }}" + "/" + rowid,
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: response.error_message
                        })
                    } else {
                        $('#current_shift_edit_form')[0].reset();
                        $("#current_shift_edit_modal").modal("hide");
                        $('#current_shift_dataTable').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Current Shift has been Updated Successfully!'
                        })
                    }

                }
            });
        });
        $('#current_search_form').submit(function(e){
            $('#current_shift_dataTable').DataTable().draw();
            e.preventDefault();
        });
        $('#prev_search_form').submit(function(e){
            $('#prev_shift_dataTable').DataTable().draw();
            e.preventDefault();
        });
        $('#history_search_form').submit(function(e){
            $('#shift_history_dataTable').DataTable().draw();
            e.preventDefault();
        });
    </script>
@endsection
