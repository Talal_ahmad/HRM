@extends('Admin.layouts.master')

@section('title', 'Users')
@section('style')
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.users')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Home')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Setup')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.users')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header row"></div>
        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table table-bordered" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Name</th>
                                        <th>E-Mail</th>
                                        <th>Departments</th>
                                        <th>Calculation Groups</th>
                                        <th>Roles</th>
                                        <th class="not_include">Actions</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--Add Modal -->
                    <div class="modal fade text-start" id="user_add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_User')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="user_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="name">@lang('app.Name')</label>
                                                    <input type="text" id="name" class="form-control" placeholder="@lang('app.Name')" name="name" required />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="email">@lang('app.E_Mail')</label>
                                                    <input type="email" id="email" class="form-control" placeholder="@lang('app.E_Mail')" name="email" required />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="password">@lang('app.Password')</label>
                                                    <input type="password" id="password" class="form-control" placeholder="@lang('app.Password')" name="password" required />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="cpassword">@lang('app.Confirm_Password')</label>
                                                    <input type="password" id="cpassword" class="form-control" placeholder="@lang('app.Confirm_Password')" name="cpassword" required />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                    <select name="employee" id="employee" 
                                                        class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
                                                        <option value="">@lang('app.Select_Employee')</option>
                                                        @foreach (employees() as $item)
                                                            <option value="{{ $item->id }}">{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12 dept">
                                                <div class="mb-1">
                                                    <label class="form-label" for="department">@lang('app.Departments')</label>
                                                    <select name="department_name[]" id="department" multiple class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                        @foreach (departments() as $department)
                                                            <option value="{{ $department->id }}">{{ $department->title }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#department')">@lang('app.Select_All')</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#department')">@lang('app.Deselect_All')</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 dept">
                                                <div class="mb-1">
                                                    <label class="form-label" for="calculationGroups">@lang('app.Calculation_Groups')</label>
                                                    <select name="calculationGroups_name[]" id="calculationGroups" multiple class="select2 form-select" data-placeholder="@lang('app.Select_Calculation_Groups')">
                                                        @foreach ($calculationGroups as $calcgroups)
                                                            <option value="{{ $calcgroups->id }}">{{ $calcgroups->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#calculationGroups')">@lang('app.Select_All')</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#calculationGroups')">@lang('app.Deselect_All')</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 dept">
                                                <div class="mb-1">
                                                    <label class="form-label" for="role">@lang('app.Roles')</label>
                                                    <select name="role_name[]" id="role" class="select2 form-select" data-placeholder="@lang('app.Select_Role')" multiple required>
                                                        <option value="">Select Role</option>
                                                        @foreach ($roles as $role)
                                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="mac_address">@lang('app.Mac_Address')</label>
                                                    <input type="text"  class="form-control" id="mac_address" placeholder="@lang('app.Mac_Address')" name="mac_address"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-6">
                                                <input type="checkbox" class="form-check-input mt-1" name="allow_mac" id="allow_mac" value="1" {{ request()->old_attendance == 1 ? 'checked' : '' }}/>
                                                <label class="custom-control-label mt-1" for="allow_mac">@lang('app.Allow_No_Mac_Address')</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Cancel')</button>
                                        <button type="submit" class="btn btn-primary" id="dave">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Edit Modal -->
                <div class="modal fade text-start" id="user_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_User')</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form class="form" id="user_edit_form">
                                @csrf
                                @method('PUT')
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_name">@lang('app.Name')</label>
                                                <input type="text" id="edit_name" class="form-control" placeholder="@lang('app.Name')" name="name" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_email">@lang('app.E_Mail')</label>
                                                <input type="email" id="edit_email" class="form-control" placeholder="@lang('app.E_Mail')" name="email" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_password">@lang('app.Password')</label>
                                                <input type="password" id="edit_password" class="form-control" placeholder="@lang('app.Password')" name="password" />
                                                <span class="text text-danger">@lang('app.dont_change')</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_cpassword">@lang('app.Confirm_Password')</label>
                                                <input type="password" id="edit_cpassword" class="form-control" placeholder="@lang('app.Confirm_Password')" name="cpassword" />
                                                <span class="text text-danger">@lang('app.dont_change')</span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                <select name="employee" id="edit_employee" 
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
                                                    <option value="">@lang('app.Select_Employee')</option>
                                                    @foreach (employees() as $item)
                                                        <option value="{{ $item->id }}">{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 dept">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_department_name">@lang('app.Departments')</label>
                                                <select name="department_name[]" id="edit_department_name" multiple
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                    <option value="">@lang('app.Select_Department')</option>
                                                    @foreach (departments() as $department)
                                                      <option value="{{ $department->id }}">{{ $department->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#edit_department_name')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#edit_department_name')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 dept">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_calculation_groups_name">@lang('app.Calculation_Groups')</label>
                                                <select name="calculationGroups_name[]" id="edit_calculation_groups_name" multiple
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Calculation_Groups')">
                                                    @foreach ($calculationGroups as $calcgroups)
                                                        <option value="{{ $calcgroups->id }}">{{ $calcgroups->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#edit_calculation_groups_name')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#edit_calculation_groups_name')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 dept">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_role_name">@lang('app.Roles')</label>
                                                <select name="role_name[]" id="edit_role_name" 
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Role')" multiple required>
                                                    <option value="">@lang('app.Select_Role')</option>
                                                    @foreach ($roles as $role)
                                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="mac_address">@lang('app.Mac_Address')</label>
                                                <input type="text"  class="form-control" id="edit_mac_address" placeholder="@lang('app.Mac_Address')" name="mac_address"/>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-6">
                                            <input type="checkbox" class="form-check-input mt-1" name="allow_mac" id="edit_allow_mac" value="1"/>
                                            <label class="custom-control-label mt-1" for="edit_allow_mac">@lang('app.Allow_No_Mac_Address')</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Cancel')</button>
                                    <button type="submit" class="btn btn-primary" id="edit_save">@lang('app.Update')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        var all_dept = <?php echo json_encode($departments); ?>;
        var all_calc_grop = <?php echo json_encode($calculationGroups); ?>;
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: "{{ route('users.index') }}",
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'username',
                        name: 'users.username',
                    },
                    {
                        data: 'email',
                        name: 'users.email',
                    },
                    {
                        data: 'user_departments' , render: function(data , type , row){
                            if(row.user_departments){
                                let user_dept = JSON.parse(row.user_departments.replace(/&quot;/g,'"'));
                                let assign_departments = '';
                                $.each(all_dept , function(index , all_dept_value){
                                    $.each(user_dept , function(index , user_dept_value){
                                        if(user_dept_value == all_dept_value.id){
                                            assign_departments += `<p class="badge bg-light-info">${all_dept_value.title}</p> `;
                                        }
                                    });
                                });
                                return assign_departments;
                            }   
                        }
                    },
                    {
                        data: 'user_calculation_groups' , render: function(data , type , row){
                            if(row.user_calculation_groups){
                                let user_calc_grop = JSON.parse(row.user_calculation_groups.replace(/&quot;/g,'"'));
                                let assign_calc_groups = '';
                                $.each(all_calc_grop , function(index , all_calc_grop_value){
                                    $.each(user_calc_grop , function(index , user_calc_grop_value){
                                        if(user_calc_grop_value == all_calc_grop_value.id){
                                            assign_calc_groups += `<p class="badge bg-light-info">${all_calc_grop_value.name}</p> `;
                                        }
                                    });
                                });
                                return assign_calc_groups;
                            }   
                        }
                    },
                    {
                        data: 'role_name'
                    },
                    {
                        data: 'status'
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#edit-modal" class="item-edit" onclick=user_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        // Status
                        targets: -2,
                        title: 'Status',
                        render: function(data, type, full, meta) {
                            // console.log(data);
                            if (data == 0) {
                                return (
                                    '<input  type="checkbox" data-id="' + full.id +
                                    '" class="toggle-class" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-size="small" data-on="Active" data-off="Inative" checked>'
                                );
                            } else {
                                return (
                                    '<input  type="checkbox" data-id="' + full.id +
                                    '" class="toggle-class "   data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-size="small" data-on="Active" data-off="Inactive">'
                                );
                            }
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                "drawCallback": function() {
                    $('.toggle-class').bootstrapToggle();
                    status();
                },
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#user_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $(function() {
                $('#toggle-two').bootstrapToggle({
                on: 'Enabled',
                off: 'Disabled'
                });
            })
            $('div.head-label').html('<h6 class="mb-0">List of Users</h6>');
            // Add Data
            $("#user_add_form").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('users.store') }}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#user_add_form')[0].reset();
                            $("#user_add_modal").modal("hide");
                            $(".select2").val('').trigger('change');
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Users has been Added Successfully!'
                            })
                        }

                    }
                });
            });
            // Edit Data
            $("#user_edit_form").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('users') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#user_edit_form')[0].reset();
                            $(".select2").val('').trigger('change');
                            $("#user_edit_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'User has been Updated Successfully!'
                            })
                        }

                    }
                });
            });

        });

        function user_edit(id) {
            $('#user_edit_form')[0].reset();
            $(".select2").val('').trigger('change');
            rowid = id;
            $.ajax({
                url: "{{ url('users') }}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    $('#edit_name').val(response.user.username);
                    $('#edit_email').val(response.user.email);
                    $('#edit_employee').val(response.user.employee).select2();
                    $('#edit_mac_address').val(response.user.mac_address);
                    if(response.user.allow_mac == '1'){
                        $('#edit_allow_mac').prop('checked', true);
                    }else{
                        $('#edit_allow_mac').prop('checked', false);
                    }
                    if(response.user.user_departments != null){
                        $('#edit_department_name').val(JSON.parse(response.user.user_departments.replace(/&quot;/g,'"'))).select2();
                    }
                    if(response.user.user_calculation_groups != null){
                        $('#edit_calculation_groups_name').val(JSON.parse(response.user.user_calculation_groups.replace(/&quot;/g,'"'))).select2();
                    }
                    $('#edit_role_name').val(response.role).select2();
                    $('#user_edit_modal').modal('show');
                }
            });
        }
        function status(){
        $(".toggle-class").change(function (event) {
            event.preventDefault();
                var status = $(this).prop('checked') == true ? 0 : 1;
                // alert(status);
                var user_id = $(this).data('id');
                $.ajax({
                    type:"GET",
                    datatype:"json",
                    url: "{{url('user')}}" + "/" + user_id,
                    data: {
                        status : status,
                        id : user_id
                    },
                    success:function (data) {
                        console.log(data.success);
                    }
                });
        });
    }
        $("#checkbox").click(function(){
            if($("#checkbox").is(':checked') ){
                $("select > option").prop("selected","selected");
            }else{
                $("select > option").removeAttr("selected");
            }
        });
    </script>
@endsection
