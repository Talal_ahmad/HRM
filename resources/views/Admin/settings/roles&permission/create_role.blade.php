@extends('Admin.layouts.master')
@section('title', 'Add Role')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Roles & Permissions</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="#">Roles</a></li>
                                <li class="breadcrumb-item active">Add Roles</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Add Role</h4>
                        </div>
                        <div class="card-body">
                            <form class="form" id="add_form" method="POST" action="{{ route('roles.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Role</label>
                                            <input type="text" id="title" class="form-control" placeholder="Role" name="roll_name" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-1 p-3">
                                    <div class="col-md-12 form-check mb-1">
                                        <input type="checkbox" class="form-check-input" name="check_all" id="check_all" value="check_all" />
                                        <label class="custom-control-label" for="check_all">Check all</label>
                                    </div>
                                    @foreach ($permission_titles as $permissiontitle)
                                        <h4 class="custom-control-label px-0 mx-0" for="{{ $permissiontitle->name }}">{{ $permissiontitle->name }}</h4>
                                        @foreach ($permissions as $permission)
                                            @if ($permission->permission_titles_id == $permissiontitle->id)
                                                <div class="col-md-4 form-check mb-1">
                                                    <input type="checkbox" class="form-check-input" name="permission_name[]" id="{{ $permission->name }}" value="{{ $permission->id }}" />
                                                    <label class="custom-control-label" for="{{ $permission->name }}">{{ $permission->name }}</label>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endforeach
                                    <hr>
                                    @foreach ($permissions as $permission)
                                        @if ($permission->permission_titles_id == null)
                                            <div class="col-md-4 form-check mb-1">
                                                <input type="checkbox" class="form-check-input" name="permission_name[]" id="{{ $permission->name }}" value="{{ $permission->id }}" />
                                                <label class="custom-control-label" for="{{ $permission->name }}">{{ $permission->name }}</label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('roles.index') }}" class="btn btn-outline-danger">Cancel</a>
                                    <button type="submit" class="btn btn-primary ms-1" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#check_all").on("change", function() {
                if ($(this).is(":checked")) {
                    $("input[type='checkbox']").prop('checked', true);
                } else {
                    $("input[type='checkbox']").prop('checked', false);
                }
            });
        });
    </script>
@endsection
