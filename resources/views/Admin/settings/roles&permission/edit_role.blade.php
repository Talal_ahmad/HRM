@extends('Admin.layouts.master')
@section('title', 'Edit Role')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Roles & Permissions</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="#">Roles</a></li>
                                <li class="breadcrumb-item active">Edit Roles</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit Role</h4>
                        </div>
                        <div class="card-body">
                            <form class="form" id="add_form" method="POST" action="{{ url('roles/' . $role->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Role</label>
                                            <input type="text" id="title" class="form-control" placeholder="Role"
                                                value="{{ $role->name }}" name="roll_name" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-1 p-3">
                                    <div class="col-md-12 form-check mb-1">
                                        <input type="checkbox" class="form-check-input" name="check_all" id="check_all"
                                            value="check_all" />
                                        <label class="custom-control-label" for="check_all">Check all</label>
                                    </div>
                                    @foreach ($permission_titles as $permissiontitle)
                                        <h4 class="custom-control-label mx-0 px-0" for="{{ $permissiontitle->name }}">
                                            {{ $permissiontitle->name }}</h4>
                                        @foreach ($permissions as $permission)
                                            @if ($permission->permission_titles_id == $permissiontitle->id)
                                                @php
                                                    $checked = in_array($permission->id, $role_has_permissions)
                                                        ? 'checked'
                                                        : '';
                                                @endphp
                                                <div class="col-md-4 form-check mb-1">
                                                    <input type="checkbox" class="form-check-input" name="permission_name[]"
                                                        id="{{ $permission->name }}" value="{{ $permission->id }}"
                                                        {{ $checked }} />
                                                    <label class="custom-control-label"
                                                        for="{{ $permission->name }}">{{ $permission->name }}</label>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endforeach
                                    <hr>
                                    @foreach ($permissions as $permission)
                                        @if ($permission->permission_titles_id == null)
                                            @php
                                                $checked = in_array($permission->id, $role_has_permissions)
                                                    ? 'checked'
                                                    : '';
                                            @endphp
                                            <div class="col-md-4 form-check mb-1">
                                                <input type="checkbox" class="form-check-input" name="permission_name[]"
                                                    id="{{ $permission->name }}" value="{{ $permission->id }}"
                                                    {{ $checked }} />
                                                <label class="custom-control-label"
                                                    for="{{ $permission->name }}">{{ $permission->name }}</label>
                                            </div>
                                        @endif
                                    @endforeach
                                    <div class="d-flex justify-content-end">
                                        <a href="{{ route('roles.index') }}" class="btn btn-outline-danger">Cancel</a>
                                        <button type="submit" class="btn btn-primary ms-1" id="save">Update</button>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#check_all").on("change", function() {
                if ($(this).is(":checked")) {
                    $("input[type='checkbox']").prop('checked', true);
                } else {
                    $("input[type='checkbox']").prop('checked', false);
                }
            });
        });
    </script>
@endsection
