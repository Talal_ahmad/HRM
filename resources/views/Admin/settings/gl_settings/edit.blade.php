@extends('Admin.layouts.master')
@section('title', 'Edit GL Setting')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Edit GL Setting</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Settings</a>
                                </li>
                                <li class="breadcrumb-item active">Edit GL Setting
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Settings</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('glSettings.update', $setting->id) }}" class="form" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label">Employment Status</label>
                                    <select name="employment_status[]" class="form-select select2 employment-status"
                                        data-placeholder="Select Status" id="employment-status" multiple required>
                                        <option value=""></option>
                                        @foreach ($employment_statuses as $employment_status)
                                            <option value="{{ $employment_status->id }}"
                                                {{ in_array($employment_status->id, $setting->employment_status_id) ? 'selected' : '' }}>
                                                {{ $employment_status->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="button-container mt-1">
                                        <button class="btn btn-sm btn-primary" type="button"
                                            onclick="selectAll('#employment-status')">Select
                                            All</button>
                                        <button class="btn btn-sm btn-danger" type="button"
                                            onclick="deselectAll('#employment-status')">Deselect
                                            All</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label">Departments</label>
                                    <select name="department[]" class="form-select select2"
                                        data-placeholder="Select Department" id="department" multiple required>
                                        <option value=""></option>
                                        @foreach ($departments as $department)
                                            <option value="{{ $department->id }}"
                                                {{ in_array($department->id, $setting->department_id) ? 'selected' : '' }}>
                                                {{ $department->title }}</option>
                                        @endforeach
                                    </select>
                                    <div class="button-container mt-1">
                                        <button class="btn btn-sm btn-primary" type="button"
                                            onclick="selectAll('#department')">Select All</button>
                                        <button class="btn btn-sm btn-danger" type="button"
                                            onclick="deselectAll('#department')">Deselect
                                            All</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label">Payroll Column</label>
                                    <select name="payroll_column[]" class="form-select select2"
                                        data-placeholder="Select Column" id="payroll-column" multiple>
                                        <option value=""></option>
                                        @foreach ($payroll_columns as $payroll_column)
                                            <option value="{{ $payroll_column->id }}"
                                                {{ in_array($payroll_column->id, $setting->payroll_column_id) ? 'selected' : '' }}>
                                                {{ $payroll_column->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="button-container mt-1">
                                        <button class="btn btn-sm btn-primary" type="button"
                                            onclick="selectAll('#payroll-column')">Select
                                            All</button>
                                        <button class="btn btn-sm btn-danger" type="button"
                                            onclick="deselectAll('#payroll-column')">Deselect
                                            All</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label">Column Type</label>
                                    <select name="column_type" class="form-select select2" data-placeholder="Select Type"
                                        >
                                        <option value=""></option>
                                        <option value="credit" {{ $setting->column_type == 'credit' ? 'selected' : '' }}>
                                            Credit</option>
                                        <option value="debit" {{ $setting->column_type == 'debit' ? 'selected' : '' }}>
                                            Debit
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label">Category</label>
                                    <select name="category" class="form-select select2" data-placeholder="Select Category"
                                        id="category" required>
                                        <option value=""></option>
                                        <option value="payroll_accrual"
                                            {{ $setting->category == 'payroll_accrual' ? 'selected' : '' }}>
                                            Payroll Accrual
                                        </option>
                                        <option value="pf_accrual"
                                            {{ $setting->category == 'pf_accrual' ? 'selected' : '' }}>PF
                                            Accrual
                                        </option>
                                        <option value="gratuity_accrual"
                                            {{ $setting->category == 'gratuity_accrual' ? 'selected' : '' }}>
                                            Gratuity Accrual</option>
                                        <option value="eobi_accrual"
                                            {{ $setting->category == 'eobi_accrual' ? 'selected' : '' }}>
                                            EOBI Accrual</option>
                                        <option value="pessi_accrual"
                                            {{ $setting->category == 'pessi_accrual' ? 'selected' : '' }}>
                                            PESSI Accrual</option>
                                        <option value="salary_payment"
                                            {{ $setting->category == 'salary_payment' ? 'selected' : '' }}>
                                            Salary Payment</option>
                                        <option value="gratuity_payment"
                                            {{ $setting->category == 'gratuity_payment' ? 'selected' : '' }}>
                                            Gratuity Payment</option>
                                        <option value="pf_payment"
                                            {{ $setting->category == 'pf_payment' ? 'selected' : '' }}>PF
                                            Payment</option>
                                        <option value="loan_payment"
                                            {{ $setting->category == 'loan_payment' ? 'selected' : '' }}>
                                            Loan Payment</option>
                                        <option value="advance_payment"
                                            {{ $setting->category == 'advance_payment' ? 'selected' : '' }}>
                                            Advance Payment</option>
                                        <option value="fair_price_advance"
                                            {{ $setting->category == 'fair_price_advance' ? 'selected' : '' }}>
                                            Fair Price Advance</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label">Type</label>
                                    <select name="type" class="form-select select2" data-placeholder="Select Type"
                                        id="type" required>
                                        <option value=""></option>
                                        <option value="payroll_accrual"
                                            {{ $setting->type == 'payroll_accrual' ? 'selected' : '' }}>
                                            Payroll Accrual
                                        </option>
                                        <option value="pf_accrual"
                                            {{ $setting->type == 'pf_accrual' ? 'selected' : '' }}>PF
                                            Accrual
                                        </option>
                                        <option value="gratuity_accrual"
                                            {{ $setting->type == 'gratuity_accrual' ? 'selected' : '' }}>
                                            Gratuity Accrual</option>
                                        <option value="eobi_accrual"
                                            {{ $setting->type == 'eobi_accrual' ? 'selected' : '' }}>
                                            EOBI Accrual</option>
                                        <option value="pessi_accrual"
                                            {{ $setting->type == 'pessi_accrual' ? 'selected' : '' }}>
                                            PESSI Accrual</option>
                                        <option value="salary_payment"
                                            {{ $setting->type == 'salary_payment' ? 'selected' : '' }}>
                                            Salary Payment</option>
                                        <option value="gratuity_payment"
                                            {{ $setting->type == 'gratuity_payment' ? 'selected' : '' }}>
                                            Gratuity Payment</option>
                                        <option value="pf_payment"
                                            {{ $setting->type == 'pf_payment' ? 'selected' : '' }}>PF
                                            Payment</option>
                                        <option value="loan_payment"
                                            {{ $setting->type == 'loan_payment' ? 'selected' : '' }}>
                                            Loan Payment</option>
                                        <option value="advance_payment"
                                            {{ $setting->type == 'advance_payment' ? 'selected' : '' }}>
                                            Advance Payment</option>
                                        <option value="fair_price_advance"
                                        {{ $setting->type == 'fair_price_advance' ? 'selected' : '' }}>
                                        Fair Price Advance</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label">Gl Account</label>
                                    <select name="gl_account[]" class="form-select select2"
                                        data-placeholder="Select Account" id="gl-account" multiple
                                        onchange="glCode(this)">
                                        <option value=""></option>
                                        @foreach (getGlAccountsList() as $account)
                                            <option value="{{ $account->id }}"
                                                {{ in_array($account->id, $setting->gl_account_id) ? 'selected' : '' }}>
                                                {{ $account->account_code . ' ' . $account->account_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="button-container mt-1">
                                        <button class="btn btn-sm btn-primary" type="button"
                                            onclick="selectAll('#gl-account')">Select All</button>
                                        <button class="btn btn-sm btn-danger" type="button"
                                            onclick="deselectAll('#gl-account')">Deselect
                                            All</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label">Gl Account Code</label>
                                    <input type="text" name="gl_accounts" class="form-control gl_account_code"
                                        placeholder="Gl Code" value="{{ $setting->gl_accounts }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="mb-1">
                                    <label class="form-label">Bank</label>
                                    <select name="bank_accounts_id[]" class="form-select select2"
                                        data-placeholder="Select Bank" id="bank" onchange="bankName(this)" multiple>
                                        <option value=""></option>
                                        @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}"
                                                {{ in_array($bank->id, $setting->bank_accounts_id) ? 'selected' : '' }}>
                                                {{ $bank->account_code . ' - ' . $bank->bank_account_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="button-container mt-1">
                                        <button class="btn btn-sm btn-primary" type="button"
                                            onclick="selectAll('#bank')">Select All</button>
                                        <button class="btn btn-sm btn-danger" type="button"
                                            onclick="deselectAll('#bank')">Deselect All</button>
                                    </div>
                                </div>
                                <input type="hidden" name="bank_accounts"class="bank_accounts" value="{{$setting->bank_accounts}}">
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="form_save btn btn-primary ms-1" id="save">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        function glCode(object) {
            var options = $(object).find("option:selected");
            var selectedOptions = '';
            options.each(function(index) {
                var accountCode = $.trim($(this).text()).split(" ")[0];
                selectedOptions += accountCode;
                if (index < options.length - 1) {
                    selectedOptions += ',';
                }
            });
            $(object).closest('.row').find('.gl_account_code').val(selectedOptions);
        }
        function bankName(object) {
            var options = $(object).find("option:selected");
            var selectedOptions = '';
            options.each(function(index) {
                var account = $.trim($(this).text());
                selectedOptions += account;
                if (index < options.length - 1) {
                    selectedOptions += ',';
                }
            });
            $(object).closest('.row').find('.bank_accounts').val(selectedOptions);
        }
    </script>
@endsection
