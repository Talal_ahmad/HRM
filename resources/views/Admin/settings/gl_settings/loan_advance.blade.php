@extends('Admin.layouts.master')
@section('title', 'Loan & Advance GL Settings')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Loan & Advance GL Settings</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Settings</a>
                                </li>
                                <li class="breadcrumb-item active">Loan & Advance Gl Settings
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add/Edit Settings</h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('loan-advance-glsettings') }}" class="form" method="POST">
                        @csrf
                        <div class="row" id="gl_settings">
                            <div data-repeater-list="settings">
                                @forelse ($settings as $setting)
                                    <div data-repeater-item>
                                        <input type="hidden" name="setting_id" value="{{ $setting->id }}">
                                        <div class="row">
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Employment Status</label>
                                                    <select name="employment_status" class="form-select select2"
                                                        data-placeholder="Select Status" multiple required>
                                                        <option value=""></option>
                                                        @foreach ($employment_statuses as $employment_status)
                                                            <option value="{{ $employment_status->id }}"
                                                                {{ in_array($employment_status->id, $setting->employment_status_id) ? 'selected' : '' }}>
                                                                {{ $employment_status->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Departments</label>
                                                    <select name="department" class="form-select select2"
                                                        data-placeholder="Select Department" multiple required>
                                                        <option value=""></option>
                                                        @foreach ($departments as $department)
                                                            <option value="{{ $department->id }}"
                                                                {{ in_array($department->id, $setting->department_id) ? 'selected' : '' }}>
                                                                {{ $department->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Column Type</label>
                                                    <select name="column_type" class="form-select select2"
                                                        data-placeholder="Select Type" required>
                                                        <option value=""></option>
                                                        <option value="credit"
                                                            {{ $setting->column_type == 'credit' ? 'selected' : '' }}>
                                                            Credit</option>
                                                        <option value="debit"
                                                            {{ $setting->column_type == 'debit' ? 'selected' : '' }}>Debit
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Category</label>
                                                    <select name="category" class="form-select select2"
                                                        data-placeholder="Select Category" required>
                                                        <option value=""></option>
                                                        <option value="fair_price_advance"
                                                            {{ $setting->category == 'fair_price_advance' ? 'selected' : '' }}>
                                                            Fair Price Advance
                                                        </option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Gl Account</label>
                                                    <select name="gl_account" class="form-select select2"
                                                        data-placeholder="Select Account" multiple required
                                                        onchange="glCode(this)">
                                                        <option value=""></option>
                                                        @foreach (getGlAccountsList() as $account)
                                                            <option value="{{ $account->id }}"
                                                                {{ in_array($account->id, $setting->gl_account_id) ? 'selected' : '' }}>
                                                                {{ $account->account_code . ' ' . $account->account_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Gl Account Code</label>
                                                    <input type="text" name="gl_accounts"
                                                        class="form-control gl_account_code" placeholder="Gl Code"
                                                        value="{{ $setting->gl_accounts }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-1 col-12">
                                                <div class="my-2">
                                                    <button class="btn btn-outline-danger text-nowrap px-1"
                                                        data-repeater-delete type="button">
                                                        <i data-feather="x" class="me-25"></i>
                                                        <span>Delete</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                @empty
                                    <div data-repeater-item>
                                        <div class="row">
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Employment Status</label>
                                                    <select name="employment_status" class="form-select select2"
                                                        data-placeholder="Select Status" multiple required>
                                                        <option value=""></option>
                                                        @foreach ($employment_statuses as $employment_status)
                                                            <option value="{{ $employment_status->id }}">
                                                                {{ $employment_status->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Departments</label>
                                                    <select name="department" class="form-select select2"
                                                        data-placeholder="Select Department" multiple required>
                                                        <option value=""></option>
                                                        @foreach ($departments as $department)
                                                            <option value="{{ $department->id }}">
                                                                {{ $department->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Column Type</label>
                                                    <select name="column_type" class="form-select select2"
                                                        data-placeholder="Select Type" required>
                                                        <option value=""></option>
                                                        <option value="credit">
                                                            Credit</option>
                                                        <option value="debit">Debit
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Category</label>
                                                    <select name="category" class="form-select select2"
                                                        data-placeholder="Select Category" required>
                                                        <option value=""></option>
                                                        <option value="fair_price_advance">Fair Price Advance</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Gl Account</label>
                                                    <select name="gl_account" class="form-select select2"
                                                        data-placeholder="Select Account" multiple required
                                                        onchange="glCode(this)">
                                                        <option value=""></option>
                                                        @foreach (getGlAccountsList() as $account)
                                                            <option value="{{ $account->id }}">
                                                                {{ $account->account_code . ' ' . $account->account_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Gl Account Code</label>
                                                    <input type="text" name="gl_accounts"
                                                        class="form-control gl_account_code" placeholder="Gl Code"
                                                        readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-1 col-12">
                                                <div class="my-2">
                                                    <button class="btn btn-outline-danger text-nowrap px-1"
                                                        data-repeater-delete type="button">
                                                        <i data-feather="x" class="me-25"></i>
                                                        <span>Delete</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                @endforelse
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                        <i data-feather="plus" class="me-25"></i>
                                        <span>Add New</span>
                                    </button>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="form_save btn btn-primary ms-1"
                                    id="save">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    @if (Session::has('message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('message') !!}"
            })
        </script>
    @endif
    <script>
        $(document).ready(function() {
            var gl_settings = $('#gl_settings');
            $(gl_settings).repeater({
                show: function() {
                    $(this).slideDown();
                    gl_settings.find('select').next('.select2-container').remove();
                    gl_settings.find('select').select2();
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                },
            });
        });

        function glCode(object) {
            var options = $(object).find("option:selected");
            var selectedOptions = '';
            options.each(function(index) {
                var accountCode = $.trim($(this).text()).split(" ")[0];
                selectedOptions += accountCode;
                if (index < options.length - 1) {
                    selectedOptions += ',';
                }
            });
            $(object).closest('.row').find('.gl_account_code').val(selectedOptions);
        }
    </script>
@endsection
