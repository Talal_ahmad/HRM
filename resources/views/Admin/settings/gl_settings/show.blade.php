@extends('Admin.layouts.master')
@section('title', 'GL Setting View')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">GL Setting View Result</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Settings</a>
                                </li>
                                <li class="breadcrumb-item active">Gl Setting View Result
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Setting</h4>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label">Employment Status</label>
                                        <p class="border p-1">{{ $employment_statuses }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label">Departments</label>
                                        <p class="border p-1">{{ $departments }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label">Payroll Column</label>
                                        <p class="border p-1">{{ $payroll_columns }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label">Column Type</label>
                                        <p class="border p-1">{{ ucfirst($setting->column_type) }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label">Category</label>
                                        <p class="border p-1">{{ ucwords(str_replace('_', ' ', $setting->category)) }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label">Gl Account Code</label>
                                        <p class="border p-1">{{ $setting->gl_accounts }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label">Bank Accounts</label>
                                        <p class="border p-1">{{ $setting->bank_accounts }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Search Form</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('glSettings.show', $id) }}" class="form" method="GET">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="mb-1">
                                                <label class="form-label">Payroll</label>
                                                <select name="payroll" class="form-select select2"
                                                    data-placeholder="Select Payroll" required>
                                                    <option value=""></option>
                                                    @foreach ($payrolls as $payroll)
                                                        <option value="{{ $payroll->id }}"
                                                            {{ request('payroll') == $payroll->id ? 'selected' : '' }}>
                                                            {{ $payroll->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="form_save btn btn-primary ms-1" id="save">View
                                        Result</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @if (request('payroll'))
                <div class="card" id="result">
                    <div class="card-header">
                        <h4 class="card-title">Result</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Total Employees</th>
                                            <th>Total Amount Found</th>
                                            <th>Total Amount Not Found</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ number_format(count($employees_found)) }}</td>
                                            <td>{{ number_format($amount_found) }}</td>
                                            <td>{{ number_format($amount_not_found) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        var param = "{{request('payroll')}}";
        if (param) {
            $('html, body').animate({
                scrollTop: $('#result').offset().top
            }, 100);
        }
    });
</script>
@endsection
