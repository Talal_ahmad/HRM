@extends('Admin.layouts.master')
@section('title', 'GL Settings')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.GL_Settings')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Home')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.settings')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.GL_Settings')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('app.Add/Edit_Settings')</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('glSettings.store') }}" class="form" method="POST">
                        @csrf
                        <div class="row" id="gl_settings">
                            <div data-repeater-list="settings">
                                <div data-repeater-item>
                                    <div class="row">
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.Employment_Status')</label>
                                                <select name="employment_status" class="form-select select2"
                                                    data-placeholder="@lang('app.Select_Status')" id="employment-status" multiple
                                                    required>
                                                    <option value=""></option>
                                                    @foreach ($employment_statuses as $employment_status)
                                                        <option value="{{ $employment_status->id }}">
                                                            {{ $employment_status->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#employment-status')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#employment-status')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.Departments')</label>
                                                <select name="department" class="form-select select2"
                                                    data-placeholder="@lang('app.Select_Department')" id="department" multiple required>
                                                    <option value=""></option>
                                                    @foreach ($departments as $department)
                                                        <option value="{{ $department->id }}">
                                                            {{ $department->title }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#department')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#department')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.Payroll_Column')</label>
                                                <select name="payroll_column" class="form-select select2"
                                                    data-placeholder="@lang('app.Select_Column')" id="payroll-column" multiple>
                                                    <option value=""></option>
                                                    @foreach ($payroll_columns as $payroll_column)
                                                        <option value="{{ $payroll_column->id }}">
                                                            {{ $payroll_column->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#payroll-column')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#payroll-column')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.Column_Type')</label>
                                                <select name="column_type" class="form-select select2"
                                                    data-placeholder="@lang('app.Select_Type')">
                                                    <option value=""></option>
                                                    <option value="credit">
                                                        Credit</option>
                                                    <option value="debit">Debit
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.Category')</label>
                                                <select name="category" class="form-select select2"
                                                    data-placeholder="@lang('app.Select_Category')" id="category" required>
                                                    <option value=""></option>
                                                    <option value="payroll_accrual">Payroll Accrual</option>
                                                    <option value="pf_accrual">PF Accrual</option>
                                                    <option value="gratuity_accrual">Gratuity Accrual</option>
                                                    <option value="eobi_accrual">EOBI Accrual</option>
                                                    <option value="pessi_accrual">PESSI Accrual</option>
                                                    <option value="salary_payment">Salary Payment</option>
                                                    <option value="gratuity_payment">Gratuity Payment</option>
                                                    <option value="pf_payment">PF Payment</option>
                                                    <option value="loan_payment">Loan Payment</option>
                                                    <option value="advance_payment">Advance Payment</option>
                                                    <option value="fair_price_advance">Fair Price Advance</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.GL_Accounts')</label>
                                                <select name="gl_account" class="form-select select2"
                                                    data-placeholder="@lang('app.Select_Account')" id="gl-account" multiple
                                                    onchange="glCode(this)">
                                                    <option value=""></option>
                                                    @foreach (getGlAccountsList() as $account)
                                                        <option value="{{ $account->id }}">
                                                            {{ $account->account_code . ' ' . $account->account_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#gl-account')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#gl-account')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.Gl_Account_Code')</label>
                                                <input type="text" name="gl_accounts"
                                                    class="form-control gl_account_code" placeholder="@lang('app.Gl_Code')" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.Type')</label>
                                                <select name="type" class="form-select select2"
                                                    data-placeholder="@lang('app.Select_Type')" id="type" required>
                                                    <option value=""></option>
                                                    <option value="payroll_accrual">Payroll Accrual</option>
                                                    <option value="pf_accrual">PF Accrual</option>
                                                    <option value="gratuity_accrual">Gratuity Accrual</option>
                                                    <option value="eobi_accrual">EOBI Accrual</option>
                                                    <option value="pessi_accrual">PESSI Accrual</option>
                                                    <option value="salary_payment">Salary Payment</option>
                                                    <option value="gratuity_payment">Gratuity Payment</option>
                                                    <option value="pf_payment">PF Payment</option>
                                                    <option value="loan_payment">Loan Payment</option>
                                                    <option value="advance_payment">Advance Payment</option>
                                                    <option value="fair_price_advance">Fair Price Advance</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.Bank')</label>
                                                <select name="bank_accounts_id" class="form-select select2"
                                                    data-placeholder="@lang('app.Select_Banks')" id="bank" onchange="bankName(this)" multiple>
                                                    <option value=""></option>
                                                    @foreach ($banks as $bank)
                                                        <option value="{{ $bank->id }}">
                                                            {{ $bank->account_code . ' - ' . $bank->bank_account_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#bank')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#bank')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="bank_accounts"class="bank_accounts">
                                        </div>
                                        <div class="col-md-1 col-12">
                                            <div class="my-2">
                                                <button class="btn btn-outline-danger text-nowrap px-1"
                                                    data-repeater-delete type="button">
                                                    <i data-feather="x" class="me-25"></i>
                                                    <span>@lang('app.Delete')</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                        <i data-feather="plus" class="me-25"></i>
                                        <span>@lang('app.Add_New')</span>
                                    </button>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="form_save btn btn-primary ms-1"
                                    id="save">@lang('app.Save')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var gl_settings = $('#gl_settings');
            $(gl_settings).repeater({
                show: function() {
                    $(this).slideDown();
                    gl_settings.find('select').next('.select2-container').remove();
                    gl_settings.find('select').select2();
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                },
            });
        });

        function glCode(object) {
            var options = $(object).find("option:selected");
            var selectedOptions = '';
            options.each(function(index) {
                var accountCode = $.trim($(this).text()).split(" ")[0];
                selectedOptions += accountCode;
                if (index < options.length - 1) {
                    selectedOptions += ',';
                }
            });
            $(object).closest('.row').find('.gl_account_code').val(selectedOptions);
        }
        function bankName(object) {
            var options = $(object).find("option:selected");
            var selectedOptions = '';
            options.each(function(index) {
                var account = $.trim($(this).text());
                selectedOptions += account;
                if (index < options.length - 1) {
                    selectedOptions += ',';
                }
            });
            $(object).closest('.row').find('.bank_accounts').val(selectedOptions);
        }
    </script>
@endsection
