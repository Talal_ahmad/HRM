@extends('Admin.layouts.master')
@section('title', 'GL Settings List')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.GL_Settings_List')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Home')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Settings')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.GL_Settings_List')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <!--Search Form -->
                            <div class="card-body">
                                <form id="search_form">
                                    <div class="row g-1 mb-md-1">
                                        <div class="col-md-4">
                                            <label class="form-label">@lang('app.Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter"
                                                class="select2 form-select" data-placeholder="@lang('app.Select_Department')">
                                                <option value="">@lang('app.Select_Department')</option>
                                                @foreach ($departments as $department)
                                                    <option value="{{ $department->id }}">{{ $department->title }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">@lang('app.Payroll_Columns'):</label>
                                            <select name="columnFilter" id="columnFilter"
                                                class="select2 form-select" data-placeholder="@lang('app.Select_Column')">
                                                <option value="">@lang('app.Select_Payroll_Column')</option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{ $payroll_column->id }}">{{ $payroll_column->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">@lang('app.Column_Type'):</label>
                                            <select name="columnTypeFilter" id="columnTypeFilter"
                                                class="select2 form-select" data-placeholder="Select TYPE">
                                                <option value="">@lang('app.Select_Type')</option>
                                                <option value="debit">@lang('app.Debit')</option>
                                                <option value="credit">@lang('app.Credit')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-end">
                                            <a href="{{url('glSettings')}}" type="button" class="btn btn-danger">@lang('app.Reset')</a>
                                            <button class="btn btn-primary">@lang('app.Filter')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>ID</th>
                                        <th>Employment Status</th>
                                        <th>Departments</th>
                                        <th>Payroll Column</th>
                                        <th>Column Type</th>
                                        <th>Category</th>
                                        <th>Gl Account</th>
                                        <th>Bank Account</th>
                                        <th>Type</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>

@endsection

@section('scripts')
    @if (Session::has('message'))
        <script>
            Toast.fire({
                icon: 'success',
                title: "{!! Session::get('message') !!}"
            });
        </script>
    @endif
    <script>
        var rowid;
        var dataTable;
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                searching: false,
                ajax: {
                    url: "{{ route('glSettings.index') }}",
                    data: function(filter) {
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.columnFilter = $('#columnFilter').val();
                        filter.columnTypeFilter = $('#columnTypeFilter').val();
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'id',
                        name: 'id',
                    },
                    {
                        data: 'employment_statuses',
                        name: 'employment_statuses',
                    },
                    {
                        data: 'departments',
                        name: 'departments',
                    },
                    {
                        data: 'columns',
                        name: 'columns',
                    },
                    {
                        data: 'column_type',
                        name: 'column_type',
                        render: function(data, type, row) {
                            if(data){
                                return data[0].toUpperCase() + data.substring(1);
                            }
                            else{
                                return '-';
                            }
                        }
                    },
                    {
                        data: 'category',
                        name: 'category',
                        render: function(data, type, row) {
                            // Capitalize the category and replace underscores with spaces
                            var formattedCategory = data.replace(/_/g, ' ').replace(/\b\w/g,
                                function(match) {
                                    return match.toUpperCase();
                                });
                            return formattedCategory;
                        }
                    },
                    {
                        data: 'gl_accounts',
                        name: 'gl_accounts',
                    },
                    {
                        data: 'bank_accounts',
                        name: 'bank_accounts',
                    },
                    {
                        data: 'type',
                        name: 'type',
                        render: function(data, type, row) {
                            if(data){
                                // Capitalize the category and replace underscores with spaces
                                var formattedType = data.replace(/_/g, ' ').replace(/\b\w/g,
                                function(match) {
                                    return match.toUpperCase();
                                });
                            return formattedType;
                            }
                        }
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            let lockUnlockTitle = full.locked == 0 ? 'Lock Setting' : 'Unlock Setting';
                            let edit = '';
                            if(full.locked == 0){
                                edit = '<a href="javascript:;" class="item-edit" title="Edit" onclick=edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                });
                            }
                            return (
                                edit+
                                '<a href="{{ url('glSettings') }}' + '/' + full.id + '" title="View Result">' +
                                feather.icons['eye'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>' 
                                +
                                '<a href="javascript:;" onclick="lockUnlock(' + full.id +
                                ','+ (full.locked == 0 ? 1 : 0) +')" title="'+lockUnlockTitle+'">' +
                                feather.icons[full.locked == 0 ? 'lock' : 'unlock'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                                +
                                '<a href="javascript:;" onclick="deleteRecord(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        action: function(e, dt, node, config) {
                            window.location.href = '{{ route('glSettings.create') }}';
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of GL Settings</h6>');
        });

        function edit(id) {
            window.location.href = "{{ url('glSettings') }}" + "/" + id + "/edit";
        }

        function lockUnlock(id, action) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to Perform this action!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "glsettings-lockunlock/" + id,
                                type: "POST",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    action: action
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Action has been Done Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        function deleteRecord(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "glSettings/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Setting has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Filter Function
        $('#search_form').submit(function(e) {
            e.preventDefault();
            dataTable.draw();
        });
    </script>
@endsection
