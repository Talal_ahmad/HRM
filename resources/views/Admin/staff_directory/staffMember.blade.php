@extends('Admin.layouts.master')
@section('title', 'Employee')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Staff Directory</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Employee
                            </li>
                            <li class="breadcrumb-item active">
                                {{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="page-account-settings">
            <div class="row">
                <div class="col-md-3 mb-2 mb-md-0">
                    <ul class="nav nav-pills flex-column nav-left">
                        <!-- Basic Information -->
                        <li class="nav-item">
                            <a class="nav-link active" id="account-pill-basic-information" data-bs-toggle="pill" href="#account-vertical-basic-information" aria-expanded="true">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Basic Information</span>
                            </a>
                        </li>

                        {{-- Advance --}}
                        <li class="nav-item">
                            <a class="nav-link" onclick="advance()" id="account-pill-advance" data-bs-toggle="pill" href="#account-vertical-advance" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Advance</span>
                            </a>
                        </li>

                        {{-- Company Belongings --}}
                        <li class="nav-item">
                            <a class="nav-link" onclick="belongings()" id="account-pill-belongings" data-bs-toggle="pill" href="#account-vertical-belongings" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Company Belongings</span>
                            </a>
                        </li>

                        <!-- Documents -->
                        <li class="nav-item">
                            <a class="nav-link" onclick="employeeDocuments()" id="account-pill-documents" data-bs-toggle="pill" href="#account-vertical-documents" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Documents</span>
                            </a>
                        </li>

                        {{-- Discipline/Inquiry --}}
                        <li class="nav-item">
                            <a class="nav-link" onclick="inquiry()" id="account-pill-inquiry" data-bs-toggle="pill" href="#account-vertical-inquiry" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Discipline/Inquiry</span>
                            </a>
                        </li>

                        {{-- Entitlements --}}
                        <li class="nav-item">
                            <a class="nav-link" onclick="entitlement()" id="account-pill-entitlements" data-bs-toggle="pill" href="#account-vertical-entitlements" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Entitlements</span>
                            </a>
                        </li>

                        {{-- Employee Leaves --}}
                        <li class="nav-item">
                            <a class="nav-link" onclick="leaves()" id="account-pill-leaves" data-bs-toggle="pill" href="#account-vertical-leaves" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Employee Leaves</span>
                            </a>
                        </li>

                        <!-- Family -->
                        <li class="nav-item">
                            <a class="nav-link" onclick="emergencyContacts()" id="account-pill-family" data-bs-toggle="pill" href="#account-vertical-family" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Family</span>
                            </a>
                        </li>

                        {{-- Insurance --}}
                        <li class="nav-item">
                            <a class="nav-link" onclick="insurance()" id="account-pill-insurance" data-bs-toggle="pill" href="#account-vertical-insurance" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Insurance</span>
                            </a>
                        </li>

                        {{-- Loan --}}
                        <li class="nav-item">
                            <a class="nav-link" onclick="loan()" id="account-pill-loan" data-bs-toggle="pill" href="#account-vertical-loan" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Loan</span>
                            </a>
                        </li>

                        {{-- <!-- Legals -->
                            <li class="nav-item">
                                <a class="nav-link" onclick="legals()" id="account-pill-legals" data-bs-toggle="pill" href="#account-vertical-legals" aria-expanded="false">
                                    <i data-feather="circle" class="font-medium-3 me-1"></i>
                                    <span class="fw-bold">Legals</span>
                                </a>
                            </li> --}}

                        {{-- Medical Claims --}}
                        <li class="nav-item">
                            <a class="nav-link" onclick="medical_claims()" id="account-pill-medical" data-bs-toggle="pill" href="#account-vertical-medical" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Medical Claims</span>
                            </a>
                        </li>

                        <!-- Qualification -->
                        <li class="nav-item">
                            <a class="nav-link" onclick="skills()" id="account-pill-qualification" data-bs-toggle="pill" href="#account-vertical-qualification" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Qualification</span>
                            </a>
                        </li>

                        {{-- Salary History --}}
                        <li class="nav-item">
                            <a class="nav-link" id="account-pill-salary_history" data-bs-toggle="pill" href="#account-vertical-salary_history" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Salary History</span>
                            </a>
                        </li>

                        <!-- Training -->
                        <li class="nav-item">
                            <a class="nav-link" onclick="training()" id="account-pill-training" data-bs-toggle="pill" href="#account-vertical-training" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Training</span>
                            </a>
                        </li>

                        <!-- Travels -->
                        <li class="nav-item">
                            <a class="nav-link" onclick="travels()" id="account-pill-travel" data-bs-toggle="pill" href="#account-vertical-travel" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Travels</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" onclick="disciplinaries()" id="account-pill-disciplinaries" data-bs-toggle="pill" href="#account-vertical-disciplinaries" aria-expanded="false">
                                <i data-feather="circle" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Disciplinaries</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                {{-- Advance --}}
                                <div class="tab-pane fade" id="account-vertical-advance" role="tabpanel" aria-labelledby="account-pill-advance" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="advance_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Remarks</th>
                                                                    <th>Advance Amount</th>
                                                                    <th>Issue Date</th>
                                                                    <th>Deduction</th>
                                                                    <th>Deducted</th>
                                                                    <th class="not_include">Remaining Balance</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                <!-- Basic Information Tab -->
                                <div role="tabpanel" class="tab-pane active" id="account-vertical-basic-information" aria-labelledby="account-pill-basic-information" aria-expanded="true">
                                    <div class="d-flex mb-1">
                                        <a href="#" class="me-25">
                                            @if (file_exists(public_path('images/employees/' . $employee->image)))
                                            <img src="{{asset('images/employees/' . $employee->image)}}" id="account-upload-img" class="rounded me-50" alt="profile image" height="80" width="80" />
                                            @else
                                            <img src="{{asset('images/employees/default_employee.png')}}" id="account-upload-img" class="rounded me-50" alt="profile image" height="80" width="80" />
                                            @endif
                                        </a>
                                        <div class="mt-75 ms-1">
                                            <h3>{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}</h3>
                                            @if($employee->status == 'Active')
                                            <button class="btn btn-sm btn-primary mb-75" data-bs-toggle="modal" data-bs-target="#employee_image_modal"><i class="fas fa-upload"></i> Upload</button>
                                            @can('Edit Employee in Employee Profile')
                                                <a href="{{url('employees'.'/'.$employee->id.'/edit')}}" target="blank"><button class="btn btn-sm btn-success mb-75"><i class="fas fa-edit"></i> Edit Info</button></a>
                                            @endcan
                                            @endif

                                        </div>
                                        @if (!empty($jsonData_for_base64_image) && $face_img)
                                            <div>
                                                <img src="{{$jsonData_for_base64_image}}" id="account-upload-img" class="rounded ms-50" alt="profile image" height="80" width="80" alt="">
                                                <small>Recognition</small>
                                            </div>
                                        @endif
                                        <!-- Basic Info Add Modal -->
                                        <div class="modal fade text-start" id="employee_image_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel17">Upload Image</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form class="form" id="image_form">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <input type="hidden" id="employee" class="form-control" name="id" value={{ $employee->id }} />
                                                                <div class="col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="title">Upload Image</label>
                                                                        <input type="file" id="type" class="form-control" name="employee_image" required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="reset" class="btn btn-outline-success">Reset</button>
                                                            <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!-- Personal Information -->
                                        <div class="col-12">
                                            <div class="d-flex align-items-center mb-2">
                                                <i data-feather="user" class="font-medium-3"></i>
                                                <h4 class="mb-0 ms-75">Personal Information</h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    @if (env('COMPANY') == 'CLINIX')
                                                    <p class="fw-bold mb-0">Father Name</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->father_name) }}</p>
                                                    @endif
                                                    <p class="fw-bold mb-0">Gender</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->gender) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    @if (env('COMPANY') == 'JSML')
                                                    <p class="fw-bold mb-0">Driving License</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->driving_license) }}</p>
                                                    @endif
                                                    <p class="fw-bold mb-0">Nationality</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->nation) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Other ID</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->other_id) }}</p>
                                                    <p class="fw-bold mb-0">Marital Status</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->marital_status) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Date of Birth</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->birthday) }}</p>
                                                    <p class="fw-bold mb-0">Joined Date</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->joined_date) }}</p>
                                                </div>
                                                @if (env('COMPANY') == 'CLINIX')
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Blood Group</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->blood_group) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">PPC Registeration Number</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->ppc_reg_no) }}</p>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <hr class="my-2" />
                                        </div>
                                        {{-- Contact Information --}}
                                        <div class="col-12 mt-1">
                                            <div class="d-flex align-items-center mb-3">
                                                <i data-feather="link" class="font-medium-3"></i>
                                                <h4 class="mb-0 ms-75">Contact Information</h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Address Line 1</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->address1) }}</p>
                                                    <p class="fw-bold mb-0">Postal/Zip Code</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->postal_code) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Address Line 2</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->address2) }}</p>
                                                    <p class="fw-bold mb-0">Home Phone</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->home_phone) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">City</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->city) }}</p>
                                                    <p class="fw-bold mb-0">Work Phone</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->work_phone) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Country</p>
                                                    <p class="mb-1">{{ HandleEmpty($employee->country_code) }}</p>
                                                    <p class="fw-bold mb-0">Private Email</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->private_email) }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <hr class="my-2" />
                                        </div>
                                        <!-- Job Information -->
                                        <div class="col-12">
                                            <div class="d-flex align-items-center mb-2">
                                                <i data-feather="user" class="font-medium-3"></i>
                                                <h4 class="mb-0 ms-75">Job Information</h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Job Title</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->designation) }}</p>
                                                    <p class="fw-bold mb-0">Department</p>
                                                    <p class="mb-0">{{ $employee->empDept }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Employment Status</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->employment) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Supervisor</p>
                                                    <p class="mb-1">{{HandleEmpty($employee->super_first_name)}} {{HandleEmpty($employee->super_last_name)}}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Confirmation Date</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->confirmation_date) }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <hr class="my-2" />
                                        </div>
                                        {{-- Termination/Restore Detail --}}
                                        <div class="col-12">
                                            <div class="d-flex align-items-center mb-2">
                                                <i data-feather="user" class="font-medium-3"></i>
                                                <h4 class="mb-0 ms-75">Termination/Restore Information</h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Termination Date</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->termination_date) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Restore Date</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->restore_date) }}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">Off Roll Date</p>
                                                    <p class="mb-1">{{HandleEmpty($employee->off_roll_date)}}</p>
                                                </div>
                                                <div class="col-6 col-md-3 text-center mb-1">
                                                    <p class="fw-bold mb-0">On Roll Date</p>
                                                    <p class="mb-0">{{ HandleEmpty($employee->on_roll_date) }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Company Belongings --}}
                                <div class="tab-pane fade" id="account-vertical-belongings" role="tabpanel" aria-labelledby="account-pill-belongings" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="belongings_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Asset</th>
                                                                    <th>Department</th>
                                                                    <th>Date Issue</th>
                                                                    <th>Recieved Back Date</th>
                                                                    <th>Recieved Back</th>
                                                                    <th class="not_include">Remarks</th>
                                                                    <th class="not_include">Attachment</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                <!-- Documents -->
                                <div class="tab-pane fade" id="account-vertical-documents" role="tabpanel" aria-labelledby="account-pill-documents" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="documents_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Document</th>
                                                                    <th>Date Added</th>
                                                                    <th>Valid Until</th>
                                                                    <th>Details</th>
                                                                    <th>Status</th>
                                                                    <th>Attachment</th>
                                                                    <th class="not_include">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <!-- Add Employee Documents Modal -->
                                        <div class="modal fade text-start" id="add_document_modal" tabindex="-1" aria-labelledby="add_modal_emply_doc" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="add_modal_emply">Add Employee Document</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form class="form" id="add_document_form">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <input type="hidden" id="employee" class="form-control" name="employee" value={{ $employee->id }} />
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="name">Employee</label>
                                                                        <input type="text" id="name" class="form-control" value="{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}" name="name" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="document">Document</label>
                                                                        <select name="document" id="document" class="select2 form-select" data-placeholder="Select Document Type">
                                                                            <option value=""></option>
                                                                            @foreach ($document_type as $document)
                                                                            <option value="{{ $document->id }}">{{$document->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="date_added">Date Added</label>
                                                                        <input type="text" name="date_added" id="date_added" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="valid_until">Valid Until</label>
                                                                        <input type="text" name="valid_until" id="valid_until" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="status">Status</label>
                                                                        <select name="status" id="status" class="select2 form-select" data-placeholder="Select Status" required>
                                                                            <option value=""></option>
                                                                            <option value="Active">Active</option>
                                                                            <option value="Inactive">Inactive</option>
                                                                            <option value="Draft">Draft</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label for="formFile" class="form-label">Attachment</label>
                                                                        <input class="form-control" type="file" id="attachment" name="attachment" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="details">Details</label>
                                                                        <textarea class="form-control" name="details" id="details" cols="20" rows="2" placeholder="Type here..." required></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="reset" class="btn btn-outline-success">Reset</button>
                                                            <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Edit Employee Documents Modal --}}
                                        <div class="modal fade text-start" id="edit_document_modal" tabindex="-1" aria-labelledby="add_modal_emply_doc" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="add_modal_emply">Update Employee Document</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form class="form" id="edit_document_form">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="employee">Employee</label>
                                                                        <input type="text" id="name" class="form-control" value="{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}" name="name" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="edit_document">Document</label>
                                                                        <select name="document" id="edit_document" class="select2 form-select" data-placeholder="Select Document Type">
                                                                            <option value=""> </option>
                                                                            @foreach ($document_type as $document)
                                                                            <option value="{{ $document->id }}">{{ $document->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="edit_date_added">Date Added</label>
                                                                        <input type="text" name="date_added" id="edit_date_added" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="edit_valid_until">Valid Until</label>
                                                                        <input type="text" name="valid_until" id="edit_valid_until" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="edit_status">Status</label>
                                                                        <select name="status" id="edit_status" class="select2 form-select" required>
                                                                            <option value="Active">Active</option>
                                                                            <option value="Inactive">Inactive</option>
                                                                            <option value="Draft">Draft</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12">
                                                                    <div class="mb-1">
                                                                        <label for="formFile" class="form-label">Attachment</label>
                                                                        <input class="form-control" type="file" id="attachment" name="attachment" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="edit_details">Details</label>
                                                                        <textarea class="form-control" name="details" id="edit_details" cols="20" rows="2" placeholder="Type here..." required></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="reset" class="btn btn-outline-success">Reset</button>
                                                            <button type="submit" class="btn btn-primary" id="save">Update</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Entitlements --}}
                                <div class="tab-pane fade" id="account-vertical-entitlements" role="tabpanel" aria-labelledby="account-pill-entitlements" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="entitlement_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Entitlements</th>
                                                                    <th>Department</th>
                                                                    <th>Date From</th>
                                                                    <th>Date To</th>
                                                                    <th class="not_include">Remarks</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                {{-- Employee Leaves --}}
                                <div class="tab-pane fade" id="account-vertical-leaves" role="tabpanel" aria-labelledby="account-pill-leaves" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="leaves_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Leave Type</th>
                                                                    <th>Allowed</th>
                                                                    <th>Availed</th>
                                                                    <th class="not_include">Balance</th>
                                                                    <th class="not_include">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        {{-- Leave Encashment Add Modal --}}
                                        <div class="modal fade text-start" id="add_encashment_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel17">Add Leaves Encashment</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form class="form" id="add_encashment_form">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="encashment_leave_type">Leave Types</label>
                                                                        <select name="leave_type" id="encashment_leave_type" class="select2 form-select" data-placeholder="Select Leave Types" required>
                                                                            <option value=""></option>
                                                                            @foreach ($leave_types as $leave_type)
                                                                            <option value="{{ $leave_type->id }}">{{ $leave_type->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="title">Year</label>
                                                                        <input type="number" id="title" class="form-control" placeholder="Year" name="year" required />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="title">Leaves</label>
                                                                        <input type="number" name="amount" id="title" class="form-control" placeholder="Leaves" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="title">Details</label>
                                                                        <textarea id="trainer_info" class="form-control" cols="20" rows="2" placeholder="Enter Details..." name="remarks"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="reset" class="btn btn-outline-success">Reset</button>
                                                            <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Add Leave Balance Modal --}}
                                        <div class="modal fade text-start" id="add_leave_balance_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel17">Add Leave Type Balance</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form class="form" id="add_leave_balance_form">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="balance_leave_type">Leave Types</label>
                                                                        <select name="leave_type" id="balance_leave_type" class="select2 form-select" data-placeholder="Select Leave Types" required>
                                                                            <option value=""></option>
                                                                            @foreach ($leave_types as $leave_type)
                                                                            <option value="{{ $leave_type->id }}">{{ $leave_type->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="title">Amount</label>
                                                                        <input type="number" id="title" class="form-control" placeholder="Amount" name="balance" required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="reset" class="btn btn-outline-success">Reset</button>
                                                            <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Add Leave Modal --}}
                                        <div class="modal fade text-start" id="add_leave_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel17">Add Leave Type Balance</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form class="form" id="add_leave_form">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="add_leave_type">Leave Types</label>
                                                                        <select name="leave_type" id="add_leave_type" class="select2 form-select" data-placeholder="Select Leave Types" required>
                                                                            <option value=""></option>
                                                                            @foreach ($leave_types as $leave_type)
                                                                            <option value="{{ $leave_type->id }}">{{ $leave_type->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="title">Leave Start date</label>
                                                                        <input type="text" id="title" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" name="date_start" required />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="title">Leave End date</label>
                                                                        <input type="text" id="title" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" name="date_end" required />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="mb-1">
                                                                        <label class="form-label" for="title">Details</label>
                                                                        <textarea id="trainer_info" class="form-control" cols="20" rows="2" placeholder="Enter Details..." name="remarks"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="mb-1">
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" name="is_paid" id="status" value="Approved" />
                                                                            <label class="custom-control-label" for="status">Paid Leave</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="reset" class="btn btn-outline-success">Reset</button>
                                                            <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Balance Log Modal --}}
                                        <div class="modal fade text-start" id="balance_log_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                            <div class="modal-dialog modal-xl">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel17">Balance Log</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body" id="balance_log_modal_body">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Balance Edit Modal --}}
                                        <div class="modal fade text-start" id="edit_balance_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel17">Update Balance</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form class="form" id="edit_balance_leave_type_form">
                                                        @csrf
                                                        <input type="hidden" name="leaveType" id="edit_balance_leave_type_id">
                                                        <input type="hidden" name="employee" id="edit_balance_leave_type_employee">
                                                        <input type="hidden" name="">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <blockquote style="color: #ff6347;">
                                                                        <p>Note: This balance is added using the <em>Add Leave Balance</em> button placed on this page.</p>
                                                                    </blockquote>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="mb-1">
                                                                        <label class="form-label">Leave Type</label>
                                                                        <input type="text" id="edit_balance_leave_type_name" class="form-control" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="mb-1">
                                                                        <label class="form-label">Amount</label>
                                                                        <input type="number" id="edit_balance_leave_type_amount" class="form-control" placeholder="Amount" name="balance" required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Family -->
                                <div class="tab-pane fade" id="account-vertical-family" role="tabpanel" aria-labelledby="account-pill-family" aria-expanded="false">
                                    <!-- Content -->
                                    <div class="content-header row">
                                        <div class="content-body">
                                            <section class="basic-tabs-components">
                                                <ul class="nav nav-pills flex-row nav-left">
                                                    <!-- Emergency Contacts -->
                                                    <li class="nav-item">
                                                        <a class="nav-link active" onclick="emergencyContacts()" id="account-pill-emergency-contacts" data-bs-toggle="pill" href="#account-vertical-emergency-contacts" aria-expanded="true">
                                                            <span class="fw-bold">Emergency Contacts</span>
                                                        </a>
                                                    </li>
                                                    <!-- Dependants -->
                                                    <li class="nav-item">
                                                        <a class="nav-link" onclick="employeeDepandents()" id="account-pill-dependants" data-bs-toggle="pill" href="#account-vertical-dependants" aria-expanded="false">
                                                            <span class="fw-bold">Depandents</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <!-- Emergency Contacts Tab -->
                                                    <div role="tabpanel" class="tab-pane active" id="account-vertical-emergency-contacts" aria-labelledby="account-pill-emergency-contacts" aria-expanded="true">
                                                        <div class="row" id="department_change_tab">
                                                            {{-- Emergency Contacts DataTable --}}
                                                            <section id="emergency-contacts-dataTable">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <table class="table" id="emergency_contacts_dataTable">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="not_include"></th>
                                                                                        <th>Sr.No</th>
                                                                                        <th>Name</th>
                                                                                        <th>Relationship</th>
                                                                                        <th>Work Phone</th>
                                                                                        <th>Home Phone</th>
                                                                                        <th>Mobile Phone</th>
                                                                                        <th class="not_include">Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            {{-- Emergency Contacts Add Modal --}}
                                                            <div class="modal fade text-start" id="add_emergency_contacts_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Add Emergency Contacts</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="add_emergency_contacts_form">
                                                                            @csrf
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Name</label>
                                                                                            <input type="text" id="title" class="form-control" name="name" placeholder="Name" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Relationship</label>
                                                                                            <input type="text" id="title" class="form-control" placeholder="Relationship" name="relationship" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Home Phone</label>
                                                                                            <input type="text" id="title" class="form-control" placeholder="Home Phone" name="home_phone" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Work Phone</label>
                                                                                            <input type="text" id="title" class="form-control" placeholder="Work Phone" name="work_phone" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Mobile Phone</label>
                                                                                            <input type="text" id="title" class="form-control" name="mobile_phone" placeholder="Mobile Phone" required />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- Emergency Contacts Edit Modal --}}
                                                            <div class="modal fade text-start" id="edit_emergency_contacts_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Edit Emergency Contacts</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="edit_emergency_contacts_form">
                                                                            @csrf
                                                                            @method('PUT')
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="contact_name">Name</label>
                                                                                            <input type="text" id="contact_name" class="form-control" name="name" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="contact_relation">Relationship</label>
                                                                                            <input type="text" id="contact_relation" class="form-control" name="relationship" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="contact_home_phone">Home Phone</label>
                                                                                            <input type="text" id="contact_home_phone" class="form-control" name="home_phone" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="contact_work_phone">Work Phone</label>
                                                                                            <input type="text" id="contact_work_phone" class="form-control" name="work_phone" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="contact_mobile_phone">Mobile Phone</label>
                                                                                            <input type="text" id="contact_mobile_phone" class="form-control" name="mobile_phone" required />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Update</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- Dependants Tab -->
                                                    <div class="tab-pane fade" id="account-vertical-dependants" role="tabpanel" aria-labelledby="account-pill-dependants" aria-expanded="false">
                                                        <div class="row" id="leaveRequest_tab">
                                                            {{-- Depandants DataTable --}}
                                                            <section id="depandents-dataTable">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <table class="table" id="depandents_dataTable">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="not_include"></th>
                                                                                        <th>Sr.No</th>
                                                                                        <th>Name</th>
                                                                                        <th>Relationship</th>
                                                                                        <th>Birth Date</th>
                                                                                        <th class="not_include">Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            {{-- Depandents Add Modal --}}
                                                            <div class="modal fade text-start" id="add_depandents_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Add Depandents</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="add_depandents_form">
                                                                            @csrf
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Name</label>
                                                                                            <input type="text" id="title" class="form-control" placeholder="Name" name="name" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="type">Relationship</label>
                                                                                            <select name="relationship" id="type" class="select2 form-select" data-placeholder="Select Relationship">
                                                                                                <option value=""></option>
                                                                                                <option value="Child">Child</option>
                                                                                                <option value="Spouse">Spouse</option>
                                                                                                <option value="Parent">Parent</option>
                                                                                                <option value="Other">Other</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Date Of Birth</label>
                                                                                            <input type="text" name="dob" id="title" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">ID Number</label>
                                                                                            <input type="test" name="id_number" id="title" class="form-control" placeholder="ID">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- Depandents Edit Modal --}}
                                                            <div class="modal fade text-start" id="edit_depandents_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Edit Depandents</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="edit_depandents_form">
                                                                            @csrf
                                                                            @method('PUT')
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="depandent_name">Name</label>
                                                                                            <input type="text" id="depandent_name" class="form-control" name="name" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="depandent_relation">Relationship</label>
                                                                                            <select name="relationship" id="depandent_relation" class="select2 form-select">
                                                                                                <option value="">Select Qualification</option>
                                                                                                <option value="Child">Child</option>
                                                                                                <option value="Spouse">Spouse</option>
                                                                                                <option value="Parent">Parent</option>
                                                                                                <option value="Other">Other</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="depandent_dob">Date Of Birth</label>
                                                                                            <input type="text" name="dob" id="depandent_dob" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="depandent_id">ID Number</label>
                                                                                            <input type="number" name="id_number" id="depandent_id" class="form-control" placeholder="ID">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Update</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>

                                {{-- Inquiry --}}
                                <div class="tab-pane fade" id="account-vertical-inquiry" role="tabpanel" aria-labelledby="account-pill-inquiry" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="inquiry_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Title</th>
                                                                    <th>Enquiry Start On</th>
                                                                    <th>Enquiry End On</th>
                                                                    <th>Inquiry Officer</th>
                                                                    <th>Designation</th>
                                                                    <th>Findings</th>
                                                                    <th>Action Taken</th>
                                                                    <th class="not_include">Description</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                {{-- Insurance --}}
                                <div class="tab-pane fade" id="account-vertical-insurance" role="tabpanel" aria-labelledby="account-pill-insurance" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="insurance_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Insurance Type</th>
                                                                    <th>Insurance Sub Types</th>
                                                                    <th>Policy Number</th>
                                                                    <th>Start Date</th>
                                                                    <th>End Date</th>
                                                                    <th class="not_include">Total Amount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                {{-- Loan --}}
                                <div class="tab-pane fade" id="account-vertical-loan" role="tabpanel" aria-labelledby="account-pill-loan" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="loan_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Remarks</th>
                                                                    <th>Loan Amount</th>
                                                                    <th>Issue Date</th>
                                                                    <th>Deduction</th>
                                                                    <th>Deducted</th>
                                                                    <th class="not_include">Remaining Balance</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                <!-- Legals -->
                                <div class="tab-pane fade" id="account-vertical-legals" role="tabpanel" aria-labelledby="account-pill-legals" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="legals_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Legal Action</th>
                                                                    <th>Legal Proceeding Title</th>
                                                                    <th>Legal Counsel Company</th>
                                                                    <th>Employee</th>
                                                                    <th>Legal Counsel Employee</th>
                                                                    <th>Case Filed On</th>
                                                                    <th>Field In The Court</th>
                                                                    <th>Case Type</th>
                                                                    <th class="not_include">Petition/ Case Copy</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                {{-- Medical Claims --}}
                                <div class="tab-pane fade" id="account-vertical-medical" role="tabpanel" aria-labelledby="account-pill-medical" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="medical_claims_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Description</th>
                                                                    <th class="not_include">Attachment</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                <!-- Qualification -->
                                <div class="tab-pane fade" id="account-vertical-qualification" role="tabpanel" aria-labelledby="account-pill-qualification" aria-expanded="false">
                                    <div class="content-header row">
                                        <div class="content-body">
                                            <section class="basic-tabs-components">
                                                <ul class="nav nav-pills flex-row nav-left">
                                                    <!-- Skills -->
                                                    <li class="nav-item">
                                                        <a class="nav-link active" onclick="skills()" id="account-pill-skills" data-bs-toggle="pill" href="#account-vertical-skills" aria-expanded="true">
                                                            <span class="fw-bold">Skills</span>
                                                        </a>
                                                    </li>
                                                    <!-- Education -->
                                                    <li class="nav-item">
                                                        <a class="nav-link" onclick="education()" id="account-pill-education" data-bs-toggle="pill" href="#account-vertical-education" aria-expanded="false">
                                                            <span class="fw-bold">Education</span>
                                                        </a>
                                                    </li>
                                                    <!-- Languages -->
                                                    <li class="nav-item">
                                                        <a class="nav-link" onclick="languages()" id="account-pill-languages" data-bs-toggle="pill" href="#account-vertical-languages" aria-expanded="false">
                                                            <span class="fw-bold">Languages</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="account-vertical-skills" aria-labelledby="account-pill-skills" aria-expanded="true">
                                                        <div class="row" id="department_change_tab">
                                                            {{-- Skills DataTable --}}
                                                            <section id="skills-dataTable">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <table class="table" id="skills_dataTable">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="not_include"></th>
                                                                                        <th>Sr.No</th>
                                                                                        <th>Details</th>
                                                                                        <th>File</th>
                                                                                        <th class="not_include">Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            {{-- Skill Add Modal --}}
                                                            <div class="modal fade text-start" id="add_skill_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Add Education</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="add_skill_form">
                                                                            @csrf
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Upload File</label>
                                                                                            <input type="file" id="title" class="form-control" name="file" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12 col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="trainer_info">Details</label>
                                                                                            <textarea id="trainer_info" class="form-control" cols="20" rows="2" placeholder="Enter Details..." name="details"></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- Skill Edit Modal --}}
                                                            <div class="modal fade text-start" id="skill_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Edit Skill</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="edit_skill_form">
                                                                            @csrf
                                                                            @method('PUT')
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="file">Upload File</label>
                                                                                            <input type="file" id="file" class="form-control" name="file" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12 col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="skill_details">Details</label>
                                                                                            <textarea id="skill_details" class="form-control" cols="20" rows="2" placeholder="Enter Details..." name="details"></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Update</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- Education Tab -->
                                                    <div class="tab-pane fade" id="account-vertical-education" role="tabpanel" aria-labelledby="account-pill-education" aria-expanded="false">
                                                        <div class="row" id="leaveRequest_tab">
                                                            {{-- Education DataTable --}}
                                                            <section id="education-dataTable">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <table class="table" id="education_dataTable">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="not_include"></th>
                                                                                        <th>Sr.No</th>
                                                                                        <th>Qualification</th>
                                                                                        <th>Institute</th>
                                                                                        <th>Start Date</th>
                                                                                        <th>End Date</th>
                                                                                        <th>Grade</th>
                                                                                        <th>Marks</th>
                                                                                        <th class="not_include">Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            {{-- Education Add Modal --}}
                                                            <div class="modal fade text-start" id="add_education_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Add Education</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="add_education_form">
                                                                            @csrf
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <div class="mb-1">
                                                                                                <label class="form-label" for="education_id">Qualification</label>
                                                                                                <select name="education_id" id="education_id" class="select2 form-select" data-placeholder="Select Qualification">
                                                                                                    <option value=""></option>
                                                                                                    @foreach ($educations as $education)
                                                                                                    <option value="{{ $education->id }}">{{ $education->name }}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Institute</label>
                                                                                            <input type="text" id="title" class="form-control" placeholder="Institute" name="institute" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Start Date</label>
                                                                                            <input type="text" name="date_start" id="title" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">End Date</label>
                                                                                            <input type="text" name="date_end" id="title" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Grade</label>
                                                                                            <input type="text" id="title" class="form-control" placeholder="Grade" name="grade" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">Marks/Percentage</label>
                                                                                            <input type="text" id="title" class="form-control" placeholder="Marks/Percentage" name="marks" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="title">File</label>
                                                                                            <input type="file" id="title" class="form-control" name="file" required />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- Education Edit Modal --}}
                                                            <div class="modal fade text-start" id="edit_education_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Edit Education</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="edit_education_form">
                                                                            @csrf
                                                                            @method('PUT')
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <div class="mb-1">
                                                                                                <label class="form-label" for="qualification">Qualification</label>
                                                                                                <select name="education_id" id="qualification" class="select2 form-select">
                                                                                                    <option value="">Select Qualification</option>
                                                                                                    @foreach ($educations as $education)
                                                                                                    <option value="{{ $education->id }}">
                                                                                                        {{ $education->name }}
                                                                                                    </option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="institute">Institute</label>
                                                                                            <input type="text" id="institute" class="form-control" name="institute" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="date_start">Start Date</label>
                                                                                            <input type="text" name="date_start" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="date_start">End Date</label>
                                                                                            <input type="text" name="date_end" id="date_end" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="grade">Grade</label>
                                                                                            <input type="text" id="grade" class="form-control" name="grade" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="marks">Marks/Percentage</label>
                                                                                            <input type="text" id="marks" class="form-control" name="marks" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="file">File</label>
                                                                                            <input type="file" id="file" class="form-control" name="file" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Update</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Languages Tab -->
                                                    <div class="tab-pane fade" id="account-vertical-languages" role="tabpanel" aria-labelledby="account-pill-languages" aria-expanded="false">
                                                        <div class="row" id="shortLeave_tab">
                                                            <section id="languages-dataTable">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <table class="table" id="languages_dataTable">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="not_include"></th>
                                                                                        <th>Sr.No</th>
                                                                                        <th>Language</th>
                                                                                        <th>Reading</th>
                                                                                        <th>Speaking</th>
                                                                                        <th>Writing</th>
                                                                                        <th>Understanding</th>
                                                                                        <th class="not_include">Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            {{-- language Add Modal --}}
                                                            <div class="modal fade text-start" id="add_language_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Add Language</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="add_language_form">
                                                                            @csrf
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <div class="mb-1">
                                                                                                <label class="form-label" for="language_id">Language</label>
                                                                                                <select name="language_id" id="language_id" class="select2 form-select" data-placeholder="Select Language">
                                                                                                    <option value=""></option>
                                                                                                    @foreach ($languages as $language)
                                                                                                    <option value="{{ $language->id }}">{{ $language->name }}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <div class="mb-1">
                                                                                                <label class="form-label" for="readinge">Reading</label>
                                                                                                <select name="reading" id="reading" class="select2 form-select" data-placeholder="Select Proficiency">
                                                                                                    <option value=""></option>
                                                                                                    <option value="Elementary Proficiency">Elementary Proficiency</option>
                                                                                                    <option value="Limited Working Proficiency">Limited Working Proficiency</option>
                                                                                                    <option value="Professional Working Proficiency">Professional Working Proficiency</option>
                                                                                                    <option value="Full Professional Proficiency">Full Professional Proficiency</option>
                                                                                                    <option value="Native or Bilingual Proficiency">Native or Bilingual Proficiency</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="speaking">Speaking</label>
                                                                                            <select name="speaking" id="speaking" class="select2 form-select" data-placeholder="Select Proficiency">
                                                                                                <option value=""></option>
                                                                                                <option value="Elementary Proficiency">Elementary Proficiency</option>
                                                                                                <option value="Limited Working Proficiency">Limited Working Proficiency</option>
                                                                                                <option value="Professional Working Proficiency">Professional Working Proficiency</option>
                                                                                                <option value="Full Professional Proficiency">Full Professional Proficiency</option>
                                                                                                <option value="Native or Bilingual Proficiency">Native or Bilingual Proficiency</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="writing">Writing</label>
                                                                                            <select name="writing" id="writing" class="select2 form-select" data-placeholder="Select Proficiency">
                                                                                                <option value=""></option>
                                                                                                <option value="Elementary Proficiency">Elementary Proficiency</option>
                                                                                                <option value="Limited Working Proficiency">Limited Working Proficiency</option>
                                                                                                <option value="Professional Working Proficiency">Professional Working Proficiency</option>
                                                                                                <option value="Full Professional Proficiency">Full Professional Proficiency</option>
                                                                                                <option value="Native or Bilingual Proficiency">Native or Bilingual Proficiency</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="understanding">Understanding</label>
                                                                                            <select name="understanding" id="understanding" class="select2 form-select" data-placeholder="Select Proficiency">
                                                                                                <option value=""></option>
                                                                                                <option value="Elementary Proficiency">Elementary Proficiency</option>
                                                                                                <option value="Limited Working Proficiency">Limited Working Proficiency</option>
                                                                                                <option value="Professional Working Proficiency">Professional Working Proficiency</option>
                                                                                                <option value="Full Professional Proficiency">Full Professional Proficiency</option>
                                                                                                <option value="Native or Bilingual Proficiency">Native or Bilingual Proficiency</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- language Edit Modal --}}
                                                            <div class="modal fade text-start" id="edit_language_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel17">Edit Language</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <form class="form" id="edit_language_form">
                                                                            @csrf
                                                                            @method('PUT')
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <input type="hidden" id="title" class="form-control" name="employee" value={{ $employee->id }} />
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="edit_language">Language</label>
                                                                                            <select name="language_id" id="edit_language" class="select2 form-select">
                                                                                                <option value="">Select Language</option>
                                                                                                @foreach ($languages as $language)
                                                                                                <option value="{{ $language->id }}">{{ $language->name }}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <div class="mb-1">
                                                                                                <label class="form-label" for="edit_reading">Reading</label>
                                                                                                <select name="reading" id="edit_reading" class="select2 form-select">
                                                                                                    <option value="">Select Proficiency</option>
                                                                                                    <option value="Elementary Proficiency">Elementary Proficiency</option>
                                                                                                    <option value="Limited Working Proficiency">Limited Working Proficiency</option>
                                                                                                    <option value="Professional Working Proficiency">Professional Working Proficiency</option>
                                                                                                    <option value="Full Professional Proficiency">Full Professional Proficiency</option>
                                                                                                    <option value="Native or Bilingual Proficiency">Native or Bilingual Proficiency</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="edit_speaking">Speaking</label>
                                                                                            <select name="speaking" id="edit_speaking" class="select2 form-select">
                                                                                                <option value="">Select Proficiency</option>
                                                                                                <option value="Elementary Proficiency">Elementary Proficiency</option>
                                                                                                <option value="Limited Working Proficiency">Limited Working Proficiency</option>
                                                                                                <option value="Professional Working Proficiency">Professional Working Proficiency</option>
                                                                                                <option value="Full Professional Proficiency">Full Professional Proficiency</option>
                                                                                                <option value="Native or Bilingual Proficiency">Native or Bilingual Proficiency</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="edit_writing">Writing</label>
                                                                                            <select name="writing" id="edit_writing" class="select2 form-select">
                                                                                                <option value="">Select Proficiency</option>
                                                                                                <option value="Elementary Proficiency">Elementary Proficiency</option>
                                                                                                <option value="Limited Working Proficiency">Limited Working Proficiency</option>
                                                                                                <option value="Professional Working Proficiency">Professional Working Proficiency</option>
                                                                                                <option value="Full Professional Proficiency">Full Professional Proficiency</option>
                                                                                                <option value="Native or Bilingual Proficiency">Native or Bilingual Proficiency</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label" for="edit_understanding">Understanding</label>
                                                                                            <select name="understanding" id="edit_understanding" class="select2 form-select">
                                                                                                <option value="">Select Proficiency</option>
                                                                                                <option value="Elementary Proficiency">Elementary Proficiency</option>
                                                                                                <option value="Limited Working Proficiency">Limited Working Proficiency</option>
                                                                                                <option value="Professional Working Proficiency">Professional Working Proficiency</option>
                                                                                                <option value="Full Professional Proficiency">Full Professional Proficiency</option>
                                                                                                <option value="Native or Bilingual Proficiency">Native or Bilingual Proficiency</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                                                <button type="submit" class="btn btn-primary" id="save">Update</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>

                                {{-- Salary History --}}
                                <div class="tab-pane fade" id="account-vertical-salary_history" role="tabpanel" aria-labelledby="account-pill-salary_history" aria-expanded="false">
                                    <div class="content-body">
                                        <div class="card mb-1">
                                            <!--Search Form -->
                                            <div class="card-body">
                                                <form id="search_form">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <label class="form-label">Filter By Payroll:</label>
                                                            <select name="payrollFilter" id="payrollFilter" class="select2 form-select" data-placeholder="Select Payroll" required>
                                                                <option value=""></option>
                                                                @foreach ($payrolls as $payroll)
                                                                <option value="{{$payroll->id}}">{{$payroll->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <label class="form-label" for="fromDate">From Date:</label>
                                                            <input type="text" name="fromDate" id="fromDate" class="form-control flatpickr-basic" value="{{request('fromDate')}}" placeholder="YYYY-MM-DD" required>
                                                        </div>
                                                        <div class="col-12 col-md-4">
                                                            <label class="form-label" for="toDate">To Date:</label>
                                                            <input type="text" name="toDate" id="toDate" class="form-control flatpickr-basic" value="{{request('toDate')}}" placeholder="YYYY-MM-DD" required>
                                                        </div>
                                                        <div class="col-md-1" style="margin-top: 23px;">
                                                            <button type="button" class="btn btn-primary" id="salary_history_submit">Apply</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        {{-- <div class="mb-1 text-center">
                                            <div class="dt-action-buttons text-end">
                                                <div class="dt-buttons d-inline-flex">
                                                    <button type="button" class="btn btn-success export" id="excel">Download Excel</button>
                                                    <button style="margin-left: 6px;" type="button" class="btn btn-danger export" id="pdf">Download Pdf</button>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <div class="card pb-2">
                                            <div class="table-responsive" id="salary_history">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Training -->
                                <div class="tab-pane fade" id="account-vertical-training" role="tabpanel" aria-labelledby="account-pill-training" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="training_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Training Session</th>
                                                                    <th>Proof</th>
                                                                    <th>Status</th>
                                                                    <th class="not_include">Feedback</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                <!-- Travel -->
                                <div class="tab-pane fade" id="account-vertical-travel" role="tabpanel" aria-labelledby="account-pill-travel" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="travel_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_include"></th>
                                                                    <th>Sr.No</th>
                                                                    <th>Purpose</th>
                                                                    <th>From</th>
                                                                    <th>To</th>
                                                                    <th>Travel Date</th>
                                                                    <th>Return Date</th>
                                                                    <th>Funding</th>
                                                                    <th>Currency</th>
                                                                    <th>Details</th>
                                                                    <th class="not_include">Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                <!-- Disciplinaries                                 -->
                                <div class="tab-pane fade" id="account-vertical-disciplinaries" role="tabpanel" aria-labelledby="account-pill-disciplinaries" aria-expanded="false">
                                    <div class="content-body">
                                        <section id="basic-dataTable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <table class="table" id="disciplinaries_dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="not_inculde"></th>
                                                                    <th>Employee Code</th>
                                                                    <th>Employee Designation</th>
                                                                    <th>Employee Department</th>                                                                    
                                                                    <th>Violated Rule</th>
                                                                    <th>Warning Type</th>                                                                   
                                                                    <th>Description</th>
                                                                    <th>Attchment Image</th>
                                                                    <th>Issued By</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                <!-- End Disciplinaries -->
                                <!-- ImagepopUp Modal -->

                                <div class="modal fade" id="ModalImage" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="ModalLabel">Record Image</h5>
                                            </div>
                                            <div class="modal-body" id="imagemodal">

                                            </div>
                                            <div class="modal-body" id="downloadimage">

                                             </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- ImagepopUp Modal End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection
@section('scripts')
<script>
    var employee_id = <?php echo json_encode($employee->id); ?>;
    var leave = <?php echo json_encode($leaves); ?>;
    var encashment = <?php echo json_encode($encashment); ?>;
    var balance = <?php echo json_encode($balance); ?>;
    var company = "{{env('COMPANY')}}";
    var employeement_status = "{{$employeement_status}}";
    var rowid;

    // Add Employee Image
    $("#image_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'employee_image');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#image_form')[0].reset();
                    $(".select2").val('').trigger('change')
                    $("#employee_image_modal").modal("hide");
                    Toast.fire({
                        icon: 'success',
                        title: 'Employee Image has been Updated Successfully!'
                    })
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000);
                }

            }
        });
    });
    // Add Skill Form
    $("#add_skill_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'skill');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_skill_form')[0].reset();
                    $(".select2").val('').trigger('change')
                    $("#add_skill_modal").modal("hide");
                    skills_dataTable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Skills has been Added Successfully!'
                    })
                }

            }
        });
    });
    
    $("#edit_balance_leave_type_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            url: "{{url('leave-type-balance-update')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                if (response.code == 200) {
                    Toast.fire({
                        icon: 'success',
                        title: 'Leave Type Balance has been Updated Successfully!'
                    })
                    $("#edit_balance_modal").modal("hide");
                    setTimeout(function() {
                        window.location.reload();
                    }, 1300);
                }
                else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: response.error_message
                    });
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    });
                }

            }
        });
    });
    // Edit Skill
    function skill_edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('staff_directory')}}" + "/" + id + "/edit",
            type: "GET",
            data: {
                type: 'skill'
            },
            success: function(response) {
                $('#skill_details').text(response.details);
                $("#skill_edit_modal").modal("show");
            }
        });
    }
    // Update Skill
    $("#edit_skill_form").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'skill');
        $.ajax({
            url: "{{ url('staff_directory') }}" + "/" + rowid,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $("#skill_edit_modal").modal("hide");
                    $(".select2").val('').trigger('change')
                    $('#skills_dataTable').DataTable().ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Skills has been Updated Successfully!'
                    })
                }

            }
        });
    });
    // Skill Delete
    function skill_delete(id) {
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: 'Are you sure want to delete!',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function() {
                        $.ajax({
                            url: "{{url('staff_directory')}}" + "/" + id,
                            type: "DELETE",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: 'skill'
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else {
                                    skills_dataTable.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Skill has been Deleted Successfully!'
                                    })
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }
    // Add Education Form
    $("#add_education_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'education');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_education_form')[0].reset();
                    $(".select2").val('').trigger('change')
                    $("#add_education_modal").modal("hide");
                    education_dataTable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Education has been Added Successfully!'
                    })
                }

            }
        });
    });
    // Edit Education
    function education_edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('staff_directory')}}" + "/" + id + "/edit",
            type: "GET",
            data: {
                type: 'education'
            },
            success: function(response) {
                $('#qualification').val(response.education_id).select2();
                $('#institute').val(response.institute);
                $('#date_start').val(response.date_start);
                $('#date_end').val(response.date_end);
                $('#grade').val(response.grade);
                $('#marks').val(response.marks);
                $("#edit_education_modal").modal("show");
            }
        });
    }
    // Update Education
    $("#edit_education_form").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'education');
        $.ajax({
            url: "{{ url('staff_directory') }}" + "/" + rowid,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $("#edit_education_modal").modal("hide");
                    $(".select2").val('').trigger('change')
                    $('#education_dataTable').DataTable().ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Education has been Updated Successfully!'
                    })
                }

            }
        });
    });
    // Education Delete
    function education_delete(id) {
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: 'Are you sure want to delete!',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function() {
                        $.ajax({
                            url: "{{url('staff_directory')}}" + "/" + id,
                            type: "DELETE",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: 'education'
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else {
                                    education_dataTable.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Education has been Deleted Successfully!'
                                    })
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }
    // Add Language
    $("#add_language_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'language');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_language_form')[0].reset();
                    $(".select2").val('').trigger('change')
                    $("#add_language_modal").modal("hide");
                    languages_dataTable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Language has been Added Successfully!'
                    })
                }

            }
        });
    });
    // Edit Language
    function language_edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('staff_directory')}}" + "/" + id + "/edit",
            type: "GET",
            data: {
                type: 'language'
            },
            success: function(response) {
                $('#edit_language').val(response.language_id).select2();
                $('#edit_reading').val(response.reading).select2();
                $('#edit_speaking').val(response.speaking).select2();
                $('#edit_writing').val(response.writing).select2();
                $('#edit_understanding').val(response.understanding).select2();
                $("#edit_language_modal").modal("show");
            }
        });
    }
    // Update Language
    $("#edit_language_form").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'language');
        $.ajax({
            url: "{{ url('staff_directory') }}" + "/" + rowid,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $("#edit_language_modal").modal("hide");
                    $(".select2").val('').trigger('change')
                    $('#languages_dataTable').DataTable().ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Language has been Updated Successfully!'
                    })
                }

            }
        });
    });
    // Language Delete
    function language_delete(id) {
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: 'Are you sure want to delete!',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function() {
                        $.ajax({
                            url: "{{url('staff_directory')}}" + "/" + id,
                            type: "DELETE",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: 'language'
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else {
                                    languages_dataTable.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Language has been Deleted Successfully!'
                                    })
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }
    // Add Emergency Contacts
    $("#add_emergency_contacts_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'emergency_contacts');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_emergency_contacts_form')[0].reset();
                    $(".select2").val('').trigger('change')
                    $("#add_emergency_contacts_modal").modal("hide");
                    emergency_contacts_dataTable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Emergency Contacts has been Added Successfully!'
                    })
                }

            }
        });
    });
    // Edit Emergency Contacts
    function emergency_contacts_edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('staff_directory')}}" + "/" + id + "/edit",
            type: "GET",
            data: {
                type: 'emergency_contacts'
            },
            success: function(response) {
                $('#contact_name').val(response.name);
                $('#contact_relation').val(response.relationship);
                $('#contact_home_phone').val(response.home_phone);
                $('#contact_work_phone').val(response.work_phone);
                $('#contact_mobile_phone').val(response.mobile_phone);
                $("#edit_emergency_contacts_modal").modal("show");
            }
        });
    }
    // Update Emergency Contacts
    $("#edit_emergency_contacts_form").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'emergency_contacts');
        $.ajax({
            url: "{{ url('staff_directory') }}" + "/" + rowid,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $("#edit_emergency_contacts_modal").modal("hide");
                    $('#emergency_contacts_dataTable').DataTable().ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Emergency Contacts has been Updated Successfully!'
                    })
                }

            }
        });
    });
    // Emergency Contacts Delete
    function emergency_contacts_delete(id) {
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: 'Are you sure want to delete!',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function() {
                        $.ajax({
                            url: "{{url('staff_directory')}}" + "/" + id,
                            type: "DELETE",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: 'emergency_contacts'
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else {
                                    emergency_contacts_dataTable.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Emergency Contact has been Deleted Successfully!'
                                    })
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }
    // Add Depandents
    $("#add_depandents_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'employee_depandents');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_depandents_form')[0].reset();
                    $(".select2").val('').trigger('change')
                    $("#add_depandents_modal").modal("hide");
                    depandents_dataTable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Emergency Contacts has been Added Successfully!'
                    })
                }

            }
        });
    });
    // Edit Depandents
    function depandents_edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('staff_directory')}}" + "/" + id + "/edit",
            type: "GET",
            data: {
                type: 'employee_depandents'
            },
            success: function(response) {
                $('#depandent_name').val(response.name);
                $('#depandent_relation').val(response.relationship).select2();
                $('#depandent_dob').val(response.dob);
                $('#depandent_id').val(response.id_number);
                $("#edit_depandents_modal").modal("show");
            }
        });
    }
    // Update Dependants
    $("#edit_depandents_form").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'employee_depandents');
        $.ajax({
            url: "{{ url('staff_directory') }}" + "/" + rowid,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $("#edit_depandents_modal").modal("hide");
                    $(".select2").val('').trigger('change')
                    $('#depandents_dataTable').DataTable().ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Employee Depandents has been Updated Successfully!'
                    })
                }

            }
        });
    });
    // Depandents Delete
    function employee_depandents_delete(id) {
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: 'Are you sure want to delete!',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function() {
                        $.ajax({
                            url: "{{url('staff_directory')}}" + "/" + id,
                            type: "DELETE",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: 'employee_depandents'
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else {
                                    depandents_dataTable.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Emergency Contact has been Deleted Successfully!'
                                    })
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }
    // Add Employee Documents
    $("#add_document_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'employee_documents');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_document_form')[0].reset();
                    $("#add_document_modal").modal("hide");
                    documents_dataTable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Employee Documents has been Added Successfully!'
                    })
                }

            }
        });
    });
    // Edit Employee Documents
    function documents_edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('staff_directory')}}" + "/" + id + "/edit",
            type: "GET",
            data: {
                type: 'employee_documents'
            },
            success: function(response) {
                $('#edit_document').val(response.document).select2();
                $('#edit_date_added').val(response.date_added);
                $('#edit_valid_until').val(response.valid_until);
                $('#edit_status').val(response.status).select2();
                $('#edit_details').text(response.details);
                $("#edit_document_modal").modal("show");
            }
        });
    }
    // Update Employee Documents
    $("#edit_document_form").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'employee_documents');
        $.ajax({
            url: "{{ url('staff_directory') }}" + "/" + rowid,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $("#edit_document_modal").modal("hide");
                    $('#documents_dataTable').DataTable().ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Employee Documents has been Updated Successfully!'
                    })
                }

            }
        });
    });
    // Delete Employee Documents
    function document_delete(id) {
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: 'Are you sure want to delete!',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function() {
                        $.ajax({
                            url: "{{url('staff_directory')}}" + "/" + id,
                            type: "DELETE",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: 'employee_documents'
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else {
                                    documents_dataTable.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Employee Documents Contact has been Deleted Successfully!'
                                    })
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }
    // Download Employee Attachment
    function attachment_download(id) {
        $(document).on('click', '.download', function() {
            $.ajax({
                url: "{{url('attachment_download')}}" + "/" + id,
                type: "GET",
                // data : {
                //     _token: "{{ csrf_token() }}",
                //     type: 'employee_documents'
                // },
                success: function(response) {
                    if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        Toast.fire({
                            icon: 'success',
                            title: 'Employee Attachment Downloaded Successfully!'
                        })
                        window.open(this.url);
                        documents_dataTable.ajax.reload();

                    }
                }
            });
        })
    }
    // Add Encashment Data
    $("#add_encashment_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'employee_encashment');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_encashment_form')[0].reset();
                    $("#add_encashment_modal").modal("hide");
                    Toast.fire({
                        icon: 'success',
                        title: 'Employee Leave Encashment has been Added Successfully!'
                    });
                    setTimeout(function() {
                        window.location.reload();
                    }, 1500);
                }

            }
        });
    });
    // Adding Employee Leave Balance Data
    $("#add_leave_balance_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'employee_leave_balance');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_leave_balance_form')[0].reset();
                    $("#add_leave_balance_modal").modal("hide");
                    Toast.fire({
                        icon: 'success',
                        title: 'Employee Leave Balance has been Added Successfully!'
                    });
                    setTimeout(function() {
                        window.location.reload();
                    }, 1500);
                }

            }
        });
    });
    // Add Leave Form
    $("#add_leave_form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('type', 'employee_leave');
        $.ajax({
            url: "{{route('staff_directory.store')}}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                // console.log(response);
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_leave_form')[0].reset();
                    $("#add_leave_modal").modal("hide");
                    leaves_dataTable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Employee Leave has been Added Successfully!'
                    })
                }

            }
        });
    });

    // OnChange Salary History
    // $(document).on('click', '.export', function(){
    //         blockUI();
    //         var export_type = $(this).attr('id');
    //         var payroll_id = $('#payrollFilter').val();
    //         var fromDate = $('#fromDate').val();
    //         var toDate = $('#toDate').val();
    //         $.ajax({
    //             url: "{{route('staff_directory.create')}}",
    //             type: "GET",
    //             data: {
    //                 // 'calculation_group':calculation_group,
    //                 'export_type':export_type,
    //                 'fromdate':fromDate,
    //                 'todate':fromDate,
    //                 'payroll_id':payroll_id,
    //                 'employee':employee_id
    //             },
    //             success: function(response,status,xhr) {
    //                 $.unblockUI();
    //                 // if(export_type == 'excel'){
    //                     window.open(this.url);
    //                 // }
    //             },
    //             error: function() {
    //                 $.unblockUI();
    //                 alert('Error occured');
    //             }
    //         });
    //     });

    $('#salary_history_submit').on('click', function(e) {
        e.preventDefault();
        blockUI();
        var payroll_id = $('#payrollFilter').val();
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        $.ajax({
            url: "{{route('staff_directory.create')}}",
            type: "GET",
            data: {
                fromdate:fromDate,
                todate:toDate,
                payroll_id: payroll_id,
                employee: employee_id
            },
            success: function(response) {
                $.unblockUI();
                $("#salary_history").empty();
                var html = '';
                if(response.data != ''){
                    html += `<table class="table table-bordered" id="salary_history_table">
                            <thead>
                                <tr><th>Payroll</th>`;
                                    $.each(response.columns, function(index, value) {
                                        html += `<th>${value.name}</th>`;
                                    });
                            html += `</tr>
                            </thead>
                            <tbody>`;
                                $.each(response.data, function(payroll, columns) {
                                    html += `<tr><td>${payroll}</td>`;
                                    $.each(columns, function(index, value) {
                                        html += `<td>${value}</td>`;
                                    });
                                    html += `</tr>`;
                                })
                    html += `<tbody></table>`;
                }
                else{
                    html += `<div class="text-center"><h3>No Record Found!</h3></div>`
                }
                $("#salary_history").append(html);
                // $('#salary_history_table').DataTable();
                var from_date = '{!! request()->query('fromDate') !!}';
            var to_date = '{!! request()->query('toDate') !!}';
            $('#salary_history_table').DataTable({
                ordering: true,
                "columnDefs": [
					{
						// For Responsive
						className: 'control',
						orderable: false,
						targets: 0
					},
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',


                            customize: function (doc) {
                                  //Remove the title created by datatTables
                                  doc.content.splice(0,1);
                                  var now = new Date();
                                  var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                                  doc.pageMargins = [5,60,5,5];
                                  doc.defaultStyle.margin = 0;
                                  doc.defaultStyle.fontSize = 8;
                                  doc.styles.tableHeader.fontSize = 8;
 
                                  doc['header']=(function() {
                                      return {
                                          columns: [
 
                                              {
                                                  alignment: 'left',
                                                  italics: true,
                                                  text: 'Employee Salary History Table ' + now,
                                                  fontSize: 10,
                                                  margin: [10,0]
                                              },
                                              {
                                                  alignment: 'center',
                                                  fontSize: 14,
                                                  text: 'Employee Salary History Report'
                                              }
                                          ],
                                          margin: 20
                                      }
                                  });
 
                                  doc['footer']=(function(page, pages) {
                                      return {
                                          columns: [
                                              {
                                                  alignment: 'left',
                                                  text: ['Created on: ', { text: jsDate.toString() }]
                                              },
                                              {
                                                  alignment: 'right',
                                                  text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                                              }
                                          ],
                                          margin: 20
                                      }
                                  });
 
                                  var objLayout = {};
                                  objLayout['hLineWidth'] = function(i) { return .5; };
                                  objLayout['vLineWidth'] = function(i) { return .5; };
                                  objLayout['hLineColor'] = function(i) { return '#aaa'; };
                                  objLayout['vLineColor'] = function(i) { return '#aaa'; };
                                  objLayout['paddingLeft'] = function(i) { return 4; };
                                  objLayout['paddingRight'] = function(i) { return 4; };
                                  doc.content[0].layout = objLayout;
                          },


                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                // responsive: {
				// 	details: {
				// 		display: $.fn.dataTable.Responsive.display.childRowImmediate,
				// 		type: 'column',
				// 	}
				// },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            },
        })
    })

    // On Click Functions
    function skills() {
        skills_dataTable = $('#skills_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'skills'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'details',
                    name: 'details',
                },
                {
                    data: 'file',
                    name: 'file',
                },
                {
                    data: '',
                    searchable: false
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    render: function(data, type, full, meta) {
                        var btn = '';
                        @can('Edit Skills in Profile')
                            btn += '<a href="javascript:;" onclick=skill_edit(' + full.id + ')>' +
                            feather.icons['edit'].toSvg({
                                class: 'font-medium-4'
                            }) +
                            '</a>';
                        @endcan
                        @can('Delete Skills in Profile')
                            btn += '<a href="javascript:;" onclick="skill_delete(' + full.id + ')">' +
                            feather.icons['trash-2'].toSvg({
                                class: 'font-medium-4 text-danger'
                            }) +
                            '</a>';
                        @endcan
                        return btn;
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
                @if($employee-> status == 'Active')
                    @can('Add New Skills in Profile')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add New',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#add_skill_modal'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                @endif
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Skills</h6>');
    }

    function education() {
        education_dataTable = $('#education_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'education'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'education',
                },
                {
                    data: 'institute',
                    name: 'institute',
                },
                {
                    data: 'date_start',
                    name: 'date_start',
                },
                {
                    data: 'date_end',
                    name: 'date_end',
                },
                {
                    data: 'grade',
                    name: 'grade',
                },
                {
                    data: 'marks',
                    name: 'marks',
                },
                {
                    data: '',
                    searchable: false
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    render: function(data, type, full, meta) {
                        var btn = '';
                        @can('Edit Education in Profile')
                            btn += '<a href="javascript:;" class="item-edit" onclick=education_edit(' + full.id + ')>' +
                            feather.icons['edit'].toSvg({
                                class: 'font-medium-4'
                            }) +
                            '</a>';
                        @endcan
                        @can('Delete Education in Profile')
                            btn += '<a href="javascript:;" onclick="education_delete(' + full.id + ')">' +
                            feather.icons['trash-2'].toSvg({
                                class: 'font-medium-4 text-danger'
                            }) +
                            '</a>';
                        @endcan
                        return btn;
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
                @if($employee-> status == 'Active') 
                    @can('Add New Education in Profile')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add New',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#add_education_modal'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                @endif
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Education</h6>');
    }

    function languages() {
        languages_dataTable = $('#languages_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'languages'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'language',
                },
                {
                    data: 'reading',
                    name: 'reading',
                },
                {
                    data: 'speaking',
                    name: 'speaking',
                },
                {
                    data: 'writing',
                    name: 'writing',
                },
                {
                    data: 'understanding',
                    name: 'understanding',
                },
                {
                    data: '',
                    searchable: false
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    render: function(data, type, full, meta) {
                        var btn = '';
                        @can('Edit Language in Profile')
                            btn += '<a href="javascript:;" class="item-edit" onclick=language_edit(' + full.id + ')>' +
                            feather.icons['edit'].toSvg({
                                class: 'font-medium-4'
                            }) +
                            '</a>';
                        @endcan
                        @can('Delete Language in Profile')
                            btn += '<a href="javascript:;" onclick="language_delete(' + full.id + ')">' +
                            feather.icons['trash-2'].toSvg({
                                class: 'font-medium-4 text-danger'
                            }) +
                            '</a>';
                        @endcan
                        return btn;
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
                @if($employee-> status == 'Active') 
                    @can('Delete Language in Profile')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add New',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#add_language_modal'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                @endif
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Languages</h6>');
    }

    function emergencyContacts() {
        emergency_contacts_dataTable = $('#emergency_contacts_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_contacts'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'name',
                    name: 'name',
                },
                {
                    data: 'relationship',
                    name: 'relationship',
                },
                {
                    data: 'work_phone',
                    name: 'work_phone',
                },
                {
                    data: 'home_phone',
                    name: 'home_phone',
                },
                {
                    data: 'mobile_phone',
                    name: 'mobile_phone',
                },
                {
                    data: '',
                    searchable: false
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    render: function(data, type, full, meta) {
                        var btn = '';
                        @can('Edit Emergency contacts in Profile')
                            btn += '<a href="javascript:;" class="item-edit" onclick=emergency_contacts_edit(' + full.id + ')>' +
                            feather.icons['edit'].toSvg({
                                class: 'font-medium-4'
                            }) +
                            '</a>';
                        @endcan
                        @can('Delete Emergency contacts in Profile')
                            btn += '<a href="javascript:;" onclick="emergency_contacts_delete(' + full.id + ')">' +
                            feather.icons['trash-2'].toSvg({
                                class: 'font-medium-4 text-danger'
                            }) +
                            '</a>';
                        @endcan
                        return btn;
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
                    @if($employee-> status == 'Active') 
                        @can('Add New Emergency Contact in Profile')
                            {
                                text: feather.icons['plus'].toSvg({
                                    class: 'me-50 font-small-4'
                                }) + 'Add New',
                                className: 'create-new btn btn-primary',
                                attr: {
                                    'data-bs-toggle': 'modal',
                                    'data-bs-target': '#add_emergency_contacts_modal'
                                },
                                init: function(api, node, config) {
                                    $(node).removeClass('btn-secondary');
                                }
                            }
                        @endcan
                    @endif
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Emergency Contacts</h6>');
    }

    function employeeDepandents() {
        depandents_dataTable = $('#depandents_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_depandents'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'name',
                    name: 'name',
                },
                {
                    data: 'relationship',
                    name: 'relationship',
                },
                {
                    data: 'dob',
                    name: 'dob',
                },
                {
                    data: '',
                    searchable: false
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    render: function(data, type, full, meta) {
                        var btn = '';
                        @can('Dependant Edit in Profile')
                            btn += '<a href="javascript:;" class="item-edit" onclick=depandents_edit(' + full.id + ')>' +
                            feather.icons['edit'].toSvg({
                                class: 'font-medium-4'
                            }) +
                            '</a>';
                        @endcan
                        @can('Dependant Delete in Profile')
                            btn += '<a href="javascript:;" onclick="employee_depandents_delete(' + full.id + ')">' +
                            feather.icons['trash-2'].toSvg({
                                class: 'font-medium-4 text-danger'
                            }) +
                            '</a>';
                        @endcan
                        return btn;
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
                @if($employee-> status == 'Active') 
                    @can('Add New Dependant in Profile')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add New',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#add_depandents_modal'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                @endif
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Depandents</h6>');
    }

    function employeeDocuments() {
        documents_dataTable = $('#documents_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_documents'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'docName',
                },
                {
                    data: 'date_added'
                },
                {
                    data: 'valid_until'
                },
                {
                    data: 'details'
                },
                {
                    data: 'status'
                },
                {
                    data: 'attachment'
                },
                {
                    data: '',
                    searchable: false
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    render: function(data, type, full, meta) {
                        var btn = '';
                        @can('Edit Document in Employee Profile')
                            btn += '<a href="javascript:;" class="item-edit" onclick=documents_edit(' + full.id + ')>' +
                            feather.icons['edit'].toSvg({
                                class: 'font-medium-4'
                            }) +
                            '</a>';
                        @endcan
                        @can('Delete Document in Employee Profile')
                            btn += '<a href="javascript:;" onclick="document_delete(' + full.id + ')">' +
                            feather.icons['trash-2'].toSvg({
                                class: 'font-medium-4 text-danger'
                            }) +
                            '</a>';
                        @endcan
                        @can('Download Document from Employee Profile')
                            btn += '<a href="javascript:;" class="download" onclick="attachment_download(' + full.id + ')">' +
                            feather.icons['download'].toSvg({
                                class: 'font-medium-4 text-danger'
                            }) +
                            '</a>';
                        @endcan
                        return btn;
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
                @can('Add New Document in Employee Profile')
                    @if($employee-> status == 'Active') {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_document_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                    @endif
                @endcan
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Documents</h6>');
    }

    function legals() {
        legals_dataTable = $('#legals_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_legals'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'legal_action'
                },
                {
                    data: 'title'
                },
                {
                    data: 'legal_counsel_company'
                },
                {
                    data: 'full name',
                    render: function(data, type, row) {
                        return row.first_name + " " + row.last_name;
                    }
                },
                {
                    data: 'legal_counsel_employee'
                },
                {
                    data: 'case_filed_on'
                },
                {
                    data: 'court'
                },
                {
                    data: 'case_type'
                },
                {
                    data: 'petition_case_copy'
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Legals</h6>');
    }

    function training() {
        training_dataTable = $('#training_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_training'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'trainingSession',
                    render: function(data, type, row) {
                        return row.tName;
                    }
                },
                {
                    data: 'proof'
                },
                {
                    data: 'status'
                },
                {
                    data: 'feedBack'
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Training</h6>');
    }

    function travels() {
        travel_dataTable = $('#travel_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_travel'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'purpose'
                },
                {
                    data: 'travel_from'
                },
                {
                    data: 'tarvel_to'
                },
                {
                    data: 'travel_date'
                },
                {
                    data: 'return_date'
                },
                {
                    data: 'funding'
                },
                {
                    data: 'money'
                },
                {
                    data: 'details'
                },
                {
                    data: 'status'
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Travel Records</h6>');
    }

    function entitlement() {
        entitlement_dataTable = $('#entitlement_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_entitlement'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'entitle'
                },
                {
                    data: 'department'
                },
                {
                    data: 'date_from'
                },
                {
                    data: 'date_to'
                },
                {
                    data: 'remarks'
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Entitlements</h6>');
    }

    function insurance() {
        insurance_dataTable = $('#insurance_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_insurance'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'insuranceType'
                },
                {
                    data: 'subName'
                },
                {
                    data: 'policy_number'
                },
                {
                    data: 'start_date'
                },
                {
                    data: 'end_date'
                },
                {
                    data: 'amount'
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Entitlements</h6>');
    }

    function inquiry() {
        inquiry_dataTable = $('#inquiry_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_inquiry'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'title'
                },
                {
                    data: 'start_date'
                },
                {
                    data: 'end_date'
                },
                {
                    data: 'inquiry_officer'
                },
                {
                    data: 'designation'
                },
                {
                    data: 'findings'
                },
                {
                    data: 'action_taken'
                },
                {
                    data: 'description'
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Disciplanry Inquiry</h6>');
    }

    function loan() {
        loan_dataTable = $('#loan_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_loan'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'remarks'
                },
                {
                    data: 'amount'
                },
                {
                    data: 'start_date'
                },
                {
                    data: 'monthly_installment'
                },
                {
                    data: 'deducted_loan_amount',
                    render: function(data, type, row) {
                        return row.amount - row.remaining_amount;
                    }
                },
                {
                    data: 'remaining_amount'
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Loan</h6>');
    }

    function advance() {
        advance_dataTable = $('#advance_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_advance'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'remarks'
                },
                {
                    data: 'amount'
                },
                {
                    data: 'start_date'
                },
                {
                    data: 'monthly_installment'
                },
                {
                    data: 'deducted_advance_amount',
                    render: function(data, type, row) {
                        return row.amount - row.remaining_amount;
                    }
                },
                {
                    data: 'remaining_amount'
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Advance</h6>');
    }

    function belongings() {
        belongings_dataTable = $('#belongings_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_belongings'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'asset_name'
                },
                {
                    data: 'department_title'
                },
                {
                    data: 'issue_date'
                },
                {
                    data: 'return_date'
                },
                {
                    data: 'remarks_return'
                },
                {
                    data: 'remarks'
                },
                {
                    "data": "attachs",
                    "render": function (data) {
                        var imageURL = "{{ asset('images/belongings') }}" + '/' + data;
                        return '<img src="' + imageURL + '" class="avatar" alt="Failed Loading Image" width="50" height="50"/>' +
                            '<a class="px-2" href="' + imageURL + '" download><button type="button" class="btn btn-success px-2"><i class="fas fa-download"></i></button></a>';
                    }
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Advance</h6>');
    }

    function leaves() {
        leaves_dataTable = $('#leaves_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'employee_leaves'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'name'
                },
                {
                    data: 'default_per_year',
                    render: function(data, type, row) {
                        if(employeement_status == 14 && company == 'JSML'){
                            var balanceAmount = 0;
                            $.each(balance, function(balance_key, balance_value) {
                                if (row.name == balance_key) {
                                    balanceAmount += parseInt(balance_value); 
                                    return false;
                                }
                            });
                            return balanceAmount;
                        }
                        if(row.new_employees_leaves_settings.length > 0){
                            return row.new_employees_leaves_settings[0].amount;
                        }
                        else{
                            return Math.floor(data);
                        }
                    }
                },
                {
                    data: 'availed',
                    render: function(data, type, row) {
                        let value = 0;
                        $.each(leave, function(leave_key, leave_value) {
                            if (row.name == leave_key) {
                                value += parseFloat(leave_value);
                            }
                        });
                        return value;
                    }
                },
                {
                    data: 'balanced',
                    render: function(data, type, row) {
                        if(row.new_employees_leaves_settings.length > 0){
                            var default_number = row.new_employees_leaves_settings[0].amount;
                        }
                        else{
                            var default_number = row.default_per_year;
                        }
                        if(employeement_status == 14 && company == 'JSML'){
                            var default_number = 0;
                        }
                        let value = Math.floor(default_number);
                        let totalLeaves = 0;
                        let EncashmentOrBalanceAmount = 0;
                        $.each(leave, function(leave_key, leave_value) {
                            if (row.name == leave_key) {
                                totalLeaves += parseFloat(leave_value);
                            }
                            if(row.new_employees_leaves_settings.length == 0){
                                $.each(encashment, function(encashment_key, encashment_value) {
                                    if (row.name == leave_key && row.name == encashment_key) {
                                        value += parseInt(encashment_value);
                                        EncashmentOrBalanceAmount += parseInt(encashment_value);
                                        return false;
                                    }
                                });
                                
                                $.each(balance, function(balance_key, balance_value) {
                                    if (row.name == leave_key && row.name == balance_key) {
                                        value += parseInt(balance_value);
                                        EncashmentOrBalanceAmount += parseInt(balance_value);
                                        return false;
                                    }
                                });
                            }
                        });
                        if(company == 'UNICORN'){
                            return '<span data-toggle="tooltip" data-placement="top" style="cursor:pointer" title="Click To See Balance Logs" onclick="viewLogs('+employee_id+','+row.id+')">' + (value - totalLeaves) + '</span>';
                        }
                        else{
                            return '<span data-toggle="tooltip" data-placement="top" style="cursor:pointer" title="Leave Encashment & Balance Added of Amount: '+EncashmentOrBalanceAmount+'">' + (value - totalLeaves) + '</span>';
                        }
                    }
                },
                {
                    render: function(data, type, row) {
                        return '<span data-toggle="tooltip" data-placement="top" style="cursor:pointer" title="Update Leave Balance" onclick="editBalance('+employee_id+','+row.id+',`'+row.name+'`)">'+
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +'</span>';   
                    }
                }
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
                @if($employee-> status == 'Active')
                @can('Add Leave Encashment in Employee Profile')
                    {
                        text: 'Add Leave Encashment',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_encashment_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    },
                @endcan
                @can('Add Leave in Employee Profile')
                    {
                        text: 'Add Leave',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_leave_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    },
                @endcan
                @can('Add Leave Balance in Employee Profile')
                    {
                        text: 'Add Leave Balance',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_leave_balance_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                @endcan
                @endif
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Employee Leaves</h6>');
    }

    function medical_claims() {
        medical_claims_dataTable = $('#medical_claims_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'medical_claims'
                },
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    "title": "Sr.No",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'description'
                },
                {
                    data: 'attachment'
                }
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">Medical Claims</h6>');
    }

    function disciplinaries() {
        travel_dataTable = $('#disciplinaries_dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            ajax: {
                url: "{{url('staff_directory')}}" + "/" + employee_id,
                data: {
                    type: 'disciplinaries'
                },
            },
            columns: [
                {
                    
                },

                {
                    data: 'employee_code'
                },
                {
                    data: 'designation'

                },
                {
                 data: 'employee_department'
                 },
                {
                    data: 'warning_type'
                },
                {
                    data: 'rule'
                },
                {
                    data: 'message'
                },
                {
                    data: 'attachment_image',
                    render: function(data, type, row, meta) {
                        // console.log(row);
                        return `<img src="{{asset('/')}}${data}" class="rounded" width="50px" height="50px" onclick="ImagepopUp(this.src);">`;
                    }
                },
                {
                    data: 'Issuer_name'
                },
               
                

            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [

            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">Disciplinaries Issued to Employee</h6>');
    }

    function ImagepopUp(src) {
        $('#imagemodal').html(`<img src="${src}" width="250px" height="250px" class="rounded" alt="" srcset="">`).append();
        $('#downloadimage').html(`<a href="${src}" download="${src}" class="btn btn-primary">Download</a>`).append();
        $('#ModalImage').modal('toggle');
    }

    function viewLogs(employee, leaveType) {
        $.ajax({
            url: "{{url('leave-balance-logs')}}",
            type: "GET",
            data: {
                "employee": employee,
                "leaveType": leaveType,
            },
            success: function(response) {
                // Generate table HTML with Bootstrap classes
                let tableHtml = '<div class="table-responsive mb-2"><table class="table table-bordered">';
                
                // Add table headers
                tableHtml += '<thead><tr>';
                tableHtml += '<th>Event</th>';
                tableHtml += '<th>Old Values</th>';
                tableHtml += '<th>New Values</th>';
                tableHtml += '<th>URL</th>';
                tableHtml += '<th>IP Address</th>';
                tableHtml += '<th>User Agent</th>';
                tableHtml += '<th>Created At</th>';
                tableHtml += '<th>Username</th>';
                tableHtml += '<th>Email</th>';
                tableHtml += '<th>User Level</th>';
                tableHtml += '</tr></thead>';

                // Add table body
                tableHtml += '<tbody>';
                response.forEach(record => {
                    let oldValues = JSON.parse(record.old_values || '{}');
                    let newValues = JSON.parse(record.new_values || '{}');

                    let oldAmount = oldValues.amount || '';
                    let oldExpiry = oldValues.expiry || '';

                    let newAmount = newValues.amount || '';
                    let newExpiry = newValues.expiry || '';

                    tableHtml += '<tr>';
                    tableHtml += `<td>${record.event}</td>`;
                    tableHtml += `<td>${(oldAmount || oldExpiry) ? `Amount: ${oldAmount}, Expiry: ${oldExpiry}` : '-'}</td>`;
                    tableHtml += `<td>${(newAmount || newExpiry) ? `Amount: ${newAmount}, Expiry: ${newExpiry}` : '-'}</td>`;
                    tableHtml += `<td>${record.url}</td>`;
                    tableHtml += `<td>${record.ip_address}</td>`;
                    tableHtml += `<td>${record.user_agent}</td>`;
                    tableHtml += `<td>${record.created_at}</td>`;
                    tableHtml += `<td>${record.username}</td>`;
                    tableHtml += `<td>${record.email}</td>`;
                    tableHtml += `<td>${record.user_level}</td>`;
                    tableHtml += '</tr>';
                });
                tableHtml += '</tbody></table></div>';

                // Add a note above the table
                let noteHtml = '<code class="mb-4">These are entries created for the balance against the current employee.</code><hr>';

                // Combine note and table HTML
                let modalBodyHtml = noteHtml + tableHtml;

                // Insert combined HTML into #balance_log_modal_body
                $('#balance_log_modal_body').html(modalBodyHtml);

                $('#balance_log_modal').modal('toggle');
            },
            error: function(error) {
                console.error("Error fetching logs:", error);
            }
        });
    }

    function editBalance(employee, leaveType, leaveTypeName) {
        $.ajax({
            url: "{{url('leave-type-balance-edit')}}",
            type: "GET",
            data: {
                "employee": employee,
                "leaveType": leaveType,
            },
            success: function(response) {
                if(response){
                    $('#edit_balance_leave_type_id').val(leaveType);
                    $('#edit_balance_leave_type_name').val(leaveTypeName);
                    $('#edit_balance_leave_type_employee').val(employee);
                    $('#edit_balance_leave_type_amount').val(response.balance);
                    $('#edit_balance_modal').modal('toggle');
                }
                else{
                    Swal.fire({
                        icon: 'error',
                        title: "No Record Found Against Requested Type",
                    });
                }
            },
            error: function(error) {
                Toast.fire({
                    icon: 'error',
                    title: "Error fetching logs:"+ error
                });
            }
        });
    }
</script>
@endsection