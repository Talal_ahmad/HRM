@extends('Admin.layouts.master')
@section('title', 'Staff Directory')

@section('content')

    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Staff_Directory')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Staff_Directory')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header row">
            <div class="content-body">
                <section class="basic-tabs-components">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form" action="{{url('staff_directory')}}">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="department">@lang('app.Filter_By_Department')</label>
                                                    <select name="department" id="department" class="select2 form-select"
                                                        data-placeholder="Select Department" required>
                                                        <option value=""></option>
                                                        @foreach (departments() as $department)
                                                            <option {{ $department->id == (request('department') ? request('department') : "") ? 'selected' : '' }} value="{{ $department->id }}">
                                                                {{ $department->title }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="employee">@lang('app.Filter_By_Employee')</label>
                                                    <select name="employee" id="employee" class="select2 form-select" data-placeholder="Select Employee">
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 text-end">
                                                <a href="{{url('staff_directory')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                                <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-1">
                        @foreach ($employees as $employee)
                            <div class="col-md-4 col-12 mt-3">
                                {{-- @can('Staff Directory Edit')    --}}
                                <a class="link-dark" href="{{ url('staff_directory/' . $employee->id) }}">
                                    <div class="card card-profile">
                                        <div class="card-body">
                                            <div class="profile-image-wrapper">
                                                <div class="profile-image">
                                                    <div class="avatar">
                                                        @if (file_exists(public_path('images/employees/' . $employee->image)))
                                                            <img src="{{asset('images/employees/' . $employee->image)}}" alt="Profile Picture" />
                                                        @else
                                                            <img src="{{asset('images/employees/default_employee.png')}}" alt="Profile Picture" />
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <h3>{{ $employee->first_name }} {{$employee->middle_name}} {{ $employee->last_name }}</h3>
                                            <span
                                                class="badge badge-light-primary profile-badge">{{ $employee->jobtitle->name }}</span>
                                            <hr class="mb-2" />
                                            <div class="row">
                                                <div class="col">
                                                    <h6 class="text-muted fw-bolder text-start">@lang('app.Department')</h6>
                                                </div>
                                                <div class="col">
                                                    <p class="badge bg-light-info mb-1 fw-bold text-end">{{ $employee->companystructure->title }}</p>
                                                </div>
                                            </div> 
                                            @if (env('COMPANY') == 'Ajmal Dawakhana')
                                                <div class="row">
                                                    <div class="col">
                                                        <h6 class="text-muted fw-bolder text-start">Employee ID</h6>
                                                    </div>
                                                    <div class="col">
                                                        <p class="badge bg-light-info mb-1 fw-bold text-end">{{ $employee->employee_id }}</p>
                                                    </div>
                                                </div> 
                                            @endif
                                            <div class="row">
                                                <div class="col">
                                                    <h6 class="text-muted fw-bolder text-start">@lang('app.EMail')</h6>
                                                </div>
                                                <div class="col">
                                                    <p class="badge bg-light-info mb-1 fw-bold text-end">{{ HandleEmpty($employee->work_email) }}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <h6 class="text-muted fw-bolder text-start">@lang('app.Phone')</h6>
                                                </div>
                                                <div class="col">
                                                    <p class="badge bg-light-info mb-1 fw-bold text-end">{{ HandleEmpty($employee->mobile_phone) }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                {{-- @endcan --}}
                                {{-- @can('Staff Directory Not Edit')   
                                <div class="card card-profile">
                                    <div class="card-body">
                                        <div class="profile-image-wrapper">
                                            <div class="profile-image">
                                                <div class="avatar">
                                                    @if (file_exists(public_path('images/employees/' . $employee->image)))
                                                        <img src="{{asset('images/employees/' . $employee->image)}}" alt="Profile Picture" />
                                                    @else
                                                        <img src="{{asset('images/employees/default_employee.png')}}" alt="Profile Picture" />
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <h3>{{ $employee->first_name }} {{$employee->middle_name}} {{ $employee->last_name }}</h3>
                                        <span
                                            class="badge badge-light-primary profile-badge">{{ $employee->jobtitle->name }}</span>
                                        <hr class="mb-2" />
                                        <div class="row">
                                            <div class="col">
                                                <h6 class="text-muted fw-bolder text-start">Department</h6>
                                            </div>
                                            <div class="col">
                                                <p class="badge bg-light-info mb-1 fw-bold text-end">{{ $employee->companystructure->title }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <h6 class="text-muted fw-bolder text-start">E-Mail</h6>
                                            </div>
                                            <div class="col">
                                                <p class="badge bg-light-info mb-1 fw-bold text-end">{{ HandleEmpty($employee->work_email) }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <h6 class="text-muted fw-bolder text-start">Phone</h6>
                                            </div>
                                            <div class="col">
                                                <p class="badge bg-light-info mb-1 fw-bold text-end">{{ HandleEmpty($employee->mobile_phone) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endcan --}}
                            </div>
                        @endforeach
                    </div>
                    <div class="float-end">{{ $employees->appends(request()->query())->links('pagination::bootstrap-4') }}</div>
                </section>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employee').empty();
                            $('#employee').html('<option value="">Select Employee</option>'); 
                            $.each(response, function(index, value) {
                                $('#employee').append(
                                    $('<option></option>').val(value.id).html(
                                        value.employee_id + '-' +value.first_name +' '+value.middle_name+' '+ value.last_name + '-' + value.designation)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employee').empty();
                }
            });
            if ($('#department').val().length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: $('#department').val()
                    },
                    success: function(response) {
                        $('#employee').empty();
                        var value = {!! json_encode(request('department')) !!};
                        value = value ? value : "";
                        $.each(response, function(index, value) {
                            var chk = (value == value.id ? "selected" : "");
                            $('#employee').append(
                                $('<option ' + chk + '></option>').val(value.id).html(value.first_name +' '+ value.last_name)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            }
        });
    </script>
@endsection
