@extends('Admin.layouts.master')

@section('title', 'Data Import')
@section('content')

<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Data Import</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Finance</a>
                            </li>
                            <li class="breadcrumb-item active">Data Import
                            </li>
                        </ol>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <div class="content-body">
        <section class="basic-tabs-components">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="data-importers-tab" data-bs-toggle="tab" href="#data-importers" aria-controls="data-importers" role="tab" aria-selected="true">Data Importers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="data-import-files-tab" data-bs-toggle="tab" href="#data-import-files" aria-controls="data-import-files" role="tab" aria-selected="false" onclick="data_import_files()">Data Import Files</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="data-importers" aria-labelledby="data-importers-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="pb-2">
                                        <table class="table" id="data_importers">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Data Type</th>
                                                    <th>Details</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                {{-- Data Import Files --}}
                <div class="tab-pane" id="data-import-files" aria-labelledby="data-import-files-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="data_import_files">
                                        <thead>
                                            <tr>
                                               <th></th>
                                               <th>Name</th>
                                               <th>Data Import Definition</th>
                                               <th>Status</th>
                                               <th class="not_include">Actions</th>
                                           </tr>
                                       </thead>
                                       <tbody>

                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </div>
                   </section>
               </div>
               <!--Data Importers Add Modal -->
               <div class="modal fade text-start" id="data_importers_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add Data Importer</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="data_importers_add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="data_importers_name">Name</label>
                                            <input type="text" id="data_importers_name" class="form-control" name="name" placeholder="Name" required/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="data_type">Data Type</label>
                                            <input type="text" id="data_type" class="form-control" name="dataType" placeholder="Data Type" required/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="details">Details</label>
                                            <textarea name="details" id="details" cols="20" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="columns">Columns</label>
                                            <button type="button" class="btn btn-primary" onclick="addColumns()">Add</button>
                                            <button type="button" class="btn btn-outline-success" onclick="columnsReset()">Reset</button>
                                            <div class="col-md-12 col-12" id="add_column_div">
                                              <div class="mb-1">
                                              </div>  
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Data Importers Add Modal -->

        <!--Data Importers Edit Modal -->
        <div class="modal fade text-start" id="data_importers_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Data Importer</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="data_importers_edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_data_importers_name">Name</label>
                                        <input type="text" id="edit_data_importers_name" class="form-control" name="name" placeholder="Name" required/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_data_type">Data Type</label>
                                        <input type="text" id="edit_data_type" class="form-control" name="dataType" placeholder="Data Type" required/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_details">Details</label>
                                        <textarea name="details" id="edit_details" cols="20" rows="2" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Data Importers Edit Modal -->

        <!--Data Import Files Add Modal -->
        <div class="modal fade text-start" id="data_import_files_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Data Import Files</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="data_import_files_add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="data_import_files_name">Name</label>
                                        <input type="text" id="data_import_files_name" class="form-control" name="name" placeholder="Name" required/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="data_import_definition">Data Import Definitions</label>
                                        <select name="data_import_definition" id="data_import_definition" class="select2 form-select" data-placeholder="Select Data Import Definition" required>
                                            <option value=""></option>
                                            @foreach ($data_importer as $type)
                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="file">File To Import</label>
                                        <label for="file"
                                        class="btn btn-sm btn-primary mb-75 mr-75 waves-effect waves-float waves-light">Upload</label>
                                        <input type="file" id="file" name="file" hidden
                                        accept="image/*" />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="data_import_files_details">Last Export Result</label>
                                        <textarea name="details" id="data_import_files_details" cols="20" rows="2" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Data Import Files Add Modal -->

        <!--Data Import Files Modal -->
        <div class="modal fade text-start" id="data_import_files_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Data Import Files</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="data_import_files_edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_data_import_files_name">Name</label>
                                        <input type="text" id="edit_data_import_files_name" class="form-control" name="name" placeholder="Name" required/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_data_import_definition">Data Import Definitions</label>
                                        <select name="data_import_definition" id="edit_data_import_definition" class="select2 form-select" data-placeholder="Select Data Import Definition" required>
                                            <option value=""></option>
                                            @foreach ($data_importer as $type)
                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="edit_file">File To Import</label>
                                        <label for="edit_file"
                                        class="btn btn-sm btn-primary mb-75 mr-75 waves-effect waves-float waves-light">Upload</label>
                                        <input type="file" id="edit_file" name="file" hidden
                                        accept="image/*" />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_data_import_files_details">Last Export Result</label>
                                        <textarea name="details" id="edit_data_import_files_details" cols="20" rows="2" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Data Import Files Edit Modal -->
    </div>
</section>
</div>
</section>
@endsection
@section('scripts')
<script>
    var rowid;
    $(document).ready(function() {

        $('#data_importers').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            destroy: true,
            ajax: {
                url: "{{ route('dataImport.index') }}",
                data: {
                    type: "data_importers"
                }
            },
            columns: [
            { 
                data: 'responsive_id'
            },
            {
                data : 'name',
                name : 'name',
            },
            {
                data: 'dataType',
                name : 'dataType',
            },
            {
                data: 'details',
                name : 'details',
            },
            {
                data: '',
            },
            ],
            "columnDefs": [
            {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=data_importers_edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="data_importers_type('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                                );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                    ],
                    dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    displayLength: 10,
                    aLengthMenu: [
                        [10,25,50,100,-1],
                        [10,25,50,100,"All"]
                    ],
                    buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                        {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Data Importers',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Data Importers',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Data Importers',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Data Importers',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Data Importers',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#data_importers_add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                    ],
                    responsive: {
                       details: {
                          display: $.fn.dataTable.Responsive.display.childRowImmediate,
                          type: 'column',
                      }
                  },
                  language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
            // Filter form control to default size for all tables
            $('.dataTables_filter .form-control').removeClass('form-control-sm');
            $('.dataTables_length .form-select').removeClass('form-select-sm').removeClass('form-control-sm');

            $('#data-importers div.head-label').html('<h6 class="mb-0">List of Data Importers</h6>');
            
            // Store data_importers
            $("#data_importers_add_form").on("submit", function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'data_importers');
                $.ajax({
                    url: "{{route('dataImport.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#data_importers_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            document.getElementById("data_importers_add_form").reset();
                            $('#data_importers').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Data Importer has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // data_importers Update
            $("#data_importers_edit_form").on("submit", function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'data_importers');
                $.ajax({
                    url: "{{url('dataImport')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#data_importers_edit_modal").modal("hide");
                            document.getElementById("data_importers_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#data_importers').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Data Importer has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Store Data Import Files
            $("#data_import_files_add_form").on("submit", function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{route('dataImport.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#data_import_files_add_modal").modal("hide");
                            document.getElementById("data_import_files_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#data_import_files').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Data Import File has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // data_import_files Update
            $("#data_import_files_edit_form").on("submit", function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{url('dataImport')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#data_import_files_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            document.getElementById("data_import_files_edit_form").reset();
                            $('#data_import_files').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Data Import File has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });
        });

        // data_import_files DataTable
        function data_import_files()
        {
            $('#data_import_files').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('dataImport.index') }}",
                    data: {
                        type: "data_import_files"
                    }
                },
                columns: [
                {
                    data: 'responsive_id'
                },
                {
                    data : 'name',
                    name : 'dataimportfiles.name',
                },
                {
                    data: 'dataImportDefinition',
                    name : 'dataimport.name',
                },
                {
                    data: 'status',
                    name : 'dataimportfiles.status',
                },
                {
                    data: '',
                },
                ],
                "columnDefs": [
                {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=data_import_files_edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="data_import_files_delete('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                                );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                    ],
                    order: [
                    [2, 'asc']
                    ],
                    dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    displayLength: 10,
                    lengthMenu: [10, 25, 50, 75, 100],
                    buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                        {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Data Import Files',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Data Import Files',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Data Import Files',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Data Import Files',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Data Import Files',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#data_import_files_add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                    ],
                    responsive: {
                       details: {
                          display: $.fn.dataTable.Responsive.display.childRowImmediate,
                          type: 'column',
                      }
                  },
                  language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
$('#data-import-files div.head-label').html('<h6 class="mb-0">List of Data Import Files</h6>');
}

        // data_importers Edit
        function data_importers_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('dataImport')}}" + "/" + id + "/edit",
                type: "get",
                data: {type : 'data_importers'},
                success: function (response) {
                    $("#edit_data_importers_name").val(response.name);
                    $("#edit_data_type").val(response.dataType);
                    $("#edit_details").val(response.details);
                    $("#data_importers_edit_modal").modal("show");
                },
            });
        }

        // Delete data_importers
        function data_importers_delete(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "dataImport/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}",
                                    type: 'data_importers'
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else{
                                        $('#data_importers').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Data Importer has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Data Import Files Edit
        function data_import_files_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('dataImport')}}" + "/" + id + "/edit",
                type: "get",
                success: function (response) {
                    $("#edit_data_import_files_name").val(response.name);
                    $("#edit_data_import_definition").val(response.data_import_definition).select2();
                    $("#edit_file").val(response.file);
                    $('#edit_data_import_files_details').text(response.details);
                    $("#data_import_files_edit_modal").modal("show");
                },
            });
        }

        // Delete data_import_files
        function data_import_files_delete(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "dataImport/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else{
                                        $('#data_import_files').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Data Import File has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        function addColumns(){ 
    $('#add_column_div').append( 
      "<div class='reset_id'>\
      <div class='row'>\
      <div class='col-md-12 col-12'>\
      <div class='mb-1'>\
      <label class='form-label'>Name</label>\
      <input class='form-control' type='text' placeholder='Enter Name' name='add_column_name' required>\
      </div>\
      </div>\
      <div class='col-md-12 col-12'>\
      <div class='mb-1'>\
      <label class='form-label'>Field Title\</label>\
      <input class='form-control' type='text' placeholder='Enter Title' name='add_column_title'>\
      </div>\
      </div>\
      <div class='col-md-12 col-12'>\
      <div class='mb-1'>\
      <label class='form-label'>Type</label>\
      <select class='form-control' name='add_column_type' required>\
      <option value='Normal'>Normal</option>\
      <option value='Reference'>Reference</option>\
      <option value='Attached'>Attached</option>\
      </select>\
      </div>\
      </div>\
      <div class='col-md-12 col-12'>\
      <div class='mb-1'>\
      <label class='form-label'>Depends On</label>\
      <select class='form-control' name='add_column_depends_on'>\
      <option value='N/A'>N/A</option>\
      <option value='Emergency Contacts'>Emergency Contacts</option>\
      <option value='Ethnicity'>Ethnicity</option>\
      <option value='Nationality'>Nationality</option>\
      <option value='JobTitle'>JobTitle</option>\
      <option value='PayFrequency'>PayFrequency</option>\
      <option value='PayGrade'>PayGrade</option>\
      <option value='EmploymentStatus'>EmploymentStatus</option>\
      <option value='CompanyStructure'>CompanyStructure</option>\
      <option value='Employee'>Employee</option>\
      </select>\
      </div>\
      </div>\
      <div class='col-md-12 col-12'>\
      <div class='mb-1'>\
      <label class='form-label'>Depends On Field</label>\
      <input class='form-control' type='text' placeholder='Enter Depends on Field' name='add_column_depends_on_field'>\
      </div>\
      </div>\
      <div class='col-md-12 col-12'>\
      <div class='mb-1'>\
      <label class='form-label'>Is Key Field</label>\
      <select class='form-control' name='add_column_key_field' required>\
      <option value='No'>No</option>\
      <option value='Yes'>Yes</option>\
      </select>\
      </div>\
      </div>\
      <div class='col-md-12 col-12'>\
      <div class='mb-1'>\
      <label class='form-label'>Is ID Field</label>\
      <select class='form-control' name='add_column_id_field' required>\
      <option value='No'>No</option>\
      <option value='Yes'>Yes</option>\
      </select>\
      </div>\
      </div>\
      </div>\
      </div>"
      );
} 
function columnsReset(){
    $('.reset_id').remove();
}
    </script>
    @endsection