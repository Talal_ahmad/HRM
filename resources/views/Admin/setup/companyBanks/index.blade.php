@extends('Admin.layouts.master')
@section('title', 'Company Banks')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Company_Banks')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Setup')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Company_Banks')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="company_banks-tab" data-bs-toggle="tab" href="#company_banks"
                            aria-controls="company_banks" role="tab" aria-selected="true">@lang('app.Company_Banks')</a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- Company Banks --}}
                    <div class="tab-pane active" id="company_banks" aria-labelledby="company_banks-tab" role="tabpanel">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th class="not_include"></th>
                                                <th>Sr.No</th>
                                                <th>Account Name</th>
                                                <th>Type</th>
                                                <th>Bank Name</th>
                                                <th>GL Account Number</th>
                                                <th>Bank Phone</th>
                                                <th>Bank Address</th>
                                                <th class="not_include">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Company_Bank')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="add_account_type">@lang('app.Account_Type')</label>
                                            <select name="type" id="add_account_type" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Account_Type')" required>
                                                <option value="">@lang('app.Select_Bank_Type')</option>
                                                <option value="cash">@lang('app.Cash')</option>
                                                <option value="bank">@lang('app.Bank')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="add_account_name">@lang('app.Account_Name')</label>
                                            <input type="text" id="add_account_name" class="form-control"
                                                placeholder="@lang('app.Account_Name')" name="account_name" required />
                                        </div>
                                    </div>
                                    @if (env('COMPANY') == 'JSML')
                                        <input type="hidden" name="erp_bank_id" id="erp_bank_id">
                                        <input type="hidden" name="bank_name">
                                        <div class="col-md-6 col-12 for-add-bank" style="display: none">
                                            <div class="mb-1">
                                                <label class="form-label" for="add_bank_wcash">@lang('app.Bank_Account_Without_Cash')
                                                    </label>
                                                <select name="add_bank_name" id="add_bank_wcash" class="select2 form-select"
                                                    data-placeholder="@lang('app.Select_Bank_Account')">
                                                    <option value=""></option>
                                                    @foreach (getBankAccounts() as $bank)
                                                        @if ($bank->account_type != 4)
                                                            <option value="{{ $bank->account_code }}">
                                                                {{ $bank->bank_account_name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12 for-add-cash" style="display: none">
                                            <div class="mb-1">
                                                <label class="form-label" for="add_bank_cash">@lang('app.Bank_Account_Cash')</label>
                                                <select name="add_bank_name" id="add_bank_cash" class="select2 form-select"
                                                    data-placeholder="@lang('app.Select_Bank_Account')">
                                                    <option value=""></option>
                                                    @foreach (getBankAccounts() as $bank)
                                                        @if ($bank->account_type == 4)
                                                            <option value="{{ $bank->account_code }}">
                                                                {{ $bank->bank_account_name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="erp_gl_account_id" id="erp_gl_account_id">
                                        <input type="hidden" name="gl_account_number">
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="add_gl_account">@lang('app.GL_Accounts')</label>
                                                <select name="add_gl_account_number" id="add_gl_account"
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_GL_Account')">
                                                    <option value=""></option>
                                                    @foreach (getGlAccountsList() as $account)
                                                        <option value="{{ $account->id }}">
                                                            {{ $account->account_code . ' ' . $account->account_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="bank_name">@lang('app.Bank_Name')</label>
                                                <input type="text" class="form-control"
                                                    placeholder="@lang('app.Bank_Name')" name="bank_name" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="gl_account_number">@lang('app.GL_Accounts')</label>
                                                <input type="text" class="form-control"
                                                    placeholder="@lang('app.GL_Accounts')" name="gl_account_number" required />
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="add_bank_phone">@lang('app.Bank_Phone')</label>
                                            <input type="text" id="add_bank_phone" class="form-control phone"
                                                placeholder="@lang('app.Phone')" name="bank_phone" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="add_bank_address">@lang('app.Bank_Address')</label>
                                            <input type="text" id="add_bank_address" class="form-control address"
                                                placeholder="@lang('app.Bank_Address')" name="bank_address" required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End Add Modal -->
            <!--start edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Company_Bank')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_account_type">@lang('app.Account_Type')</label>
                                            <select name="type" id="edit_account_type" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Account_Type')" required>
                                                <option value="">@lang('app.Select_Bank_Type')</option>
                                                <option value="cash">@lang('app.Cash')</option>
                                                <option value="bank">@lang('app.Bank')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_account_name">@lang('app.Account_Name')</label>
                                            <input type="text" id="edit_account_name" class="form-control"
                                                placeholder="@lang('app.Account_Name')" name="account_name" required />
                                        </div>
                                    </div>
                                    @if (env('COMPANY') == 'JSML')
                                        <input type="hidden" name="erp_bank_id" id="erp_bank_id">
                                        <input type="hidden" name="bank_name">
                                        <div class="col-md-6 col-12 for-edit-bank" style="display: none">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_bank_wcash">@lang('app.Bank_Account_Without_Cash')
                                                    </label>
                                                <select name="edit_bank_name" id="edit_bank_wcash"
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Bank_Account')">
                                                    <option value=""></option>
                                                    @foreach (getBankAccounts() as $bank)
                                                        @if ($bank->account_type != 4)
                                                            <option value="{{ $bank->account_code }}">
                                                                {{ $bank->bank_account_name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12 for-edit-cash" style="display: none">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_bank_cash">@lang('app.Bank_Account_Cash')</label>
                                                <select name="edit_bank_name" id="edit_bank_cash"
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Bank_Account')">
                                                    <option value=""></option>
                                                    @foreach (getBankAccounts() as $bank)
                                                        @if ($bank->account_type == 4)
                                                            <option value="{{ $bank->account_code }}">
                                                                {{ $bank->bank_account_name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="erp_gl_account_id" id="erp_gl_account_id">
                                        <input type="hidden" name="gl_account_number">
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_gl_account">@lang('app.GL_Accounts')</label>
                                                <select name="edit_gl_account_number" id="edit_gl_account_number"
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_GL_Account')">
                                                    <option value=""></option>
                                                    @foreach (getGlAccountsList() as $account)
                                                        <option value="{{ $account->id }}">
                                                            {{ $account->account_code . ' ' . $account->account_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="bank_name">@lang('app.Bank_Name')</label>
                                                <input type="text" id="edit_bank_name" class="form-control"
                                                    placeholder="@lang('app.Bank_Name')" name="bank_name" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_gl_account_number">@lang('app.GL_Accounts')</label>
                                                <input type="text" id="edit_gl_account_number" class="form-control"
                                                    placeholder="@lang('app.GL_Accounts')" name="gl_account_number" required />
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_bank_phone">@lang('app.Bank_Phone')</label>
                                            <input type="text" id="edit_bank_phone" class="form-control phone"
                                                placeholder="@lang('app.Phone')" name="bank_phone" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_bank_address">@lang('app.Bank_Address')</label>
                                            <input type="text" id="edit_bank_address" class="form-control address"
                                                placeholder="@lang('app.Bank_Address')" name="bank_address" required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End edit Modal -->
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('company_banks.index') }}",
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'account_name',
                        name: 'account_name',
                    },
                    {
                        data: 'type',
                        name: 'type',
                    },
                    {
                        data: 'bank_name',
                        name: 'bank_name',
                    },
                    {
                        data: 'gl_account_number',
                        name: 'gl_account_number',
                    },
                    {
                        data: 'bank_phone',
                        name: 'bank_phone',
                    },
                    {
                        data: 'bank_address',
                        name: 'bank_address',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Company Banks</h6>');

            $("#add_form").submit(function(e) {
                ButtonStatus('.form_save', true);
                blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('company_banks.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        ButtonStatus('.form_save', false);
                        $.unblockUI();
                        if (response.code == 200) {
                            $('#add_form')[0].reset();
                            $("#add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Bank has been Added Successfully!'
                            });
                        } else if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                    }
                });
            });

            // Update record
            $("#edit_form").on("submit", function(e) {
                ButtonStatus('.form_save', true);
                blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('company_banks') }}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save', false);
                        $.unblockUI();
                        if (response.code == 200) {
                            $("#edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Bank has been Updated Successfully!'
                            })
                        } else if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                    }
                });
            });
            @if (env('COMPANY') == 'JSML')
                $('#add_account_type').on('change', function() {
                    var type = $(this).val();
                    if (type == 'bank') {
                        $('.for-add-cash').hide(300);
                        $('.for-add-bank').show(300);
                        $('#add_bank_wcash').attr('required', true);
                        $('#add_bank_cash').attr('required', false);
                        $('#add_bank_phone').attr('required', true);
                        $('#add_bank_address').attr('required', true);
                    } else {
                        $('.for-add-bank').hide(300);
                        $('.for-add-cash').show(300);
                        $('#add_bank_cash').attr('required', true);
                        $('#add_bank_wcash').attr('required', false);
                        $('#add_bank_phone').attr('required', false);
                        $('#add_bank_address').attr('required', false);
                    }
                });
                $('#add_bank_cash').change(function() {
                    $('#add_form #erp_bank_id').val($(this).val());
                    $("#add_form [name='bank_name']").val($("#add_bank_cash option:selected").text());
                });
                $('#add_bank_wcash').change(function() {
                    $('#add_form #erp_bank_id').val($(this).val());
                    $("#add_form [name='bank_name']").val($("#add_bank_wcash option:selected").text());
                });
                $('#add_gl_account').change(function() {
                    $('#add_form #erp_gl_account_id').val($(this).val());
                    $("#add_form [name='gl_account_number']").val($("#add_gl_account option:selected")
                        .text());
                });
                $('#edit_account_type').on('change', function() {
                    var type = $(this).val();
                    if (type == 'bank') {
                        $('.for-edit-cash').hide(300);
                        $('.for-edit-bank').show(300);
                        $('#edit_bank_wcash').attr('required', true);
                        $('#edit_bank_cash').attr('required', false);
                        $('#edit_bank_phone').attr('required', true);
                        $('#edit_bank_address').attr('required', true);
                    } else {
                        $('.for-edit-bank').hide(300);
                        $('.for-edit-cash').show(300);
                        $('#edit_bank_cash').attr('required', true);
                        $('#edit_bank_wcash').attr('required', false);
                        $('#edit_bank_phone').attr('required', false);
                        $('#edit_bank_address').attr('required', false);
                    }
                });
                $('#edit_bank_cash').change(function() {
                    $('#edit_form #erp_bank_id').val($(this).val());
                    $("#edit_form [name='bank_name']").val($("#edit_bank_cash option:selected").text());
                });
                $('#edit_bank_wcash').change(function() {
                    $('#edit_form #erp_bank_id').val($(this).val());
                    $("#edit_form [name='bank_name']").val($("#edit_bank_wcash option:selected").text());
                });
                $('#edit_gl_account_number').change(function() {
                    $('#edit_form #erp_gl_account_id').val($(this).val());
                    $("#edit_form [name='gl_account_number']").val($(
                            "#edit_gl_account_number option:selected")
                        .text());
                });
            @endif
        });

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('company_banks') }}" + "/" + id + "/edit",
                type: "get",
                success: function(response) {
                    $('#edit_account_type').val(response.type).select2();
                    $('#edit_account_name').val(response.account_name);
                    @if (env('COMPANY') == 'JSML')
                        if (response.type == 'bank') {
                            $('.for-edit-cash').hide(300);
                            $('.for-edit-bank').show(300);
                            $('#edit_bank_wcash').attr('required', true);
                            $('#edit_bank_cash').attr('required', false);
                            $('#edit_bank_phone').attr('required', true);
                            $('#edit_bank_address').attr('required', true);
                            $('#edit_bank_wcash').val(response.erp_bank_id).select2().trigger("change");
                        } else {
                            $('.for-edit-bank').hide(300);
                            $('.for-edit-cash').show(300);
                            $('#edit_bank_cash').attr('required', true);
                            $('#edit_bank_wcash').attr('required', false);
                            $('#edit_bank_phone').attr('required', false);
                            $('#edit_bank_address').attr('required', false);
                            $('#edit_bank_cash').val(response.erp_bank_id).select2();
                        }
                        $('#edit_gl_account_number').val(response.erp_gl_account_id).select2().trigger("change");
                    @else
                        $('#edit_bank_name').val(response.bank_name);
                        $('#edit_gl_account_number').val(response.gl_account_number);
                    @endif
                    $('#edit_bank_phone').val(response.bank_phone);
                    $('#edit_bank_address').val(response.bank_address);
                    $("#edit_modal").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "company_banks/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Company Bank has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
