@extends('Admin.layouts.master')

@section('title', 'HR Form Management')
@section('content')

<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">HR Form Management</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Setup</a>
                            </li>
                            <li class="breadcrumb-item active">HR Form Management
                            </li>
                        </ol>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <div class="content-body">
        <section class="basic-tabs-components">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="hr-forms-tab" data-bs-toggle="tab" href="#hr-forms" aria-controls="hr-forms" role="tab" aria-selected="true">HR Forms</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="employee-forms-tab" data-bs-toggle="tab" href="#employee-forms" aria-controls="employee-forms" role="tab" aria-selected="false" onclick="employee_forms()">Employee Forms</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="hr-forms" aria-labelledby="hr-forms-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="pb-2">
                                        <table class="table" id="hr_forms">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Name</th> 
                                                    <th>Description</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                {{-- Employee Forms --}}
                <div class="tab-pane" id="employee-forms" aria-labelledby="employee-forms-tab" role="tabpanel">
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pb-2">
                                    <table class="table" id="employee_forms">
                                        <thead>
                                            <tr>
                                               <th></th>
                                               <th>Employee</th>
                                               <th>Form</th>
                                               <th>Status</th>
                                               <th class="not_include">Actions</th>
                                           </tr>
                                       </thead>
                                       <tbody>

                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </div>
                   </section>
               </div>
               <!--HR Forms Add Modal -->
               <div class="modal fade text-start" id="hr_forms_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add HR Form</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="hr_forms_add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="name">Form Name</label>
                                            <input type="text" id="name" class="form-control" name="name" placeholder="Name" required/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="description">Description</label>
                                            <textarea name="description" id="description" cols="20" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="fields">Fields</label>
                                            <button type="button" class="btn btn-primary" onclick="addFields()">Add</button>
                                            <button type="button" class="btn btn-outline-success" onclick="fieldsReset()">Reset</button>
                                            <div class="col-md-12 col-12" id="add_field_div">
                                              <div class="mb-1">
                                              </div>  
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End HR FOrms Add Modal -->

        <!--HR Forms Edit Modal -->
        <div class="modal fade text-start" id="hr_forms_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit HR Form</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="hr_forms_edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_name">Form Name</label>
                                        <input type="text" id="edit_name" class="form-control" name="name" placeholder="Name" required/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_description">Description</label>
                                        <textarea name="description" id="edit_description" cols="20" rows="2" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End HR FOrm Edit Modal -->

        <!--EMployee Form Add Modal -->
        <div class="modal fade text-start" id="employee_forms_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Employee Form</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="employee_forms_add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="employee">Employee</label>
                                        <select name="employee" id="employee" class="select2 form-select" data-placeholder="Select Employee" required>
                                            <option value=""></option>
                                            @foreach ($employees as $type)
                                            <option value="{{$type->id}}">{{$type->employee}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="form">Form</label>
                                        <select name="form" id="form" class="select2 form-select" data-placeholder="Select Form" required>
                                            <option value=""></option>
                                            @foreach ($forms as $type)
                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="status">Status</label>
                                        <select name="status" id="status" class="select2 form-select" data-placeholder="Select Status" required>
                                            <option value="pending">Pending</option>
                                            <option value="completed">Completed</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Employee Forms Add Modal -->

        <!--Employee Forms Edit Modal -->
        <div class="modal fade text-start" id="employee_forms_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Employee Form</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="employee_forms_edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_employee">Employee</label>
                                        <select name="employee" id="edit_employee" class="select2 form-select" data-placeholder="Select Employee" required>
                                            <option value=""></option>
                                            @foreach ($employees as $type)
                                            <option value="{{$type->id}}">{{$type->employee}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_form">Form</label>
                                        <select name="form" id="edit_form" class="select2 form-select" data-placeholder="Select Form" required>
                                            <option value=""></option>
                                            @foreach ($forms as $type)
                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_status">Status</label>
                                        <select name="status" id="edit_status" class="select2 form-select" data-placeholder="Select Status" required>
                                            <option value="pending">Pending</option>
                                            <option value="completed">Completed</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End employee forms Edit Modal -->
    </div>
</section>
</div>
</section>
@endsection
@section('scripts')
<script>
    var rowid;
    $(document).ready(function() {

        $('#hr_forms').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            destroy: true,
            ajax: {
                url: "{{ route('hrFormManagement.index') }}",
                data: {
                    type: "hr_forms"
                }
            },
            columns: [
            {
                data: 'responsive_id' 
            },
            {
                data : 'name',
                name: 'name',
            },
            {
                data: 'description',
                name: 'description',
            },
            {
                data: '',
            },
            ],
            "columnDefs": [
            {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=hr_forms_edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="hr_forms_delete('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                                );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                    ],
                    dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    displayLength: 10,
                    aLengthMenu: [
                        [10,25,50,100,-1],
                        [10,25,50,100,"All"]
                    ],
                    buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                        {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'HR Forms',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'HR Forms',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'HR Forms',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'HR Forms',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'HR Forms',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#hr_forms_add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                    ],
                    responsive: {
                       details: {
                          display: $.fn.dataTable.Responsive.display.childRowImmediate,
                          type: 'column',
                      }
                  },
                  language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;' 
                }
            }
        });
            // Filter form control to default size for all tables
            $('.dataTables_filter .form-control').removeClass('form-control-sm');
            $('.dataTables_length .form-select').removeClass('form-select-sm').removeClass('form-control-sm');

            $('#hr-forms div.head-label').html('<h6 class="mb-0">List of HR Forms</h6>');
            
            // Store hr forms
            $("#hr_forms_add_form").on("submit", function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'hr_forms');
                $.ajax({
                    url: "{{route('hrFormManagement.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#hr_forms_add_modal").modal("hide");
                            document.getElementById("hr_forms_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#hr_forms').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'HR FOrm has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

			// HR FOrm Update
            $("#hr_forms_edit_form").on("submit", function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'hr_forms');
                $.ajax({
                    url: "{{url('hrFormManagement')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#hr_forms_edit_modal").modal("hide");
                            document.getElementById("hr_forms_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#hr_forms').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'HR Form has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Store Employee Forms
            $("#employee_forms_add_form").on("submit", function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{route('hrFormManagement.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#employee_forms_add_modal").modal("hide");
                            document.getElementById("employee_forms_add_form").reset();
                            $(".select2").val('').trigger('change')

                            $('#employee_forms').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Form has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // employee_forms Update
            $("#employee_forms_edit_form").on("submit", function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{url('hrFormManagement')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#employee_forms_edit_modal").modal("hide");
                            document.getElementById("employee_forms_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#employee_forms').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Form has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });
        });

        // employee_forms DataTable
        function employee_forms()
        {
            $('#employee_forms').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('hrFormManagement.index') }}",
                    data: {
                        type: "employee_forms"
                    }
                },
                columns: [
                {
                    data: 'responsive_id'
                },
                {
                    data : 'employee',
                    name: 'employee',
                },
                {
                    data: 'name',
                    name: 'forms.name',
                },
                {
                    data: 'status',
                    name: 'employeeforms.status',
                },
                {
                    data: '',
                },
                ],
                "columnDefs": [
                {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=employee_forms_edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="employee_forms_delete('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                                );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                    ],
                    order: [
                    [2, 'asc']
                    ],
                    dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    displayLength: 10,
                    lengthMenu: [10, 25, 50, 75, 100],
                    buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                        {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employee Forms',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employee Forms',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employee Forms',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employee Forms',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employee Forms',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                        }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#employee_forms_add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                    ],
                    responsive: {
                       details: {
                          display: $.fn.dataTable.Responsive.display.childRowImmediate,
                          type: 'column',
                      }
                  },
                  language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
$('#employee-forms div.head-label').html('<h6 class="mb-0">List of Employee Forms</h6>');
}

        // hr_forms Edit
        function hr_forms_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('hrFormManagement')}}" + "/" + id + "/edit",
                type: "get",
                data: {type : 'hr_forms'}, 
                success: function (response) {
                    $("#edit_name").val(response.name);
                    $("#edit_description").val(response.description);
                    $("#hr_forms_edit_modal").modal("show");
                },
            });
        }

        // Delete hr_forms
        function hr_forms_delete(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "hrFormManagement/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}",
                                    type: 'hr_forms'
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else if(response.code == 300)
                                    {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        $('#hr_forms').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'HR Form has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!'); 
                    },
                }
            });
        }

        // Employee FOrm Edit
        function employee_forms_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('hrFormManagement')}}" + "/" + id + "/edit",
                type: "get",
                success: function (response) {
                    $("#edit_employee").val(response.employee).select2();
                    $("#edit_form").val(response.form).select2();
                    $('#edit_status').val(response.status).select2();
                    $("#employee_forms_edit_modal").modal("show"); 
                },
            });
        }

        // Delete employee_forms
        function employee_forms_delete(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "hrFormManagement/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else{
                                        $('#employee_forms').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Employee Form has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        function addFields(){ 
            $('#add_field_div').append( 
              "<div class='reset_id'>\
              <div class='row'>\
              <div class='col-md-12 col-12'>\
              <div class='mb-1'>\
              <label class='form-label'>Name</label>\
              <input class='form-control' type='text' placeholder='Enter Name' name='add_field_name' required>\
              </div>\
              </div>\
              <div class='col-md-12 col-12'>\
              <div class='mb-1'>\
              <label class='form-label'>Field Label\</label>\
              <input class='form-control' type='text' placeholder='Enter Label' name='add_field_label' required>\
              </div>\
              </div>\
              <div class='col-md-12 col-12'>\
              <div class='mb-1'>\
              <label class='form-label'>Field Type</label>\
              <select class='form-control' name='add_field_type' required>\
              <option value='Text Field'>Text Field</option>\
              <option value='Text Area'>Text Area</option>\
              <option value='Select'>Select</option>\
              <option value='Select2'>Select2</option>\
              <option value='Multi Select'>Multi Select</option>\
              <option value='File Upload'>File Upload</option>\
              <option value='Date'>Date</option>\
              <option value='Date Time'>Date Time</option>\
              <option value='Time'>Time</option>\
              </select>\
              </div>\
              </div>\
              <div class='col-md-12 col-12'>\
              <div class='mb-1'>\
              <label class='form-label'>Validation</label>\
              <select class='form-control' name='add_field_validation'>\
              <option value='Required'>Required</option>\
              <option value='None'>None</option>\
              <option value='Number'>Number</option>\
              <option value='Number or Empty'>Number or Empty</option>\
              <option value='Decimal'>Decimal</option>\
              <option value='Email'>Email</option>\
              <option value='Email or Empty'>Email or Empty</option>\
              </select>\
              </div>\
              </div>\
              <div class='col-md-12 col-12'>\
              <div class='mb-1'>\
              <label class='form-label'>Options</label>\
              <textarea name='add_field_options' cols='20' rows='2' placeholder='Enter Options' class='form-control'></textarea>\
              </div>\
              </div>\
              <div class='col-md-12 col-12'>\
              <div class='mb-1'>\
              <label class='form-label'>Help</label>\
              <input class='form-control' type='text' placeholder='Enter Help' name='add_field_help'>\
              </div>\
              </div>\
              </div>\
              </div>"
              );
        } 
        function fieldsReset(){
            $('.reset_id').remove();
        }
    </script>
    @endsection