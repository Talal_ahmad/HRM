@extends('Admin.layouts.master') @section('title', 'Job Positions')
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">
                        @lang('app.Job_Positions')
                    </h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/') }}">@lang('app.dashboard')</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">@lang('app.Setup')</a>
                            </li>
                            <li class="breadcrumb-item active">
                                @lang('app.Job_Positions')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-dataTable">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{route('job_positions.index')}}">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <label class="form-label" for="dept">@lang('app.Department')</label>
                                        <select name="department" id="dept" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                            <option value=""></option>
                                            @if (Auth::user()->hasRole('Admin'))
                                                <option value="all" {{request('department') == 'all' ? 'selected' : ''}}>All</option>
                                            @endif
                                            @foreach (departments() as $department)
                                            <option value="{{ $department->id }}" {{request('department') == $department->id ? 'selected' : ''}}>{{ $department->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <a href="{{url('job_positions')}}" type="button" class="btn btn-danger" style="margin-top: 24px">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary" style="margin-top: 24px">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="employees_job_position">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Job Title</th>
                                    <th>Department</th>
                                    <th>No. of Persons</th>
                                    <th>Persons Used</th>
                                    <th>Remaining Persons</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total_person = 0;
                                    $used_person = 0;
                                    $remaining_person = 0;
                                @endphp
                                @if (isset($JobPositions) && count($JobPositions) > 0)
                                    @foreach ($JobPositions as $key => $position)
                                        <tr>
                                            <td></td>
                                            <td>{{$key+1}}</td>
                                            <td>{{$position->name}}</td>
                                            <td>{{$position->title}}</td>
                                            @php
                                                $total_person += $position->num_of_persons;
                                                $used_person += $position->person_used;
                                            @endphp
                                            <td>{{$position->num_of_persons}}</td>
                                            <td>{{$position->person_used}}</td>
                                            @if ($position->remaining_persons == 0)
                                                @php
                                                    $remaining_person += $position->remaining_persons;
                                                @endphp
                                                <td><span class="badge bg-success">{{$position->remaining_persons}}</span></td>
                                            @elseif($position->remaining_persons > 0)
                                                @php
                                                    $remaining_person += $position->remaining_persons;
                                                @endphp
                                                <td><span class="badge bg-warning">{{$position->remaining_persons}}</span></td>
                                            @else
                                                @php
                                                    $remaining_person += $position->remaining_persons;
                                                @endphp
                                                <td><span class="badge bg-danger">{{$position->remaining_persons}}</span></td>
                                            @endif
                                            <td>
                                                @can('Job Position Edit Button')    
                                                    <a href="javascript:;" class="item-edit" onclick="edit({{$position->id}})"><i data-feather="edit" class="font-medium-4"></i></a>
                                                @endcan
                                                @can('Job Position Delete Button')    
                                                    <a href="javascript:;" onclick="delete_item({{$position->id}})"> <i data-feather="trash-2" class="font-medium-4 text-danger"></i></a>
                                                @endcan
                                            </td>
                                        </tr>   
                                    @endforeach
                                    <tr>
                                        <td class="not_include"></td>
                                        <td class="not_include"></td>
                                        <td class="not_include"></td>
                                        <td>Grand Total</td>
                                        <td>{{$total_person}}</td>
                                        <td>{{$used_person}}</td>
                                        <td>{{$remaining_person}}</td>
                                        <td class="not_include"></td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--start edit Modal -->
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Job Position</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="department_id">@lang('app.Departments')</label>
                                        <select name="department_id" id="department_id" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                            <option value=""></option>
                                            @foreach (departments() as $department)
                                                <option value="{{$department->id}}">{{$department->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="job_title">@lang('app.Job_Titles')</label>
                                        <select name="job_title" id="job_title" class="select2 form-select" data-placeholder="@lang('app.Select_Job_Title')" required>
                                            <option value=""></option>
                                            @foreach ($jobtitles as $title)
                                                <option value="{{$title->id}}">{{$title->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <label class="form-label" for="job_title">@lang('app.No_of_Persons')</label>
                                    <input type="text" name="no_of_persons" id="no_of_persons" class="form-control" placeholder="@lang('app.No_of_Persons')">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End edit Modal -->
        </section>
    </div>
</section>
@endsection 
@section('scripts')
    @if(Session::has('success_message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('success_message') !!}"
            })
        </script>
    @endif
<script>
    var rowid;
    var datatable;
    $(document).ready(function () {
        datatable = $('#employees_job_position').DataTable({
            ordering: true,
            "columnDefs": [
                {
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    targets: 0
                },
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [
                {
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                    buttons: [
                        {
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                        className: 'dropdown-item',
                        title: 'Employees Payroll',
                            exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                        className: 'dropdown-item',
                        title: 'Employees Payroll',
                            exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                        className: 'dropdown-item',
                        title: 'Employees Payroll',
                            exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                        className: 'dropdown-item',
                        title: 'Employees Payroll',
                        orientation : 'landscape',
                        pageSize : 'A2',
                        customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();
                            doc.pageMargins = [20, 60, 20, 30];
                            // Set the font size fot the entire document
                            doc.defaultStyle.fontSize = 7;
                            // Set the fontsize for the table header
                            doc.styles.tableHeader.fontSize = 7;
                            // Create a header
                            doc["header"] = function () {
                                return {
                                    columns: [
                                        {
                                            alignment: "center",
                                            italics: true,
                                            text: "Employees Payroll",
                                            fontSize: 18,
                                            margin: [10, 0]
                                        },
                                    ],
                                    margin: 20
                                };
                            };
                            doc["footer"] = function (page, pages) {
                                return {
                                    columns: [
                                        {
                                            alignment: "left",
                                            text: ["Created on: ", { text: jsDate.toString() }]
                                        },
                                        {
                                            alignment: "right",
                                            text: [
                                                "page ",
                                                { text: page.toString() },
                                                " of ",
                                                { text: pages.toString() }
                                            ]
                                        }
                                    ],
                                    margin: 20
                                };
                            };
                            var objLayout = {};
                            objLayout["hLineWidth"] = function (i) {
                                return 0.5;
                            };
                            objLayout["vLineWidth"] = function (i) {
                                return 0.5;
                            };
                            objLayout["hLineColor"] = function (i) {
                                return "#aaa";
                            };
                            objLayout["vLineColor"] = function (i) {
                                return "#aaa";
                            };
                            objLayout["paddingLeft"] = function (i) {
                                return 4;
                            };
                            objLayout["paddingRight"] = function (i) {
                                return 4;
                            };
                            doc.content[0].layout = objLayout;
                        },
                            exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                        className: 'dropdown-item',
                        title: 'Employees Payroll',
                            exportOptions: { columns: ':not(.not_include)' }
                        }
                    ],
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function () {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                        }, 50);
                    }
                },  
                @can('Job Position Add New Button')    
                {
                    text: feather.icons['plus'].toSvg({
                        class: 'me-50  font-small-4'
                    }) + 'Add New',
                    className: 'btn btn-primary',
                    action: function(e, dt, node, config) {
                        window.location.href = '{{ route('job_positions.create') }}';
                    }
                },
                @endcan
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                // remove previous & next text from pagination
                previous: '&nbsp;',
                next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Job Positions</h6>');
        // Update Data
        $('#edit_form').on('submit' , function(e){
            ButtonStatus('.form_save',true);
            blockUI();
            e.preventDefault();
            $.ajax({
                url: "{{url('job_positions')}}" + "/" + rowid,
                type: "POST",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(response){
                    ButtonStatus('.form_save',false);
                    $.unblockUI();
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        $('#edit_modal').modal('hide');
                        document.getElementById("edit_form").reset();
                        Toast.fire({
                            icon: 'success',
                            title: 'Job Position has been Updated Successfully!'
                        })
                        setTimeout(function(){
                            window.location.reload();
                        }, 1000);
                    }
                }
            });
        })
    });

    // Edit Function
    function edit(id){
        rowid = id;
        $.ajax({
            url: "{{url('job_positions')}}" + "/" + id + "/edit",
            type: "GET",
            success: function(response){
                $('#department_id').val(response.department_id).select2();
                $('#job_title').val(response.jobtitle).select2();
                $('#no_of_persons').val(response.num_of_persons);
                $('#edit_modal').modal('show');
            }
        });
    }

    function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "job_positions/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Job Position has been Deleted Successfully!'
                                        });
                                        setTimeout(function(){
                                            window.location.reload();
                                        }, 1000);
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
</script>
@endsection