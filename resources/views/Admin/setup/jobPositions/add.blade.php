@extends('Admin.layouts.master')
@section('title', 'Add Job Position')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Add_Job_Position')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.setup')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Job_Position')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Add_Job_Position')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form" action="{{route('job_positions.store')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="department_id">@lang('app.Departments')</label>
                                            <select name="department_id" id="department_id" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Job Title</th>
                                                    <th>No of Persons</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($jobtitles as $jobtitle)
                                                    <tr>
                                                        <td>{{$jobtitle->name}}</td>
                                                        <td>
                                                            <input type="hidden" name="jobtitles[]" value="{{$jobtitle->id}}">
                                                            <input type="text" name="no_of_persons[{{$jobtitle->id}}]" class="form-control" placeholder="No of Persons">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
