@extends('Admin.layouts.master')
@section('title', 'Overtime Administration')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Overtime_Administration')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Setup')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Overtime_Administration')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        {{-- Overtime Categories --}}
                        <a class="nav-link active" id="overtimeCategories-tab" data-bs-toggle="tab"
                            href="#overtimeCategories" aria-controls="overtimeCategories" role="tab"
                            aria-selected="true">@lang('app.Overtime_Categories')</a>
                    </li>
                    <li class="nav-item">
                        {{-- Overtime Requests --}}
                        <a class="nav-link" id="overtimeRequests-tab" onclick="overtimeRequests()"
                            data-bs-toggle="tab" href="#overtimeRequests" aria-controls="overtimeRequests" role="tab"
                            aria-selected="false">@lang('app.Overtime_Requests')</a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- Overtime Categories --}}
                    <div class="tab-pane active" id="overtimeCategories" aria-labelledby="overtimeCategories-tab"
                        role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="overtimeCategories_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!--Overtime Categories Add Modal -->
                        <div class="modal fade text-start" id="add_modal_overtimeCategories" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Overtime_Category')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="add_form_overtimeCategories">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="name">@lang('app.Name')</label>
                                                        <input type="text" id="name" class="form-control"
                                                            placeholder="@lang('app.Enter_name')" name="name" required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--End Overtime Categories Add Modal -->

                        <!--start Overtime Categories edit Modal -->
                        <div class="modal fade text-start" id="edit_modal_overtimeCategories" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Overtime_Category')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="edit_form_overtimeCategories">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_name">@lang('app.Name')</label>
                                                        <input type="text" id="edit_name" class="form-control"
                                                            name="name" required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--End Overtime Categories edit Modal -->
                    </div>

                    {{-- Overtime Requests --}}
                    <div class="tab-pane" id="overtimeRequests" aria-labelledby="overtimeRequests-tab"
                        role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="overtimeRequests_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Employee</th>
                                                    <th>Category</th>
                                                    <th>Start Time</th>
                                                    <th>End Time</th>
                                                    <th>Project</th>
                                                    <th>Status</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!--overtimeRequests Add Modal -->
                        <div class="modal fade text-start" id="add_modal_overtimeRequests" tabindex="-1"
                            aria-labelledby="myModalLabel18" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel18">@lang('app.Add_Overtime_Request')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="add_form_overtimeRequests">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                        <select class="form-control select2" name="employee" id="employee" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="category">@lang('app.Category')</label>
                                                        <select class="form-control select2" name="category" id="category" data-placeholder="@lang('app.Select_Category')" required>
                                                            <option value=""></option>
                                                            @foreach ($category as $item)
                                                                <option value="{{ $item->id }}">{{ $item->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="start_time">@lang('app.Start_Time')</label>
                                                        <input type="text" id="start_time" class="form-control flatpickr-time text-center flatpickr-input" placeholder="HH:MM" name="start_time" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="end_time">@lang('app.End_Time')</label>
                                                        <input type="text" id="end_time" class="form-control flatpickr-time text-left" placeholder="HH:MM" name="end_time" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="project">@lang('app.Project')</label>
                                                        <select class="form-control select2" name="project" id="project" data-placeholder="@lang('app.Select_Project')">
                                                            <option value=""></option>
                                                            @foreach ($project as $item)
                                                                <option value="{{ $item->id }}">{{ $item->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="notes">@lang('app.Notes')</label>
                                                        <textarea id="notes" class="form-control" cols="20" rows="2"
                                                            placeholder="@lang('app.Enter_Notes')" name="notes"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--End Overtime Requests Add Modal -->

                        <!--start Overtime Requests edit Modal -->
                        <div class="modal fade text-start" id="edit_modal_overtimeRequests" tabindex="-1"
                            aria-labelledby="myModalLabel18" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel18">@lang('app.Edit_Overtime_Request')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="edit_form_overtimeRequests">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_employee">@lang('app.Employee')</label>
                                                        <select class="form-control select2" name="employee" id="edit_employee" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_category">@lang('app.Category')</label>
                                                        <select class="form-control select2" name="category" id="edit_category" data-placeholder="@lang('app.Select_Category')" required>
                                                            <option value=""></option>
                                                            @foreach ($category as $item)
                                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_start_time">@lang('app.Start_Time')</label>
                                                        <input type="text" id="edit_start_time" class="form-control flatpickr-time text-center flatpickr-input" placeholder="HH:MM" name="start_time" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_end_time">@lang('app.End_Time')</label>
                                                        <input type="text" id="edit_end_time" class="form-control flatpickr-time text-left" placeholder="HH:MM" name="end_time" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_project">@lang('app.Project')</label>
                                                        <select class="form-control select2" name="project" id="edit_project" data-placeholder="@lang('app.Select_Project')">
                                                            <option value=""></option>
                                                            @foreach ($project as $item)
                                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_notes">@lang('app.Notes')</label>
                                                        <textarea id="edit_notes" class="form-control" cols="20" rows="2"
                                                            placeholder="@lang('app.Enter_Notes')" name="notes"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--End Salary Components edit Modal -->
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            $('#overtimeCategories_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('overtimeAdministration.index') }}",
                    data: {
                        type: 'overtimeCategories'
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'name', 
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=overtimeCategories_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="overtimeCategories_delete(' +
                                full.id + ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Overtime Categories',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Overtime Categories',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Overtime Categories',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Overtime Categories',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Overtime Categories',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal_overtimeCategories'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#overtimeCategories .head-label').html('<h6 class="mb-0">List of Overtime Categories</h6>');

            // Add overtimeCategories Data
            $("#add_form_overtimeCategories").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'overtimeCategories');
                $.ajax({
                    url: "{{ route('overtimeAdministration.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form_overtimeCategories')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal_overtimeCategories").modal("hide");
                            $('#overtimeCategories_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Overtime Category has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // overtimeCategories Update Data
            $("#edit_form_overtimeCategories").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'overtimeCategories');
                $.ajax({
                    url: "{{ url('overtimeAdministration') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#edit_form_overtimeCategories')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#edit_modal_overtimeCategories").modal("hide");
                            $('#overtimeCategories_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Overtime Category has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            // Add overtimeRequests
            $("#add_form_overtimeRequests").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'overtimeRequests');
                $.ajax({
                    url: "{{ route('overtimeAdministration.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form_overtimeRequests')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal_overtimeRequests").modal("hide");
                            $('#overtimeRequests_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Overtime Request has been Added Successfully!'
                            })
                        }

                    }
                });
            });
            // overtimeRequests Update data
            $("#edit_form_overtimeRequests").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'overtimeRequests');
                $.ajax({
                    url: "{{ url('overtimeAdministration') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_form_overtimeRequests")[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#edit_modal_overtimeRequests").modal("hide");
                            $('#overtimeRequests_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Overtime Request has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
        });

        // Onclick Functions
        function overtimeRequests() {
            // overtimeRequests DataTable
            $('#overtimeRequests_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('overtimeAdministration.index') }}",
                    data: {
                        type: 'overtimeRequests'
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'employeeName',
                        name: 'employeeName',
                    },
                    {
                        data: 'category',
                        name: 'overtimecategories.name',
                    },
                    {
                        data: 'start_time',
                        name: 'employeeovertime.start_time',
                    },
                    {
                        data: 'end_time',
                        name: 'employeeovertime.end_time',
                    },
                    {
                        data: 'project',
                        name: 'employeeovertime.project',
                    },
                    {
                        data: 'status',
                        name: 'employeeovertime.status',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=overtimeRequests_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="overtimeRequests_delete(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Overtime Requests',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Overtime Requests',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Overtime Requests',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Overtime Requests',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Overtime Requests',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal_overtimeRequests'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#overtimeRequests div.head-label').html('<h6 class="mb-0">List of Overtime Requests</h6>');
        }
        // Edit overtimeCategories
        function overtimeCategories_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('overtimeAdministration') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'overtimeCategories'
                },
                success: function(response) {
                    $("#edit_name").val(response.name);
                    $("#edit_modal_overtimeCategories").modal("show");
                },
            });
        }
        // overtimeCategories Delete
        function overtimeCategories_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "overtimeAdministration/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'overtimeCategories'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#overtimeCategories_dataTable').DataTable().ajax
                                    .reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Overtime Category has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Edit overtimeRequests
        function overtimeRequests_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('overtimeAdministration') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'overtimeRequests'
                },
                success: function(response) {
                    $("#edit_employee").val(response.employee).select2();
                    $("#edit_category").val(response.category).select2();
                    $("#edit_start_time").val(response.start_time);
                    $("#edit_end_time").val(response.end_time);
                    $("#edit_project").val(response.project).select2();
                    $("#edit_notes").val(response.notes);
                    $("#edit_modal_overtimeRequests").modal("show");
                },
            });
        }
        // overtimeRequests Delete
        function overtimeRequests_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "overtimeAdministration/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'overtimeRequests'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please ContactAdministrator.'
                                        })
                                    } else {
                                        $('#overtimeRequests_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Overtime Request has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
