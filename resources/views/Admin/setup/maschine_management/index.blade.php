@extends('Admin.layouts.master')
@section('title', 'Machine Management')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Machine_Management')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Home')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.Setup')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Machine_Management')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Machine ID</th>
                                    <th>Machine Department</th>
                                    <th>Machine Serial Number</th>
                                    <th>Status</th>
                                    <th>Machine SIM</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <!--Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Machine_Details')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_id">@lang('app.Machine_ID')</label>
                                        <input type="text"  class="form-control" placeholder="@lang('app.Machine_ID')" id="machine_id" name="machine_id" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_imei">@lang('app.Machine_IMEI')</label>
                                        <input type="text" class="form-control" placeholder="@lang('app.Machine_IMEI')" id="machine_imei" name="machine_imei"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_sr_no">@lang('app.Machine_Serial_No')</label>
                                        <input type="text" class="form-control" placeholder="@lang('app.Machine_Serial_No')" id="machine_sr_no" name="machine_sr_no" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_gps">@lang('app.Machine_GPS')</label>
                                        <input type="text"  class="form-control" placeholder="@lang('app.Machine_GPS')" id="machine_gps" name="machine_gps"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="lat">@lang('app.Lat')</label>
                                        <input type="text"  class="form-control" placeholder="@lang('app.Lat')" id="lat" name="latitude"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="long">@lang('app.Long')</label>
                                        <input type="text"  class="form-control" placeholder="@lang('app.Long')" id="long" name="longitude"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_sim">@lang('app.Machine_SIM')</label>
                                        <input type="text" class="form-control" placeholder="@lang('app.Machine_SIM')" id="machine_sim" name="machine_sim"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_department">@lang('app.Machine_Department')</label>
                                        <select name="machine_department" id="machine_department" class="select2 form-select" data-placeholder="@lang('app.Select_Machine_Department')" required>
                                            <option value=""></option>
                                            @foreach (departments() as $department)
                                                <option value="{{$department->id}}">{{$department->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_model_no">@lang('app.Machine_Model_No')</label>
                                        <input type="text" class="form-control" placeholder="@lang('app.Machine_Model_No')" id="machine_model_no" name="machine_model_no"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_desc">@lang('app.Machine_Description')</label>
                                        <input type="text" class="form-control" placeholder="@lang('app.Machine_Description')" id="machine_desc" name="machine_desc" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_type">@lang('app.Machine_Type')</label>
                                        <select name="machine_type" class="form-select select2" id="machine_type" data-placeholder="Select Machine Type" required>
                                            <option value=""></option>
                                            <option value="Time In Machine">@lang('app.Time_In_Machine')</option>
                                            <option value="Time Out Machine">@lang('app.Time_Out_Machine')</option>
                                            <option value="Both (IN/Out) Machine">@lang('app.Both_IN/Out_Machine')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="recogination_type">@lang('app.Machine_Recogination_Type')</label>
                                        <select name="machine_rec_type" id="recogination_type"  class="form-select select2" data-placeholder="@lang('app.Select_Machine_Recogination_Type')" required>
                                            <option value=""></option>
                                            <option value="Face">@lang('app.Face')</option>
                                            <option value="Card">@lang('app.Card')</option>
                                            <option value="Vein">@lang('app.Vein')</option>
                                            <option value="Finger Print">@lang('app.Finger_Print')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_score">@lang('app.Machine_Score')</label>
                                        <input type="text" class="form-control" id="machine_score" placeholder="@lang('app.Machine_Score')" name="machine_score"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="machine_key">@lang('app.Machine_Key')</label>
                                        <input type="text" class="form-control" id="machine_key" placeholder="@lang('app.Machine_Key')" name="machine_key"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="master_key">@lang('app.Master_Key')</label>
                                        <input type="text" class="form-control" id="master_key" placeholder="@lang('app.Master_Key')" name="master_key"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="tab_imei_number">@lang('app.Tab_IMEI_Number')</label>
                                        <input type="text"  class="form-control" id="tab_imei_number" placeholder="@lang('app.Tab_IMEI_Number')" name="tab_imei_number"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Add Modal -->

        <!--start edit Modal -->
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Machine Details</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_id">Machine ID</label>
                                        <input type="text" id="edit_machine_id" class="form-control" placeholder="Machine ID" name="machine_id" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_imei">Machine IMEI</label>
                                        <input type="text" id="edit_machine_imei" class="form-control" placeholder="Machine IMEI" name="machine_imei"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_sr_no">Machine Serial No</label>
                                        <input type="text" id="edit_machine_sr_no" class="form-control" placeholder="Machine Serial No" name="machine_sr_no" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_gps">Machine GPS</label>
                                        <input type="text" id="edit_machine_gps" class="form-control" placeholder="Machine GPS" name="machine_gps"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="lat">Lat</label>
                                        <input type="text"  class="form-control" placeholder="Machine Lat" id="lat_edit" name="latitude"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="long">Long</label>
                                        <input type="text"  class="form-control" placeholder="Machine Long" id="long_edit" name="longitude"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_sim">Machine SIM</label>
                                        <input type="text" id="edit_machine_sim" class="form-control" placeholder="Machine SIM" name="machine_sim"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_department">Machine Department</label>
                                        <select name="machine_department" id="edit_machine_department" class="form-select select2" data-placeholder="Machine Department" required>
                                            <option value=""></option>
                                            @foreach (departments() as $department)
                                                <option value="{{$department->id}}">{{$department->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_model_no">Machine Model No</label>
                                        <input type="text" id="edit_machine_model_no" class="form-control" placeholder="Machine Model No" name="machine_model_no"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_desc">Machine Description</label>
                                        <input type="text" id="edit_machine_desc" class="form-control" placeholder="Machine Description" name="machine_desc" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_type">Machine Type</label>
                                        <select name="machine_type" id="edit_machine_type" class="form-select select2" data-placeholder="Select Machine Type" required>
                                            <option value=""></option>
                                            <option value="Time In Machine">Time In Machine</option>
                                            <option value="Time Out Machine">Time Out Machine</option>
                                            <option value="Both (IN/Out) Machine">Both (IN/Out) Machine</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_rec_type">Machine Recogination Type</label>
                                        <select name="machine_rec_type" id="edit_machine_rec_type" class="form-select select2" data-placeholder="Select Machine Recogination Type" required>
                                            <option value=""></option>
                                            <option value="Face">Face</option>
                                            <option value="Card">Card</option>
                                            <option value="Vein">Vein</option>
                                            <option value="Finger Print">Finger Print</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_score">Machine Score</label>
                                        <input type="text" id="edit_machine_score" class="form-control" placeholder="Machine Score" name="machine_score"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_machine_key">Machine Key</label>
                                        <input type="text" id="edit_machine_key" class="form-control" placeholder="Machine Key" name="machine_key"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_master_key">Master Key</label>
                                        <input type="text" id="edit_master_key" class="form-control" placeholder="Master Key" name="master_key"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_tab_imei_number">Tab IMEI Number</label>
                                        <input type="text" id="edit_tab_imei_number" class="form-control" placeholder="Tab IMEI Number" name="tab_imei_number"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="status">Status</label>
                                        <select name="status" id="status" class="form-select select2" data-placeholder="Select Machine Status" required>
                                            <option value=""></option>
                                            <option value="1">Active</option>
                                            <option value="0">Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End edit Modal -->
    </div>
</section>

@endsection

@section('scripts')
    <script>
        var rowid;
        var dataTable;        
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('machine_management.index') }}",
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data : 'machine_id',
                        name : 'machine_id',
                        searchable: false,

                    },
                    {
                        data: 'title',
                        name: 'companystructures.title',
                        searchable: true,
                    },
                    {
                        data: 'machine_sr_no',
                        name: 'machine_sr_no',
                        searchable: false,
                    },
                    {
                        data: 'status',
                        searchable: false,
                    },
                    {
                        data: 'machine_sim',
                        name: 'machine_sim',
                        searchable: false,
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Status
                        targets: -3,
                        title: 'Status',
                        render: function(data, type, full, meta) {
                            if(data==1){
                                    return (
                                        '<span class="badge bg-success">Active</span>'
                                    );
                                }
                                else{
                                    return (
                                        '<span class="badge bg-danger">Disable</span>'
                                    );
                                }
                        }
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#edit-modal" class="item-edit" onclick=edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="delete_item('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Maschine Management',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Maschine Management',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Maschine Management',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Maschine Management',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Maschine Management',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Machines</h6>');

            $("#add_form").submit(function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{route('machine_management.store')}}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#add_form')[0].reset();
                            // $(".select2").val('').trigger('change')
                            // $("#machine_department").select2("val", "");
                            $(".select2").val('').trigger('change')

                            $("#add_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Machine has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Update record
            $("#edit_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{url('machine_management')}}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#edit_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Machine has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
        });
        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('machine_management')}}" + "/" + id + "/edit",
                type: "get",
                success: function (response) {
                    $("#edit_machine_id").val(response.machine_id);
                    $("#edit_machine_imei").val(response.machine_imei);
                    $("#edit_machine_sr_no").val(response.machine_sr_no);
                    $("#edit_machine_gps").val(response.machine_gps);
                    $("#lat_edit").val(response.latitude);
                    $("#long_edit").val(response.longitude);
                    $("#edit_machine_sim").val(response.machine_sim);
                    $("#edit_machine_department").val(response.machine_department).select2();
                    $("#edit_machine_model_no").val(response.machine_model_no);
                    $("#edit_machine_desc").val(response.machine_desc);
                    $("#edit_machine_type").val(response.machine_type).select2();
                    $("#edit_machine_rec_type").val(response.machine_rec_type).select2();
                    $("#status").val(response.status).select2();
                    $("#edit_machine_score").val(response.machine_score);
                    $("#edit_machine_key").val(response.machine_key);
                    $("#edit_master_key").val(response.master_key);
                    $("#edit_tab_imei_number").val(response.tab_imei_number);
                    $("#edit_modal").modal("show");
                },
            });
        }
        function delete_item(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "machine_management/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else{
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Machine has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection