@extends('Admin.layouts.master')
@section('title', 'Manage Metadata')

@section('content')

    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Manage_Metadata')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Setup')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Manage_Metadata')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    {{-- Countries --}}
                    <li class="nav-item">
                        <a class="nav-link active" id="countries-tab" data-bs-toggle="tab" href="#countries"
                            aria-controls="countries" role="tab" aria-selected="true">@lang('app.Countries')</a>
                    </li>
                    {{-- Provinces --}}
                    <li class="nav-item">
                        <a class="nav-link" id="provinces-tab" data-bs-toggle="tab" href="#provinces"
                            aria-controls="provinces" role="tab" aria-selected="false" onclick="provinces()">@lang('app.Provinces')</a>
                    </li>
                    {{-- Currency --}}
                    <li class="nav-item">
                        <a class="nav-link" id="currency-types-tab" data-bs-toggle="tab" href="#currency-types"
                            aria-controls="currency-types" role="tab" aria-selected="false"
                            onclick="currency_types()">@lang('app.Currency_Type')</a>
                    </li>
                    {{-- Nationality --}}
                    <li class="nav-item">
                        <a class="nav-link" id="nationality-tab" data-bs-toggle="tab" href="#nationality"
                            aria-controls="nationality" role="tab" aria-selected="false"
                            onclick="nationality()">@lang('app.Nationality')</a>
                    </li>
                    {{-- Ethnicity --}}
                    <li class="nav-item">
                        <a class="nav-link" id="ethnicity-tab" data-bs-toggle="tab" href="#ethnicity"
                            aria-controls="ethnicity" role="tab" aria-selected="false" onclick="ethnicity()">@lang('app.Ethnicity')</a>
                    </li>
                    {{-- immigration-status --}}
                    <li class="nav-item">
                        <a class="nav-link" id="immigration-status-tab" data-bs-toggle="tab"
                            href="#immigration-status" aria-controls="immigration-status" role="tab" aria-selected="false"
                            onclick="immigration_status()">@lang('app.Immigration_Status')</a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- // countries --}}
                    <div class="tab-pane active" id="countries" aria-labelledby="countries-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="pb-2">
                                            <table class="table" id="countries_dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="not_include"></th>
                                                        <th>Sr.No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th class="not_include">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Provinces --}}
                    <div class="tab-pane" id="provinces" aria-labelledby="provinces-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="provinces_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Code</th>
                                                    <th>Name</th>
                                                    <th>Country</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Currency Types --}}
                    <div class="tab-pane" id="currency-types" aria-labelledby="currency-types-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="currency_types_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Code</th>
                                                    <th>Name</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Nationality --}}
                    <div class="tab-pane" id="nationality" aria-labelledby="nationality-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="nationality_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Ethnicity --}}
                    <div class="tab-pane" id="ethnicity" aria-labelledby="ethnicity-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="ethnicity_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Immigration Status --}}
                    <div class="tab-pane" id="immigration-status" aria-labelledby="immigration-status-tab"
                        role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="immigration_status_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--Countries Add Modal -->
                    <div class="modal fade text-start" id="countries_add_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Country')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="countries_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="country_code">@lang('app.Code')</label>
                                                    <input type="text" id="country_code" class="form-control" name="code"
                                                        placeholder="@lang('app.Code')" required />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="country_name">@lang('app.Name')</label>
                                                    <input type="text" id="country_name" class="form-control" name="name"
                                                        placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End country Add Modal -->

                    <!--country Edit Modal -->
                    <div class="modal fade text-start" id="countries_edit_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Country')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="countries_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_country_code">@lang('app.Code')</label>
                                                    <input type="text" id="edit_country_code" class="form-control"
                                                        name="code" placeholder="@lang('app.Code')" required />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_country_name">@lang('app.Name')</label>
                                                    <input type="text" id="edit_country_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Country Edit Modal -->

                    <!--provinces Add Modal -->
                    <div class="modal fade text-start" id="provinces_add_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Province')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="provinces_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="province_code">@lang('app.Code')</label>
                                                    <input type="text" id="province_code" class="form-control"
                                                        name="code" placeholder="@lang('app.Code')" required />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="province_name">@lang('app.Name')</label>
                                                    <input type="text" id="province_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="province_country">@lang('app.Country')</label>
                                                    <select name="country" id="province_country"
                                                        class="select2 form-select" data-placeholder="@lang('app.Select_Country')"
                                                        required>
                                                        <option value=""></option>
                                                        @foreach ($countries as $item)
                                                            <option value="{{ $item->code }}">{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End provinces Add Modal -->

                    <!--provinces edit Modal -->
                    <div class="modal fade text-start" id="provinces_edit_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Province')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="provinces_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_province_code">@lang('app.Code')</label>
                                                    <input type="text" id="edit_province_code" class="form-control"
                                                        name="code" placeholder="@lang('app.Code')" required />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_province_name">@lang('app.Name')</label>
                                                    <input type="text" id="edit_province_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_province_country">@lang('app.Country')</label>
                                                    <select name="country" id="edit_province_country"
                                                        class="select2 form-select" data-placeholder="@lang('app.Select_Country')"
                                                        required>
                                                        <option value=""></option>
                                                        @foreach ($countries as $item)
                                                            <option value="{{ $item->code }}">{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End provinces Edit Modal -->

                    <!--currency types Add Modal -->
                    <div class="modal fade text-start" id="currency_types_add_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Currency_Type')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="currency_types_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="currency_code">@lang('app.Code')</label>
                                                    <input type="text" id="currency_code" class="form-control"
                                                        name="code" placeholder="@lang('app.Code')" required />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="currency_name">@lang('app.Name')</label>
                                                    <input type="text" id="currency_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End currency Add Modal -->

                    <!--currency edit Modal -->
                    <div class="modal fade text-start" id="currency_types_edit_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Currency_Type')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="currency_types_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_currency_code">@lang('app.Code')</label>
                                                    <input type="text" id="edit_currency_code" class="form-control"
                                                        name="code" placeholder="@lang('app.Code')" required />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_currency_name">@lang('app.Name')</label>
                                                    <input type="text" id="edit_currency_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End currency Edit Modal -->

                    <!--nationality Add Modal -->
                    <div class="modal fade text-start" id="nationality_add_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Nationality')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="nationality_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="nationality_name">@lang('app.Name')</label>
                                                    <input type="text" id="nationality_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End nationality Add Modal -->

                    <!--nationality edit Modal -->
                    <div class="modal fade text-start" id="nationality_edit_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Nationality')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="nationality_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_nationality_name">@lang('app.Name')</label>
                                                    <input type="text" id="edit_nationality_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End nationality Edit Modal -->
                    
                    <!--ethnicity Add Modal -->
                    <div class="modal fade text-start" id="ethnicity_add_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Ethnicity')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="ethnicity_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="ethnicity_name">@lang('app.Name')</label>
                                                    <input type="text" id="ethnicity_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End ethnicity Add Modal -->

                    <!--ethnicity edit Modal -->
                    <div class="modal fade text-start" id="ethnicity_edit_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Ethnicity')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="ethnicity_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_ethnicity_name">@lang('app.Name')</label>
                                                    <input type="text" id="edit_ethnicity_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End ethnicity Edit Modal -->

                    <!--immigration status Add Modal -->
                    <div class="modal fade text-start" id="immigration_status_add_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Immigration_Status')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="immigration_status_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label"
                                                        for="immigration_status_name">@lang('app.Name')</label>
                                                    <input type="text" id="immigration_status_name" class="form-control"
                                                        name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End immigration status Add Modal -->

                    <!--immigration status edit Modal -->
                    <div class="modal fade text-start" id="immigration_status_edit_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Province')</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="immigration_status_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label"
                                                        for="edit_immigration_status_name">@lang('app.Name')</label>
                                                    <input type="text" id="edit_immigration_status_name"
                                                        class="form-control" name="name" placeholder="@lang('app.Name')" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                        <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End immigration status Edit Modal -->
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            $('#countries_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('manageMetadata.index') }}",
                    data: {
                        type: "countries"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'code',
                        name: 'code',
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=countries_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="countries_delete(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Countries',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Countries',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Countries',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Countries',
                                orientation: 'landscape',
                                pageSize: 'LEGAL',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Countries',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#countries_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#countries div.head-label').html('<h6 class="mb-0">List of Countries</h6>');

            // Store countries
            $("#countries_add_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'countries');
                $.ajax({
                    url: "{{ route('manageMetadata.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#countries_add_modal").modal("hide");
                            document.getElementById("countries_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#countries_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Country has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // countries Update
            $("#countries_edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'countries');
                $.ajax({
                    url: "{{ url('manageMetadata') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#countries_edit_modal").modal("hide");
                            document.getElementById("countries_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#countries_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Country has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            // Store provinces
            $("#provinces_add_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'provinces');
                $.ajax({
                    url: "{{ route('manageMetadata.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#provinces_add_modal").modal("hide");
                            document.getElementById("provinces_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#provinces_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Province has been Added Successfully!'
                            })
                        }

                    }
                });
            });

            // provinces Update
            $("#provinces_edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'provinces');
                $.ajax({
                    url: "{{ url('manageMetadata') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#provinces_edit_modal").modal("hide");
                            document.getElementById("provinces_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#provinces_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Province has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
            // Store currency types
            $("#currency_types_add_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'currency_types');
                $.ajax({
                    url: "{{ route('manageMetadata.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#currency_types_add_modal").modal("hide");
                            document.getElementById("currency_types_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#currency_types_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Currency Type has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // currency types Update
            $("#currency_types_edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'currency_types');
                $.ajax({
                    url: "{{ url('manageMetadata') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#currency_types_edit_modal").modal("hide");
                            document.getElementById("currency_types_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#currency_types_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Currency Type has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
            // Store nationality
            $("#nationality_add_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'nationality');
                $.ajax({
                    url: "{{ route('manageMetadata.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#nationality_add_modal").modal("hide");
                            document.getElementById("nationality_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#nationality_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Nationality has been Added Successfully!'
                            })
                        }

                    }
                });
            });

            // nationality Update
            $("#nationality_edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'nationality');
                $.ajax({
                    url: "{{ url('manageMetadata') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#nationality_edit_modal").modal("hide");
                            document.getElementById("nationality_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#nationality_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Nationality has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
            // Store ethnicity
            $("#ethnicity_add_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'ethnicity');
                $.ajax({
                    url: "{{ route('manageMetadata.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#ethnicity_add_modal").modal("hide");
                            document.getElementById("ethnicity_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#ethnicity_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Ethnicity has been Added Successfully!'
                            })
                        }

                    }
                });
            });

            // ethnicity Update
            $("#ethnicity_edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'ethnicity');
                $.ajax({
                    url: "{{ url('manageMetadata') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#ethnicity_edit_modal").modal("hide");
                            document.getElementById("ethnicity_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#ethnicity_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Ethnicity has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
            // Store immigration status
            $("#immigration_status_add_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'immigration_status');
                $.ajax({
                    url: "{{ route('manageMetadata.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#immigration_status_add_modal").modal("hide");
                            document.getElementById("immigration_status_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#immigration_status_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Immigration Status has been Added Successfully!'
                            })
                        }

                    }
                });
            });

            // immigration_status Update
            $("#immigration_status_edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'immigration_status');
                $.ajax({
                    url: "{{ url('manageMetadata') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#immigration_status_edit_modal").modal("hide");
                            document.getElementById("immigration_status_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#immigration_status_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Immigration Status has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
        });

        // provinces DataTable
        function provinces() {
            $('#provinces_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('manageMetadata.index') }}",
                    data: {
                        type: "provinces"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'code',
                        name: 'province.code',
                    },
                    {
                        data: 'name',
                        name: 'province.name',
                    },
                    {
                        data: 'country',
                        name: 'country.name',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=provinces_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="provinces_delete(' + full.id + ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [2, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Provinces',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Provinces',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Provinces',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Provinces',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Provinces',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#provinces_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#provinces div.head-label').html('<h6 class="mb-0">List of Provinces</h6>');
        }
        // currency types DataTable
        function currency_types() {
            $('#currency_types_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('manageMetadata.index') }}",
                    data: {
                        type: "currency_types"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'code',
                        name: 'code',
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=currency_types_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="currency_types_delete(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [2, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Currency Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Currency Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Currency Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Currency Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Currency Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#currency_types_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#currency-types div.head-label').html('<h6 class="mb-0">List of Currency Types</h6>');
        }
        // nationality DataTable
        function nationality() {
            $('#nationality_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('manageMetadata.index') }}",
                    data: {
                        type: "nationality"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=nationality_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="nationality_delete(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [2, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Nationality',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Nationality',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Nationality',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Nationality',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Nationality',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#nationality_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#nationality div.head-label').html('<h6 class="mb-0">List of Nationalities</h6>');
        }
        // ethnicity DataTable
        function ethnicity() {
            $('#ethnicity_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('manageMetadata.index') }}",
                    data: {
                        type: "ethnicity"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=ethnicity_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="ethnicity_delete(' + full.id + ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [2, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Ethnicity',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Ethnicity',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Ethnicity',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Ethnicity',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Ethnicity',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#ethnicity_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#ethnicity div.head-label').html('<h6 class="mb-0">List of Ethnicity</h6>');
        }
        // immigration_status DataTable
        function immigration_status() {
            $('#immigration_status_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('manageMetadata.index') }}",
                    data: {
                        type: "immigration_status"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=immigration_status_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="immigration_status_delete(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [2, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Immigration Status',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Immigration Status',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Immigration Status',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Immigration Status',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Immigration Status',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#immigration_status_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#immigration-status div.head-label').html('<h6 class="mb-0">List of Immigration Status</h6>');
        }

        // countries Edit
        function countries_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('manageMetadata') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'countries'
                },
                success: function(response) {
                    $("#edit_country_code").val(response.code);
                    $("#edit_country_name").val(response.name);
                    $("#countries_edit_modal").modal("show");
                },
            });
        }

        // Delete countries
        function countries_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "manageMetadata/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'countries'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#countries_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Country has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // provinces Edit
        function provinces_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('manageMetadata') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'provinces'
                },
                success: function(response) {
                    $("#edit_province_code").val(response.code);
                    $('#edit_province_name').val(response.name);
                    $("#edit_province_country").val(response.country).select2();
                    $("#provinces_edit_modal").modal("show");
                },
            });
        }

        // Delete provinces
        function provinces_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "manageMetadata/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'provinces'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#provinces_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Province has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // currency types Edit
        function currency_types_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('manageMetadata') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'currency_types'
                },
                success: function(response) {
                    $("#edit_currency_code").val(response.code);
                    $('#edit_currency_name').val(response.name);
                    $("#currency_types_edit_modal").modal("show");
                },
            });
        }

        // Delete currency types
        function currency_types_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "manageMetadata/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'currency_types'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#currency_types_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Currency Type has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // nationality Edit
        function nationality_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('manageMetadata') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'nationality'
                },
                success: function(response) {
                    $('#edit_nationality_name').val(response.name);
                    $("#nationality_edit_modal").modal("show");
                },
            });
        }

        // Delete nationality
        function nationality_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "manageMetadata/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'nationality'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#nationality_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Nationality has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // ethnicity Edit
        function ethnicity_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('manageMetadata') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'ethnicity'
                },
                success: function(response) {
                    $('#edit_ethnicity_name').val(response.name);
                    $("#ethnicity_edit_modal").modal("show");
                },
            });
        }

        // Delete ethnicity
        function ethnicity_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "manageMetadata/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'ethnicity'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#ethnicity_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Ethnicity has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // immigration status Edit
        function immigration_status_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('manageMetadata') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'immigration_status'
                },
                success: function(response) {
                    $('#edit_immigration_status_name').val(response.name);
                    $("#immigration_status_edit_modal").modal("show");
                },
            });
        }

        // Delete immigration status
        function immigration_status_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "manageMetadata/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'immigration_status'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#immigration_status_dataTable').DataTable().ajax
                                    .reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Immigration Status has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
