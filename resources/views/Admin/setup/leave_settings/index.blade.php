@extends('Admin.layouts.master')
@section('title', 'Leave Settings')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Leave_Settings')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Home')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Leave_Settings')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Leave_Settings')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="leave_types-tab" data-bs-toggle="tab" href="#leave_types" aria-controls="leave_types" role="tab" aria-selected="true">@lang('app.Leave_Types')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="leave-periods-tab" onclick="leavePeriods()" data-bs-toggle="tab"
                            href="#leave-periods" aria-controls="leave-periods" role="tab" aria-selected="false">@lang('app.Leave_Periods')
                            </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="work-week-tab" onclick="workWeek()" data-bs-toggle="tab"
                            href="#work-week" aria-controls="work-week" role="tab" aria-selected="false">@lang('app.Work_Week')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="holidays-tab" onclick="holidays()" data-bs-toggle="tab"
                            href="#holidays" aria-controls="holidays" role="tab" aria-selected="false">@lang('app.Holidays')</a>
                    </li>
                    @if(isHolidaySettingAllowed())
                        <li class="nav-item">
                            <a class="nav-link" id="holiday-setting-tab" onclick="holidaySettings()" data-bs-toggle="tab"
                                href="#holiday-setting" aria-controls="holiday-setting" role="tab" aria-selected="false">@lang('app.Holiday_Setting')</a>
                        </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" id="leave_rules_tab" onclick="leaveRules()" data-bs-toggle="tab"
                            href="#leave_rules" aria-controls="leave_rules" role="tab" aria-selected="false">@lang('app.Leave_Rules')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="paid-time-off-tab" onclick="paidTimeOff()" data-bs-toggle="tab"
                            href="#paid-time-off" aria-controls="paid-time-off" role="tab" aria-selected="false">@lang('app.Paid_Time_Off')
                            </a>
                    </li>
                    <li class="dropdown nav-item" data-menu="dropdown"><a
                            class="dropdown-item d-flex align-items-center dropdown-toggle" href="#"
                            data-bs-toggle="dropdown"><span data-i18n="Invoice">@lang('app.Leave_Group')</span></a>
                        <ul class="dropdown-menu" data-bs-popper="none">
                            <li data-menu=""><a class="dropdown-item d-flex align-items-center" id="edit-leave-group-tab"
                                    onclick="editLeaveGroup()" href="#edit-leave-group" data-bs-toggle="tab"
                                    aria-controls="edit-leave-group" role="tab"><span>@lang('app.Edit_Leave_Group')</span></a>
                            </li>
                            <li data-menu=""><a class="dropdown-item d-flex align-items-center"
                                    id="leave-group-employee-tab" onclick="editLeaveEmployee()" href="#leave-group-employee"
                                    data-bs-toggle="tab" aria-controls="leave-group-employee" role="tab"><span>@lang('app.Leave_Group_Employee')
                                        </span></a>
                            </li>
                            <li data-menu=""><a class="dropdown-item d-flex align-items-center"
                                id="all-leave-group-employee-tab" onclick="AllLeaveEmployee()" href="#all-leave-group-employee"
                                data-bs-toggle="tab" aria-controls="all-leave-group-employee" role="tab"><span>@lang('app.Leave_Group_For_All_Employee')</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="special-day-off-tab" onclick="SpecialDays()" data-bs-toggle="tab"
                            href="#special-day-off" aria-controls="special-day-off" role="tab" aria-selected="false">@lang('app.Special_Day')</a>
                    </li>
                    @if(env('COMPANY') == 'JSML')
                        <li class="nav-item">
                            <a class="nav-link" id="leave-type-setting-tab" onclick="leaveTypesetting()" data-bs-toggle="tab"
                                href="#leave-type-setting" aria-controls="leave-type-setting" role="tab" aria-selected="false">@lang('app.leave_type_setting')</a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content">
                    {{-- Leave Types --}}
                    <div class="tab-pane active" id="leave_types" aria-labelledby="leave_types-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="leave_types_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Leave Name</th>
                                                    <th>Leave Abbreviation</th>
                                                    <th>Leave Accrue Enabled</th>
                                                    <th>Leave Carried Forward</th>
                                                    <th>Leaves Per Year</th>
                                                    <th>Leaves Group</th>
                                                    <th>Leaves Color</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Leave Types Add Modal --}}
                        <div class="modal fade text-start" id="leave_types_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Leave_Types')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_types_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="title">@lang('app.Leave_Name')</label>
                                                        <input type="text" id="title" class="form-control" placeholder="@lang('app.Leave_Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="abbreviation">@lang('app.Leave_Abbreviation')</label>
                                                        <input type="text" id="abbreviation" class="form-control" placeholder="@lang('app.Leave_Abbreviation')" name="abbreviation" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="per_period">@lang('app.Leaves_Per_Leave_Period')</label>
                                                        <input type="text" id="per_period" class="form-control" placeholder="@lang('app.Default_Per_Year')" name="default_per_year" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="admin_assign">@lang('app.Admin_Can_Assign_Leave_To_Employee')</label>
                                                        <select name="supervisor_leave_assign" id="admin_assign" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employee_can_apply">@lang('app.Employee_Can_Apply_For_Leave')
                                                            </label>
                                                        <select name="employee_can_apply" id="employee_can_apply" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="apply_beyond_current">@lang('app.Employees_Can_Apply_Beyond_The_Current_Leave_Balance')</label>
                                                        <select name="apply_beyond_current" id="apply_beyond_current" class="select2 form-select" required>
                                                            <option value="No">@lang('app.No')</option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_accrue">@lang('app.Leave_Accrue_Enabled')</label>
                                                        <select name="leave_accrue" id="leave_accrue" class="select2 form-select" required>
                                                            <option value="No">@lang('app.No')</option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="carried_forward">@lang('app.Leave_Carried_Forward*')</label>
                                                        <select name="carried_forward" id="carried_forward"
                                                            class="select2 form-select" required>
                                                            <option value="No">@lang('app.No')</option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="more_quota">@lang('app.Leaves_More_Than_Quota')</label>
                                                        <select name="more_quota" id="more_quota"
                                                            class="select2 form-select" required>
                                                            <option value="No">@lang('app.No')</option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="carried_forward_percentage">@lang('app.Percentage_Leave_Carried_Forward')
                                                            </label>
                                                        <input type="text" id="carried_forward_percentage" class="form-control"
                                                            placeholder="@lang('app.Percentage_Carried_Forward')"
                                                            name="carried_forward_percentage" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="max_carried_forward_amount">@lang('app.Maximum_Carried_Forward_Amount')
                                                            </label>
                                                        <input type="text" id="max_carried_forward_amount" class="form-control"
                                                            placeholder="@lang('app.Maximum_Carried_Forward')"
                                                            name="max_carried_forward_amount" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="carried_forward_leave">@lang('app.Carried_Forward_Leave_Availability_Period*')</label>
                                                        <select name="carried_forward_leave_availability" id="add_carried_forward_leave" data-placeholder="Select period" class="select2 form-select" required>
                                                            <option disabled value=""></option>
                                                            <option value="1">1 Month</option>
                                                            <option value="2">2 Month</option>
                                                            <option value="3">3 Month</option>
                                                            <option value="4">4 Month</option>
                                                            <option value="5">5 Month</option>
                                                            <option value="6">6 Month</option>
                                                            <option value="7">7 Month</option>
                                                            <option value="8">8 Month</option>
                                                            <option value="9">9 Month</option>
                                                            <option value="10">10 Month</option>
                                                            <option value="11">11 Month</option>
                                                            <option value="12">12 Month</option>
                                                            <option value="13">13 Month</option>
                                                            <option value="14">14 Month</option>
                                                            <option value="15">15 Month</option>
                                                            <option value="16">16 Month</option>
                                                            <option value="17">17 Month</option>
                                                            <option value="18">18 Month</option>
                                                            <option value="19">19 Month</option>
                                                            <option value="20">20 Month</option>
                                                            <option value="21">21 Month</option>
                                                            <option value="22">22 Month</option>
                                                            <option value="23">23 Month</option>
                                                            <option value="24">24 Month</option>
                                                            <option value="25">25 Month</option>
                                                            <option value="26">26 Month</option>
                                                            <option value="27">27 Month</option>
                                                            <option value="28">28 Month</option>
                                                            <option value="29">29 Month</option>
                                                            <option value="30">30 Month</option>
                                                            <option value="31">31 Month</option>
                                                            <option value="32">32 Month</option>
                                                            <option value="33">33 Month</option>
                                                            <option value="34">34 Month</option>
                                                            <option value="35">35 Month</option>
                                                            <option value="36">36 Month</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="propotionate_on_joined_date">@lang('app.Porpotionate_leaves_on_Joined_Date')</label>
                                                        <select name="propotionate_on_joined_date" id="propotionate_on_joined_date"
                                                            class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="send_notification_emails">@lang('app.Send_Notification_Emails')
                                                            </label>
                                                        <select name="send_notification_emails" id="send_notification_emails"
                                                            class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_group">@lang('app.Leave_Group')</label>
                                                        <select name="leave_group" id="leave_group_period" class="select2  form-select" data-placeholder="@lang('app.Select_Leave_Group')" required>
                                                            <option value="">@lang('app.Select_Leave_Group')</option>
                                                            @foreach ($leaveGroups as $leaveGroup)
                                                                <option value="{{ $leaveGroup->id }}">
                                                                    {{ $leaveGroup->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_color">@lang('app.Leave_Color')</label>
                                                        <select name="leave_color" id="leave_color" class="select2 form-select" data-placeholder="@lang('app.Select_Leave_Color')" required>
                                                            <option value="">@lang('app.Select_Leave_Color')</option>
                                                            <option value="Red">Red</option>
                                                            <option value="Green">Green</option>
                                                            <option value="Blue">Blue</option>
                                                            <option value="Orange">Orange</option>
                                                            <option value="Yellow">Yellow</option>
                                                            <option value="Pink">Pink</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_color">@lang('app.Leave_not_Allowed_for_no_of_Days_after_Joining')</label>
                                                        <input type="text" id="joining_date_allow" class="form-control mr-2" placeholder="@lang('app.Enter_days')" name="joining_date_allow" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_color">@lang('app.Maximum_no_of_Leaves_Allowed_per_Month')</label>
                                                        <input type="text" id="allow_leaves_per_month" class="form-control" placeholder="@lang('app.Enter_days')" name="allow_leaves_per_month" />
                                                    </div>
                                                </div>
                                                {{-- <div class="col-md-12 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 d-flex align-items-center">
                                                                <small>Leave not Allowed for&nbsp;</small>
                                                                <input type="text" id="joining_date_allow" style="width: 60px" class="form-control mr-2" placeholder="days" name="joining_date_allow" required />
                                                                <small>&nbsp;no. of Days after Joining.</small>
                                                                <small>&nbsp;Maximum no. of Leaves Allowed per Month&nbsp;</small>
                                                                <input type="text" id="allow_leaves_per_month" style="width: 60px" class="form-control" placeholder="days" name="allow_leaves_per_month" required />
                                                                <small>.</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check"> 
                                                            <input class="form-check-input"  type="radio" id="status" name="leavetype_status" value="1">
                                                            <label class="form-check-label" for="Yes">@lang('app.Active')</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" id="status" name="leavetype_status" value="0">
                                                            <label class="form-check-label" for="Yes">@lang('app.In_Active')</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check">
                                                            <label for="allow">@lang('app.Allow_Leaves'):</label>
                                                            <input type="checkbox" value="allow" name="allow">
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <div class="col-12">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" id="sandwich_leave" name="sandwich_leave" value="1">
                                                        <label class="form-check-label">Sandwich Leave</label>
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger"
                                                data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Leave Types Edit Modal --}}
                        <div class="modal fade text-start" id="leave_types_edit_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Leave_Types')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_types_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="name">@lang('app.Leave_Name')</label>
                                                        <input type="text" id="edit_name" class="form-control" placeholder="@lang('app.Leave_Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_abbreviation">@lang('app.Leave_Abbreviation')</label>
                                                        <input type="text" id="edit_abbreviation" class="form-control" placeholder="@lang('app.Leave_Abbreviation')" name="abbreviation" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_default_per_year">@lang('app.Leaves_Per_Leave_Period')</label>
                                                        <input type="text" id="edit_default_per_year" class="form-control" placeholder="@lang('app.Default_Per_Year')" name="default_per_year" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_supervisor_leave_assign">@lang('app.Admin_Can_Assign_Leave_To_Employee')</label>
                                                        <select name="supervisor_leave_assign" id="edit_supervisor_leave_assign" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_employee_can_apply">@lang('app.Employee_Can_Apply_For_Leave')</label>
                                                        <select name="employee_can_apply" data-placeholder="@lang('app.Employee_Can_Apply_For_Leave')" id="edit_employee_can_apply" class="select2 form-select" required>
                                                            <option value=""></option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_apply_beyond_current">@lang('app.Employees_Can_Apply_Beyond_The_Current_Leave_Balance')</label>
                                                        <select name="apply_beyond_current" id="edit_apply_beyond_current" class="select2 form-select" required>
                                                            <option value="No">@lang('ap.No')</option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_accrue">@lang('app.Leave_Accrue_Enabled')</label>
                                                        <select name="leave_accrue" id="edit_leave_accrue" class="select2 form-select" required>
                                                            <option value="No">@lang('app.No')</option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_carried_forward">@lang('app.Leave_Carried_Forward*')</label>
                                                        <select name="carried_forward" id="edit_carried_forward" class="select2 form-select" required>
                                                            <option value="No">@lang('app.No')</option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_more_quota">@lang('app.Leaves_More_Than_Quota')</label>
                                                        <select name="more_quota" id="edit_more_quota"
                                                            class="select2 form-select" required>
                                                            <option value="No">@lang('app.No')</option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_carried_forward_percentage">@lang('app.Percentage_Leave_Carried_Forward')
                                                            </label>
                                                        <input type="text" id="edit_carried_forward_percentage" class="form-control" placeholder="@lang('app.Percentage_Leave_Carried_Forward')" name="carried_forward_percentage" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_max_carried_forward_amount">@lang('app.Maximum_Carried_Forward_Amount')</label>
                                                        <input type="text" id="edit_max_carried_forward_amount" class="form-control" placeholder="@lang('app.Maximum_Carried_Forward_Amount')" name="max_carried_forward_amount" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_carried_forward_leave">@lang('app.Carried_Forward_Leave_Availability_Period*')
                                                            </label>
                                                        <select name="carried_forward_leave_availability" data-placeholder="select-month" id="edit_carried_forward_leave" class="select2 form-select" required>
                                                            <option disabled value=""></option>
                                                            <option value="1">1 Month</option>
                                                            <option value="2">2 Month</option>
                                                            <option value="3">3 Month</option>
                                                            <option value="4">4 Month</option>
                                                            <option value="5">5 Month</option>
                                                            <option value="6">6 Month</option>
                                                            <option value="7">7 Month</option>
                                                            <option value="8">8 Month</option>
                                                            <option value="9">9 Month</option>
                                                            <option value="10">10 Month</option>
                                                            <option value="11">11 Month</option>
                                                            <option value="12">12 Month</option>
                                                            <option value="13">13 Month</option>
                                                            <option value="14">14 Month</option>
                                                            <option value="15">15 Month</option>
                                                            <option value="16">16 Month</option>
                                                            <option value="17">17 Month</option>
                                                            <option value="18">18 Month</option>
                                                            <option value="19">19 Month</option>
                                                            <option value="20">20 Month</option>
                                                            <option value="21">21 Month</option>
                                                            <option value="22">22 Month</option>
                                                            <option value="23">23 Month</option>
                                                            <option value="24">24 Month</option>
                                                            <option value="25">25 Month</option>
                                                            <option value="26">26 Month</option>
                                                            <option value="27">27 Month</option>
                                                            <option value="28">28 Month</option>
                                                            <option value="29">29 Month</option>
                                                            <option value="30">30 Month</option>
                                                            <option value="31">31 Month</option>
                                                            <option value="32">32 Month</option>
                                                            <option value="33">33 Month</option>
                                                            <option value="34">34 Month</option>
                                                            <option value="35">35 Month</option>
                                                            <option value="36">36 Month</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_propotionate_on_joined_date">@lang('app.Porpotionate_leaves_on_Joined_Date')
                                                            </label>
                                                        <select name="propotionate_on_joined_date" id="edit_propotionate_on_joined_date" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_send_notification_emails">@lang('app.Send_Notification_Emails')</label>
                                                        <select name="send_notification_emails" id="edit_send_notification_emails" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_group">@lang('app.Leave_Group')</label>
                                                        <select name="leave_group" id="edit_leave_group" class="form-select select2"
                                                            data-placeholder="@lang('app.Select_Leave_Group')" required>
                                                            <option value="">@lang('app.Select_Leave_Group')</option>
                                                            @foreach ($leaveGroups as $leaveGroup)
                                                                <option value="{{ $leaveGroup->id }}">
                                                                    {{ $leaveGroup->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_color">@lang('app.Leave_Color')</label>
                                                        <select name="leave_color" id="edit_leave_color" class="select2 form-select" data-placeholder="@lang('app.Select_Leave_Color')" required>
                                                            <option value="">@lang('app.Select_Leave_Color')</option>
                                                            <option value="Red">Red</option>
                                                            <option value="Green">Green</option>
                                                            <option value="Blue">Blue</option>
                                                            <option value="Orange">Orange</option>
                                                            <option value="Yellow">Yellow</option>
                                                            <option value="Pink">Pink</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_color">@lang('app.Leave_not_Allowed_for_no_of_Days_after_Joining')</label>
                                                        <input type="text" id="joining_date_allow_edit" class="form-control mr-2" placeholder="@lang('app.Enter_days')" name="joining_date_allow" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_color">@lang('app.Maximum_no_of_Leaves_Allowed_per_Month')</label>
                                                        <input type="text" id="allow_leaves_per_month_edit" class="form-control" placeholder="@lang('app.Enter_days')" name="allow_leaves_per_month" required />
                                                    </div>
                                                </div>
                                                {{-- <div class="col-md-12 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 d-flex align-items-center">
                                                                <small>Leave not Allowed for&nbsp;</small>
                                                                <input type="text" id="joining_date_allow_edit" style="width: 60px" class="form-control mr-2" placeholder="days" name="joining_date_allow" required />
                                                                <small>&nbsp;no. of Days after Joining.</small>
                                                                <small>&nbsp;Maximum no. of Leaves Allowed per Month&nbsp;</small>
                                                                <input type="text" id="allow_leaves_per_month_edit" style="width: 60px" class="form-control" placeholder="days" name="allow_leaves_per_month" required />
                                                                <small>.</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check"> 
                                                            <input class="form-check-input"  type="radio" id="edit_status" name="leavetype_status" value="1">
                                                            <label class="form-check-label" for="Yes">@lang('app.Active')</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" id="edit_status_1" name="leavetype_status" value="0">
                                                            <label class="form-check-label" for="Yes">@lang('app.In_Active')</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check">
                                                            <label for="allow">@lang('app.Allow_Negative_Leaves'):</label>
                                                            <input type="checkbox" id="edit_allow" name="allow" value="allow">
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <div class="col-12">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" id="edit_sandwich_leave" name="sandwich_leave" value="1">
                                                        <label class="form-check-label">Sandwich Leave</label>
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger"
                                                data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Leave Periods --}}
                    <div class="tab-pane" id="leave-periods" aria-labelledby="leave-periods-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="leave-periods_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th>Period Start</th>
                                                    <th>Period End</th>
                                                    <th>Status</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Leave Periods Add Modal --}}
                        <div class="modal fade text-start" id="leave_periods_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Leave_Periods')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_periods_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="title">@lang('app.Name')</label>
                                                        <input type="text" id="title" class="form-control"
                                                            placeholder="@lang('app.Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_period_status">@lang('app.Status')</label>
                                                        <select name="status" id="leave_period_status" class="select2 form-select" data-placeholder="@lang('app.Select_Status')" required>
                                                            <option value=""></option>
                                                            <option value="Active">@lang('app.Active')</option>
                                                            <option value="Inactive">@lang('app.In_Active')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="date_start">@lang('app.Period_Start')</label>
                                                        <input type="date" name="date_start" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="date_end">@lang('app.Period_End')</label>
                                                        <input type="date" name="date_end" id="date_end" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Leave Periods Edit Modal --}}
                        <div class="modal fade text-start" id="leave_periods_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Leave_Periods')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_periods_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="name">@lang('app.Name')</label>
                                                        <input type="text" id="leave_periods_name" placeholder="@lang('app.Name')"
                                                            class="form-control" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_period_status">@lang('app.Status')</label>
                                                        <select name="status" id="edit_leave_period_status" class="select2 form-select" data-placeholder="@lang('app.Select_Status')" required>
                                                            <option value=""></option>
                                                            <option value="Active">@lang('app.Active')</option>
                                                            <option value="Inactive">@lang('app.In_Active')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_date_start">@lang('app.Period_Start')
                                                            </label>
                                                        <input type="date" id="edit_date_start" class="form-control flatpickr-basic" name="date_start" placeholder="YYYY-MM-DD" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_date_end">@lang('app.Period_End')</label>
                                                        <input type="date" id="edit_date_end" class="form-control flatpickr-basic" name="date_end" placeholder="YYYY-MM-DD" required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger"
                                                data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Work Week --}}
                    <div class="tab-pane" id="work-week" role="tabpanel" aria-labelledby="work-week-tab">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="work_week_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Description</th>
                                                    <th>Off Days</th>
                                                    <th class="not_include"></th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Work Week Add Modal --}}
                        <div class="modal fade text-start" id="work_week_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Work_Week')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="work_week_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="work_week_name">@lang('app.Work_Week_Description')</label>
                                                        <input type="text" id="work_week_name" class="form-control"
                                                            placeholder="@lang('app.Description')" name="work_week_name" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">@lang('app.Select_Off_Days')</label>
                                                        <hr>
                                                        @foreach ($weekDays as $key => $day)
                                                        <div class="form-check">
                                                            <input type="checkbox" class="form-check-input" id="{{$day}}" name="off_days[]" value="{{$key}}">
                                                            <label class="form-check-label" for="{{$day}}">{{$day}}</label>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check"> 
                                                            <input class="form-check-input"  type="radio" id="status" name="status" value="1">
                                                            <label class="form-check-label" for="Yes">@lang('app.Active')</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" id="status_1" name="status" value="2">
                                                            <label class="form-check-label" for="Yes">@lang('app.In_Active')</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger"
                                                data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Work Week Edit Modal --}}
                        <div class="modal fade text-start" id="work_week_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Work_Week')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="work_week_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_work_week">@lang('app.Work_Week_Description')</label>
                                                        <input type="text" id="edit_work_week" class="form-control" placeholder="@lang('app.Description')" name="work_week_name" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" >@lang('app.Select_Off_Days')</label>
                                                        <hr>
                                                        <div id="append_days"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check"> 
                                                            <input class="form-check-input"  type="radio" id="edit_status_2" name="status" value="1">
                                                            <label class="form-check-label" for="Yes">@lang('app.Active')</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" id="edit_status_3" name="status" value="2">
                                                            <label class="form-check-label" for="Yes">@lang('app.In_Active')</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger"
                                                data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Holidays --}}
                    <div class="tab-pane" id="holidays" aria-labelledby="holidays-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="holidays_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th>Date</th>
                                                    <th>Status</th>
                                                    <th>Country</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Holidays Add Modal --}}
                        <div class="modal fade text-start" id="holidays_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Holidays')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="holidays_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="title">@lang('app.Name')</label>
                                                        <input type="text" id="title" class="form-control"
                                                            placeholder="@lang('app.Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="date">@lang('app.Date')</label>
                                                        <input type="date" id="date" class="form-control flatpickr-basic" name="dateh" placeholder="YYYY-MM-DD" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="status">@lang('app.Status')</label>
                                                        <select name="status" id="status" class="select2 form-select" data-placeholder="@lang('app.Select_Status')" required>
                                                            <option value="">@lang('app.Select_Status')</option>
                                                            <option value="Full Day">@lang('app.Full_Day')</option>
                                                            <option value="Half Day">@lang('app.Half_Day')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="country">@lang('app.Country')</label>
                                                        <select name="country" id="country" class="select2 form-select" data-placeholder="@lang('app.Select_Country')" required>
                                                            <option value="">@lang('app.Select_Country')</option>
                                                            @foreach ($countries as $country)
                                                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Holidays Edit Modal --}}
                        <div class="modal fade text-start" id="holidays_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Holidays')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="holidays_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="holiday_name">@lang('app.Name')</label>
                                                        <input type="text" id="holiday_name" class="form-control"
                                                            placeholder="@lang('app.Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_dateh">@lang('app.Date')</label>
                                                        <input type="date" id="edit_dateh" class="form-control flatpickr-basic" name="dateh" placeholder="YYYY-MM-DD" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_status">@lang('app.Status')</label>
                                                        <select name="status" id="edit_status" class="select2 form-select" data-placeholder="@lang('app.Select_Status')" required>
                                                            <option value="">@lang('app.Select_Status')</option>
                                                            <option value="Full Day">@lang('app.Full_Day')</option>
                                                            <option value="Half Day">@lang('app.Half_Day')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_country">@lang('app.Country')</label>
                                                        <select name="country" id="edit_country" class="form-select select2" data-placeholder="@lang('app.Select_Country')" required>
                                                            <option value=""></option>
                                                            @foreach ($countries as $country)
                                                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Leave Rules --}}
                    <div class="tab-pane" id="leave_rules" aria-labelledby="leave_rules_tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="leave_rules_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Leave Type</th>
                                                    <th>Leave Group</th>
                                                    <th>Job Title</th>
                                                    <th>Employement Status</th>
                                                    <th>Employee</th>
                                                    <th>Experience (Days)</th>
                                                    <th>Leaves Per Year</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Leave Rules Add Modal --}}
                        <div class="modal fade text-start" id="leave_rules_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Leave_Rules')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_rules_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="rule_leave_type">@lang('app.Leave_Types')</label>
                                                        <select name="leave_type" id="rule_leave_type" class="select2 form-select"
                                                            data-placeholder="@lang('app.Select_Leave_Type')" required>
                                                            <option value="">@lang('app.Select_Leave_Type')</option>
                                                            @foreach ($leaveTypes as $leaveType)
                                                                <option value="{{ $leaveType->id }}">
                                                                    {{ $leaveType->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_group">@lang('app.Leave_Group')
                                                            </label>
                                                        <select name="leave_group" id="leave_group" class="select2 form-select"
                                                            data-placeholder="@lang('app.Select_Leave_Group')" required>
                                                            <option value="">@lang('app.Select_Leave_Group')</option>
                                                            @foreach ($leaveGroups as $leaveGroup)
                                                                <option value="{{ $leaveGroup->id }}">
                                                                    {{ $leaveGroup->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="job_title">@lang('app.Job_Titles')</label>
                                                        <select name="job_title" id="job_title" class="select2 form-select"
                                                            data-placeholder="@lang('app.Select_Job_Title')" required>
                                                            <option value="">@lang('app.Select_Job_Title')</option>
                                                            @foreach ($jobTitles as $jobTitle)
                                                                <option value="{{ $jobTitle->id }}">
                                                                    {{ $jobTitle->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employment_status">@lang('app.Employment_Status')
                                                            </label>
                                                        <select name="employment_status" id="employment_status"
                                                            class="select2 form-select" data-placeholder="@lang('app.Select_Employment_Status')" required>
                                                            <option value="">@lang('app.Select_Employment_Status')</option>
                                                            @foreach ($employmentStatus as $item)
                                                                <option value="{{ $item->id }}">
                                                                    {{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="rule_employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="rule_employee" class="select2 form-select"
                                                            data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value="">@lang('app.Select_Employee')</option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="title">@lang('app.Required_Experienced_Days')
                                                            </label>
                                                        <input type="number" id="title" class="form-control"
                                                            placeholder="@lang('app.Experienced_Days')" name="exp_days"
                                                            required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="title">@lang('app.Leaves_Per_Leave_Period')
                                                            </label>
                                                        <input type="number" id="title" class="form-control"
                                                            placeholder="@lang('app.Leaves_Per_Leave_Period')"
                                                            name="default_per_year" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="supervisor_leave_assign">@lang('app.Admin_Can_Assign_Leave_To_Employees')
                                                            </label>
                                                        <select name="supervisor_leave_assign" id="supervisor_leave_assign"
                                                            class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employee_can_apply">@lang('app.Employees_Can_Apply_For_This_Leave_Type')
                                                            </label>
                                                        <select name="employee_can_apply" id="employee_can_apply"
                                                            class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="apply_beyond_current">@lang('app.Employees_Can_Apply_Beyond_The_Current_Leave_Balance')
                                                            </label>
                                                        <select name="apply_beyond_current" id="apply_beyond_current"
                                                            class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_accrue">@lang('app.Leave_Accrue_Enabled')
                                                            </label>
                                                        <select name="leave_accrue" id="leave_accrue"
                                                            class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="carried_forward">@lang('app.Leave_Carried_Forward*')
                                                            </label>
                                                        <select name="carried_forward" id="carried_forward"
                                                            class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="type">@lang('app.Percentage_Leave_Carried_Forward')
                                                            </label>
                                                        <input type="text" id="type" class="form-control"
                                                            placeholder="@lang('app.Percentage_Leave_Carried_Forward')"
                                                            name="carried_forward_percentage" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="type">@lang('app.Maximum_Carried_Forward_Amount')
                                                            </label>
                                                        <input type="text" id="type" class="form-control"
                                                            placeholder="@lang('app.Maximum_Carried_Forward_Amount')"
                                                            name="max_carried_forward_amount" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="carried_forward_leave_availability">@lang('app.Carried_Forward_Leave_Availability_Period*')
                                                            </label>
                                                        <select name="carried_forward_leave_availability"
                                                            id="carried_forward_leave_availability" class="select2 form-select"
                                                            data-placeholder="@lang('app.Select_Availibilty_Period')" required>
                                                            <option value="">@lang('app.Select_Availibilty_Period')</option>
                                                            <option value="1">1 Month</option>
                                                            <option value="2">2 Month</option>
                                                            <option value="3">3 Month</option>
                                                            <option value="4">4 Month</option>
                                                            <option value="5">5 Month</option>
                                                            <option value="6">6 Month</option>
                                                            <option value="7">7 Month</option>
                                                            <option value="8">8 Month</option>
                                                            <option value="9">9 Month</option>
                                                            <option value="10">10 Month</option>
                                                            <option value="11">11 Month</option>
                                                            <option value="12">12 Month</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="type">@lang('app.Porpotionate_leaves_on_Joined_Date')
                                                            </label>
                                                        <select name="propotionate_on_joined_date" id="type"
                                                            class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- Leave Rules Edit Modal --}}
                        <div class="modal fade text-start" id="leave_rules_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Leave_Rules')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_rules_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_leave_type">@lang('app.Leave_Types')</label>
                                                        <select name="leave_type" id="leave_rules_leave_type"
                                                            class="form-select select2" data-placeholder="@lang('app.Select_Leave_Type')" required>
                                                            <option value="">@lang('app.Select_Leave_Type')</option>
                                                            @foreach ($leaveTypes as $leaveType)
                                                                <option value="{{ $leaveType->id }}">
                                                                    {{ $leaveType->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_leave_group">@lang('app.Leave_Group')</label>
                                                        <select name="leave_group" id="leave_rules_leave_group"
                                                            class="form-select select2" data-placeholder="@lang('app.Select_Leave_Group')" required>
                                                            <option value="">@lang('app.Select_Leave_Group')</option>
                                                            @foreach ($leaveGroups as $leaveGroup)
                                                                <option value="{{ $leaveGroup->id }}">
                                                                    {{ $leaveGroup->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_rules_job_title">@lang('app.Job_Titles')
                                                            </label>
                                                        <select name="job_title" id="leave_rules_job_title"
                                                            class="form-select select2" data-placeholder="@lang('app.Select_Job_Title')" required>
                                                            <option value="">@lang('app.Select_Job_Title')</option>
                                                            @foreach ($jobTitles as $jobTitle)
                                                                <option value="{{ $jobTitle->id }}">
                                                                    {{ $jobTitle->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_employment_status">@lang('app.Employment_Status')
                                                            </label>
                                                        <select name="employment_status"
                                                            id="leave_rules_employment_status"
                                                            class="form-select select2" data-placeholder="@lang('app.Select_Employment_Status')" required>
                                                            <option value="">@lang('app.Select_Employment_Status')</option>
                                                            @foreach ($employmentStatus as $item)
                                                                <option value="{{ $item->id }}">
                                                                    {{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="leave_rules_employee"
                                                            class="form-select select2" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value="">@lang('app.Select_Employee')</option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_exp_days">@lang('app.Required_Experienced_Days')
                                                            </label>
                                                        <input type="number" id="leave_rules_exp_days"
                                                            class="form-control" placeholder="@lang('app.Experienced_Days')"
                                                            name="exp_days" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_default_per_year">@lang('app.Leaves_Per_Leave_Period')
                                                            </label>
                                                        <input type="number" id="leave_rules_default_per_year"
                                                            class="form-control"
                                                            placeholder="@lang('app.Leaves_Per_Leave_Period')"
                                                            name="default_per_year" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_supervisor_leave_assign">@lang('app.Admin_Can_Assign_Leave_To_Employees')
                                                            </label>
                                                        <select name="supervisor_leave_assign"
                                                            id="leave_rules_supervisor_leave_assign"
                                                            class="form-select select2" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_employee_can_apply">@lang('app.Employees_Can_Apply_For_This_Leave_Type')
                                                            </label>
                                                        <select name="employee_can_apply"
                                                            id="leave_rules_employee_can_apply"
                                                            class="form-select select2" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_apply_beyond_current">@lang('app.Employees_Can_Apply_Beyond_The_Current_Leave_Balance')
                                                            </label>
                                                        <select name="apply_beyond_current"
                                                            id="leave_rules_apply_beyond_current"
                                                            class="form-select select2" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_leave_accrue">@lang('app.Leave_Accrue_Enabled')
                                                            </label>
                                                        <select name="leave_accrue" id="leave_rules_leave_accrue"
                                                            class="form-select select2" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_carried_forward">@lang('app.Leave_Carried_Forward*')
                                                            </label>
                                                        <select name="carried_forward"
                                                            id="leave_rules_carried_forward"
                                                            class="form-select select2" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_carried_forward_percentage">@lang('app.Percentage_Leave_Carried_Forward')
                                                            </label>
                                                        <input type="text"
                                                            id="leave_rules_carried_forward_percentage"
                                                            class="form-control"
                                                            placeholder="@lang('app.Percentage_Leave_Carried_Forward')"
                                                            name="carried_forward_percentage" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_max_carried_forward_amount">@lang('app.Maximum_Carried_Forward_Amount')
                                                            </label>
                                                        <input type="text"
                                                            id="leave_rules_max_carried_forward_amount"
                                                            class="form-control"
                                                            placeholder="@lang('app.Maximum_Carried_Forward_Amount')"
                                                            name="max_carried_forward_amount" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_carried_forward_leave_availability">@lang('app.Carried_Forward_Leave_Availability_Period*')
                                                            </label>
                                                        <select name="carried_forward_leave_availability"
                                                            id="leave_rules_carried_forward_leave_availability"
                                                            class="form-select select2"
                                                            data-placeholder="@lang('app.Select_Time_Period')" required>
                                                            <option value="">@lang('app.Select_Time_Period')</option>
                                                            <option value="1">1 Month</option>
                                                            <option value="2">2 Month</option>
                                                            <option value="3">3 Month</option>
                                                            <option value="4">4 Month</option>
                                                            <option value="5">5 Month</option>
                                                            <option value="6">6 Month</option>
                                                            <option value="7">7 Month</option>
                                                            <option value="8">8 Month</option>
                                                            <option value="9">9 Month</option>
                                                            <option value="10">10 Month</option>
                                                            <option value="11">11 Month</option>
                                                            <option value="12">12 Month</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="leave_rules_propotionate_on_joined_date">@lang('app.Porpotionate_leaves_on_Joined_Date')
                                                            </label>
                                                        <select name="propotionate_on_joined_date"
                                                            id="leave_rules_propotionate_on_joined_date"
                                                            class="form-select select2" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Paid Time Off --}}
                    <div class="tab-pane" id="paid-time-off" aria-labelledby="paid-time-off-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="paid_time_off_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Leave Type</th>
                                                    <th>Employee</th>
                                                    <th>Leave Periods</th>
                                                    <th>Leave Amount</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Paid Time Off Add Modal --}}
                        <div class="modal fade text-start" id="paid_time_off_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Paid_Time_Off')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="paid_time_off_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_type">@lang('app.Leave_Types')</label>
                                                        <select name="leave_type" id="leave_type" class="form-select select2" data-placeholder="@lang('app.Select_Leave_Type')" required>
                                                            <option value=""></option>
                                                            @foreach ($leaveTypes as $leaveType)
                                                                <option value="{{ $leaveType->id }}">{{ $leaveType->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="employee" class="form-select select2" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_period">@lang('app.Leave_Periods')</label>
                                                        <select name="leave_period" id="leave_period" class="form-select select2" data-placeholder="@lang('app.Select_Leave_Period')" required>
                                                            <option value=""></option>
                                                            @foreach ($leavePeriods as $leavePeriod)
                                                                <option value="{{ $leavePeriod->id }}"> {{ $leavePeriod->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="amount">@lang('app.Amount')</label>
                                                        <input type="text" id="amount" class="form-control"
                                                            placeholder="@lang('app.Amount')" name="amount" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="note">@lang('app.Note')</label>
                                                        <textarea class="form-control" id="note" name="note" rows="3" placeholder="@lang('app.Type_here')"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- Paid Time Off Edit Modal --}}
                        <div class="modal fade text-start" id="paid_time_off_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Paid_Time_Off')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="paid_time_off_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_type">@lang('app.Leave_Types')</label>
                                                        <select name="leave_type" id="edit_leave_type" class="form-select select2" data-placeholder="@lang('app.Select_Leave_Type')" required>
                                                            <option value=""></option>
                                                            @foreach ($leaveTypes as $leaveType)
                                                                <option value="{{ $leaveType->id }}">
                                                                    {{ $leaveType->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="edit_employee" class="form-select select2" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_period">@lang('app.')</label>
                                                        <select name="leave_period" id="edit_leave_period" class="form-select Leave_Periodsselect2" data-placeholder="@lang('app.Select_Leave_Period')" required>
                                                            <option value=""></option>
                                                            @foreach ($leavePeriods as $leavePeriod)
                                                                <option value="{{ $leavePeriod->id }}">{{ $leavePeriod->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_amount">@lang('app.Amount')</label>
                                                        <input type="number" id="edit_amount" class="form-control" placeholder="@lang('app.Amount')" name="amount" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_note">@lang('app.Note')</label>
                                                        <textarea class="form-control" id="edit_note" name="note" rows="3" placeholder="@lang('app.Type_here')"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Special Day Off --}}
                    <div class="tab-pane" id="special-day-off" aria-labelledby="special-day-off-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="special_day_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Employee</th>
                                                    <th>Special Day</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Special Day Add Modal --}}
                        <div class="modal fade text-start" id="special_day_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Paid_Time_Off')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="special_day_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="departmentFilter">@lang('app.Department'):</label>
                                                        <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                            <option value=""></option>
                                                            <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : '' }}>@lang('app.All_Departments')</option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                        <select name="get_employees[]" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" multiple>
                                                            <option value=""></option>
                                                            @if (!empty(request('departmentFilter')))
                                                                @foreach (employees(request('departmentFilter')) as $item)
                                                                <option value="{{$item->id}}" {{$item->id == request('employeeFilter') ? 'selected' : ''}}>{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                                @endforeach
                                                            @endif 
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#employeeFilter')">@lang('app.Select_All')</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#employeeFilter')">@lang('app.Deselect_All')</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="User Call Date">@lang('app.Special_Day_Date')</label>
                                                        <input type="date" id="special_day" class="form-control" placeholder="@lang('app.Special_Day')" name="special_day"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- Special Day Edit Modal --}}
                        <div class="modal fade text-start" id="paid_time_off_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Paid_Time_Off')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="paid_time_off_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_type">@lang('app.Leave_Types')</label>
                                                        <select name="leave_type" id="edit_leave_type" class="form-select select2" data-placeholder="@lang('app.Select_Leave_Type')" required>
                                                            <option value=""></option>
                                                            @foreach ($leaveTypes as $leaveType)
                                                                <option value="{{ $leaveType->id }}">
                                                                    {{ $leaveType->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="edit_employee" class="form-select select2" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_period">@lang('app.Leave_Periods')</label>
                                                        <select name="leave_period" id="edit_leave_period" class="form-select select2" data-placeholder="@lang('app.Select_Leave_Period')" required>
                                                            <option value=""></option>
                                                            @foreach ($leavePeriods as $leavePeriod)
                                                                <option value="{{ $leavePeriod->id }}">{{ $leavePeriod->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_amount">@lang('app.Amount')</label>
                                                        <input type="number" id="edit_amount" class="form-control" placeholder="@lang('app.Amount')" name="amount" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_note">@lang('app.Note')</label>
                                                        <textarea class="form-control" id="edit_note" name="note" rows="3" placeholder="@lang('app.Type_here')"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Leave Type Setting --}}
                    <div class="tab-pane" id="leave-type-setting" aria-labelledby="leave-type-setting-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="leave_type_setting_datatable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Leave Type</th>
                                                    <th>Employee</th>
                                                    <th>Departments</th>
                                                    <th>Sections</th>
                                                    <th>Employement Statuses</th>
                                                    <th>Amount</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Add Modal --}}
                        <div class="modal fade text-start" id="leave_setting_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">Add Leave Setting</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_setting_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Departments:</label>
                                                        <select name="departments[]" class="form-select select2" data-placeholder="Select Departments" id="departments" multiple>
                                                            <option value=""></option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{$department->id}}">{{$department->title}}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#departments')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#departments')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Sections:</label>
                                                        <select name="sections[]" id="sections"
                                                            data-placeholder="@lang('app.Select_Section')"
                                                            class="select2 form-select" multiple></select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#sections')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#sections')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employement_statuses">Employment
                                                            Statuses</label>
                                                        <select name="employement_statuses[]" id="employement_statuses"
                                                            class="select2 form-select" data-placeholder="Select Employment Statuses" multiple>
                                                            <option value=""></option>
                                                            @foreach ($employmentStatus as $item)
                                                                <option value="{{ $item->id }}">
                                                                    {{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#employement_statuses')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#employement_statuses')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_setting_employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="leave_setting_employee" class="form-select select2" data-placeholder="@lang('app.Select_Employee')">
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_type_for_setting">Leave Type</label>
                                                        <select name="leave_type_for_setting" id="leave_type_for_setting" class="form-select select2" data-placeholder="Select Leave Type" required>
                                                            <option value=""></option>
                                                            @foreach ($leaveTypes as $leaveType)
                                                                <option value="{{ $leaveType->id }}">
                                                                    {{ $leaveType->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Amount</label>
                                                        <input type="number" class="form-control" placeholder="Amount" name="amount" step=".5" required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Edit Modal --}}
                        <div class="modal fade text-start" id="leave_setting_edit_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">Update Leave Setting</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_setting_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Departments:</label>
                                                        <select name="departments[]" class="form-select select2" data-placeholder="Select Departments" id="edit_departments" multiple>
                                                            <option value=""></option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{$department->id}}">{{$department->title}}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#edit_departments')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#edit_departments')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Sections:</label>
                                                        <select name="sections[]" id="edit_sections"
                                                            data-placeholder="@lang('app.Select_Section')"
                                                            class="select2 form-select" multiple></select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#edit_sections')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#edit_sections')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_employement_statuses">Employment
                                                            Statuses</label>
                                                        <select name="employement_statuses[]" id="edit_employement_statuses"
                                                            class="select2 form-select" data-placeholder="Select Employment Statuses" multiple>
                                                            <option value=""></option>
                                                            @foreach ($employmentStatus as $item)
                                                                <option value="{{ $item->id }}">
                                                                    {{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#edit_employement_statuses')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#edit_employement_statuses')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_setting_employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="edit_leave_setting_employee" class="form-select select2" data-placeholder="@lang('app.Select_Employee')">
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_type_for_setting_edit">Leave Type</label>
                                                        <select name="leave_type_for_setting" id="leave_type_for_setting_edit" class="form-select select2" data-placeholder="Select Leave Type" required>
                                                            <option value=""></option>
                                                            @foreach ($leaveTypes as $leaveType)
                                                                <option value="{{ $leaveType->id }}">
                                                                    {{ $leaveType->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_amount_for_leave_setting">Amount</label>
                                                        <input type="number" id="edit_amount_for_leave_setting" class="form-control" placeholder="Amount" name="amount" step=".5" required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Holiday Setting --}}
                    <div class="tab-pane" id="holiday-setting" aria-labelledby="holiday-setting-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="holiday_setting_datatable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Holidays</th>
                                                    <th>Departments</th>
                                                    <th>Sections</th>
                                                    <th>Employement Statuses</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Add Modal --}}
                        <div class="modal fade text-start" id="holiday_setting_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">Add Holiday Setting</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="holiday_setting_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Departments:</label>
                                                        <select name="departments[]" class="form-select select2" data-placeholder="Select Departments" id="holiday_setting_departments" multiple>
                                                            <option value=""></option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{$department->id}}">{{$department->title}}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#holiday_setting_departments')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#holiday_setting_departments')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Sections:</label>
                                                        <select name="sections[]" id="holiday_setting_sections"
                                                            data-placeholder="@lang('app.Select_Section')"
                                                            class="select2 form-select" multiple></select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#holiday_setting_sections')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#holiday_setting_sections')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employement_statuses">Employment
                                                            Statuses</label>
                                                        <select name="employement_statuses[]" id="holiday_settings_employement_statuses"
                                                            class="select2 form-select" data-placeholder="Select Employment Statuses" multiple>
                                                            <option value=""></option>
                                                            @foreach ($employmentStatus as $item)
                                                                <option value="{{ $item->id }}">
                                                                    {{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#holiday_settings_employement_statuses')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#holiday_settings_employement_statuses')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="holidays_for_setting">Holidays</label>
                                                        <select name="holidays[]" id="holidays_for_setting" class="form-select select2" data-placeholder="Select Holidays" multiple>
                                                            <option value=""></option>
                                                            @foreach ($holidays as $holiday)
                                                                <option value="{{ $holiday->id }}">
                                                                    {{ $holiday->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#holidays_for_setting')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#holidays_for_setting')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Edit Modal --}}
                        <div class="modal fade text-start" id="holiday_setting_edit_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">Update Holiday Setting</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="holiday_setting_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Departments:</label>
                                                        <select name="departments[]" class="form-select select2" data-placeholder="Select Departments" id="edit_holiday_settings_departments" multiple>
                                                            <option value=""></option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{$department->id}}">{{$department->title}}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#edit_holiday_settings_departments')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#edit_holiday_settings_departments')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Sections:</label>
                                                        <select name="sections[]" id="edit_holiday_settings_sections"
                                                            data-placeholder="@lang('app.Select_Section')"
                                                            class="select2 form-select" multiple></select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#edit_holiday_settings_sections')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#edit_holiday_settings_sections')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_employement_statuses">Employment
                                                            Statuses</label>
                                                        <select name="employement_statuses[]" id="edit_holiday_settings_employement_statuses"
                                                            class="select2 form-select" data-placeholder="Select Employment Statuses" multiple>
                                                            <option value=""></option>
                                                            @foreach ($employmentStatus as $item)
                                                                <option value="{{ $item->id }}">
                                                                    {{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#edit_holiday_settings_employement_statuses')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#edit_holiday_settings_employement_statuses')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="holidays_for_settings_edit">Holidays</label>
                                                        <select name="holidays[]" id="holidays_for_settings_edit" class="form-select select2" data-placeholder="Select Holidays" multiple>
                                                            <option value=""></option>
                                                            @foreach ($holidays as $holiday)
                                                                <option value="{{ $holiday->id }}">
                                                                    {{ $holiday->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#holidays_for_settings_edit')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#holidays_for_settings_edit')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Leave Group => Edit Leave Group --}}
                    <div class="tab-pane" id="edit-leave-group" aria-labelledby="edit-leave-group-tab"
                        role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="edit_leave_group_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th>Details</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Edit Leave Group Add Modal --}}
                        <div class="modal fade text-start" id="edit_leave_group_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Leave_Group')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="edit_leave_group_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_group_name">@lang('app.Name')</label>
                                                        <input type="text" id="leave_group_name" class="form-control" placeholder="@lang('app.Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_group_details">@lang('app.Details')</label>
                                                        <textarea class="form-control" id="leave_group_details" name="details" placeholder="@lang('app.Details')" rows="3" placeholder="Type here..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Edit Leave Group Edit Modal --}}
                        <div class="modal fade text-start" id="edit_leave_group_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Leave_Group')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="edit_leave_group_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_group_name">@lang('app.Name')</label>
                                                        <input type="text" id="edit_leave_group_name" class="form-control"
                                                            placeholder="@lang('app.Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_group_details">@lang('app.Details')</label>
                                                        <textarea class="form-control" id="edit_leave_group_details" name="details" placeholder="@lang('app.Type_here')" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Leave Group => Leave Group Employee --}}
                    <div class="tab-pane" id="leave-group-employee" aria-labelledby="leave-group-employee-tab"
                        role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="leave_group_employee_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th>Leave Group</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                    {{-- Leave Group Employee Add Modal --}}
                        <div class="modal fade text-start" id="leave_group_employee_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Leave_Group_Employee')
                                        </h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_group_employee_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="leave_group_employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="leave_group_employee" class="form-select select2" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="group">@lang('app.Leave_Group')</label>
                                                        <select name="leave_group" id="group" class="form-select select2" data-placeholder="@lang('app.Select_Leave_Group')" required>
                                                            <option value=""></option>
                                                            @foreach ($leaveGroups as $leaveGroup)
                                                                <option value="{{ $leaveGroup->id }}">{{ $leaveGroup->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Leave Group Employee Edit Modal --}}
                        <div class="modal fade text-start" id="leave_group_employee_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Leave_Group_Employee')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_group_employee_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_group_employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="edit_leave_group_employee" class="form-select select2" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_group">@lang('app.Leave_Group')</label>
                                                        <select name="leave_group" id="edit_group" class="form-select select2" data-placeholder="@lang('app.Select_Leave_Group')" required>
                                                            <option value=""></option>
                                                            @foreach ($leaveGroups as $leaveGroup)
                                                                <option value="{{ $leaveGroup->id }}">{{ $leaveGroup->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        {{-- Leave Group => Leave Group Employee --}}
                        <div class="tab-pane" id="all-leave-group-employee" aria-labelledby="all-leave-group-employee-tab"
                        role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="all-leave_group_employee_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Leave Group</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                    {{-- Leave Group Employee Add Modal --}}
                        <div class="modal fade text-start" id="all_leave_group_employee_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Leave_Group_Employee')
                                        </h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="all_leave_group_employee_add_form">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="group">@lang('app.Leave_Group')</label>
                                                        <select name="all_leave_group" id="group" class="form-select select2" data-placeholder="@lang('app.Select_Leave_Group')" required>
                                                            <option value=""></option>
                                                            @foreach ($leaveGroups as $leaveGroup)
                                                                <option value="{{ $leaveGroup->id }}">{{ $leaveGroup->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Leave Group Employee Edit Modal --}}
                        <div class="modal fade text-start" id="leave_group_employee_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Leave_Group_Employee')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="leave_group_employee_edit_form">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_leave_group_employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="edit_leave_group_employee" class="form-select select2" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_group">@lang('app.Leave_Group')</label>
                                                        <select name="leave_group" id="edit_group" class="form-select select2" data-placeholder="@lang('app.Select_Leave_Group')" required>
                                                            <option value=""></option>
                                                            @foreach ($leaveGroups as $leaveGroup)
                                                                <option value="{{ $leaveGroup->id }}">{{ $leaveGroup->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            leave_types_dataTable = $('#leave_types_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "leave_type"
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'leavetypes.name',
                    },
                    {
                        data: 'abbreviation',
                        name: 'leavetypes.abbreviation',
                    },
                    {
                        data: 'leave_accrue',
                        name: 'leavetypes.leave_accrue',
                    },
                    {
                        data: 'carried_forward',
                        name: 'leavetypes.carried_forward',
                    },
                    {
                        data: 'default_per_year' , render: function(data , type , row){
                            return Math.trunc(row.default_per_year);
                        },
                        name: 'leavetypes.default_per_year',
                    },
                    {
                        data: 'leave_group_name',
                        name: 'leavegroups.name',
                    },
                    {
                        data: 'leave_color',
                        name: 'leavetypes.leave_color',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=leave_types_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' 
                                // +
                                // '<a href="javascript:;" onclick=leave_types_delete(' + full.id +
                                // ')>' +
                                // feather.icons['trash-2'].toSvg({
                                //     class: 'font-medium-4 text-danger'
                                // }) +
                                // '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Leave Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Leave Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Leave Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Leave Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Leave Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#leave_types_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#leave_types div.head-label').html('<h6 class="mb-0">List of Leave Types</h6>');

            // Leave Types Add Data
            $("#leave_types_add_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'leave_type');
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#leave_types_add_form')[0].reset();
                            $("#leave_types_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            // $("#add_carried_forward_leave option:selected").removeAttr("selected");
                            // $("#add_carried_forward_leave").select2("val", "");

                            leave_types_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Type has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Leave Types Update Data
            $("#leave_types_edit_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'leave_type');
                $.ajax({
                    url: "{{ url('leave_types') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#leave_types_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            leave_types_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Type has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            // Leave Periods Add Data
            $("#leave_periods_add_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'leave_period');
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } 
                        else if (response.errorMessage) {
                            Toast.fire({
                                icon: 'error',
                                title: response.errorMessage
                            })
                        }
                        else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#leave_periods_add_form')[0].reset();
                            $("#leave_periods_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            leave_periods_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Period has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Leave Periods Update Data
            $("#leave_periods_edit_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'leave_period');
                $.ajax({
                    url: "{{ url('leave_types') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } 
                        else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else if (response.errorMessage) {
                            Toast.fire({
                                icon: 'error',
                                title: response.errorMessage
                            })
                        } else {
                            $("#leave_periods_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            leave_periods_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Periods has been Updated Successfully!'
                            })
                        }

                    }
                });
            })

            // Work Week Add Data
            $('#work_week_add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'work_week');
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#work_week_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            work_week_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Work Week has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Work Week Update Data
            $("#work_week_edit_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'work_week');
                $.ajax({
                    url: "{{ url('leave_types') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#work_week_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            work_week_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Work Week has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
            // Holidays Add Data
            $('#holidays_add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                var formData = new FormData(this);
                formData.append('type', 'holiday')
                e.preventDefault();
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#holidays_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            holidays_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Holiday has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Holidays Update Data
            $('#holidays_edit_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'holiday')
                $.ajax({
                    url: "{{ url('leave_types') }}" + "/" + rowid,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#holidays_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            holidays_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Holiday has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            // Leave Rules Add Data
            $('#leave_rules_add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                var formData = new FormData(this);
                formData.append('type', 'leave_rule');
                e.preventDefault();
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#leave_rules_add_form")[0].reset();
                            $("#leave_rules_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            leave_rules_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Rules has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Leave Rules Update Data
            $("#leave_rules_edit_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'leave_rule');
                $.ajax({
                    url: "{{ url('leave_types') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#leave_rules_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            leave_rules_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Rules has been Updated Successfully!'
                            })
                        }

                    }
                });
            });

            // Paid Time Off Add Data
            $('#paid_time_off_add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'paid_time_off');
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#paid_time_off_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            paid_time_off_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Paid Time Off has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Paid Time Off Update Data
            $('#paid_time_off_edit_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'paid_time_off');
                $.ajax({
                    url: "{{ url('leave_types') }}" + "/" + rowid,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#paid_time_off_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            paid_time_off_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Paid Time Off has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            // Edit Leave Group Add Data
            $('#edit_leave_group_add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'edit_leave_group');
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_leave_group_add_form")[0].reset();
                            $("#edit_leave_group_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            edit_leave_group_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Group has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Edit Leave Group Updated Data
            $("#edit_leave_group_edit_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'edit_leave_group');
                $.ajax({
                    url: "{{ url('leave_types') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_leave_group_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            edit_leave_group_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Group has been Updated Successfully!'
                            })
                        }

                    }
                });
            });

            // Leave Group Employee Add Data
            $('#leave_group_employee_add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'leave_group_employee');
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#leave_group_employee_add_form")[0].reset();
                            $("#leave_group_employee_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            leave_group_employee_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Group Employee has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Leave Group For All Employee Add Data
            $('#all_leave_group_employee_add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'all_leave_group_employee');
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#all_leave_group_employee_add_form")[0].reset();
                            $("#all_leave_group_employee_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            leave_group_employee_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Group Employee has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Leave Group Employee Update Data
            $("#leave_group_employee_edit_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'leave_group_employee');
                $.ajax({
                    url: "{{ url('leave_types') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#leave_group_employee_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            leave_group_employee_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Leave Group Employee has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
        });

        // Leave Period DataTable
        function leavePeriods() {
            leave_periods_dataTable = $('#leave-periods_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "leave_period"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'date_start',
                        name: 'date_start',
                    },
                    {
                        data: 'date_end',
                        name: 'date_end',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=leave_periods_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' 
                                // +
                                // '<a href="javascript:;" onclick=leave_periods_delete(' + full.id +
                                // ')>' +
                                // feather.icons['trash-2'].toSvg({
                                //     class: 'font-medium-4 text-danger'
                                // }) +
                                // '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Leave Periods',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Leave Periods',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Leave Periods',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Leave Periods',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Leave Periods',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#leave_periods_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#leave-periods div.head-label').html('<h6 class="mb-0">List of Leave Periods</h6>');
        }

        // Work Week DataTables
        function workWeek() {
            work_week_dataTable = $('#work_week_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "work_week"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'desc',
                        name: 'work_week.desc',
                    },
                    {
                        data: 'name',
                        name: 'workdays.name',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=work_week_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                })
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Work Week',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Work Week',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Work Week',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Work Week',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Work Week',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#work_week_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#work-week div.head-label').html('<h6 class="mb-0">List of Work Week</h6>');
        }

        // Holidays DataTables
        function holidays() {
            holidays_dataTable = $('#holidays_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "holidays"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'holidays.name',
                    },
                    {
                        data: 'dateh',
                        name: 'holidays.dateh',
                    },
                    {
                        data: 'status',
                        name: 'holidays.status',
                    },
                    {
                        data: 'namecap',
                        name: 'country.namecap',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=holidays_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick=holidays_delete(' + full.id + ')>' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Holidays',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Holidays',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Holidays',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Holidays',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Holidays',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#holidays_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#holidays div.head-label').html('<h6 class="mb-0">List of Holidays</h6>');
        }

        // Leave Rules DataTables
        function leaveRules() {
            leave_rules_dataTable = $('#leave_rules_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "leave_rules"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'leave_name',
                        name: 'leavetypes.name',
                    },
                    {
                        data: 'leaveGroupName',
                        name: 'leavegroups.name',
                    },
                    {
                        data: 'specification',
                        name: 'jobtitles.specification',
                    },
                    {
                        data: 'name',
                        name: 'employmentstatus.name',
                    },
                    {
                        data: 'full name',
                        render: function(data, type, row) {
                            return row.first_name + ' ' + row.last_name;
                        }
                    },
                    {
                        data: 'exp_days',
                        name: 'leaverules.exp_days',
                    },
                    {
                        data: 'default_per_year',
                        name: 'leaverules.default_per_year',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=leave_rules_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick=leave_rules_delete(' + full.id + ')>' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Leave Rules',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Leave Rules',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Leave Rules',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Leave Rules',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Leave Rules',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#leave_rules_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#leave_rules div.head-label').html('<h6 class="mb-0">List of Leave Rules</h6>');
        }

        // Paid Time Off DataTables
        function paidTimeOff() {
            paid_time_off_dataTable = $('#paid_time_off_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "paid_time_off"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'leavetypes.name',
                    },
                    {
                        data: 'full name',
                        render: function(data, type, row) {
                            return row.fname + ' ' + row.lname;
                        }
                    },
                    {
                        data: 'periodName',
                        name: 'leaveperiods.name',
                    },
                    {
                        data: 'amount',
                        name: 'leavestartingbalance.amount',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=paid_time_off_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick= paid_time_off_delete(' + full.id +
                                ')>' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#paid_time_off_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#paid-time-off div.head-label').html('<h6 class="mb-0">List of Paid Time Off</h6>');
        }

        // Edit Leave Group DataTables
        function editLeaveGroup() {
            edit_leave_group_dataTable = $('#edit_leave_group_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "edit_leave_group"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'details',
                        name: 'details',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=edit_leave_group_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick=edit_leave_group_delete(' + full.id +
                                ')>' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#edit_leave_group_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#edit-leave-group div.head-label').html('<h6 class="mb-0">List of Edit Leave Group</h6>');
        }

        // Leave Group Employee DataTables
        function editLeaveEmployee() {
            leave_group_employee_dataTable = $('#leave_group_employee_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: "{{ route('leave_types.index') }}",
                columns: [{
                        data: 'responsive_id',
                        searchable: false,
                        orderable:false
                    },
                    {
                        data : 'DT_RowIndex',
                        name : 'DT_RowIndex',
                        searchable: false,
                    },
                    // {
                    //     data: 'full name',
                    //     render: function(data, type, row) {
                    //         return row.first_name + " " + row.last_name;
                    //     }
                    // },
                    {
                        data: 'full_name' , render: function(data , type , row){
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + " " + middleName + " " + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'name',
                        name: 'leavegroups.name',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=leave_group_employee_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick=leave_group_employee_delete(' + full
                                .id + ')>' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#leave_group_employee_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#leave-group-employee div.head-label').html('<h6 class="mb-0">List of Leave Group Employee</h6>');
        }

        // Special Days DataTables

        $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            var selected = employee == value.id ? 'selected' : '';
                            $('#employeeFilter').append(
                                $('<option '+selected+'></option>').val(value.id).html(
                                    value.employee_id + ' - ' +value.employee_code + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation + ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });

        function SpecialDays() {
            paid_time_off_dataTable = $('#special_day_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "special_working_day"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'full_name' , render: function(data , type , row){
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + " " + middleName + " " + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'special_day',
                        name: 'special_day',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" onclick= special_day_delete(' + full.id +
                                ')>' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Paid Time Off',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#special_day_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#paid-time-off div.head-label').html('<h6 class="mb-0">List of Paid Time Off</h6>');
        }

        function leaveTypesetting() {
            leave_type_setting_datatable = $('#leave_type_setting_datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "leave_type_setting"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'leavetypes.name',
                    },
                    {
                        data: 'full_name',
                        render: function(data, type, row) {
                            var firstName = row.first_name != null ? row.first_name : '-';
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';

                            // Check if the full name is composed of only null values and return '-'
                            if (firstName === '-' && middleName === '' && lastName === '') {
                                return '-';
                            }

                            return firstName + " " + middleName + " " + lastName;
                        },
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'department_names',
                        searchable: false,
                        render: function(data) {
                            return data ? data : '-';
                        }
                    },
                    {
                        data: 'section_names',
                        searchable : false,
                        render: function(data) {
                            return data ? data : '-';
                        }
                    },
                    {
                        data: 'employment_statuses_names',
                        searchable : false,
                        render: function(data) {
                            return data ? data : '-';
                        }
                    },
                    {
                        data: 'amount',
                        name: 'leave_type_settings.amount', 
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=edit_leave_setting(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick=deleteLeaveTypeSetting(' + full.id +
                                ')>' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#leave_setting_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#leave-type-setting div.head-label').html('<h6 class="mb-0">List Leaves Settings</h6>');
        }

        var leave_type_settings_sections;
        $("#departments").change(function() {
            var optVal = $(this).val();
            if (optVal.length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_department_child') }}",
                    data: {
                        department_id: optVal,
                        leave_type_setting : true,
                    },
                    success: function(response) {
                        $('#sections').empty();
                        $('#sections').html('<option value="">Select Sections</option>');
                        $.each(response, function(index, value) {
                            $('#sections').append(
                                $('<option></option>').val(
                                    value.id).html(
                                    value.title)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            } else {
                $('#section').empty();
            }
        });

        $("#edit_departments").change(function() {
            var optVal = $(this).val();
            if (optVal.length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_department_child') }}",
                    data: {
                        department_id: optVal,
                        leave_type_setting : true,
                    },
                    success: function(response) {
                        $('#edit_sections').empty();
                        $('#edit_sections').html('<option value="">Select Section</option>');
                        $.each(response, function(index, value) {
                            var selected = (leave_type_settings_sections ? leave_type_settings_sections : []).includes('' +
                                value.id + '') ? 'selected' : '';
                            $('#edit_sections').append(
                                $('<option ' + selected + '></option>').val(
                                    value.id).html(
                                    value.title)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            } else {
                $('#section').empty();
            }
        });

        // Leave Setting Add
        $('#leave_setting_add_form').submit(function(e) {
            ButtonStatus('.form_save',true);
                blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'leave_type_setting');
            $.ajax({
                url: "{{ route('leave_types.store') }}",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                    $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $("#leave_setting_add_modal").modal("hide");
                        $('#leave_setting_add_form')[0].reset();
                        $("#leave_setting_add_form .select2").val([]).trigger('change');
                        leave_type_setting_datatable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Leave Type Setting Added Successfully!'
                        })
                    }
                }
            });
        });

        function edit_leave_setting(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid + "/edit",
                type: "GET",
                data: {
                    type: 'leave_type_setting'
                },
                success: function(response) {
                    leave_type_settings_sections = response.sections;
                    $('#edit_departments').val(response.departments).select2().trigger('change');
                    $('#edit_employement_statuses').val(response.employement_statuses).select2();
                    $('#leave_type_for_setting_edit').val(response.leave_type_id).select2();
                    $('#edit_leave_setting_employee').val(response.employee).select2();
                    $('#edit_amount_for_leave_setting').val(response.amount);
                    $("#leave_setting_edit_modal").modal("show");
                },
            });
        }

        // Leave Setting Update
        $('#leave_setting_edit_form').submit(function(e) {
            ButtonStatus('.form_save',true);
                blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'leave_type_setting');
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid,
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                    $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $("#leave_setting_edit_modal").modal("hide");
                        $('#leave_setting_edit_form')[0].reset();
                        $("#leave_setting_edit_form .select2").val([]).trigger('change');
                        leave_type_setting_datatable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Leave Type Setting Updated Successfully!'
                        })
                    }
                }
            });
        });

        // leave setting delete
        function deleteLeaveTypeSetting(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want you to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.blockUI();
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'leave_type_setting'
                                },
                                success: function(response) {
                                    $.unblockUI();
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        leave_type_setting_datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Leave Setting has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        function holidaySettings() {
            holiday_setting_datatable = $('#holiday_setting_datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "holiday_setting"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'holiday_names',
                        searchable : false
                    },
                    {
                        data: 'department_names',
                        searchable : false
                    },
                    {
                        data: 'section_names',
                        searchable : false
                    },
                    {
                        data: 'employment_statuses_names',
                        searchable : false
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;"  class="item-edit" onclick=edit_holiday_setting(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick=deleteHolidaySetting(' + full.id +
                                ')>' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#holiday_setting_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#leave-type-setting div.head-label').html('<h6 class="mb-0">List Holiday Settings</h6>');
        }

        var holiday_settings_sections;
        $("#holiday_setting_departments").change(function() {
            var optVal = $(this).val();
            if (optVal.length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_department_child') }}",
                    data: {
                        department_id: optVal,
                        leave_type_setting : true,
                    },
                    success: function(response) {
                        $('#holiday_setting_sections').empty();
                        $('#holiday_setting_sections').html('<option value="">Select Sections</option>');
                        $.each(response, function(index, value) {
                            $('#holiday_setting_sections').append(
                                $('<option></option>').val(
                                    value.id).html(
                                    value.title)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            } else {
                $('#holiday_setting_sections').empty();
            }
        });

        $("#edit_holiday_settings_departments").change(function() {
            var optVal = $(this).val();
            if (optVal.length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_department_child') }}",
                    data: {
                        department_id: optVal,
                        leave_type_setting : true,
                    },
                    success: function(response) {
                        $('#edit_holiday_settings_sections').empty();
                        $('#edit_holiday_settings_sections').html('<option value="">Select Section</option>');
                        $.each(response, function(index, value) {
                            var selected = (holiday_settings_sections ? holiday_settings_sections : []).includes('' +
                                value.id + '') ? 'selected' : '';
                            $('#edit_holiday_settings_sections').append(
                                $('<option ' + selected + '></option>').val(
                                    value.id).html(
                                    value.title)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            } else {
                $('#section').empty();
            }
        });

        // Leave Setting Add
        $('#holiday_setting_add_form').submit(function(e) {
            ButtonStatus('.form_save',true);
                blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'holiday_setting');
            $.ajax({
                url: "{{ route('leave_types.store') }}",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                    $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $("#holiday_setting_add_modal").modal("hide");
                        $('#holiday_setting_add_form')[0].reset();
                        $("#holiday_setting_add_form .select2").val([]).trigger('change');
                        holiday_setting_datatable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Holiday Setting Added Successfully!'
                        })
                    }
                }
            });
        });

        function edit_holiday_setting(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid + "/edit",
                type: "GET",
                data: {
                    type: 'holiday_setting'
                },
                success: function(response) {
                    holiday_settings_sections = response.sections;
                    $('#edit_holiday_settings_departments').val(response.departments).select2().trigger('change');
                    $('#edit_holiday_settings_employement_statuses').val(response.employement_statuses).select2();
                    $('#holidays_for_settings_edit').val(response.holidays).select2();
                    $("#holiday_setting_edit_modal").modal("show");
                },
            });
        }

        // Leave Setting Update
        $('#holiday_setting_edit_form').submit(function(e) {
            ButtonStatus('.form_save',true);
                blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'holiday_setting');
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid,
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                    $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $("#holiday_edit_modal").modal("hide");
                        $('#holiday_setting_edit_form')[0].reset();
                        $("#holiday_setting_edit_form .select2").val([]).trigger('change');
                        $("#holiday_setting_edit_modal").modal("hide");
                        holiday_setting_datatable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Holiday Setting Updated Successfully!'
                        })
                    }
                }
            });
        });

        // leave setting delete
        function deleteHolidaySetting(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want you to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.blockUI();
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'holiday_setting'
                                },
                                success: function(response) {
                                    $.unblockUI();
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        holiday_setting_datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Holiday Setting has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Paid Time Off Add Data
        $('#special_day_add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'special_working_day');
                $.ajax({
                    url: "{{ route('leave_types.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#paid_time_off_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            paid_time_off_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Spacial Day has been Added Successfully!'
                            })
                        }
                    }
                });
            });
            // Paid Time Off Delete
        function special_day_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want you to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'special_working_day'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        paid_time_off_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Special Day has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        
        // Leave Group Employee DataTables
        function AllLeaveEmployee() {
            leave_group_employee_dataTable = $('#all-leave_group_employee_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('leave_types.index') }}",
                    data: {
                        type: "all_leave_employee"
                    }
                },
                columns: [{
                        data: 'responsive_id',
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'leavegroups.name',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" onclick=all_leave_group_employee_delete(' + full
                                .id + ')>' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Edit Leave Group Employee',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#all_leave_group_employee_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#all-leave-group-employee div.head-label').html('<h6 class="mb-0">List of All Leave Group Employee</h6>');
        }

        // Leave Types Edit
        function leave_types_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + id + "/edit",
                type: "GET",
                data: {
                    type: 'leave_type'
                },
                success: function(response) {
                    $('#edit_name').val(response.name);
                    $('#edit_default_per_year').val(response.default_per_year);
                    $('#edit_abbreviation').val(response.abbreviation);
                    $('#edit_supervisor_leave_assign').val(response.supervisor_leave_assign).select2();
                    $('#edit_employee_can_apply').val(response.employee_can_apply).select2();
                    $('#edit_leave_accrue').val(response.leave_accrue).select2();
                    $('#edit_carried_forward').val(response.carried_forward).select2();
                    $('#edit_more_quota').val(response.more_quota).select2();
                    $('#edit_carried_forward_percentage').val(response.carried_forward_percentage);
                    $('#edit_max_carried_forward_amount').val(response.max_carried_forward_amount);
                    $('#edit_carried_forward_leave').val(response.carried_forward_leave_availability).select2();
                    $('#edit_propotionate_on_joined_date').val(response.propotionate_on_joined_date).select2();
                    $('#edit_send_notification_emails').val(response.send_notification_emails).select2();
                    $('#edit_leave_group').val(response.leave_group).select2();
                    $('#edit_leave_color').val(response.leave_color).select2();
                    $('#joining_date_allow_edit').val(response.joining_date_allow);
                    $('#allow_leaves_per_month_edit').val(response.allow_leaves_per_month);
                    if(response.allow == 'allow'){
                        $('#edit_allow').prop('checked', true);
                    }else{
                        $('#edit_allow').prop('checked', false);
                    }
                    if(response.status == 1)
                    {
                        $('#edit_status').attr('checked', true);
                    }else{
                        $('#edit_status_1').attr('checked', true);

                    }
                    $('#edit_apply_beyond_current').val(response.apply_beyond_current).select2();
                    // if(response.sandwich_leave == 1)
                    // {
                    //     $("#edit_sandwich_leave").prop('checked', true);
                    // }
                    $("#leave_types_edit_modal").modal("show");
                },
            });
        }
        // Leave Types Delete
        function leave_types_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want you to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'leave_type'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        leave_types_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Leave Typess has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Leave Periods Edit
        function leave_periods_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid + "/edit",
                type: "GET",
                data: {
                    type: 'leave_period'
                },
                success: function(response) {
                    $('#leave_periods_name').val(response.name);
                    $('#edit_date_start').val(response.date_start);
                    $('#edit_date_end').val(response.date_end);
                    $('#edit_leave_period_status').val(response.status).select2();
                    $("#leave_periods_edit_modal").modal("show");
                },
            });
        }
        // Leave Periods Delete
        function leave_periods_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'leave_period'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        leave_periods_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Leave Periods has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Work Week Edit
        function work_week_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid + "/edit",
                type: "GET",
                data: {
                    type: 'work_week'
                },
                success: function(response) {
                    console.log(response.work_week.status);
                    var weekDays = <?php echo json_encode($weekDays); ?>;
                    weekDays = Object.values(weekDays);
                    $('#edit_work_week').val(response.work_week.desc);
                    $('#append_days').empty();
                    if(response.work_week.status == 1)
                    {
                        $('#edit_status_2').attr('checked', true);
                    }else{
                        $('#edit_status_3').attr('checked', true);
                    }
                    $(weekDays).each(function(index, day) {
                        var checked = '';
                        if(response.work_days.includes(day)){
                            checked = 'checked';
                        }
                        $('#append_days').append(`
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="edit_${day}" name="off_days[]" value="${index+1}" ${checked}>
                                <label class="form-check-label" for="edit_${day}">${day}</label>
                            </div>
                        `)
                    });
                    $("#work_week_edit_modal").modal("show");
                },
            });
        }   
        // Work Week Delete
        function work_week_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'work_week'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        work_week_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Work Week has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Holidays Edit
        function holidays_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid + "/edit",
                type: "GET",
                data: {
                    type: 'holiday'
                },
                success: function(response) {
                    $('#holiday_name').val(response.name);
                    $('#edit_dateh').val(response.dateh);
                    $('#edit_status').val(response.status).select2();
                    $('#edit_country').val(response.country).select2();
                    $("#holidays_edit_modal").modal("show");
                },
            });
        }
        // Holidays Delete
        function holidays_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'holiday'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        holidays_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Holidays has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Leave Rules Edit
        function leave_rules_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid + "/edit",
                type: "GET",
                data: {
                    type: 'leave_rule'
                },
                success: function(response){
                    $('#leave_rules_leave_type').val(response.leave_type).select2();
                    $('#leave_rules_leave_group').val(response.leave_group).select2();
                    $('#leave_rules_job_title').val(response.job_title).select2();
                    $('#leave_rules_employment_status').val(response.employment_status).select2();
                    $('#leave_rules_employee').val(response.employee).select2();
                    $('#leave_rules_exp_days').val(response.exp_days);
                    $('#leave_rules_default_per_year').val(response.default_per_year);
                    $('#leave_rules_supervisor_leave_assign').val(response.supervisor_leave_assign).select2();
                    $('#leave_rules_apply_beyond_current').val(response.apply_beyond_current).select2();
                    $('#leave_rules_leave_accrue').val(response.leave_accrue).select2();
                    $('#leave_rules_carried_forward').val(response.carried_forward).select2();
                    $('#leave_rules_carried_forward_percentage').val(response.carried_forward_percentage);
                    $('#leave_rules_max_carried_forward_amount').val(response.max_carried_forward_amount);
                    $('#leave_rules_carried_forward_leave_availability').val(response.carried_forward_leave_availability).select2();
                    $('#leave_rules_propotionate_on_joined_date').val(response.propotionate_on_joined_date).select2();
                    $("#leave_rules_edit_modal").modal("show");
                },
            });
        }
        // Leave Rules Delete
        function leave_rules_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'leave_rule'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        leave_rules_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Leave Rules has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Paid Time Off Edit
        function paid_time_off_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid + "/edit",
                type: "GET",
                data: {
                    type: 'paid_time_off'
                },
                success: function(response) {
                    $('#edit_leave_type').val(response.leave_type).select2();
                    $('#edit_employee').val(response.employee).select2();
                    $('#edit_leave_period').val(response.leave_period).select2();
                    $('#edit_amount').val(response.amount);
                    $('#edit_note').val(response.note);
                    $("#paid_time_off_edit_modal").modal("show");
                },
            });
        }
        // Paid Time Off Delete
        function paid_time_off_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want you to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'paid_time_off'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        paid_time_off_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Paid Time Off has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Edit Leave Group Edit
        function edit_leave_group_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid + "/edit",
                type: "GET",
                data: {
                    type: 'leave_group'
                },
                success: function(response) {
                    $('#edit_leave_group_name').val(response.name);
                    $('#edit_leave_group_details').val(response.details);
                    $("#edit_leave_group_edit_modal").modal("show");
                },
            });
        }
        // Edit Leave Group Delete
        function edit_leave_group_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'edit_leave_group'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        edit_leave_group_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Edit Leave Group has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Leave Group Employee Edit
        function leave_group_employee_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('leave_types') }}" + "/" + rowid + "/edit",
                type: "GET",
                data: {
                    type: 'leave_group_employee'
                },
                success: function(response) {
                    console.log(response);
                    $('#edit_leave_group_employee').val(response.employee).select2();
                    $('#edit_group').val(response.leave_group).select2();
                    $("#leave_group_employee_edit_modal").modal("show");
                },
            });
        }
        // Leave Group Employee Delete
        function leave_group_employee_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure  you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'leave_group_employee'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        leave_group_employee_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Leave Group Employee has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        //All Leave Group Employee Delete
        function all_leave_group_employee_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure  you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',

                        action: function() {
                            $.ajax({
                                url: "leave_types/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'leave_group_employee'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        leave_group_employee_dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Leave Group For All Employee has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
