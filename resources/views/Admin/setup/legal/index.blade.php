@extends('Admin.layouts.master')

@section('title', 'Company Legals');

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Company Legals</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Setup</a>
                            </li>
                            <li class="breadcrumb-item active">Legals
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="basic-dataTable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Legal Action</th>
                                    <th>Legal Proceeding Title</th>
                                    <th>Legal Counsel Company</th>
                                    <th>Employee</th>
                                    <th>Legal Counsel Employee</th>
                                    <th>Case Filed On</th>
                                    <th>Field In The Court</th>
                                    <th>Case Type</th>
                                    <th>Petition/ Case Copy</th>
                                    <th class="not_include">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="modal-size-lg d-inline-block">
            <!--Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add Company Legals</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Legal Action</label>
                                            <select name="legal_action" id="type" class="form-select" required>
                                                <option selected>Legal Action</option>
                                                <option value="By Company">By Company</option>
                                                <option value="Against Company">Against Company</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Legal Proceeding Title</label>
                                            <input type="text" id="type" class="form-control" placeholder="Legal Proceeding Title" name="title" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Legal Counsel Company</label>
                                            <input type="text" id="type" class="form-control" placeholder="Legal Counsel Company" name="legal_counsel_company" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Employee</label>
                                            <select name="employee_id" id="type" class="form-select" data-placeholder="Employee" required>
                                                <option selected>Employee</option>
                                                @foreach ($employees as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->first_name}} {{$employee->last_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Legal Counsel Employee</label>
                                            <input type="text" id="type" class="form-control" placeholder="Legal Counsel Employee" name="legal_counsel_employee" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Case Filed On</label>
                                            <input type="date" id="type" class="form-control" placeholder="" name="case_filed_on" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Field In Court</label>
                                            <input type="text" id="type" class="form-control" placeholder="Yes Or No" name="court" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Petition/Case Copy</label>
                                            <input type="file" id="type" class="form-control" value="" placeholder="" name="petition_case_copy" required/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Case Type</label>
                                            <select name="case_type" id="type" class="form-select" data-placeholder="Select Case Type" required>
                                                <option selected>Case Type</option>
                                                <option value="Civil">Civil</option>
                                                <option value="Criminal">Criminal</option>
                                                <option value="Military">Military</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End Add Modal -->

            <!--start edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add Company Legals</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="legal_action">Legal Action</label>
                                            <select name="legal_action" id="legal_action" class="form-select" required>
                                                <option selected>Legal Action</option>
                                                <option value="By Company">By Company</option>
                                                <option value="Against Company">Against Company</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Legal Proceeding Title</label>
                                            <input type="text" id="title" class="form-control" placeholder="Legal Proceeding Title" name="title" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="legal_counsel_company">Legal Counsel Company</label>
                                            <input type="text" id="legal_counsel_company" class="form-control" placeholder="Legal Counsel Company" name="legal_counsel_company" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee_id">Employee</label>
                                            <select name="employee_id" id="employee_id" class="form-select" data-placeholder="Employee" required>
                                                <option selected>Employee</option>
                                                @foreach ($employees as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->first_name}} {{$employee->last_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="legal_counsel_employee">Legal Counsel Employee</label>
                                            <input type="text" id="legal_counsel_employee" class="form-control" placeholder="Legal Counsel Employee" name="legal_counsel_employee" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="case_filed_on">Case Filed On</label>
                                            <input type="date" id="case_filed_on" class="form-control" placeholder="" name="case_filed_on" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="court">Field In Court</label>
                                            <input type="text" id="court" class="form-control" placeholder="" name="court" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="petition_case_copy">Petition/Case Copy</label>
                                            <input type="file" id="petition_case_copy" class="form-control" placeholder="" name="petition_case_copy"/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="case_type">Case Type</label>
                                            <select name="case_type" id="case_type" class="form-select" data-placeholder="Select Case Type" required>
                                                <option selected>Case Typeu</option>
                                                <option value="Civil">Civil</option>
                                                <option value="Criminal">Criminal</option>
                                                <option value="Military">Military</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End edit Modal -->
            
        </div>
    </div>
</section>

@endsection


@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering : true,
                ajax: "{{ route('legals.index') }}",
                columns: [
                    {
                        data: 'responsive_id' 
                    },
                    {
                        data : 'legal_action',
                        name : 'legals.legal_action',
                    },
                    {
                        data: 'title',
                        name: 'legals.title',
                    }, 
                    {
                        data: 'legal_counsel_company',
                        name: 'legals.legal_counsel_company',
                    },
                    {
                        data: 'full name', render:function ( data, type, row ) {                        
                        return row.first_name + row.last_name;
                    }
                        name: 'employees.first_name' + 'employees.last_name',
                    },
                    {
                        data: 'legal_counsel_employee',
                        name: 'legals.legal_counsel_employee',
                    },
                    {
                        data: 'case_filed_on',
                        name: 'legals.case_filed_on',
                    },
                    {
                        data: 'court',
                        name: 'legals.court',
                    },
                    {
                        data: 'case_type',
                        name: 'legals.case_type',
                    },
                    {
                        data: 'petition_case_copy',
                        name: 'legals.petition_case_copy',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#edit_modal" class="item-edit" onclick=edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="delete_item('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of Company Legals</h6>');
            $("#add_form").submit(function (e) {
                e.preventDefault();
                $.ajax({
                    url: "{{route('legals.store')}}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function (response) {
                        console.log(response);
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Legals has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Update record
            $("#edit_form").on("submit", function (e) {
                e.preventDefault();
                $.ajax({
                    url: "{{url('legals')}}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#edit_modal").modal("hide");
                            $('#edit_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'CompanyStructure has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });
        });
        function edit(id) {
            var x = document.getElementById('petition_case_copy').value;
            console.log(x);
            rowid = id;
            $.ajax({
                url: "{{url('legals')}}" + "/" + id + "/edit",
                type: "GET",
                success: function (response) {
                    console.log(response);
                    $("#legal_action").val(response.legal_action);
                    $("#title").val(response.title);
                    $("#legal_counsel_company").val(response.legal_counsel_company);
                    $("#employee_id").val(response.employee_id).select2();
                    $("#legal_counsel_employee").val(response.legal_counsel_employee);
                    $("#case_filed_on").val(response.case_filed_on);
                    $("#court").val(response.court);
                    $("#case_type").val(response.case_type).select2();
                    $("#edit_modal").modal("show");
                },
            });
        }
        function delete_item(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "legals/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else{
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Legals has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection