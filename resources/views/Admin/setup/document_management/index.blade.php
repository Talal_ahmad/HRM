@extends('Admin.layouts.master')
@section('title', 'Document Management')
@section('style')
<style>
    .dept .select2-search__field{
        width: initial !important;
    }
</style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Document_Management')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Home')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Setup')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Document_Management')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="company_documents-tab" data-bs-toggle="tab" href="#company_documents"
                            aria-controls="company_documents" role="tab" aria-selected="true">@lang('app.Company_Documents')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="documents()" id="document_types-tab" data-bs-toggle="tab"
                            href="#document_types" aria-controls="document_types" role="tab" aria-selected="false">@lang('app.Documents_Types')
                            </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="employeeDocuments()" id="employee_documents-tab"
                            data-bs-toggle="tab" href="#employee_documents" aria-controls="employee_documents" role="tab"
                            aria-selected="false">@lang('app.Employee_Documents')</a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- Company Documents --}}
                    <div class="tab-pane active" id="company_documents" aria-labelledby="company_documents-tab"
                        role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="company_documents_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th>Details</th>
                                                    <th>Status</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!--Add Modal -->
                        <div class="modal fade text-start" id="company_documents_add_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Company_Documents')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="company_documents_add_form" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="title">@lang('app.Name')</label>
                                                        <input type="text" id="title" class="form-control" placeholder="@lang('app.Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="status">@lang('app.Status')</label>
                                                        <select name="status" id="status" class="select2 form-select" required>
                                                            <option value="Active">@lang('app.Active')</option>
                                                            <option value="Inactive">@lang('app.In_Active')</option>
                                                            <option value="Draft">@lang('app.Draft')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12 dept">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="share_departments">@lang('app.Share_Departments')</label>
                                                        <select name="share_departments[]" id="share_departments" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" multiple required>
                                                            <option value=""></option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{ $department->id }}">{{ $department->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12 dept">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="share_employees">@lang('app.Share_Employees')</label>
                                                        <select name="share_employees[]" id="share_employees" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" multiple required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label for="attachment" class="form-label">@lang('app.Attachment')</label>
                                                        <input class="form-control" name="attachment" type="file" id="attachment" required>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="details">@lang('app.Details')</label>
                                                        <textarea class="form-control" name="details" id="details" cols="20" rows="2" placeholder="@lang('app.Type_here')" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Edit Modal -->
                        <div class="modal fade text-start" id="company_documents_edit_modal" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Company_Documents')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="company_documents_edit_form" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="title">@lang('app.Name')</label>
                                                        <input type="text" id="edit_name" class="form-control"
                                                            placeholder="@lang('app.Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_status">@lang('app.Status')</label>
                                                        <select name="status" id="edit_status" class="select2 form-select" required>
                                                            <option value="Active">@lang('app.Active')</option>
                                                            <option value="Inactive">@lang('app.In_Active')</option>
                                                            <option value="Draft">@lang('app.Draft')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12 dept">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_share_departments">@lang('app.Share_Departments')</label>
                                                        <select name="share_departments[]" id="edit_share_departments" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" multiple required>
                                                            <option value=""></option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{ $department->id }}">{{ $department->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12 dept">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_share_employees">@lang('app.Share_Employees')</label>
                                                        <select name="share_employees[]" id="edit_share_employees" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" multiple required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name." - ".$employee->desigination}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label for="edit_attachment" class="form-label">@lang('app.Attachment')</label>
                                                        <input class="form-control" type="file" id="edit_attachment" name="attachment" />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_details">@lang('app.Details')</label>
                                                        <textarea class="form-control" name="details" id="edit_details" cols="20" rows="2" placeholder="@lang('app.Type_here')" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Company Documents End --}}

                    {{-- Document Types --}}
                    <div class="tab-pane" id="document_types" aria-labelledby="suspend-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="document_types_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th>Details</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!--Add Modal -->
                        <div class="modal fade text-start" id="add_modal_doc_type" tabindex="-1"
                            aria-labelledby="add_modal_doc_type" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="add_modal_doc_type">@lang('app.Add_Document_Types')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="add_form_doc_type">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="doc_type_name">@lang('app.Name')</label>
                                                        <input type="text" id="doc_type_name" class="form-control" placeholder="@lang('app.Name')" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="expire_notification">@lang('app.Notify_Expiry')</label>
                                                        <select name="expire_notification" id="expire_notification"
                                                            class="select2 form-select" data-placeholder="Select" required>
                                                            <option value=""></option>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="expire_notification_month">@lang('app.Notify_Expiry_Before_One_Month')</label>
                                                        <select name="expire_notification_month" id="expire_notification_month" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="expire_notification_week">@lang('app.Notify_Expiry_Before_One_Week')</label>
                                                        <select name="expire_notification_week" id="expire_notification_week" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="expire_notification_day">@lang('app.Notify_Expiry_Before_One_Day')</label>
                                                        <select name="expire_notification_day" id="expire_notification_day" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="details">@lang('app.Details')</label>
                                                        <textarea class="form-control" name="details" id="details" cols="20" rows="2" placeholder="@lang('app.Type_here')" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Edit Modal -->
                        <div class="modal fade text-start" id="edit_modal_doc_type" tabindex="-1"
                            aria-labelledby="edit_modal_doc_type" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="edit_modal_doc">@lang('app.Edit_Document_Types')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="edit_form_doc_type">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_doc_type_name">@lang('app.Name')</label>
                                                        <input type="text" id="edit_doc_type_name" class="form-control" placeholder="" name="name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_expire_notification">@lang('app.Notify_Expiry')</label>
                                                        <select name="expire_notification" id="edit_expire_notification" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_expire_notification_month">@lang('app.Notify_Expiry_Before_One_Month')</label>
                                                        <select name="expire_notification_month" id="edit_expire_notification_month" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_expire_notification_week">@lang('app.Notify_Expiry_Before_One_Week')</label>
                                                        <select name="expire_notification_week" id="edit_expire_notification_week" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_expire_notification_day">@lang('app.Notify_Expiry_Before_One_Day')</label>
                                                        <select name="expire_notification_day" id="edit_expire_notification_day" class="select2 form-select" required>
                                                            <option value="Yes">@lang('app.Yes')</option>
                                                            <option value="No">@lang('app.No')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_typedetails">@lang('app.Details')</label>
                                                        <textarea class="form-control" name="details" id="edit_typedetails" cols="20" rows="2" placeholder="@lang('app.Type_here')" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Document Types End --}}

                    {{-- Employee Documents --}}
                    <div class="tab-pane" id="employee_documents" aria-labelledby="employee_documents-tab"
                        role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="employee_documents_table">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Employee</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Document</th>
                                                    <th>Details</th>
                                                    <th>Date Added</th>
                                                    <th>Status</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!--Add Modal -->
                        <div class="modal fade text-start" id="add_modal_emply_doc" tabindex="-1"
                            aria-labelledby="add_modal_emply_doc" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="add_modal_emply">@lang('app.Add_Employee_Document')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="add_form_emply_doc">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                        <select name="employee" id="employee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="document">@lang('app.Document')</label>
                                                        <select name="document" id="document" class="select2 form-select" data-placeholder="@lang('app.Select_Document')" required>
                                                            <option value=""></option>
                                                            @foreach ($document_type as $document)
                                                                <option value="{{ $document->id }}">{{ $document->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="date_added">@lang('app.Date_Added')</label>
                                                        <input type="text" name="date_added" id="date_added" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="valid_until">@lang('app.Valid_Until')</label>
                                                        <input type="text" name="valid_until" id="valid_until" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="employee_document_status">@lang('app.Status')</label>
                                                        <select name="status" id="employee_document_status" class="select2 form-select"
                                                            required>
                                                            <option value="Active">@lang('app.Active')</option>
                                                            <option value="Inactive">@lang('app.In_Active')</option>
                                                            <option value="Draft">@lang('app.Draft')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label for="formFile" class="form-label">@lang('app.Attachment')</label>
                                                        <input class="form-control" type="file" id="attachment" name="attachment" />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="details">@lang('app.Details')</label>
                                                        <textarea class="form-control" name="details" id="details" cols="20" rows="2" placeholder="@lang('app.Type_here')"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Edit Modal -->
                        <div class="modal fade text-start" id="edit_modal_emply_doc" tabindex="-1"
                            aria-labelledby="edit_modal_emply_doc" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="edit_modal_emply">@lang('app.Edit_Document_Types')</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="edit_form_emply_doc">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_employee">@lang('app.Employees')</label>
                                                        <select name="employee" id="edit_employee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                                            <option value=""></option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="document">@lang('app.Document')</label>
                                                        <select name="document" id="edit_empl_document" class="select2 form-select" required data-placeholder="@lang('app.Select_Document')">
                                                            <option value=""></option>
                                                            @foreach ($document_type as $document)
                                                                <option value="{{ $document->id }}">{{ $document->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="date_added">@lang('app.Date_Added')</label>
                                                        <input type="text" name="date_added" id="edit_date_added" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="valid_until">@lang('app.Valid_Until')</label>
                                                        <input type="text" name="valid_until" id="edit_valid_until" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_employee_document_status">@lang('app.Status')</label>
                                                        <select name="status" id="edit_employee_document_status" class="select2 form-select" required>
                                                            <option value="Active">@lang('app.Active')</option>
                                                            <option value="Inactive">@lang('app.In_Active')</option>
                                                            <option value="Draft">@lang('app.Draft')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label for="formFile" class="form-label">@lang('app.Attachment')</label>
                                                        <input class="form-control" type="file" id="edit_empl_attachment" name="attachment" />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="details">@lang('app.Details')</label>
                                                        <textarea class="form-control" name="details" id="edit_empl_details" cols="20" rows="2" placeholder="@lang('app.Type_here')" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                            <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            $('#company_documents_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: true,
                ajax: {
                    url: "{{ route('document_management.index') }}",
                    data: {
                        type: 'company_documents',
                    },
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'details',
                        name: 'details',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#company_documents_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#company_documents div.head-label').html('<h6 class="mb-0">List of Company Documents</h6>');

            // Company Documents Add Data
            $("#company_documents_add_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'company_documents');
                $.ajax({
                    url: "{{ route('document_management.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        console.log(response);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#company_documents_add_form')[0].reset();
                            $("#company_documents_add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            $('#company_documents_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Documents has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Company Documents Update Data
            $("#company_documents_edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'company_documents');
                $.ajax({
                    url: "{{ url('document_management') }}" + "/" + rowid,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#company_documents_edit_form")[0].reset();
                            $("#company_documents_edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            $('#company_documents_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Documents has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
        });

        //Add Form Document Type
        $("#add_form_doc_type").submit(function(e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'document_types');
            $.ajax({
                url: "{{ route('document_management.store') }}",
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#add_form_doc_type')[0].reset();
                        $("#add_modal_doc_type").modal("hide");
                        $(".select2").val('').trigger('change')
                        $('#document_types_dataTable').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Document Type has been Added Successfully!'
                        })
                    }
                }
            });
        });

        // Update Document Type
        $("#edit_form_doc_type").on("submit", function(e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'document_types');
            $.ajax({
                url: "{{ url('document_management') }}" + "/" + rowid,
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#edit_form_doc_type')[0].reset();
                        $("#edit_modal_doc_type").modal("hide");
                        $(".select2").val('').trigger('change')
                        $('#document_types_dataTable').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Document Type has been Updated Successfully!'
                        })
                    }

                }
            });
        });

        //Add Employee Document
        $("#add_form_emply_doc").submit(function(e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'employee_documents')
            $.ajax({
                url: "{{ route('document_management.store') }}",
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                responsive: true,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#add_form_emply_doc')[0].reset();
                        $("#add_modal_emply_doc").modal("hide");
                        $(".select2").val('').trigger('change')
                        $('#employee_documents_table').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Employee Documents has been Added Successfully!'
                        })
                    }

                }
            });
        });
        // Update Employee Document Type
        $("#edit_form_emply_doc").on("submit", function(e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'employee_documents');
            $.ajax({
                url: "{{ url('document_management') }}" + "/" + rowid,
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#edit_form_emply_doc')[0].reset();
                        $("#edit_modal_emply_doc").modal("hide");
                        $(".select2").val('').trigger('change')
                        $('#employee_documents_table').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Employee Document has been Updated Successfully!'
                        })
                    }

                }
            });
        });

        // Document Types DataTable
        function documents() {
            $('#document_types_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('document_management.index') }}",
                    data: {
                        type: 'document_types',
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'details'
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit_doc_type(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item_type(' + full.id + ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal_doc_type'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#document_types div.head-label').html('<h6 class="mb-0">List of Document Types</h6>');
        }

        function employeeDocuments() {
            // Employee Documents DataTable
            $('#employee_documents_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('document_management.index') }}",
                    data: {
                        type: 'employee_documents',
                    },
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'empName',
                        render: function(data, type, row) {
                            return row.first_name+ ' ' + row.middle_name + ' ' + row.last_name
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'details'
                    },
                    {
                        data: 'date_added'
                    },
                    {
                        data: 'status'
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit_employee_doc(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_employee_doc(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal_emply_doc'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#employee_documents div.head-label').html('<h6 class="mb-0">List of Employee Documents</h6>');
        }

        // Company Documents Edit
        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('document_management') }}" + "/" + id + "/edit",
                type: "GET",
                data: {
                    type: 'company_documents'
                },
                success: function(response) {
                    $("#edit_name").val(response.name);
                    $("#edit_status").val(response.status).select2();
                    $("#edit_share_departments").val(JSON.parse(response.share_departments)).select2();
                    $("#edit_share_employees").val(JSON.parse(response.share_employees)).select2();
                    $("#edit_details").val(response.details);
                    $("#company_documents_edit_modal").modal("show");
                },
            });
        }
        // Company Documents Delete
        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "document_management/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'company_documents',
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#company_documents_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Company Documents has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }


        //Edit Document Type
        function edit_doc_type(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('document_management') }}" + "/" + id + "/edit",
                type: "GET",
                data: {
                    type: 'document_types',
                },
                success: function(response) {
                    $("#edit_doc_type_name").val(response.name);
                    $("#edit_expire_notification").val(response.expire_notification).select2();
                    $("#edit_expire_notification_month").val(response.expire_notification_month).select2();
                    $("#edit_expire_notification_week").val(response.expire_notification_week).select2();
                    $("#edit_expire_notification_day").val(response.expire_notification_day).select2();
                    $("#edit_typedetails").val(response.details);
                    $("#edit_modal_doc_type").modal("show");
                },
            });
        }
        //Delete Document Type
        function delete_item_type(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "document_management/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'document_types'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#document_types_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Document Type has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        //Edit Employee Document Type
        function edit_employee_doc(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('document_management') }}" + "/" + id + "/edit",
                type: "GET",
                data: {
                    type: 'employee_documents',
                },
                success: function(response) {
                    $("#edit_employee").val(response.employee).select2();
                    $("#edit_empl_document").val(response.document).select2();
                    $("#edit_date_added").val(response.date_added);
                    $("#edit_valid_until").val(response.valid_until);
                    $("#edit_empl_status").val(response.status);
                    $("#edit_empl_details").val(response.details);
                    $("#edit_modal_emply_doc").modal("show");
                },
            });
        }
        //Delete Employee Document Type
        function delete_employee_doc(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "document_management/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'employee_documents'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#employee_documents_table').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Document Type has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
