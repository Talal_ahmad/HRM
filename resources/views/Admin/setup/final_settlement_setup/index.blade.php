@extends('Admin.layouts.master')
@section('title', 'Final Settlement Setup')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Final_Settlement_Setup')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.Setup')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Final_Settlement_Setup') 
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-end">
    </div>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Employee Name Column</th>
                                    <th>Employee Code Column Name</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <!--Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Collumn Name</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-8">
                                    <div class="mb-1">
                                        <label class="form-label" for="title">Employe Detail Section</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="title" class="form-control" placeholder="Enter Employee Code Column Name" name="emp_code" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" class="form-check-input" name="emp_code_status" value="1"/>
                                        <label class="form-label" for="title">Employe Code Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="title" class="form-control" placeholder="Enter Employee Name Column" name="emp_name" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" class="form-check-input" name="emp_name_status" value="1"/>
                                        <label class="form-label" for="title">Employee Name Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="title" class="form-control" placeholder="Enter CNIC Column Name" name="cnic" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" class="form-check-input" name="cnic_status" value="1"/>
                                        <label class="form-label" for="title">CNIC Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="title" class="form-control" placeholder="Enter Designation Column Name" name="designation" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" class="form-check-input" name="desig_status" value="1"/>
                                        <label class="form-label" for="title">Designation Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="title" class="form-control" placeholder="Enter Department Column Name" name="dept" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" class="form-check-input" name="dept_status" value="1"/>
                                        <label class="form-label" for="title">Department Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <label class="form-label" for="title">Section 2</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="title" class="form-control" placeholder="Enter Last Working Day Column Name" name="lwd" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" class="form-check-input" name="lwd_status" value="1"/>
                                        <label class="form-label" for="title">Last Working Day Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="title" class="form-control" placeholder="Enter Notice Period Status Column Name" name="nps" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" class="form-check-input" name="nps_status" value="1"/>
                                        <label class="form-label" for="title">Notice Period Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="basic_salary" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" class="form-check-input" name="basic_salary_status" value="1"/>
                                        <label class="form-label" for="title">Basic Salary Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="ded_days" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="ded_days_status_edit" class="form-check-input" name="ded_days_status" value="1"/>
                                        <label class="form-label" for="title">Deduction Days Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="doj" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" class="form-check-input" name="doj_status" value="1"/>
                                        <label class="form-label" for="title">Date of Joining Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="title">Allowances</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="title">Deductions</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="days_payable" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="days_payable_status" value="1"/>
                                        <label class="form-label" for="title">Days Payable Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="unpaid" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="unpaid_status" value="1"/>
                                        <label class="form-label" for="title">Unpaid Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="leave_encash" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="leave_encash_status" value="1"/>
                                        <label class="form-label" for="title">Leave Encashment Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="extra_leaves" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="extra_leaves_status" value="1"/>
                                        <label class="form-label" for="title">Extra Leaves Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="overtime_1" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="overtime_1_status" value="1"/>
                                        <label class="form-label" for="title">Overtime 1 Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="notice_period" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="notice_period_status" value="1"/>
                                        <label class="form-label" for="title">Notice Period Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="overtime_2" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="overtime_2_status" value="1"/>
                                        <label class="form-label" for="title">Overtime 2 Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="eobi" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="eobi_status" value="1"/>
                                        <label class="form-label" for="title">EOBI Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="food" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="food_status" value="1"/>
                                        <label class="form-label" for="title">Food Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="tardiness" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="tardiness_status" value="1"/>
                                        <label class="form-label" for="title">Tardiness Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="arrears" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="arrears_status" value="1"/>
                                        <label class="form-label" for="title">Arrears Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="loan_advance" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="loan_advance_status" value="1"/>
                                        <label class="form-label" for="title">Loan/Advance Salary Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="others_1" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="others_1_status" value="1"/>
                                        <label class="form-label" for="title">Others 1 Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="others_2" id="" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" class="form-check-input" name="others_2_status" value="1"/>
                                        <label class="form-label" for="title">Others 2 Status</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Add Modal -->

        <!--start edit Modal -->
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Collumn Name</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-8">
                                    <div class="mb-1">
                                        <label class="form-label" for="title">Employees Datail Section</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="emp_code_edit" class="form-control" placeholder="Enter Employee Code Column Name" name="emp_code" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="emp_code_status_edit" class="form-check-input" name="emp_code_status" value="1"/>
                                        <label class="form-label" for="title">Employe Code Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="emp_name_edit" class="form-control" placeholder="Enter Employee Name Column" name="emp_name" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="emp_name_status_edit" class="form-check-input" name="emp_name_status" value="1"/>
                                        <label class="form-label" for="title">Employee Name Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="cnic_edit" class="form-control" placeholder="Enter CNIC Column Name" name="cnic" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="cnic_status_edit" class="form-check-input" name="cnic_status" value="1"/>
                                        <label class="form-label" for="title">CNIC Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="designation_edit" class="form-control" placeholder="Enter Designation Column Name" name="designation" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="designation_status_edit" class="form-check-input" name="desig_status" value="1"/>
                                        <label class="form-label" for="title">Designation Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="dept_edit" class="form-control" placeholder="Enter Department Column Name" name="dept" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="dept_status_edit" class="form-check-input" name="dept_status" value="1"/>
                                        <label class="form-label" for="title">Department Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <label class="form-label" for="title">Section 2</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="lwd_edit" class="form-control" placeholder="Enter Last Working Day Column Name" name="lwd" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="lwd_status_edit" class="form-check-input" name="lwd_status" value="1"/>
                                        <label class="form-label" for="title">Last Working Day Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <input type="text" id="nps_edit" class="form-control" placeholder="Enter Notice Period Status Column Name" name="nps" required/>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="nps_status_edit" class="form-check-input" name="nps_status" value="1"/>
                                        <label class="form-label" for="title">Notice Period Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="doj" id="doj_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="doj_status_edit" class="form-check-input" name="doj_status" value="1"/>
                                        <label class="form-label" for="title">Date of Joining Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="basic_salary" id="basic_salary_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="basic_salary_status_edit" class="form-check-input" name="basic_salary_status" value="1"/>
                                        <label class="form-label" for="title">Basic Salary Status</label>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="ded_days" id="ded_days_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mt-1">
                                        <input type="checkbox" id="ded_days_status_edit" class="form-check-input" name="ded_days_status" value="1"/>
                                        <label class="form-label" for="title">Deduction Days Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="title">Allowances</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="title">Deductions</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="days_payable" id="days_payable_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="days_payable_status_edit" class="form-check-input" name="days_payable_status" value="1"/>
                                        <label class="form-label" for="title">Days Payable Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="leave_encash" id="leave_encash_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="leave_encash_status_edit" class="form-check-input" name="leave_encash_status" value="1"/>
                                        <label class="form-label" for="title">Leave Encashment Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="extra_leaves" id="extra_leaves_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="extra_leaves_status_edit" class="form-check-input" name="extra_leaves_status" value="1"/>
                                        <label class="form-label" for="title">Extra Leaves Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="notice_period" id="notice_period_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="notice_period_status_edit" class="form-check-input" name="notice_period_status" value="1"/>
                                        <label class="form-label" for="title">Notice Period Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="unpaid" id="unpaid_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="unpaid_status_edit" class="form-check-input" name="unpaid_status" value="1"/>
                                        <label class="form-label" for="title">Unpaid Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="overtime_1" id="overtime_1_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="overtime_1_status_edit" class="form-check-input" name="overtime_1_status" value="1"/>
                                        <label class="form-label" for="title">Overtime 1 Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="overtime_2" id="overtime_2_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="overtime_2_status_edit" class="form-check-input" name="overtime_2_status" value="1"/>
                                        <label class="form-label" for="title">Overtime 2 Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="eobi" id="eobi_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="eobi_status_edit" class="form-check-input" name="eobi_status" value="1"/>
                                        <label class="form-label" for="title">EOBI Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="food" id="food_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="food_status_edit" class="form-check-input" name="food_status" value="1"/>
                                        <label class="form-label" for="title">Food Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="tardiness" id="tardiness_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="tardiness_status_edit" class="form-check-input" name="tardiness_status" value="1"/>
                                        <label class="form-label" for="title">Tardiness Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="arrears" id="arrears_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="arrears_status_edit" class="form-check-input" name="arrears_status" value="1"/>
                                        <label class="form-label" for="title">Arrears Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="loan_advance" id="loan_advance_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="loan_advance_status_edit" class="form-check-input" name="loan_advance_status" value="1"/>
                                        <label class="form-label" for="title">Loan/Advance Salary Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="others_1" id="others_1_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="others_1_status_edit" class="form-check-input" name="others_1_status" value="1"/>
                                        <label class="form-label" for="title">Others 1 Status</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <select class="select2 form-control" name="others_2" id="others_2_edit" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach ($payrollColumns as $column)
                                                <option value="{{ $column->name }}">{{ $column->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <input type="checkbox" id="others_2_status_edit" class="form-check-input" name="others_2_status" value="1"/>
                                        <label class="form-label" for="title">Others 2 Status</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">Reset</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End edit Modal -->
    </div>
</section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('final_settlement_setup.index') }}",
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'emp_name',
                        name: 'emp_name',
                    },
                    {
                        data: 'emp_code',
                        name: 'emp_code',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="delete_item('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary me-1',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    },
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Env Checks</h6>');

            // add record
            $("#add_form").submit(function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{route('final_settlement_setup.store')}}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        console.log(response);
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#add_form')[0].reset();
                            $("#add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Column Name has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Update record
            $("#edit_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{url('final_settlement_setup')}}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Column Name has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });
        });
        
        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('final_settlement_setup')}}" + "/" + id + "/edit",
                type: "get",
                success: function (response) {
                    $("#emp_code_edit").val(response.emp_code);
                    $("#emp_name_edit").val(response.emp_name);
                    $("#cnic_edit").val(response.cnic);
                    $("#designation_edit").val(response.designation);
                    $("#dept_edit").val(response.dept);
                    $("#doj_edit").val(response.doj).trigger('change.select2');
                    $("#lwd_edit").val(response.lwd);
                    $("#basic_salary_edit").val(response.basic_salary).trigger('change.select2');
                    $("#ded_days_edit").val(response.ded_days).trigger('change.select2');
                    $("#nps_edit").val(response.nps);
                    $("#days_payable_edit").val(response.days_payable).trigger('change.select2');
                    $("#unpaid_edit").val(response.unpaid).trigger('change.select2');
                    $("#leave_encash_edit").val(response.leave_encash).trigger('change.select2');
                    $("#extra_leaves_edit").val(response.extra_leaves);
                    $("#overtime_1_edit").val(response.overtime_1).trigger('change.select2');
                    $("#overtime_2_edit").val(response.overtime_2).trigger('change.select2');
                    $("#notice_period_edit").val(response.notice_period).trigger('change.select2');
                    $("#eobi_edit").val(response.eobi).trigger('change.select2');
                    $("#food_edit").val(response.food).trigger('change.select2');
                    $("#tardiness_edit").val(response.tardiness).trigger('change.select2');
                    $("#arrears_edit").val(response.arrears).trigger('change.select2');
                    $("#others_1_edit").val(response.others_1).trigger('change.select2');
                    $("#others_2_edit").val(response.others_2).trigger('change.select2');
                    $("#loan_advance_edit").val(response.loan_advance).trigger('change.select2');
                    if (response.emp_code_status == '1') {
                        $('#emp_code_status_edit').prop('checked', true);
                    }
                    if (response.emp_name_status == '1') {
                        $('#emp_name_status_edit').prop('checked', true);
                    }
                    if (response.cnic_status == '1') {
                        $('#cnic_status_edit').prop('checked', true);
                    }
                    if (response.desig_status == '1') {
                        $('#designation_status_edit').prop('checked', true);
                    }
                    if (response.dept_status == '1') {
                        $('#dept_status_edit').prop('checked', true);
                    }
                    if (response.doj_status == '1') {
                        $('#doj_status_edit').prop('checked', true);
                    }
                    if (response.lwd_status == '1') {
                        $('#lwd_status_edit').prop('checked', true);
                    }
                    if (response.basic_salary_status == '1') {
                        $('#basic_salary_status_edit').prop('checked', true);
                    }
                    if (response.ded_days_status == '1') {
                        $('#ded_days_status_edit').prop('checked', true);
                    }
                    if (response.nps_status == '1') {
                        $('#nps_status_edit').prop('checked', true);
                    }
                    if (response.leave_encash_status == '1') {
                        $('#leave_encash_status_edit').prop('checked', true);
                    }
                    if (response.extra_leaves_status == '1') {
                        $('#extra_leaves_status_edit').prop('checked', true);
                    }
                    if (response.overtime_1_status == '1') {
                        $('#overtime_1_status_edit').prop('checked', true);
                    }
                    if (response.overtime_2_status == '1') {
                        $('#overtime_2_status_edit').prop('checked', true);
                    }
                    if (response.days_payable_status == '1') {
                        $('#days_payable_status_edit').prop('checked', true);
                    }
                    if (response.notice_period_status == '1') {
                        $('#notice_period_status_edit').prop('checked', true);
                    }
                    if (response.unpaid_status == '1') {
                        $('#unpaid_status_edit').prop('checked', true);
                    }
                    if (response.eobi_status == '1') {
                        $('#eobi_status_edit').prop('checked', true);
                    }
                    if (response.food_status == '1') {
                        $('#food_status_edit').prop('checked', true);
                    }
                    if (response.tardiness_status == '1') {
                        $('#tardiness_status_edit').prop('checked', true);
                    }
                    if (response.arrears_status == '1') {
                        $('#arrears_status_edit').prop('checked', true);
                    }
                    if (response.others_1_status == '1') {
                        $('#others_1_status_edit').prop('checked', true);
                    }
                    if (response.others_2_status == '1') {
                        $('#others_2_status_edit').prop('checked', true);
                    }
                    if (response.loan_advance_status == '1') {
                        $('#loan_advance_status_edit').prop('checked', true);
                    }
                    
                    $("#edit_modal").modal("show");
                },
            });
        }
        function delete_item(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "final_settlement_setup/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else{
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Column Name has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
