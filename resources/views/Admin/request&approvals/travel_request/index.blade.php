@extends('Admin.layouts.master')
@section('title', 'Travel Requests')

@section('content')

    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Travel_Requests')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Travel_Requests')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="travel_requests-tab" data-bs-toggle="tab" href="#travel_requests" aria-controls="travel_requests" role="tab" aria-selected="true">Travel Requests</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" onclick="custom_fields()" id="custom-fields-tab" data-bs-toggle="tab" href="#custom_fields" aria-controls="custom_fields" role="tab" aria-selected="false">Custom Fields</a>
                    </li> --}}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="travel_requests" aria-labelledby="travel_requests-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="travel_requests_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Travel Type</th>
                                                    <th>Purpose</th>
                                                    <th>From</th>
                                                    <th>To</th>
                                                    <th>Travel Date</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!--Add Travel Requests Modal -->
                            <div class="modal fade text-start" id="add_travel_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Travel_Requests')</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <form class="form" id="add_travel_form" enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                            <select name="employee" id="employee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                                                <option value=""></option>
                                                                @foreach (employees() as $employee)
                                                                    <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="type">@lang('app.Means_Of_Transportation')</label>
                                                            <select name="type" id="type" class="select2 form-select" data-placeholder="Select Vehicle" required>
                                                                <option value=""></option>
                                                                <option value="Plane">@lang('app.Plane')</option>
                                                                <option value="Rail">@lang('app.Rail')</option>
                                                                <option value="Taxi">@lang('app.Taxi')</option>
                                                                <option value="Own Vehicle">@lang('app.Own_Vehicle')</option>
                                                                <option value="Rented">@lang('app.Rented')</option>
                                                                <option value="Other">@lang('app.Other')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="travel_from">@lang('app.Travel_From')</label>
                                                            <input type="text" id="travel_from" class="form-control" placeholder="@lang('app.Travel_From')" name="travel_from" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="travel_to">@lang('app.Travel_to')</label>
                                                            <input type="text" id="travel_to" class="form-control" placeholder="@lang('app.Travel_to')" name="travel_to" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="travel_date">@lang('app.Travel_Date')</label>
                                                            <input type="text" name="travel_date" id="travel_date" class="form-control flatpickr-basic flatpickr-input active" placeholder="YYYY-MM-DD" required="" readonly="readonly" style="user-select: auto;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="return_date">@lang('app.Return_Date')</label>
                                                            <input type="text" name="return_date" id="return_date" class="form-control flatpickr-basic flatpickr-input active" placeholder="YYYY-MM-DD" required="" readonly="readonly" style="user-select: auto;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="currency">@lang('app.Currency')</label>
                                                            <select name="currency" id="currency" class="select2 form-select" data-placeholder="@lang('app.Select_Currency')" required>
                                                                <option value=""></option>
                                                                @foreach ($currencies as $currency)
                                                                    <option value="{{$currency->id}}">{{$currency->code}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="funding">@lang('app.Total_Funds_Purposed')</label>
                                                            <input type="number" id="funding" class="form-control" placeholder="0.0" name="funding" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="mb-1">
                                                            <label for="purpose" class="form-label">@lang('app.Purpose_Of_Travel')</label>
                                                            <textarea name="purpose" placeholder="@lang('app.Remarks')" class="form-control" id="purpose" rows="2"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="attachment1">@lang('app.Attachment')</label>
                                                            <input type="file" id="attachment1" class="form-control" placeholder="" name="attachment1"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="attachment2">@lang('app.Attachment')</label>
                                                            <input type="file" id="attachment2" class="form-control" placeholder="" name="attachment2"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="attachment3">@lang('app.Attachment')</label>
                                                            <input type="file" id="attachment3" class="form-control" placeholder="" name="attachment3"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                                <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- <div class="tab-pane" id="custom_fields" aria-labelledby="custom-fields-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <table class="table" id="custom_fields_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Name</th>
                                                    <th>Display Status</th>
                                                    <th>Priority</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--Add Custom Fields Modal -->
                            <div class="modal fade text-start" id="add_custom_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel17">Add Custom Fields</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <form class="form" id="add_custom_form">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="type">Name</label>
                                                            <input type="text" id="type" class="form-control" placeholder="Name" name="name" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="display">Display Status</label>
                                                            <select name="display" id="display" class="select2 form-select" required>
                                                                <option value="">Select Status</option>
                                                                <option value="Show">Show</option>
                                                                <option value="Hidden">Hidden</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="field_type">Field Type</label>
                                                            <select name="field_type" id="field_type" class="select2 form-select" required>
                                                                <option value="">Select Field Type</option>
                                                                <option value="Text Field">Text Field</option>
                                                                <option value="Text Area">Text Area</option>
                                                                <option value="Select">Select</option>
                                                                <option value="Select2">Select2</option>
                                                                <option value="Multi Select">Multi Select</option>
                                                                <option value="File upload">File Upload</option>
                                                                <option value="Date">Date</option>
                                                                <option value="Date and Time">Date and Time</option>
                                                                <option value="File upload">File Upload</option>
                                                                <option value="Time">Time</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="type">Field Label</label>
                                                            <input type="text" id="type" class="form-control" placeholder="Label" name="field_label" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="field_validation">Validation</label>
                                                            <select name="field_validation" id="field_validation" class="select2 form-select" required>
                                                                <option value="">Select Validation</option>
                                                                <option value="Show">Required</option>
                                                                <option value="Hidden">None</option>
                                                                <option value="Hidden">Number</option>
                                                                <option value="Hidden">Number or Empty</option>
                                                                <option value="Hidden">Decimal</option>
                                                                <option value="Hidden">Email</option>
                                                                <option value="Hidden">Email or Empty</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="columns">Columns</label>
                                                            <button type="button" class="btn btn-primary" onclick="addColumns()">Add</button>
                                                            <button type="button" class="btn btn-outline-success" onclick="columnsReset()">Reset</button>
                                                            <div class="col-md-12 col-12" id="add_column_div">
                                                              <div class="mb-1">
                                                              </div>  
                                                          </div>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="type">Priority</label>
                                                            <input type="text" id="type" class="form-control" placeholder="Label" name="display_order" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="type">Display Section</label>
                                                            <input type="text" id="type" class="form-control" placeholder="Label" name="display_section" required/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div> --}}
                </div>
            </section>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            travel_requests_dataTable = $('#travel_requests_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('travel_requests.index') }}",
                    data: {
                        case: "travel_request"
                    }
                },
                columns: [
                    { data: 'responsive_id' },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        data : 'employeeName', 
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + ' ' + middleName + ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'type'
                    },
                    {
                        data: 'purpose'
                    },
                    {
                        data: 'travel_from'
                    },
                    {
                        data: 'travel_to'
                    },
                    {
                        data: 'travel_date'
                    },
                    {
                        data: 'status'
                    },
                    
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add Travel Requests',
                        className: 'btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_travel_modal'
                        },
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('#add_travel_form').submit(function (e){
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('case' , 'travel_request');
                $.ajax({
                    url: "{{route('travel_requests.store')}}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#add_travel_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_travel_modal").modal("hide");
                            travel_requests_dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Travel Request has been Added Successfully!'
                            })
                        }
                    }
                });
            });
            $('#travel_requests div.head-label').html('<h6 class="mb-0">List of Travel Request</h6>');
            

            // $('#add_custom_form').submit(function (e){
            //     e.preventDefault();
            //     var formData = new FormData(this);
            //     formData.append('case' , 'custom_fields');
            //     $.ajax({
            //         url: "{{route('travel_requests.store')}}",
            //         type: "POST",
            //         data: formData,
            //         processData: false,
            //         contentType: false,
            //         success: function (response){
            //             console.log(response);
            //             if(response.errors){
            //                 $.each( response.errors, function( index, value ){
            //                     Toast.fire({
            //                         icon: 'error',
            //                         title: value
            //                     })
            //                 });
            //             }
            //             else if(response.error_message){
            //                 Toast.fire({
            //                     icon: 'error',
            //                     title: 'An error has been occured! Please Contact Administrator.'
            //                 })
            //             }
            //             else{
            //                 $('#add_custom_form')[0].reset();
            //                 $("#add_custom_modal").modal("hide");
            //                 custom_fields_dataTable.ajax.reload();
            //                 Toast.fire({
            //                     icon: 'success',
            //                     title: 'Travel Request has been Added Successfully!'
            //                 })
            //             }
            //         }
            //     });
            // });
        });
        // function custom_fields(){
        //     // Custom Fields DataTable
        //     custom_fields_dataTable = $('#custom_fields_dataTable').DataTable({
        //         processing: true,
        //         serverSide: true,
        //         responsive: true,
        //         ordering : false,
        //         destroy: true,
        //         ajax: {
        //             url: "{{ route('travel_requests.index') }}",
        //             data: {
        //                 case: "custom_fields"
        //             }
        //         },
        //         columns: [
        //             { data: 'responsive_id' },
        //             {
        //                 data: 'name'
        //             },
        //             {
        //                 data: 'display'
        //             },
        //             {
        //                 data: 'display_order'
        //             },
                    
        //         ],
        //         "columnDefs": [
        //             {
		// 				// For Responsive
		// 				className: 'control',
		// 				orderable: false,
		// 				targets: 0
		// 			},
        //             {
        //                 "defaultContent": "-",
        //                 "targets": "_all"
        //             }
        //         ],
        //         "order": [[0, 'asc']],
        //         dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        //         displayLength: 10,
        //         lengthMenu: [10, 25, 50, 75, 100],
        //         buttons: [
        //             {
        //                 extend: 'collection',
        //                 className: 'btn btn-outline-secondary dropdown-toggle me-2',
        //                 text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
        //                 buttons: [
        //                     {
        //                     extend: 'print',
        //                     text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
        //                     className: 'dropdown-item',
        //                     action: newexportaction,
                                // exportOptions: { columns: [0,1] }
        //                     },
        //                     {
        //                     extend: 'csv',
        //                     text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
        //                     className: 'dropdown-item',
        //                     action: newexportaction,
                                // exportOptions: { columns: [0,1] }
        //                     },
        //                     {
        //                     extend: 'excel',
        //                     text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
        //                     className: 'dropdown-item',
        //                     action: newexportaction,
                                // exportOptions: { columns: [0,1] }
        //                     },
        //                     {
        //                     extend: 'pdf',
        //                     text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
        //                     className: 'dropdown-item',
        //                     action: newexportaction,
                                // exportOptions: { columns: [0,1] }
        //                     },
        //                     {
        //                     extend: 'copy',
        //                     text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
        //                     className: 'dropdown-item',
        //                     action: newexportaction,
                                // exportOptions: { columns: [0,1] }
        //                     }
        //                 ],
        //                 init: function (api, node, config) {
        //                     $(node).removeClass('btn-secondary');
        //                     $(node).parent().removeClass('btn-group');
        //                     setTimeout(function () {
        //                     $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
        //                     }, 50);
        //                 }
        //             },
        //             {
        //                 text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add Custom Fields',
        //                 className: 'btn btn-primary',
        //                 attr: {
        //                     'data-bs-toggle': 'modal',
        //                     'data-bs-target': '#add_custom_modal'
        //                 },
        //             }
        //         ],
        //         responsive: {
		// 			details: {
		// 				display: $.fn.dataTable.Responsive.display.childRowImmediate,
		// 				type: 'column',
		// 			}
		// 		},
        //         language: {
        //             paginate: {
        //             // remove previous & next text from pagination
        //             previous: '&nbsp;',
        //             next: '&nbsp;'
        //             }
        //         }
        //     });

        //     $('#custom_fields div.head-label').html('<h6 class="mb-0">List of Custom Fields</h6>');
        // }
        // function addColumns() {
        //     $('#add_column_div').append(
        //             "<div class='reset_id'>\
        //             <div class='row'>\
        //             <div class='col-md-6'>\
        //             <div class='mb-1'>\
        //             <label class='form-label'>Label</label>\
        //             <input class='form-control' type='text' placeholder='Label' name='add_column_label'>\
        //             </div>\
        //             </div>\
        //             <div class='col-md-6'>\
        //             <div class='mb-1'>\
        //             <label class='form-label'>Value</label>\
        //             <input class='form-control' type='text' placeholder='Label' name='add_column_value'>\
        //             </div>\
        //             </div>\
        //             </div>\
        //             <div>\
        //             "
        //     );
        // }
        // function columnsReset(){
        //     $('.reset_id').remove();
        // }
    </script>
@endsection