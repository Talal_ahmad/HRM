@extends('Admin.layouts.master')
@section('title', 'Bulk Overtime Management')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Bulk_Overtime_Management')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-head">
                            <div class="row">
                                <div class="col-12 mt-1 mb-0 mx-2">
                                    <h5>@lang('app.Add_Bulk_Overtime_Management')</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form id="add_form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label">@lang('app.Departments'):</label>
                                            <select name="department" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee">@lang('app.Employees')</label>
                                            <select name="employee" id="employee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
    
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label">@lang('app.From_Date'):</label>
                                            <input type="date" name="fromDate" id="fromDate" class="form-control" placeholder="YYYY-MM-DD" required readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label">@lang('app.To_Date'):</label>
                                            <input type="date" name="toDate" id="toDate" class="form-control" placeholder="YYYY-MM-DD" required readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row check_all mt-1" style="display: none; margin-left: 2px;">
                                    <div class="col-md-12 form-check">
                                        <input type="checkbox" class="form-check-input" name="check_all" id="check_all" value="check_all" />
                                        <label class="custom-control-label" for="check_all">@lang('app.Check_all')</label>
                                    </div>
                                </div>
                                <div class="row detail_table" style="display: none" id="append_data">
                                    
                                </div>
                                <div class="row submit_btn" style="display: none;">
                                    <div class="text-start">
                                        <button type="submit" class="btn btn-success" id="save">@lang('app.Submit')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#department").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employee').empty();
                        $('#employee').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employee').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + '-' + value.employee_code + '-' + value.first_name +' '+value.middle_name +' '+ value.last_name + ' - ' + value.designation+ ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
                $('#fromDate').attr('readonly', false);
                $('#toDate').attr('readonly', false);
            }); 
            $('#toDate').on('change', function(){
                var employee = $('#employee').val();
                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                if(employee == '' || employee == 'undefined' || fromDate == '')
                {
                    Toast.fire({
                        icon: 'error',
                        title: 'Please Select Above All Fields!'
                    })
                    $('#fromDate').val('');
                    $('#toDate').val('');
                    return false;
                }
                $.ajax({
                    url: "{{ route('bulk_overtime_management.create') }}",
                    type:"GET",
                    data:{employee , fromDate , toDate},
                    success: function(response){
                        console.log(response);
                        if(response.code == 500)
                        {
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            })
                        }
                        if(response.code == 300)
                        {
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            })
                        }
                        else{
                            $('.detail_table').show();
                            var html = '';
                            var check = true;
                            $.each(response , function(index , value){
                                if(value.in_time != '' && value.out_time != ''){
                                    check = false;
                                    html += `
                                        <div class="col-12 mt-1 mb-1">
                                            <div class="table-responsive"  >
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Overtime Date</th>
                                                            <th>Shift Type</th>
                                                            <th>Work Week</th>
                                                            <th>Shift Start Time</th>
                                                            <th>Shift End Time</th>
                                                            <th>In time</th>
                                                            <th>Out Time</th>
                                                            <th>Overtime</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                    `;
                                    html += `
                                                        <tr>
                                                            <td>${index}</td>
                                                            <td>${value.shift_name}</td>    
                                                            <td>${value.work_week}</td>    
                                                            <td>${value.shift_start_time}</td>    
                                                            <td>${value.shift_end_time}</td>    
                                                            <td>${value.in_time}</td>    
                                                            <td>${value.out_time}</td>    
                                                            <td id="overtime_val_${index}" clas="overtime_val">${value.overtime}</td>   
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class='row selection_div'>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="start_datetime_${index}">Start DateTime</label>
                                                    <input type="datetime-local" name="${index}[start_datetime]" id="start_datetime_${index}" value="${value.shift_end_datetime}" class="form-control start_datetime" placeholder="YYYY-MM-DD HH:MM"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="end_datetime_${index}">End DateTime</label>
                                                    <input type="datetime-local" name="${index}[end_datetime]" id="end_datetime_${index}" onchange="change_date(event , '#start_datetime_${index}' , ${employee} , '${value.shift_start_time}' , '${value.shift_end_time}' , '${value.in_time}' , '${value.out_time}' , '${index}' , '#overtime_val_${index}')" value="${value.out_time}" class="form-control end_datetime" placeholder="YYYY-MM-DD HH:MM"/>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label for="type" class="form-label">Remarks</label>
                                                    <textarea name="${index}[remarks]" placeholder="Remarks ..." class="form-control" id="type" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="${index}[status]" id="type" value="Approved" />
                                                        <label class="custom-control-label" for="type">Request Approve Now</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    `;
                                    $('.submit_btn').show();
                                    $('.check_all').show();
                                }
                            });
                            $('#append_data').html(html);
                            if(check)
                            {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Oops! Employee is Absent'
                                });   
                            }
                        }
                    }
                });
            })
            $("#check_all").on("change", function(){
                if($(this).is(":checked")){
                    $("input[type='checkbox']").prop('checked', true);  
                }
                else{
                    $("input[type='checkbox']").prop('checked', false);
                }
            });
            // Add Data
            $('#add_form').on('submit' , function(e){
                e.preventDefault();
                $.ajax({
                    url: "{{route('bulk_overtime_management.store')}}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response){
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else if(response.code == 300){
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            })
                        }
                        else{
                            Toast.fire({
                                icon: 'success',
                                title: 'Overtime has been added Successfully!'
                            })
                            setTimeout(function(){
                                window.location.reload();
                            }, 2000);
                        }
                    }
                });
            });
        });

        function change_date(e , start_date , employee , shift_start_time , shift_end_time , in_time , out_time , overtime_date , overtime_id) {
            var start_datetime = $(start_date).val();
            var end_datetime = e.target.value;
            var shift_end_datetime = overtime_date + ' ' + shift_end_time;
            HandleOverTime(start_datetime , end_datetime , overtime_id);
            $.ajax({
                url: "{{url('check_bulk_overtime')}}",
                type: 'GET',
                data: {
                    start_datetime , end_datetime , employee , shift_start_time , shift_end_time , in_time , out_time
                },
                success: function(response){
                    if(response.code == 300){
                        Toast.fire({
                            icon: 'error',
                            title: response.message
                        })
                    }
                }
            });
        }

        function HandleOverTime(shift_end_datetime , end_datetime , overtime_id){
            var end_time = new Date(shift_end_datetime);
            var over_time = new Date(end_datetime);
            diff = over_time.getTime() - end_time.getTime();
            hour = Number(diff/3600000).toFixed(2);
            $(overtime_id).text(hour);
        }

    </script>
@endsection