@extends('Admin.layouts.master')
@section('title', 'Short Leave Requests')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Short_Leaves')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Short_Leaves')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <!--Search Form -->
                            @if(userRoles()!="Self Service")
                                <div class="card-body">
                                    <form id="search_form">
                                        <div class="row g-1 mb-md-1">
                                            <div class="col-md-5">
                                                <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                                <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
                                                    <option value=""></option>
                                                    @foreach (employees() as $employee)
                                                        <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.From_Date'):</label>
                                                <input type="text" name="fromDateFilter" id="fromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.To_Date'):</label>
                                                <input type="text" name="toDateFilter" id="toDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div style="margin-top:37px" class="col-md-1">
                                                <button class="btn btn-primary">@lang('app.Filter')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Department/Branch</th>
                                        <th>Date</th>
                                        <th>Time From</th>
                                        <th>Time To</th>
                                        <th>Remarks</th>
                                        <th>Status</th>
                                        <th>Requested DateTime</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1"
                aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Short_Leave_Request')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee">@lang('app.Employees')</label>
                                            <select name="employee_id" id="employee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                                <option value="">Select Employee</option>
                                                @foreach (employees() as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @if(env('COMPANY') == 'JSML')
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="leave_type">@lang('app.Leave_Types')</label>
                                                <select name="leave_type" id="leave_type" class="select2 form-select" data-placeholder="@lang('app.Select_Type')">
                                                    <option value="">@lang('app.Select_Type')</option>
                                                    <option value="Medical">@lang('app.Medical')</option>
                                                    <option value="Other">@lang('app.Other')</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="date">@lang('app.Date')</label>
                                            <input type="text" name="date" id="date" class="form-control flatpickr-basic flatpickr-input active" placeholder="YYYY-MM-DD" required="" readonly="readonly" style="user-select: auto;">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="time_from">@lang('app.Time_From')</label>
                                            <input type="text" name="time_from" id="time_from" class="form-control flatpickr-time text-start" placeholder="HH:MM" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="time_to">@lang('app.Time_To')</label>
                                            <input type="text" name="time_to" id="time_to" class="form-control flatpickr-time text-start" placeholder="HH:MM" required/>
                                        </div>
                                    </div>
                                    @if(env('COMPANY') == 'JSML')
                                        <div class="col-12" id="deduction_leave_type_div" style="display: none">
                                            <div class="mb-1">
                                                <label class="form-label" for="deduction_leave_type_id">@lang('app.Deduction_Leave_Type')</label>
                                                <select name="deduction_leave_type_id" id="deduction_leave_type_id" data-placeholder="@lang('app.Select_Leave_Type')" class="select2 form-select">
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="attachment">@lang('app.Attachment')</label>
                                            <input type="file" id="attachment" class="form-control" placeholder="" name="image"/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="type" class="form-label">@lang('app.Remarks')</label>
                                            <textarea name="remarks" placeholder="@lang('app.Remarks')" class="form-control" id="type" rows="3"></textarea>
                                        </div>
                                    </div>
                                    @can('Short Leave Request Approve Now')    
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="status" id="type" value="Approved" />
                                                    <label class="custom-control-label" for="type">@lang('app.Request_Approve_Now')</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1"
                aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Short_Leave_Request')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="editEmployee">@lang('app.Employee')</label>
                                            <select name="employee_id" id="editEmployee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                                <option value=""></option>
                                                @foreach (employees() as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @if(env('COMPANY') == 'JSML')
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_leave_type">@lang('app.Leave_Types')</label>
                                                <select name="leave_type" id="edit_leave_type" class="select2 form-select" data-placeholder="@lang('app.Select_Type')">
                                                    <option value="">@lang('app.Select_Type')</option>
                                                    <option value="Medical">@lang('app.Medical')</option>
                                                    <option value="Other">@lang('app.Other')</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="editDate">@lang('app.Date')</label>
                                            <input type="text" name="date" id="editDate" class="form-control flatpickr-basic flatpickr-input active" placeholder="YYYY-MM-DD" required readonly="readonly" style="user-select: auto;">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_time_from">@lang('app.Time_From')</label>
                                            <input type="text" name="time_from" id="edit_time_from" class="form-control flatpickr-time text-start" placeholder="HH:MM" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_time_to">@lang('app.Time_To')</label>
                                            <input type="text" name="time_to" id="edit_time_to" class="form-control flatpickr-time text-start" placeholder="HH:MM" required/>
                                        </div>
                                    </div>
                                    @if(env('COMPANY') == 'JSML')
                                        <div class="col-12" id="edit_deduction_leave_type_div" style="display: none">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_deduction_leave_type_id">@lang('app.Deduction_Leave_Type')</label>
                                                <select name="deduction_leave_type_id" id="edit_deduction_leave_type_id" data-placeholder="@lang('app.Select_Leave_Type')" class="select2 form-select">
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="editRemarks" class="form-label">@lang('app.Remarks')</label>
                                            <textarea name="remarks" placeholder="@lang('app.Remarks')" class="form-control" id="editRemarks" rows="3"></textarea>
                                        </div>
                                    </div>
                                    @can('Short Leave Request Approve Now')    
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="status" id="editStatus" value="Approved" />
                                                    <label class="custom-control-label" for="editStatus">@lang('app.Request_Approve_Now')</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Update')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        var rowid;
        var edit_deduction_leave_type_id = 0;
        var company = "{{env('COMPANY')}}";
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('short_leave_requests.index') }}",
                    data: function (filter) {
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.fromDateFilter = $('#fromDateFilter').val();
                        filter.toDateFilter = $('#toDateFilter').val();
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        data: 'employeeName',
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + ' ' + middleName + ' ' + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'title',
                        name: 'companystructures.title',
                    },
                    {
                        data: 'date',
                        name: 'short_leave_requests.date',
                    },
                    {
                        data: 'time_from',
                        name: 'short_leave_requests.time_from',
                    },
                    {
                        data: 'time_to',
                        name: 'short_leave_requests.time_to',
                    },
                    {
                        data: 'remarks',
                        name: 'short_leave_requests.remarks',
                    },
                    {
                        data: 'status',
                        name: 'short_leave_requests.status',
                    },
                    {
                        data: 'created_at' , render : function(data , type , row){
                            var date = new Date(row.created_at),
                                        month = String(date.getMonth() + 1).padStart(2, '0'),
                                        day = String(date.getDate() + 1).padStart(2, '0'),
                                        year = date.getFullYear();
                            return year+"-"+month+"-"+day;
                        }
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" title="Edit Short Leave" class="item-edit" onclick=short_leave_request_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" title="Delete Short Leave" onclick="short_leave_request_delete(' +
                                full.id + ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            if(company == 'JSML'){
                $("#employee").change(function() { 
                    var optVal = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_emp_leave') }}",
                        data: {
                            employee: optVal,
                        },
                        success: function(response) {
                            $('#deduction_leave_type_id').empty();
                            $('#deduction_leave_type_id').html('<option value="">Select Type</option>'); 
                            $.each(response, function(index, value) {
                                $('#deduction_leave_type_id').append(
                                    $('<option></option>').val(value.id).html(
                                    value.name )
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                });
                $("#editEmployee").on('change', function() { 
                    var optVal = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_emp_leave') }}",
                        data: {
                            employee: optVal,
                        },
                        success: function(response) {
                            $('#edit_deduction_leave_type_id').empty();
                            $('#edit_deduction_leave_type_id').html('<option value="">Select Type</option>'); 
                            $.each(response, function(index, value) {
                                let selected = edit_deduction_leave_type_id === value.id ? 'selected' : '';
                                $('#edit_deduction_leave_type_id').append(
                                    $(`<option ${selected}></option>`).val(value.id).html(
                                    value.name )
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                });
                $("#time_from").on("change", function() {
                    calculateTimeDifference('#date', '#time_from', '#time_to', '#deduction_leave_type_div', '#deduction_leave_type_id');
                });
                $("#time_to").on("change", function() {
                    calculateTimeDifference('#date', '#time_from', '#time_to', '#deduction_leave_type_div', '#deduction_leave_type_id');
                });
                $("#edit_time_from").on("change", function() {
                    calculateTimeDifference('#editDate', '#edit_time_from', '#edit_time_to', '#edit_deduction_leave_type_div', '#edit_deduction_leave_type_id');
                });
                $("#edit_time_to").on("change", function() {
                    calculateTimeDifference('#editDate', '#edit_time_from', '#edit_time_to', '#edit_deduction_leave_type_div', '#edit_deduction_leave_type_id');
                });
            }
            // Add Data
            $('#add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('short_leave_requests.store') }}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.code == 200){
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: response.message,
                            });
                        }
                        else if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: response.error_message,
                            });
                        } 
                        else if (response.code == 300) {
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            })
                        }
                        else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            });
                        }
                    }
                });
            })
            // Update Data
            $('#edit_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('short_leave_requests') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.code == 200){
                            $('#edit_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#edit_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Short Leave Request has been Updated Successfully!'
                            })
                        }
                        else if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            });
                        }
                    }
                });
            });
            $('div.head-label').html('<h6 class="mb-0">List of Short Leave Requests</h6>');
        });

        if(company == 'JSML'){
            function calculateTimeDifference(dateInput, time_from, time_to, deduction_leave_type_div, deduction_leave_type_id) {
                const date = $(dateInput).val();
                if(date == '' || date == null){
                    Toast.fire({
                        icon: 'error',
                        title: 'Please Select Date!'
                    });
                    return false;
                }
                const timeFromValue = $(time_from).val();
                const timeToValue = $(time_to).val();

                if (timeFromValue && timeToValue) {
                    const timeFrom = new Date(`${date} ${timeFromValue}`);
                    const timeTo = new Date(`${date} ${timeToValue}`);
                    const timeDifference = (timeTo - timeFrom) / (1000 * 60 * 60);

                    // Show the hidden div if the difference is 4 hours or greater
                    if (timeDifference >= 4) {
                        $(deduction_leave_type_div).show();
                        $(deduction_leave_type_id).attr("required", true);
                    } else {
                        $(deduction_leave_type_id).val("").trigger('change');
                        $(deduction_leave_type_id).attr("required", false);
                        $(deduction_leave_type_div).hide();
                    }
                }
            }
        }

        function short_leave_request_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('short_leave_requests') }}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    edit_deduction_leave_type_id = response.deduction_leave_type_id;
                    $('#editEmployee').val(response.employee_id).select2().trigger('change');
                    $('#editDate').val(response.date);
                    $('#edit_time_from').val(response.time_from);
                    $('#edit_time_to').val(response.time_to);
                    $('#editRemarks').text(response.remarks);
                    if (response.status == 'Approved') {
                        $("#editStatus").prop('checked', true);
                    }
                    if(company == 'JSML'){
                        $('#edit_leave_type').val(response.leave_type).select2();
                        calculateTimeDifference('#editDate', '#edit_time_from', '#edit_time_to', '#edit_deduction_leave_type_div','#edit_deduction_leave_type_id');
                    }
                    $('#edit_modal').modal('show');
                }
            });
        }

        function short_leave_request_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "short_leave_requests/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Short Leave Request has been Deleted Successfully!'
                                        });
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Filter Function
        $('#search_form').submit(function(e){
            datatable.draw();
            e.preventDefault();
        });
    </script>
@endsection
