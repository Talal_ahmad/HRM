@extends('Admin.layouts.master')
@section('title', 'Miscellaneous Requests')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Miscellaneous_Requests')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Miscellaneous_Requests')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            @if(userRoles()!="Self Service")
                                <!--Search Form -->
                                <div class="card-body">
                                    <form id="search_form">
                                        <div class="row g-1 mb-md-1">
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.Department'):</label>
                                                <select name="departmentFilter" id="departmentFilter"
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                    <option value="">@lang('app.Select_Department')</option>
                                                    <option value="all">All</option>
                                                    @foreach (departments() as $department)
                                                        <option value="{{ $department->id }}">{{ $department->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                                <select name="employeeFilter" id="employeeFilter" class="select2 form-select"
                                                    data-placeholder="@lang('app.Select_Employee')">

                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.From_Date'):</label>
                                                <input type="text" name="fromDateFilter" id="fromDateFilter"
                                                    class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.To_Date'):</label>
                                                <input type="text" name="toDateFilter" id="toDateFilter"
                                                    class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div style="margin-top:35px" class="col-md-3">
                                                <button class="btn btn-primary">@lang('app.Filter')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Requested Date</th>
                                        <th>Department/Branch</th>
                                        <th>Request Type</th>
                                        <th>Description</th>
                                        <th>Feed Back</th>
                                        <th>Status</th>
                                        <th class="not_include">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1"
                aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Miscellaneous_Request')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee_id">@lang('app.Employee')</label>
                                            <select name="employee_id" id="employee_id" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Employee')" required>
                                                <option value=""></option>
                                                @foreach (employees() as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="request_type">@lang('app.Request_Types')</label>
                                            <select name="request_type" id="request_type" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Request_Type')">
                                                <option value=""></option>
                                                @foreach ($miscellaneous_request_types as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="description" class="form-label">@lang('app.Description')</label>
                                            <textarea name="description" placeholder="@lang('app.Description')" class="form-control" id="description" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="attachment">@lang('app.Attachments')</label>
                                            <input type="file" id="attachment" class="form-control" placeholder="" name="image"/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="feedback" class="form-label">@lang('app.Feedback')</label>
                                            <textarea name="feedback" placeholder="@lang('app.Feedback')" class="form-control" id="feedback" rows="3"></textarea>
                                        </div>
                                    </div>
                                    @can('Miscellaneous Request Approve Now')    
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="status"
                                                        id="status" value="Approved" />
                                                    <label class="custom-control-label" for="type">@lang('app.Request_Approve_Now')</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1"
                aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Miscellaneous_Request')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="mb-1">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_employee_id">@lang('app.Employee')</label>
                                                <select name="employee_id" id="edit_employee_id" class="select2 form-select"
                                                    data-placeholder="@lang('app.Select_Employee')" required>
                                                    <option value=""></option>
                                                    @foreach (employees() as $employee)
                                                        <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_request_type">@lang('app.Request_Types')</label>
                                            <select name="request_type" id="edit_request_type" class="select2 form-select"
                                                data-placeholder="Select Request Type">
                                                <option value=""></option>
                                                @foreach ($miscellaneous_request_types as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_active">@lang('app.Status')</label>
                                            <select name="active" id="edit_active" class="select2 form-select"
                                                data-placeholder="Select Status">
                                                <option value=""></option>
                                                <option value="1">@lang('app.Active')</option>
                                                <option value="0">@lang('app.In_Active')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="edit_description" class="form-label">@lang('app.Description')</label>
                                            <textarea name="description" placeholder="@lang('app.Description')" class="form-control" id="edit_description" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="edit_feedback" class="form-label">@lang('app.Feedback')</label>
                                            <textarea name="feedback" placeholder="@lang('app.Feedback')" class="form-control" id="edit_feedback" rows="3"></textarea>
                                        </div>
                                    </div>
                                    @can('Miscellaneous Request Approve Now')    
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="status"
                                                        id="edit_status" value="Approved" />
                                                    <label class="custom-control-label" for="status">@lang('app.Request_Approve_Now')</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Update')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: true,
                ajax: {
                    url: "{{ route('miscellaneous.index') }}",
                    data: function (filter) {
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.fromDateFilter = $('#fromDateFilter').val();
                        filter.toDateFilter = $('#toDateFilter').val();
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        data: 'employeeName',
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + ' ' + middleName + ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'created_at', 
                        render : function(data , type , row){
                            var date = new Date(row.created_at);
                            return date.getFullYear() + "-" + parseInt(date.getMonth() + 1) + "-" + date.getDate();
                        }
                    },
                    {
                        data: 'title',
                        name: 'companystructures.title',
                    },
                    {
                        data: 'request_type',
                        name: 'miscellaneous_request_type.name',
                    },
                    {
                        data: 'description',
                        name: 'miscellaneous.description',
                    },
                    {
                        data: 'feedback',
                        name: 'miscellaneous.feedback',
                    },
                    {
                        data: 'status',
                        name: 'miscellaneous.status',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=miscellaneous_request_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="miscellaneous_request_delete(' +
                                full.id + ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Miscellaneous Request</h6>');

            // Filter department Employees
            $('#departmentFilter').on('change', function(){
                var department_id = $(this).val();
                var employee_id = '#employeeFilter';
                get_department_employees(department_id,employee_id);
            });
            // Add Data
            $("#add_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('miscellaneous.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Miscellaneous Request has been Added Successfully!'
                            })
                        }
                    },
                    error: function(jqXHR, exception)
                    {
                        $.unblockUI();
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                });
            });

            // Update Data
            $("#edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('miscellaneous') }}" + "/" + rowid,
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#edit_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#edit_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Miscellaneous Request has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
        });

        function miscellaneous_request_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('miscellaneous') }}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    $("#edit_employee_id").val(response.employee_id).select2();
                    $("#edit_request_type").val(response.request_type).select2();
                    $("#edit_active").val(response.active).select2();
                    $("#edit_description").val(response.description);
                    $("#edit_feedback").val(response.feedback);
                    if (response.status == 'Approved') {
                        $('#edit_status').prop('checked', true);
                    }
                    $("#edit_modal").modal("show");
                },
            });
        }

        function miscellaneous_request_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "miscellaneous/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Miscellaneous Requests has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Get Employees
        function get_department_employees(department_id, employee_id)
        {
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: "{{url('get_dept_employees')}}",
                data: {
                    department_id : department_id
                },
                success: function(data)
                {
                    $(employee_id).attr('disabled', false);
                    $(employee_id).html('');
                    $(employee_id).html('<option value="">Select Employee</option>'); 
                    $.each(data, function(index,employee){
                        var opt = $('<option>');
                        opt.val(employee.id);
                        opt.text(employee.employee_id+' - '+employee.employee_code+' - '+employee.first_name+' '+employee.middle_name+' '+employee.last_name+' '+employee.designation+' '+employee.department);
                        $(employee_id).append(opt);
                    });
                }
            });
        }
        
        // Filter Function
        $('#search_form').submit(function(e) {
            datatable.draw();
            e.preventDefault();
        });
    </script>
@endsection
