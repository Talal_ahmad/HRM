@extends('Admin.layouts.master')
@section('title', 'Loan Requests')
@section('content')
<style>
    td{
        font-size: 10px;
    }
    /* th{
        font-size: 10px;
    } */
</style> 
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Employee_Loan')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Employee_Loan')
                            </li>
                        </ol>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        @if(userRoles()!="Self Service")
                            <!--Search Form --> 
                            <div class="card-body">
                                <form id="search_form"> 
                                    <div class="row g-1 mb-md-1">
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if (env('COMPANY') == 'JSML')        
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="title">@lang('app.Section')</label>
                                                    <select name="section[]" id="section" data-placeholder="@lang('app.Section')" class="select2 form-select" multiple required>
                                                        
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                    </div>
                                                </div>
                                            </div>    
                                        @endif
                                        <div class="col-md-3">
                                            <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
                                                <option value=""></option>
                                                @foreach (employees('','loanRequest', true) as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="typeFilter">@lang('app.Type')</label>
                                            <select name="typeFilter" id="typeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Type')">
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach ($loan_types as $loan_type)
                                                    <option value="{{$loan_type->id}}" {{$loan_type->id == request('typeFilter') ? 'selected' : ''}}>{{$loan_type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="fromDateFilter" class="form-label">@lang('app.Request_From'):</label>
                                            <input type="text" name="fromDateFilter" id="fromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                        <div for="toDateFilter" class="col-md-3">
                                            <label class="form-label">@lang('app.Request_To'):</label>
                                            <input type="text" name="toDateFilter" id="toDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                        <div for="dedFromDateFilter" class="col-md-3">
                                            <label class="form-label">@lang('app.Deduction_From'):</label>
                                            <input type="text" name="dedFromDateFilter" id="dedFromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                        <div for="dedToDateFilter" class="col-md-3">
                                            <label class="form-label">@lang('app.Deduction_To'):</label>
                                            <input type="text" name="dedToDateFilter" id="dedToDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="statusFilter">@lang('app.Status')</label>
                                            <select name="statusFilter" id="statusFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Status')">
                                                <option value=""></option>
                                                <option value="unpaid">@lang('app.Unpaid')</option>
                                                <option value="paid">@lang('app.Paid')</option>
                                            </select>
                                        </div>
                                        <div style="margin-top:35px" class="col-md-3">
                                            <a href="{{url('loan_request')}}" type="button" class="btn btn-danger">@lang('app.Reset')</a>
                                            <button class="btn btn-primary" type="submit">@lang('app.Filter')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th style="font-size: 9px;">Sr.<br>No</th>
                                    <th style="font-size: 9px;">Emp. ID</th>
                                    <th style="font-size: 9px;">Emp. Code</th>
                                    <th style="font-size: 9px;">Emp.</th>
                                    <th style="font-size: 9px;">First Name</th>
                                    <th style="font-size: 9px;">Last Name</th>
                                    <th style="font-size: 9px;">Dept.</th>
                                    <th style="font-size: 9px;">Type</th>
                                    <th style="font-size: 9px;">Loan Status</th>
                                    <th style="font-size: 9px;">Start Date</th>
                                    <th style="font-size: 9px;">T. Instls.</th>
                                    <th style="font-size: 9px;">Emp. Status</th>
                                    <th style="font-size: 9px;">Loan Amt.</th>
                                    <th style="font-size: 9px;">Pending Amount(Opening Balance)</th>
                                    <th style="font-size: 9px;">Remaining</th>
                                    <th style="font-size: 9px;">Requested Date Time</th>
                                    <th style="font-size: 9px;">Remarks</th>
                                    @if (env('COMPANY')=='RoofLine')
                                        <th style="font-size: 9px;">Calculation Groups</th>
                                    @endif
                                    <th style="font-size: 9px;">Recovery Status</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        
        <!--Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Loan_Request')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="loan_request_add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="employee_id">@lang('app.Employee')</label>
                                        <select name="employee_id" id="employee_id" class="select2 trig form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                            <option value=""></option>
                                            @foreach (employees('', '', true) as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="show_type">@lang('app.Loan_Type')</label>
                                        <select name="loan_type" id="show_type" class="select2 trig form-select" data-placeholder="@lang('app.Select_Type')" required>
                                            <option value=""></option>
                                            @foreach ($loan_types as $loan_type)
                                                <option value="{{$loan_type->id}}">{{$loan_type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="start_date">@lang('app.Start_Date')</label>
                                        <input type="text" name="start_date" id="start_date" class="form-control flatpickr-basic flatpickr-input active" placeholder="YYYY-MM-DD" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="amount">@lang('app.Amount')</label>
                                        <input type="number" id="amount" class="form-control " placeholder="@lang('app.Amount')" name="amount" required>
                                    </div> 
                                </div>  
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="time_duration">@lang('app.Time_Duration(Months)')</label>
                                        <input type="number" id="time_duration" class="form-control " placeholder="@lang('app.Month')" name="time_duration" required>
                                    </div>
                                </div>  
                                @if (env('COMPANY')=='RoofLine')
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="deduction_group">@lang('app.Calculation_Groups')</label>
                                            <select type="select-multi" name="deduction_group" id="deduction_group" class="select2 form-select @error('deduction_group') is-invalid @enderror" data-placeholder="@lang('app.Select_Calculation_Group')" required>
                                                <option value=""></option>
                                                @foreach ($calculationGroup as $group)
                                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                @endforeach 
                                            </select>
                                            @error('deduction_group')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                @endif
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label for="type" class="form-label">@lang('app.Remarks')</label>
                                            <textarea name="remarks" placeholder="@lang('app.Remarks')" class="form-control" id="type" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                @can('Loan/Advance Request Approve Now')    
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="status" id="type" value="Approved"/>
                                                <label class="custom-control-label" for="type">@lang('app.Request_Approve_Now')</label>
                                            </div> 
                                        </div>
                                    </div>
                                @endcan
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @php
            if(env('COMPANY')=='RoofLine'){
                $readonly = 'readonly';
                $disabled = 'disabled';
            }
            else{
                $readonly = '';
                $disabled = '';
            }
        @endphp
        {{-- Edit Modal --}}
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Loan_Request')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="employee_id">@lang('app.Employee')</label>
                                        <select name="employee_id" id="edit_employee_id" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required {{$disabled}}>
                                            <option value=""></option>
                                            @foreach (employees('', '', true) as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="type">@lang('app.Loan_Type')</label>
                                        <select name="loan_type" id="edit_loan_type" class="select2 form-select" data-placeholder="@lang('app.Select_Type')" required {{$disabled}}>
                                            <option value=""></option>
                                            @foreach ($loan_types as $loan_type)
                                                <option value="{{$loan_type->id}}">{{$loan_type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="start_date">@lang('app.Start_Date')</label>
                                        <input type="text" name="start_date" id="edit_start_date" class="form-control flatpickr-basic flatpickr-input active" placeholder="YYYY-MM-DD" required {{$disabled}}>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="amount">@lang('app.Amount')</label>
                                        <input type="number" id="edit_amount" class="form-control " placeholder="@lang('app.Amount')" name="amount" required {{$readonly}}>
                                    </div>
                                </div>  
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="time_duration">@lang('app.Time_Duration(Months)')</label>
                                        <input type="number" id="edit_time_duration" class="form-control " placeholder="@lang('app.Month')" name="time_duration" required {{$readonly}}>
                                    </div>
                                </div>  
                                @if (env('COMPANY')=='RoofLine')
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="deduction_group">@lang('app.Calculation_Groups')</label>
                                            <select type="select-multi" name="deduction_group" id="edit_deduction_group" class="select2 form-select @error('deduction_group') is-invalid @enderror" data-placeholder="@lang('app.Select_Calculation_Group')" required>
                                                <option value=""></option>
                                                @foreach ($calculationGroup as $group)
                                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                @endforeach 
                                            </select>
                                            @error('deduction_group')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                @endif
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label for="type" class="form-label">@lang('app.Remarks')</label>
                                            <textarea name="remarks" placeholder="@lang('app.Remarks')" class="form-control" id="edit_remarks" rows="3" {{$readonly}}></textarea>
                                        </div>
                                    </div>
                                </div>
                                @can('Loan/Advance Request Approve Now')    
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="status" id="edit_status" value="Approved" {{$disabled}}/>
                                                <label class="custom-control-label" for="type">@lang('app.Request_Approve_Now')</label>
                                            </div> 
                                        </div>
                                    </div>
                                @endcan
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Update')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Payment Modal --}}
        <div class="modal fade text-start" id="payment_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Make_Payment')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="loan_details">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <input type="hidden" id="loan_request" name="loan_request_id">
                                <input type="hidden" id="EmployeeId" name="EmployeeId">
                                <input type="hidden" id="employee_code" name="employee_code">
                                <input type="hidden" id="loan_type" name="loan_type">
                                <input type="hidden" id="fair_price_customer" name="fair_price_customer">
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="loan_for">@lang('app.Loan_For')</label>
                                        <input type="text" id="loan_for" class="form-control" name="employee" readonly/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="loan_amount">@lang('app.Loan_Amount')</label>
                                        <input type="text" id="loan_amount" class="form-control" name="loan_amount" readonly/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="payment_type">@lang('app.Payment_Method')</label>
                                        <select name="payment_method" id="payment_type" class="select2 form-select" data-placeholder="@lang('app.Select_Payment_Method')" required>
                                            <option value="">@lang('app.Select_Payment_Method')</option>
                                            <option value="Cash">@lang('app.Cash')</option>
                                            <option value="Bank">@lang('app.Bank')</option>
                                            {{-- <option value="fair_price_advance">Fair Price Advance</option> --}}
                                        </select>
                                    </div>
                                </div>  
                                <div class="col-md-6 col-12">
                                    <label class="form-label">@lang('app.Payment_Date'):</label>
                                    <input type="text" name="payment_date" id="payment_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                </div>
                                <div class="col-12 cash" style="display: none">
                                    <div class="mb-1">
                                        <label class="form-label" for="cash_account">@lang('app.Cash_Account')</label>
                                        <select name="cash_account" id="cash_account" class="select2 form-select" data-placeholder="@lang('app.Select_Cash_Account')">
                                            <option value="">@lang('app.Select_Cash_Account')</option>
                                            @foreach ($company_banks as $cash)
                                                @if ($cash->type == 'cash')
                                                <option value="{{$cash->id}}">{{$cash->bank_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>  
                                <div class="col-12 bank" style="display: none">
                                    <div class="row">
                                        <div class="col-md-6 col-12 bank" style="display: none">
                                            <div class="mb-1">
                                                <label class="form-label" for="bank_id">@lang('app.Bank_Account')</label>
                                                <select name="bank_id" id="bank_id" class="select2 form-select" data-placeholder="@lang('app.Select_Bank_Account')">
                                                    <option value=""></option>
                                                    @foreach ($company_banks as $bank)
                                                        @if ($bank->type == 'bank')
                                                        <option value="{{$bank->id}}" id="{{$bank->erp_bank_id}}">{{$bank->bank_name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="cheque_book">@lang('app.Cheque_Books')</label>
                                                <select name="cheque_book" id="cheque_book" class="select2 form-select" data-placeholder="@lang('app.Select_Cheque_Book')">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="cheque_no">@lang('app.Cheque_No')</label>
                                                <select name="cheque_no" id="cheque_no" class="select2 form-select" data-placeholder="@lang('app.Select_Cheque_No')">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="checque_date">@lang('app.Cheque_Date')</label>
                                                <input type="date" id="checque_date" name="checque_date" value="{{date('Y-m-d')}}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-12 fair_price_shop_customer" style="display: none">
                                    <div class="mb-1">
                                        <label class="form-label">@lang('app.Fair_Price_Customer')</label>
                                        <select name="fair_price_customer_id" id="fair_price_customer_id" class="form-select select2"
                                            data-placeholder="Select Customer">
                                            <option value=""></option>
                                            @foreach (getFairPriceShopCustomersList() as $customer)
                                                <option value="{{ $customer->debtor_no }}">
                                                    {{ $customer->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="attachment">@lang('app.Attachments')</label>
                                        <input type="file" id="attachment" class="form-control" placeholder="" name="attachment"/>
                                    </div>
                                </div>  
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label for="type" class="form-label">@lang('app.Remarks')</label>
                                            <textarea name="remarks" placeholder="@lang('app.Remarks')" class="form-control" id="type" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Payment Details Modal --}}
        <div class="modal fade text-start" id="payment_details_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Payment_Details')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table">
                                        <thead>
                                            <th>Key</th>
                                            <th>Detail</th>
                                        </thead>
                                        <tbody id="payment_details_table">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection

@section('scripts')
    <script>
        var rowid; 
        var env = <?php echo json_encode($env); ?>;
        $(document).ready(function(){
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                // console.log(value.id); 
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });

            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching : true,
                ajax: {
                    url: "{{ route('loan_request.index') }}",
                    data: function (filter) {
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.typeFilter = $('#typeFilter').val();
                        filter.section = $('#section').val();
                        filter.fromDateFilter = $('#fromDateFilter').val();
                        filter.toDateFilter = $('#toDateFilter').val();
                        filter.dedFromDateFilter = $('#dedFromDateFilter').val();
                        filter.dedToDateFilter = $('#dedToDateFilter').val();
                        filter.statusFilter = $('#statusFilter').val();
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'employeeId',
                        name: 'employees.employeeId',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        data: 'empName', 
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + "<br>" + middleName + " " + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'title',
                        name: 'companystructures.title',
                    },
                    {
                        data: 'loan_type_name',
                        name: 'loan_types.name',
                    },
                    {
                        data: 'status' , render: function(data , type , row){
                            // var badgeStat =  {{ getbadge($request->status) }}
                            var btn;
                            if (row.status == 'Approved') {
                                btn = '<p style="font-size: 8px;" class = "badge bg-light-success">' +
                                        row.status+
                                    '</p>';
                            }
                            else if (row.status == 'Pending') {
                                
                                btn = '<p font-size: 8px;" class = "badge bg-light-info">' +
                                        row.status+
                                    '</p>';
                            }
                            else if (row.status == 'Cancelled') {
                                
                                btn = '<p font-size: 8px;" class = "badge bg-light-danger">' +
                                    row.status+
                                    '</p>';
                            }else{
                                btn = '<p font-size: 8px;" class = "badge bg-light-warning">' +
                                        row.status+
                                    '</p>';
                            }
                            return btn;
                        }
                    },
                    {
                        data: 'start_date',
                        name: 'loan_requests.start_date'
                    },
                    {
                        data: 'total_installment',
                        searchable: false
                    },
                    {
                        data: 'emp_status',
                        name: 'employees.emp_status'
                    },
                    {
                        data: 'amount',
                        name: 'loan_requests.amount',
                    },
                    {
                        data: 'pending',
                        name: 'loan_requests.pending',
                    },
                    {
                        data: 'remaining_amount',
                        name: 'loan_requests.remaining_amount'
                    },
                    {
                        data: 'created_at' , render: function(data , type , row){
                            var date = new Date(row.created_at),
                                        month = String(date.getMonth() + 1).padStart(2, '0'),
                                        day = String(date.getDate()).padStart(2, '0'),
                                        year = date.getFullYear();
                            return year+"-"+month+"-"+day;
                        }
                    },
                    {
                        data: 'remarks',
                        name: 'loan_requests.remarks'
                    },
                    @if (env('COMPANY')=='RoofLine')
                        {
                            data: 'deduction',
                            name: 'loan_requests.deduction_group'
                        },
                    @endif
                    {
                        data: 'recoveryStatus' , render: function(data , type , row){
                            if(row.final_satelment == 1){
                                return 'Setteled';
                            }else if(row.remaining_amount > 0){
                                return 'In Progress';
                            }else{
                                return 'Recovered';
                            }
                        },
                        searchable: false
                    },
                    {
                        data: ''
                    },
                ], 
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            var btn;
                            btn = '<a href="{{url("loan_request/")}}'+"/"+full.id+'" title="View Loan">' +
                                feather.icons['eye'].toSvg({ class: 'font-medium-4 me-1 text-warning' }) +
                                '</a>';
                            if(env == 'JSML' || env == 'RoofLine'){
                                @can('Edit Loan Request')
                                btn += '<a href="javascript:;" onclick="edit('+full.id+')" title="Edit Loan">' +
                                        feather.icons['edit'].toSvg({ class: 'font-medium-4 me-1 text-primary' }) +
                                        '</a>';
                                @endcan
                            }
                            if(env == 'JSML' || env == 'CLINIX' || env == 'RoofLine' || env == 'UNICORN'){
                                @can('Loan Request Delete')
                                    btn += '<a href="javascript:;" onclick="delete_item('+full.id+')" title="Delete Loan">' +
                                    feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                    '</a>';
                                @endcan
                            }
                            if(full.status == 'Approved'){
                                if(full.payment_id){
                                    btn += `<a href="javascript:;"  data-bs-toggle="tooltip" data-bs-placement="top" title="Loan Payment Details" onclick="payment_details(${full.payment_id})"><i class="fas fa-credit-card ms-1 fa-lg text-info"></i></a>`;
                                }
                                else{
                                    btn += `<a href="javascript:;" data-bs-toggle="tooltip" data-bs-placement="top" title="Loan Payment" onclick="loan_payment(${full.id},'${full.employee_id}','${full.employee_code}','${full.first_name}','${full.last_name}','${full.loan_type_name}',${full.amount})"><i class="fas fa-credit-card ms-1 fa-lg text-info"></i></a>`;
                                }
                            }
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Add Loan')
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Employee Loan Requests</h6>');
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'loan_request'
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.employee_code+ ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation+' - '+ value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            // Add Loan Details
            $('#loan_details').submit(function(e){
                ButtonStatus('.form_save',true);
                blockUI();
                e.preventDefault();
                var bank_name = $('#bank_id').find("option:selected").text();
                var formData = new FormData(this);
                formData.append('type','Payment');
                formData.append('bank_name', bank_name);
                $.ajax({
                    url: "{{route('loan_request.store')}}",
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.code == 200){
                            $("#payment_modal").modal("hide");
                            document.getElementById("loan_details").reset();
                            // $(".select2").val('').trigger('change');
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Payment Details has been Added Successfully!'
                            });
                        }
                        else if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                });
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: response.error_message
                            });
                        }
                        else{
                            Toast.fire({
                                icon: 'error',
                                title: 'Please Contact Admin!'
                            });
                        }
                    }
                });
            })
            // Add Data
            $('#loan_request_add_form').submit(function(e){
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'loan');
                $.ajax({
                    url: "{{route('loan_request.store')}}",
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#loan_request_add_form')[0].reset();
                            $(".trig").val('').trigger('change');
                            $("#add_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Loan Request has been Added Successfully!'
                            })
                        }
                    }
                });
            })
            // Update Data
            $('#edit_form').on('submit' , function(e){
                ButtonStatus('.form_save',true);
                blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{url('loan_request')}}" + "/" + rowid,
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#edit_modal").modal("hide");
                            $(".trig").val('').trigger('change');
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Loan Request has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
            $('#bank_id').change(function(){
                var bank_id = $(this).find("option:selected").attr('id');
                $.ajax({
                    type: "GET",
                    url: "{{ url('getCheckBookByBanks') }}" + '/' +bank_id,
                    success: function(response) {
                        if(response.code == 500){
                            Toast.fire({
                                icon: 'error',
                                title: response.error_message
                            });
                        }
                        $('#cheque_book').empty();
                        $('#cheque_no').empty();
                        $('#cheque_book').html('<option value="">Select Cheque Book</option>'); 
                        $.each(response, function(index, value) {
                            $('#cheque_book').append(
                                $('<option></option>').val(value.id).html(value.leafs)
                            );
                        });
                    },
                    error: function() {
                        Toast.fire({
                            icon: 'error',
                            title: 'Error Loading Cheque Books!'
                        });
                    }
                });
            });
            $('#fair_price_customer_id').change(function(){
                var customer = $(this).find("option:selected").text();
                $('#fair_price_customer').val(customer);
            });

            $('#cheque_book').change(function(){
                var cheque_book = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('getCheckBookByLeafs') }}" + '/' +cheque_book,
                    success: function(response) {
                        $('#cheque_no').empty();
                        $('#cheque_no').html('<option value="">Select Cheque No</option>'); 
                        $.each(response.leafs_list, function(index, value) {
                            $('#cheque_no').append(
                                $('<option></option>').val(value).html(value)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
        });    
        $('#payment_type').change(function(){
            if($(this).val() == 'Cash'){
                $(".cash").css("display", "block");
                $(".bank").css("display", "none");
                $(".fair_price_shop_customer").css("display", "none");
            }
            else if($(this).val() == 'Bank'){
                $(".cash").css("display", "none");
                $(".fair_price_shop_customer").css("display", "none");
                $(".bank").css("display", "block");
            }
            else{
                $(".cash").css("display", "none");
                $(".bank").css("display", "block");
                $(".fair_price_shop_customer").css("display", "block");
            }
        });
        function loan_payment(id,employee_id,employee_code,f_name,l_name,loan_type,amount){
            // console.log(employee_id);
            $("#loan_request").val(id);
            $("#EmployeeId").val(employee_id);
            $("#employee_code").val(employee_code);
            $("#loan_for").val(f_name +' '+ l_name);
            $("#loan_type").val(loan_type);
            $("#loan_amount").val(amount);
            $("#payment_modal").modal("show");   
        }
        function payment_details(id){
            $.ajax({
                url: "{{url('loan_request')}}" + "/" + id + "/edit",
                type: "GET",
                data: {
                    type: 'payment_details'
                },
                success: function(response){
                    var html = '';
                    if(response){
                        html += `
                            <tr>
                                <td>Employee</td>
                                <td>${response.first_name} ${response.last_name}</td>
                            </tr>
                            <tr>
                                <td>Loan Amount</td>
                                <td>${response.loan_amount}</td>
                            </tr>
                            <tr>
                                <td>Payment Method</td>
                                <td>${response.payment_method}</td>
                            </tr>
                            <tr>
                                <td>Cash Account Name</td>
                                <td>${!response.account_name ? '' : response.account_name}</td>
                            </tr>
                            <tr>
                                <td>Bank Name</td>
                                <td>${!response.bank_name ? '' : response.bank_name}</td>
                            </tr>
                            <tr>
                                <td>Checque No</td>
                                <td>${!response.checque_no ? '' : response.checque_no}</td>
                            </tr>
                            <tr>
                                <td>Fair Price Customer</td>
                                <td>${!response.fair_price_customer ? '' : response.fair_price_customer}</td>
                            </tr>
                            <tr>
                                <td>Payment Date</td>
                                <td>${response.payment_date}</td>
                            </tr>
                            <tr>
                                <td>Reference No.</td>
                                <td><a href="{{url('loan-payment-erp-detail')}}/${response.erp_reference}" title="View Entry" target="blank" class="">${response.erp_reference}</a></td>
                            </tr>
                            <tr>
                                <td>Paid By</td>
                                <td>${response.username}</td>
                            </tr>
                            <tr>
                                <td>Remarks</td>
                                <td>${response.remarks}</td>
                            </tr>
                            <tr>
                                <td>Attachment</td>
                                <td><a href="images/loan_details/${response.attachment}" target="blank" class="" download>${response.attachment}</a></td>
                            </tr>
                        `
                    }
                    $('#payment_details_table').html('');
                    $('#payment_details_table').append(html);   
                    $("#payment_details_modal").modal("show");
                }
            });
        }
        function edit(id){
            rowid = id;
            $.ajax({
                url: "{{url('loan_request')}}" + "/" + id + "/edit",
                type: "GET",
                success: function(response){
                    $("#edit_employee_id").val(response.employee_id).select2();
                    $("#edit_deduction_group").val(response.deduction_group).select2();
                    $("#edit_loan_type").val(response.loan_type).select2();
                    $("#edit_start_date").val(response.start_date);
                    $("#edit_amount").val(response.amount);
                    $("#edit_time_duration").val(response.time_duration);
                    $("#edit_remarks").val(response.remarks);
                    if(response.status == 'Approved'){
                        $('#edit_status').prop('checked', true);
                    }
                    $("#edit_modal").modal("show");
                }
            });
        } 
        function delete_item(id){
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                    url: "loan_request/" + id,
                    type: "DELETE",
                    data : {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (response) {
                        if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Loan Request has been Deleted Successfully!'
                            })
                        }
                    }
                });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Filter Function
        $('#search_form').submit(function(e){
            dataTable.draw();
            e.preventDefault();
        });
    </script>
@endsection
