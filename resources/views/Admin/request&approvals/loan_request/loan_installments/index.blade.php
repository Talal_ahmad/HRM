@extends('Admin.layouts.master')
@section('title', 'Loan Installments')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Loan Installments</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Loan/Advance</a>
                                </li>
                                <li class="breadcrumb-item active">Employee Loan Installments
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="app-user-view">
                <div class="card p-2">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <h5>Employee Id:</h5>
                            <p>{{ $employee_data->employee_id }}</p>
                        </div>
                        <div class="col-md-3 col-6">
                            <h5>Employee Name:</h5>
                            <p>{{ $employee_data->first_name.' '. $employee_data->last_name }}</p>
                        </div>
                        <div class="col-md-3 col-6">
                            <h5>Employee Status:</h5>
                            @if ($employee_data->suspended == 0)
                            <p>{{ $employee_data->emp_status }}</p>
                            @else
                                <p>Suspended</p>
                            @endif
                        </div>
                        <div class="col-md-3 col-6">
                            <h5>Employee Department:</h5>
                            <p>{{ $employee_data->title }}</p>
                        </div>
                        <div class="col-md-3 col-6">
                            <h5>Application Date:</h5>
                            <p>{{ $employee_data->created_at }}</p>
                        </div>
                        <div class="col-md-3 col-6">
                            <h5>Loan Status:</h5>
                            <p>{{ $employee_data->status }}</p>
                        </div>
                        <div class="col-md-3 col-6">
                            <h5>Start Date:</h5>
                            <p>{{ $employee_data->start_date }}</p>
                        </div>
                        <div class="col-md-3 col-6">
                            <h5>Loan Amount:</h5>
                            <p>{{ $employee_data->amount }}</p>
                        </div>
                        <div class="col-md-6 col-6">
                            <h5>Pending Amount (Opening Balance):</h5>
                            <p>{{ $employee_data->pending }}</p>
                        </div>
                        <div class="col-md-3 col-6">
                            <h5>Remaining Amount:</h5>
                            <p>{{ $employee_data->remaining_amount }}</p>
                        </div>
                        <div class="col-md-3 col-6">
                            <h5>Loan Type:</h5>
                            <p>{{ $employee_data->typeName }}</p>
                        </div>
                        <div class="col-md-6 col-6">
                            <h5>Remarks:</h5>
                            <p>{{ $employee_data->remarks }}</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="basic-dataTable">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Deduction Year</th>
                                        <th>Deduction Month</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Pay</th>
                                        <th>Changed By</th>
                                        <th>Description</th>
                                        {{-- @can('Edit Loan Installment') --}}
                                            <th class="not_include">Action</th>
                                        {{-- @endcan --}}
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!--Add Installment Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add Installment</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <input type="hidden" id="type" name="loan_request_id" value="{{$employee_data->id}}">
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="month">Month</label>
                                            <input type="text" name="month" id="month" class="form-control flatpickr-basic flatpickr-input" placeholder="YYYY-MM-DD" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="amount">Amount</label>
                                            <input type="number" id="amount" class="form-control " placeholder="Amount" name="amount" required>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="mb-1">
                                                <label for="description" class="form-label">Remarks</label>
                                                <textarea name="description" placeholder="Remarks ..."
                                                    class="form-control" id="description" rows="3" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--Edit Installment Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit Installment</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <input type="hidden" id="type" name="id" value="{{$employee_data->id}}">
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_amount">Amount</label>
                                            <input type="number" id="edit_amount" class="form-control" placeholder="Amount" name="amount" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_status">Status</label>
                                            <select name="status" id="edit_status" class="form-select select2" data-placeholder="Select Status" required>
                                                <option value=""></option>
                                                <option value="active">Active</option>
                                                <option value="cancelled">Cancelled</option>
                                                <option value="deducted">Deducted</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="remarks" class="form-label">Remarks</label>
                                            <textarea name="remarks" placeholder="Remarks ..."
                                                class="form-control" id="remarks" rows="3" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--Settlement Modal -->
            <div class="modal fade text-start" id="settlement_modal" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Final Settlement</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="settlement_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <input type="hidden" id="type" name="id" value="{{$employee_data->id}}">
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="mb-1">
                                                <label for="settlement_remarks" class="form-label">Remarks</label>
                                                <textarea name="remarks" placeholder="Remarks ..." class="form-control" id="settlement_remarks" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        var id = <?php echo json_encode($employee_data->id); ?>;
        var rowid;
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: false,
                ordering: true,
                ajax: "{{ url('loan_request') }}" + '/' + id,
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'year',
                        render: function(data, type, row) {
                            var date = new Date(row.month);
                            return date.getFullYear();
                        }
                    },
                    {
                        data: 'month',
                        render: function(data, type, row) {
                            const monthNames = ["January", "February", "March", "April", "May","June","July", "August", "September", "October", "November", "December"
                            ];
                            var date = new Date(row.month);
                            return monthNames[date.getMonth()];
                        }
                    },
                    {
                        data: 'amount'
                    },
                    {
                        data: 'status'
                    },
                    {
                        data: 'pay'  , render: function (data , type , row) {
                            if(row.status == 'deducted' &&  row.system_deducted){
                                return "Through System Deduction";
                            }else if(row.status == 'deducted'){
                                return "Through Cash";
                            }else{
                                return '-';
                            }
                        }
                    },
                    {
                        data: 'changed_by'
                    },
                    {
                        data: 'description'
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                        var btn = '';
                        if (full.status == 'active' || full.status == 'deducted') {
                            @can('Edit Loan Installment')
                                btn += '<a href="javascript:;" class="item-edit" onclick=edit(' + full.id + ')>' +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    });
                                btn += '<a href="javascript:;" class="item-edit" onclick=delete_item(' + full.id + ')>' +
                                    feather.icons['trash-2'].toSvg({
                                        class: 'font-medium-4 text-danger'
                                    });
                            @endcan
                        }
                        return btn;
}
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Add Loan Installment')
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    },
                    @endcan
                    @can('Final Loan Settlement')
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Final Settlement',
                        className: 'create-new btn btn-primary ms-1',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#settlement_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">Loan Installments</h6>');

            // Add Installment
            $("#add_form").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('loan_installments.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Loan Installment has been Added Successfully!'
                            })
                        }
                    }
                });
            });
            // Update Data
            $("#edit_form").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('loan_installments') }}" + "/" + rowid,
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Loan Installments has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
            // Update Settlement
            $("#settlement_form").on("submit", function(e) {
                var formData = new FormData(this);
                formData.append('type' , 'settlement'); 
                e.preventDefault();
                $.ajax({
                    url: "{{ url('loan_installments') }}" + "/" + id,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#settlement_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Loan Installments has been Updated Successfully!'
                            })
                        }

                    }
                });
            });

        });

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('loan_installments') }}" + "/" + id + "/edit",
                type: "GET",
                success: function(response) {
                    $("#edit_amount").val(response.amount);
                    $("#edit_status").val(response.status).select2();
                    $("#remarks").text(response.description);
                    $("#edit_modal").modal("show");
                },
            });
        }
        // Delete Function
        function delete_item(id){
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "{{url('loan_request')}}" +'/'+ id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}",
                                    type: 'installment'
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else{
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Loan Installment has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
