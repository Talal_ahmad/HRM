@extends('Admin.layouts.master')
@section('title', 'Manual Attendance Request')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Attendance_Request')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Attendance_Request')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            @if(userRoles()!="Self Service")
                                <!--Search Form -->
                                <div class="card-body">
                                    <form id="search_form">
                                        <div class="row g-1 mb-md-1">
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.Department'):</label>
                                                <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                    <option value=""></option>
                                                    <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : ''}}>All</option>
                                                    @foreach (departments() as $department)
                                                        <option value="{{$department->id}}">{{$department->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                                <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
        
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.Status'):</label>
                                                <select name="statusFilter" id="statusFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Manual_Attendance_Status')">
                                                    <option value=""></option>
                                                    <option value="Pending">@lang('app.Pending')</option>
                                                    <option value="Approved">@lang('app.Approved')</option>
                                                    <option value="Cancelled">@lang('app.Cancelled')</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.To_Date'):</label>
                                                <input type="text" name="fromDateFilter" id="fromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.To_Date'):</label>
                                                <input type="text" name="toDateFilter" id="toDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div style="margin-top:35px" class="col-md-3">
                                                <button class="btn btn-primary">@lang('app.Filter')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Designation</th>
                                        <th>Department</th>
                                        <th>In Time</th>
                                        <th>Out Time</th>
                                        <th>Status</th>
                                        <th>Remarks</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <div class="modal-size-lg d-inline-block">
                {{-- Add Modal --}}
                <div class="modal fade text-start" id="add_modal" data-bs-focus="false" tabindex="-1"
                    aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Manual_Attendance')</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form class="form" id="add_form">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                <select name="employee" id="employee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                                    <option value="">@lang('app.Select_Employee')</option>
                                                    @foreach (employees() as $employee)
                                                        <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="in_time">@lang('app.In_Time')</label>
                                                <input type="text" id="in_time" class="form-control flatpickr-time text-left" placeholder="HH:MM" name="in_time" required />
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="out_time">@lang('app.Out_Time')</label>
                                                <input type="text" id="out_time" class="form-control flatpickr-time text-left" placeholder="HH:MM" name="out_time" required />
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="in_date">@lang('app.In_Date')</label>
                                                <input type="text" name="in_date" id="in_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="out_date">@lang('app.Out_Date')</label>
                                                <input type="text" name="out_date" id="out_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="attachment">@lang('app.Attachments')</label>
                                                <input type="file" id="attachment" class="form-control" placeholder="" name="attachment"/>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="note">@lang('app.Remarks')</label>
                                                <textarea id="note" class="form-control" cols="20" rows="2" placeholder="@lang('app.Type_here')" name="note"></textarea>
                                            </div>
                                        </div>
                                        @can('Manual Attendance Approval Request Approve Now')    
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="status" id="status" value="Approved"/>
                                                        <label class="custom-control-label" for="status">@lang('app.Request_Approve_Now')</label>
                                                    </div> 
                                                </div>
                                            </div>
                                        @endcan
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                    <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Edit Modal -->
                <div class="modal fade text-start" id="edit_modal" tabindex="-1"
                    aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Manual_Attendance')</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form class="form" id="manual_attendance_edit_form">
                                @csrf
                                @method('PUT')
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_employee">@lang('app.Employee')</label>
                                                    <select name="employee" id="edit_employee" class="select2 form-select"
                                                        required>
                                                        <option value="">@lang('app.Select_Employee')</option>
                                                        @foreach (employees() as $employee)
                                                            <option value="{{ $employee->id }}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{ $employee->first_name }} {{ $employee->last_name }}-{{$employee->designation}}-{{$employee->department}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_in_time">@lang('app.In_Time')</label>
                                                    <input type="text" id="edit_in_time"
                                                        class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                        name="in_time" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_out_time">@lang('app.Out_Time')</label>
                                                    <input type="text" id="edit_out_time"
                                                        class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                        name="out_time" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_in_date">@lang('app.In_Date')</label>
                                                    <input type="date" name="in_date" id="edit_in_date"
                                                        class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                        required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_out_date">@lang('app.Out_Date')</label>
                                                    <input type="date" name="out_date" id="edit_out_date"
                                                        class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                        required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_note">@lang('app.Remarks')</label>
                                                    <textarea id="edit_note" class="form-control" cols="20" rows="2" placeholder="@lang('app.Type_here')" name="note"></textarea>
                                                </div>
                                            </div>
                                            {{-- <input type="checkbox" name="both_date"> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                    <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Update')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {{-- Edit Status --}}
                {{-- <div class="modal fade text-start" id="status_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Leave Request Status</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form class="form" id="status_form">
                                @csrf
                                @method('PUT')
                                <div class="modal-body">
                                    <div class="row">      
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_status">Status</label>
                                                <select name="status" id="edit_status" class="select2 form-select" data-placeholder="Select Status" required>
                                                    <option value=""></option>
                                                    <option value="Pending">Pending</option>
                                                    <option value="Approved">Approved</option>
                                                    <option value="Cancelled">Cancelled</option>
                                                    <option value="Rejected">Rejected</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        @if (env('COMPANY') == 'RoofLine')        
            $("#in_time").flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                defaultDate: "9:00"
            });
            $("#out_time").flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                defaultDate: "17:30"
            });
        @endif
        var rowid;
        var datatable;
        $(document).ready(function() {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.employee_code + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation+' - '+ value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('attendance_approval.index') }}",
                    data: function (filter) {
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.statusFilter = $('#statusFilter').val();
                        filter.fromDateFilter = $('#fromDateFilter').val();
                        filter.toDateFilter = $('#toDateFilter').val();
                    }
                },
                columns: [
                    { 
                        data: 'responsive_id'
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data : 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data : 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        data: 'employeeName', 
                        render:function (data , type , row){
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + ' ' + middleName + ' ' + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'designation',
                        name: 'jobtitles.name'
                    },
                    {
                        data: 'department',
                        name: 'companystructures.title'
                    },
                    {
                        data: 'in_time',
                        name: 'manual_attendance_request.in_time'
                    },
                    {
                        data: 'out_time',
                        name: 'manual_attendance_request.out_time'
                    },
                    {
                        data: 'status',
                        name: 'manual_attendance_request.status'
                    },
                    {
                        data: 'note',
                        name: 'manual_attendance_request.note'
                    },
                    {
                        data: 'action' , render: function(data , type , row){
                            if(row.status !== 'Approved' && row.status !=='Rejected'){
                                @can('Approve Manual Attendance')
                                return `<button class="print_btn actionBtn btn btn-success mb-1" id="Approved" onclick="allow(${row.id} , 'manual_attendance')">&nbsp;Allow&nbsp;</button> <button class="actionBtn btn btn-warning print_btn mb-1" id="Rejected" onclick="reject(${row.id} , 'manual_attendance')">Reject&nbsp;</button> <button class="print_btn actionBtn btn btn-danger mb-1" id="Cancelled" onclick="cancel(${row.id} , 'manual_attendance')">Cancel</button> <button class="print_btn actionBtn btn btn-secondary mb-1" id="Approved" onclick="edit(${row.id})">&nbsp;Edit&nbsp;</button>`;
                                @endcan
                            }else{
                                return `<button class="print_btn actionBtn btn btn-secondary mb-1" id="Approved" onclick="edit(${row.id})">&nbsp;Edit&nbsp;</button>`
                            }
                        },
                        searchable: false,
                    }
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'landscape',
                            pageSize: 'LEGAL',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Manual Attendance Approvals</h6>');

            // Add Data
            $('#add_form').on('submit' , function(e){
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{route('attendance_approval.store')}}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#add_modal").modal("hide");
                            $('#add_form')[0].reset();
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Manual Attendance has been Added Successfully!'
                            })
                        }
                    }
                });
            });

        });
        // Status Form
        // function change_status(id){
        //     rowid = id;
        //     $.ajax({
        //         url: "{{url('attendance_approval')}}" + "/" + id + "/edit",
        //         type: "GET",
        //         data: {
        //             type: 'status'
        //         },
        //         success: function(response){
        //             $("#edit_status").val(response.data.status).select2();
        //             $("#status_modal").modal('show');
        //         }
        //     })
        // }
        
        // Update Status
        $('#status_form').submit(function (e) {
            ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{url('attendance_approval')}}" + "/" + rowid,
                    type: 'post',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#status_form')[0].reset();
                            $("#status_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Attendance Request has been Updated Successfully!'
                            })
                        }
                    } 
                });
            });
            function allow(id , type){
            record_id = id;
            record_type = type;
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: "Are you sure you want to Approve Employee's request!",
                type: 'green',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Approve',
                        btnClass: 'btn-success',
                        action: function(){
                            $.ajax({
                                url: "{{url('statusUpdate')}}" + "/" + record_id,
                                type: 'GET',
                                data: {
                                    data_type: record_type,
                                    type: 'Approved',
                                },
                                success: function (response){
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }else{
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Status has been Updated Successfully!'
                                        }) 
                                        setTimeout(function(){
                                            window.location.reload();
                                        },2000)
                                    }
                                }
                            }); 
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        //Edit Attendance Request
        function edit(id, type) {
            rowid = id;
            $.ajax({
                url: "{{ url('attendance_approval') }}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    var in_time = new Date(response.in_time);
                    var out_time = new Date(response.out_time);
                    $("#edit_employee").val(response.employee_id).select2();
                    $("#edit_in_time").val(in_time.getMinutes() > 10 ? `${in_time.getHours()}:${in_time.getMinutes()}` : `${in_time.getHours()}:0${in_time.getMinutes()}`);
                    $("#edit_out_time").val(out_time.getMinutes() > 10 ? `${out_time.getHours()}:${out_time.getMinutes()}` : `${out_time.getHours()}:0${out_time.getMinutes()}`);
                    $("#edit_in_date").val(new Date(response.in_time).toISOString().slice(0, 10));
                    $("#edit_out_date").val(new Date(response.out_time).toISOString().slice(0, 10));
                    $("#edit_note").val(response.note);
                    $("#edit_modal").modal("show");
                },
            });
        }
        // Update Data
        $('#manual_attendance_edit_form').submit(function(e) {
                e.preventDefault();
                ButtonStatus('.form_save',true);
                blockUI();
                $.ajax({
                    url: "{{ url('attendance_approval') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#manual_attendance_edit_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#edit_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Manual Attendance has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
        function reject(id , type){
            record_id = id;
            record_type = type;
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: "Are you sure you want to Reject Employee's request!",
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Reject',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "{{url('statusUpdate')}}" + "/" + record_id,
                                type: 'GET',
                                data: {
                                    data_type: record_type,
                                    type: 'Rejected',
                                },
                                success: function (response){
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }else{
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Status has been Updated Successfully!'
                                        }) 
                                        setTimeout(function(){
                                            window.location.reload();
                                        },2000)
                                    }
                                }
                            });  
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        function cancel(id , type){
            record_id = id;
            record_type = type;
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: "Are you sure you want to Cancel Employee's request!",
                type: 'red',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Cancel',
                        btnClass: 'btn-danger',
                        action: function(){
                            $.ajax({
                                url: "{{url('statusUpdate')}}" + "/" + record_id,
                                type: 'GET',
                                data: {
                                    data_type: record_type,
                                    type: 'Cancelled',
                                },
                                success: function (response){
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }else{
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Status has been Updated Successfully!'
                                        }) 
                                        setTimeout(function(){
                                            window.location.reload();
                                        },2000)
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Filter Function
        $('#search_form').submit(function(e){
            datatable.draw();
            e.preventDefault();
        });
    </script>
@endsection