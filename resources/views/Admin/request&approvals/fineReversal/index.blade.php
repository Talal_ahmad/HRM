@extends('Admin.layouts.master')
@section('title', 'Fine Reversal')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Fine Reversal</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Request&Approvals</a>
                            </li>
                            <li class="breadcrumb-item active">Fine Reversal
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <!--Search Form -->
                        <div class="card-body">
                            <form id="search_form">
                                <div class="row g-1 mb-md-1">
                                    <div class="col-md-6">
                                        <label class="form-label">Department:</label>
                                        <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department">
                                            <option value=""></option>
                                            <option value="all">All</option>
                                            @foreach (departments() as $department)
                                                <option value="{{$department->id}}">{{$department->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label">Month:</label>
                                        <input type="month" name="month" id="month" class="form-control" value="{{date("Y-m", strtotime("first day of previous month"))}}">
                                    </div>
                                    <div class="col-md-3" style="margin-top:24px">
                                        <a href="{{url('fineReversal')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Employee ID</th>
                                    <th>Employee Code</th>
                                    <th>Employee</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Department/Branch</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <!-- View Fine  Modal -->
        <div class="modal fade text-start" id="view_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">View Fine</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Name</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody id="append_row"></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Add Fine Request  Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Fine Reversal Request</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <input type="hidden" name="employee_id" id="employee_id">
                                <input type="hidden" name="month" id="fine_month">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="fine_amount">Amount</label>
                                        <input type="number" name="amount" id="fine_amount" class="form-control" step=".01" required readonly>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="remarks" class="form-label">Remarks</label>
                                        <textarea name="remarks" placeholder="Remarks ..." class="form-control" id="remarks" rows="3" required></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="fine_amount">Attachment</label>
                                        <input type="file" name="attachment_file" id="attachment_file" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <input type="hidden" name="payrolldata_id" id="payrolldata_id" class="form-control">
                                        <input type="hidden" name="payroll_id" id="payroll_id" class="form-control">
                                    </div>
                                </div>
                                
                                {{-- @can('Leave Request Approve Now')    
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="status" id="status" value="Approved"/>
                                                <label class="custom-control-label" for="status">Request Approve Now</label>
                                            </div> 
                                        </div>
                                    </div>
                                @endcan --}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="form_save btn btn-primary" id="leave_save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('scripts')
    <script>
        var rowid;
        var datatable;
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('fineReversal.index') }}",
                    data: function (filter) {
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.monthFilter = $('#month').val();
                    }
                },
                columns: [
                    { 
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data : 'employee_id',
                        name : 'employees.employee_id',
                    },
                    {
                        data : 'employee_code',
                        name : 'employees.employee_code',
                    },
                    {
                        data: 'employeeName', 
                        render:function (data , type , row){
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + ' ' + middleName + ' ' + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'department',
                        name : 'payrolldata.department', 
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            var btn = '';
                            btn = '<a href="javascript:;" class="item-edit" title="View Fine" onclick=viewFine('+full.id+','+full.payroll_id+')>'
                            +'View '+ feather.icons['eye'].toSvg({ class: 'font-medium-4 me-1' }) +
                                '</a>';
                            if(full.fine_request){
                                if(full.fine_request == 'Approved'){
                                    btn += '<span class="badge bg-success">'+full.fine_request+'</span>';
                                }
                                else if(full.fine_request == 'Rejected'){
                                    btn += '<span class="badge bg-warning">'+full.fine_request+'</span>';
                                }else if(full.fine_request == 'Cancelled'){
                                    btn += '<span class="badge bg-danger">'+full.fine_request+'</span>';
                                }else{
                                    btn += '<span class="badge bg-primary">'+full.fine_request+'</span>';
                                }
                                
                                }else{
                                    @can('Apply Fine Request')
                                    btn += '<a href="javascript:;" class="item-edit" title="Add Request" onclick=addFineRequest('+full.id+','+full.total_fine+','+full.fine_relation_id+','+full.payroll_id+')>' 
                            +'Apply'+
                                feather.icons['plus'].toSvg({ class: 'font-medium-4 me-1' }) +
                                '</a>';
                                @endcan

                                }
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'landscape',
                            pageSize: 'LEGAL',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Employees</h6>');

            // Add Data
            $("#add_form").submit(function (e) {
                ButtonStatus('.form_save',true);
                blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{route('fineReversal.store')}}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else if(response.customMessage){
                            // Handling the specific 'exist' message with code 500
                            Toast.fire({
                                icon: 'error',
                                title: 'The fine reversal already exists.' // Modify the message as needed
                            })
                        }
                        else{
                            $('#add_form')[0].reset();
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Fine Reversal Request has been Added Successfully!'
                            })
                        }
                    }
                });
            });
        });

        // View Fine
        function viewFine(employee_id,payroll_id){
            $.ajax({
                url: "{{url('fineReversal')}}" + "/" + employee_id,
                type: "GET",
                data: {
                    payroll_id: payroll_id
                },
                success: function(response){
                    $('#append_row').empty();
                    var html = '';
                    var total = 0;
                    $.each(response, function(key, value){
                        total = Number(total)+Number(value.amount);
                        html += '<tr>'+
                                '<td>'+ (key + 1) + '</td>'+
                                '<td>'+ value.name +'</td>'+
                                '<td>'+value.amount+'</td>'+
                            '</tr>';
                    });
                    html += `<tr>
                            <td colspan='2'><strong>Total</strong></td>
                            <td><strong>${parseFloat(total).toFixed(2)}</strong></td>
                        </tr>`;
                    $('#append_row').append(html);
                    $("#view_modal").modal('show');
                }
            })
        }

        function addFineRequest(employee_id,amount,fine_relation_id,payroll_id){
            $('#employee_id').val(employee_id);
            $('#fine_amount').val(amount);
            $('#payroll_id').val(payroll_id);
            document.getElementById('fine_amount').max=amount;
            $('#fine_month').val($('#month').val());
            $('#payrolldata_id').val(fine_relation_id);
            $("#add_modal").modal('show');
        }
        // Filter Function
        $('#search_form').submit(function(e){
            datatable.draw();
            e.preventDefault();
        });
    </script>
@endsection