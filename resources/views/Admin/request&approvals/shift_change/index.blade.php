@extends('Admin.layouts.master')
@section('title', 'Shift Change Request')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Shift_Change')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Shift_Change')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <!--Search Form -->
                            @if(userRoles()!="Self Service")
                                <div class="card-body">
                                    <form id="search_form">
                                        <div class="row g-1 mb-md-1">
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.Department'):</label>
                                                <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                    <option value=""></option>
                                                    <option value="all">All</option>
                                                    @foreach (departments() as $department)
                                                        <option value="{{ $department->id }}">{{ $department->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                                <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" disabled>

                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.Designation'):</label>
                                                <select name="designationFilter[]" id="designationFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Designation')" multiple>
                                                    <option value=""></option>
                                                    @foreach (designation() as $jobTitle)
                                                        <option value="{{$jobTitle->id}}" {{!empty(request('designationFilter')) ? in_array($jobTitle->id,request('designationFilter')) ? 'selected' : '' : ''}}>{{$jobTitle->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label" for="shift_filter">@lang('app.Shift_Type')</label>
                                                <select name="shift_filter" id="shift_filter" class="select2 form-select" data-placeholder="@lang('app.Select_Shift_Type')">
                                                    <option value=""></option>
                                                    @foreach ($shiftTypes as $shiftType)
                                                        <option value="{{ $shiftType->id }}">{{ $shiftType->shift_desc }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.Status'):</label>
                                                <select name="statusFilter" id="statusFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Leave_Status')">
                                                    <option value=""></option>
                                                    <option value="Pending">@lang('app.Pending')</option>
                                                    <option value="Approved">@lang('app.Approved')</option>
                                                    <option value="Rejected">@lang('app.Rejected')</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.From_Date'):</label>
                                                <input type="text" name="fromDateFilter" id="fromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.To_Date'):</label>
                                                <input type="text" name="toDateFilter" id="toDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div style="margin-top:35px" class="col-md-3">
                                                <button class="btn btn-primary">@lang('app.Filter')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Department/Branch</th>
                                        <th>Designation</th>
                                        <th>Work Week</th>
                                        <th>Shift Type</th>
                                        <th>Shift From</th>
                                        <th>Shift To</th>
                                        <th>Requested Date Time</th>
                                        <th>Requested By</th>
                                        <th>Action Taken By</th>
                                        <th>Remarks</th>
                                        <th>Status</th>
                                        <th class="not_include">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            @php
                if(env('COMPANY') == 'JSML'){
                    $modalSize = 'xl';
                    $statusColumn = '6';
                }    
                else{
                    $modalSize = 'lg';
                    $statusColumn = '12';
                }
            @endphp
            <!-- Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1"
                aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-{{$modalSize}}">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Shift_Change_Request')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    @if(env('COMPANY') == 'JSML')
                                        <div class="col-md-4">
                                            <label class="form-label">@lang('app.Department'):</label>
                                            <select name="department" id="departmentFilterAddModal" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}">{{ $department->title }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label"
                                                    for="title">@lang('app.Section')</label>
                                                <select name="sections[]" id="sectionFilterAddModal"
                                                    data-placeholder="@lang('app.Select_Section')"
                                                    class="select2 form-select" multiple required></select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary"
                                                        type="button"
                                                        onclick="selectAll('#sectionFilterAddModal')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#sectionFilterAddModal')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="employment_status">@lang('app.Employment_Status')</label>
                                                <select name="employment_status[]" id="employment_status" class="select2 form-select" data-placeholder="@lang('app.Select_Employment_Status')" multiple>
                                                    <option value=""></option>
                                                    @foreach ($employment_status as $employment)
                                                        <option value="{{$employment->id}}" {{!empty(request('employment_status')) ? in_array($employment->id,request('employment_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#employment_status')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#employment_status')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label" for="employeeFilterAddModal">@lang('app.Employees')</label>
                                            <select name="employee_id[]" id="employeeFilterAddModal" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" multiple required disabled></select>
                                            <div class="button-container mt-1 mb-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#employeeFilterAddModal')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#employeeFilterAddModal')">Deselect All</button>
                                            </div>
                                        </div>
                                        <hr>
                                    @else
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="employee">@lang('app.Employee')</label>
                                                <select name="employee_id" id="employee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                                    <option value=""></option>
                                                    @foreach (employees() as $employee)
                                                        <option value="{{$employee->id}}">{{$employee->employee_id}} {{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="work_week">@lang('app.Select_Work_Week')</label>
                                            <select name="work_week_id" id="work_week" class="select2 form-select" data-placeholder="@lang('app.Select_Work_Week')" required>
                                                <option value=""></option>
                                                @foreach ($workWeeks as $workWeek)
                                                    <option value="{{ $workWeek->id }}">{{ $workWeek->desc }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @if (env('COMPANY') == 'CLINIX')
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="shift_type">@lang('app.Shift_Types')</label>
                                                <select name="shift_id" id="shift_type" class="select2 form-select" data-placeholder="@lang('app.Select_Shift_Type')" required>
                                                
                                                </select>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="shift_type">@lang('app.Shift_Types')</label>
                                                <select name="shift_id" id="" class="select2 form-select" data-placeholder="@lang('app.Select_Shift_Type')" required>
                                                    <option value=""></option>
                                                    @foreach ($shiftTypes as $item)
                                                        <option value="{{ $item->id }}">{{ $item->shift_desc }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="shift_from">@lang('app.Shift_From')</label>
                                            <input type="text" name="shift_from_date" id="shift_from" class="form-control flatpickr-basic flatpickr-input" placeholder="YYYY-MM-DD" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="shift_to">@lang('app.Shift_To')</label>
                                            <input type="text" name="shift_to_date" id="shift_to" class="form-control flatpickr-basic flatpickr-input" placeholder="YYYY-MM-DD" required>
                                        </div>
                                    </div>
                                    <div class="col-{{$statusColumn}}">
                                        <div class="mb-1">
                                            <label class="form-label" for="attachment">@lang('app.Attachment')</label>
                                            <input type="file" id="attachment" class="form-control" placeholder="" name="image"/>
                                        </div>
                                    </div>
                                    @if(env('COMPANY') == 'JSML')
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="status">@lang('app.Select_Status')</label>
                                                <select name="status" id="status" class="select2 form-select" data-placeholder="@lang('app.Select_Status')">
                                                    <option value=""></option>
                                                    <option value="Approved">Approved</option>
                                                    <option value="Pending">Pending</option>
                                                    <option value="Rejected">Rejected</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="remarks" class="form-label">@lang('app.Remarks')</label>
                                            <textarea name="remarks" placeholder="@lang('app.Remarks')" class="form-control" id="remarks" rows="3"></textarea>
                                        </div>
                                    </div>
                                    @if(env('COMPANY') != 'JSML')
                                        @can('Shift Change Request Approve Now')    
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="status" id="type" value="Approved" />
                                                        <label class="custom-control-label" for="type">@lang('app.Request_Approve_Now')</label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endcan
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="show-existing-shifts-table" style="display:none">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1"
                aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Shift_Change_Request')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_employee">@lang('app.Employee')</label>
                                            <select name="employee_id" id="edit_employee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                                <option value=""></option>
                                                @foreach (employees('', '', true) as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id}} {{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="work_week_id">@lang('app.Select_Work_Week')</label>
                                            <select name="work_week_id" id="work_week_id" class="select2 form-select" data-placeholder="@lang('app.Select_Work_Week')" required>
                                                <option value=""></option>
                                                @foreach ($workWeeks as $workWeek)
                                                    <option value="{{ $workWeek->id }}">{{ $workWeek->desc }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="shift_id">@lang('app.Shift_Type')</label>
                                            <select name="shift_id" id="shift_id" class="select2 form-select" data-placeholder="@lang('app.Select_Shift_Type')" required>
                                                <option value=""></option>
                                                @foreach ($shiftTypes as $shiftType)
                                                    <option value="{{ $shiftType->id }}">{{ $shiftType->shift_desc }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="shift_from_date">@lang('app.Shift_From')</label>
                                            <input type="text" name="shift_from_date" id="shift_from_date" class="form-control flatpickr-basic flatpickr-input" placeholder="YYYY-MM-DD" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="shift_to_date">@lang('app.Shift_To')</label>
                                            <input type="text" name="shift_to_date" id="shift_to_date" class="form-control flatpickr-basic flatpickr-input" placeholder="YYYY-MM-DD" required>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="edit_remarks" class="form-label">@lang('app.Remarks')</label>
                                            <textarea name="remarks" placeholder="@lang('app.Remarks')" class="form-control" id="edit_remarks" rows="3"></textarea>
                                        </div>
                                    </div>
                                    @can('Shift Change Request Approve Now')    
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="status" id="edit_status" value="Approved" />
                                                    <label class="custom-control-label" for="edit_status">@lang('app.Request_Approve_Now')</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="form_save btn btn-primary">@lang('app.Update')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script>
        var rowid;
        $(document).ready(function() {
            $("#employee").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_employees_shift') }}",
                        data: {
                            emp_id: optVal,
                        },
                        success: function(response) {
                            $('#shift_type').empty();
                            $('#shift_type').html(
                                '<option value="">Select Shift Type</option>');
                            $.each(response, function(index, value) {
                                var startTime = moment(value.shift_start_time, 'HH:mm:ss');
                                var endTime = moment(value.shift_end_time, 'HH:mm:ss');
                                if (endTime.isBefore(startTime)) {
                                    endTime.add(1, 'day'); // Add 1 day to end time
                                }
                                var duration = moment.duration(endTime.diff(startTime));
                                var totalHours = duration.hours();
                                var totalMinutes = duration.minutes();
                                $('#shift_type').append(
                                    $('<option></option>').val(value.id).html(value.shift_desc +' ('+totalHours + ' hours ' + totalMinutes + ' minutes'+')')
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#shift_type').empty();
                }
            });
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: true,
                stateSave : true,
                cache : false,
                ajax: {
                    url: "{{ route('shift_change.index') }}",
                    data: function (filter) {
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.statusFilter = $('#statusFilter').val();
                        filter.fromDateFilter = $('#fromDateFilter').val();
                        filter.designationFilter = $('#designationFilter').val();
                        filter.toDateFilter = $('#toDateFilter').val();
                        filter.shift_filter = $('#shift_filter').val();
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        data: 'employeeName',
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + ' ' + middleName + ' ' + lastName;
                        },
                        searchable: false,
                        orderable:false,

                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'title',
                        name: 'companystructures.title',
                    },
                    {
                        data: 'job_title_name',
                        name: 'jobtitles.name',
                    },
                    {
                        data: 'desc',
                        name: 'work_week.desc',
                    },
                    {
                        data: 'shift_desc',
                        name: 'shift_type.shift_desc',
                    },
                    {
                        data: 'shift_from_date',
                        name: 'shift_management.shift_from_date',
                    },
                    {
                        data: 'shift_to_date',
                        name: 'shift_management.shift_to_date',
                    },
                    {
                        data: 'created_at' , render: function(data , type , row){
                            let dateTime = new Date(row.created_at);
                            return `${dateTime.getHours()}:${dateTime.getMinutes()}:${dateTime.getSeconds()} ${dateTime.getFullYear()}-${parseInt(dateTime.getMonth() + 1)}-${dateTime.getDate()}`;
                        }
                    },
                    {
                        data: 'requested_by',
                        name: 'users.username',
                    },
                    {
                        data: 'action_taken_by',
                        name: 'users2.username',
                    },
                    {
                        data: 'remarks',
                        name: 'shift_management.remarks',
                    },
                    {
                        data: 'status',
                        name: 'shift_management.status',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=shift_change_request_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="shift_change_request_delete(' +
                                full.id + ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Shift Change Request</h6>');

            // Filter department Employees
            $('#departmentFilter').on('change', function(){
                var department_id = $(this).val();
                var employee_id = '#employeeFilter';
                get_department_employees(department_id,employee_id);
            });

            $("#departmentFilterAddModal").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employeeFilterAddModal').html('');
                            $('#employeeFilterAddModal').empty();
                            $('#sectionFilterAddModal').empty();
                            $('#sectionFilterAddModal').html('<option value="">Select Section</option>');
                            $.each(response, function(index, value) {
                                $('#sectionFilterAddModal').append(
                                    $('<option></option>').val(
                                        value.id).html(
                                        value.title)
                                );
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Sections Fetched Successfully!'
                            });
                        },
                        error: function() {
                            Toast.fire({
                                icon: 'success',
                                title: 'Error Fetching Department Sections!',
                            });
                        }
                    });
                } else {
                    $('#sectionFilterAddModal').empty();
                }
            });

            $("#sectionFilterAddModal,#employment_status").change(function() {
                var optVal = $("#sectionFilterAddModal").val();
                var employment_status = $("#employment_status").val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_department_employees_from_section') }}",
                    data: {
                        department_id: optVal,
                        type: 'attendance_report',
                        employment_status:employment_status
                    },
                    success: function(response) {
                        $('#employeeFilterAddModal').attr('disabled', false);
                        $('#employeeFilterAddModal').html('');
                        $('#employeeFilterAddModal').empty();
                        $('#employeeFilterAddModal').html(
                            '<option value="">Select Employee</option>');
                        $.each(response, function(index, value) {
                            $('#employeeFilterAddModal').append(
                                $('<option></option>').val(
                                    value.id).html(
                                    value.employee_id + ' - ' + value
                                    .employee_code + ' - ' + value.first_name +
                                    ' ' + value.middle_name + ' ' + value
                                    .last_name + ' - ' + value.designation +
                                    ' - ' + value.department)
                            );
                        });
                        Toast.fire({
                            icon: 'success',
                            title: 'Employees Fetched Successfully!'
                        });
                    },
                    error: function() {
                        $('#employeeFilterAddModal').empty();
                        Toast.fire({
                            icon: 'success',
                            title: 'Error Fetching Sections Employees!',
                        });
                    }
                });
            });
            // Add Data
            $("#add_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('shift_change.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.code == 200){
                            $('#add_form')[0].reset();
                            datatable.ajax.reload();
                            if(response.response_message){
                                Swal.fire({
                                    icon: 'success',
                                    title: response.response_message
                                });
                            }
                            else{
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Shift Change has been Added Successfully!'
                                });
                            }
                            if(response.records){
                                $("#show-existing-shifts-table").show();
                                var table = '<h4 class="text-danger">These employee already has existing shifts (Pending Or Approved). For These Employees Shift Were Not Created. Delete Records & Then Add Shift.</h4><hr><table class="table"><thead><tr><th>Employee</th><th>Shift</th><th>Shift From Date</th><th>Shift To Date</th><th>Status</th></tr></thead><tbody>';
                                $.each(response.records, function(index, record) {
                                    table += '<tr><td>' + record.employee.employee_code +' - '+ record.employee.first_name + record.employee.last_name + '</td><td>' + record.shift_type.shift_desc + '</td><td>' + record.shift_from_date + '</td><td>' + record.shift_to_date + '</td><td>' + record.status + '</td></tr>';
                                });
                                table += '</tbody></table>';
                                $("#show-existing-shifts-table").html(table);
                                $("#show-existing-shifts-table").show();
                            }
                            else{
                                $("#add_modal").modal("hide");
                            }
                        }
                        else if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: response.error_message
                            });
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                    }
                });
            });

            // Update Data
            $("#edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('shift_change') }}" + "/" + rowid,
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Shift Change Request has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
        });

        function shift_change_request_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('shift_change') }}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    $("#edit_employee").val(response.employee_id).select2();
                    $("#work_week_id").val(response.work_week_id).select2();
                    $("#shift_id").val(response.shift_id).select2();
                    $("#shift_from_date").val(response.shift_from_date);
                    $("#shift_to_date").val(response.shift_to_date);
                    $("#edit_remarks").val(response.remarks);
                    if (response.status == 'Approved') {
                        $('#edit_status').prop('checked', true);
                    }
                    $("#edit_modal").modal("show");
                },
            });
        }

        function shift_change_request_delete(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "shift_change/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Shift Change Requests has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        function get_department_employees(department_id, employee_id)
        {
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: "{{url('get_dept_employees')}}",
                data: {
                    department_id : department_id
                },
                success: function(data)
                {
                    $(employee_id).attr('disabled', false);
                    $(employee_id).html('');
                    $(employee_id).html('<option value="">Select Employee</option>'); 
                    $.each(data, function(index,employee){
                        var opt = $('<option>');
                        opt.val(employee.id);
                        opt.text(employee.employee_id+' - '+employee.employee_code+' - '+employee.first_name+' '+employee.middle_name +' '+employee.last_name + ' - ' + employee.designation+ ' - ' + employee.department);
                        $(employee_id).append(opt);
                    });
                }
            });
        }

        // Filter Function
        $('#search_form').submit(function(e) {
            datatable.draw();
            e.preventDefault();
        });
    </script>
@endsection
