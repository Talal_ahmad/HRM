@extends('Admin.layouts.master')
@section('title', 'Bulk Advance Approval')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Add_Bulk_Advance_Approval')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request_&_Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Add_Bulk_Advance_Approval')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <!--Search Form -->
                        <div class="card-body">
                            <form id="search_form">
                                <div class="row">
                                    @if (env('COMPANY') == 'RoofLine')
                                    <div class="col-md-5">
                                        <label class="form-label">@lang('app.Calculation_Groups'):</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="@lang('app.Select_Calculation_Group')" required>
                                            <option value=""></option>
                                            @foreach ($calculations_groups as $group)
                                                <option value="{{$group->id}}" {{$group->id == request('calculation_group') ? 'selected' : ''}}>{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-7">
                                        <label class="form-label">@lang('app.Employees'):</label>
                                        <select name="employeeFilter[]" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" multiple required>
                                            <option value=""></option>
                                            @if (isset($calculation_employees))
                                                @foreach ($calculation_employees as $employee)
                                                    <option value="{{$employee->id}}" {{!empty(request('employeeFilter')) ? in_array($employee->id,request('employeeFilter')) ? 'selected' : '' : ''}}>{{$employee->employee_id.' - '.$employee->employee_code.' - '.$employee->first_name.' '.$employee->last_name.' - '.$employee->title}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="button-container mt-1">
                                            <button class="btn btn-sm btn-primary" type="button"
                                                onclick="selectAll('#employeeFilter')">@lang('app.Select_All')</button>
                                            <button class="btn btn-sm btn-danger" type="button"
                                                onclick="deselectAll('#employeeFilter')">@lang('app.Deselect_All')</button>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-3">
                                        <label class="form-label">@lang('app.Employees'):</label>
                                        <select name="employeeFilter[]" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" multiple required>
                                            <option value=""></option>
                                            @foreach (employees() as $employee)
                                                <option value="{{$employee->id}}" {{!empty(request('employeeFilter')) ? in_array($employee->id,request('employeeFilter')) ? 'selected' : '' : ''}}>{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                    <div class="col-md-3">
                                        <label class="form-label" for="loan_type">@lang('app.Loan_Type'):</label>
                                        <select name="loan_type" id="loan_type" class="select2 form-select" data-placeholder="@lang('app.Select_Loan_Type')" required>
                                            <option value=""></option>
                                            @foreach ($loan_types as $loan_type)
                                                <option value="{{$loan_type->id}}" {{request('loan_type') == $loan_type->id ? 'selected' : ''}}>{{$loan_type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="advance">@lang('app.Advance_Percentage'):</label>
                                        <input type="number" id="advance" value="{{request('advance_percent')}}" class="form-control" placeholder="@lang('app.Advance_Percentage')" name="advance_percent" required/>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="month">@lang('app.Month'):</label>
                                        <input type="text" name="month" id="month" value="{{date('Y-m-d')}}" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="basic_salary">@lang('app.Basic_Salary_Column'):</label>
                                        <select name="basic_salary" id="basic_salary" class="select2 form-select" data-placeholder="@lang('app.Select_Basic_Salary_Column')" required>
                                            <option value=""></option>
                                            @foreach ($payroll_columns as $column)
                                                <option value="{{$column->id}}" {{request('basic_salary') == $column->id ? 'selected' : ''}}>{{$column->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{url('bulk_advance_approval/create')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <form id="add_form">
                        @csrf
                        <div class="card mb-1 pb-2">
                            <div class="card-header border-bottom p-1" style="user-select: auto;">
                                <div class="head-label" style="user-select: auto;">
                                    <h6 class="mb-0" style="user-select: auto;">@lang('app.Add_Bulk_Advance')</h6>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Employee ID</th>
                                            <th>Employee Code</th>
                                            <th>Employee</th>
                                            <th>Designation</th>
                                            <th>Department</th>
                                            <th>Basic salary</th>
                                            <th>Advance %</th>
                                            <th>Advance Amount</th>
                                            {{-- <th>Status</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($employees) && count($employees) > 0)
                                            @foreach ($employees as $key => $employee)
                                                <tr>
                                                    <td>{{$key + 1}}</td>
                                                    <td>{{$employee->employee_id}}</td>
                                                    <td>{{$employee->employee_code}}</td>
                                                    <td>{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}</td>
                                                    <td>{{$employee->designation}}</td>
                                                    <td>{{$employee->department}}</td>
                                                    <td>{{$employee->amount}}</td>
                                                    <td>
                                                        <input type="hidden" name="employees[]" value="{{$employee->id}}">
                                                        <input type="hidden" name="loan_type" value="{{request('loan_type')}}">
                                                        @if (env('COMPANY') == 'RoofLine')
                                                        <input type="hidden" name="calculation_group" value="{{request('calculation_group')}}">
                                                        @endif
                                                        <input type="hidden" name="start_date" value="{{request()->query('month')}}">
                                                        <input type="number" value="{{request()->query('advance_percent')}}" onkeyup="amount_cal(event , '#amount_{{$key}}' , {{$employee->amount}})" id="advance" class="form-control" name="advance_percent"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" value="{{  (request()->query('advance_percent')/100) * $employee->amount  }}" id="amount_{{$key}}" class="form-control" name="amount[{{$employee->id}}]"/>
                                                    </td>
                                                    {{-- <td>
                                                        <input type="checkbox" class="custom-control-input" name="status[{{$employee->id}}]" id="type" value="Approved"/>
                                                    </td> --}}
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if (isset($employees) && count($employees) > 0)
                        <div class="row">
                            <div class="col-12 text-end">
                                <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    @if(Session::has('message'))
        <script>
            Toast.fire({
                icon: 'error',
                title: "{!! Session::get('message') !!}"
            });
        </script>
    @endif
    <script>
        $(document).ready(function(){
            $("#calculation_group").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    url: "{{ url('bulk_advance_approval') }}"+"/"+optVal,
                    type: "GET",
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            employee_code = value.employee_code != null ? value.employee_code : ''
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+employee_code + ' - '+value.first_name +' '+ value.last_name + ' - ' + value.title)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            // Add Data
            $('#add_form').on('submit' , function(e){
                ButtonStatus('.form_save',true);
                blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{route('bulk_advance_approval.store')}}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#add_form')[0].reset();
                            Toast.fire({
                                icon: 'success',
                                title: 'Bulk Advance has been Added Successfully!'
                            })
                            setTimeout(function(){
                                window.location.href = "{{ url('bulk_advance_approval') }}";
                            }, 2000);
                        }
                    }
                });
            });
        });
        function amount_cal(e , amount_id , salary){
            var percent = e.target.value;
            var basic_salary = salary
            $(amount_id).val( (percent/100)*salary );
        }
    </script>
@endsection