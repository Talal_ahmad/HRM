@extends('Admin.layouts.master')
@section('title', 'Bulk Advance Approval')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Bulk_Advance_Approval')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Bulk_Advance_Approval')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    @if(userRoles()!="Self Service")
                        <div class="card mb-1">
                            <!--Search Form -->
                            <div class="card-body">
                                <form id="search_form">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-label" for="departmentFilter">@lang('app.Departments'):</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">@lang('app.Employees'):</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">

                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label" for="month">@lang('app.Month'):</label>
                                            <input type="month" id="month" class="form-control date" value="{{ empty(request('month')) ? date('Y-m') : request('month') }}" placeholder="Filter By Month" name="month" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{url('bulk_advance_approval')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                            <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mb-1">
                                <button type="submit" id="approve_loan" class="btn btn-primary">@lang('app.Approve')</button>
                            </div>
                        </div>
                    @endif
                    <div class="card mb-1 pb-2">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    @can('Approve Bulk Adavnce')    
                                        <th class="not_include">
                                            <div class="form-check"> 
                                                <input class="form-check-input bulk_all_delete" data-target="product-bulk-delete" type="checkbox" id="checkboxSelectAll" />
                                            </div>
                                        </th>
                                    @endcan
                                    <th>Sr.No</th>
                                    <th>Employee ID</th>
                                    <th>Employee Code</th>
                                    <th>Employee</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Amount</th>
                                    <th>Loan Type</th>
                                    <th>Status</th>
                                    <th>Start Date</th>
                                    <th>Requested Date Time</th>
                                    <th>Submitted By</th>
                                    <th class="not_include">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($loan_requests as $key => $loan_request)
                                    <tr id="product-bulk-delete">
                                        <td></td>
                                        @can('Approve Bulk Advance')    
                                            <td class="not_include">
                                                <div class="form-check">
                                                    @if ($loan_request->status != 'Approved')
                                                        <input class="form-check-input approveCheck bulk-item"  name="loan_request_{{$key}}" type="checkbox" value="{{$loan_request->id}}" id="loan_request_{{$key}}"/>
                                                    @endif
                                                </div>
                                            </td>
                                        @endcan
                                        <td>{{$key + 1}}</td>
                                        <td>{{$loan_request->employee_id}}</td>
                                        <td>{{$loan_request->employee_code}}</td>
                                        <td>{{$loan_request->first_name}} {{$loan_request->middle_name}} {{$loan_request->last_name}}</td>
                                        <td>{{$loan_request->designation}}</td>
                                        <td>{{$loan_request->title}}</td>
                                        <td>{{$loan_request->amount}}</td>
                                        <td>{{$loan_request->loan_type_name}}</td>
                                        <td><span class="card-text ms-sm-1 mb-0 {{ getbadge($loan_request->status) }}">{{$loan_request->status}}</span></td>
                                        <td>{{$loan_request->start_date}}</td>
                                        <td>{{$loan_request->created_at}}</td>
                                        <td>{{$loan_request->username}}</td>
                                        <td>
                                            <a href="{{url("loan_request")."/".$loan_request->id}}">
                                                <i class="fas fa-lg fa-eye text text-primary"></i>    
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - ' +value.employee_code + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation + ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });

            $('#dataTable').DataTable({
                ordering: false,
                "columnDefs": [
					{
						// For Responsive
						className: 'control',
						orderable: false,
						targets: 0
					},
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 30,
                aLengthMenu: [
                    [30,50,100,-1],
                    [30,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Bulk Advance Approval',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Bulk Advance Approval',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Bulk Advance Approval',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Bulk Advance Approval',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Bulk Advance Approval',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Add Bulk Advance')
                        {
                            text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add Advance',
                            className: 'create-new btn btn-primary',
                            action: function(e, dt, node, config) {
                                window.location.href = '{{ route('bulk_advance_approval.create') }}';
                            }
                        },
                    @endcan
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Bulk Advance Approval</h6>');
        });
        
        $(document).on('change','.bulk_all_delete',function(){
            let target = $(this).attr('data-target');
            if($(this).is(':checked')){
                $('#'+target+' .bulk-item').prop('checked',true);
            }else{
                $('#'+target+' .bulk-item').prop('checked',false);
            }
        });

        $('#approve_loan').click(function(){
            blockUI();
            $.ajax({
                url: "{{url('bulk_advance_approved')}}",
                type: 'GET',
                data: $('#dataTable').DataTable().$('input[type="checkbox"]').serialize(),
                success: function(response){
                    $.unblockUI();
                    if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        });
                    }
                    else if(response.message){
                        Toast.fire({
                            icon: 'error',
                            title: response.message
                        });
                    }
                    else{
                        Toast.fire({
                            icon: 'success',
                            title: 'Bulk Advance has been Approved Successfully!'
                        });
                        setTimeout(function(){
                            window.location.href = "{{ url('bulk_advance_approval') }}";
                        }, 2000);
                    }
                } 
            });
        })
    </script>
@endsection