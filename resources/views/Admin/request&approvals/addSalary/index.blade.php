@extends('Admin.layouts.master')
@section('title', 'Add Salary Component Request')
@section('content') 
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Add Salary Component Request</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Request&Approval</a>
                            </li>
                            <li class="breadcrumb-item active">Add Salary Component Request
                            </li>
                        </ol>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        {{-- @if(userRoles()!="Self Service")
                            <!--Search Form --> 
                            <div class="card-body">
                                <form id="search_form"> 
                                    <div class="row g-1 mb-md-1">
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" required>
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label" for="employeeFilter">Employee</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="Select Employee">
                                                <option value=""></option>
                                                @foreach (employees('','loanRequest', true) as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="fromDateFilter" class="form-label">Request From:</label>
                                            <input type="text" name="fromDateFilter" id="fromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                        <div for="toDateFilter" class="col-md-3">
                                            <label class="form-label">Request To:</label>
                                            <input type="text" name="toDateFilter" id="toDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                        <div for="dedFromDateFilter" class="col-md-3">
                                            <label class="form-label">Deduction From:</label>
                                            <input type="text" name="dedFromDateFilter" id="dedFromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                        <div for="dedToDateFilter" class="col-md-3">
                                            <label class="form-label">Deduction To:</label>
                                            <input type="text" name="dedToDateFilter" id="dedToDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="statusFilter">Status</label>
                                            <select name="statusFilter" id="statusFilter" class="select2 form-select" data-placeholder="Select Status">
                                                <option value=""></option>
                                                <option value="unpaid">Unpaid</option>
                                                <option value="paid">Paid</option>
                                            </select>
                                        </div>
                                        <div style="margin-top:35px" class="col-md-3">
                                            <a href="{{url('loan_request')}}" type="button" class="btn btn-danger">Reset</a>
                                            <button class="btn btn-primary" type="submit">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif --}}
                    </div>
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.<br>No</th>
                                    <th>Employee Code</th>
                                    <th>Employee</th>
                                    <th></th>
                                    <th></th>
                                    <th>Department</th>
                                    <th>Component</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        
        <!--Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Salary Request</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_salary_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="departmentFilter">Select Department:</label>
                                        <select name="departmentFilter" id="department" class="select2 form-select" data-placeholder="Select Department" required>
                                            <option value=""></option>
                                            <option value="all">All</option>
                                            @foreach (departments() as $department)
                                                <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="employeeFilter">Employee</label>
                                        <select name="employee_id_send" id="employees" class="select2 form-select" data-placeholder="Select Employee">
                                            <option value=""></option>
                                            @foreach (employees() as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 d-none" id="component">
                                    <div class="mb-1">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Salary Component</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody id="append_rows">
        
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="calculation_group">Calculation group:</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="Select Department" required>
                                            <option value=""></option>
                                            @foreach ($CalculationGroups as $item)
                                                <option value="{{$item->id}}" {{$item->id == request('calculation_group') ? 'selected' : ''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                {{-- @can('Add Salary Request Approve Now')    
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="status" id="type" value="Approved"/>
                                                <label class="custom-control-label" for="type">Request Approve Now</label>
                                            </div> 
                                        </div>
                                    </div>
                                @endcan --}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Edit Modal --}}
        {{-- <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Loan Request</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="employee_id">Employee</label>
                                        <select name="employee_id_send" id="edit_employee_id" class="select2 form-select" data-placeholder="Select Employee" required >
                                            <option value=""></option>
                                            @foreach (employees('', '', true) as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="start_date">Start Date</label>
                                        <input type="text" name="start_date" id="edit_start_date" class="form-control flatpickr-basic flatpickr-input active" placeholder="YYYY-MM-DD" required >
                                    </div>
                                </div> 
                                @if (env('COMPANY')=='RoofLine')
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="deduction_group">Calculation Groups</label>
                                            <select type="select-multi" name="calculation_group" id="deduction_group" class="select2 form-select @error('deduction_group') is-invalid @enderror" data-placeholder="Select Calculation Group" required>
                                                <option value=""></option>
                                                @foreach ($calculationGroup as $group)
                                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                @endforeach 
                                            </select>
                                            @error('deduction_group')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                @endif
                                @can('Loan/Advance Request Approve Now')    
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="status" id="edit_status" value="Approved"/>
                                                <label class="custom-control-label" for="type">Request Approve Now</label>
                                            </div> 
                                        </div>
                                    </div>
                                @endcan
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> --}}
    </div>
</section>

@endsection

@section('scripts')
    <script>
        var rowid; 
        $(document).ready(function(){
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching : true,
                ajax: {
                    url: "{{ route('addSalary.index') }}",
                    data: function (filter) {
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.typeFilter = $('#typeFilter').val();
                        filter.section = $('#section').val();
                        filter.fromDateFilter = $('#fromDateFilter').val();
                        filter.toDateFilter = $('#toDateFilter').val();
                        filter.dedFromDateFilter = $('#dedFromDateFilter').val();
                        filter.dedToDateFilter = $('#dedToDateFilter').val();
                        filter.statusFilter = $('#statusFilter').val();
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
            
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        data: 'empName', 
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + "<br>" + middleName + " " + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'title',
                        name: 'companystructures.title',
                        searchable: true,
                    }, 
                    {
                        data: 'component_name',
                        name: 'salarycomponent.name',
                        searchable: true,
                    },
                    {
                        data: 'amount',
                        name: 'amount',
                    },
                    {
                        data: 'status',
                        name: 'employeesalary_pendings.status',
                    },
                ], 
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Add Loan')
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Add Salary Request</h6>');
            $("#department").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'loan_request'
                    },
                    success: function(response) {
                        $('#employees').empty();
                        $('#employees').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employees').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.employee_code+ ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation+' - '+ value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            // get employees components
            $('#employees').on('change', function(){
                var components = <?php echo json_encode($salary_components); ?>;
                $('#component').removeClass('d-none');
                $('#append_rows').html('');
                $.each(components, function(index,component){
                    var html = `<tr>
                        <td>${component.name}</td>
                        <td>
                            <input type="hidden" name="salary_id[]" value="${component.id}">
                            <input type="text" class="form-control" name="salary_amount[${component.id}]" value="0.00">
                        </td>
                    </tr>`
                    $('#append_rows').append(html);
                });
            });
        });
        // Add Data
        $('#add_salary_form').submit(function(e){
            ButtonStatus('.form_save',true);
                blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: "{{route('addSalary.store')}}",
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(response){
                    ButtonStatus('.form_save',false);
                    $.unblockUI();
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        $('#add_salary_form')[0].reset();
                        $(".trig").val('').trigger('change');
                        $("#add_modal").modal("hide");
                        dataTable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Salary Request has been Added Successfully!'
                        })
                    }
                }
            });
        })
        // Update Data
        $('#edit_form').on('submit' , function(e){
            ButtonStatus('.form_save',true);
            blockUI();
            e.preventDefault();
            $.ajax({
                url: "{{url('loan_request')}}" + "/" + rowid,
                type: "POST",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(response){
                    ButtonStatus('.form_save',false);
                    $.unblockUI();
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        $("#edit_modal").modal("hide");
                        $(".trig").val('').trigger('change');
                        dataTable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Loan Request has been Updated Successfully!'
                        })
                    }
                }
            });
        });
        function edit(id){
            rowid = id;
            $.ajax({
                url: "{{url('loan_request')}}" + "/" + id + "/edit",
                type: "GET",
                success: function(response){
                    $("#edit_employee_id").val(response.employee_id).select2();
                    $("#edit_deduction_group").val(response.deduction_group).select2();
                    $("#edit_loan_type").val(response.loan_type).select2();
                    $("#edit_start_date").val(response.start_date);
                    $("#edit_amount").val(response.amount);
                    $("#edit_time_duration").val(response.time_duration);
                    $("#edit_remarks").val(response.remarks);
                    if(response.status == 'Approved'){
                        $('#edit_status').prop('checked', true);
                    }
                    $("#edit_modal").modal("show");
                }
            });
        } 
        function delete_item(id){
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                    url: "loan_request/" + id,
                    type: "DELETE",
                    data : {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (response) {
                        if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Loan Request has been Deleted Successfully!'
                            })
                        }
                    }
                });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Filter Function
        $('#search_form').submit(function(e){
            dataTable.draw();
            e.preventDefault();
        });
    </script>
@endsection
