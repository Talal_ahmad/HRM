@extends('Admin.layouts.master')
@section('title', 'Overtime Management')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Overtime_Management')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Overtime_Management')
                                </li>
                            </ol>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            @if(userRoles()!="Self Service")
                                <!--Search Form -->
                                <div class="card-body">
                                    <form id="search_form">
                                        <div class="row g-1 mb-md-1">
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.Department'):</label>
                                                <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                    <option value=""></option>
                                                    <option value="all">All</option>
                                                    @foreach (departments() as $department)
                                                        <option value="{{$department->id}}">{{$department->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if (env('COMPANY') == 'JSML')        
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="title">@lang('app.Section')</label>
                                                        <select name="section[]" id="section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                            
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                        </div>
                                                    </div>
                                                </div>    
                                            @endif
                                            <div class="col-md-3">
                                                <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                                <select name="employeeFilter[]" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" multiple>
        
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.Status'):</label>
                                                <select name="statusFilter" id="statusFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Leave_Status')">
                                                    <option value=""></option>
                                                    <option value="Pending">@lang('app.Pending')</option>
                                                    <option value="Approved">@lang('app.Approved')</option>
                                                    <option value="Rejected">@lang('app.Rejected')</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.From_Date'):</label>
                                                <input type="text" name="fromDateFilter" id="fromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">@lang('app.To_Date'):</label>
                                                <input type="text" name="toDateFilter" id="toDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                            </div>
                                            <div style="margin-top:37px" class="col-md-3">
                                                <button class="btn btn-primary">@lang('app.Filter')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Department</th>
                                        <th>Shift</th>
                                        <th>Overtime Date</th>
                                        <th>Overtime Start</th>
                                        <th>Overtime End</th>
                                        <th>Overtime (Hours)</th>
                                        <th>Remarks</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th colspan="12" style="text-align:right">Total Over Time Hours:</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Add Modal -->
            <div class="modal fade text-start" id="add_modal" data-bs-focus="false" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Overtime_Management')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        @php
                            if (env('COMPANY') == 'JSML') {
                                $set_col = 'col-md-4 col-12';
                            }else{
                                $set_col = 'col-md-6 col-12';
                            }
                        @endphp
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="{{$set_col}}">
                                        <div class="mb-1">
                                            <label class="form-label" for="department">@lang('app.Select_Department')</label>
                                            <select id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}">{{ $department->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @if (env('COMPANY') == 'JSML')        
                                        <div class="{{$set_col}}">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">@lang('app.Section')</label>
                                                <select name="section[]" id="get_emp_section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                    
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#get_emp_section')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#get_emp_section')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>    
                                    @endif
                                    <div class="{{$set_col}}">
                                        <div class="mb-1">
                                            <label class="form-label" for="employees">@lang('app.Employee')</label>
                                            <select name="employee_id[]" id="employees" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" disabled multiple required>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="overtime_date">@lang('app.Date')</label>
                                            <input type="date" name="overtime_date" id="overtime_date" class="form-control" placeholder="YYYY-MM-DD" required readonly>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="shift_detail_table" style="display: none">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Overtime Date</th>
                                                        <th>Shift Type</th>
                                                        <th>Work Week</th>
                                                        <th>Shift Start Time</th>
                                                        <th>Shift End Time</th>
                                                        <th>In time</th>
                                                        <th>Out Time</th>
                                                        <th>Leave</th>
                                                        <th>Holiday</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="append_data">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="start_datetime">@lang('app.Start_DateTime')</label>
                                            <input type="text" name="start_datetime" id="start_datetime" class="form-control flatpickr-date-time" placeholder="YYYY-MM-DD HH:MM" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="end_datetime">@lang('app.End_DateTime')</label>
                                            <input type="text" name="end_datetime" id="end_datetime" class="form-control flatpickr-date-time" placeholder="YYYY-MM-DD HH:MM" required/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="attachment">@lang('app.Attachments')</label>
                                            <input type="file" id="attachment" class="form-control" placeholder="" name="attachment"/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="type" class="form-label">@lang('app.Remarks')</label>
                                            <textarea name="remarks" placeholder="@lang('app.Remarks')" class="form-control" id="type" rows="3"></textarea>
                                        </div>
                                    </div>
                                    @can('Overtime Management Request Approve Now')    
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="status" id="type" value="Approved" />
                                                    <label class="custom-control-label" for="type">@lang('app.Request_Approve_Now')</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
     <!-- status Change Modal -->
     <div class="modal fade text-start" id="status_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Leave_Request_Status')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="status_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">      
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_status">@lang('app.Status')</label>
                                        <select name="status" id="edit_status" class="select2 form-select" data-placeholder="Select Status">
                                            <option value=""></option>
                                            <option value="Approved">@lang('app.Approved')</option>
                                            <option value="Cancelled">@lang('app.Cancelled')</option>
                                            <option value="Rejected">@lang('app.Rejected')</option>
                                            <option value="Pending">@lang('app.Pending')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            $("#departmentFilter").change(function() {
                // var optVal = $(this).val();
                var department_id = $(this).val();
                var section = '#section';
                get_department_child(department_id,section);
            });
            $("#department").change(function() {
                var department_id = $(this).val();
                var section = '#get_emp_section';
                get_department_child(department_id,section);
            });
            function get_department_child(department_id,section)
            {
                if (department_id.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: department_id,
                        },
                        success: function(response) {
                            $(section).empty();
                            $(section).html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $(section).append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $(section).empty();
                }
            }
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: true,
                ajax: {
                    url: "{{ route('overtime_managements.index') }}",
                    data: function (filter) {
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.section = $('#section').val();
                        filter.statusFilter = $('#statusFilter').val();
                        filter.fromDateFilter = $('#fromDateFilter').val();
                        filter.toDateFilter = $('#toDateFilter').val();
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        data: 'employeeName',
                        render: function(data, type, row) {
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + ' ' + middleName + ' ' + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'title',
                        name: 'companystructures.title'
                    },
                    {
                        data: 'shift_desc',
                        name: 'shift_type.shift_desc'
                    },
                    {
                        data: 'overtime_date'
                    },
                    {
                        data: 'overtime_start_time_stamp'
                    },
                    {
                        data: 'overtime_end_time_stamp'
                    },
                    {
                        data: 'overtime'
                    },
                    {
                        data: 'remarks'
                    },
                    {
                        data: 'status'
                    },
                    
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            var btn = '';
                            @can('Overtime Change Status Button')    
                            btn = '<a href="javascript:;" class="item-edit" title="Change Status" onclick=change_status('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4 me-1' }) +
                                '</a>';
                            @endcan
                            @can('Overtime Void Button')    
                            btn += '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')" title="Void Entry">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>';
                            @endcan
                            return btn;
                        }
                    },
                   
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }

                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                },
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api();
                    var overtimeTotal = api.ajax.json().totalOvertime; // Access totalOvertime from JSON response
                    
                    // Update footer with the sum
                    $(api.column(12).footer()).html('Total Over Time Hours: ' + overtimeTotal);
                },
            });

            // get department employees
            $('#department').on('change', function(){
                var department_id = $(this).val();
                var employee_id = '#employees';
                get_department_employees(department_id,employee_id);
            });

            // Filter department Employees
            $('#departmentFilter').on('change', function(){
                var department_id = $(this).val();
                var employee_id = '#employeeFilter';
                get_department_employees(department_id,employee_id);
            });

            $('#overtime_date').on('change', function(){
                var employee_id = $('#employees').val();
                if(employee_id == '' || employee_id == 'undefined')
                {
                    alert('Please Select Employee');
                    $('#overtime_date').val('');
                    return false;
                }
                var overtime_date = $(this).val();
                $.ajax({
                    url: "{{ route('overtime_managements.create') }}",
                    type:"GET",
                    data:{'employee_id':employee_id, 'overtime_date':overtime_date},
                    success: function(response) {
                        if (response.code == 500) {
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            });
                        } else {
                            $('#shift_detail_table').show();
                            $('#append_data').html(''); // Clear existing data
                            response.forEach(employee => {
                                $('#append_data').append(`
                                    <tr>
                                        <td>${employee.name}</td>
                                        <td>${overtime_date}</td>
                                        <td>${employee.shift_name}</td>
                                        <td>${employee.work_week}</td>
                                        <td>${employee.shift_start_time}</td>
                                        <td>${employee.shift_end_time}</td>
                                        <td>${employee.in_time}</td>
                                        <td>${employee.out_time}</td>
                                        <td>${employee.leave}</td>
                                        <td>${employee.is_holiday}</td>
                                    </tr>
                                `);
                            });
                        }
                    }
                });
            });

            $('#add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('overtime_managements.store') }}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } 
                        else if (response.code == 300) {
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            })
                        }
                        else {
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Overtime Managements has been Added Successfully!'
                            })
                        }
                    }
                });
            })

            $('div.head-label').html('<h6 class="mb-0">List of Overtime Management</h6>');
        });

        $('#section').on('change', function(){
            var department_id = $(this).val();
            var employee_id = '#employeeFilter';
            get_section_employees(department_id,employee_id);
        });

        $('#get_emp_section').on('change', function(){
            var department_id = $(this).val();
            var employee_id = '#employees';
            get_section_employees(department_id,employee_id);
        });

        function get_section_employees(department_id, employee_id)
        {
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: "{{url('get_department_employees_from_section')}}",
                data: {
                    department_id : department_id,
                    overtime_request : 'overtime_request'
                },
                success: function(data)
                {
                    $('#overtime_date').attr('readonly', false);
                    $(employee_id).attr('disabled', false);
                    $(employee_id).html('');
                    $(employee_id).html('<option value="">Select Employee</option>'); 
                    $.each(data, function(index,employee){
                        var opt = $('<option>');
                        opt.val(employee.id);
                        opt.text(employee.employee_id+' - '+employee.employee_code+' - '+employee.first_name+' '+employee.middle_name+ ' ' +employee.last_name + ' - ' + employee.designation+ ' - ' + employee.department);
                        $(employee_id).append(opt);
                    });
                }
            });
        }

        function get_department_employees(department_id, employee_id)
        {
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: "{{url('get_dept_employees')}}",
                data: {
                    department_id : department_id
                },
                success: function(data)
                {
                    $('#overtime_date').attr('readonly', false);
                    $(employee_id).attr('disabled', false);
                    $(employee_id).html('');
                    $(employee_id).html('<option value="">Select Employee</option>'); 
                    $.each(data, function(index,employee){
                        var opt = $('<option>');
                        opt.val(employee.id);
                        opt.text(employee.employee_id+' - '+employee.employee_code+' - '+employee.first_name+' '+employee.middle_name+ ' ' +employee.last_name + ' - ' + employee.designation+ ' - ' + employee.department);
                        $(employee_id).append(opt);
                    });
                }
            });
        }
        
        // DataTable After Filter
        $('#search_form').submit(function(e){
            datatable.draw();
            e.preventDefault();
        });
        function change_status(id){
            rowid = id;
            $.ajax({
                url: "{{url('overtime_managements')}}" + "/" + id + "/edit",
                type: "GET",
                data: {
                    type: 'status'
                },
                success: function(response){
                    $("#edit_status").val(response.status).select2();
                    $("#status_modal").modal('show');
                }
            })
        }
        $('#status_form').submit(function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'status');
                $.ajax({
                    url: "{{url('overtime_managements')}}" + "/" + rowid,
                    type: 'post',
                    data:formData,
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#status_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#status_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Overtime status has been Updated Successfully!'
                            })
                        }
                    } 
                });
            });
        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to Void Entry!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.blockUI();
                            $.ajax({
                                url: "overtime_managements/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    $.unblockUI();
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'OverTime Management Entry has been Voided Successfully!',
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
