@extends('Admin.layouts.master')
@section('title', 'Approvals')

@section('style')
<style>
    @media (min-width: 250px) and (max-width: 383px) {
        .actionBtn {
            padding: 0.486rem 1rem;
            font-size: 0.9rem;
            border-radius: 0.358rem;
        }
    }

    .pagination {
        float: right;
    }
</style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Approvals')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Approvals')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row">
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-pills flex-row nav-left" id="myTab">
                    <!-- All Request -->
                    <li class="nav-item">
                        <a class="nav-link {{empty($type) ? 'active' : ''}}" id="account-pill-requestAll" href="{{url('approvals')}}">
                            <span class="fw-bold">@lang('app.All_Request')</span>
                        </a>
                    </li>
                    <!-- Department Change -->
                    <li class="nav-item">
                        <a class="nav-link {{$type == 'department_change' ? 'active' : ''}}" id="account-pill-departmentChange" href="{{url('approvals')}}?type=department_change">
                            <span class="fw-bold">@lang('app.Department_Change')</span>
                        </a>
                    </li>
                    <!-- Leave Request -->
                    <li class="nav-item">
                        <a class="nav-link {{$type == 'leaveRequest' ? 'active' : ''}}" id="account-pill-leaveRequest" href="{{url('approvals')}}?type=leaveRequest">
                            <span class="fw-bold">@lang('app.Leave_Request')</span>
                        </a>
                    </li>
                    <!-- Salary Component Request -->
                    <li class="nav-item">
                        <a class="nav-link {{$type == 'addSalarychange' ? 'active' : ''}}" id="account-pill-departmentChange" href="{{url('approvals')}}?type=addSalarychange">
                            <span class="fw-bold">Salary Component Request</span>
                        </a>
                    </li>
                    <!-- Short Leave Request -->
                    <li class="nav-item">
                        <a class="nav-link {{$type == 'shortLeaveRequest' ? 'active' : ''}}" id="account-pill-shortLeaveRequest" href="{{url('approvals')}}?type=shortLeaveRequest">
                            <span class="fw-bold">@lang('app.Short_Leave_Request')</span>
                        </a>
                    </li>
                    <!-- Overtime Request -->
                    <li class="nav-item">
                        <a class="nav-link {{$type == 'overtimeRequest' ? 'active' : ''}}" id="account-pill-overtimeManagement" href="{{url('approvals')}}?type=overtimeRequest">
                            <span class="fw-bold">@lang('app.Overtime_Request')</span>
                        </a>
                    </li>
                    <!-- Shift Change Request -->
                    <li class="nav-item">
                        <a class="nav-link {{$type == 'shiftChange' ? 'active' : ''}}" id="account-pill-shiftChange" href="{{url('approvals')}}?type=shiftChange">
                            <span class="fw-bold">@lang('app.Shift_Change_Request')</span>
                        </a>
                    </li>
                    <!-- Loan Request -->
                    <li class="nav-item">
                        <a class="nav-link {{$type == 'loan' ? 'active' : ''}}" id="account-pill-loan" href="{{url('approvals')}}?type=loan">
                            <span class="fw-bold">@lang('app.Loan/Advance_Request')</span>
                        </a>
                    </li>
                    <!-- Miscellaneous Request -->
                    <li class="nav-item">
                        <a class="nav-link {{$type == 'miscellaneous' ? 'active' : ''}}" id="account-pill-miscellaneous" href="{{url('approvals')}}?type=miscellaneous">
                            <span class="fw-bold">@lang('app.Miscellaneous_Request')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{$type == 'fineconcession' ? 'active' : ''}}" id="account-pill-fineconcession" href="{{url('approvals')}}?type=fineconcession">
                            <span class="fw-bold">@lang('app.Fine_Concession_Request')</span>
                        </a>
                    </li>

                </ul>
                @if (userRoles()!="Self Service") 
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form" action="{{route('approvals.index')}}" method="GET">
                                        <input type="hidden" name="type" value="{{request('type')}}">
                                        <div class="row">
                                            <div class="col-md-3 col-12">
                                                <label class="form-label">@lang('app.Departments'):</label>
                                                <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')">
                                                    <option value=""></option>
                                                    <option value="all">@lang('app.All_Departments')</option>
                                                    @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{ $department->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <label class="form-label">@lang('app.Filter_By_Employee'):</label>
                                                <select name="employeeFilter" id="employeeFilter" class="select2 form-select filter" data-placeholder="@lang('app.Select_Employee')">
                                                    <option value="">@lang('app.Select_Employee')</option>
                                                    @foreach (employees() as $employee)
                                                    <option value="{{ $employee->id }}" {{$employee->id == request('employeeFilter') ? 'selected' : ''}}>{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <label class="form-label">@lang('app.Filter_By_Designation'):</label>
                                                <select name="designationFilter" id="designationFilter" class="select2 form-select filter" data-placeholder="@lang('app.Select_Designation')">
                                                    <option value="">@lang('app.Select_Designation')</option>
                                                    @foreach ($job_titles as $item)
                                                    <option value="{{ $item->id }}" {{$item->id == request('designationFilter') ? 'selected' : ''}}>{{$item->code}}-{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="statusFilter">@lang('app.Filter_By_Status')</label>
                                                    <select name="statusFilter" id="statusFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Status')">
                                                        <option value="">@lang('app.Select_Status')</option>
                                                        <option value="all" {{'all' == request('statusFilter') ? 'selected' : ''}}>All</option>
                                                        <option value="Pending" {{'Pending' == request('statusFilter') ? 'selected' : ''}}>Pending</option>
                                                        <option value="Approved" {{'Approved' == request('statusFilter') ? 'selected' : ''}}>Approved</option>
                                                        <option value="Rejected" {{'Rejected' == request('statusFilter') ? 'selected' : ''}}>Rejected</option>
                                                        <option value="Cancelled" {{'Cancelled' == request('statusFilter') ? 'selected' : ''}}>Cancelled</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="selectFromDate">@lang('app.From_Date'):</label>
                                                    <input type="text" name="selectFromDate" id="selectFromDate" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" value="{{request('selectFromDate')}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="selectToDate">@lang('app.To_Date'):</label>
                                                    <input type="text" name="selectToDate" id="selectToDate" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" value="{{request('selectToDate')}}" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 text-end">
                                                    <a href="{{url('approvals')}}?type={{ \Request::input('type') }}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                                    <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="tab-content">
                    <!-- All Request Tab -->
                    <div role="tabpanel" class="tab-pane {{$type == '' ? 'active' : ''}}" id="account-vertical-requestAll" aria-labelledby="account-pill-departmentChange">
                        <div class="row" id="requestAllTab">
                            @if ($type=='')
                            @if (count($department_change_requests) > 0 || count($leave_requests) > 0 || count($short_leave_requests) > 0 || count($overtime_requests) > 0 || count($shift_change_requests) > 0 || count($loan_requests) > 0 || count($miscellaneous_requests) > 0)
                            {{-- Department change --}}
                            @if (count($department_change_requests) > 0)
                            @foreach ($department_change_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Department Change Requests</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">Department From:</span>
                                                                            <span class="card-text mb-0">{{$request->title}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0  ms-1">Department To:</span>
                                                                            <span class="card-text mb-0">{{$request->dept_to}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                            <span class="card-text mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-1">Submitted By:</span>
                                                                            <span class="card-text  mb-0">{{$request->creator}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex flex-wrap align-items-center mt-1">
                                                            @if ($request->status == "Pending")
                                                            @can('Department Change Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 mb-1 me-1">
                                                                    <button class="actionBtn btn btn-success print_btn" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'department_change')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Department Change Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 mb-1 me-1">
                                                                    <button class="actionBtn btn btn-warning print_btn" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_department">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Department Change Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 mb-1 me-1">
                                                                    <button class="actionBtn btn btn-danger print_btn" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'department_change')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 mb-1 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'department_change')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_department" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Department Change Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'department_change')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    @if (!empty($type))
                                    <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found">
                                    @endif
                                </div>
                            </div>
                            @endif


                            <!-- Leave Request Tab -->
                            @if (count($leave_requests) > 0)
                            @foreach ($leave_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Employee Leave Requests</h3>
                                                            {{-- @if($request->status == 'Approved' || env('COMPANY') == 'Ajmal Dawakhana') --}}
                                                            @can('Employee leave records in approval')
                                                                <div class="text-end mt-1">
                                                                    <button class="btn btn-sm btn-success print_btn" onclick="leave_history(<?php echo $request->id; ?>)">View Leaves</button>
                                                                </div>
                                                            @endcan
                                                            {{-- @endif --}}
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Leave Type:</span>
                                                                        <span class="card-text ms-1 mb-0">{{HandleEmpty($request->lt_name)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">From:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->date_start}}</span>
                                                                        <span class="card-text user-info-title fw-bolder ms-sm-1 mb-0">To:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->date_end}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')

                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Leave Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'leave_request')">Allow</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Leave Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_leave">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Leave Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'leave_request')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'leave_request')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_leave" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Employee Leave Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'leave_request')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    @if (!empty($type))
                                    <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found">
                                    @endif
                                </div>
                            </div>
                            @endif

                            <!-- Short Leave Tab -->
                            @if (count($short_leave_requests) > 0)
                            @foreach ($short_leave_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Short Leave Request</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">From:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->time_from}}</span>
                                                                        <span class="card-text user-info-title fw-bolder ms-sm-1 mb-0">To:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->time_to}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->remarks}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-md-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Short Leave Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="actionBtn btn btn-success print_btn" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'short_leave')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Short Leave Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="actionBtn btn btn-warning print_btn" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_short_leave">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Short Leave Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="actionBtn btn btn-danger print_btn" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'short_leave')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'short_leave')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_short_leave" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Short Leave Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'short_leave')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    @if (!empty($type))
                                    <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found">
                                    @endif
                                </div>
                            </div>
                            @endif

                            <!-- Overtime Management Tab -->
                            @if (count($overtime_requests) > 0)
                            @foreach ($overtime_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Overtime Requests</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Overtime Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->overtime_date}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Overtime Hours:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->overtime}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Overtime Management Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'overtime_request')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Overtime Management Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_overtime">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Overtime Management Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'overtime_request')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'overtime_request')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_overtime" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Overtime Request Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'overtime_request')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    @if (!empty($type))
                                    <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found">
                                    @endif
                                </div>
                            </div>
                            @endif

                            <!-- Shift Change Tab -->
                            @if (count($shift_change_requests) > 0)
                            @foreach ($shift_change_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Shift Change Requests</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Shift Type:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->shift_desc}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Shift Change Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'shift_change')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Shift Change Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_shift">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Shift Change Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'shift_change')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'shift_change')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_shift" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Shift Change Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'shift_change')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    @if (!empty($type))
                                    <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found">
                                    @endif
                                </div>
                            </div>
                            @endif

                            <!-- Loan Tab -->
                            @if (count($loan_requests) > 0)
                            @foreach ($loan_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Loan Requests</h3>
                                                            @if ($request->status == 'Approved')
                                                            <div class="text-end mt-1">
                                                                <span><a class="btn btn-sm btn-success print_btn" target="_blank" href="loan_request/<?php echo $request->id; ?>">View Loan</a>
                                                                    @if (!empty($request->payment_id))
                                                                    <a href="javascript:;" class="btn btn-sm btn-success print_btn" data-bs-toggle="tooltip" onclick="payment_details({{ $request->payment_id }})">Loan Payment Details</a>
                                                                    @endif
                                                            </div>
                                                            @endif
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Amount:</span>
                                                                        <span class="card-text ms-1 mb-0">{{$request->amount}}</span>
                                                                        <span class="card-text user-info-title fw-bold mb-0 ms-1">Time Duration:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->time_duration}} Months</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Status:</span>
                                                                        <span class="card-text ms-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bold mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Loan/Advance Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'loan_request')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Loan/Advance Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_loan">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Loan/Advance Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'loan_request')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'loan_request')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_loan" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Loan Request Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'loan_request')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    @if (!empty($type))
                                    <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found">
                                    @endif
                                </div>
                            </div>
                            @endif

                            <!-- Miscellaneous Tab -->
                            @if (count($miscellaneous_requests) > 0)
                            @foreach ($miscellaneous_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Miscellaneous Requests</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Description:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->description}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Feedback:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->feedback)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</p>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Miscellaneous Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'miscellaneous_request')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Miscellaneous Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_misclaneous">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Miscellaneous Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'miscellaneous_request')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'miscellaneous_request')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_misclaneous" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Miscellaneous Request Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remark" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'miscellaneous_request')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    @if (!empty($type))
                                    <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found">
                                    @endif
                                </div>
                            </div>
                            @endif


                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif
                            @endif
                        </div>
                    </div>

                    <!-- Department Change Tab -->
                    <div role="tabpanel" class="tab-pane {{$type == 'department_change' ? 'active' : ''}}" id="account-vertical-departmentChange" aria-labelledby="account-pill-departmentChange">
                        <div class="row" id="department_change_tab">
                            @if (count($department_change_requests) > 0 && $type=='department_change')
                            @foreach ($department_change_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Department Change Requests</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">Department From:</span>
                                                                            <span class="card-text mb-0">{{$request->title}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0  ms-1">Department To:</span>
                                                                            <span class="card-text mb-0">{{$request->dept_to}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                            <span class="card-text mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-1">Submitted By:</span>
                                                                            <span class="card-text  mb-0">{{$request->creator}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                            {{-- <span class="card-text  mb-0">{{ $request->approvedBy }}</span> --}}
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex flex-wrap align-items-center mt-1">
                                                            @if ($request->status == "Pending")
                                                            @can('Department Change Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 mb-1 me-1">
                                                                    <button class="actionBtn btn btn-success print_btn" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'department_change')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Department Change Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 mb-1 me-1">
                                                                    <button class="actionBtn btn btn-warning print_btn" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_department">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Department Change Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 mb-1 me-1">
                                                                    <button class="actionBtn btn btn-danger print_btn" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'department_change')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 mb-1 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'department_change')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_department" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Department Change Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'department_change')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="float-end">{{ $department_change_requests->appends(request()->query())->links('pagination::bootstrap-4') }}</div>
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <!-- Leave Request Tab -->
                    <div class="tab-pane {{$type == 'leaveRequest' ? 'active' : ''}}" id="account-vertical-leaveRequest" role="tabpanel" aria-labelledby="account-pill-leaveRequest">
                        <div class="row" id="leaveRequest_tab">
                            @if (count($leave_requests) > 0 && $type=='leaveRequest')
                            @foreach ($leave_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Employee Leave Requests</h3>
                                                            {{-- @if($request->status == 'Approved' || env('COMPANY') == 'Ajmal Dawakhana') --}}
                                                                @can('Employee leave records in approval')
                                                                    <div class="text-end mt-1">
                                                                        <button class="btn btn-sm btn-success print_btn" onclick="leave_history(<?php echo $request->id; ?>)">View Leaves</button>
                                                                    </div>
                                                                @endcan
                                                            {{-- @endif --}}
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Leave Type:</span>
                                                                        <span class="card-text ms-1 mb-0">{{HandleEmpty($request->lt_name)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">From:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->date_start}}</span>
                                                                        <span class="card-text user-info-title fw-bolder ms-sm-1 mb-0">To:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->date_end}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Leave Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'leave_request')">Allow</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Leave Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_leave">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Leave Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'leave_request')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'leave_request')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_leave" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Employee Leave Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'leave_request')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="float-end">{{ $leave_requests->appends(request()->query())->links('pagination::bootstrap-4') }}</div>
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <!-- Short Leave Tab -->
                    <div class="tab-pane {{$type == 'shortLeaveRequest' ? 'active' : ''}}" id="account-vertical-shortLeaveRequest" role="tabpanel" aria-labelledby="account-pill-shortLeaveRequest">
                        <div class="row" id="shortLeave_tab">
                            @if (count($short_leave_requests) > 0 && $type=='shortLeaveRequest')
                            @foreach ($short_leave_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Short Leave Request</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">From:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->time_from}}</span>
                                                                        <span class="card-text user-info-title fw-bolder ms-sm-1 mb-0">To:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->time_to}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->remarks}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-md-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Short Leave Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="actionBtn btn btn-success print_btn" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'short_leave')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Short Leave Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="actionBtn btn btn-warning print_btn" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_short_leave_1">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Short Leave Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="actionBtn btn btn-danger print_btn" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'short_leave')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'short_leave')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_short_leave_1" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Short Leave Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'short_leave')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="float-end">{{ $short_leave_requests->appends(request()->query())->links('pagination::bootstrap-4') }}</div>

                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
                    <!-- Short Leave Tab -->
                    <div class="tab-pane {{$type == 'shortLeaveRequest' ? 'active' : ''}}" id="account-vertical-shortLeaveRequest" role="tabpanel" aria-labelledby="account-pill-shortLeaveRequest">
                        <div class="row" id="shortLeave_tab">
                            @if (count($short_leave_requests) > 0 && $type=='shortLeaveRequest')
                            @foreach ($short_leave_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Short Leave Request</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">From:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->time_from}}</span>
                                                                        <span class="card-text user-info-title fw-bolder ms-sm-1 mb-0">To:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->time_to}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->remarks}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-md-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Short Leave Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="actionBtn btn btn-success print_btn" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'short_leave')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Short Leave Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="actionBtn btn btn-warning print_btn" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_short_leave_2">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Short Leave Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="actionBtn btn btn-danger print_btn" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'short_leave')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'short_leave')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_short_leave_2" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Short Leave Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'short_leave')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="float-end">{{ $short_leave_requests->appends(request()->query())->links('pagination::bootstrap-4') }}</div>

                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
                    <!-- Salary Component Request -->
                    <div class="tab-pane {{$type == 'addSalarychange' ? 'active' : ''}}" id="account-vertical-addSalarychange" role="tabpanel" aria-labelledby="account-pill-addSalarychange">
                        <div class="row" id="shortLeave_tab">
                            @if (count($salaryAllownaces) > 0 && $type=='addSalarychange')
                            @foreach ($salaryAllownaces as $request)
                            <div id="printrequest{{ $request->id }}">
                                <div class="col-12">
                                    <div class="card user-card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                    <div class="user-avatar-section">
                                                        <h3 class="mb-1">Salary Component Request</h3>
                                                        <div class="d-flex justify-content-start">
                                                            {{-- @if (file_exists(public_path('images/employees/' . $request->image)))
                                                            <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                            @else --}}
                                                            <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                            {{-- @endif --}}
                                                            <div class="d-flex flex-column ms-1">
                                                                <div class="user-info">
                                                                    <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                </div>
                                                                <div class="user-info mb-0">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->department}}</span>
                                                                </div>
                                                                {{-- <div class="user-info mb-0">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">From:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->time_from}}</span>
                                                                    <span class="card-text user-info-title fw-bolder ms-sm-1 mb-0">To:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->time_to}}</span>
                                                                </div> --}}
                                                                {{-- <div class="user-info mb-0">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->remarks}}</span>
                                                                </div> --}}
                                                                <div class="user-info mb-0 d-flex">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                    <span class="card-text ms-md-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                    {{-- <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                    <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span> --}}
                                                                </div>
                                                                <div class="user-info mb-0">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                    <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                </div>
                                                                @if ($request->status != 'Pending')
                                                                <div class="user-info mb-0">
                                                                    <div>
                                                                        <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                        {{-- <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                        <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span> --}}
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                <div class="user-info mb-0">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                    <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex align-items-center mt-1">
                                                        @if ($request->status == 'Pending')
                                                        @can('Short Leave Request Approve Button')
                                                        <div class="d-flex align-items-center">
                                                            @if ($request->status != 'Approved')
                                                            <div class="ms-0 me-1">
                                                                <button class="actionBtn btn btn-success print_btn" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'salaryComponentRequest')">Allow</button>
                                                            </div>
                                                            @endif
                                                        </div>
                                                        @endcan
                                                        @can('Short Leave Request Reject Button')
                                                        <div class="d-flex align-items-center">
                                                            <div class="ms-0 me-1">
                                                                <button class="actionBtn btn btn-warning print_btn" id="Rejected" onclick="reject('<?php echo $request->id; ?>' , 'salaryComponentRequest')">Reject</button>
                                                            </div>
                                                        </div>
                                                        @endcan
                                                        @can('Short Leave Request Cancel Button')
                                                        <div class="d-flex align-items-center">
                                                            <div class="ms-0 me-1">
                                                                <button class="actionBtn btn btn-danger print_btn" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'salaryComponentRequest')">Cancel</button>
                                                            </div>
                                                        </div>
                                                        @endcan
                                                        @endif
                                                        <div class="d-flex align-items-center">
                                                            <div class="ms-0 me-1">
                                                                <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'salaryComponentRequest')">Print</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="float-end">{{ $short_leave_requests->appends(request()->query())->links('pagination::bootstrap-4') }}</div>

                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
                    <!-- Overtime Management Tab -->
                    <div class="tab-pane {{$type == 'overtimeRequest' ? 'active' : ''}}" id="account-vertical-overtimeManagement" role="tabpanel" aria-labelledby="account-pill-overtimeManagement">
                        <div class="row" id="overtimeManagement_tab">
                            @if (count($overtime_requests) > 0 && $type=='overtimeRequest')
                            @foreach ($overtime_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Overtime Requests</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Overtime Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->overtime_date}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Overtime Hours:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->overtime}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Overtime Management Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'overtime_request')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Overtime Management Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_overtime">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Overtime Management Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'overtime_request')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'overtime_request')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_overtime" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Overtime Request Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'overtime_request')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="float-end">{{ $overtime_requests->appends(request()->query())->links('pagination::bootstrap-4') }}</div>

                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <!-- Shift Change Tab -->
                    <div class="tab-pane {{$type == 'shiftChange' ? 'active' : ''}}" id="account-vertical-shiftChange" role="tabpanel" aria-labelledby="account-pill-shiftChange">
                        <div class="row" id="shiftChange_tab">
                            @if (count($shift_change_requests) > 0 && $type=='shiftChange')
                            @foreach ($shift_change_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Shift Change Requests</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Shift Type:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->shift_desc}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Shift Change Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'shift_change')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Shift Change Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_shift">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Shift Change Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'shift_change')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'shift_change')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_shift" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Shift Change Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'shift_change')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="float-end">{{ $shift_change_requests->appends(request()->query())->links('pagination::bootstrap-4') }}</div>
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <!-- Loan Tab -->
                    <div class="tab-pane {{$type == 'loan' ? 'active' : ''}}" id="account-vertical-loan" role="tabpanel" aria-labelledby="account-pill-loan">
                        <div class="row" id="loanRequest_tab">
                            @if (count($loan_requests) > 0 && $type=='loan')
                            @foreach ($loan_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Loan Requests</h3>
                                                            @if ($request->status == 'Approved')
                                                            <div class="text-end mt-1">
                                                                <span><a class="btn btn-sm btn-success print_btn" target="_blank" href="loan_request/<?php echo $request->id; ?>">View Loan</a>
                                                                    @if (!empty($request->payment_id))
                                                                    <a href="javascript:;" class="btn btn-sm btn-success print_btn" data-bs-toggle="tooltip" onclick="payment_details({{ $request->payment_id }})">Loan Payment Details</a>
                                                                    @endif
                                                            </div>
                                                            @endif
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Amount:</span>
                                                                        <span class="card-text ms-1 mb-0">{{$request->amount}}</span>
                                                                        <span class="card-text user-info-title fw-bold mb-0 ms-1">Time Duration:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->time_duration}} Months</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Remarks:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->remarks)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Status:</span>
                                                                        <span class="card-text ms-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bold mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bold mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</span>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Loan/Advance Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'loan_request')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Loan/Advance Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_loan">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Loan/Advance Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'loan_request')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'loan_request')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_loan" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Loan Request Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'loan_request')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="float-end">{{ $loan_requests->appends(request()->query())->links('pagination::bootstrap-4') }}</div>
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <!-- Miscellaneous Tab -->
                    <div class="tab-pane {{$type == 'miscellaneous' ? 'active' : ''}}" id="account-vertical-miscellaneous" role="tabpanel" aria-labelledby="account-pill-miscellaneous">
                        <div class="row" id="miscellaneous_tab">
                            @if (count($miscellaneous_requests) > 0 && $type=='miscellaneous')
                            @foreach ($miscellaneous_requests as $request)
                                <div id="printrequest{{ $request->id }}">
                                    <div class="col-12">
                                        <div class="card user-card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                        <div class="user-avatar-section">
                                                            <h3 class="mb-1">Miscellaneous Requests</h3>
                                                            <div class="d-flex justify-content-start">
                                                                @if (file_exists(public_path('images/employees/' . $request->image)))
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/' . $request->image)}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @else
                                                                <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                                @endif
                                                                <div class="d-flex flex-column ms-1">
                                                                    <div class="user-info">
                                                                        <h4 class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Description:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->description}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Feedback:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{HandleEmpty($request->feedback)}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0 d-flex">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                        <span class="card-text ms-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Print Status:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 print_status{{$request->id}}">{{$request->print_status}}</span>
                                                                    </div>
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d' , strtotime($request->created_at))}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                        <span class="card-text ms-sm-1 mb-0">{{$request->creator}}</p>
                                                                    </div>
                                                                    @if ($request->status != 'Pending')
                                                                    <div class="user-info mb-0">
                                                                        <div>
                                                                            <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                            <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                            <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                            <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    <div class="user-info mb-0">
                                                                        <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                        <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center mt-1">
                                                            @if ($request->status == 'Pending')
                                                            @can('Miscellaneous Request Approve Button')
                                                            <div class="d-flex align-items-center">
                                                                @if ($request->status != 'Approved')
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-success" id="Approved" onclick="allow('<?php echo $request->id; ?>' , 'miscellaneous_request')">Allow</button>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @endcan
                                                            @can('Miscellaneous Request Reject Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-warning" id="Rejected" data-bs-toggle="modal" data-bs-target="#reject_misclaneous">Reject</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @can('Miscellaneous Request Cancel Button')
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button class="print_btn actionBtn btn btn-danger" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'miscellaneous_request')">Cancel</button>
                                                                </div>
                                                            </div>
                                                            @endcan
                                                            @endif
                                                            <div class="d-flex align-items-center">
                                                                <div class="ms-0 me-1">
                                                                    <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'miscellaneous_request')">Print</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade text-start" id="reject_misclaneous" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel17">Miscellaneous Request Rejection Remark</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form class="form" id="reject">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="restore_date">Rejection Remarks</label>
                                                                <input type="text" id="rejection_remarks" class="form-control" name="reject_remarks" placeholder="Rejection Remarks" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                                    <button type="button" class="btn btn-primary" id="save" onclick="reject('<?php echo $request->id; ?>' , 'miscellaneous_request')">@lang('app.Save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="float-end">{{ $miscellaneous_requests->appends(request()->query())->links('pagination::bootstrap-4') }}</div>
                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!--Fine Concession Tab -->
                    <div class="tab-pane {{$type == 'fineconcession' ? 'active' : ''}}" id="account-vertical-fineconcession" role="tabpanel" aria-labelledby="account-pill-fineconcession">
                        <div class="row" id="shortLeave_tab">
                            @if (count($fines) > 0 && $type=='fineconcession')
                            @foreach ($fines as $request)
                            <div id="printrequest{{$request->id}}">
                                <div class="col-12">
                                    <div class="card user-card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                                    <div class="user-avatar-section">
                                                        <h3 class="mb-1">Fine Concession Request</h3>
                                                        <div class="text-end mt-1">
                                                            @can('View Approved Fine')
                                                            <button class="btn btn-sm btn-success print_btn" onclick="viewFine({{$request->employee_id}},{{$request->fine_payroll_id}})">View Fine</button>
                                                            @endcan
                                                        </div>
                                                        <div class="d-flex justify-content-start">
                                                            <img class="img-fluid rounded" src="{{asset('images/employees/default_employee.png')}}" style="height: 110px; width: 110px;" alt="User avatar" />
                                                            <div class="d-flex flex-column ms-1">
                                                                <div class="user-info" style="display: flex;">
                                                                    <h4 style="padding-right: 570px;" class="mb-0">{{$request->first_name}} {{$request->middle_name}} {{$request->last_name}}</h4>
                                                                    <!-- </div> -->
                                                                    <!-- <div class="user-info"> -->

                                                                </div>
                                                                <div class="user-info mb-0 mt-1">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Department:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->title}}</span>
                                                                </div>
                                                                <div class="user-info mb-0 mt-1">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Amount:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->amount}}</span>
                                                                </div>
                                                                <div class="user-info mb-0 mt-1">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Remarks:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->remarks}}</span>
                                                                </div>
                                                                <div class="user-info mb-0 mt-1">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Fine Month:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->month}}</span>
                                                                </div>
                                                                <div class="user-info mb-0 mt-1">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Fine Reversal Month:</span>
                                                                    @if(!empty($request->fine_reverse_month))
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->fine_reverse_month}}</span>
                                                                    @else
                                                                    <span class="card-text ms-md-1 mb-0 badge bg-light-info">Pending</span>
                                                                    @endif
                                                                </div>
                                                                <div class="user-info mb-0 d-flex mt-1">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Status:</span>
                                                                    <span class="card-text ms-md-1 mb-0 {{ getbadge($request->status) }}">{{$request->status}}</span>
                                                                </div>

                                                                <div class="user-info mb-0 mt-1">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Requested Date:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{date('Y-m-d H:i:s' , strtotime($request->created_at))}}</span>
                                                                    <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">Submitted By:</span>
                                                                    <span class="card-text ms-sm-1 mb-0">{{$request->username}}</span>
                                                                </div>
                                                                @if ($request->status != 'Pending')
                                                                <div class="user-info mb-0 mt-1">
                                                                    <div>
                                                                        <span class="card-text user-info-title fw-bolder mb-0">{{$request->status}} Date:</span>
                                                                        <span class="card-text mb-0">{{$request->status_changed_at != null ? date('Y-m-d H:i:s' , strtotime($request->status_changed_at)) : '-'}}</span>
                                                                        <span class="card-text user-info-title fw-bolder mb-0 ms-md-1">{{$request->status}} By:</span>
                                                                        <span class="card-text  mb-0">{{$request->status_changed_by != null ? getUser($request->status_changed_by) : '-'}}</span>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                <div class="user-info mb-0 mt-2">
                                                                    <span class="card-text user-info-title fw-bolder mb-0">Signature:</span>
                                                                    <span class="card-text ms-sm-1 mb-0 px-5" style="border-bottom: 1px solid black"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex align-items-center mt-1">
                                                        @if ($request->status == 'Pending')
                                                        @can('Fine Concession Request Approve Button')
                                                        <div class="d-flex align-items-center">
                                                            @if ($request->status != 'Approved')
                                                            <div class="ms-0 me-1">
                                                                <button class="actionBtn btn btn-success print_btn" id="Approved" onclick="fine_allow('{{$request->id}}','{{$request->amount}}','{{$request->remarks}}','{{$request->attachment_file}}')" data-bs-toggle="modal" data-bs-target="#add_modal">Allow</button>
                                                            </div>
                                                            @endif
                                                        </div>
                                                        @endcan
                                                        @can('Fine Concession Request Reject Button')
                                                        <div class="d-flex align-items-center">
                                                            <div class="ms-0 me-1">
                                                                <button class="actionBtn btn btn-warning print_btn" id="Rejected" onclick="reject('<?php echo $request->id; ?>' , 'fineconcession')">Reject</button>
                                                            </div>
                                                        </div>
                                                        @endcan
                                                        @can('Fine Concession Request Cancel Button')
                                                        <div class="d-flex align-items-center">
                                                            <div class="ms-0 me-1">
                                                                <button class="actionBtn btn btn-danger print_btn" id="Cancelled" onclick="cancel('<?php echo $request->id; ?>' , 'fineconcession')">Cancel</button>
                                                            </div>
                                                        </div>
                                                        @endcan
                                                        @endif
                                                        <div class="d-flex align-items-center">
                                                            <div class="ms-0 me-1">
                                                                <button type="button" class="actionBtn btn btn-primary print_btn" onclick="print_request({{$request->id}},'fineconcession')">Print</button>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center">
                                                            @if ($request->status == 'Approved')
                                                            <div class="ms-0 me-1">
                                                                @can('View Approved Fine Request')
                                                                <button class="actionBtn btn btn-warning print_btn" id="Approved" onclick="fine_allow_show('{{$request->id}}','{{$request->amount}}','{{$request->remarks}}','{{$request->attachment_file}}')" >Fine Request</button>
                                                                @endcan
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="float-end">{{ $fines->appends(request()->query())->links('pagination::bootstrap-4') }}</div>

                            @else
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p class="textcenter">No Records Found</p>
                                    {{-- <img src="{{asset('app-assets/images/elements/no_data.png')}}" style="max-width:80%;" class="img-fluid" alt="Not Found"> --}}
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade text-start" id="leave_history_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title modal_heading" id="myModalLabel17">Leave History</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h4 class="text-center font-weight-bold" id="emp_name"></h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-info p-1" id="leave_history_balance"></div>
                            <div id="leave_table" class="table-responsive">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- Payment Details Modal --}}
<div class="modal fade text-start" id="payment_details_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Payment Details</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <thead>
                                <th>Key</th>
                                <th>Detail</th>
                            </thead>
                            <tbody id="payment_details_table">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Fine Conssion Modal  -->
<div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Add Fine Reversal Request</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="form" id="add_form" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="employee_id" id="employee_id">
                        <input type="hidden" name="month" id="fine_month">
                        <input type="hidden" name="fine_reversal_id" id="fine_reversal_id">
                        <div class="col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fine_amount">Amount</label>
                                <input type="number" name="amount" id="fine_amount" class="form-control" step=".01" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-1">
                                <label for="remarks" class="form-label">Remarks</label>
                                <textarea name="remarks" placeholder="Remarks ..." class="form-control" id="remarks" rows="3" required></textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-1" id="attachmet_div">

                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-1">
                                <input type="hidden" name="payrolldata_id" id="payrolldata_id" class="form-control">
                            </div>
                        </div>

                        {{-- @can('Leave Request Approve Now')    
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="status" id="status" value="Approved"/>
                                                <label class="custom-control-label" for="status">Request Approve Now</label>
                                            </div> 
                                        </div>
                                    </div>
                                @endcan --}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="form_save btn btn-primary" id="leave_save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Fine Show Modal -->
<div class="modal fade text-start" id="show_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Add Fine Reversal Request</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="form" id="add_form" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="employee_id" id="show_employee_id">
                        <input type="hidden" name="month" id="show_fine_month">
                        <input type="hidden" name="fine_reversal_id" id="show_fine_reversal_id">
                        <div class="col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fine_amount">Amount</label>
                                <input type="number" name="amount" id="show_fine_amount" class="form-control" step=".01" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-1">
                                <label for="remarks" class="form-label">Remarks</label>
                                <textarea name="remarks" placeholder="Remarks ..." class="form-control" id="show_remarks" rows="3" required></textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-1" id="show_attachmet_div">

                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-1">
                                <input type="hidden" name="payrolldata_id" id="show_payrolldata_id" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Fine View Modal-->
<div class="modal fade text-start" id="view_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">View Fine</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Name</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody id="append_row"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $("#departmentFilter").change(function() {
        var optVal = $(this).val();
        $.ajax({
            type: "GET",
            url: "{{ url('get_dept_employees') }}",
            data: {
                department_id: optVal,
            },
            success: function(response) {
                $('#employeeFilter').empty();
                $('#employeeFilter').html('<option value="">Select Employee</option>');
                $.each(response, function(index, value) {
                    $('#employeeFilter').append(
                        $('<option></option>').val(value.id).html(
                            value.employee_id + ' - ' + value.employee_code + ' - ' + value.first_name + ' ' + value.middle_name + ' ' + value.last_name + ' - ' + value.designation + ' - ' + value.department)
                    );
                });
            },
            error: function() {
                alert('Error occured');
            }
        });
    });
    // $(document).ready(function(){
    //     $('a[data-bs-toggle="pill"]').on('click', function(e) {
    //         console.log($(e.target).attr('href'));
    //         localStorage.setItem('activeTab', $(e.target).attr('href'));
    //     });
    //     var activeTab = localStorage.getItem('activeTab');
    //     if(activeTab){
    //         $('#myTab a[href="' + activeTab + '"]').tab('show');
    //     }
    // });

    var record_id;
    var record_type;

    // Allow Function
    function allow(id, type) {
        record_id = id;
        record_type = type;
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: "Are you sure you want to Approve Employee's request!",
            type: 'green',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Approve',
                    btnClass: 'btn-success',
                    action: function() {
                        $.ajax({
                            url: "{{url('statusUpdate')}}" + "/" + record_id,
                            type: 'GET',
                            data: {
                                data_type: record_type,
                                type: 'Approved',
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else {
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Status has been Updated Successfully!'
                                    })
                                    setTimeout(function() {
                                        window.location.reload();
                                    }, 2000)
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }

    // Cancel Function
    function cancel(id, type) {
        record_id = id;
        record_type = type;
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: "Are you sure you want to Cancel Employee's request!",
            type: 'red',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Cancel',
                    btnClass: 'btn-danger',
                    action: function() {
                        $.ajax({
                            url: "{{url('statusUpdate')}}" + "/" + record_id,
                            type: 'GET',
                            data: {
                                data_type: record_type,
                                type: 'Cancelled',
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else {
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Status has been Updated Successfully!'
                                    })
                                    setTimeout(function() {
                                        window.location.reload();
                                    }, 2000)
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }

    // Reject Function
    function reject(id, type) {
        record_id = id;
        record_type = type;
        var rejectionRemarks1 = $('#rejection_remarks').val();
        var rejectionRemarks2 = $('#rejection_remark').val();
        var rejection_remarks = rejectionRemarks1 ? rejectionRemarks1 : rejectionRemarks2;
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: "Are you sure you want to Reject Employee's request!",
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Reject',
                    btnClass: 'btn-orange',
                    action: function() {
                        $.ajax({
                            url: "{{url('statusUpdate')}}" + "/" + record_id,
                            type: 'GET',
                            data: {
                                data_type: record_type,
                                rejection_remarks: rejection_remarks,
                                type: 'Rejected',
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else {
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Status has been Updated Successfully!'
                                    })
                                    setTimeout(function() {
                                        window.location.reload();
                                    }, 2000)
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }

    // Leave History
    function leave_history(id) {
        rowid = id;
        $.ajax({
            url: "{{url('employee_leave_requests')}}" + "/" + id,
            type: "GET",
            success: function(response) {
                $('#leave_history_balance').html('<strong> Selected Employee has <span style="color:black">' + response.leave_history.balance + " " + response.leave_name + '</span> Leaves in Balance.</strong>');
                $('#leave_history_balance').show(100);
                $('#leave_table').empty();
                var html = "";
                html = `
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>    
                                    <th>Employee</th>    
                                    <th>Leave Type</th>    
                                    <th>Leave Start Date</th>  
                                    <th>Leave End Date</th>    
                                    <th>Leave Status</th>    
                                    <th>Created By</th>    
                                </tr>
                            </thead>    
                        <tbody>
                    `;
                if (response.leave_history.leaves.length > 0) {
                    $.each(response.leave_history.leaves, function(index, val) {
                        html += `
                                    <tr>
                                        <td>${index + 1}</td>    
                                        <td>${val.first_name} ${val.last_name}</td>    
                                        <td>${val.leave_name}</td>    
                                        <td>${val.date_start}</td>    
                                        <td>${val.date_end}</td>    
                                        <td>${val.status}</td>    
                                        <td>${val.creator}</td>    
                                    </tr>
                                `;
                    });
                } else {
                    html += `
                                    <tr> 
                                        <td colspan="7" class="text-center fw-bold">No Record Found</td>
                                    </tr>
                                `;
                }
                html += `</tbody>
                                    </table>
                            `;

                $('#leave_table').html(html);
                $("#leave_history_modal").modal('show');
            }
        });
    }

    function payment_details(id) {
        $.ajax({
            url: "{{url('loan_request')}}" + "/" + id + "/edit",
            type: "GET",
            data: {
                type: 'payment_details'
            },
            success: function(response) {
                var html = '';
                if (response) {
                    html += `
                            <tr>
                                <td>Employee</td>
                                <td>${response.first_name} ${response.last_name}</td>
                            </tr>
                            <tr>
                                <td>Loan Amount</td>
                                <td>${response.loan_amount}</td>
                            </tr>
                            <tr>
                                <td>Payment Method</td>
                                <td>${response.payment_method}</td>
                            </tr>
                            <tr>
                                <td>Cash Account Name</td>
                                <td>${!response.account_name ? '' : response.account_name}</td>
                            </tr>
                            <tr>
                                <td>Bank Name</td>
                                <td>${!response.bank_name ? '' : response.bank_name}</td>
                            </tr>
                            <tr>
                                <td>Checque No</td>
                                <td>${!response.checque_no ? '' : response.checque_no}</td>
                            </tr>
                            <tr>
                                <td>Date Time</td>
                                <td>${response.created_at}</td>
                            </tr>
                            <tr>
                                <td>Paid By</td>
                                <td>${response.username}</td>
                            </tr>
                            <tr>
                                <td>Remarks</td>
                                <td>${response.remarks}</td>
                            </tr>
                            <tr>
                                <td>Attachment</td>
                                <td><a href="images/loan_details/${response.attachment}" target="blank" class="" download>${response.attachment}</a></td>
                            </tr>
                        `
                }
                $('#payment_details_table').html('');
                $('#payment_details_table').append(html);
                $("#payment_details_modal").modal("show");
            }
        });
    }

    // $(document).ready(function(){
    function print_request(id, type) {
        $.ajax({
            url: "{{ url('approvals') }}" + "/" + id,
            type: "PUT",
            data: {
                type: type,
                _token: "{{ csrf_token() }}",
            },
            success: function(response) {
                $('.print_btn').addClass('d-none');
                $('.print_status' + id).text(response.data.print_status)
                var contents = $("#printrequest" + id).html();
                var frame1 = $('<iframe />');
                frame1[0].name = "frame1";
                frame1.css({
                    "position": "absolute",
                    "top": "-1000000px",
                    "margin": "none"
                });
                $("body").append(frame1);
                var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
                frameDoc.document.open();
                //Create a new HTML document.
                // frameDoc.document.write('<html><head><title>DIV Contents</title>');
                frameDoc.document.write('</head><body>');
                //Append the external CSS file.
                frameDoc.document.write('<link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/bootstrap.css")}}">');
                frameDoc.document.write('<link rel="stylesheet" type="text/css" href="{{asset("assets/css/style.css")}}">');
                frameDoc.document.write('<link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/bootstrap-extended.css")}}">');
                frameDoc.document.write('<link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/colors.css")}}">');
                //Append the DIV contents.
                frameDoc.document.write(contents);
                frameDoc.document.write('</body></html>');
                frameDoc.document.close();
                setTimeout(function() {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    frame1.remove();
                    $(".print_btn").removeClass('d-none');
                    // $(".print_btn").addClass('d-block');
                }, 500);
            }
        });
    }
    // });
    function fine_allow(id, amount, remarks, attachment_file) {
        $('#fine_amount').val(amount);
        $('#fine_reversal_id').val(id);
        $('#remarks').val(remarks);
        document.getElementById('fine_amount').max = amount;
        if(attachment_file){
            $('#attachmet_div').html(`<img src="${attachment_file}" width="400px" height="400px" class="rounded" alt="" srcset="">`).append();

        }
       
        $("#add_modal").modal('show');
    }
    function fine_allow_show(id, amount, remarks, attachment_file) {
        $('#show_fine_amount').val(amount);
        $('#show_fine_reversal_id').val(id);
        $('#show_remarks').val(remarks);
        document.getElementById('fine_amount').max = amount;
        if(attachment_file){
            $('#show_attachmet_div').html(`<img src="${attachment_file}" width="400px" height="400px" class="rounded" alt="" srcset="">`).append();

        }  
        $("#show_add_modal").modal('show');
    }
    $("#add_form").submit(function(e) {
        var fine_reversal_id = $('#fine_reversal_id').val();
        ButtonStatus('.form_save', true);
        blockUI();
        e.preventDefault();
        $.ajax({
            url: "{{url('fineReversal')}}" + '/' + fine_reversal_id,
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                window.location.reload()
                ButtonStatus('.form_save', false);
                $.unblockUI();
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })

                } else {
                    $('#add_form')[0].reset();
                    $("#add_modal").modal("hide");
                    datatable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Fine Reversal Request has been Added Successfully!'

                    })
                }
            }
        });
    });

    function viewFine(employee_id, payroll_id) {
        $.ajax({
            url: "{{url('fineReversal')}}" + "/" + employee_id,
            type: "GET",
            data: {
                payroll_id: payroll_id
            },
            success: function(response) {
                $('#append_row').empty();
                var html = '';
                var total = 0;
                $.each(response, function(key, value) {
                    total = Number(total) + Number(value.amount);
                    html += '<tr>' +
                        '<td>' + (key + 1) + '</td>' +
                        '<td>' + value.name + '</td>' +
                        '<td>' + value.amount + '</td>' +
                        '</tr>';
                });
                html += `<tr>
                            <td colspan='2'><strong>Total</strong></td>
                            <td><strong>${parseFloat(total).toFixed(2)}</strong></td>
                        </tr>`;
                $('#append_row').append(html);
                $("#view_modal").modal('show');
            }
        })
    }
</script>
@endsection