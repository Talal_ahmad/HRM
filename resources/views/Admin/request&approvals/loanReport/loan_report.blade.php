@extends('Admin.layouts.master')
@section('title', 'Loan Report')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Loan_Report')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.Request_&_Approvals')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Loan_Report')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form" action="{{ url('loanReport') }}" method="GET">
                                    <div class="row">
                                        @if (env('COMPANY') == 'JSML')        
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : '' }}>All Departments</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="title">@lang('app.Section')</label>
                                                    <select name="section[]" id="section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                        
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                    </div>
                                                </div>
                                            </div>    
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="employeeFilter">@lang('app.Filter_By_Employee'):</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
                                                <option value=""></option>
                                                @if (!empty(request('departmentFilter')))
                                                    @foreach (employees(request('departmentFilter')) as $item)
                                                    <option value="{{$item->id}}" {{$item->id == request('employeeFilter') ? 'selected' : ''}}>{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                    @endforeach
                                                @endif 
                                            </select>
                                        </div>
                                        @else
                                        <div class="col-md-4 col-12">
                                            <label class="form-label">@lang('app.Filter_By_Employee'):</label>
                                            <select name="employeeFilter" id="employeeFilter"
                                                class="select2 form-select filter" data-placeholder="@lang('app.Select_Employee')">
                                                <option value="">Select Employee</option>
                                                @foreach (employees('','loanReport') as $employee)
                                                    <option value="{{ $employee->id }}"
                                                        {{ $employee->id == request('employeeFilter') ? 'selected' : '' }}>
                                                        {{ $employee->employee_id }} - {{ $employee->first_name }}
                                                        {{ $employee->middle_name }}
                                                        {{ $employee->last_name }} - {{ $employee->desigination }} - {{ $employee->department }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @endif
                                        <div class="col-md-4 col-12">
                                            <label class="form-label">@lang('app.Filter_By_Type'):</label>
                                            <select name="typeFilter" id="typeFilter"
                                                class="select2 form-select filter" data-placeholder="Select Type" required>
                                                <option value="all"
                                                        {{ request('typeFilter') == 'all' ? 'selected' : '' }}>All
                                                </option>
                                                @foreach ($loan_types as $type)
                                                    <option value="{{ $type->id }}"
                                                        {{ $type->id == request('typeFilter') ? 'selected' : '' }}>
                                                        {{$type->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="statusFilter">@lang('app.Filter_By_Status')</label>
                                                <select name="statusFilter" id="statusFilter" class="select2 form-select"
                                                    data-placeholder="@lang('app.Select_Status')">
                                                    <option value="all"
                                                        {{ request('statusFilter') == 'all' ? 'selected' : '' }}>All
                                                    </option>
                                                    <option value="completed"
                                                        {{ request('statusFilter') == 'completed' ? 'selected' : '' }}>
                                                        Completed</option>
                                                    <option value="progress"
                                                        {{ request('statusFilter') == 'progress' ? 'selected' : '' }}>In
                                                        Progress</option>
                                                    <option value="settled"
                                                        {{ request('statusFilter') == 'settled' ? 'selected' : '' }}>
                                                        Settled</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="fromMonth">@lang('app.Loan_From')</label>
                                                <input type="month" name="fromMonth" id="fromMonth" class="form-control"
                                                    value="{{ request('fromMonth', '2022-10') }}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="toMonth">@lang('app.Loan_To')</label>
                                                <input type="month" name="toMonth" id="toMonth" class="form-control"
                                                    value="{{ request('toMonth') }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{ url('loanReport') }}" type="button"
                                                class="btn btn-primary mt-1">@lang('app.Reset')</a>
                                            <button type="submit" class="btn btn-info mt-1">@lang('app.Apply')</button>
                                            @if (isset($loans) && count($loans) > 0)
                                                <a href="{{ url()->full() }}&pdf=true" target="_blank"
                                                class="btn btn-danger mt-1">@lang('app.Download_Pdf')</a>
                                                <a href="{{ url()->full() }}&excel=true" target="_blank"
                                                class="btn btn-success mt-1">@lang('app.Download_Excel')</a>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card pb-2 table-responsive">
                            <table class="table table-bordered" id="loan_request">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        {{-- <th>Employee Status</th> --}}
                                        <th>Department</th>
                                        <th>Start Date</th>
                                        <th>Loan Amount</th>
                                        @if(env('COMPANY') == 'JSML')
                                            <th>Deduction</th>
                                            <th>Balance</th>
                                            @else
                                            <th>Remaining Amount</th>
                                        @endif
                                        @if (isset($months) && count($months) > 0)
                                            @foreach ($months as $month)
                                            @php
                                                $totals[$month] = 0;
                                                $totals_inst[$month] = 0;
                                            @endphp
                                                <th>{{ $month }}</th>
                                            @endforeach
                                        @endif
                                        <th>Status</th>
                                        <th>Installments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($loans) && count($loans) > 0)
                                    @php
                                        $sr_no = 1;
                                        $g_total_loan_amount = 0;
                                        $g_total_remaining_amount = 0;
                                        // $g_total_monthly_amount = 0;
                                        $g_deduction_amount = 0;
                                        $g_balance_sum_amount = 0;
                                    @endphp
                                        @foreach ($loans as $key => $loan)
                                        @php
                                            $g_total_loan_amount += $loan->amount;
                                            $g_total_remaining_amount += $loan->remaining_amount;
                                            $balance_sum_amount = 0;
                                        @endphp
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $loan->employee_id }}</td>
                                                <td>{{ $loan->employee_code }}</td>
                                                <td>{{ $loan->first_name . ' ' . $loan->middle_name . ' ' . $loan->last_name }}
                                                </td>
                                                {{-- <td>{{ $loan->employement_status }}</td> --}}
                                                <td>{{ $loan->title }}</td>
                                                <td>{{ date('Y-m-d', strtotime($loan->start_date)) }}</td>
                                                <td>{{ number_format($loan->amount) }}</td>
                                                @if(env('COMPANY') == 'JSML')
                                                @php
                                                    $deducted_amount = getLoanDeductedAmount($loan->id);
                                                    $g_deduction_amount += $deducted_amount;
                                                @endphp
                                                <td>{{ number_format($deducted_amount) }}</td>
                                                @else
                                                <td>{{ number_format($loan->remaining_amount) }}</td>
                                                @endif
                                                @foreach ($months as $month)
                                                    @php
                                                        $installment = getMonthInstallment($loan->id, $month);
                                                        $installment_total = getMonthInstallment_total($loan->id, $month);
                                                        if (!empty($installment_total)){
                                                            $totals_inst[$month] += $installment_total->amount; 
                                                        }
                                                    @endphp
                                                    @if (!empty($installment))
                                                        @php
                                                            $balance_sum_amount += $installment->amount; 
                                                        @endphp
                                                    @endif
                                                @endforeach
                                                @if(env('COMPANY') == 'JSML')
                                                    {{-- @php
                                                        $g_balance_sum_amount += $balance_sum_amount;
                                                    @endphp --}}
                                                    <td>{{ number_format($loan->amount - $deducted_amount) }}</td>
                                                @endif
                                                @foreach ($months as $month)
                                                    @php
                                                        $installment = getMonthInstallment($loan->id, $month);
                                                    @endphp
                                                    @if (!empty($installment))
                                                        @php
                                                            if ($installment->status == 'deducted') {
                                                                $icon = '<i class="fas fa-check-circle text-success"></i>';
                                                                $class= 'deducted';
                                                            } elseif ($installment->status == 'cancelled') {
                                                                $icon = '<i class="fas fa-times-circle text-danger"></i>';
                                                                $class= 'cancelled';
                                                            } else {
                                                                $icon = '';
                                                                $class= '';
                                                            }
                                                            $totals[$month] += $installment->amount; 
                                                        @endphp
                                                        <td style="white-space: nowrap" class="{{$class}}">{!! $icon !!}{{ $installment->amount }}</td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                @endforeach
                                                @if ($loan->final_satelment == 1)
                                                    <td><span class="badge rounded-pill bg-primary">Settled</span></td>
                                                @elseif($loan->remaining_amount > 0)
                                                    <td><span class="badge rounded-pill bg-danger">In Progress</span></td>
                                                @else
                                                    <td><span class="badge rounded-pill bg-success">Recovered</span></td>
                                                @endif
                                                <td>
                                                    <a href="{{ route('loan_request.show', $loan->id) }}"
                                                        class="btn btn-sm btn-primary">View</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @php
                                                // dd($totals);
                                            @endphp
                                    @endif
                                </tbody>
                                @if (isset($loans) && count($loans))
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td style="font-weight: bold;text-align:end">Grand Total</td>
                                            <td>{{number_format($g_total_loan_amount)}}</td>
                                            @if(env('COMPANY') == 'JSML')
                                                <td>{{number_format($g_deduction_amount)}}</td>
                                                <td>
                                                    {{number_format($g_total_loan_amount - $g_deduction_amount)}}
                                                </td>
                                                @else
                                                <td>{{number_format($g_total_remaining_amount)}}</td>
                                            @endif
                                            @if (env('COMPANY') != 'Ajmal Dawakhana')
                                                @if (env('COMPANY') == 'JSML')
                                                    @if (isset($loans) && count($loans) > 0)
                                                        @foreach ($months as $month)
                                                            <td class="">{{number_format($totals_inst[$month])}}</td>
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @if (isset($loans) && count($loans) > 0)
                                                        @foreach ($months as $month)
                                                        <td class="">{{number_format($totals[$month])}}</td>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endif
                                            <td><td></td></td>
                                        </tr>
                                    </tfoot>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            @if (env('COMPANY') == 'JSML') 
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'attendance_report',
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - ' +value.employee_code + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation + ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            @endif

            var table = $('#loan_request').DataTable({
                ordering: false,
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            },
                            footer: true
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            },
                            footer: true,
                            @if (env('COMPANY') == 'Ajmal Dawakhana')
                                action: function (e, dt, button, config) {
                                    var data = dt.rows().data();
                                    var rowIndex = dt.rows({ selected: true }).indexes();
                                    $.each(rowIndex, function(index, item) {
                                        // if ($(dt.row(item).node()).find('td').hasClass('cancelled')) {
                                            var cellIndex = $(dt.row(item).node()).find('td.cancelled').index();
                                            if(cellIndex != -1){
                                                data[item][cellIndex] = 0;
                                            }
                                        // }
                                    });
                                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(this, e, dt, button, config);
                                }
                            @endcan
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            },
                            footer: true,
                            @if (env('COMPANY') == 'Ajmal Dawakhana')
                                action: function (e, dt, button, config) {
                                    var data = dt.rows().data();
                                    var rowIndex = dt.rows({ selected: true }).indexes();
                                    $.each(rowIndex, function(index, item) {
                                        // if ($(dt.row(item).node()).find('td').hasClass('cancelled')) {
                                            var cellIndex = $(dt.row(item).node()).find('td.cancelled').index();
                                            if(cellIndex != -1){
                                                data[item][cellIndex] = 0;
                                            }
                                        // }
                                    });
                                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(this, e, dt, button, config);
                                }
                            @endcan
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            orientation: 'landscape',
                            pageSize: 'LEGAL',
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            },
                            footer: true
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            },
                            footer: true
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }, ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
        });
    </script>
@endsection
