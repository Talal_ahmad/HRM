<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Loan Report</title>
</head>

<body>
    <table>
        <thead>
            <tr>
                <th style="font-weight: bold;">Sr.No</th>
                <th style="font-weight: bold;">Employee ID</th>
                <th style="font-weight: bold;">Employee Code</th>
                <th style="font-weight: bold;">Employee</th>
                <th style="font-weight: bold;">Department</th>
                <th style="font-weight: bold;">Start Date</th>
                <th style="font-weight: bold;">Loan Amount</th>
                @if (env('COMPANY') == 'JSML')
                    <th style="font-weight: bold;">Deduction</th>
                    <th style="font-weight: bold;">Balance</th>
                @else
                    <th style="font-weight: bold;">Remaining Amount</th>
                @endif
                @if (isset($months) && count($months) > 0)
                    @foreach ($months as $month)
                        @php
                            $totals[$month] = 0;
                            $totals_inst[$month] = 0;
                        @endphp
                        <th style="font-weight: bold;">{{ $month }}</th>
                    @endforeach
                @endif
                <th style="font-weight: bold;">Status</th>
            </tr>
        </thead>

        <tbody>
            @if (isset($loans) && count($loans) > 0)
                @php
                    $sr_no = 1;
                    $g_total_loan_amount = 0;
                    $g_total_remaining_amount = 0;
                    $g_balance_sum_amount = 0;
                    $g_deduction_amount = 0;
                @endphp
                @foreach ($loans as $key => $loan)
                    @php
                        $g_total_loan_amount += $loan->amount;
                        $g_total_remaining_amount += $loan->remaining_amount;
                        $balance_sum_amount = 0;
                    @endphp
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $loan->employee_id }}</td>
                        <td>{{ $loan->employee_code }}</td>
                        <td>{{ $loan->first_name . ' ' . $loan->middle_name . ' ' . $loan->last_name }}
                        </td>
                        <td>{{ $loan->title }}</td>
                        <td>{{ date('Y-m-d', strtotime($loan->start_date)) }}</td>
                        <td>{{ number_format($loan->amount) }}</td>
                        @if (env('COMPANY') == 'JSML')
                            @php
                                $deducted_amount = getLoanDeductedAmount($loan->id);
                                $g_deduction_amount += $deducted_amount;
                            @endphp
                            <td>{{ number_format($deducted_amount) }}</td>
                        @else
                            <td>{{ number_format($loan->remaining_amount) }}</td>
                        @endif
                        @foreach ($months as $month)
                            @php
                                $installment = getMonthInstallment($loan->id, $month);
                                $installment_total = getMonthInstallment_total($loan->id, $month);
                                if (!empty($installment_total)) {
                                    $totals_inst[$month] += $installment_total->amount;
                                }
                            @endphp
                            @if (!empty($installment))
                                @php
                                    $balance_sum_amount += $installment->amount;
                                @endphp
                            @endif
                        @endforeach
                        @if (env('COMPANY') == 'JSML')
                            {{-- @php
                                $g_balance_sum_amount += $balance_sum_amount;
                            @endphp --}}
                            <td>{{ number_format($loan->amount - $deducted_amount) }}</td>
                        @endif
                        @foreach ($months as $month)
                            @php
                                $installment = getMonthInstallment($loan->id, $month);
                            @endphp
                            @if (!empty($installment))
                                @php
                                    if ($installment->status == 'deducted') {
                                        $bgColor = 'background-color: green';
                                    } elseif ($installment->status == 'cancelled') {
                                        $bgColor = 'background-color: red';
                                    } else {
                                        $bgColor = '';
                                    }
                                    $totals[$month] += $installment->amount;
                                @endphp
                                <td style="white-space: nowrap;{{ $bgColor }}">
                                    {{ $installment->amount }}</td>
                            @else
                                <td></td>
                            @endif
                        @endforeach
                        @if ($loan->final_satelment == 1)
                            <td><span class="badge rounded-pill bg-primary">Settled</span></td>
                        @elseif($loan->remaining_amount > 0)
                            <td><span class="badge rounded-pill bg-danger">In Progress</span></td>
                        @else
                            <td><span class="badge rounded-pill bg-success">Recovered</span></td>
                        @endif
                    </tr>
                @endforeach
            @endif
        </tbody>
        @if (isset($loans) && count($loans))
            <tfoot>
                <tr>
                    <td colspan="6" style="font-weight: bold">Grand Total</td>
                    <td>{{ number_format($g_total_loan_amount) }}</td>
                    @if (env('COMPANY') == 'JSML')
                        <td>{{ number_format($g_deduction_amount) }}</td>
                        <td>
                            {{ number_format($g_total_loan_amount - $g_deduction_amount) }}
                        </td>
                    @else
                        <td>{{ number_format($g_total_remaining_amount) }}</td>
                    @endif
                    @if (env('COMPANY') != 'Ajmal Dawakhana')
                        @if (env('COMPANY') == 'JSML')
                            @if (isset($loans) && count($loans) > 0)
                                @foreach ($months as $month)
                                    <td class="">{{ number_format($totals_inst[$month]) }}</td>
                                @endforeach
                            @endif
                        @else
                            @if (isset($loans) && count($loans) > 0)
                                @foreach ($months as $month)
                                    <td class="">{{ number_format($totals[$month]) }}</td>
                                @endforeach
                            @endif
                        @endif
                    @endif
                    <td></td>
                </tr>
            </tfoot>
        @endif
    </table>
</body>

</html>
