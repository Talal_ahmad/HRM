@extends('Admin.layouts.master')
@section('title', 'Leave Requests')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Leave_Requests')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.Request&Approvals')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Leave_Requests')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        @if(userRoles()!="Self Service")
                            <!--Search Form -->
                            <div class="card-body">
                                <form id="search_form">
                                    <div class="row g-1 mb-md-1">
                                        <div class="col-md-3">
                                            <label class="form-label">@lang('app.Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')">
                                                <option value=""></option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if (env('COMPANY') == 'JSML')        
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="title">@lang('app.Section')</label>
                                                    <select name="section[]" id="section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                        
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                    </div>
                                                </div>
                                            </div>    
                                        @endif
                                        <div class="col-md-3">
                                            <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">

                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label">@lang('app.Leave_Types'):</label>
                                            <select name="leaveTypeFilter" id="leaveTypeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Leave_Type')">
                                                <option value=""></option>
                                                @foreach ($leaveTypes as $leaveType)
                                                    <option value="{{$leaveType->id}}">{{$leaveType->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label">@lang('app.Designation'):</label>
                                            <select name="designationFilter[]" id="designationFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Designation')" multiple>
                                                <option value=""></option>
                                                @foreach (designation() as $jobTitle)
                                                    <option value="{{$jobTitle->id}}" {{!empty(request('designationFilter')) ? in_array($jobTitle->id,request('designationFilter')) ? 'selected' : '' : ''}}>{{$jobTitle->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label">@lang('app.Status'):</label>
                                            <select name="statusFilter" id="statusFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Leave_Status')">
                                                <option value=""></option>
                                                <option value="Approved">@lang('app.Approved')</option>
                                                <option value="Rejected">@lang('app.Rejected')</option>
                                                <option value="Pending">@lang('app.Pending')</option>
                                                <option value="Cancelled">@lang('app.Cancelled')</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label">@lang('app.From_Date'):</label>
                                            <input type="text" name="fromDateFilter" id="fromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label">@lang('app.To_Date'):</label>
                                            <input type="text" name="toDateFilter" id="toDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                        </div>
                                        @if(env('COMPANY') == 'JSML')
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="is_half" id="is_half" value="1"/>
                                                        <label class="custom-control-label" for="is_half">@lang('app.Half_Leave')</label>
                                                    </div> 
                                                </div>
                                            </div>
                                        @endif
                                        <div style="margin-top:35px" class="col-md-3">
                                            <button class="btn btn-primary">@lang('app.Filter')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Employee ID</th>
                                    <th>Employee Code</th>
                                    <th>Employee</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Department/Branch</th>
                                    <th>Leave Type</th>
                                    <th>Leave Start Date</th>
                                    <th>Leave End Date</th>
                                    <th>Requested Date</th>
                                    <th>Designation</th>
                                    <th>Status</th>
                                    <th>Remarks</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Add  Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Leave_Request')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="employee">@lang('app.Employee')</label>
                                        <select name="employee" id="employee" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')" required>
                                            <option value=""></option>
                                            @foreach (employees() as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="type">@lang('app.Leave_Types')</label>
                                        <select name="leave_type" id="type" data-placeholder="@lang('app.Select_Leave_Type')" class="select2 form-select" required>
                                        
                                        </select>
                                    </div>
                                    <div class="alert alert-info p-1" id="leave_balance_show_div" style="display: none"></div>
                                    <div class="" id="leaveTable" style="">
                                    
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="date_start">@lang('app.Leave_Start_Date')</label>
                                        <input type="text" name="date_start" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="date_end">@lang('app.Leave_End_Date')</label>
                                        <input type="text" name="date_end" id="date_end" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="attachment">@lang('app.Attachments')</label>
                                        <input type="file" id="attachment" class="form-control" placeholder="" name="attachment"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="remarks" class="form-label">@lang('app.Remarks')</label>
                                        <textarea name="remarks" placeholder="@lang('app.Remarks')" class="form-control" id="remarks" rows="3"></textarea>
                                    </div>
                                </div>
                                @if(env('COMPANY') == 'JSML')
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="is_half" id="is_half" value="1"/>
                                                <label class="custom-control-label" for="is_half">@lang('app.Half_Leave')</label>
                                            </div> 
                                        </div>
                                    </div>
                                @endif
                                @can('Leave Request Approve Now')    
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="status" id="status" value="Approved"/>
                                                <label class="custom-control-label" for="status">@lang('app.Request_Approve_Now')</label>
                                            </div> 
                                        </div>
                                    </div>
                                @endcan
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                            <button type="submit" class="form_save btn btn-primary" id="leave_save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Edit Status Modal -->
        <div class="modal fade text-start" id="status_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Leave_Request_Status')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="status_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">      
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_status">@lang('app.Status')</label>
                                        <select name="status" id="edit_status" class="select2 form-select" data-placeholder="@lang('app.Select_Status')">
                                            <option value=""></option>
                                            <option value="Approved">@lang('app.Approved')</option>
                                            <option value="Cancelled">@lang('app.Cancelled')</option>
                                            <option value="Rejected">@lang('app.Rejected')</option>
                                            <option value="Pending">@lang('app.Pending')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Is Paid Modal -->
        <div class="modal fade text-start" id="paid_leave_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Single_Leaves')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="paid_leave_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <h4 class="text-center font-weight-bold" id="emp_name"></h4>
                            <div class="row">
                                <div class="col-12">
                                    <table class="table" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>Date</th>
                                                <th>Mark</th>
                                            </tr>
                                        </thead>
                                        <tbody id="append_row">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Leave History Modal -->
        <div class="modal fade text-start" id="leave_history_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title modal_heading" id="myModalLabel17">@lang('app.Leave_History')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h4 class="text-center font-weight-bold" id="emp_name"></h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-info p-1" id="leave_history_balance"></div>
                                <div id="leave_table" class="table-responsive">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('scripts')
    <script>
        var company = "{{env('COMPANY')}}";
        var rowid;
        var datatable;
        $(document).ready(function() {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                // console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });

            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'leave_request'

                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - ' +value.employee_code + ' - ' + value.first_name +' '+value.middle_name+' '+ value.last_name + ' - ' + value.designation+ ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });

            $("#employee").change(function() {
                var optVal = $(this).val();
                if(optVal){
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_emp_leave') }}",
                        data: {
                            employee: optVal,
                        },
                        success: function(response) {
                            $('#type').empty();
                            $('#type').html('<option value="">Select Employee</option>'); 
                            $.each(response, function(index, value) {
                                $('#type').append(
                                    $('<option></option>').val(value.id).html(
                                    value.name )
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                }
            });
            
            $("#type").change(function() {
                var employee = $('#employee').val();
                var leave_type = $(this).val();
                if(employee){
                    leaveBalance(employee,leave_type);
                }
            });
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('employee_leave_requests.index') }}",
                    data: function (filter) {
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.section = $('#section').val();
                        filter.leaveTypeFilter = $('#leaveTypeFilter').val();
                        filter.designationFilter = $('#designationFilter').val();
                        filter.statusFilter = $('#statusFilter').val();
                        filter.fromDateFilter = $('#fromDateFilter').val();
                        filter.toDateFilter = $('#toDateFilter').val();
                    }
                },
                columns: [
                    { 
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data : 'employee_id',
                        name : 'employees.employee_id',
                    },
                    {
                        data : 'employee_code',
                        name : 'employees.employee_code',
                    },
                    {
                        data: 'employeeName', 
                        render:function (data , type , row){
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + ' ' + middleName + ' ' + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'title',
                        name : 'companystructures.title', 
                    },
                    {
                        data: 'leave_name',
                        name : 'leavetypes.name',
                    },
                    {
                        data: 'date_start',
                        name : 'employeeleaves.date_start',
                    },
                    {
                        data: 'date_end',
                        name : 'employeeleaves.date_end',
                    },
                    {
                        data: 'created_at',
                        render:function (data , type , row){
                            var date = new Date(row.created_at),
                                        month = String(date.getMonth() + 1).padStart(2, '0'),
                                        day = String(date.getDate()).padStart(2, '0'),
                                        year = date.getFullYear();
                            return year+"-"+month+"-"+day;
                        }
                    },
                    {
                        data: 'job_title_name',
                        name: 'jobtitles.name',
                    },
                    {
                        data: 'status',
                        name : 'employeeleaves.status',
                    },
                    {
                        data: 'remarks',
                        name : 'employeeleaves.remarks',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            var btn = '';
                            @can('Leave Request Edit Status')    
                            btn = '<a href="javascript:;" class="item-edit" title="Chnage Status" onclick=change_status('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4 me-1' }) +
                                '</a>';
                            @endcan
                            btn += '<a href="javascript:;" class="item-edit" title="Leave History" onclick=leave_history('+full.id+')>' +
                                feather.icons['archive'].toSvg({ class: 'font-medium-4 me-1' }) +
                                '</a>';

                            @can('Paid Leave')
                                btn += '<a href="javascript:;" title="Single Paid Leave" onclick="paid_leave('+full.id+')">' +
                                feather.icons['calendar'].toSvg({ class: 'font-medium-4' }) +
                                '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'landscape',
                            pageSize: 'LEGAL',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Employee Leave Requests</h6>');

            // Add Data
            $("#add_form").submit(function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{route('employee_leave_requests.store')}}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else if(response.customMessage){
                            Swal.fire({
                                icon: 'error',
                                title: response.customMessage
                            })
                        }
                        else{
                            // console.log(response);
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $('#leave_balance_show_div').hide();
                            $("#add_modal").modal("hide");
                            $('#leaveTable').empty();
                            datatable.ajax.reload();
                            if(response.error){
                            Toast.fire({
                                icon: 'error',
                                title: 'Select Leaves under Given Limit.'
                            })
                            }else{
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Employee Leave Request has been Added Successfully!'
                                })
                            }
                        }
                    }
                });
            });
            // Update Status
            $('#status_form').submit(function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'status');
                $.ajax({
                    url: "{{url('employee_leave_requests')}}" + "/" + rowid,
                    type: 'post',
                    data:formData,
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#status_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#status_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Leave Request has been Updated Successfully!'
                            })
                        }
                    } 
                });
            });
            // Update Paid Leave
            $('#paid_leave_form').submit(function (e) {
                ButtonStatus('.form_save',true);
                blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'is_paid');
                $.ajax({
                    url: "{{url('employee_leave_requests')}}" + "/" + rowid,
                    type: 'post',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#paid_leave_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#paid_leave_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Leave Request has been Updated Successfully!'
                            })
                        }
                    } 
                });
            });
        });
        // Edit Status
        function change_status(id){
            rowid = id;
            $.ajax({
                url: "{{url('employee_leave_requests')}}" + "/" + id + "/edit",
                type: "GET",
                data: {
                    type: 'status'
                },
                success: function(response){
                    $("#edit_status").val(response.status).select2();
                    $("#status_modal").modal('show');
                }
            })
        }
        // Paid Leave
        function paid_leave(id){
            rowid = id;
            $.ajax({
                url: "{{url('employee_leave_requests')}}" + "/" + id + "/edit",
                type: "GET",
                data: {
                    type: 'is_paid'
                },
                success: function(response){
                    $('#paid_leave_modal').modal('show');
                    $('#emp_name').text(response.employee.first_name + ' ' + response.employee.last_name);
                    $('#append_row').empty();
                    $.each(response.listOfDates, function(key, value){
                        var checked = '';
                        if(response.paid_leaves.includes(value)){
                            checked = 'checked';
                        }
                        var html = '';
                        html += '<tr>'+
                                '<td>'+ (key + 1) + '</td>'+
                                '<td>'+ value +'</td>'+
                                '<td>'+
                                    '<div class="form-check">'+
                                        '<input type="checkbox" class="form-check-input" name="leave_date[]" value="'+value+'" '+checked+'>'
                                    +'</div>'+
                                '</td>'+
                            '</tr>';
                        $('#append_row').append(html);
                    });
                }
            })
        }
        function leaveBalance(employee,leave_type){
            $.ajax({
                type: "GET",
                url: "{{ url('get_leave_balance') }}",
                data: {
                    employee: employee,
                    leave_type: leave_type,
                },
                success: function(response) {
                    if(response.errorMessage){
                        Toast.fire({
                            icon: 'error',
                            title: response.errorMessage
                        })
                        return false;
                    }

                    // var total_dif = Number(response.data.balance) - (company != 'Ajmal Dawakhana' ? Number(response.data.leaves_taken) : Number(response.difference)); // Commented For Now, Can be discussed later
                    var balance = response.data.balance;
                    $('#leave_balance_show_div').html('<strong> Selected Employee has <span style="color:black">'+balance+'</span> Leaves in Balance.</strong>');
                    $('#leave_balance_show_div').show(100);
                    if (response.quota.more_quota=="Yes") {
                        ButtonStatus('#leave_save',false);
                    }
                    else if (response.leave.allow) {
                        ButtonStatus('#leave_save',false);
                    }
                    else if(balance <= 0){
                        ButtonStatus('#leave_save',true);
                    }
                    else{
                        ButtonStatus('#leave_save',false);
                    }
                    $('#leaveTable').empty();
                        var html = '';
                        html = `<table class="table table-responsive table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Leave Date Start</th>
                                            <th>Leave Date End</th>
                                            <th>Leave Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>`;
                                        if( response.data.leaves.length  > 0  ){
                                            $.each(response.data.leaves , function(index , value){
                                                html += `
                                                    <tr> 
                                                        <td>${index + 1}</td>
                                                        <td>${value.date_start}</td>    
                                                        <td>${value.date_end}</td>    
                                                        <td>${value.name}</td>    
                                                    </tr>
                                                `;
                                            });
                                        }
                                        else{
                                            html += `
                                                    <tr> 
                                                        <td colspan="4" class="text-center fw-bold">No Record Found</td>
                                                    </tr>
                                                `;
                                        }
                            html += `</tbody>
                                    </table>
                            `
                    $('#leaveTable').append(html);
                    $('#leaveTable').show(100);

                },
                error: function() {
                    alert('Error occured');
                }
            });
        }
        function leave_history(id){
            rowid = id;
            $.ajax({
                url: "{{url('employee_leave_requests')}}" + "/" + id,
                type: "GET",
                success: function(response){
                    $('#leave_history_balance').html('<strong> Selected Employee has <span style="color:black">'+response.leave_history.balance+" "+response.leave_name+'</span> Leaves in Balance.</strong>');
                    $('#leave_history_balance').show(100);
                    $('#leave_table').empty();
                    var html = "";
                    html = `
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>    
                                    <th>Employee</th>    
                                    <th>Leave Type</th>    
                                    <th>Leave Start Date</th>  
                                    <th>Leave End Date</th>    
                                    <th>Leave Status</th>    
                                    <th>Created By</th>    
                                </tr>
                            </thead>    
                        <tbody>
                    `;
                        if(response.leave_history.leaves.length > 0){
                            $.each(response.leave_history.leaves , function(index , val){
                                html += `
                                    <tr>
                                        <td>${index + 1}</td>    
                                        <td>${val.first_name} ${val.last_name}</td>    
                                        <td>${val.leave_name}</td>    
                                        <td>${val.date_start}</td>    
                                        <td>${val.date_end}</td>    
                                        <td>${val.status}</td>    
                                        <td>${val.creator}</td>    
                                    </tr>
                                `;
                            });
                        }
                        else{
                            html += `
                                    <tr> 
                                        <td colspan="7" class="text-center fw-bold">No Record Found</td>
                                    </tr>
                                `;
                        }
                        html += `</tbody>
                                    </table>
                            `;
                
                    $('#leave_table').html(html);
                    $("#leave_history_modal").modal('show');
                }
            });
        }
        // Filter Function
        $('#search_form').submit(function(e){
            datatable.draw();
            e.preventDefault();
        });
    </script>
@endsection