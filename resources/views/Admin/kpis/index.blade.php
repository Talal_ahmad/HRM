@extends('Admin.layouts.master')
@section('title', 'KPI Index')
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">KPI</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item active">Kpis
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="basic-dataTable">
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th>SR.NO</th>
                                    <th>KPI</th>
                                    <th>@lang('app.Marking_Criteria')</th>
                                    <th>@lang('app.Number_Assigned')</th>
                                    <th>@lang('app.Actions')</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Add Modal -->
    <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Create_KPI')</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form" id="createKpiForm">
                    @csrf
                    @method('POST')
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="kpidefination">KPI</label>
                                    <input type="text" name="kpidefination" id="kpidefination" class="form-control" placeholder="Define KPI">
                                </div>
                                @error('kpidefination')
                                <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="rule">@lang('app.Select_Marking_Criteria')</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline2" name="marking_criteria" class="custom-control-input ms-2" value="Very Excelent">
                                            <label class="custom-control-label" for="customRadioInline1"><b>A</b></label>
                                            <input type="radio" id="customRadioInline2" name="marking_criteria" class="custom-control-input ms-2" value="Excelent">
                                            <label class="custom-control-label" for="customRadioInline2"><b>B</b></label>
                                            <input type="radio" id="customRadioInline2" name="marking_criteria" class="custom-control-input ms-2" value="Very Good">
                                            <label class="custom-control-label" for="customRadioInline2"><b>C</b></label>
                                            <input type="radio" id="customRadioInline2" name="marking_criteria" class="custom-control-input ms-2" value="Good">
                                            <label class="custom-control-label" for="customRadioInline2"><b>D</b></label>
                                            <input type="radio" id="customRadioInline2" name="marking_criteria" class="custom-control-input ms-2" value="Setisfying">
                                            <label class="custom-control-label" for="customRadioInline2"><b>E</b></label>
                                        </div>
                                        @error('marking_criteria')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <label class="form-label" for="rule">@lang('app.Select_Number_Range')</label>
                            <input id="number_range" type="range" min="0" max="100" step="1" name="number_range" />
                            <p>Value: <output id="value"></output></p>
                        </div>
                        @error('number_range')
                        <div class="error">{{ $message }}</div>
                        @enderror
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                            <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Save')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Add Modal -->
    <!-- Edit Modal -->
    <div class="modal fade text-start" id="edit_kpi_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">@lang('app.Update_KPI')</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form" id="updateKpiForm">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="editkpidefination">KPI</label>
                                    <input type="text" name="editkpidefination" id="editkpidefination" class="form-control" placeholder="Define KPI">
                                </div>
                                @error('kpidefination')
                                <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="rule">@lang('app.Select_Marking_Criteria')</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="editmarking_criteria1" name="editmarking_criteria" class="custom-control-input ms-2" value="Very Excelent">
                                            <label class="custom-control-label" for="editmarking_criteria1"><b>A</b></label>
                                            <input type="radio" id="editmarking_criteria2" name="editmarking_criteria" class="custom-control-input ms-2" value="Excelent">
                                            <label class="custom-control-label" for="editmarking_criteria2"><b>B</b></label>
                                            <input type="radio" id="editmarking_criteria3" name="editmarking_criteria" class="custom-control-input ms-2" value="Very Good">
                                            <label class="custom-control-label" for="editmarking_criteria3"><b>C</b></label>
                                            <input type="radio" id="editmarking_criteria4" name="editmarking_criteria" class="custom-control-input ms-2" value="Good">
                                            <label class="custom-control-label" for="editmarking_criteria4"><b>D</b></label>
                                            <input type="radio" id="editmarking_criteria5" name="editmarking_criteria" class="custom-control-input ms-2" value="Setisfying">
                                            <label class="custom-control-label" for="editmarking_criteria5"><b>E</b></label>
                                        </div>
                                        @error('marking_criteria')
                                        <div class="error">{{ $message }}

                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <label class="form-label" for="editnumber_range">@lang('app.Select_Number_Range')</label>
                            <input id="editnumber_range" type="range" min="0" max="100" step="1" name="editnumber_range" />
                            <p>@lang('app.Value'):<output id="editvalue"></output></p>
                        </div>
                        @error('number_range')
                        <div class="error">{{ $message }}</div>
                        @enderror
                        <input id="kpi_id" type="hidden" name="kpi_id" />
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                            <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Save')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Edit Modal -->
</section>
@endsection

@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function() {
        datatable = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('kpis.index') }}",
            columns: [
                {
                    data: 'responsive_id'
                },
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    searchable: false,
                    orderable: false
                },
                {
                    data: 'kpi',
                    name: 'kpi',
                },
                {
                    data: 'marking_criteria',
                    name: 'marking_criteria',
                },
                {
                    data: 'number_range',
                    name: 'number_range',
                },
                // {
                //     data: '',
                //     searchable: false
                // },
            ],
            "columnDefs": [
                {
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full, meta) {
                        return (
                            '<a href="javascript:;" class="item-edit" onclick=kpi_edit(' + full
                            .id + ')>' +
                            feather.icons['edit'].toSvg({
                                class: 'font-medium-4'
                            }) +
                            '</a>' 
                            +
                            '<a href="javascript:;" onclick="kpi_delete(' + full.id +
                            ')">' +
                            feather.icons['trash-2'].toSvg({
                                class: 'font-medium-4 text-danger'
                            }) +
                            '</a>'
                        );
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            aLengthMenu: [
                [10,25,50,100,-1],
                [10,25,50,100,"All"]
            ],
            buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                            exportOptions: {
                                columns: [0, 1]
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                            exportOptions: {
                                columns: [0, 1]
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                            exportOptions: {
                                columns: [0, 1]
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                            exportOptions: {
                                columns: [0, 1]
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                            exportOptions: {
                                columns: [0, 1]
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                },
                {
                    text: feather.icons['plus'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Add New',
                    className: 'create-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">KPIs Table</h6>');
        
        
    });

    $("#createKpiForm").submit(function(e) {
        blockUI();
        e.preventDefault();
        $.ajax({
            url: "{{ route('kpis.store') }}",
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                $.unblockUI();
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    console.log();
                    $('#createKpiForm')[0].reset();
                    $(".select2").val('').trigger('change')
                    datatable.ajax.reload();
                    $('#add_modal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'KPI has been Added Successfully!'
                    })
                }

            }
        });
    });

    function kpi_edit(id) {
        $.ajax({
            url: "{{ url('kpis') }}" + "/" + id +"/edit",
            type: "GET",
            success: function(data) {
                $('#editkpidefination').val(data.kpi);
                if(data.marking_criteria == 'Very Excelent'){
                    $('#editmarking_criteria1').attr('checked','checked');
                }
                if(data.marking_criteria == 'Excelent'){
                    $('#editmarking_criteria2').attr('checked','checked');
                }
                if(data.marking_criteria == 'Very Good'){
                    $('#editmarking_criteria3').attr('checked','checked');
                }
                if(data.marking_criteria == 'Good'){
                    $('#editmarking_criteria4').attr('checked','checked');
                }
                if(data.marking_criteria == 'Setisfying'){
                    $('#editmarking_criteria5').attr('checked','checked');
                }
                $('#editmarking_criteria').val(data.marking_criteria);
                $('#editnumber_range').val(data.number_range);
                $('#kpi_id').val(data.id);
                $('#editvalue').empty();
                $('#editvalue').append(data.number_range);
                $('#edit_kpi_modal').modal('show');
            },
            error: function(xhr) {
                Toast.fire({
                    icon: 'error',
                    title: 'An error has been occured! Please Contact Administrator.'
                });
            }
        });

    }


    $("#updateKpiForm").submit(function(e) {
        blockUI();
        e.preventDefault();
        var id = $('#kpi_id').val();
        $.ajax({
            url: "{{ url('kpis') }}/" + id,
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                $.unblockUI();
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#updateKpiForm')[0].reset();
                    $(".select2").val('').trigger('change')
                    datatable.ajax.reload();
                    $('#edit_kpi_modal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'KPI has been Updated Successfully!'
                    })
                }

            }
        });
    });

    // function update_kpi(id) {
    //     $.ajax({
    //         url: "{{ url('kpis') }}/" + id,
    //         type: "PUT",
    //         data: {
    //             "_token": "{{ csrf_token() }}",
    //             "kpidefination": $('#editkpidefination').val(),
    //             "marking_criteria": $('input[name=editmarking_criteria]:checked').val(),
    //             "number_range": $('#editnumber_range').val()
    //         },
    //         success: function(response) {
    //             if (response.code === 200) {
    //                 console.log('KPI updated successfully');
    //             } else if (response.code === 404) {
    //                 console.error('KPI not found');
    //             } else if (response.code === 422) {
    //                 console.error('Validation error:', response.errors);
    //             } else {
    //                 console.error('Error:', response.error_message);
    //             }
    //         },
    //         error: function(xhr) {
    //             console.error('An error occurred:', xhr.statusText);
    //         }
    //     });
    // }

    function kpi_delete(id) {
        $.ajax({
            url: "{{url('kpis')}}" + "/" + id,
            type: 'POST',
            data: {
                '_token': $('meta[name=csrf-token]').attr("content"),
                '_method': 'DELETE',
                "id": id
            },

            success: function() {
                Toast.fire({
                    icon: 'success',
                    title: 'KPI has been Deleted Successfully!'
                })
                location.reload();

            },
            error: function(xhr) {
                Toast.fire({
                    icon: 'error',
                    title: 'An error has been occured! Please Contact Administrator.'
                });
            }
        });

    }
    const value = document.querySelector("#value")
    const input = document.querySelector("#number_range")
    value.textContent = input.value
    input.addEventListener("input", (event) => {
        value.textContent = event.target.value
    })
    const editvalue = document.querySelector("#editvalue")
    const editinput = document.querySelector("#editnumber_range")
    value.textContent = input.value
    editinput.addEventListener("input", (event) => {
        editinput.textContent = event.target.value
    })
</script>
@endsection