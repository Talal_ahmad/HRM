@extends('Admin.layouts.master')
@section('title', 'kpi_evaluation_forms')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">KPIs</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">kpi_evaluation_form</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @can('Issue-New-Disciplinarie')
    <div class="col-12">
        <div class="card mb-1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary mt-2" data-bs-toggle="modal" data-bs-target="#add_modal">@lang('app.Create_Evaluation_Form') <b> + </b></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan
</section>
<section>
    <div class="modal-size-lg d-inline-block">
        <!-- Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.KPI_Evaluation_Form')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" action="{{url('issue_disciplinaries')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="form_name">@lang('app.Name_Evaluation_Form')</label>
                                            <input type="text" name="form_name" id="form_name" class="form-control " placeholder="Enter Form Name" />

                                            @error('form_name')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="department">@lang('app.Select_Department')</label>
                                            <select name="department[]" id="department[]" class="select2 form-select" onchange="selectedemployees(this)" required multiple>
                                                <option value="">Select Department</option>
                                                @foreach($departments as $department)
                                                <option value="{{$department->id}}" {{$department->id ==request('department') ? 'selected' : ""}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button" onclick="selectAll('#department')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button" onclick="deselectAll('#department')">@lang('app.Deselect_All')</button>
                                            </div>
                                            @error('department')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee">@lang('app.Select_Employee')</label>
                                            <select name="employee[]" id="employee" class="select2 form-select" required multiple>
                                                <option value="1">Select Employee</option>
                                                @foreach($employees as $employee)
                                                <option value="{{$employee->id}}" {{$employee->id ==request('employee') ? 'selected' : ""}}>{{$employee->employee_id." ".$employee->first_name." ".$employee->middle_name." ".$employee->last_name ." ".$employee->employee_code." ".$employee->job_title." ".$employee->department}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button" onclick="selectAll('#employee')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button" onclick="deselectAll('#employee')">@lang('app.Deselect_All')</button>
                                            </div>
                                            @error('employee')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="kpi">@lang('app.Assign_KPI')</label>
                                            <select name="kpi[]" id="kpi" class="select2 form-select" required multiple>
                                                <option value="1">Select KPI</option>
                                                @foreach($kpis as $kpi)
                                                <option value="{{$kpi->id}}" {{$employee->id ==request('employee') ? 'selected' : ""}}>{{$kpi->kpi}}</option>
                                                @endforeach
                                            </select>

                                            @error('employee')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                            <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    function selectedemployees(values) {
        // var data=values.val()
        console.log(values);

    }
</script>

@endsection