@extends('Admin.layouts.master')
@section('title', 'Add Headlines')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Headlines')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.Headlines')</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @can('Add-New-Headlines')
    <div class="col-12">
        <div class="card mb-1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary mt-2" data-bs-toggle="modal" data-bs-target="#add_headline">@lang('app.Add_Headlines') <b>+</b></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan
</section>
<section>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="basic-dataTable">
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <!-- <th class="not_include">SR.NO</th> -->
                                    <th>Created By</th>
                                    <th>Designation</th>
                                    <th>Content</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($headlines as $key => $headline)
                                <tr>
                                    <!-- <td class="not_include">{{$key+1}}</td> -->
                                    <td>{{$headline->name}}</td>
                                    <td>{{$headline->designation}}</td>                                  
                                    <td>{!!$headline->headline!!}</td>
                                    <td>{{$headline->from_datetime}}</td>
                                    <td>{{$headline->to_datetime}}</td>
                                    @can('Edit-Delete-Headlines')
                                    <td>
                                        <a href="" data-bs-toggle="modal" data-bs-target="#edit-modal" class="item-edit font-medium-4 me-2" onclick=headlines_items_edit({{$headline->id}})><i data-feather='edit'></i></a>
                                        <a href="" data-bs-toggle="modal" data-bs-target="#edit-modal" class="font-medium-4 text-danger" onclick=headlines_items_delete({{$headline->id}})><i data-feather='trash-2'></i></a>
                                    </td>
                                    @endcan
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal-size-lg d-inline-block">

        <!-- Headline Modal -->
        <div class="modal fade text-start" id="add_headline" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">@lang('app.Create_Headline')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" action="{{url('createheadline')}}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="name">@lang('app.Name')</label>
                                            <input type="text" class="form-control" id="name" placeholder="@lang('app.Enter_Your_Name')" name="name" required />
                                        </div>
                                    </div>
                                    @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="designation">@lang('app.Designation')</label>
                                            <input type="text" class="form-control" id="designation" placeholder="@lang('app.Designation')" name="designation" required />
                                        </div>
                                    </div>
                                    @error('designation')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                            <div class="mb-1">
                                <div class="mb-1">
                                    <label class="form-label" for="from_date">@lang('app.From_Date')</label>
                                    <input type="datetime-local" class="form-control" id="from_date" placeholder="@lang('app.From_Date')" name="from_date" required />
                                </div>
                            </div>
                            @error('from_date')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-6">
                            <div class="mb-1">
                                <div class="mb-1">
                                    <label class="form-label" for="to_date">@lang('app.To_Date')</label>
                                    <input type="datetime-local" class="form-control" id="to_date" placeholder="@lang('app.To_Date')" name="to_date" required />
                                </div>
                            </div>
                            @error('from_date')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>                     
                        </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-1">
                                <div class="mb-1">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label" for="edit_description">@lang('app.Message')</label>
                                            <textarea class="ckeditor form-control" id="edit_description" name="headline" rows="3"></textarea>
                                        </div>
                                        @error('message')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @error('from_date')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <input type="hidden" name="created_by" id="created_by" value="{{auth()->user()->id}}">
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                            <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End HeadLine Modal -->
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
            ordering: true,
            "columnDefs": [{
                // For Responsive
                className: 'control',
                orderable: false,
                targets: 0
            }, ],
            dom: `<"card-header border-bottom p-1"<"head-label">
            <"dt-action-buttons text-end"B>>
            <"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l>
            <"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i>
            <"col-sm-12 col-md-6"p>>`,
            displayLength: 50,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [{
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                }
            }],

            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">Headlines Listing</h6>');
    });

    function headlines_items_edit(id) {
        var imgHtml;
        $.ajax({
            url: "headlines_items_edit/" + id,
            success: function(data){
               $('#name').val(data.name);
               $('#designation').val(data.designation);
               $('#from_date').val(data.from_datetime);
               $('#to_date').val(data.to_datetime);
               $('#edit_description').val(data.headline);
               $('#myModalLabel').html(`Edit Headlines`).append();
               $('#add_headline').modal('show');

            }
        });

    }

    function headlines_items_delete(id) {
        $.ajax({
            url: "headlines_items_delete/" + id,
            success: function() {
                Toast.fire({
                    icon: 'success',
                    title: 'HeadLine has been Deleted Successfully!'
                })
                location.reload();

            },
            error: function(xhr) {
                Toast.fire({
                    icon: 'error',
                    title: 'An error has been occured! Please Contact Administrator.'
                });
            }
        });

    }
</script>
@endsection