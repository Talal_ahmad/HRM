@extends('Admin.layouts.master')
@section('title', 'News-Dashboard')

@section('content')

<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0 bg-danger text-light rounded-pill" style="font-size: xx-large;" >&nbsp; @lang('app.News_Page')</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section>                
    <div class="card pt-1 pb-0 rounded-pill" style="background-color: #f7fff7;">
      <marquee class="rounded-pill mb-1" height="30px" direction="right" attribute_name="attribute_value" ....more attributes>
      {!! $newsheadlines !!}
      </marquee>        
    </div>   
</section>
<section id="card-demo-example">
    <div class="row match-height">
        @foreach($news as $currentnews)
        @if(!empty(array_intersect(login_user_departments(),explode(',' ,$currentnews->department_ids))))
        @if(!empty($currentnews->attachment_vedio))
        <div class="col-md-6 col-lg-4">
            <div class="card">
                <div class="card-body d-flex justify-content-end">
                    @can('Remove-or-ExtendDeuration-News')
                    <a href="{{url('deletenews/'.$currentnews->id)}}" class="card-link">Remove</a>
                    <a href="#" class="card-link" value="{{$currentnews->id}}" onclick="updationRecoredId(<?php echo $currentnews->id; ?>);" data-bs-toggle="modal" data-bs-target="#add_modal">Extend Duration</a>
                    @endcan
                </div>
                <hr>
                <div class="card-body">
                    <h4 class="card-title">{{ucfirst($currentnews->type)}}</h4>
                    <p class="card-subtitle text-muted m-1">Issued By: {{$currentnews->name}}</p>
                    <p class="card-subtitle text-muted m-1">Designation: {{$currentnews->designation}}</p>

                </div>
               <div>
               <video width="320" class="rounded" height="240" controls autoplay>
                    <source src="{{asset($currentnews->attachment_vedio)}}" type="video/mp4">
                    <source src="movie.ogg" type="video/ogg">
                    Your browser does not support the video tag.
                </video>
               </div>
                <div class="card-body">
                <div style="text-align:justify; font-weight:bold; color:blue;" class="m-3">
                        <p class="card-text m-5">{!! $currentnews->message !!}</p>
                 </div>
                 <p class="card-text m-2">From: {{$currentnews->from_date}} To:{{$currentnews->to_date}}</p>

                </div>
            </div>
        </div>
        @endif
        @if(empty($currentnews->attachment_image) && empty($currentnews->attachment_vedio))
        <div class="col-md-6 col-lg-8">
            <div class="card border">
                <div class="card-body d-flex justify-content-end">
                    @can('Remove-or-ExtendDeuration-News')
                    <a href="{{url('deletenews/'.$currentnews->id)}}" class="card-link">Remove</a>
                    <a href="#" class="card-link" onclick="updationRecoredId({{$currentnews->id}});" data-bs-toggle="modal" data-bs-target="#add_modal">Extend Duration</a>

                    @endcan
                </div>
                <hr>
                <div class="card text-bg-dark">
                    <img src="{{asset('../storage/app/News-Images_Attachments/stickynotes.jpg')}}" class="card-img" alt="...">
                    <div class="card-img-overlay mt-5">
                        <h5 class="card-title mt-5 pt-5 text-danger d-flex justify-content-center">{{ucfirst($currentnews->type)}}</h5>
                        <p class="card-subtitle text-muted m-1"> Issued By: {{$currentnews->name}}</p>
                        <p class="card-subtitle text-muted m-1">Designation: {{$currentnews->designation}}</p>
                        <div>
                        <div style="text-align:justify; font-weight:bold; color:blue;" class="m-3">
                        <p class="card-text m-5">{!! $currentnews->message !!}</p>
                       </div>
                            <p class="card-text m-2">From: {{$currentnews->from_date}} To:{{$currentnews->to_date}}</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if(!empty($currentnews->attachment_image))
        <div class="col-md-6 col-lg-8">
            <div class="card">
                <div class="card-body d-flex justify-content-end">
                    @can('Remove-or-ExtendDeuration-News')
                    <a href="{{url('deletenews/'.$currentnews->id)}}" class="card-link">Remove</a>
                    <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#add_modal">Extend Duration</a>
                    @endcan
                </div>
                <hr>
                <div class="card-body ">
                    <h5 class="card-title mt-5 pt-5 text-danger d-flex justify-content-center">{{ucfirst($currentnews->type)}}</h5>
                    <p class="card-subtitle text-muted m-1"> Issued By: {{$currentnews->name}}</p>
                    <p class="card-subtitle text-muted m-1">Designation: {{$currentnews->designation}}</p>


                </div>
                <img src="{{asset($currentnews->attachment_image)}}" alt="" srcset="">
                <div class="card-body">
                <div style="text-align:justify; font-weight:bold; color:blue;" class="m-3">
                        <p class="card-text m-5">{!! $currentnews->message !!}</p>
                 </div>
                 <p class="card-text m-2">From: {{$currentnews->from_date}} To:{{$currentnews->to_date}}</p>
                 <a href="{{asset($currentnews->attachment_image)}}" download="{{asset($currentnews->attachment_image)}}" class="btn btn-primary">Download</a>
                </div>
            </div>
        </div>
        @endif
        @endif
        @endforeach
    </div>
</section>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Update Modal -->
<div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Update Duration</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="form" action="{{url('updatenewsdeuration')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="mb-1">
                                <div class="mb-1">
                                    <label class="form-label" for="from_date">From-Date</label>
                                    <input type="datetime-local" class="form-control" id="from_date" placeholder="From-Date" name="from_date" required />
                                </div>
                            </div>
                            @error('from_date')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-6">
                            <div class="mb-1">
                                <div class="mb-1">
                                    <label class="form-label" for="to_date">To-Date</label>
                                    <input type="datetime-local" class="form-control " placeholder="To-Date" name="to_date" id="to_date" required />
                                </div>
                            </div>
                            @error('to_date')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-6">
                            <div class="mb-1">
                                <div class="mb-1">
                                    <input type="hidden" class="form-control" id="newsId" value="" placeholder="From-Date" name="newsId" required />
                                </div>
                            </div>
                            @error('from_date')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-outline-success">Reset</button>
                    <button type="submit" class="btn btn-primary form_save" id="save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Update Modal End -->

</section>

@endsection
@section('scripts')
<script>
    function updationRecoredId(id) {
        document.getElementById('newsId').value = id;

    }
</script>
@endsection