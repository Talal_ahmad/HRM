@extends('Admin.layouts.master')
@section('title', 'News-Index')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.News_Page')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.News_Page')</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @can('Add New News')
    <div class="col-12">
        <div class="card mb-1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary mt-2" data-bs-toggle="modal" data-bs-target="#add_modal">@lang('app.Add_News') <b>+</b></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan
</section>
<section>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="basic-dataTable">
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include">SR.NO</th>
                                    <th>Created By</th>
                                    <th>Designation</th>
                                    <th>Content</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $key => $current_news)
                                <tr>
                                    <td class="not_include"{{$key+1}}></td>
                                    <td>{{$current_news->name}}</td>
                                    <td>{{$current_news->designation}}</td>
                                    @if(!empty($current_news->attachment_image))
                                    <td><img src="{{asset($current_news->attachment_image)}}" class="rounded-circle" width="50px" height="50px"></td>
                                    @elseif(!empty($current_news->attachment_vedio))
                                    <td> <video width="50px" class="rounded-circle" height="50px">
                                            <source src="{{asset($current_news->attachment_vedio)}}" type="video/mp4">
                                            <source src="movie.ogg" type="video/ogg">
                                            Your browser does not support the video tag.
                                        </video></td>
                                    @else
                                    <td>{{$current_news->message}}</td>
                                    @endif
                                    <td>{{$current_news->from_date}}</td>
                                    <td>{{$current_news->to_date}}</td>
                                    @can('Edit-Delete-News')
                                    <td>
                                        <a href="" data-bs-toggle="modal" data-bs-target="#edit-modal" class="item-edit font-medium-4 me-2" onclick=newslist_items_edit({{$current_news->id}})><i data-feather='edit'></i></a>
                                        <a href="" data-bs-toggle="modal" data-bs-target="#edit-modal" class="font-medium-4 text-danger" onclick=newslist_items_delete({{$current_news->id}})><i data-feather='trash-2'></i></a>
                                    </td>
                                    @endcan
                               </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal-size-lg d-inline-block">
        <!-- Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_News')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" action="{{url('news_create')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="user">@lang('app.Requested_By')</label>
                                            <select name="user" id="user" class="select2 form-select" required>
                                                <option value="">Select User</option>
                                                @foreach($users as $user)
                                                <option value="{{$user->id}}" {{$user->id == request('user') ? 'selected' : ""}}>{{$user->username}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="noticetype">@lang('app.Type')</label>
                                            <select name="noticetype" id="noticetype" class="select2 form-select" required>
                                                <option value="">@lang('app.Select_Type')</option>
                                                <option value="warning">Warning</option>
                                                <option value="alert">Alert</option>
                                                <option value="notice">Notice</option>

                                            </select>
                                            @error('user')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="from_date">@lang('app.From_Date')</label>
                                            <input type="datetime-local" class="form-control" id="from_date" placeholder="@lang('app.From_Date')" name="from_date" required />
                                        </div>
                                    </div>
                                    @error('from_date')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="to_date">@lang('app.To_Date')</label>
                                            <input type="datetime-local" class="form-control " placeholder="@lang('app.To_Date')" name="to_date" id="to_date" required />
                                        </div>
                                    </div>
                                    @error('to_date')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="name">@lang('app.Issuer_Name')</label>
                                            <input type="text" class="form-control" id="name" placeholder="@lang('app.Enter_Your_Name')" name="name" required />
                                        </div>
                                    </div>
                                    @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="designation">@lang('app.Issuer_Designation')</label>
                                            <input type="text" class="form-control" id="designation" placeholder="@lang('app.Designation')" name="designation" required />
                                        </div>
                                    </div>
                                    @error('designation')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="attachment_image">@lang('app.Attachment_Image')</label>
                                            <input type="file" name="attachment_image" id="attachment_image" class="form-control " placeholder="@lang('app.Attachment_Image')" />
                                        </div>
                                    </div>
                                    @error('attachment_image')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="attachment_video">@lang('app.Attachment_Video')</label>
                                            <input type="file" name="attachment_video" id="attachment_video" class="form-control" placeholder="Attachment_Video" />
                                            <p style="color:red;">*(video size must not be greater then 30MB)</p>
                                        </div>
                                        @error('attachment_video')
                                        <div class="error">{{ "video size must not be greater then 10MB" }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="department">@lang('app.Select_Department')</label>
                                            <select name="department[]" id="department" class="select2 form-select" placeholder="@lang('app.Select_Employee')" required multiple>
                                                <option value="">@lang('app.Select_Department')</option>
                                                @foreach( $departments as $department)
                                                <option value="{{$department->id}}" {{$department->id ==request('department') ? 'selected' : ""}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button" onclick="selectAll('#department')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button" onclick="deselectAll('#department')">@lang('app.Deselect_All')</button>
                                            </div>
                                            @error('department')
                                            <div class="error">{{ "employee not selected" }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="message">@lang('app.Message')</label>
                                        <textarea class="ckeditor form-control" id="edit_description" name="message" id="message" rows="3"></textarea>
                                    </div>
                                    @error('message')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="hidden" name="created_by" id="created_by" value="{{auth()->user()->id}}">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                            <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Edit News Modal -->
        <div class="modal fade text-start" id="Edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_News')</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" action="{{url('newslist_items_update')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="edituser">@lang('app.Requested_By')</label>
                                            <select name="edituser" id="edituser" class="select2 form-select" required>
                                                <option value="">@lang('app.Select_User')</option>
                                                @foreach($users as $user)
                                                <option value="{{$user->id}}" {{$user->id == request('edituser') ? 'selected' : ""}}>{{$user->username}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="editeditnoticetype">@lang('app.Type')</label>
                                            <select name="editnoticetype" id="editnoticetype" class="select2 form-select" required>
                                                <option value="">@lang('app.Select_Type')</option>
                                                <option value="warning">Warning</option>
                                                <option value="alert">Alert</option>
                                                <option value="notice">Notice</option>

                                            </select>
                                            @error('user')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="editfrom_date">@lang('app.From_Date')</label>
                                            <input type="datetime-local" class="form-control" id="editfrom_date" placeholder="@lang('app.From_Date')" name="editfrom_date" required />
                                        </div>
                                    </div>
                                    @error('from_date')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="editto_date">@lang('app.To_Date')</label>
                                            <input type="datetime-local" class="form-control " placeholder="@lang('app.To_Date')" name="editto_date" id="editto_date" required />
                                        </div>
                                    </div>
                                    @error('to_date')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="editname">@lang('app.Issuer_Name')</label>
                                            <input type="text" class="form-control" id="editname" placeholder="@lang('app.Enter_Your_Name')" name="editname" required />
                                        </div>
                                    </div>
                                    @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="editdesignation">@lang('app.Issuer_Designation')</label>
                                            <input type="text" class="form-control" id="editdesignation" placeholder="@lang('app.Designation')" name="editdesignation" required />
                                        </div>
                                    </div>
                                    @error('designation')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="editattachment_image">@lang('app.Attachment_Image')</label>
                                            <input type="file" name="editattachment_image" id="editattachment_image" class="form-control " placeholder="@lang('app.Attachment_Image')" />
                                        </div>
                                    </div>
                                    @error('attachment_image')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="editattachment_video">@lang('app.Attachment_Video')</label>
                                            <input type="file" name="editattachment_video" id="editattachment_video" class="form-control" placeholder="Attachment_Video" />
                                            <p style="color:red;">*(video size must not be greater then 30MB)</p>
                                        </div>
                                        @error('attachment_video')
                                        <div class="error">{{ "video size must not be greater then 10MB" }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1" id="ImageAttachmentdiv">

                                       </div>
                                    </div>
                                   
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <div class="mb-1" id="VideoAttachmentdiv">

                                        </div>
                                    </div>
                                </div>
                               
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="editdepartment">@lang('app.Select_Department')</label>
                                            <select name="editdepartment[]" id="editdepartment" class="select2 form-select" placeholder="@lang('app.Select_Employee')" required multiple>
                                                <option value="">@lang('app.Select_Department')</option>
                                                @foreach( $departments as $department)
                                                <option value="{{$department->id}}" {{$department->id ==request('editdepartment') ? 'selected' : ""}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button" onclick="selectAll('#editdepartment')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button" onclick="deselectAll('#editdepartment')">@lang('app.Deselect_All')</button>
                                            </div>
                                            @error('department')
                                            <div class="error">{{ "employee not selected" }}</div>
                                            @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label" for="editmessage">@lang('app.Message')</label>
                                                <textarea class="ckeditor form-control" name="editmessage" id="editmessage" rows="3"></textarea>
                                            </div>
                                            @error('message')
                                            <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <input type="hidden" name="created_by" id="created_by" value="{{auth()->user()->id}}">
                                        <input type="hidden" name="toupdate" id="toupdate">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">@lang('app.Reset')</button>
                                    <button type="submit" class="btn btn-primary form_save" id="save">@lang('app.Save')</button>
                                </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Edit News Modal -->
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
            ordering: true,
            "columnDefs": [{
                // For Responsive
                className: 'control',
                orderable: false,
                targets: 0
            }, ],
            dom: `<"card-header border-bottom p-1"<"head-label">
            <"dt-action-buttons text-end"B>>
            <"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l>
            <"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i>
            <"col-sm-12 col-md-6"p>>`,
            displayLength: 50,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [{
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                }
            }],

            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">News Listing</h6>');
    });

    function newslist_items_edit(id) {
        $.ajax({
            url: "newslist_items_edit/" + id,
            success: function(data) {
                $('#edituser').val(data.user_id).select2();
                $('#editnoticetype').val(data.type).select2();
                $('#editfrom_date').val(data.from_date);
                $('#editto_date').val(data.to_date);
                $('#editname').val(data.name);
                $('#editdesignation').val(data.designation);
                $('#editdepartment').val(data.department_ids).select2();
                $('#editmessage').val(data.message);
                $('#toupdate').val(data.id);
                if(data.attachment_image){
                    $('#ImageAttachmentdiv').html(`<img src='${data.attachment_image}' class="rounded-circle" width="70px" height="70px">`).append();
                }
               if(data.attachment_vedio){
                $('#VideoAttachmentdiv').html(` <video width="50px" class="rounded-circle" height="50px">
                                            <source src="${data.attachment_vedio}" type="video/mp4">
                                            <source src="movie.ogg" type="video/ogg">
                                            Your browser does not support the video tag.
                                        </video>`).append();
               }
               $('#Edit_modal').modal('show');
              


              
                // $('#editstatus').val(data.status).select2();
                // $('#Edit_modal').modal('show');

            }
        });

    }

    function newslist_items_delete(id) {
        $.ajax({
            url: "newslist_items_delete/" + id,
            success: function() {
                Toast.fire({
                    icon: 'success',
                    title: 'News has been Deleted Successfully!'
                })
                location.reload();

            },
            error: function(xhr) {
                Toast.fire({
                    icon: 'error',
                    title: 'An error has been occured! Please Contact Administrator.'
                });
            }
        });

    }
    
</script>
@endsection