@extends('Admin.layouts.master')
@section('title', request()->query('type'))
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{ request()->query('type') }} Report</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                </li>
                                <li class="breadcrumb-item"><a href="#">Attendance Dashboard</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <div class="card-body">
                                <form class="form" action="{{ route('attendance_dashboard.create') }}" id="search_form">
                                    <div class="row">
                                        <input type="hidden" name="type" value="{{ request()->query('type') }}">
                                        <input type="hidden" name="department"
                                            value="{{ request()->query('department') }}">
                                        <input type="hidden" name="date" value="{{ request()->query('date') }}">
                                        @if (env('COMPANY') == 'JSML')        
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="dptfilter">Filter By department:</label>
                                                    <select name="dptfilter" id="department" class="select2 form-select" data-placeholder="Select Department" required>
                                                        <option value=""></option>
                                                        <option value="all" {{request('dptfilter') == 'all' ? 'selected' : ''}}>All</option>
                                                        @foreach (departments() as $department)
                                                            <option value="{{$department->id}}" {{!empty(request('dptfilter')) ? $department->id == request('dptfilter') ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="departmentFilter">Filter By
                                                        section:</label>
                                                    <select name="departmentFilter[]" id="section"
                                                        class="select2 form-select" data-placeholder="Select Department"
                                                        multiple>
                                                        <option value=""></option>
                                                        @foreach (departments() as $department)
                                                            <option value="{{ $department->id }}"
                                                                {{ !empty(request('departmentFilter')) ? (in_array($department->id, request('departmentFilter')) ? 'selected' : '') : '' }}>
                                                                {{ $department->title }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#section')">Select All</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#section')">Deselect All</button>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="departmentFilter">Filter By
                                                        department:</label>
                                                    <select name="departmentFilter[]" id="departmentFilter"
                                                        class="select2 form-select" data-placeholder="Select Department"
                                                        multiple>
                                                        <option value=""></option>
                                                        @foreach (departments() as $department)
                                                            <option value="{{ $department->id }}"
                                                                {{ !empty(request('departmentFilter')) ? (in_array($department->id, request('departmentFilter')) ? 'selected' : '') : '' }}>
                                                                {{ $department->title }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#departmentFilter')">Select All</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="employment_status">Filter By Employment
                                                    Status</label>
                                                <select name="employment_status" id="employment_status"
                                                    class="select2 form-select" data-placeholder="Select Employment Status">
                                                    <option value=""></option>
                                                    @foreach ($employment_status as $employment)
                                                        <option value="{{ $employment->id }}"
                                                            {{ $employment->id == request('employment_status') ? 'selected' : '' }}>
                                                            {{ $employment->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="designation">Filter By Designation</label>
                                                <select name="designation" id="designation" class="select2 form-select"
                                                    data-placeholder="Select Designation">
                                                    <option value=""></option>
                                                    @foreach (designation() as $designation)
                                                        <option value="{{ $designation->id }}"
                                                            {{ $designation->id == request('designation') ? 'selected' : '' }}>
                                                            {{ $designation->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="shift_type">Filter By Shift Type</label>
                                                <select name="shift_type" id="shift_type" class="select2 form-select"
                                                    data-placeholder="Select Shift Type">
                                                    <option value=""></option>
                                                    @foreach ($shift_type as $shift)
                                                        <option value="{{ $shift->id }}"
                                                            {{ $shift->id == request('shift_type') ? 'selected' : '' }}>
                                                            {{ $shift->shift_desc }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="singleDate">Filter By Date:</label>
                                            <input type="text" name="singleDate" id="singleDate"
                                                class="form-control flatpickr-basic singleDate" placeholder="YYYY-MM-DD"
                                                required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{ route('attendance_dashboard.create') }}?type={{ request()->query('type') }}&department={{ request()->query('department') }}&date={{ request()->query('date') }}"
                                                type="button" class="btn btn-danger mt-1">Reset</a>
                                            <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card pb-2">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Employee ID</th>
                                            <th>Employee Code</th>
                                            <th>Employee</th>
                                            <th>Employment Status</th>
                                            <th>Designation</th>
                                            <th>Sub Unit Branch</th>
                                            <th>Shift Type</th>
                                            @if (request()->query('type') == 'Present Employees' ||
                                                    request()->query('type') == 'LateCommers Employees' ||
                                                    request()->query('type') == 'Missing Checkout Employees' ||
                                                    request()->query('type') == 'On OffDay Present Employees' ||
                                                    request()->query('type') == 'On Leave But Present Employees')
                                                <th>In Time</th>
                                            @endif
                                            @if (request()->query('type') == 'Present Employees' ||
                                                    request()->query('type') == 'LateCommers Employees' ||
                                                    request()->query('type') == 'On OffDay Present Employees' ||
                                                    request()->query('type') == 'On Leave But Present Employees')
                                                <th>Out Time</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($query as $key => $employee)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $employee->employee_id }}</td>
                                                <td>{{ $employee->employee_code }}</td>
                                                <td>{{ $employee->first_name }} {{ $employee->middle_name }}
                                                    {{ $employee->last_name }}</td>
                                                <td>{{ $employee->employment }}</td>
                                                <td>{{ $employee->designation }}</td>
                                                <td>{{ $employee->department }}</td>
                                                @if (empty($employee->shift_desc))
                                                    <td>
                                                        <p class="btn btn-primary"
                                                            onclick="user_shift_add({{ $employee->id }})">Add Shift</p>
                                                    </td>
                                                @else
                                                    <td>{{ $employee->shift_desc }}</td>
                                                @endif
                                                @if (request()->query('type') == 'Present Employees' ||
                                                        request()->query('type') == 'LateCommers Employees' ||
                                                        request()->query('type') == 'Missing Checkout Employees' ||
                                                        request()->query('type') == 'On OffDay Present Employees' ||
                                                        request()->query('type') == 'On Leave But Present Employees')
                                                    <td>{{ $employee->in_time }}</td>
                                                @endif
                                                @if (request()->query('type') == 'Present Employees' ||
                                                        request()->query('type') == 'LateCommers Employees' ||
                                                        request()->query('type') == 'On OffDay Present Employees' ||
                                                        request()->query('type') == 'On Leave But Present Employees')
                                                    <td>{{ $employee->out_time }}</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
    <!-- add Modal -->
    <div class="modal fade text-start" id="user_shift_add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">Add Shift</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form" id="usre_shift_add_form">
                    @csrf
                    @method('POST')
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="shift_id">Shift Type</label>
                                    <select name="shift_id" id="shift_id" class="select2 form-select"
                                        data-placeholder="Select Shift Type" required>
                                        <option value=""></option>
                                        @foreach ($shift_type as $shift)
                                            <option value="{{ $shift->id }}">
                                                {{ $shift->shift_desc }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="emp_id" id="emp_id" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <label class="form-label" for="work_week">Work Week</label>
                                <select name="work_week_id" id="work_week" class="select2 form-select"
                                    data-placeholder="Select Work Week" required>
                                    <option value=""></option>
                                    @foreach ($work_week as $week)
                                        <option value="{{ $week->id }}">{{ $week->desc }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="shift_from">Shift From</label>
                                    <input type="text" id="shift_from" class="form-control flatpickr-basic text-left"
                                        placeholder="Shift From" name="shift_from_date" required />
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="shift_to">Shift To</label>
                                    <input type="text" id="shift_to" class="form-control flatpickr-basic text-left"
                                        placeholder="Shift Date" name="shift_to_date" required />
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="time_allowed">Late Time
                                        Allowed</label>
                                    @if (env('COMPANY') == 'JSML' || env('COMPANY') == 'AJMAL' || env('COMPANY') == 'RoofLine')
                                        <input type="text" id="time_allowed" class="form-control"
                                            name="late_time_allowed" value="15" readonly required />
                                    @else
                                        <input type="text" id="time_allowed" class="form-control"
                                            name="late_time_allowed" value="11" readonly required />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="save">submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        var d = new Date();
        var type = '{!! request()->query('type') !!}';
        var department = <?php echo json_encode($dept_name); ?>;
        var date = '{!! request()->query('date') !!}';
        var current_date = new Date();
        var cur_date = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
        $(document).ready(function() {
            $('#dataTable').DataTable({
                "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    targets: 0
                }, ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            messageTop: `Report Type: ${type}
                                Department: ${department}
                                Date: ${date ? date : cur_date}
                                `,
                            customize: function(win) {
                                $(win.document.body)
                                    .css('font-size', '7pt')
                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            },
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            messageTop: `Report Type: ${type}
                                Department: ${department}
                                Date: ${date ? date : cur_date}
                                `,
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'portrait',
                            pageSize: 'LEGAL',
                            customize: function(doc) {
                                doc.defaultStyle.fontSize = 7;
                            },
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }, ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">{!! request()->query('type') !!}</h6>');
        });

        function user_shift_add(id) {
            $('#usre_shift_add_form')[0].reset();
            emp_id = id;
            $("#emp_id").val(emp_id);
            $("#user_shift_add_modal").modal("show");
        }
        $('#usre_shift_add_form').submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: "{{ route('shiftstore') }}",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#usre_shift_add_form')[0].reset();
                        $("#user_shift_add_modal").modal("hide");
                        Toast.fire({
                            icon: 'success',
                            title: 'User Shift has been Added Successfully!'
                        })
                        location.reload();
                    }
                }
            });
        });
        $("#department").change(function() {
            var optVal = $(this).val();
            if (optVal.length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_department_child') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#section').empty();
                        $('#section').html('<option value="">Select Section</option>'); 
                        $.each(response, function(index, value) {
                            
                            console.log(value.id);
                            $('#section').append(
                                $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                    value.title)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            } else {
                $('#section').empty();
            }
        });
    </script>
@endsection
