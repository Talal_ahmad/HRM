@extends('Admin.layouts.master')
@section('title', 'Attendance Dashboard')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Attendance_Dashboard')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.attendance')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Attendance_Dashboard')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form" id="search_form" method="GET" action="{{ route('attendance_dashboard.index') }}">
                                    <div class="row d-flex align-items-center">
                                        @if (env('COMPANY') == 'JSML')        
                                            <div class="col-md-6 col-12">
                                                <label class="form-label" for="dptfilter">@lang('app.Filter_By_department:')</label>
                                                <select name="dptfilter" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                    <option value=""></option>
                                                    <option value="all" {{request('dptfilter') == 'all' ? 'selected' : ''}}>All</option>
                                                    @foreach (departments() as $department)
                                                        <option value="{{$department->id}}" {{!empty(request('dptfilter')) ? $department->id == request('dptfilter') ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6 col-12 mt-3">
                                                <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Section')</label>
                                                <select name="department[]" id="section"
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Department')"
                                                    multiple>
                                                    <option value=""></option>
                                                    @foreach (departments() as $department)
                                                        <option value="{{ $department->id }}"
                                                            {{ !empty(request('department')) ? (in_array($department->id, request('department')) ? 'selected' : '') : '' }}>
                                                            {{ $department->title }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#section')">@lang('app.Select_all')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#section')">@lang('app.Deselect_all')</button>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-12 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="departmentFilter">@lang('app.Filter_By_department:')</label>
                                                    <select name="department[]" id="departmentFilter"
                                                        class="select2 form-select" data-placeholder="Select Department"
                                                        multiple>
                                                        <option value=""></option>
                                                        @foreach (departments() as $department)
                                                            <option value="{{ $department->id }}"
                                                                {{ !empty(request('departmentFilter')) ? (in_array($department->id, request('departmentFilter')) ? 'selected' : '') : '' }}>
                                                                {{ $department->title }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#departmentFilter')">@lang('app.Select_all')</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#departmentFilter')">@lang('app.Deselect_all')</button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="date_filter">@lang('app.Filter_By_Date:')</label>
                                                <input type="text" id="date_filter" id="shift_from" class="form-control flatpickr-basic text-left" placeholder="YYYY-mm-dd" name="date" value="{{request('date')}}" required />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{url('attendance_dashboard')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                            <button type="submit" class="btn btn-primary mt-1">@lang('app.Filter')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body row">
                    <div class="col-xl-4 col-md-6 col-12">
                        <a href="#" class="text-dark">
                            <div class="card card-statistics" onclick="all_dataTable(this.id)" id="All Employees" style="height:175px;">
                                <div class="card-header">
                                    <h4 class="card-title">@lang('app.Employees')</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-info me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="user" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                @if (env('COMPANY') == 'JSML')
                                                    <h4 class="fw-bolder mb-0">{{ employees()->count() }}</h4>
                                                @else
                                                    <h4 class="fw-bolder mb-0">{{ $total_employees }}</h4>
                                                @endif
                                                <p class="card-text font-small-3 mb-0">@lang('app.Total_Employees')</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6 col-12">
                        <a href="#" class="text-dark">
                            <div class="card card-statistics" onclick="all_dataTable(this.id)" id="Present Employees" style="height:175px;">
                                <div class="card-header">
                                    <h4 class="card-title">@lang('app.Present')</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-info me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="user" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{ count($total_present_employees)}}</h4>
                                                <p class="card-text font-small-3 mb-0">@lang('app.Total_Present_Employees')</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6 col-12">
                        <a href="#" class="text-dark">
                            <div class="card card-statistics" onclick="all_dataTable(this.id)" id="Absent Employees" style="height:175px;">
                                <div class="card-header">
                                    <h4 class="card-title">@lang('app.Absent')</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{ !empty($holiday) ? 0 : count($total_absent_employees) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">@lang('app.Total_Absent_Employees')</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6 col-12">
                        <a href="#" class="text-dark">
                            <div class="card card-statistics" onclick="all_dataTable(this.id)" id="LateCommers Employees" style="height:175px;">
                                <div class="card-header">
                                    <h4 class="card-title">@lang('app.Latecommers')</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-info me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="user" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{ count($total_late_employees) }}</h4>
                                                <p class="card-text font-small-3 mb-0">@lang('app.All_Latecommers')</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6 col-12">
                        <a href="#" class="text-dark">
                            <div class="card card-statistics" onclick="all_dataTable(this.id)" id="OnLeave Employees" style="height:175px;">
                                <div class="card-header">
                                    <h4 class="card-title">@lang('app.On_Leave')</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{ count($total_leave_employees) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">@lang('app.Total_Employees_On_Leave')</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6 col-12">
                        <a href="#" class="text-dark">
                            <div class="card card-statistics" onclick="all_dataTable(this.id)" id="On Leave But Present Employees" style="height:175px;">
                                <div class="card-header">
                                    <h4 class="card-title">@lang('app.On_Leave_(Present)')</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{ count($total_leave_present) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">@lang('app.On_Leave_But_Present')</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6 col-12">
                        <a href="#" class="text-dark">
                            <div class="card card-statistics" onclick="all_dataTable(this.id)" id="On OffDay Employees" style="height:175px;">
                                <div class="card-header">
                                    <h4 class="card-title">@lang('app.On_Off_Day')</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{ count($total_on_off_day) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">@lang('app.Employees_on_Off_Day')</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6 col-12">
                        <a href="#" class="text-dark">
                            <div class="card card-statistics" onclick="all_dataTable(this.id)" id="On OffDay Present Employees" style="height:175px;">
                                <div class="card-header">
                                    <h4 class="card-title">@lang('app.Off_Day_(Present)')</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{ count($total_on_off_day_present) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">@lang('app.Employees_Present_On_Off_Day')</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-md-6 col-12">
                        <a href="#" class="text-dark">
                            <div class="card card-statistics" onclick="all_dataTable(this.id)" id="Missing Checkout Employees" style="height:175px;">
                                <div class="card-header">
                                    <h4 class="card-title">@lang('app.Missing_Checkout')</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{ count($total_missing_checkout)}}</h4>
                                                    <p class="card-text font-small-3 mb-0">@lang('app.Employees_Missing_Checkouts')</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="content-body"> --}}
            {{-- <section id="basic-dataTable" class="d-none">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2" id="dataTable">
                            <table class="table" id="all_dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th class="not_include">Name</th>
                                        <th>Designation</th>
                                        <th class="not_include">First Name</th>
                                        <th class="not_include">Last Name</th>
                                        <th>Employment Status</th>
                                        <th class="not_include">Department</th>
                                        <th>Shift Type</th>
                                        <th>In Time</th>
                                        <th>Out Time</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section> --}}
        {{-- </div> --}}
    </section>
@endsection
@section('scripts')
    <script>
        var dept_id = $('#department_filter').val();
        var date = $('#date_filter').val();
        function all_dataTable(clicked_id) {
            var url = "{{route('attendance_dashboard.create')}}" + "?type=" + clicked_id + "&department=" + dept_id + "&date=" + date;
            window.location.href = url; 
            // $('#basic-dataTable').removeClass('d-none');
            // $('#all_dataTable').DataTable({
            //     processing: true,
            //     serverSide: true,
            //     responsive: true,
            //     destroy: true,
            //     ajax: {
            //         url: "{{ route('attendance_dashboard.create') }}?type=" + clicked_id + "&department=" + dept_id + "&date=" + date,
            //         data: {
            //             type: clicked_id,
            //             department: dept_id,
            //             date: date,
            //         },
            //     },
            //     columns: [{
            //             data: 'responsive_id'
            //         },
            //         {
            //             "title": "Sr.No",
            //             render: function(data, type, row, meta) {
            //                 return meta.row + meta.settings._iDisplayStart + 1;
            //             }
            //         },
            //         {
            //             data: 'employee_id',
            //             name: 'employees.employee_id',
            //             visible: false
            //         },
            //         {
            //             data: 'employee_code',
            //             name: 'employees.employee_code',
            //             visible: false
            //         },
            //         {
            //             data: 'full_name',
            //             render: function(data, type, row) {
            //                 return `${row.first_name} ${row.last_name}`;
            //             },
            //             visible: false,
            //             searchable: false
            //         },
            //         {
            //             data: 'name',
            //             render: function(data, type, row) {
            //                 return `${row.employee_id}-${row.employee_code}-${row.first_name} ${row.last_name}`;
            //             },
            //             searchable: false
            //         },
            //         {
            //             data: 'designation',
            //             name: 'jobtitles.name'
            //         },
            //         {
            //             data: 'first_name',
            //             name: 'employees.first_name',
            //             searchable: true,
            //             visible: false
            //         },
            //         {
            //             data: 'last_name',
            //             name: 'employees.last_name',
            //             searchable: true,
            //             visible: false
            //         },
            //         {
            //             data: 'employment',
            //             name: 'employmentstatus.name'
            //         },
            //         {
            //             data: 'department' , render: function(data , type , row){
            //                 return `
            //                     <label style="font-size: 12px;" >Parent:</label>
            //                     <span style="color: green;">
            //                         ${row.parent ? row.parent : " " }
            //                     </span>
            //                     <br>
            //                     <label style="font-size: 12px;">Sub Unit Branch:</label>
            //                     <span style="color: orange">
            //                         ${row.department}
            //                     </span>
            //                 `
            //             }
            //         },
            //         {
            //             data: 'shift_desc',
            //             name: 'shift_type.shift_desc'
            //         },
            //         {
            //             data: 'in_time',
            //             name: 'attendance.in_time'
            //         },
            //         {
            //             data: 'out_time',
            //             name: 'attendance.out_time'
            //         },
            //     ],
            //     "columnDefs": [{
            //             // For Responsive
            //             className: 'control',
            //             orderable: false,
            //             searchable: false,
            //             targets: 0
            //         },
            //         {
            //             "defaultContent": "-",
            //             "targets": "_all"
            //         }
            //     ],
            //     "order": [
            //         [0, 'asc']
            //     ],
            //     dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            //     displayLength: 10,
            //     aLengthMenu: [
            //         [10,25,50,100,-1],
            //         [10,25,50,100,"All"]
            //     ],
            //     iDisplayLength: -1,
            //     buttons: [{
            //             extend: 'collection',
            //             className: 'btn btn-outline-secondary dropdown-toggle me-2',
            //             text: feather.icons['share'].toSvg({
            //                 class: 'font-small-4 me-50'
            //             }) + 'Export',
            //             buttons: [{
            //                     extend: 'print',
            //                     text: feather.icons['printer'].toSvg({
            //                         class: 'font-small-4 me-50'
            //                     }) + 'Print',
            //                     className: 'dropdown-item',
            //                     title: 'Employees',
            //                     action: newexportaction,
                                // exportOptions: {
            //                         columns: ':not(.not_include)'
            //                     }
            //                 },
            //                 {
            //                     extend: 'csv',
            //                     text: feather.icons['file-text'].toSvg({
            //                         class: 'font-small-4 me-50'
            //                     }) + 'Csv',
            //                     className: 'dropdown-item',
            //                     title: 'Employees',
            //                     action: newexportaction,
                                // exportOptions: {
            //                         columns: ':not(.not_include)'
            //                     }
            //                 },
            //                 {
            //                     extend: 'excel',
            //                     text: feather.icons['file'].toSvg({
            //                         class: 'font-small-4 me-50'
            //                     }) + 'Excel',
            //                     className: 'dropdown-item',
            //                     title: 'Employees',
            //                     action: newexportaction,
                                // exportOptions: {
            //                         columns: ':not(.not_include)'
            //                     }
            //                 },
            //                 {
            //                     extend: 'pdf',
            //                     text: feather.icons['clipboard'].toSvg({
            //                         class: 'font-small-4 me-50'
            //                     }) + 'Pdf',
            //                     className: 'dropdown-item',
            //                     orientation: 'landscape',
            //                     pageSize: 'LEGAL',
            //                     title: 'Employees',
            //                     action: newexportaction,
                                // exportOptions: {
            //                         columns: ':not(.not_include)'
            //                     }
            //                 },
            //                 {
            //                     extend: 'copy',
            //                     text: feather.icons['copy'].toSvg({
            //                         class: 'font-small-4 me-50'
            //                     }) + 'Copy',
            //                     className: 'dropdown-item',
            //                     title: 'Employees',
            //                     action: newexportaction,
                                // exportOptions: {
            //                         columns: ':not(.not_include)'
            //                     }
            //                 }
            //             ],
            //             init: function(api, node, config) {
            //                 $(node).removeClass('btn-secondary');
            //                 $(node).parent().removeClass('btn-group');
            //                 setTimeout(function() {
            //                     $(node).closest('.dt-buttons').removeClass('btn-group')
            //                         .addClass('d-inline-flex');
            //                 }, 50);
            //             }
            //         }
            //     ],
            //     responsive: {
            //         details: {
            //             display: $.fn.dataTable.Responsive.display.childRowImmediate,
            //             type: 'column',
            //         }
            //     },
            //     language: {
            //         paginate: {
            //             // remove previous & next text from pagination
            //             previous: ,
            //             next: '&nbsp;'
            //         }
            //     }
            // });
            // $('div.head-label').html('<h6 class="mb-0">List of '+clicked_id+'</h6>');
        }
        $(document).on('select2:open', () => {
            document.querySelector('.select2-search__field').focus();
        });
        $("#department").change(function() {
            var optVal = $(this).val();
            if (optVal.length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_department_child') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#section').empty();
                        $('#section').html('<option value="">Select Section</option>'); 
                        $.each(response, function(index, value) {
                            
                            console.log(value.id);
                            $('#section').append(
                                $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                    value.title)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            } else {
                $('#section').empty();
            }
        });
    </script>
@endsection
