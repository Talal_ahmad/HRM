@extends('Admin.layouts.master')
@section('title', 'Leave Report')
@section('style')
    <style>
        tr {
            border: 1px solid black;
        }

        thead tr>th {
            vertical-align: middle !important;
        }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Leave_Report')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Leave_Report')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <!--Search Form -->
                            <div class="card-body">
                                <form id="search_form">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-label">@lang('app.Departments')</label>
                                            <select name="departmentFilter" id="departmentFilter"
                                                class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all"
                                                    {{ request('departmentFilter') == 'all' ? 'selected' : '' }}>All
                                                </option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}"
                                                        {{ request('departmentFilter') == $department->id ? 'selected' : '' }}>
                                                        {{ $department->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Employee')">
                                                <option value=""></option>
                                                @if (isset($departmentEmployees))
                                                    @foreach ($departmentEmployees as $employee)
                                                        <option value="{{ $employee->id }}"
                                                            {{ request('employeeFilter') == $employee->id ? 'selected' : '' }}>
                                                            {{ $employee->employee_id . '-' . $employee->employee_code . '-' . $employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name . ' - ' . $employee->designation }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">@lang('app.Leave_Periods')</label>
                                            <select name="periodFilter" id="periodFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Leave_Period')" required>
                                                <option value=""></option>
                                                @foreach ($periods as $period)
                                                    <option value="{{ $period->id }}"
                                                        {{ request('periodFilter') == $period->id ? 'selected' : '' }}>
                                                        {{ $period->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <button onclick="divToPrint()"
                                                class="btn btn-primary mt-1 printre">@lang('app.Print')</button>
                                        </div>
                                        <div class="col-md-6 text-end">
                                            <a href="{{ url('leave_report') }}" type="button"
                                                class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                            <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card p-2 table-responsive" id="spot1">
                            <div id="div_to_print">
                                <table class="table table-bordered" style="border: 1px solid black; white-space:nowrap;">
                                    <thead>
                                        <tr rowspan="2" class="text-center">
                                            <th style="font-size: 20px" colspan="{{(count($leave_types) * 3) + 5}}">{{ env('COMPANY') }} (PVT) Ltd</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">Employee</th>
                                            <th rowspan="2">Department</th>
                                            <th colspan="{{ count($leave_types) + 1 }}" class="text-center">Entitled</th>
                                            <th colspan="{{ count($leave_types) + 1 }}" class="text-center">Availed</th>
                                            <th colspan="{{ count($leave_types) + 1 }}" class="text-center">Balance</th>
                                        </tr>
                                        <tr>
                                            @foreach ($leave_types as $leave)
                                                <th style="padding: 0; text-align:center;">{{ $leave->abbreviation }}</th>
                                            @endforeach
                                            <th style="padding: 0; text-align:center;">Total</th>
                                            @foreach ($leave_types as $leave)
                                                <th style="padding: 0; text-align:center;">{{ $leave->abbreviation }}</th>
                                            @endforeach
                                            <th style="padding: 0; text-align:center;">Total</th>
                                            @foreach ($leave_types as $leave)
                                                <th style="padding: 0; text-align:center;">{{ $leave->abbreviation }}</th>
                                            @endforeach
                                            <th style="padding: 0; text-align:center;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($employees) && count($employees) > 0)
                                            @foreach ($employees as $employee)
                                                @php
                                                    $total_entitled = 0;
                                                    $total_availed = 0;
                                                @endphp
                                                <tr>
                                                    <td>{{ $employee->first_name }} {{ $employee->middle_name }}
                                                        {{ $employee->last_name }}
                                                        <br>{{ HandleEmpty($employee->employee_code) }}<br>{{ $employee->designation }}
                                                    </td>
                                                    <td style="padding: 0;">{{ $employee->department }}</td>
                                                    @foreach ($leave_types as $type)
                                                        @php
                                                            $leave_record = checkLeaveBalance($employee->id, $type->id, request('periodFilter'));
                                                            $total_entitled += $leave_record['total_balance'];
                                                        @endphp
                                                        <td>{{ $leave_record['total_balance'] }}</td>
                                                    @endforeach
                                                    <td style="padding: 0; text-align:center;">{{ $total_entitled }}</td>
                                                    @foreach ($leave_types as $type)
                                                        @php
                                                            $leave_record = checkLeaveBalance($employee->id, $type->id, request('periodFilter'));
                                                            $total_availed += $leave_record['leaves_taken'];
                                                        @endphp
                                                        <td>{{ $leave_record['leaves_taken'] }}</td>
                                                    @endforeach
                                                    <td style="padding: 0; text-align:center;">{{ $total_availed }}</td>
                                                    @foreach ($leave_types as $type)
                                                        @php
                                                            $leaveDetail = checkLeaveBalance($employee->id, $type->id, request('periodFilter'));
                                                        @endphp
                                                        <td>{{ $leaveDetail['total_balance'] - $leaveDetail['leaves_taken'] }}</td>
                                                    @endforeach
                                                    <td style="padding: 0; text-align:center;">{{ $total_entitled - $total_availed }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="{{(count($leave_types) * 3) + 5}}" class="text-center">No Record Found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employeeFilter').empty();
                            $('#employeeFilter').html(
                                '<option value="">Select Employee</option>');
                            $.each(response, function(index, value) {
                                $('#employeeFilter').append(
                                    $('<option></option>').val(value.id).html(
                                        value.employee_id + '-' + value
                                        .employee_code + '-' + value.first_name +
                                        ' ' + value.middle_name + ' ' + value
                                        .last_name + ' - ' + value.designation)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employeeFilter').empty();
                }
            });
        });

        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.write(`<!DOCTYPE html>`);
            WinPrint.document.write(`<head>`);
            WinPrint.document.write(`<link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">`);
            WinPrint.document.write(`<style>
                            *{  
                                padding: 0;
                                color: black;
                            }
                            table tr{
                                border: 1px solid black;
                            }
                            `);
            WinPrint.document.write(`</style>`);
            WinPrint.document.write(`</head><body>`);
            WinPrint.document.write(`${divToPrint.innerHTML}`);
            WinPrint.document.write(`</body></html>`);
            WinPrint.document.close();
            setTimeout(function() {
                WinPrint.print();
                WinPrint.close();
            }, 1000);
            WinPrint.focus();
        }
    </script>
@endsection
