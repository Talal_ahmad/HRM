@extends('Admin.layouts.master')
@section('title', 'Provident Fund Report')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Provident_Fund_Report')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Provident_Fund_Report')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{url('providentFundReport')}}">
                                <input type="hidden" name="type" value="pf_fund">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" id="calculation_group">@lang('app.Filter_By_Calculation_Group'):</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="@lang('app.Select_Calculation_Group')" required>
                                            <option value=""></option>
                                            @foreach ($calculation_groups as $calculation_group)
                                                <option value="{{ $calculation_group->id }}" {{$calculation_group->id == request('calculation_group') ? 'selected' : ''}}>{{ $calculation_group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="date">@lang('app.Filter_By_Month')</label>
                                        <input type="month" id="date" class="form-control" name="date" value="{{request('date')}}" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">@lang('app.Print')</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{url('providentFundReport')}}?type=pf_fund" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @php
                        $grand_total = [];
                    @endphp
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                <button type="button" class="btn btn-success export" id="excel">@lang('app.Download_Excel')</button>
                                <button style="margin-left: 6px;" type="button" class="btn btn-danger pdf" id="pdf">@lang('app.Download_Pdf')</button>
                            </div>
                        </div>
                    </div> 
                    <div id="div_to_print">
                        <div class="card">
                            <div class="row p-1">
                                <div class="col-md-12 d-flex">
                                    <div>
                                        <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
                                    </div>
                                    <div style="width: 60%">
                                        <h4 style="font-weight: bold;font-size: 24px;margin: 24px 0 0 17px;">{{$company_setup->company_name}}, SCHEDULE OF Provident Fund CONTRIBUTION</h4>
                                    </div>
                                </div>
                                <div class="col-md-9 text-end">
                                    <p style="font-weight: 600;font-size:20px;">F.M : &emsp;<span style="text-decoration: underline;text-decoration-thickness: 3px;"><?php echo $year."-".$month; ?></span></p>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr</th>
                                            <th>ID</th>
                                            <th>Employee Code</th>
                                            <th>Name of IP</th>
                                            <th>Father's Name</th>
                                            <th>Desigination</th>
                                            {{-- <th>CNIC</th> --}}
                                            @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                                @foreach ($payroll_columns as $column)
                                                    @php
                                                        $grand_total[$column->payroll_column_id] = 0;
                                                    @endphp
                                                    <th>{{$column->name}}</th>
                                                @endforeach
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($departments) && count($departments) > 0)
                                            @php
                                                $sr_no = 1;
                                            @endphp
                                            @foreach ($departments as $department)
                                                @if (count($employees[$department->id]) > 0)
                                                    <tr>
                                                        <td colspan="{{7+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                                                    </tr>
                                                    @foreach ($employees[$department->id] as $employee)
                                                        <tr>
                                                            <td>{{$sr_no++}}</td>
                                                            <td>{{$employee->employee_id}}</td>
                                                            <td>{{HandleEmpty($employee->employee_code)}}</td>
                                                            <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                                                            <td>{{$employee->father_name}}</td>
                                                            <td>{{$employee->desigination}}</td>
                                                            {{-- <td style="white-space: nowrap">{{preg_replace('~.*(\d{5})[^\d]{0,7}(\d{7})[^\d]{0,7}(\d{1}).*~', '$1-$2-$3', $employee->nic_num)}}</td> --}}
                                                            @foreach ($payroll_columns as $col)
                                                                <td>{{payrollColumnAmount_pf($calculation_group_payroll->payroll_id,$employee->employee,$col->payroll_column_id,$col->is_decimal)}}</td>
                                                            @endforeach
                                                        </tr>
                                                    @endforeach
                                                <tr>
                                                    <td colspan="6" style="font-weight: bold;text-align:end">Sub Total:</td>
                                                    @foreach ($columns_totals[$department->id] as $item)
                                                        @php
                                                            $grand_total[$item->payroll_item] += $item->amount;
                                                        @endphp
                                                        @if ($item->is_decimal == 1)
                                                            <td>{{number_format($item->amount,1)}}</td>
                                                        @else
                                                            <td>{{number_format($item->amount)}}</td>
                                                        @endif
                                                    @endforeach
                                                </tr>
                                                @endif
                                            @endforeach
                                        @else
                                            <tr>
                                                <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="7">No Record Found!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                    <tfoot>
                                        <tr>
                                            <td colspan="6" style="font-weight: bold;text-align:end">Grand Total</td>
                                            @foreach ($grand_total as $total)
                                                <td>{{number_format($total)}}</td>
                                            @endforeach
                                        </tr>
                                    </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                        <div class="row mb-1" id="signatures">
                            <div id="sign1" class="col-4 d-flex flex-column">
                                <input type="text" value="{{$company_setup->payroll_fName}} {{$company_setup->payroll_lName}}" style="border-width: 0 0 1px 0;" class="sign text-center">
                                <label for="sign1" class="text-center">Payroll Officer</label>
                            </div>
                            <div id="sign2" class="col-4 d-flex flex-column">
                                <input type="text" style="border-width: 0 0 1px 0;" class="sign text-center">
                                <label for="sign2" class="text-center">A.M. (H.R)</label>
                            </div>
                            <div id="sign2" class="col-4 d-flex flex-column">
                                <input type="text" value="{{$company_setup->first_name}} {{$company_setup->last_name}}" style="border-width: 0 0 1px 0;" class="sign text-center">
                                <label for="sign2" class="text-center">D.G.M (HR & Comp.),JSML</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var date = $('#date').val();
            $.ajax({
                url: "{{url('pessiMonthlyReport')}}?type=pf_fund",
                type: "GET",
                data: {
                    export_type,
                    calculation_group,
                    date,
                },
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        $(document).on('click', '.pdf', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var employment_status = $('#employment_status').val();
            var banksFilter = $('#banksFilter').val();
            var departmentFilter = $('#departmentFilter').val();
            var date = $('#date').val();
            $.ajax({
                url: "{{url('pessiMonthlyReport')}}?type=pf_fund",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    date:date,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.write(`<!DOCTYPE html>
                    <head>
                        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
                    </head>
                    <body>
                    ${WinPrint.document.write(divToPrint.outerHTML)}
                    </body>
                    </html>
                    `)
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
@endsection
