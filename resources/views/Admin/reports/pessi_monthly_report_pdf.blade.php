<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pessi Monthly Report</title>
    <head>
        <style>
            #salary_sheet {
                border-collapse: collapse;
                width: 100%;
                font-family:'Segoe UI',sans-serif
            }

            #salary_sheet td, #salary_sheet th {
                border: 1px solid #ddd;
                padding: 5px;
                font-size: 100px;
                width: 5px;
            }

            #salary_sheet td{
                padding: 30px;
            }
            h1 {
                font-size: 200px;
                text-align: center;
                color: black;
                /* background-color: #356e9c; */
            }
            #salary_sheet tr:nth-child(even){background-color: #f2f2f2;}

            #salary_sheet tr:hover {background-color: #ddd;}

            #salary_sheet th {
                padding-top: 12px;
                padding-bottom: 12px;
                background-color: #2e475c;
                color: white;
            }
            img {
                height: 700px;
                width: 20%;
            }
            .txt_rgt{
                text-align: right;
            }
        </style>
    </head>
</head>
<body>
    <div>
        @if (!empty($company_setup->logo))
            <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
        @else
            <img src="{{asset('images/company_logo/')}}" alt="Logo">
            
        @endif
        @if (request()->type != 'pf_fund')
            <h1>Pessi Monthly Report</h1>
        @else
            <h1>Provident Fund Report</h1>
        @endif
        <table class="table table-bordered table-striped" id="salary_sheet">
            <thead>
                <tr>
                    @if (request()->type != 'pf_fund')
                        <th>Month : {{$date}}</th>
                        <th colspan="3">Calculation Group : {{$cal_group}}</th>
                        <th colspan="3">Printed By : {{auth()->user()->username}}</th>
                        <th colspan="3">Printed At : {{date('Y-m-d H:i:s')}}</th>
                    @else
                        <th>Month : {{$date}}</th>
                        <th colspan="2">Calculation Group : {{$cal_group}}</th>
                        <th colspan="2">Printed By : {{auth()->user()->username}}</th>
                        <th colspan="2">Printed At : {{date('Y-m-d H:i:s')}}</th>
                    @endif
                </tr>
                <tr>
                    <th>Sr</th>
                    <th>ID</th>
                    <th>Employee Code</th>
                    <th>Name of IP</th>
                    <th>Father's Name</th>
                    <th>Desigination</th>
                    @if (request()->type != 'pf_fund')
                        <th>CNIC</th>
                    @endif
                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                        @foreach ($payroll_columns as $column)
                            @php
                                $grand_total[$column->payroll_column_id] = 0;
                            @endphp
                            <th>{{$column->name}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                    @php
                        $sr_no = 1;
                        $gr_total_employees = 0;
                    @endphp
                    @foreach ($departments as $department)
                        @if (count($employees[$department->id]) > 0)
                            @php
                                $net_total_employees = count($employees[$department->id]);
                                $gr_total_employees += count($employees[$department->id]);
                            @endphp
                            <tr>
                                @if (request()->type != 'pf_fund')
                                    <td colspan="{{7+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                                @else
                                    <td colspan="5" style="font-weight: bold">{{$department->title}}</td>
                                @endif
                            </tr>
                            @foreach ($employees[$department->id] as $employee)
                                <tr>
                                    <td>{{$sr_no++}}</td>
                                    <td>{{$employee->employee_id}}</td>
                                    <td>{{HandleEmpty($employee->employee_code)}}</td>
                                    <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                                    <td>{{$employee->father_name}}</td>
                                    <td>{{$employee->desigination}}</td>
                                    @if (request()->type != 'pf_fund')
                                        <td style="white-space: nowrap">{{preg_replace('~.*(\d{5})[^\d]{0,7}(\d{7})[^\d]{0,7}(\d{1}).*~', '$1-$2-$3', $employee->nic_num)}}</td>
                                    @endif
                                    @foreach ($payroll_columns as $col)
                                        <td>{{payrollColumnAmount($payrollIds,$employee->employee,$col->payroll_column_id,$col->is_decimal,$request->employment_status)}}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        <tr>
                            @if (request()->type != 'pf_fund')
                                <td style="font-weight: bold;" colspan="3">Sub Total Emp.:</td>
                                <td>{{$net_total_employees}}</td>
                                <td colspan="3" style="font-weight: bold;text-align:right">Sub Total:</td>
                            @else
                                <td style="font-weight: bold;" colspan="4">Sub Total Emp.:</td>
                                <td>{{$net_total_employees}}</td>
                                <td style="font-weight: bold;text-align:right">Sub Total:</td>
                            @endif
                            @foreach ($columns_totals[$department->id] as $item)
                                @php
                                    $grand_total[$item->payroll_item] += $item->amount;
                                @endphp
                                @if ($item->is_decimal == 1)
                                    <td>{{number_format($item->amount,1)}}</td>
                                @else
                                    <td>{{number_format($item->amount)}}</td>
                                @endif
                            @endforeach
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="7">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
            @if (isset($payroll_columns) && count($payroll_columns) > 0)
            <tfoot>
                <tr>
                    @if (request()->type != 'pf_fund')
                        <td colspan="3" style="font-weight: bold;">Grand Total Emp.:</td>
                        <td>{{$gr_total_employees}}</td>
                        <td colspan="3" style="font-weight: bold;text-align:right;">Grand Total</td>
                        @foreach ($grand_total as $total)
                            <td>{{number_format($total)}}</td>
                        @endforeach
                    @else
                        <td colspan="4" style="font-weight: bold;">Grand Total Emp.:</td>
                        <td>{{$gr_total_employees}}</td>
                        <td style="font-weight: bold;text-align:right;">Grand Total</td>
                        @foreach ($grand_total as $total)
                            <td>{{number_format($total)}}</td>
                        @endforeach
                    @endif
                </tr>
            </tfoot>
            @endif
        </table>
    </div>
</body>
</html>