<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EOBI Monthly Summary</title>
</head>
<body>
    <div>
        @php
            $grand_total = [];
        @endphp
        <table border=".5pt">
            <thead>
                <tr>
                    <th style="text-align: center;" colspan="2">Month : {{$date}}</th>
                    <th style="text-align: center;" colspan="2">Calculation Group : {{$cal_group}}</th>
                    <th style="text-align: center;" colspan="2">Printed By : {{auth()->user()->username}}</th>
                    <th style="text-align: center;" colspan="2">Printed At : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    <th>Sr.</th>
                    @if (env('COMPANY') == 'JSML')        
                        <th>Section</th>
                    @else
                        <th>Department</th>
                    @endif
                    <th>Head Count</th>
                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                        @foreach ($payroll_columns as $column)
                            @php
                                $grand_total[$column->payroll_column_id] = 0;
                            @endphp
                            <th>{{$column->name}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                    @php
                        $sr_no = 1;
                        $net_total_employees = 0;
                    @endphp
                    @foreach ($departments as $key => $department)
                        @if (count($total_employees[$department->id]) > 0)
                        @php
                            $net_total_employees += count($total_employees[$department->id]);
                        @endphp
                        <tr>
                            <td>{{$sr++}}</td>
                            <td>{{$department->title}}</td>
                            <td>{{count($total_employees[$department->id])}}</td>
                            @foreach ($columns_totals[$department->id] as $total)
                                @php
                                    $grand_total[$total->payroll_item] += $total->amount;
                                @endphp
                                <th>{{round($total->amount)}}</th>
                            @endforeach
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="3">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
            @if (isset($payroll_columns) && count($payroll_columns) > 0)
            <tfoot>
                <tr>
                    <td colspan="2" style="font-weight: bold;text-align:end">Totals:</td>
                    <td>{{$net_total_employees}}</td>
                    @foreach ($grand_total as $total)
                        <td>{{round($total)}}</td>
                    @endforeach
                </tr>
            </tfoot>
            @endif
        </table>
    </div>
</body>
</html>