<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Department Wise Salary Summary</title>
    <head>
        <style>
            #salary_sheet {
                border-collapse: collapse;
                width: 100%;
                font-family:'Segoe UI',sans-serif
            }

            #salary_sheet td, #salary_sheet th {
                border: 1px solid #ddd;
                padding: 5px;
                font-size: 45px;
                width: 5px;
            }

            #salary_sheet td{
                padding: 30px;
            }
            h1 {
                font-size: 200px;
                text-align: center;
                color: black;
            }
            #salary_sheet tr:nth-child(even){background-color: #f2f2f2;}

            #salary_sheet tr:hover {background-color: #ddd;}

            #salary_sheet th {
                padding-top: 12px;
                padding-bottom: 12px;
                background-color: #2e475c;
                color: white;
            }
            img {
                height: 700px;
                width: 20%;
            }
            .txt_rgt{
                text-align: right;
            }
        </style>
    </head>
</head>
<body>
    <div>
        @if (!empty($company_setup->logo))
            <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
        @else
            <img src="{{asset('images/company_logo/')}}" alt="Logo">
            
        @endif
        <h1>Department Wise Salary Summary</h1>
        <table class="table table-bordered table-striped" id="salary_sheet">
            <thead>
                <tr>
                    @php
                        $sum_id=count($payroll_columns);
                    @endphp
                    <th colspan="3">Month : {{$date}} <br>Calculation Group : {{$cal_group}} <br>Printed By : {{auth()->user()->username}}</th>
                    {{-- <th colspan="">Calculation Group : {{$cal_group}}</th>
                    <th >Printed By : {{auth()->user()->username}}</th> --}}
                    <th colspan="{{$sum_id}}">Printed At : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    <th>Sr.No</th>
                    @if (env('COMPANY') == 'JSML')        
                        <th>Section</th>
                    @else
                        <th>Department</th>
                    @endif
                    <th>Total Employees</th>
                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                        @foreach ($payroll_columns as $column)
                            @php
                                $grand_total[$column->id] = 0;
                            @endphp
                            <th>{{$column->name}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                    @php
                        $sr_no = 1;
                        $net_total_employees = 0;
                        $g_total_employees = 0;
                    @endphp
                    @foreach ($departments as $department)
                    @php
                        $parent_total_employees = 0;
                    @endphp
                        @foreach ($all_childrens[$department->id] as $child)
                            @if (count($total_employees[$child->id]) > 0)
                            @php
                                $net_total_employees = count($total_employees[$child->id]);
                                $parent_total_employees += count($total_employees[$child->id]);
                                $g_total_employees += count($total_employees[$child->id]);
                            @endphp
                                <tr>
                                    <td>{{$sr_no++}}</td>
                                    <td colspan="">{{$child->title}}</td>
                                    <td>{{$net_total_employees}}</td>
                                    @foreach ($columns_totals[$child->id] as $item)
                                        @php
                                            $grand_total[$item->payroll_item] += $item->amount;
                                        @endphp
                                        <td style="text-align: right;">{{number_format($item->amount)}}</td>
                                    @endforeach
                                </tr>
                            @endif
                        @endforeach
                        @if (count($parent_totals[$department->id]) > 0)
                        <tr>
                            <td colspan="2" style="font-weight: bold;text-align:right">{{$department->title}} Total:</td>
                                <td>
                                    {{$parent_total_employees}}
                                </td>
                                @foreach ($parent_totals[$department->id] as $parent)
                                    <td style="font-weight: bold;text-align:right">{{number_format($parent->amount)}}</td>
                                @endforeach
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="10">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
            @if (isset($payroll_columns) && count($payroll_columns) > 0 && count($departments) > 0 && !empty($all))
            <tfoot>
                <tr>
                    <td colspan="2" style="font-weight: bold;text-align:end">Total</td>
                    <td>{{$g_total_employees}}</td>
                    @foreach ($grand_total as $total)
                        <td style="text-align: right;">{{number_format($total)}}</td>
                    @endforeach
                </tr>
            </tfoot>
            @endif
        </table>
    </div>
</body>
</html>