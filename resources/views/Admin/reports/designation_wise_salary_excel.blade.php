<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EOBI Monthly Report</title>
</head>
<body>
    <div>
        @php
            $grand_total = [];
        @endphp
        <table border=".5pt">
            <thead>
                <tr>
                    <th style="text-align: center;" colspan="2">Month : {{$date}}</th>
                    <th style="text-align: center;" colspan="2">Printed By : {{auth()->user()->username}}</th>
                    <th style="text-align: center;" colspan="2">Printed At : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    <th>Sr.No</th>
                    <th>Employee ID</th>
                    <th>Employee Code</th>
                    <th>Employee</th>
                    <th>Department</th>
                    <th>Designation</th>
                    <th>Employement Status</th>
                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                        @foreach ($payroll_columns as $column)
                            <th>{{$column->name}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0 )
                    @foreach ($departments as $key => $department)
                        @if (isset($employees[$department->id]) && count($employees[$department->id]) > 0)
                            <tr>
                                <td colspan="{{7+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                            </tr>
                            @foreach ($employees[$department->id] as $key2 => $employee)
                                <tr>
                                    <td>{{$key2+1}}</td>
                                    <td>{{$employee->employee_id}}</td>
                                    <td>{{$employee->employee_code}}</td>
                                    <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                                    <td>{{$employee->department}}</td>
                                    <td>{{$employee->job_title}}</td>
                                    <td>{{$employee->employement_status}}</td>
                                    @foreach ($payroll_columns as $col)
                                        <td class="text-end">{{payrollColumnAmount($employee->payroll,$employee->employee,$col->id,0,$col->round_off)}}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                            <tr>
                                {{-- <td colspan="7" style="font-weight: bold;text-align:end">Total:</td> --}}
                                @foreach ($payroll_columns as $column)
                                @php
                                    $total = '-';
                                    if(!empty($net_total[$department->id][$column->id]))
                                    {
                                        $total = number_format($net_total[$department->id][$column->id]);
                                    }
                                @endphp
                                {{-- <td class="text-end">{{$total}}</td> --}}
                                @endforeach
                            </tr>
                            @else
                                <tr>
                                    {{-- <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="{{7+count($payroll_columns)}}">No Record Found!</td> --}}
                                </tr>
                            @endif
                        @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="8">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</body>
</html>