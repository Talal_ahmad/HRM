@extends('Admin.layouts.master')
@section('title', 'UIF Report')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">UIF Report</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">UIF Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{url('uifReport')}}">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" id="calculation_group">Calculation Group:</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="Select Calculation Group" required>
                                            <option value=""></option>
                                            @foreach ($calculation_groups as $calculation_group)
                                                <option value="{{ $calculation_group->id }}" {{$calculation_group->id == request('calculation_group') ? 'selected' : ''}}>{{ $calculation_group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" multiple>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? in_array($department->id,request('departmentFilter')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#departmentFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Department</label>
                                            <select name="departmentFilter" id="department" class="select2 form-select" data-placeholder="Select Department" required>
                                                <option value="">Select Department</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? $department->id == request('departmentFilter') ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Section</label>
                                            <select name="section[]" id="section" data-placeholder="Select Section" class="select2 form-select" multiple required>
                                                @if (!empty(request('departmentFilter')))
                                                    @foreach ($parentvar as $item)
                                                        <option value="{{$item->id}}" {{!empty(request('section')) ? in_array($item->id,request('section')) ? 'selected' : '' : ''}}>{{$item->title}}</option>
                                                    @endforeach
                                                @endif 
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#section')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#section')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-3">
                                        <label class="form-label" for="employeeFilter">Employee</label>
                                        <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="Select Employee">
                                            <option value=""></option> --}}
                                            {{-- @foreach (employees() as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach --}}
                                        {{-- </select>
                                    </div> --}}
                                    {{-- <div class="col-md-3 col-12">
                                        <label class="form-label" id="bank_status">Bank:</label>
                                        <select name="bank_status" id="bank_status" class="select2 form-select" data-placeholder="Select Bank Status">
                                            <option value=""></option>
                                                <option value="All" {{request('bank_status') == 'All' ? 'selected' : ''}}>All</option>
                                                <option value="Cash" {{request('bank_status') == 'Cash' ? 'selected' : ''}}>Cash</option>
                                                <option value="Bank" {{request('bank_status') == 'Bank'? 'selected' : ''}}>Bank</option>
                                        </select>
                                    </div> --}}
                                    {{-- <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employment_status">Employment Status</label>
                                            <select name="employment_status[]" id="employment_status" class="select2 form-select" data-placeholder="Select Employment Status" multiple>
                                                <option value=""></option>
                                                @foreach ($employment_status as $employment)
                                                    <option value="{{$employment->id}}" {{!empty(request('employment_status')) ? in_array($employment->id,request('employment_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#employment_status')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#employment_status')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="banksFilter">Filter By Banks:</label>
                                            <select name="banksFilter[]" id="banksFilter" class="select2 form-select" data-placeholder="Select Banks" multiple>
                                                <option value=""></option>
                                                @foreach ($bank_name as $banks)
                                                    <option value="{{$banks->id}}" {{!empty(request('banksFilter')) ? in_array($banks->id,request('banksFilter')) ? 'selected' : '' : ''}}>{{$banks->name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#banksFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#banksFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div> --}}


                                    {{-- <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="banksFilter">Filter By Banks</label>
                                            <select name="banksFilter" id="banksFilter" class="select2 form-select" data-placeholder="Select Bank">
                                                <option value=""></option>
                                                <option value="all" {{request('banksFilter') == 'all' ? 'selected' : ''}}>All</option>
                                                @foreach ($bank_name as $banks)
                                                    <option value="{{ $banks->id }}" {{$banks->id == request('banksFilter') ? 'selected' : ''}}>{{ $banks->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-md-3 col-12">
                                        <label class="form-label" id="order_filter">Filter By Order:</label>
                                        <select name="order_filter" id="order_filter" class="select2 form-select" data-placeholder="Select Any Order">
                                            <option value=""></option>
                                                <option value="employee_id" {{request('order_filter') == 'employee_id' ? 'selected' : ''}}>By Employee ID</option>
                                                <option value="employee_code" {{request('order_filter') == 'employee_code'? 'selected' : ''}}>By Employee Code</option>
                                        </select>
                                    </div> --}}
                                    {{-- <div class="col-md-3 col-12">
                                        <label class="form-label" for="date">Filter By Month</label>
                                        <input type="month" id="date" class="form-control" name="date" value="{{request('date')}}" required/>
                                    </div> --}}
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="fromDate">From Date:</label>
                                        <input type="month" id="fromDate" class="form-control date"  name="fromDate" value="{{empty(request('fromDate')) ? date('Y-m') : request('fromDate')}}" required/>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="toDate">To Date:</label>
                                        <input type="month" id="toDate" class="form-control date"  name="toDate" value="{{empty(request('toDate')) ? date('Y-m') : request('toDate')}}" required/>
                                    </div>
                                    {{-- <div class="col-md-3 col-12">
                                        <label class="form-label" for="singleDate">Filter By Date:</label>
                                        <input type="text" name="toDate" id="toDate" class="form-control flatpickr-basic toDate" placeholder="YYYY-MM-DD" required>
                                    </div> --}}
                                    {{-- <div class="col-12 col-md-6">
                                        <label class="form-label" for="fromDate">From Date:</label>
                                        <input type="text" name="fromDate" id="fromDate" class="form-control flatpickr-basic" value="{{request('fromDate')}}" placeholder="YYYY-MM-DD" required>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label class="form-label" for="toDate">To Date:</label>
                                        <input type="text" name="toDate" id="toDate" class="form-control flatpickr-basic" value="{{request('toDate')}}" placeholder="YYYY-MM-DD" required>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">Print</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{url('uifReport')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @php
                        // $grand_total = [];
                    @endphp
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                @if (!empty($calculation_group_payroll))
                                <button type="button" class="btn btn-success ml-2 export" id="excel">Download Excel</button>
                                <button style="margin-left: 6px;" type="button" class="btn btn-danger pdf" id="pdf">Download Pdf</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card">
                            <div class="row p-1">
                                <div class="col-md-12">
                                    <h4 style="text-align: start;font-size:16px;font-weight: bold;margin: 10px 0px 0px 0px;">UIF Report</h4>
                                </div>
                            </div>
                            <div class="row px-1">
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Company : Homegrown Brokers (PTY) Ltd</p>
                                </div>
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Unit: All Units</p>
                                </div>
                            </div>
                            <div class="row px-1">
                                <div class="col-md-4">
                                    <p style= "font-weight: 600;font-size:14px;">Effective Date :
                                         @if (!empty($fromDate))
                                             {{$fromDate}}
                                            - {{$toDate}}
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Level : All Levels</p>
                                </div>
                            </div>
                            <div class="row px-1">
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Employee : All Employee</p>
                                </div>
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Position: All Positions</p>
                                </div>
                            </div>
                            <div class="row px-1">
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Frequency : Monthly</p>
                                </div>
                                <div class="col-md-4" id="dateTimeContainer">
                                    {{-- <p style="font-weight: 600;font-size:14px;">Run on :</p> --}}
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Employee, Last Name</th>
                                            <th>Init.</th>
                                            <th>Employee Number</th>
                                            <th>ID Number</th>
                                            <th>Org. Unit</th>
                                            <th>Total Uif Earnings</th>
                                            <th>Employee Contribution</th>
                                            <th>Employer Contribution</th>
                                            <th>Total Contribution</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($departments) && count($departments) > 0)
                                            @php
                                            // dd($total_days);
                                                $sr_no = 1;
                                                $gross_salary_grand_total = 0;
                                                $net_salary_grand_total = 0;
                                                $deducation_grand_total = 0;
                                            @endphp
                                            @foreach ($departments as $department)
                                                @if (count($employees[$department->id]) > 0)
                                                    <tr>
                                                        <td colspan="14" style="font-weight: bold">{{$department->title}}</td>
                                                    </tr>
                                                    @foreach ($employees[$department->id] as $employee)
                                                        <tr>
                                                            <td>{{$employee->first_name}}<br>{{$employee->middle_name}}<br>{{$employee->last_name}}</td>
                                                            <td></td>
                                                            {{-- <td>{{$employee->initials}}</td> --}}
                                                            {{-- <td>{{$sr_no++}}</td> --}}

                                                            <td>{{$employee->employee_code}}</td>
                                                            {{-- <td>{{$employee->employee_id}}</td> --}}
  
                                                            
                                                            {{-- <td>{{$employee->job_title}}</td> --}}
                                                            {{-- <td>{{$employee->employement_status}}</td> --}}
                                                            <td>{{$employee->nic_num}}</td>
                                                            <td>{{$employee->department}}</td>
                                                            {{-- @if (!empty($total_days))   
                                                                <td style="text-align: right">{{number_format(getMultipleColumnValue($payrol_ids_str, $total_days->id,$employee->employee),1)}}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif --}}
                                                            @if (!empty($gross_salary))
                                                                @php
                                                                    $gross_salary_grand_total +=getMultipleColumnValue($payrol_ids_str, $gross_salary->id,$employee->employee);
                                                                @endphp   
                                                                <td style="text-align: right">{{number_format(getMultipleColumnValue($payrol_ids_str, $gross_salary->id,$employee->employee))}}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif
                                                            @if (!empty($deducation_column))   
                                                                @php
                                                                    $deducation_grand_total +=getMultipleColumnValue($payrol_ids_str, $deducation_column->id,$employee->employee);
                                                                @endphp
                                                                <td style="text-align: right">{{number_format(getMultipleColumnValue($payrol_ids_str, $deducation_column->id,$employee->employee))}}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif
                                                            @if (!empty($net_salary)) 
                                                                @php
                                                                    $net_salary_grand_total +=getMultipleColumnValue($payrol_ids_str, $net_salary->id,$employee->employee);
                                                                @endphp  
                                                                <td style="text-align: right">{{number_format(getMultipleColumnValue($payrol_ids_str, $net_salary->id,$employee->employee))}}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif
                                                            {{-- @if ($request->banksFilter==null)
                                                            <td></td>
                                                            {{-- @endif --}}
                                                        </tr>
                                                    @endforeach
                                                {{-- <tr>
                                                    <td colspan="9" style="font-weight: bold;text-align:end">Sub Total:</td>
                                                    @if (!empty($gross_salary))
                                                        @php
                                                            // $gross_salary_grand_total += !empty($sub_total_net_gross_salary[$department->id][0]->amount) ? $sub_total_net_gross_salary[$department->id][0]->amount : 0;
                                                        @endphp
                                                        <td style="text-align: right;">{{number_format($sub_total_net_gross_salary[$department->id][0]->amount)}}</td>
                                                        @else
                                                        <td>-</td>
                                                    @endif
                                                    @if (!empty($sub_total_deducations))
                                                        @php
                                                            // $deducation_grand_total += !empty($sub_total_deducations[$department->id][0]->amount) ? $sub_total_deducations[$department->id][0]->amount : 0;
                                                        @endphp
                                                        <td style="text-align: right;">{{number_format($sub_total_deducations[$department->id][0]->amount)}}</td>
                                                        @else
                                                        <td>-</td>
                                                    @endif
                                                    @if (!empty($net_salary))
                                                        @php
                                                            // $net_salary_grand_total += !empty($sub_total_net_salary[$department->id][0]->amount) ? $sub_total_net_salary[$department->id][0]->amount : 0;
                                                        @endphp
                                                        <td style="text-align: right;">{{number_format($sub_total_net_salary[$department->id][0]->amount)}}</td>
                                                        @else
                                                        <td>-</td>
                                                    @endif
                                                </tr> --}}
                                                @endif
                                            @endforeach
                                        @else
                                            <tr>
                                            <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="9">No Record Found!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    @if (isset($departments) && count($departments) > 0)
                                    <tfoot>
                                        <tr>
                                            <td colspan="5" style="font-weight: bold;text-align:end">All Units Total</td>
                                            <td style="text-align: right;">{{number_format($gross_salary_grand_total)}}</td>
                                            <td style="text-align: right;">{{number_format($deducation_grand_total)}}</td>
                                            <td style="text-align: right;">{{number_format($net_salary_grand_total)}}</td>
                                            <td style="text-align: right;">{{number_format($net_salary_grand_total)}}</td>
                                        </tr>
                                    </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                        {{-- <div class="row mb-1" id="signatures">
                            <div id="sign1" class="col-4 mt-3 d-flex flex-column text-end">
                                <input type="text" value="" style="border-width: 0 0 1px 0;" class="sign text-center">
                                <label for="sign1" class="text-center">Payroll Officer</label>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
<script>
    var currentDate = new Date();
        
        // Format the date and time as desired (adjust as needed)
        var formattedDateTime = currentDate.toLocaleString(); // Change the format as needed
        
        // Find the container div by its class
        var dateTimeContainer = document.getElementById('dateTimeContainer');
        
        // Create a new paragraph element to hold the formatted date and time
        var dateTimeParagraph = document.createElement('p');
        dateTimeParagraph.style.fontWeight = '600';
        dateTimeParagraph.style.fontSize = '14px';
        
        // Set the text content of the paragraph to the formatted date and time
        dateTimeParagraph.textContent = 'Run on: ' + formattedDateTime;
        
        // Append the paragraph element to the container div
        dateTimeContainer.appendChild(dateTimeParagraph);
    </script>
    <script>
        $(document).ready(function(){
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $("#department").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'loan_request'
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.employee_code+ ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation+' - '+ value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            // $('#get_data').submit(function(e){
            //     e.preventDefault();
            //     $('#date').text($('#joining').val());
            //     $('#name').text($('#section').val());
            // });
        });
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var banksFilter = $('#banksFilter').select2("val");
            var section = $('#section').select2("val");
            var employment_status = $('#employment_status').val();
            var departmentFilter = $('#departmentFilter').val();
            var order_filter = $('#order_filter').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            
            $.ajax({
                url: "{{url('uifReport')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    section:section,
                    fromDate:fromDate,
                    toDate:toDate,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter,
                    order_filter:order_filter
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        $(document).on('click', '.pdf', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var employment_status = $('#employment_status').val();
            var banksFilter = $('#banksFilter').val();
            var section = $('#section').select2("val");
            var departmentFilter = $('#departmentFilter').val();
            var order_filter = $('#order_filter').val();
            // var date = $('#date').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            $.ajax({
                url: "{{url('uifReport')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    section:section,
                    // date:date,
                    fromDate:fromDate,
                    toDate:toDate,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter,
                    order_filter:order_filter,
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open();
            WinPrint.document.write(`<!DOCTYPE html>`);
            WinPrint.document.write(`<head>`);
            WinPrint.document.write(`<style>
                        *{  
                            font-size: 10px;
                            color: black;
                        }
                        table tr{
                        }
                        table {
                        }
                        td{
                            padding:15px 0px 15px 0px;
                            border-bottom:1px solid black;
                        }
                        `);
            WinPrint.document.write(`</style>`);
            WinPrint.document.write(`</head><body>`);
            WinPrint.document.write(`${divToPrint.innerHTML}`);
            WinPrint.document.write(`</body></html>`);
            WinPrint.document.close();
            setTimeout(function(){
                WinPrint.print();
                // WinPrint.close();
            },1000);
            WinPrint.focus();
        }
    </script>
@endsection
