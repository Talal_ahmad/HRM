@extends('Admin.layouts.master')
@section('title', 'Bank Salary Sheet')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Bank_Salary_Sheet')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Cash_Salary_Sheet')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{url('bank_salary_sheet')}}">
                                <div class="row">
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" id="calculation_group">@lang('app.Calculation_Group'):</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="@lang('app.Select_Calculation_Group')" required>
                                            <option value=""></option>
                                            @foreach ($calculation_groups as $calculation_group)
                                                <option value="{{ $calculation_group->id }}" {{$calculation_group->id == request('calculation_group') ? 'selected' : ''}}>{{ $calculation_group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" multiple>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? in_array($department->id,request('departmentFilter')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#departmentFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">@lang('app.Department')</label>
                                            <select name="departmentFilter" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value="">Select Department</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? $department->id == request('departmentFilter') ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">@lang('app.Section')</label>
                                            <select name="section[]" id="section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                @if (!empty(request('departmentFilter')))
                                                    @foreach ($parentvar as $item)
                                                        <option value="{{$item->id}}" {{!empty(request('section')) ? in_array($item->id,request('section')) ? 'selected' : '' : ''}}>{{$item->title}}</option>
                                                    @endforeach
                                                @endif 
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                        <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
                                            <option value=""></option>
                                            {{-- @foreach (employees() as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" id="bank_status">@lang('app.Bank'):</label>
                                        <select name="bank_status" id="bank_status" class="select2 form-select" data-placeholder="@lang('app.Select_Bank_Status')">
                                            <option value=""></option>
                                                <option value="All" {{request('bank_status') == 'All' ? 'selected' : ''}}>All</option>
                                                <option value="Cash" {{request('bank_status') == 'Cash' ? 'selected' : ''}}>Cash</option>
                                                <option value="Bank" {{request('bank_status') == 'Bank'? 'selected' : ''}}>Bank</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employment_status">@lang('app.Employment_Status')</label>
                                            <select name="employment_status[]" id="employment_status" class="select2 form-select" data-placeholder="@lang('app.Select_Employment_Status')" multiple>
                                                <option value=""></option>
                                                @foreach ($employment_status as $employment)
                                                    <option value="{{$employment->id}}" {{!empty(request('employment_status')) ? in_array($employment->id,request('employment_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#employment_status')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#employment_status')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="banksFilter">@lang('app.Filter_By_Banks'):</label>
                                            <select name="banksFilter[]" id="banksFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Banks')" multiple>
                                                <option value=""></option>
                                                @foreach ($bank_name as $banks)
                                                    <option value="{{$banks->id}}" {{!empty(request('banksFilter')) ? in_array($banks->id,request('banksFilter')) ? 'selected' : '' : ''}}>{{$banks->name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#banksFilter')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#banksFilter')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                    </div>


                                    {{-- <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="banksFilter">Filter By Banks</label>
                                            <select name="banksFilter" id="banksFilter" class="select2 form-select" data-placeholder="Select Bank">
                                                <option value=""></option>
                                                <option value="all" {{request('banksFilter') == 'all' ? 'selected' : ''}}>All</option>
                                                @foreach ($bank_name as $banks)
                                                    <option value="{{ $banks->id }}" {{$banks->id == request('banksFilter') ? 'selected' : ''}}>{{ $banks->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" id="order_filter">@lang('app.Filter_By_Order'):</label>
                                        <select name="order_filter" id="order_filter" class="select2 form-select" data-placeholder="@lang('app.Select_Any_Order')">
                                            <option value=""></option>
                                                <option value="employee_id" {{request('order_filter') == 'employee_id' ? 'selected' : ''}}>By Employee ID</option>
                                                <option value="employee_code" {{request('order_filter') == 'employee_code'? 'selected' : ''}}>By Employee Code</option>
                                        </select>
                                    </div>
                                    {{-- <div class="col-md-3 col-12">
                                        <label class="form-label" for="date">Filter By Month</label>
                                        <input type="month" id="date" class="form-control" name="date" value="{{request('date')}}" required/>
                                    </div> --}}
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="fromDate">@lang('app.From_Date'):</label>
                                        <input type="month" id="fromDate" class="form-control date"  name="fromDate" value="{{empty(request('fromDate')) ? date('Y-m') : request('fromDate')}}" required/>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="toDate">@lang('app.To_Date'):</label>
                                        <input type="month" id="toDate" class="form-control date"  name="toDate" value="{{empty(request('toDate')) ? date('Y-m') : request('toDate')}}" required/>
                                    </div>
                                    {{-- <div class="col-md-3 col-12">
                                        <label class="form-label" for="singleDate">Filter By Date:</label>
                                        <input type="text" name="toDate" id="toDate" class="form-control flatpickr-basic toDate" placeholder="YYYY-MM-DD" required>
                                    </div> --}}
                                    {{-- <div class="col-12 col-md-6">
                                        <label class="form-label" for="fromDate">From Date:</label>
                                        <input type="text" name="fromDate" id="fromDate" class="form-control flatpickr-basic" value="{{request('fromDate')}}" placeholder="YYYY-MM-DD" required>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label class="form-label" for="toDate">To Date:</label>
                                        <input type="text" name="toDate" id="toDate" class="form-control flatpickr-basic" value="{{request('toDate')}}" placeholder="YYYY-MM-DD" required>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">@lang('app.Print')</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{url('bank_salary_sheet')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @php
                        // $grand_total = [];
                    @endphp
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                @if (!empty($calculation_group_payroll))
                                <button type="button" class="btn btn-success ml-2 export" id="excel">Download Excel</button>
                                <button style="margin-left: 6px;" type="button" class="btn btn-danger pdf" id="pdf">Download Pdf</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card">
                            <div class="row p-1">
                                <div class="col-md-4">
                                    @if (!empty($company_setup->logo))
                                        <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
                                    @else
                                        <img src="{{asset('images/company_logo/')}}" alt="Logo">
                                        
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <h4 style="text-align: center;font-weight: bold;margin: 10px 0px 0px 0px;">JAUHARABAD SUGAR MILLS LIMITED, JAUHARABAD <br> BANK SALARY SHEET
                                        @if (!empty($fromDate))
                                            <br>From: {{$fromDate}}
                                            To: {{$toDate}}
                                            {{-- Month: {{$date}} --}}
                                        @endif
                                    </h4>
                                </div> 
                                {{-- <div class="col-md-4">
                                    <h4 style="font-weight: bold;text-align:end;text-decoration: underline;text-decoration-thickness: 3px;">EOBI-PR-02-M</h4>
                                </div> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr</th>
                                            <th>Employee Code</th>
                                            <th>Employee ID</th>
                                            <th>Employee</th>
                                            @if (env('COMPANY') == 'JSML')        
                                                <th>Section</th>
                                            @else
                                                <th>Department</th>
                                            @endif
                                            <th>Designation</th>
                                            <th>Employement Status</th>
                                            <th>Bank Name</th>
                                            <th>Account No.</th>
                                            <th>Days</th>
                                            <th>Gross Pay</th>
                                            <th>Deduction</th>
                                            <th>Net Salary</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($departments) && count($departments) > 0)
                                            @php
                                            // dd($total_days);
                                                $sr_no = 1;
                                                $gross_salary_grand_total = 0;
                                                $net_salary_grand_total = 0;
                                                $deducation_grand_total = 0;
                                            @endphp
                                            @foreach ($departments as $department)
                                                @if (count($employees[$department->id]) > 0)
                                                    <tr>
                                                        <td colspan="14" style="font-weight: bold">{{$department->title}}</td>
                                                    </tr>
                                                    @foreach ($employees[$department->id] as $employee)
                                                        <tr>
                                                            <td>{{$sr_no++}}</td>
                                                            <td>{{$employee->employee_code}}</td>
                                                            <td>{{$employee->employee_id}}</td>
                                                            <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                                            <td>{{$employee->department}}</td>
                                                            <td>{{$employee->job_title}}</td>
                                                            <td>{{$employee->employement_status}}</td>
                                                            <td>{{$employee->bank_name}}</td>
                                                            <td>{{$employee->account_number}}</td>
                                                            @if (!empty($total_days))   
                                                                <td style="text-align: right">{{number_format(getMultipleColumnValue($payrol_ids_str, $total_days->id,$employee->employee),1)}}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif
                                                            @if (!empty($gross_salary))
                                                                @php
                                                                    $gross_salary_grand_total +=getMultipleColumnValue($payrol_ids_str, $gross_salary->id,$employee->employee);
                                                                @endphp   
                                                                <td style="text-align: right">{{number_format(getMultipleColumnValue($payrol_ids_str, $gross_salary->id,$employee->employee))}}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif
                                                            @if (!empty($deducation_column))   
                                                                @php
                                                                    $deducation_grand_total +=getMultipleColumnValue($payrol_ids_str, $deducation_column->id,$employee->employee);
                                                                @endphp
                                                                <td style="text-align: right">{{number_format(getMultipleColumnValue($payrol_ids_str, $deducation_column->id,$employee->employee))}}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif
                                                            @if (!empty($net_salary)) 
                                                                @php
                                                                    $net_salary_grand_total +=getMultipleColumnValue($payrol_ids_str, $net_salary->id,$employee->employee);
                                                                @endphp  
                                                                <td style="text-align: right">{{number_format(getMultipleColumnValue($payrol_ids_str, $net_salary->id,$employee->employee))}}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif
                                                            {{-- @if ($request->banksFilter==null)
                                                            <td></td>
                                                            {{-- @endif --}}
                                                        </tr>
                                                    @endforeach
                                                <tr>
                                                    <td colspan="10" style="font-weight: bold;text-align:end">Sub Total:</td>
                                                    @if (!empty($gross_salary))
                                                        @php
                                                            // $gross_salary_grand_total += !empty($sub_total_net_gross_salary[$department->id][0]->amount) ? $sub_total_net_gross_salary[$department->id][0]->amount : 0;
                                                        @endphp
                                                        <td style="text-align: right;">{{number_format($sub_total_net_gross_salary[$department->id][0]->amount)}}</td>
                                                        @else
                                                        <td>-</td>
                                                    @endif
                                                    @if (!empty($sub_total_deducations))
                                                        @php
                                                            // $deducation_grand_total += !empty($sub_total_deducations[$department->id][0]->amount) ? $sub_total_deducations[$department->id][0]->amount : 0;
                                                        @endphp
                                                        <td style="text-align: right;">{{number_format($sub_total_deducations[$department->id][0]->amount)}}</td>
                                                        @else
                                                        <td>-</td>
                                                    @endif
                                                    @if (!empty($net_salary))
                                                        @php
                                                            // $net_salary_grand_total += !empty($sub_total_net_salary[$department->id][0]->amount) ? $sub_total_net_salary[$department->id][0]->amount : 0;
                                                        @endphp
                                                        <td style="text-align: right;">{{number_format($sub_total_net_salary[$department->id][0]->amount)}}</td>
                                                        @else
                                                        <td>-</td>
                                                    @endif
                                                </tr>
                                                @endif
                                            @endforeach
                                        @else
                                            <tr>
                                            <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="10">No Record Found!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    @if (isset($departments) && count($departments) > 0)
                                    <tfoot>
                                        <tr>
                                            <td colspan="10" style="font-weight: bold;text-align:end">Grand Total</td>
                                            <td style="text-align: right;">{{number_format($gross_salary_grand_total)}}</td>
                                            <td style="text-align: right;">{{number_format($deducation_grand_total)}}</td>
                                            <td style="text-align: right;">{{number_format($net_salary_grand_total)}}</td>
                                        </tr>
                                    </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                        <div class="row mb-1" id="signatures">
                            <div id="sign1" class="col-4 mt-3 d-flex flex-column text-end">
                                <input type="text" value="" style="border-width: 0 0 1px 0;" class="sign text-center">
                                <label for="sign1" class="text-center">Payroll Officer</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $("#department").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'loan_request'
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.employee_code+ ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation+' - '+ value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            // $('#get_data').submit(function(e){
            //     e.preventDefault();
            //     $('#date').text($('#joining').val());
            //     $('#name').text($('#section').val());
            // });
        });
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var banksFilter = $('#banksFilter').select2("val");
            var section = $('#section').select2("val");
            var employment_status = $('#employment_status').val();
            var departmentFilter = $('#departmentFilter').val();
            var order_filter = $('#order_filter').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            
            $.ajax({
                url: "{{url('bank_salary_sheet')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    section:section,
                    fromDate:fromDate,
                    toDate:toDate,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter,
                    order_filter:order_filter
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        $(document).on('click', '.pdf', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var employment_status = $('#employment_status').val();
            var banksFilter = $('#banksFilter').val();
            var section = $('#section').select2("val");
            var departmentFilter = $('#departmentFilter').val();
            var order_filter = $('#order_filter').val();
            // var date = $('#date').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            $.ajax({
                url: "{{url('bank_salary_sheet')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    section:section,
                    // date:date,
                    fromDate:fromDate,
                    toDate:toDate,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter,
                    order_filter:order_filter,
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open();
            WinPrint.document.write(`<!DOCTYPE html>`);
            WinPrint.document.write(`<head>`);
            WinPrint.document.write(`<style>
                        *{  
                            font-size: 10px;
                            color: black;
                        }
                        table tr{
                            border: 1px solid black;
                        }
                        `);
            WinPrint.document.write(`</style>`);
            WinPrint.document.write(`</head><body>`);
            WinPrint.document.write(`${divToPrint.innerHTML}`);
            WinPrint.document.write(`</body></html>`);
            WinPrint.document.close();
            setTimeout(function(){
                WinPrint.print();
                WinPrint.close();
            },1000);
            WinPrint.focus();
        }
    </script>
@endsection
