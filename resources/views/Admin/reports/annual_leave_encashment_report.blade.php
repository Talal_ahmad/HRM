@extends('Admin.layouts.master')
@section('title', 'Annual Leave Encashment Report')
@section('style')
    <style>
        tr {
            border: 1px solid black;
        }

        thead tr>th {
            vertical-align: middle !important;
        }
        tr td, tr th{
            padding: 8px !important;
        }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('Annual Leave Encashment')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('Annual Leave Encashment')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <!--Search Form -->
                            <div class="card-body">
                                <form action="{{url('annualLeaveEncashmentReport')}}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-label">@lang('app.Departments')</label>
                                            <select name="departmentFilter" id="departmentFilter"
                                                class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all"
                                                    {{ request('departmentFilter') == 'all' ? 'selected' : '' }}>All
                                                </option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}"
                                                        {{ request('departmentFilter') == $department->id ? 'selected' : '' }}>
                                                        {{ $department->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Employee')">
                                                <option value=""></option>
                                                @if (isset($departmentEmployees))
                                                    @foreach ($departmentEmployees as $employee)
                                                        <option value="{{ $employee->id }}"
                                                            {{ request('employeeFilter') == $employee->id ? 'selected' : '' }}>
                                                            {{ $employee->employee_id . '-' . $employee->employee_code . '-' . $employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name . ' - ' . $employee->designation }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">@lang('app.Leave_Periods')</label>
                                            <select name="periodFilter" id="periodFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Leave_Period')" required>
                                                <option value=""></option>
                                                @foreach ($periods as $period)
                                                    <option value="{{ $period->id }}"
                                                        {{ request('periodFilter') == $period->id ? 'selected' : '' }}>
                                                        {{ $period->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{ url('annualLeaveEncashmentReport') }}" type="button"
                                                class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                            <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card p-2 table-responsive" id="spot1">
                            @if (isset($employees) && count($employees) > 0)
                            <div class="dt-action-buttons text-end">
                                <div class="dt-buttons d-inline-flex">
                                    <div class="dt-buttons d-inline-flex">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary">Print</button>
                                    </div>
                                </div>
                                <div class="dt-buttons d-inline-flex">
                                    <div class="dt-buttons d-inline-flex">
                                        <a href="{{url('annualLeaveEncashmentReport')}}?export_type=pdf&departmentFilter={{request('departmentFilter')}}&employeeFilter={{request('employeeFilter')}}&periodFilter={{request('periodFilter')}}" class="btn btn-danger export"
                                            id="pdf">Download Pdf</a>
                                    </div>
                                </div>
                                <div class="dt-buttons d-inline-flex">
                                    <a href="{{url('annualLeaveEncashmentReport')}}?export_type=excel&departmentFilter={{request('departmentFilter')}}&employeeFilter={{request('employeeFilter')}}&periodFilter={{request('periodFilter')}}" class="btn btn-success export"
                                            id="excel">Download Excel</a>
                                </div>
                            </div>
                            @endif
                            <div id="div_to_print" class="pt-2">
                                <table class="table table-bordered" style="border: 1px solid black; white-space:nowrap;">
                                    <thead>
                                        <tr rowspan="2" class="text-center">
                                            <th style="font-size: 20px" colspan="9">{{ env('COMPANY') }} (PVT) Ltd<br>
                                            <span style="font-size: 16px">Leave Encashment Report</span></th>
                                        </tr>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Employee Code</th>
                                            <th>Employee</th>
                                            <th>Department</th>
                                            <th>Basic Salary</th>
                                            <th>Total Annual</th>
                                            <th>Availed Annual</th>
                                            <th>Remaining</th>
                                            <th>Encashment</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($employees) && count($employees) > 0)
                                            @foreach ($employees as $key => $employee)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{ HandleEmpty($employee->employee_code) }}</td>
                                                    <td>{{ $employee->first_name }} {{ $employee->middle_name }}
                                                        {{ $employee->last_name }} - {{ $employee->designation }}
                                                    </td>
                                                    <td>{{ $employee->department }}</td>
                                                        @php
                                                            $leave_record = checkLeaveBalance($employee->id, $annual_leave_type->id, request('periodFilter'));
                                                            $basicSalary = getBasicSalaryFromLatestPayrool($employee->id,$employee->department_id);
                                                            $remaining = $leave_record['total_balance'] - $leave_record['leaves_taken'];
                                                        @endphp
                                                    <td>{{$basicSalary}}</td>
                                                    <td>{{ $leave_record['total_balance'] }}</td>
                                                    <td>{{ $leave_record['leaves_taken'] }}</td>
                                                    <td>{{ $remaining }}</td>
                                                    <td>{{round(($basicSalary/30)*$remaining,2)}}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9" class="text-center">No Record Found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employeeFilter').empty();
                            $('#employeeFilter').html(
                                '<option value="">Select Employee</option>');
                            $.each(response, function(index, value) {
                                $('#employeeFilter').append(
                                    $('<option></option>').val(value.id).html(
                                        value.employee_id + '-' + value
                                        .employee_code + '-' + value.first_name +
                                        ' ' + value.middle_name + ' ' + value
                                        .last_name + ' - ' + value.designation)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employeeFilter').empty();
                }
            });
        });

        // $(document).on('click', '.export', function() {
        //     blockUI();
        //     var export_type = $(this).attr('id');
        //     $.ajax({
        //         type: "GET",
        //         url: "{{ url('annualLeaveEncashmentReport') }}",
        //         data: {
        //             export_type: export_type,
        //             departmentFilter: departmentFilter,
        //             employeeFilter: "{!! request('employeeFilter') !!}",
        //             periodFilter: "{!! request('periodFilter') !!}",
        //         },
        //         success: function(response, status, xhr) {
        //             $.unblockUI();
        //             window.open(this.url);
        //         },
        //         error: function() {
        //             $.unblockUI();
        //             alert('Error occured');
        //         }
        //     });
        // });

        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.write(`<!DOCTYPE html>`);
            WinPrint.document.write(`<head>`);
            WinPrint.document.write(`<link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">`);
            WinPrint.document.write(`<style>
                            *{
                                padding: 0;
                                color: black;
                            }
                            table tr{
                                border: 1px solid black;
                            }
                            `);
            WinPrint.document.write(`</style>`);
            WinPrint.document.write(`</head><body>`);
            WinPrint.document.write(`${divToPrint.innerHTML}`);
            WinPrint.document.write(`</body></html>`);
            WinPrint.document.close();
            setTimeout(function() {
                WinPrint.print();
                WinPrint.close();
            }, 1000);
            WinPrint.focus();
        }
    </script>
@endsection
