<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bank Salary Report</title>
    <head>
        <style>
            #salary_sheet {
                border-collapse: collapse;
                width: 100%;
                font-family:'Segoe UI',sans-serif
            }

            #salary_sheet td, #salary_sheet th {
                border: 1px solid #ddd;
                padding: 5px;
                font-size: 100px;
                width: 5px;
            }

            #salary_sheet td{
                padding: 30px;
            }
            h1 {
                font-size: 200px;
                text-align: center;
                color: black;
                /* background-color: #356e9c; */
            }
            #salary_sheet tr:nth-child(even){background-color: #f2f2f2;}

            #salary_sheet tr:hover {background-color: #ddd;}

            #salary_sheet th {
                padding-top: 12px;
                padding-bottom: 12px;
                background-color: #2e475c;
                color: white;
            }
            img {
                height: 700px;
                width: 20%;
            }
            .txt_rgt{
                text-align: right;
            }
        </style>
    </head>
</head>
<body>
    <div>
        @if (!empty($company_setup->logo))
            <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
        @else
            <img src="{{asset('images/company_logo/')}}" alt="Logo">
            
        @endif
        <h1>Bank Salary Report</h1>
        <table class="table table-bordered table-striped" id="salary_sheet">
            <thead>
                <tr>
                    <th>Month : {{$date}}</th>
                    <th colspan="3">Calculation Group : {{$cal_group}}</th>
                    <th colspan="2">Printed By : {{auth()->user()->username}}</th>
                    <th colspan="3">Printed At : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    <th>Sr</th>
                    <th>Employee Code</th>
                    <th>Employee ID</th>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Bank Name</th>
                    <th>Employement Status</th>
                    <th>IBAN/ Account No</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                    @php
                        $sr_no = 1;
                        $grand_total = 0;
                    @endphp
                    @foreach ($departments as $department)
                        @if (count($employees[$department->id]) > 0)
                            <tr>
                                <td colspan="6" style="font-weight: bold">{{$department->title}}</td>
                            </tr>
                            @foreach ($employees[$department->id] as $employee)
                            <tr>
                                <td>{{$sr_no++}}</td>
                                <td>{{$employee->employee_code}}</td>
                                <td>{{$employee->employee_id}}</td>
                                <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                <td>{{$employee->department}}</td>
                                <td>{{$employee->bank_name}}</td>
                                <td>{{$employee->employement_status}}</td>
                                <td style="text-align: right;">{{getMultipleColumnValue($payrol_ids_str,196,$employee->employee)}}</td>
                                @if (!empty($net_salary))   
                                    <td style="text-align: right;">{{number_format(getMultipleColumnValue($payrol_ids_str, $net_salary->id,$employee->employee))}}</td>
                                @else
                                    <td>-</td>
                                @endif
                            </tr>
                            @endforeach
                        <tr>
                            <td colspan="8" style="font-weight: bold;text-align:end">Sub Total:</td>
                            @if (count($columns_totals[$department->id]) > 0)
                                @foreach ($columns_totals[$department->id] as $item)
                                    @php
                                        $grand_total += $item;
                                    @endphp
                                    <td style="text-align: right">{{number_format($item)}}</td>
                                @endforeach
                            @endif
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="6">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
            @if (isset($net_salary) && !empty($net_salary))
            <tfoot>
                <tr>
                    <td colspan="8" style="font-weight: bold;text-align:end">Grand Total</td>
                    <td style="text-align: right;">{{number_format($grand_total)}}</td>
                </tr>
            </tfoot>
            @endif
        </table>
    </div>
</body>
</html>