<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EOBI Monthly Report</title>
</head>
<body>
    <div>
        @php
            $grand_total = [];
        @endphp
        <table border=".5pt">
            <thead>
                <tr>
                    <th style="text-align: center;" colspan="2">Month : {{$date}}</th>
                    <th style="text-align: center;" colspan="2">Calculation Group : {{$cal_group}}</th>
                    <th style="text-align: center;" colspan="2">Printed By : {{auth()->user()->username}}</th>
                    <th style="text-align: center;" colspan="2">Printed At : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    <th>Sr.No</th>
                    <th>Employee ID</th>
                    <th>Employee Code</th>
                    <th>Employee</th>
                    @if (env('COMPANY') == 'JSML')        
                        <th>Section</th>
                    @else
                        <th>Department</th>
                    @endif
                    <th>Bank Name</th>
                    <th>Job Title</th>
                    <th>Employement Status</th>
                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                        @foreach ($payroll_columns as $column)
                            @php
                                $grand_total[$column->id] = 0;
                            @endphp
                            <th>{{$column->name}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                    @php
                        $sr_no = 1;
                        $g_total_employees = 0;
                    @endphp
                    @foreach ($departments as $department)
                        @php
                            $net_total_employees = 0;
                        @endphp
                        {{-- @foreach ($all_childrens[$department->id] as $child) --}}
                            @if (count($employees[$department->id]) > 0)
                                @php
                                    $net_total_employees = count($employees[$department->id]);
                                    $g_total_employees += count($employees[$department->id]);
                                @endphp
                                <tr>
                                    <td colspan="{{8+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                                </tr>
                                @foreach ($employees[$department->id] as $employee)
                                    <tr>
                                        <td>{{$sr_no++}}</td>
                                        <td>{{$employee->employee_id}}</td>
                                        <td>{{HandleEmpty($employee->employee_code)}}</td>
                                        <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                                        <td>{{$employee->department_name}}</td>
                                        <td>{{$employee->bank_name}}</td>
                                        <td>{{$employee->job_title}}</td>
                                        <td>{{$employee->employment_status}}</td>
                                        @foreach ($payroll_columns as $col)
                                            <td class="text-end">{{payrollColumnAmount($payrollIds,$employee->employee,$col->id,0,$col->round_off,$request->employment_status)}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3" style="font-weight: bold;text-align:end">Total No. of Employees:</td>
                                    <td>{{$net_total_employees}}</td>
                                    <td colspan="4" style="font-weight: bold;text-align:end">Sub Total:</td>
                                    @foreach ($columns_totals[$department->id] as $item)
                                        @php
                                            $grand_total[$item->payroll_item] += $item->amount;
                                        @endphp
                                        <td>{{number_format($item->amount)}}</td>
                                    @endforeach
                                </tr>
                            @endif
                        {{-- @endforeach --}}
                        {{-- @if (count($parent_totals[$department->id]) > 0)
                            <tr>
                                <td colspan="7" style="font-weight: bold;text-align:end">{{$department->title}} Total:</td>
                                    @foreach ($parent_totals[$department->id] as $parent)
                                        <td>{{number_format($parent->amount)}}</td>
                                    @endforeach
                            </tr>
                        @endif --}}
                    @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="8">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
            @if (isset($payroll_columns) && count($payroll_columns) > 0 && count($departments) > 0)
                <tfoot>
                    <tr>
                        <td colspan="3" style="font-weight: bold;text-align:end">Grand Total No. Of Employee</td>
                        <td>{{$g_total_employees}}</td>
                        <td colspan="4" style="font-weight: bold;text-align:end">Grand Total</td>
                        @foreach ($grand_total as $total)
                            <td>{{number_format($total)}}</td>
                        @endforeach
                    </tr>
                </tfoot>
            @endif
        </table>
    </div>
</body>
</html>