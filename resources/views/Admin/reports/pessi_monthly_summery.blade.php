@extends('Admin.layouts.master')
@section('title', 'PESSI Monthly Summary')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.PESSI_Monthly_Summery')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.PESSI_Monthly_Summery')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{url('pessiMonthlySummery')}}">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" id="calculation_group">@lang('app.Filter_By_Calculation_Group'):</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="@lang('app.Select_Calculation_Group')" required>
                                            <option value=""></option>
                                            @foreach ($calculation_groups as $calculation_group)
                                                <option value="{{ $calculation_group->id }}" {{$calculation_group->id == request('calculation_group') ? 'selected' : ''}}>{{ $calculation_group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : '' }}>All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? $department->id == request('departmentFilter') ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- @if (env('COMPANY') == 'JSML')        
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">Section</label>
                                                <select name="section[]" id="section" data-placeholder="Select Section" class="select2 form-select" multiple required>
                                                    @if (!empty(request('departmentFilter')))
                                                        @foreach ($parentvar as $item)
                                                            <option value="{{$item->id}}" {{!empty(request('section')) ? in_array($item->id,request('section')) ? 'selected' : '' : ''}}>{{$item->title}}</option>
                                                        @endforeach
                                                    @endif 
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#section')">Select All</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#section')">Deselect All</button>
                                                </div>
                                            </div>
                                        </div>    
                                    @endif --}}
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employment_status">@lang('app.Employment_Status')</label>
                                            <select name="employment_status[]" id="employment_status" class="select2 form-select" data-placeholder="@lang('app.Employment_Status')" multiple required>
                                                <option value=""></option>
                                                @foreach ($employment_status as $employment)
                                                    <option value="{{$employment->id}}" {{!empty(request('employment_status')) ? in_array($employment->id,request('employment_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#employment_status')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#employment_status')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="fromDate">@lang('app.From_Date'):</label>
                                        <input type="month" id="fromDate" class="form-control date"  name="fromDate" value="{{empty(request('fromDate')) ? date('Y-m') : request('fromDate')}}" required/>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="toDate">@lang('app.To_Date'):</label>
                                        <input type="month" id="toDate" class="form-control date"  name="toDate" value="{{empty(request('toDate')) ? date('Y-m') : request('toDate')}}" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">@lang('app.Print')</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{url('pessiMonthlySummery')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @php
                        $grand_total = [];
                    @endphp
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                <button type="button" class="btn btn-success export" id="excel">@lang('app.Download_Excel')</button>
                                <button style="margin-left: 6px;" type="button" class="btn btn-danger pdf" id="pdf">@lang('app.Download_Pdf')</button>
                            </div>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card">
                            <div class="row p-1">
                                <div class="col-md-12 d-flex">
                                    <div>
                                        <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
                                    </div>
                                    <div style="width: 60%">
                                        <h4 style="font-weight: bold;font-size: 24px;margin: 24px 0 0 17px;">{{$company_setup->company_name}}, SUMMARY OF PESSI CONTRIBUTION
                                            @if (!empty($fromDate))
                                                <br>From: {{$fromDate}}
                                                To: {{$toDate}}
                                                {{-- Month: {{$date}} --}}
                                            @endif
                                        </h4>
                                    </div>
                                </div>
                                {{-- <div class="col-md-9 text-end">
                                    <p style="font-weight: 600;font-size:20px;">F.M : &emsp;<span style="text-decoration: underline;text-decoration-thickness: 3px;"><?php echo $year."-".$month; ?></span></p>
                                </div> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            @if (env('COMPANY') == 'JSML')        
                                                <th>Section</th>
                                            @else
                                                <th>Department</th>
                                            @endif
                                            <th>Head Count</th>
                                            @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                                @foreach ($payroll_columns as $column)
                                                    @php
                                                        $grand_total[$column->payroll_column_id] = 0;
                                                    @endphp
                                                    <th>{{$column->name}}</th>
                                                @endforeach
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($departments) && count($departments) > 0)
                                            @php
                                                $sr_no = 1;
                                                $net_total_employees = 0;
                                                $g_total_employees = 0;
                                            @endphp
                                            @foreach ($departments as $department)
                                                @php
                                                    $parent_total_employees = 0;
                                                @endphp
                                                @foreach ($all_childrens[$department->id] as $child)
                                                    @if (count($total_employees[$child->id]) > 0)
                                                    @php
                                                        $net_total_employees = count($total_employees[$child->id]);
                                                        $parent_total_employees += count($total_employees[$child->id]);
                                                        $g_total_employees += count($total_employees[$child->id]);
                                                    @endphp
                                                        <tr>
                                                            <td>{{$sr_no++}}</td>
                                                            <td colspan="">{{$child->title}}</td>
                                                            <td>{{$net_total_employees}}</td>
                                                            @foreach ($columns_totals[$child->id] as $item)
                                                                @php
                                                                    $grand_total[$item->payroll_item] += $item->amount;
                                                                @endphp
                                                                <td style="text-align: right;">{{number_format($item->amount)}}</td>
                                                            @endforeach
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                @if (count($parent_totals[$department->id]) > 0)
                                                    <tr>
                                                        <td colspan="2" style="font-weight: bold;text-align:end">{{$department->title}} Total:</td>
                                                        <td>{{$parent_total_employees}}</td>
                                                        @foreach ($parent_totals[$department->id] as $parent)
                                                            <td style="text-align: right">{{number_format($parent->amount)}}</td>
                                                        @endforeach
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @else
                                            <tr>
                                                <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="10">No Record Found!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    @if (isset($payroll_columns) && count($payroll_columns) > 0 && count($departments) > 0)
                                    <tfoot>
                                        <tr>
                                            <td colspan="2" style="font-weight: bold;text-align:end">Total</td>
                                            <td>{{$g_total_employees}}</td>
                                            @foreach ($grand_total as $total)
                                                <td style="text-align: right;">{{number_format($total)}}</td>
                                            @endforeach
                                        </tr>
                                    </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                        <div class="row mb-1" id="signatures">
                            <div id="sign1" class="col-4 offset-1 d-flex flex-column">
                                <input type="text" value="{{$company_setup->first_name}} {{$company_setup->last_name}}" style="border-width: 0 0 1px 0;" class="sign text-center">
                                <label for="sign1" class="text-center">D.G.M (HR & Comp.)</label>
                            </div>
                            <div id="sign2" class="col-4 offset-2 d-flex flex-column">
                                <input type="text" value="{{$company_setup->gm_fName}} {{$company_setup->gm_lName}}" style="border-width: 0 0 1px 0;" class="sign text-center">
                                <label for="sign2" class="text-center">G.M. (Operations)</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        // $(document).ready(function(){
        //     $("#departmentFilter").change(function() {
        //         var optVal = $(this).val();
        //         if (optVal.length > 0) {
        //             $.ajax({
        //                 type: "GET",
        //                 url: "{{ url('get_department_child') }}",
        //                 data: {
        //                     department_id: optVal,
        //                 },
        //                 success: function(response) {
        //                     $('#section').empty();
        //                     $('#section').html('<option value="">Select Section</option>'); 
        //                     $.each(response, function(index, value) {                                
        //                         $('#section').append(
        //                             $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
        //                                 value.title)
        //                         );
        //                     });
        //                 },
        //                 error: function() {
        //                     alert('Error occured');
        //                 }
        //             });
        //         } else {
        //             $('#section').empty();
        //         }
        //     });
        //     // $('#get_data').submit(function(e){
        //     //     e.preventDefault();
        //     //     $('#date').text($('#joining').val());
        //     //     $('#name').text($('#section').val());
        //     // });
        // });
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var date = $('#date').val();
            $.ajax({
                url: "{{url('pessiMonthlySummery')}}",
                type: "GET",
                data: {
                    export_type,
                    calculation_group,
                    date,
                },
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        $(document).on('click', '.pdf', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var employment_status = $('#employment_status').val();
            var banksFilter = $('#banksFilter').val();
            var departmentFilter = $('#departmentFilter').val();
            var section = $('#section').select2("val");
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            $.ajax({
                url: "{{url('pessiMonthlySummery')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    section:section,
                    fromDate:fromDate,
                    toDate:toDate,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.write(`<!DOCTYPE html>
                    <head>
                        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
                    </head>
                    <body>
                    ${WinPrint.document.write(divToPrint.outerHTML)}
                    </body>
                    </html>
                    `)
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
@endsection
