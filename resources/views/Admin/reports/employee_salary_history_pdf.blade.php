<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Designation Wise Salary Report</title>
    <head>
        <style>
            #salary_sheet {
                border-collapse: collapse;
                width: 100%;
                font-family:'Segoe UI',sans-serif
            }

            #salary_sheet td, #salary_sheet th {
                border: 1px solid #ddd;
                padding: 5px;
                font-size: 40px;
                width: 5px;
            }

            #salary_sheet td{
                padding: 30px;
            }
            h1 {
                font-size: 200px;
                text-align: center;
                color: black;
                /* background-color: #356e9c; */
            }
            #salary_sheet tr:nth-child(even){background-color: #f2f2f2;}

            #salary_sheet tr:hover {background-color: #ddd;}

            #salary_sheet th {
                padding-top: 12px;
                padding-bottom: 12px;
                background-color: #2e475c;
                color: white;
            }
            img {
                height: 700px;
                width: 20%;
            }
            .txt_rgt{
                text-align: right;
            }
        </style>
    </head>
</head>
<body>
    <div>
        @if (!empty($company_setup->logo))
            <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
        @else
            <img src="{{asset('images/company_logo/')}}" alt="Logo">
            
        @endif
        <h1>Designation Wise Salary Report</h1>
        <table class="table table-bordered table-striped" id="salary_sheet">
            <thead>
                <tr>
                    @php
                        $sum_id=count($payroll_columns);
                    @endphp
                    <th colspan="3">Month : {{$date}}</th>
                    <th colspan="3">Printed By : {{auth()->user()->username}}</th>
                    <th colspan="{{$sum_id}}">Printed At : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    <th>Sr.No</th>
                    <th>Payroll</th>
                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                        @foreach ($payroll_columns as $column)
                            <th>{{$column->name}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($data))
                @php
                    $sr_no = 1;
                @endphp
                <tr>
                    @foreach ($payrolls as $key => $value)
                    <td>{{$sr_no++}}</td>
                    <td>{{$value->name}}</td>
                    @foreach ($data[$value->name] as $key => $column)
                        <td>{{$column}}</td>
                        {{-- <td>{{$data[$payroll->name]}}</td> --}}
                        @endforeach
                        @endforeach
                    </tr>
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="8">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</body>
</html>