@extends('Admin.layouts.master')
@section('title', 'Department Wise Salary')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Department_Wise_Salary')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Department_Wise_Salary')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{url('departmentWiseSalary')}}">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" id="calculation_group">@lang('app.Filter_By_Calculation_Group'):</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="@lang('app.Select_Calculation_Group')" required>
                                            <option value=""></option>
                                            @foreach ($calculation_groups as $calculation_group)
                                                <option value="{{ $calculation_group->id }}" {{$calculation_group->id == request('calculation_group') ? 'selected' : ''}}>{{ $calculation_group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                {{-- <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : ''}}>All</option> --}}
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? $department->id == request('departmentFilter') ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @if (env('COMPANY') == 'JSML')        
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">@lang('app.Section')</label>
                                                <select name="section[]" id="section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                    @if (!empty(request('departmentFilter')))
                                                        @foreach ($parentvar as $item)
                                                            <option value="{{$item->id}}" {{!empty(request('section')) ? in_array($item->id,request('section')) ? 'selected' : '' : ''}}>{{$item->title}}</option>
                                                        @endforeach
                                                    @endif 
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button" onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>    
                                    @endif
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                        <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
                                            <option value=""></option>
                                            {{-- @foreach (employees() as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employment_status">@lang('app.Employment_Status')</label>
                                            <select name="employment_status[]" id="employment_status" class="select2 form-select" data-placeholder="@lang('app.Select_Employment_Status')" multiple>
                                                <option value=""></option>
                                                @foreach ($employment_status as $employment)
                                                    <option value="{{$employment->id}}" {{!empty(request('employment_status')) ? in_array($employment->id,request('employment_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#employment_status')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#employment_status')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="banksFilter">Filter By Banks</label>
                                            <select name="banksFilter" id="banksFilter" class="select2 form-select" data-placeholder="Select Bank">
                                                <option value=""></option>
                                                <option value="all" {{request('banksFilter') == 'all' ? 'selected' : ''}}>All</option>
                                                @foreach ($banks as $bank)
                                                    <option value="{{ $bank->id }}" {{$bank->id == request('banksFilter') ? 'selected' : ''}}>{{ $bank->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" for="pay_grade">@lang('app.Pay_Grade_Filter'):</label>
                                        <select name="pay_grade" id="pay_grade" class="select2 form-select" data-placeholder="@lang('app.Select_a_Pay_Grade')">
                                            <option value=""></option>
                                            @foreach ($paygrades as $paygrade)
                                                <option value="{{$paygrade->id}}" {{$paygrade->id == request('pay_grade') ? 'selected' : ''}}>{{$paygrade->name}} ({{$paygrade->min_salary}}-{{$paygrade->max_salary}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" id="order_filter">@lang('app.Filter_By_Order'):</label>
                                        <select name="order_filter" id="order_filter" class="select2 form-select" data-placeholder="@lang('app.Select_Any_Order')">
                                            <option value=""></option>
                                                <option value="emp_id" {{request('order_filter') == 'emp_id' ? 'selected' : ''}}>By Employee ID</option>
                                                <option value="emp_code" {{request('order_filter') == 'emp_code'? 'selected' : ''}}>By Employee Code</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="fromDate">@lang('app.From_Date'):</label>
                                        <input type="month" id="fromDate" class="form-control date"  name="fromDate" value="{{empty(request('fromDate')) ? date('Y-m') : request('fromDate')}}" required/>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="toDate">@lang('app.To_Date'):</label>
                                        <input type="month" id="toDate" class="form-control date"  name="toDate" value="{{empty(request('toDate')) ? date('Y-m') : request('toDate')}}" required/>
                                    </div>
                                    {{-- <div class="col-md-4 col-12">
                                        <label class="form-label" for="date">Filter By Month</label>
                                        <input type="month" id="date" class="form-control" name="date" value="{{request('date')}}" required/>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">@lang('app.Print')</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{url('departmentWiseSalary')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @php
                        $grand_total = [];
                    @endphp
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                <button type="button" class="btn btn-success export" id="excel">@lang('app.Download_Excel')</button>
                                <button style="margin-left: 6px;" type="button" class="btn btn-danger export" id="pdf">@lang('app.Download_Pdf')</button>
                            </div>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card">
                            <div class="row p-1">
                                <div class="col-md-4">
                                    @if (!empty($company_setup->logo))
                                        <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
                                    @else
                                        <img src="{{asset('images/company_logo/')}}" alt="Logo">
                                        
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <h4 style="text-align: center;font-weight: bold;margin: 10px 0px 0px 0px;">JAUHARABAD SUGAR MILLS LIMITED, JAUHARABAD <br>Department Wise Salary Report
                                        @if (!empty($fromDate))
                                            <br>From: {{$fromDate}}
                                            To: {{$toDate}}
                                            {{-- Month: {{$date}} --}}
                                        @endif
                                    </h4>
                                </div>
                                {{-- <div class="col-md-4">
                                    <h4 style="font-weight: bold;text-align:end;text-decoration: underline;text-decoration-thickness: 3px;">EOBI-PR-02-M</h4>
                                </div> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Employee ID</th>
                                            <th>Employee Code</th>
                                            <th>Employee</th>
                                            @if (env('COMPANY') == 'JSML')        
                                                <th>Section</th>
                                            @else
                                                <th>Department</th>
                                            @endif
                                            {{-- <th>Bank Name</th> --}}
                                            <th>Job Title</th>
                                            <th>Employement Status</th>
                                            <th>Pay Grade</th>
                                            @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                                @foreach ($payroll_columns as $column)
                                                    @php
                                                        $grand_total[$column->id] = 0;
                                                    @endphp
                                                    <th>{{$column->name}}</th>
                                                @endforeach
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($departments) && count($departments) > 0)
                                            @php
                                                $sr_no = 1;
                                                $g_total_employees = 0;
                                            @endphp
                                            @foreach ($departments as $department)
                                                    @php
                                                        $net_total_employees = 0;
                                                    @endphp
                                                {{-- @foreach ($all_childrens[$department->id] as $child) --}}
                                                    @if (count($employees[$department->id]) > 0)
                                                        @php
                                                            $net_total_employees = count($employees[$department->id]);
                                                            $g_total_employees += count($employees[$department->id]);
                                                        @endphp
                                                        <tr>
                                                            <td colspan="{{8+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                                                        </tr>
                                                        @foreach ($employees[$department->id] as $employee)
                                                            <tr>
                                                                <td>{{$sr_no++}}</td>
                                                                <td>{{$employee->employee_id}}</td>
                                                                <td>{{HandleEmpty($employee->employee_code)}}</td>
                                                                <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                                                <td>{{$employee->department_name}}</td>
                                                                {{-- <td>{{$employee->bank_name}}</td> --}}
                                                                <td>{{$employee->job_title}}</td>
                                                                <td>{{$employee->employment_status}}</td>
                                                                <td>{{$employee->paygrade_name}} ({{$employee->min_salary}}-{{$employee->max_salary}})</td>
                                                                @foreach ($payroll_columns as $col)
                                                                    <td class="text-end">{{payrollColumnAmount($payrollIds,$employee->employee,$col->id,0,$col->round_off,$request->employment_status)}}</td>
                                                                @endforeach
                                                            </tr>
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="3" style="font-weight: bold;text-align:end">Total No. of Employees:</td>
                                                            <td>{{$net_total_employees}}</td>
                                                            <td colspan="4" style="font-weight: bold;text-align:end">Sub Total:</td>
                                                            @foreach ($columns_totals[$department->id] as $item)
                                                                @php
                                                                    $grand_total[$item->payroll_item] += $item->amount;
                                                                @endphp
                                                                <td class="text-end">{{number_format($item->amount)}}</td>
                                                            @endforeach
                                                        </tr>
                                                    @endif
                                                {{-- @endforeach --}}
                                                {{-- @if (count($parent_totals[$department->id]) > 0)
                                                <tr>
                                                    <td colspan="7" style="font-weight: bold;text-align:end">{{$department->title}} Total:</td>
                                                        @foreach ($parent_totals[$department->id] as $parent)
                                                            <td>{{number_format($parent->amount)}}</td>
                                                        @endforeach
                                                </tr>
                                                @endif --}}
                                            @endforeach
                                        @else
                                            <tr>
                                                <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="9">No Record Found!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    @if (isset($payroll_columns) && count($payroll_columns) > 0 && count($departments) > 0)
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" style="font-weight: bold;text-align:end">Grand Total No. Of Employee</td>
                                            <td>{{$g_total_employees}}</td>
                                            <td colspan="4" style="font-weight: bold;text-align:end">Grand Total</td>
                                            @foreach ($grand_total as $total)
                                                <td class="text-end">{{number_format($total,0)}}</td>
                                            @endforeach
                                        </tr>
                                    </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $('#get_data').submit(function(e){
                e.preventDefault();
                $('#date').text($('#joining').val());
                $('#name').text($('#section').val());
            });
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'loan_request'
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.employee_code+ ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation+' - '+ value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
        });
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var departmentFilter = $('#departmentFilter').val();
            var section = $('#section').select2("val");
            var employment_status = $('#employment_status').val();
            var banksFilter = $('#banksFilter').val();
            var order_filter = $('#order_filter').val();
            var employeeFilter = $('#employeeFilter').val();
            $.ajax({
                url: "{{url('departmentWiseSalary')}}",
                type: "GET",
                data: {
                    'export_type':export_type,
                    'calculation_group':calculation_group,
                    'fromDate':fromDate,
                    'toDate':toDate,
                    'departmentFilter':departmentFilter,
                    'section':section,
                    'employment_status':employment_status,
                    'banksFilter':banksFilter,
                    'order_filter':order_filter,
                    'employeeFilter':employeeFilter,
                },
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel'){
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.write(`<!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
                        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
                    </head>
                    <body>
                    ${WinPrint.document.write(divToPrint.innerHTML)}
                    </body>
                    </html>
                    `)
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
@endsection
