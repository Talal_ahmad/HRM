@extends('Admin.layouts.master')
@section('title', 'Designation Wise Salary')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Designation Wise Salary</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">Designation Wise Salary
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{url('designationWiseSalary')}}">
                                <div class="row">
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" multiple>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? in_array($department->id,request('departmentFilter')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#departmentFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Filter By Designation:</label>
                                            <select name="designationFilter[]" id="designationFilter" class="select2 form-select" data-placeholder="Select Designation" multiple>
                                                <option value=""></option>
                                                @foreach (designation() as $jobTitle)
                                                    <option value="{{$jobTitle->id}}" {{!empty(request('designationFilter')) ? in_array($jobTitle->id,request('designationFilter')) ? 'selected' : '' : ''}}>{{$jobTitle->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="banksFilter">Filter By Banks</label>
                                            <select name="banksFilter" id="banksFilter" class="select2 form-select" data-placeholder="Select Bank">
                                                <option value=""></option>
                                                <option value="all" {{request('banksFilter') == 'all' ? 'selected' : ''}}>All</option>
                                                @foreach ($banks as $bank)
                                                    <option value="{{ $bank->id }}" {{$bank->id == request('banksFilter') ? 'selected' : ''}}>{{ $bank->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-md-4 col-12">
                                        <label class="form-label" id="order_filter">Filter By Order:</label>
                                        <select name="order_filter" id="order_filter" class="select2 form-select" data-placeholder="Select Any Order">
                                            <option value=""></option>
                                                <option value="emp_id" {{request('order_filter') == 'emp_id' ? 'selected' : ''}}>By Employee ID</option>
                                                <option value="emp_code" {{request('order_filter') == 'emp_code'? 'selected' : ''}}>By Employee Code</option>
                                        </select>
                                    </div> --}}
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="date">Filter By Month</label>
                                        <input type="month" id="date" class="form-control" name="date" value="{{request('date')}}" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">Print</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{url('designationWiseSalary')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                <button type="button" class="btn btn-success export" id="excel">Download Excel</button>
                                <button style="margin-left: 6px;" type="button" class="btn btn-danger export" id="pdf">Download Pdf</button>
                            </div>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card">
                            <div class="row p-1">
                                <div class="col-md-4">
                                    @if (!empty($company_setup->logo))
                                        <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" style="width:200px;" alt="Logo">
                                    @else
                                        <img src="{{asset('images/company_logo/')}}" alt="Logo">
                                        
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <h4 style="text-align: center;font-weight: bold;margin: 10px 0px 0px 0px;">Designation Wise Salary Report</h4>
                                </div>
                                {{-- <div class="col-md-4">
                                    <h4 style="font-weight: bold;text-align:end;text-decoration: underline;text-decoration-thickness: 3px;">EOBI-PR-02-M</h4>
                                </div> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Employee ID</th>
                                            <th>Employee Code</th>
                                            <th>Employee</th>
                                            <th>Department</th>
                                            <th>Designation</th>
                                            <th>Employement Status</th>
                                            @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                                @foreach ($payroll_columns as $column)
                                                    <th>{{$column->name}}</th>
                                                @endforeach
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($departments) && count($departments) > 0 )
                                            @foreach ($departments as $key => $department)
                                                @if (isset($employees[$department->id]) && count($employees[$department->id]) > 0)
                                                    <tr>
                                                        <td colspan="{{7+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                                                    </tr>
                                                    @foreach ($employees[$department->id] as $key2 => $employee)
                                                        <tr>
                                                            <td>{{$key2+1}}</td>
                                                            <td>{{$employee->employee_id}}</td>
                                                            <td>{{$employee->employee_code}}</td>
                                                            <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                                                            <td>{{$employee->department}}</td>
                                                            <td>{{$employee->job_title}}</td>
                                                            <td>{{$employee->employement_status}}</td>
                                                            @foreach ($payroll_columns as $col)
                                                                <td class="text-end">{{payrollColumnAmount($employee->payroll,$employee->employee,$col->id,0,$col->round_off)}}</td>
                                                            @endforeach
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        {{-- <td colspan="7" style="font-weight: bold;text-align:end">Total:</td> --}}
                                                        @foreach ($payroll_columns as $column)
                                                        @php
                                                            $total = '-';
                                                            if(!empty($net_total[$department->id][$column->id]))
                                                            {
                                                                $total = number_format($net_total[$department->id][$column->id]);
                                                            }
                                                        @endphp
                                                        {{-- <td class="text-end">{{$total}}</td> --}}
                                                        @endforeach
                                                    </tr>
                                                    @else
                                                        <tr>
                                                            {{-- <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="{{7+count($payroll_columns)}}">No Record Found!</td> --}}
                                                        </tr>
                                                    @endif
                                                @endforeach
                                        @else
                                            <tr>
                                                <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="8">No Record Found!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    @if (isset($payroll_columns) && count($payroll_columns) > 0 && count($departments) > 0)
                                    <tfoot>
                                        <tr>
                                            {{-- <td colspan="7" style="font-weight: bold;text-align:end">Grand Total</td> --}}
                                            @foreach ($payroll_columns as $col)
                                                @php
                                                    $GrandTotal = '-';
                                                    if(!empty($grand_total[$col->id]))
                                                    {
                                                        $GrandTotal = number_format($grand_total[$col->id]);
                                                    }
                                                @endphp
                                                {{-- <td class="text-end">{{$GrandTotal}}</td> --}}
                                            @endforeach
                                        </tr>
                                    </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var date = $('#date').val();
            var departmentFilter = $('#departmentFilter').val();
            var employment_status = $('#employment_status').val();
            var designationFilter = $('#designationFilter').val();
            var banksFilter = $('#banksFilter').val();
            var order_filter = $('#order_filter').val();
            $.ajax({
                url: "{{url('designationWiseSalary')}}",
                type: "GET",
                data: {
                    'export_type':export_type,
                    // 'calculation_group':calculation_group,
                    'date':date,
                    'departmentFilter':departmentFilter,
                    'employment_status':employment_status,
                    'designationFilter':designationFilter,
                    'banksFilter':banksFilter,
                    'order_filter':order_filter,
                },
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel'){
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.write(`<!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
                        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
                    </head>
                    <body>
                    ${WinPrint.document.write(divToPrint.innerHTML)}
                    </body>
                    </html>
                    `)
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
@endsection
