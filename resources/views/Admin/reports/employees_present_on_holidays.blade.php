<!DOCTYPE html>
<html>

<head>
    <title>Employee Attendance on Holidays</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid">
        <h1 class="mt-2">Employee Attendance on Holidays</h1>
        <hr />
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col">SR No</th>
                    <th scope="col">In Time</th>
                    <th scope="col">Out Time</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $employeeId => $employeeData)
                    <tr>
                        <td colspan="3">
                            <strong>Employee</strong> : {{ $employeeData['employee']->employee_code }} -
                            {{ $employeeData['employee']->first_name }}
                            {{ $employeeData['employee']->last_name }} - {{ $employeeData['employee']->jobtitle->name }}
                            - {{ $employeeData['employee']->companystructure->title }}
                        </td>
                    </tr>
                    @foreach ($employeeData['entries'] as $index => $entry)
                        <tr>
                            <th scope="row">{{ $index + 1 }}</th>
                            <td>{{ $entry->in_time }}</td>
                            <td>{{ $entry->out_time }}</td>
                        </tr>
                    @endforeach
                @endforeach
            </tbody>
        </table>
    </div>
</body>

</html>
