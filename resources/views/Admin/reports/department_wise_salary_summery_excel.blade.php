<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Department Wise Summary Report</title>
</head>
<body>
    <div>
        @php
            $grand_total = [];
        @endphp
        <table border=".5pt">
            <thead>
                <tr>
                    <th style="text-align: center;" colspan="2">Month : {{$date}}</th>
                    <th style="text-align: center;" colspan="2">Calculation Group : {{$cal_group}}</th>
                    <th style="text-align: center;" colspan="2">Printed By : {{auth()->user()->username}}</th>
                    <th style="text-align: center;" colspan="2">Printed At : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    <th>Sr.No</th>
                    {{-- <th>Employee ID</th>
                    <th>Employee Code</th>
                    <th>Employee</th> --}}
                    @if (env('COMPANY') == 'JSML')        
                        <th>Section</th>
                    @else
                        <th>Department</th>
                    @endif
                    <th>Total Employees</th>
                    {{-- <th>Employement Status</th> --}}
                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                        @foreach ($payroll_columns as $column)
                            @php
                                $grand_total[$column->id] = 0;
                            @endphp
                            <th>{{$column->name}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                    @php
                        $sr_no = 1;
                        $net_total_employees = 0;
                        $g_total_employees = 0;
                    @endphp
                    @foreach ($departments as $department)
                    @php
                        $parent_total_employees = 0;
                    @endphp
                        @foreach ($all_childrens[$department->id] as $child)
                            @if (count($total_employees[$child->id]) > 0)
                            @php
                                $net_total_employees = count($total_employees[$child->id]);
                                $parent_total_employees += count($total_employees[$child->id]);
                                $g_total_employees += count($total_employees[$child->id]);
                            @endphp
                                <tr>
                                    <td>{{$sr_no++}}</td>
                                    <td colspan="">{{$child->title}}</td>
                                    <td>{{$net_total_employees}}</td>
                                    @foreach ($columns_totals[$child->id] as $item)
                                        @php
                                            $grand_total[$item->payroll_item] += $item->amount;
                                        @endphp
                                        <td style="text-align: right;">{{number_format($item->amount)}}</td>
                                    @endforeach
                                </tr>
                            @endif
                        @endforeach
                        @if (count($parent_totals[$department->id]) > 0)
                        <tr>
                            <td colspan="2" style="font-weight: bold;text-align:right">{{$department->title}} Total:</td>
                            <td>
                                {{$parent_total_employees}}
                            </td>
                            @foreach ($parent_totals[$department->id] as $parent)
                                <td style="font-weight: bold;text-align:right">{{number_format($parent->amount)}}</td>
                            @endforeach
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="10">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
            @if (isset($payroll_columns) && count($payroll_columns) > 0 && count($departments) > 0 && !empty($all))
            <tfoot>
                <tr>
                    <td colspan="2" style="font-weight: bold;text-align:end">Total</td>
                    <td>{{$g_total_employees}}</td>
                    @foreach ($grand_total as $total)
                        <td style="text-align: right;">{{number_format($total)}}</td>
                    @endforeach
                </tr>
            </tfoot>
            @endif
        </table>
    </div>
</body>
</html>