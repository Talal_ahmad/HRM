@extends('Admin.layouts.master')
@if (env('COMPANY') == 'Ajmal Dawakhana')
    @section('title', 'Overtime Report')
@else
    @section('title', 'Deduction Report')
@endif
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        @if (env('COMPANY') == 'Ajmal Dawakhana')
                            <h2 class="content-header-title float-start mb-0">@lang('app.Overtime_Report')</h2>
                        @else
                            <h2 class="content-header-title float-start mb-0">@lang('app.Deduction_Report')</h2>
                        @endif
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                @if (env('COMPANY') == 'Ajmal Dawakhana')
                                    <li class="breadcrumb-item active">@lang('app.Overtime_Report')
                                    </li>
                                @else
                                    <li class="breadcrumb-item active">@lang('app.Deduction_Report')
                                    </li>
                                @endif
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <!--Search Form -->
                        <div class="card mb-1">
                            <div class="card-body">
                                <form id="search_form" action="{{ url('deductionReport') }}" method="GET">
                                    <input type="hidden" name="type" value="{{ request('type') }}">
                                    <div class="row">
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter"
                                                class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all"
                                                    {{ 'all' == request('departmentFilter') ? 'selected' : '' }}>All
                                                </option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}"
                                                        {{ $department->id == request('departmentFilter') ? 'selected' : '' }}>
                                                        {{ $department->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="employeeFilter">@lang('app.Filter_By_Employee'):</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Employee')">
                                                <option value=""></option>
                                                @if (isset($employees))
                                                    @foreach ($employees as $item)
                                                        <option value="{{ $item->id }}"
                                                            {{ $item->id == request('employeeFilter') ? 'selected' : '' }}>
                                                            {{ $item->employee_id . '-' . $item->first_name . ' ' . $item->middle_name . ' ' . $item->last_name }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <label class="form-label" for="payrollFilter">@lang('app.Select_Deduction_Column'):</label>
                                            <select name="payrollFilter[]" id="payrollFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Deduction_Column')" required multiple>
                                                <option value=""></option>
                                                @foreach ($payrollColumn as $item)
                                                    <option value="{{ $item->id }}"
                                                        {{ !empty(request('payrollFilter')) ? (in_array($item->id, request('payrollFilter')) ? 'selected' : '') : '' }}>
                                                        {{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#payrollFilter')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#payrollFilter')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="employment_status">@lang('app.Employment_Status')</label>
                                                <select name="employment_status[]" id="employment_status"
                                                    class="select2 form-select" data-placeholder="@lang('app.Select_Employment_Status')"
                                                    multiple required>
                                                    <option value=""></option>
                                                    @foreach ($employment_status as $employment)
                                                        <option value="{{ $employment->id }}"
                                                            {{ !empty(request('employment_status')) ? (in_array($employment->id, request('employment_status')) ? 'selected' : '') : '' }}>
                                                            {{ $employment->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#employment_status')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#employment_status')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                            <input type="checkbox" id="suppress" name="suppress" value="1"
                                                {{ !empty(request('suppress')) ? 'checked' : '' }}>
                                            <label for="suppress">@lang('app.Suppress_zero')</label><br><br>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="month">@lang('app.Select_Month'):</label>
                                            <input type="month" name="month" id="month" class="form-control"
                                                value="{{ request('month') }}" placeholder="YYYY-MM" required>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <label class="form-label" for="payroll">@lang('app.Payrolls'):</label>
                                            <select name="payrolls[]" id="payroll" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Payrolls')" multiple required>
                                                <option value=""></option>
                                                @if (isset($payrolls))
                                                    @foreach ($payrolls as $payroll)
                                                        <option value="{{ $payroll->id }}"
                                                            {{ !empty(request('payrolls')) ? (in_array($payroll->id, request('payrolls')) ? 'selected' : '') : '' }}>
                                                            {{ $payroll->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#payroll')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#payroll')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{ url('deductionReport') }}" type="button"
                                                class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                            <input type="submit" class="btn btn-primary mt-1" value="@lang('app.Apply')">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card pb-2">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="deducted_report">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            @if (env('COMPANY') != 'RoofLine')
                                                <th>Employee ID</th>
                                                <th>Employee Code</th>
                                            @endif
                                            <th>Employee</th>
                                            <th>Department</th>
                                            <th>Employement Status</th>
                                            @if (env('COMPANY') != 'RoofLine')
                                                <th>Payroll</th>
                                            @endif
                                            @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                                @foreach ($payroll_columns as $item)
                                                    <th>{{ $item->name }}</th>
                                                @endforeach
                                            @endif

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($employees) && count($employees) > 0)
                                            @php
                                                $grand_total = 0;
                                            @endphp
                                            @foreach ($employees as $key => $employee)
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    @if (env('COMPANY') != 'RoofLine')
                                                        <td>{{ $employee->employee_id }}</td>
                                                        <td>{{ $employee->employee_code }}</td>
                                                    @endif
                                                    <td>{{ $employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name }}
                                                    </td>
                                                    <td>{{ $employee->department }}</td>
                                                    <td>{{ $employee->status }}</td>
                                                    @if (env('COMPANY') != 'RoofLine')
                                                        <td>{{ $employee->payroll_name }}</td>
                                                    @endif
                                                    @foreach ($payroll_columns as $payroll_column)
                                                        <td style="white-space: nowrap; text-align:right;">
                                                            {{ getColumnValue($employee->payroll_id, $payroll_column->id, $employee->employee) }}
                                                        </td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            @if (isset($employees) && count($employees) > 0)
                                                <th></th>
                                                @if (env('COMPANY') != 'RoofLine')
                                                    <th></th>
                                                    <th></th>
                                                @endif
                                                <th></th>
                                                <th></th>
                                                @if (env('COMPANY') != 'RoofLine')
                                                    <th></th>
                                                @endif
                                                <th class="text-end">Totals: </th>
                                                @foreach ($totals as $total)
                                                    <th style="white-space: nowrap; text-align:right;">
                                                        {{ number_format($total->amount) }}</th>
                                                @endforeach
                                            @endif
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    </section>
@endsection
@section('scripts')
    <script>
        // Define the hasNonZeroValues function
        function hasNonZeroValues(rowData, selectedColumns) {
            // Your logic to check if there are non-zero values in selected columns
            // Example: Check if any selected column has a non-zero value in the current row
            for (var i = 0; i < selectedColumns.length; i++) {
                var columnIndex = selectedColumns[i];
                var cellValue = rowData[columnIndex];

                // Assuming the cell value should be a number for this example
                if (parseFloat(cellValue) !== 0) {
                    return true; // Found a non-zero value, so return true
                }
            }

            return false; // No non-zero values found in selected columns
        }
        $(document).ready(function() {
            $('#deducted_report').DataTable({
                ordering: true,
                // "columnDefs": [
                // 	{
                // 		// For Responsive
                // 		className: 'control',
                // 		orderable: false,
                // 		targets: 0
                // 	},
                // ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            footer: true,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            footer: true,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            footer: true,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: true,
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation: 'landscape',
                            pageSize: 'LEGAL',
                            customize: function(doc) {

                                var now = new Date();
                                var jsDate = now.getDate() + "-" + (now.getMonth() + 1) +
                                    "-" + now.getFullYear();
                                // doc.defaultStyle.fontSize = 11; 
                                var rowCount = doc.content[1].table.body.length;
                                for (i = 1; i < rowCount; i++) {
                                    var row = doc.content[1].table.body[i];
                                    for (var j = 6; j <= row.length; j++) {
                                        if (row[j]) {
                                            row[j].alignment = 'right';
                                        }
                                    }
                                }

                                var selectmonth = '{!! request()->query('month') !!}';
                                var encodedJsonString = '{{ $employment_status_show }}';
                                var decodedJsonString = $('<textarea />').html(
                                    encodedJsonString).text();
                                var employmentStatusArray = JSON.parse(decodedJsonString);
                                var titleString = 'For Month: ' + selectmonth + '\n' +
                                    'Employment Status: ' + employmentStatusArray.map(
                                        function(item) {
                                            return item.name;
                                        }).join(', ');
                                console.log(titleString);
                                doc.content.unshift({
                                    text: titleString,
                                    bold: true,
                                    style: 'header'
                                });
                                doc["footer"] = function(page, pages) {
                                    if (page === pages) {
                                        return {
                                            columns: [{
                                                    alignment: "left",
                                                    text: ["Created on: ", {
                                                        text: jsDate
                                                            .toString()
                                                    }]
                                                },
                                                @if (env('COMPANY') == 'Ajmal Dawakhana')
                                                    {
                                                        alignment: "center", // Center align the second signature
                                                        text: [
                                                            "Prepared by : ___________________"
                                                        ] // Add the second signature here
                                                    }, {
                                                        alignment: "Right", // Center align the second signature
                                                        text: [
                                                            "Approve by: ___________________"
                                                        ] // Add the second signature here
                                                    }
                                                @endif
                                            ],
                                            margin: 20
                                        };
                                    } else {
                                        return null;
                                    }
                                };
                            },
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            // action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }, ],
                // responsive: {
                // 	details: {
                // 		display: $.fn.dataTable.Responsive.display.childRowImmediate,
                // 		type: 'column',
                // 	}
                // },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });


            $('div.head-label').html('<h6 class="mb-0">List of Employees</h6>');
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'deducation_report'
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>');
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - ' + value.first_name +
                                    ' ' + value.middle_name + ' ' + value
                                    .last_name + ' - ' + value.designation)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });

            $("#month").change(function() {
                var month = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('getPayrollsByMonth') }}" + "/" + month,
                    success: function(response) {
                        $('#payroll').empty();
                        $('#payroll').html('<option value="">Select Payrolls</option>');
                        $.each(response, function(index, value) {
                            $('#payroll').append(
                                $('<option></option>').val(value.id).html(
                                    value.name)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            // $("#suppress").change(function() {
            //     if ($(this).is(":checked")) {
            //         // If suppress checkbox is checked, make an AJAX request to get filtered data
            //         var selectedColumns = $("#payrollFilter").val();
            //         var formData = {
            //             departmentFilter: $("#departmentFilter").val(),
            //             employeeFilter: $("#employeeFilter").val(),
            //             payrollFilter: selectedColumns,
            //             employment_status: $("#employment_status").val(),
            //             month: $("#month").val(),
            //             payrolls: $("#payroll").val(),
            //             suppress: 1,
            //         };

            //         $.ajax({
            //             type: "GET",
            //             url: "{{ url('deductionReport') }}",
            //             data: formData,
            //             success: function(response) {

            //             },
            //             error: function() {
            //                 alert('Error occurred');
            //             },
            //         });
            //     } else {
            //         // If suppress checkbox is unchecked, reload the original data
            //         $("#search_form").submit();
            //     }
            // });
        });
    </script>
@endsection
