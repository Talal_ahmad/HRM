<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel 9 Import Export Excel & CSV File - TechvBlogs</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>
<body>
    <div class="container mt-5 text-center">
        <h2 class="mb-4">
            Import File
        </h2>
        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group mb-4">
                <div class="custom-file text-left">
                    <input type="file" name="file" class="custom-file-input" id="customFile" onchange="updateFileName()">
                    <label class="custom-file-label" for="customFile">Choose a file</label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Import Employees</button>
        </form>
    </div>

    <script>
        function updateFileName() {
            var fileName = $('#customFile')[0].files[0].name;
            $('.custom-file-label').html(fileName);
        }
    </script>
</body>
</html>
