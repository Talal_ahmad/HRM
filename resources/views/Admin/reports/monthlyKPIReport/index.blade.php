@extends('Admin.layouts.master')

@section('title', 'Monthly KPI Report')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Monthly KPI Report</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">MIS Reports</a>
                            </li>
                            <li class="breadcrumb-item active">Monthly KPI Report
                            </li>
                        </ol>
                    </div>
                </div>
            </div> 
        </div>
    </div> 
    <div class="content-header row"></div> 
    <div class="content-body">
        <section id="basic-dataTable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>ID</th>
                                    <th>Employee</th>
                                    <th>Total Marks</th>
                                    <th>Marks</th>
                                    <th>Date</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="modal-size-lg d-inline-block">
            <!-- Add Modal -->
            <div class="modal fade text-start" id="monthly_kpi_report_add_modal" tabindex="-1"
            aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Monthly KPI Report</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                    </div>
                    <form class="form" id="monthly_kpi_report_add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee_id">Employee</label>
                                            <select name="employee_id" id="employee_id" class="select2 form-select"
                                            required>
                                            <option value="">Select Employee</option>
                                            @foreach ($employees as $item)
                                            <option value="{{ $item->id }}">{{ $item->employee }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="marks">Marks</label>
                                        <input type="text" id="marks" class="form-control" placeholder="Enter Marks" name="marks" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="start_date">Date</label>
                                        <input type="text" name="start_date" id="start_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-outline-success">Reset</button>
                        <button type="submit" class="btn btn-primary" id="save">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade text-start" id="monthly_kpi_report_edit_modal" tabindex="-1"
    aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Edit Overtime Management</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                aria-label="Close"></button>
            </div>
            <form class="form" id="monthly_kpi_report_edit_form">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_employee_id">Employee</label>
                                            <select name="employee_id" id="edit_employee_id" class="select2 form-select"
                                            required>
                                            <option value="">Select Employee</option>
                                            @foreach ($employees as $item)
                                            <option value="{{ $item->id }}">{{ $item->employee }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_marks">Marks</label>
                                        <input type="text" id="edit_marks" class="form-control" placeholder="Enter Marks" name="marks" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_start_date">Date</label>
                                        <input type="text" name="start_date" id="edit_start_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-outline-success">Reset</button>
                <button type="submit" class="btn btn-primary" id="save">Save</button>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div>
</section>
@endsection

@section('scripts')
<script>
    var rowid;
    $(document).ready(function() {
        datatable = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: true,
            ajax: "{{ route('monthlyKPIReport.index') }}", 
            columns: [
            {
                data: 'responsive_id'
            },
            {
                data: 'id',
                name: 'monthly_kpi.id',
            },
            {
                data: 'employeeName',
                name: 'employeeName',
            },
            {
                data: 'tmarks',
                name: 'tmarks', 
            },
            {
                data: 'marks',
                name: 'monthly_kpi.marks',
            },
            {
                data: 'start_date',
                name: 'monthly_kpi.start_date',
            },
            {
                data: '',
                searchable: false
            },
            ],
            "columnDefs": [
            {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                           return (
                            '<a href="javascript:;" class="item-edit" onclick=edit('+full.id+')>' +
                            feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                            '</a>'+
                            '<a href="javascript:;" onclick="delete_item('+full.id+')">' +
                            feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                            '</a>'
                            );
                       }
                   },
                   {
                    "defaultContent": "-",
                    "targets": "_all"
                }
                ],
                "order": [
                [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Print',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Csv',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Pdf',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Copy',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                            .addClass('d-inline-flex');
                        }, 50);
                    }
                },
                {
                    text: feather.icons['plus'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Add New',
                    className: 'create-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#monthly_kpi_report_add_modal'
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
                ],
                responsive: {
                 details: {
                  display: $.fn.dataTable.Responsive.display.childRowImmediate,
                  type: 'column',
              }
          },
          language: {
            paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
$('#monthly_kpi_report_add_form').submit(function(e) {
    e.preventDefault();
    $.ajax({
        url: "{{ route('monthlyKPIReport.store') }}",
        type: 'POST',
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(response) {
            if (response.errors) {
                $.each(response.errors, function(index, value) {
                    Toast.fire({
                        icon: 'error',
                        title: value
                    })
                });
            } else if (response.error_message) {
                Toast.fire({
                    icon: 'error',
                    title: 'An error has been occured! Please Contact Administrator.'
                })
            } else {
                $('#monthly_kpi_report_add_form')[0].reset();
                $(".select2").val('').trigger('change')
                $("#monthly_kpi_report_add_modal").modal("hide");
                datatable.ajax.reload();
                Toast.fire({
                    icon: 'success',
                    title: 'Monthly KPI Report has been Added Successfully!'
                })
            }
        }
    });
})
$('#monthly_kpi_report_edit_form').submit(function(e) {
    e.preventDefault();
    $.ajax({
        url: "{{ url('monthlyKPIReport') }}" + "/" + rowid,
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.errors) {
                $.each(response.errors, function(index, value) {
                    Toast.fire({
                        icon: 'error',
                        title: value
                    })
                });
            } else if (response.error_message) {
                Toast.fire({
                    icon: 'error',
                    title: 'An error has been occured! Please Contact Administrator.'
                })
            } else {
                $('#monthly_kpi_report_edit_form')[0].reset();
                $(".select2").val('').trigger('change')
                $("#monthly_kpi_report_edit_modal").modal("hide");
                datatable.ajax.reload();
                Toast.fire({
                    icon: 'success',
                    title: 'Monthly KPI Report has been Updated Successfully!'
                })
            }
        }
    });
});

$('div.head-label').html('<h6 class="mb-0">List of Monthly KPI Report</h6>');
});

function edit(id) {
 rowid = id;
 $.ajax({
    url: "{{url('monthlyKPIReport')}}"+"/"+rowid+"/edit",
    type: "get",
    success: function (response) {
        console.log(response);
        $("#edit_employee_id").val(response.employee_id).select2();
        $("#edit_marks").val(response.marks);
        $("#edit_start_date").val(response.start_date);
        $("#monthly_kpi_report_edit_modal").modal("show"); 
    },
});
}

function delete_item(id) {
    $.confirm({
        icon: 'far fa-question-circle',
        title: 'Confirm!',
        content: 'Are you sure want to delete!', 
        type: 'orange', 
        typeAnimated: true,
        buttons: {
            Confirm: {
                text: 'Confirm',
                btnClass: 'btn-orange',
                action: function() {
                    $.ajax({
                        url: "monthlyKPIReport/" + id,
                        type: "DELETE",
                        data: {
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(response) { 
                            if (response.error_message) {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'An error has been occured! Please Contact Administrator.'
                                })
                            } else {
                                datatable.ajax.reload();
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Monthly KPI Report has been Deleted Successfully!'
                                })
                            }
                        }
                    });
                }
            },
            cancel: function() {
                $.alert('Canceled!');
            },
        }
    });
}
</script>
@endsection
