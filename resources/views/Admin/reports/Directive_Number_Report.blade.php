@extends('Admin.layouts.master')
@section('title', 'Directive Number Report')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Directive Number Report</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">Directive Number Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{url('DirevtiveNumberReport')}}">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" id="calculation_group">Filter By Calculation Group:</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="Select Calculation Group">
                                            <option value=""></option>
                                            @foreach ($calculation_groups as $calculation_group)
                                                <option value="{{ $calculation_group->id }}" {{$calculation_group->id == request('calculation_group') ? 'selected' : ''}}>{{ $calculation_group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-md-8 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" multiple>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? in_array($department->id,request('departmentFilter')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#departmentFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div> --}}
                                    @if (env('COMPANY') == 'JSML')        
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter" id="department" class="select2 form-select" data-placeholder="Select Department" required>
                                                <option value=""></option>
                                                <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : ''}}>All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? $department->id == request('departmentFilter') ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">Section</label>
                                                <select name="section[]" id="section" data-placeholder="Select Section" class="select2 form-select" multiple required>
                                                    @if (!empty(request('departmentFilter')))
                                                        @foreach ($parentvar as $item)
                                                            <option value="{{$item->id}}" {{!empty(request('section')) ? in_array($item->id,request('section')) ? 'selected' : '' : ''}}>{{$item->title}}</option>
                                                        @endforeach
                                                    @endif 
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#section')">Select All</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#section')">Deselect All</button>
                                                </div>
                                            </div>
                                        </div>   
                                    @else 
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" multiple>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? in_array($department->id,request('departmentFilter')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#departmentFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employment_status">Employment Status</label>
                                            <select name="employment_status[]" id="employment_status" class="select2 form-select" data-placeholder="Select Employment Status" multiple>
                                                <option value=""></option>
                                                @foreach ($employment_status as $employment)
                                                    <option value="{{$employment->id}}" {{!empty(request('employment_status')) ? in_array($employment->id,request('employment_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" id="order_filter">Filter By Order:</label>
                                        <select name="order_filter" id="order_filter" class="select2 form-select" data-placeholder="Select Any Order">
                                            <option value=""></option>
                                                <option value="emp_id" {{request('order_filter') == 'emp_id' ? 'selected' : ''}}>By Employee ID</option>
                                                <option value="emp_code" {{request('order_filter') == 'emp_code'? 'selected' : ''}}>By Employee Code</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="fromDate">From Date:</label>
                                        <input type="month" id="fromDate" class="form-control date"  name="fromDate" value="{{empty(request('fromDate')) ? date('Y-m') : request('fromDate')}}" required/>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="toDate">To Date:</label>
                                        <input type="month" id="toDate" class="form-control date"  name="toDate" value="{{empty(request('toDate')) ? date('Y-m') : request('toDate')}}" required/>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="date">Filter By Month</label>
                                        <input type="month" id="date" class="form-control" name="date" value="{{request('date')}}" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">Print</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{url('DirevtiveNumberReport')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @php
                        $grand_total = [];
                    @endphp
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                <button type="button" class="btn btn-success export" id="excel">Download Excel</button>
                                <button style="margin-left: 6px;" type="button" class="btn btn-danger pdf" id="pdf">Download Pdf</button>
                            </div>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card">
                            <div class="row p-1">
                                <div class="col-md-12">
                                    <h4 style="text-align: start;font-weight: bold;margin: 10px 0px 0px 0px;">Fixed Percentage Tax Directive Number Report</h4>
                                </div>
                            </div>
                            <div class="row px-1">
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Company : Homegrown Brokers (PTY) Ltd</p>
                                </div>
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Unit: All Units</p>
                                </div>
                            </div>
                            <div class="row px-1">
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Year Start Date</p>
                                </div>
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Year End Date</p>
                                </div>
                            </div>
                            <div class="row px-1">
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Level : All Level</p>
                                </div>
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Position: All Positions</p>
                                </div>
                            </div>
                            <div class="row px-1">
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Employees: All Employees</p>
                                </div>
                                <div class="col-md-4">
                                    <p style="font-weight: 600;font-size:14px;">Frequency : Monthly</p>
                                </div>
                            </div>
                            <div class="row px-1">
                                <div class="col-md-4" id="dateTimeContainer">
                                    {{-- <p style="font-weight: 600;font-size:14px;">Run on :</p> --}}
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Employee Last Name</th>
                                            <th>Init.</th>
                                            <th>Employee Number</th>
                                            <th>Tax Year</th>
                                            <th>Tax Reference Number</th>
                                            <th>Directive Number</th>
                                            <th>Amount/Percentage Type</th>
                                            <th>Directive Amount/Percentage</th>

                                            {{-- @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                                @foreach ($payroll_columns as $column)
                                                    @php
                                                        $grand_total[$column->payroll_column_id] = 0;
                                                    @endphp
                                                    <th>{{$column->name}}</th>
                                                @endforeach
                                            @endif
                                            <th>Employee Contribution</th>
                                            <th>Employer Contribution</th>
                                            <th>Total Contributions</th>  --}}
                                        </tr>
                                        <tr><th> Group: All Groups</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($departments) && count($departments) > 0)
                                            @php
                                                $sr_no = 1;
                                                $gr_total_employees = 0;
                                            @endphp
                                            @foreach ($departments as $department)
                                                @if (count($employees[$department->id]) > 0)
                                                    @php
                                                        $net_total_employees = count($employees[$department->id]);
                                                        $gr_total_employees += count($employees[$department->id]);
                                                    @endphp
                                                    <tr>
                                                        <td colspan="{{10+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                                                    </tr>
                                                    @foreach ($employees[$department->id] as $employee)
                                                        <tr>
                                                            <td>{{$employee->first_name," ",$employee->last_name}}</td>
                                                            <td>{{$employee->employee_id}}</td>
                                                            <td>{{$employee->employee_code}}</td>
                                                            <td>{{!empty($employee->eobi) ? $employee->eobi : '-'}}</td>
                                                            <td style="white-space: nowrap">{{preg_replace('~.*(\d{5})[^\d]{0,7}(\d{7})[^\d]{0,7}(\d{1}).*~', '$1-$2-$3', $employee->nic_num)}}</td>
                                                            <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                                            <td>{{$employee->father_name}}</td>
                                                            <td>{{$employee->employement_status}}</td>
                                                            <td style="white-space: nowrap">{{date('d-M-y',strtotime($employee->birthday))}}</td>
                                                            <td style="white-space: nowrap">{{date('d-M-y',strtotime($employee->joined_date))}}</td>
                                                            <td></td>
                                                            @foreach ($payroll_columns as $col)
                                                                <td>{{payrollColumnAmount($calculation_group_payroll->payroll_id,$employee->employee,$col->payroll_column_id,$col->is_decimal)}}</td>
                                                            @endforeach
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="4"></td>
                                                        <td>Sub Total Emp.:</td>
                                                        <td>{{$net_total_employees}}</td>
                                                        <td colspan="5" style="font-weight: bold;text-align:end">Sub Total:</td>
                                                        @if (count($columns_totals[$department->id]) > 0)
                                                            @foreach ($columns_totals[$department->id] as $item)
                                                                @php
                                                                    $grand_total[$item->payroll_item] += $item->amount;
                                                                @endphp
                                                                @if ($item->is_decimal == 1)
                                                                    <td>{{number_format($item->amount,1)}}</td>
                                                                @else
                                                                    <td>{{number_format($item->amount)}}</td>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @else
                                            <tr>
                                                <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="9">No Record Found!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                    <tfoot>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td>Grand Total Emp.:</td>
                                            <td>{{$gr_total_employees}}</td>
                                            <td colspan="5" style="font-weight: bold;text-align:end">Grand Total</td>
                                            @foreach ($grand_total as $total)
                                                <td>{{number_format($total)}}</td>
                                            @endforeach
                                        </tr>
                                    </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
<script>
var currentDate = new Date();
    
    // Format the date and time as desired (adjust as needed)
    var formattedDateTime = currentDate.toLocaleString(); // Change the format as needed
    
    // Find the container div by its class
    var dateTimeContainer = document.getElementById('dateTimeContainer');
    
    // Create a new paragraph element to hold the formatted date and time
    var dateTimeParagraph = document.createElement('p');
    dateTimeParagraph.style.fontWeight = '600';
    dateTimeParagraph.style.fontSize = '14px';
    
    // Set the text content of the paragraph to the formatted date and time
    dateTimeParagraph.textContent = 'Run on: ' + formattedDateTime;
    
    // Append the paragraph element to the container div
    dateTimeContainer.appendChild(dateTimeParagraph);
</script>
    <script>
        $(document).ready(function(){
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $('#get_data').submit(function(e){
                e.preventDefault();
                $('#date').text($('#joining').val());
                $('#name').text($('#section').val());
            });
        });
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var employment_status = $('#employment_status').val();
            var banksFilter = $('#banksFilter').val();
            var departmentFilter = $('#departmentFilter').val();
            var section = $('#section').select2("val");
            var order_filter = $('#order_filter').val();
            var date = $('#date').val();
            $.ajax({
                url: "{{url('uifReport')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    date:date,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter,
                    section:section,
                    order_filter:order_filter
                },
                success: function(response,status,xhr) {
                    $.unblockUI();
                        window.open(this.url);
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        $(document).on('click', '.pdf', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var employment_status = $('#employment_status').val();
            var banksFilter = $('#banksFilter').val();
            var departmentFilter = $('#departmentFilter').val();
            var section = $('#section').select2("val");
            var order_filter = $('#order_filter').val();
            var date = $('#date').val();
            $.ajax({
                url: "{{url('uifReport')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    date:date,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter,
                    section:section,
                    order_filter:order_filter
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.write(`<!DOCTYPE html>
                    <head>
                        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
                    </head>
                    <body>
                    ${WinPrint.document.write(divToPrint.outerHTML)}
                    </body>
                    </html>
                    `)
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
@endsection
