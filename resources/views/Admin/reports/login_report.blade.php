@extends('Admin.layouts.master')
@section('title', 'Login Report')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Login_Report')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Login_Report')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form" id="search_form">
                                    <div class="row">                                      
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="employeeFilter">@lang('app.User')</label>
                                            <select name="user" id="user" class="select2 form-select" data-placeholder="@lang('app.Select_User')" required>
                                                <option value=""></option>
                                                    @foreach ($users as $user)
                                                    <option value="{{$user->id}}">{{$user->username}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="fromDate">@lang('app.From_Date')</label>
                                                <input type="date" name="fromDate" id="fromDate" class="form-control"
                                                    value="{{ request('fromDate') }}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="toDate">@lang('app.To_Date')</label>
                                                <input type="date" name="toDate" id="toDate" class="form-control"
                                                    value="{{ request('toDate') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{ url('login-report') }}" type="button"
                                                class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                            <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card pb-2 table-responsive">
                            <table class="table table-bordered" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>User</th>
                                        <th>IP Address</th>
                                        <th>Login At</th>
                                        <th>Login Out</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching : true,
                ajax: {
                    url: "{{ url('login-report') }}",
                    data: function (filter) {
                        filter.user = $('#user').val();
                        filter.fromDate = $('#fromDate').val();
                        filter.toDate = $('#toDate').val();
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'username',
                        name: 'users.username',
                    },
                    {
                        data: 'ip_address',
                        name: 'authentication_log.ip_address'
                    },
                    {
                        data: 'login_at',
                        name: 'authentication_log.login_at',
                    },
                    {
                        data: 'logout_at',
                        name: 'authentication_log.logout_at',
                    },
                    // {
                    //     data: 'created_at' , render: function(data , type , row){
                    //         var date = new Date(row.created_at),
                    //                     month = String(date.getMonth() + 1).padStart(2, '0'),
                    //                     day = String(date.getDate()).padStart(2, '0'),
                    //                     year = date.getFullYear();
                    //         return year+"-"+month+"-"+day;
                    //     }
                    // },
                ], 
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">User Login Report</h6>');
        });
         // Filter Function
         $('#search_form').submit(function(e){
            dataTable.draw();
            e.preventDefault();
        });
    </script>
@endsection
