<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gross Salary Report</title>
</head>
<body>
    <div>
        <table class="table table-bordered table-striped" id="dataTable">
            <thead>
                <tr>
                    <th>Banks</th>
                    <th colspan="13">
                        @foreach ($bank_show as $item)
                            {{$item->name}} /
                        @endforeach
                    </th>
                </tr>
                <tr>
                    <th>Sr</th>
                    <th>Employee Code</th>
                    <th>Employee ID</th>
                    <th>Employee</th>
                    @if (env('COMPANY') == 'JSML')        
                        <th>Section</th>
                    @else
                        <th>Department</th>
                    @endif
                    <th>Designation</th>
                    <th>Employement Status</th>
                    <th>Bank Name</th>
                    <th>Account No.</th>
                    <th>Days</th>
                    <th>Gross Pay</th>
                    {{-- <th>Signature</th> --}}
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                    @php
                        $sr_no = 1;
                        $gross_salary_grand_total = 0;
                        $net_salary_grand_total = 0;
                        $deducation_grand_total = 0;
                    @endphp
                    @foreach ($departments as $department)
                        @if (count($employees[$department->id]) > 0)
                            <tr>
                                <td colspan="8" style="font-weight: bold">{{$department->title}}</td>
                            </tr>
                            @foreach ($employees[$department->id] as $employee)
                            <tr>
                                <td>{{$sr_no++}}</td>
                                <td>{{$employee->employee_code}}</td>
                                <td>{{$employee->employee_id}}</td>
                                <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                <td>{{$employee->department}}</td>
                                <td>{{$employee->job_title}}</td>
                                <td>{{$employee->employement_status}}</td>
                                <td>{{$employee->bank_name}}</td>
                                <td>{{$employee->account_number}}</td>
                                @if (!empty($total_days))   
                                    <td style="text-align: right">{{number_format(getColumnValue($calculation_group_payroll->payroll_id, $total_days->id,$employee->employee),1)}}</td>
                                @else
                                    <td>-</td>
                                @endif
                                @if (!empty($gross_salary))   
                                    <td style="text-align: right">{{number_format(getColumnValue($calculation_group_payroll->payroll_id, $gross_salary->id,$employee->employee))}}</td>
                                @else
                                    <td>-</td>
                                @endif
                                {{-- @if ($request->banksFilter==null) --}}
                                {{-- <td></td> --}}
                                {{-- @endif --}}
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="10" style="font-weight: bold;text-align:end">Sub Total:</td>
                                @if (!empty($gross_salary))
                                    @php
                                        $gross_salary_grand_total += !empty($sub_total_net_gross_salary[$department->id][0]->amount) ? $sub_total_net_gross_salary[$department->id][0]->amount : 0;
                                    @endphp
                                    <td style="text-align: right;">{{number_format($sub_total_net_gross_salary[$department->id][0]->amount)}}</td>
                                @else
                                    <td>-</td>
                                @endif
                            </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="3">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
            @if (isset($departments) && count($departments) > 0)
            <tfoot>
                <tr>
                    <td colspan="10" style="font-weight: bold;text-align:right">Grand Total</td>
                    <td>{{number_format($gross_salary_grand_total)}}</td>
                </tr>
            </tfoot>
            @endif
        </table>
    </div>
</body>
</html>