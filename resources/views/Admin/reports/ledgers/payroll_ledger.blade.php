@extends('Admin.layouts.master')
@section('title', 'Payroll Ledger')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Payroll_Ledger')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Payroll_Ledger')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <!--Search Form -->
                        <div class="card mb-1">
                            <form id="search_form" action="{{ url('payrollLedger') }}" method="GET">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter"
                                                class="select2 form-select" data-placeholder="@lang('app.Select_Department')">
                                                <option value=""></option>
                                                <option value="all"
                                                    {{ 'all' == request('departmentFilter') ? 'selected' : '' }}>All
                                                </option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}"
                                                        {{ $department->id == request('departmentFilter') ? 'selected' : '' }}>
                                                        {{ $department->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="employeeFilter">@lang('app.Filter_By_Employee'):</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Employee')">
                                                <option value=""></option>
                                                @if (isset($employees))
                                                    @foreach ($employees as $employee)
                                                        <option value="{{ $employee->id }}"
                                                            {{ $employee->id == request('employeeFilter') ? 'selected' : '' }}>
                                                            {{ $employee->name }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">@lang('app.Category')</label>
                                                <select name="categoryFilter" class="form-select select2"
                                                    data-placeholder="@lang('app.Select_Category')" required>
                                                    <option value=""></option>
                                                    <option value="payroll_accrual"
                                                        {{ 'payroll_accrual' == request('categoryFilter') ? 'selected' : '' }}>
                                                        Payroll Accrual</option>
                                                    <option value="pf_accrual"
                                                        {{ 'pf_accrual' == request('categoryFilter') ? 'selected' : '' }}>
                                                        PF Accrual</option>
                                                    <option value="gratuity_accrual"
                                                        {{ 'gratuity_accrual' == request('categoryFilter') ? 'selected' : '' }}>
                                                        Gratuity Accrual</option>
                                                    <option value="eobi_accrual"
                                                        {{ 'eobi_accrual' == request('categoryFilter') ? 'selected' : '' }}>
                                                        EOBI Accrual</option>
                                                    <option value="pessi_accrual"
                                                        {{ 'pessi_accrual' == request('categoryFilter') ? 'selected' : '' }}>
                                                        PESSI Accrual</option>
                                                    <option value="salary_payment"
                                                        {{ 'salary_payment' == request('categoryFilter') ? 'selected' : '' }}>
                                                        Salary Payment</option>
                                                    <option value="gratuity_payment"
                                                        {{ 'gratuity_payment' == request('categoryFilter') ? 'selected' : '' }}>
                                                        Gratuity Payment</option>
                                                    <option value="pf_payment"
                                                        {{ 'pf_payment' == request('categoryFilter') ? 'selected' : '' }}>
                                                        PF Payment</option>
                                                    <option value="loan_payment"
                                                        {{ 'loan_payment' == request('categoryFilter') ? 'selected' : '' }}>
                                                        Loan Payment</option>
                                                    <option value="advance_payment"
                                                        {{ 'advance_payment' == request('categoryFilter') ? 'selected' : '' }}>
                                                        Advance Payment</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="payrollFilter">@lang('app.Filter_By_Payroll'):</label>
                                            <select name="payrollFilter" id="payrollFilter" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Payroll')" required>
                                                <option value=""></option>
                                                @if (isset($payrolls))
                                                    @foreach ($payrolls as $payroll)
                                                        <option value="{{ $payroll->id }}"
                                                            {{ $payroll->id == request('payrollFilter') ? 'selected' : '' }}>
                                                            {{ $payroll->name }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{ url('payrollLedger') }}" type="button"
                                                class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                            <input type="submit" class="btn btn-primary mt-1" value="Apply">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @if (isset($records))
                            <div class="card pb-2">
                                @if (isset($payroll_obj))
                                    <div class="card-header">
                                        <h3 class="float-right">@lang('app.Payroll') : <small class="badeg bg-success rounded"
                                                style="padding: 4px;">{{ $payroll_obj->name }}</small></h3>
                                    </div>
                                @endif
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="deducted_report">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Employee</th>
                                                    <th>Department</th>
                                                    <th>Designation</th>
                                                    <th>Column</th>
                                                    <th>Description</th>
                                                    <th>Debit</th>
                                                    <th>Credit</th>
                                                    <th>Balance</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $debit_total = 0;
                                                    $credit_total = 0;
                                                @endphp
                                                @foreach ($records as $record)
                                                    <tr>
                                                        <td>{{ $record->trans_date }}</td>
                                                        <td>{{ $record->employee }}</td>
                                                        <td>{{ $record->department }}</td>
                                                        <td>{{ $record->designation }}</td>
                                                        <td>{{ $record->column_name }}</td>
                                                        <td>{{ $record->description }}</td>
                                                        <td style="text-align: right">
                                                            {{ number_format($record->gl_account_type == 'debit' ? $record->amount : 0) }}
                                                        </td>
                                                        <td style="text-align: right">
                                                            {{ number_format($record->gl_account_type == 'credit' ? abs($record->amount) : 0) }}
                                                        </td>
                                                        <td style="text-align: right">
                                                            {{ number_format(abs($record->balance)) }}</td>
                                                    </tr>
                                                    @php
                                                        $debit_total += $record->gl_account_type == 'debit' ? abs($record->amount) : 0;
                                                        $credit_total += $record->gl_account_type == 'credit' ? abs($record->amount) : 0;
                                                    @endphp
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td style="font-weight: bold">Ending Balance</td>
                                                    <td style="text-align: right;font-weight: bold">
                                                        {{ number_format($debit_total) }}</td>
                                                    <td style="text-align: right;font-weight: bold">
                                                        {{ number_format(abs($credit_total)) }}</td>
                                                    <td style="text-align: right;font-weight: bold">
                                                        {{ number_format(abs($debit_total - $credit_total)) }}</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'deducation_report'
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>');
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - ' + value.first_name +
                                    ' ' + value.middle_name + ' ' + value
                                    .last_name + ' - ' + value.designation)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
        });
    </script>
@endsection
