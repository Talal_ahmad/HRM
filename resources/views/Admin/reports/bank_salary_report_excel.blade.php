<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bank Salary Report</title>
</head>
<body>
    <div>
        <table>
            <thead>
                <tr>
                    <th>Sr</th>
                    <th>Employee Code</th>
                    <th>Employee ID</th>
                    <th>Name</th>
                    <th>IBAN/ Account No</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                    @php
                        $sr_no = 1;
                        $grand_total = 0;
                    @endphp
                    @foreach ($departments as $department)
                        @if (count($employees[$department->id]) > 0)
                            <tr>
                                <td style="font-weight: bold" colspan="6">{{$department->title}}</td>
                            </tr>
                            @foreach ($employees[$department->id] as $employee)
                            <tr>
                                <td>{{$sr_no++}}</td>
                                <td>{{$employee->employee_code}}</td>
                                <td>{{$employee->employee_id}}</td>
                                <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                <td>{{$employee->department}}</td>
                                <td>{{$employee->bank_name}}</td>
                                <td>{{$employee->employement_status}}</td>
                                {{-- <td style="text-align: right;">{{getColumnValue($calculation_group_payroll->payroll_id,196,$employee->employee)}}</td> --}}
                                <td style="text-align: right;">{{getMultipleColumnValue($payrol_ids_str,196,$employee->employee)}}</td>
                                @if (!empty($net_salary))   
                                    <td style="text-align: right;">{{number_format(getMultipleColumnValue($payrol_ids_str, $net_salary->id,$employee->employee))}}</td>
                                @else
                                    <td>-</td>
                                @endif
                            </tr>
                            @endforeach
                        <tr>
                            <td style="font-weight: bold;text-align:end" colspan="8">Sub Total:</td>
                            @if (count($columns_totals[$department->id]) > 0)
                                @foreach ($columns_totals[$department->id] as $item)
                                    @php
                                        $grand_total += $item;
                                    @endphp
                                    <td>{{number_format($item)}}</td>
                                @endforeach
                            @endif
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="8">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
            @if (isset($net_salary) && !empty($net_salary))
            <tfoot>
                <tr>
                    <td style="font-weight: bold;text-align:end" colspan="8">Grand Total</td>
                    <td>{{number_format($grand_total)}}</td>
                </tr>
            </tfoot>
            @endif
        </table>
    </div>
</body>
</html>