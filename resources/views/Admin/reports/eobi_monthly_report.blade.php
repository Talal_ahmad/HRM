@extends('Admin.layouts.master')
@section('title', 'EOBI Monthly Report')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.EOBI_Monthly_Report')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.EOBI_Monthly_Report')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{url('eobiMonthlyReport')}}">
                                <div class="row">
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" id="calculation_group">@lang('app.Filter_By_Calculation_Group'):</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="@lang('app.Select_Calculation_Group')" required>
                                            <option value=""></option>
                                            @foreach ($calculation_groups as $calculation_group)
                                                <option value="{{ $calculation_group->id }}" {{$calculation_group->id == request('calculation_group') ? 'selected' : ''}}>{{ $calculation_group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-md-8 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" multiple>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? in_array($department->id,request('departmentFilter')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#departmentFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div> --}}
                                    @if (env('COMPANY') == 'JSML')        
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="departmentFilter" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : ''}}>All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? $department->id == request('departmentFilter') ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                        <div class="col-md-3 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">@lang('app.Section')</label>
                                                <select name="section[]" id="section" data-placeholder="@lang('app.Select_Section')" class="select2 form-select" multiple required>
                                                    @if (!empty(request('departmentFilter')))
                                                        @foreach ($parentvar as $item)
                                                            <option value="{{$item->id}}" {{!empty(request('section')) ? in_array($item->id,request('section')) ? 'selected' : '' : ''}}>{{$item->title}}</option>
                                                        @endforeach
                                                    @endif 
                                                </select>
                                                <div class="button-container mt-1">
                                                    <button class="btn btn-sm btn-primary" type="button"
                                                        onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                    <button class="btn btn-sm btn-danger" type="button"
                                                        onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                </div>
                                            </div>
                                        </div>   
                                    @else 
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" multiple>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? in_array($department->id,request('departmentFilter')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#departmentFilter')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#departmentFilter')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" for="employeeFilter">@lang('app.Employees')</label>
                                        <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="Select Employee">
                                            <option value=""></option>
                                            {{-- @foreach (employees() as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}-{{$employee->desigination}}-{{$employee->department}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employment_status">@lang('app.Employment_Status')</label>
                                            <select name="employment_status[]" id="employment_status" class="select2 form-select" data-placeholder="@lang('app.Select_Employment_Status')" multiple>
                                                <option value=""></option>
                                                @foreach ($employment_status as $employment)
                                                    <option value="{{$employment->id}}" {{!empty(request('employment_status')) ? in_array($employment->id,request('employment_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" id="order_filter">@lang('app.Filter_By_Order'):</label>
                                        <select name="order_filter" id="order_filter" class="select2 form-select" data-placeholder="@lang('app.Select_Any_Order')">
                                            <option value=""></option>
                                                <option value="emp_id" {{request('order_filter') == 'emp_id' ? 'selected' : ''}}>By Employee ID</option>
                                                <option value="emp_code" {{request('order_filter') == 'emp_code'? 'selected' : ''}}>By Employee Code</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="fromDate">@lang('app.From_Date'):</label>
                                        <input type="month" id="fromDate" class="form-control date"  name="fromDate" value="{{empty(request('fromDate')) ? date('Y-m') : request('fromDate')}}" required/>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="toDate">@lang('app.To_Date'):</label>
                                        <input type="month" id="toDate" class="form-control date"  name="toDate" value="{{empty(request('toDate')) ? date('Y-m') : request('toDate')}}" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">@lang('app.Print')</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{url('eobiMonthlyReport')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @php
                        $grand_total = [];
                    @endphp
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                <button type="button" class="btn btn-success export" id="excel">@lang('app.Download_Excel')</button>
                                <button style="margin-left: 6px;" type="button" class="btn btn-danger pdf" id="pdf">@lang('app.Download_Pdf')</button>
                            </div>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card">
                            <div class="row p-1">
                                <div class="col-md-4">
                                    @if (!empty($company_setup->logo))
                                        <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
                                    
                                    @else
                                        <img src="{{asset('images/company_logo/')}}" alt="Logo">
                                        
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <h4 style="text-align: center;font-weight: bold;margin: 10px 0px 0px 0px;">Employee Old Age Benefits Institution <br> Monthly Contribution Schedule
                                        @if (!empty($fromDate))
                                            <br>From: {{$fromDate}}
                                            To: {{$toDate}}
                                        @endif
                                    </h4>
                                </div>
                                <div class="col-md-4">
                                    <h4 style="font-weight: bold;text-align:end;text-decoration: underline;text-decoration-thickness: 3px;">EOBI-PR-02-M</h4>
                                </div>
                            </div>
                            <div class="row p-1">
                                <div class="col-md-3">
                                    <p style="font-weight: 600;font-size:14px;">Registration No : BEB00002fs</p>
                                </div>
                                <div class="col-md-6">
                                    <p style="font-weight: 600;font-size:16px;">Statement of Employees for the Month of : &emsp;&emsp;<span style="text-decoration: underline;text-decoration-thickness: 3px;">{{$year.'-'.$month}}</span></p>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr</th>
                                            <th>ID</th>
                                            <th>Employee Code</th>
                                            <th>EOBI No</th>
                                            <th>CNIC</th>
                                            <th>Name of IP</th>
                                            <th>Father's Name</th>
                                            <th>Employment Status</th>
                                            <th>DOB</th>
                                            <th>DOJ</th>
                                            <th>DOE</th>
                                            @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                                @foreach ($payroll_columns as $column)
                                                    @php
                                                        $grand_total[$column->payroll_column_id] = 0;
                                                    @endphp
                                                    <th>{{$column->name}}</th>
                                                @endforeach
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($departments) && count($departments) > 0)
                                            @php
                                                $sr_no = 1;
                                                $gr_total_employees = 0;
                                            @endphp
                                            @foreach ($departments as $department)
                                                @if (count($employees[$department->id]) > 0)
                                                    @php
                                                        $net_total_employees = count($employees[$department->id]);
                                                        $gr_total_employees += count($employees[$department->id]);
                                                    @endphp
                                                    <tr>
                                                        <td colspan="{{10+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                                                    </tr>
                                                    @foreach ($employees[$department->id] as $employee)
                                                        <tr>
                                                            <td>{{$sr_no++}}</td>
                                                            <td>{{$employee->employee_id}}</td>
                                                            <td>{{$employee->employee_code}}</td>
                                                            <td>{{!empty($employee->eobi) ? $employee->eobi : '-'}}</td>
                                                            <td style="white-space: nowrap">{{preg_replace('~.*(\d{5})[^\d]{0,7}(\d{7})[^\d]{0,7}(\d{1}).*~', '$1-$2-$3', $employee->nic_num)}}</td>
                                                            <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                                            <td>{{$employee->father_name}}</td>
                                                            <td>{{$employee->employement_status}}</td>
                                                            <td style="white-space: nowrap">{{$employee->birthday}}</td>
                                                            <td style="white-space: nowrap">{{$employee->joined_date}}</td>
                                                            <td></td>
                                                            @foreach ($payroll_columns as $col)
                                                                <td>{{payrollColumnAmount($payrollIds,$employee->employee,$col->payroll_column_id,$col->is_decimal,$request->employment_status)}}</td>
                                                            @endforeach
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="4"></td>
                                                        <td>Sub Total Emp.:</td>
                                                        <td>{{$net_total_employees}}</td>
                                                        <td colspan="5" style="font-weight: bold;text-align:end">Sub Total:</td>
                                                        @if (count($columns_totals[$department->id]) > 0)
                                                            @foreach ($columns_totals[$department->id] as $item)
                                                                @php
                                                                    $grand_total[$item->payroll_item] += $item->amount;
                                                                @endphp
                                                                @if ($item->is_decimal == 1)
                                                                    <td>{{number_format($item->amount,1)}}</td>
                                                                @else
                                                                    <td>{{number_format($item->amount)}}</td>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @else
                                            <tr>
                                                <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="9">No Record Found!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                    <tfoot>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td>Grand Total Emp.:</td>
                                            <td>{{$gr_total_employees}}</td>
                                            <td colspan="5" style="font-weight: bold;text-align:end">Grand Total</td>
                                            @foreach ($grand_total as $total)
                                                <td>{{number_format($total)}}</td>
                                            @endforeach
                                        </tr>
                                    </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                        <div class="row mb-1" id="signatures">
                            <div class="col-4">
                                <p class="h4">Note:</p>
                                <p>Errors & Omissions are expected, that may be rectified upon intimation. Date of Exit (DOE) may not be treated as final date, being Seasonal Factory.</p>
                            </div>
                            <div id="sign1" class="col-4 mt-3 d-flex flex-column">
                                <input type="text" value="{{$company_setup->first_name}} {{$company_setup->last_name}}" style="border-width: 0 0 1px 0;" class="sign text-center">
                                <label for="sign1" class="text-center">D.G.M (HR & Comp.)</label>
                            </div>
                            <div id="sign2" class="col-4 mt-3 d-flex flex-column">
                                <input type="text" value="{{$company_setup->gm_fName}} {{$company_setup->gm_lName}}" style="border-width: 0 0 1px 0;" class="sign text-center">
                                <label for="sign2" class="text-center">G.M. (Operations)</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $("#department").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type: 'loan_request'
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.employee_code+ ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation+' - '+ value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            // $('#get_data').submit(function(e){
            //     e.preventDefault();
            //     $('#date').text($('#joining').val());
            //     $('#name').text($('#section').val());
            // });
        });
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var employment_status = $('#employment_status').val();
            var banksFilter = $('#banksFilter').val();
            var departmentFilter = $('#departmentFilter').val();
            var section = $('#section').select2("val");
            var order_filter = $('#order_filter').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var employeeFilter = $('#employeeFilter').val();

            $.ajax({
                url: "{{url('eobiMonthlyReport')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    fromDate:fromDate,
                    toDate:toDate,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter,
                    section:section,
                    employeeFilter:employeeFilter,
                    order_filter:order_filter
                },
                success: function(response,status,xhr) {
                    $.unblockUI();
                        window.open(this.url);
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        $(document).on('click', '.pdf', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var calculation_group = $('#calculation_group').select2("val");
            var bank_status = $('#bank_status').select2("val");
            var employment_status = $('#employment_status').val();
            var banksFilter = $('#banksFilter').val();
            var departmentFilter = $('#departmentFilter').val();
            var section = $('#section').select2("val");
            var order_filter = $('#order_filter').val();
            var fromDate = $('#fromDate').val();
            var employeeFilter = $('#employeeFilter').val();
            var toDate = $('#toDate').val();
            $.ajax({
                url: "{{url('eobiMonthlyReport')}}",
                type: "GET",
                data: {
                    export_type:export_type,
                    calculation_group:calculation_group,
                    banksFilter:banksFilter,
                    fromDate:fromDate,
                    toDate:toDate,
                    employment_status:employment_status,
                    bank_status:bank_status,
                    departmentFilter:departmentFilter,
                    section:section,
                    employeeFilter:employeeFilter,
                    order_filter:order_filter
                }, 
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.write(`<!DOCTYPE html>
                    <head>
                        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
                    </head>
                    <body>
                    ${WinPrint.document.write(divToPrint.outerHTML)}
                    </body>
                    </html>
                    `)
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
@endsection
