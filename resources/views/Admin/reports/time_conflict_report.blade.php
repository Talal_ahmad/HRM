@extends('Admin.layouts.master')
@section('title', 'Time Conflict Report')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Time Conflict Report</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item active">Time Conflict Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form" id="search_form">
                                    <div class="row">                                      
                                        <div class="col-md-3 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">@lang('app.Department')</label>
                                                <select name="department" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                    <option value="">Select Department</option>
                                                    <option value="all">All</option>
                                                    @foreach (departments() as $department)
                                                        <option value="{{$department->id}}">{{$department->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="title">@lang('app.Employees')</label>
                                                <select name="employee" id="employee" data-placeholder="@lang('app.Select_Employee')" class="select2 form-select">
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="fromDate">@lang('app.From_Date')</label>
                                                <input type="date" name="fromDate" id="fromDate" class="form-control"
                                                    value="{{ request('fromDate') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="toDate">@lang('app.To_Date')</label>
                                                <input type="date" name="toDate" id="toDate" class="form-control"
                                                    value="{{ request('toDate') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{ url('time_conflict_report') }}" type="button"
                                                class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                            <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card pb-2 table-responsive">
                            <table class="table table-bordered" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr No.</th>
                                        <th>Name</th>
                                        <th class="not_include"></th>
                                        <th class="not_include"></th>
                                        <th>Department</th>
                                        <th>Date</th>
                                        <th>Check In</th>
                                        <th>Updated(IN)</th>
                                        <th>Check Out</th>
                                        <th>Updated(OUT)</th>
                                        <th>Conflict(IN)</th>
                                        <th>Conflict(OUT)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employee').empty();
                            $('#employee').html('<option value="">Select Employee</option>'); 
                            $.each(response, function(index, value) {
                                $('#employee').append(
                                    $('<option></option>').val(value.id).html(
                                        value.employee_id + ' - ' + value.first_name +' '+value.middle_name +' '+ value.last_name + ' - ' + value.designation)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employee').empty();
                }
            });
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching : true,
                ajax: {
                    url: "{{ url('time_conflict_report') }}",
                    data: function (filter) {
                        filter.department = $('#department').val();
                        filter.employee = $('#employee').val();
                        filter.fromDate = $('#fromDate').val();
                        filter.toDate = $('#toDate').val();
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        render: function(data, type, full, meta) {
                            var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                            var lastName = full['last_name'] != null ? full['last_name'] : '';
                            return full['first_name'] + ' ' + middleName+ ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'title',
                        name: 'companystructures.title'
                    },
                    {
                        data: 'in_time',
                        name: 'attendance.in_time',
                        render: function(data) {
                            return new Date(data).toLocaleDateString(); // Format the date as a local date string
                        }
                    },
                    {
                        data: 'in_time',
                        name: 'attendance.in_time',
                        render: function(data) {
                            return new Date(data).toLocaleString(); // Format the date as a local date and time string
                        }
                    },
                    {
                        data: 'created_at',
                        name: 'attendance.created_at',
                        render: function(data) {
                            return new Date(data).toLocaleString(); // Format the date as a local date and time string
                        }
                    },
                    {
                        data: 'out_time',
                        name: 'attendance.out_time',
                        render: function(data) {
                            return new Date(data).toLocaleString(); // Format the date as a local date and time string
                        }
                    },
                    {
                        data: 'updated_at',
                        name: 'attendance.updated_at',
                        render: function(data) {
                            return new Date(data).toLocaleString(); // Format the date as a local date and time string
                        }
                    },
                    {
                        render: function (data, type, row) {
                            var startTime = new Date(row.in_time);
                            var endTime = new Date(row.created_at);
                            var distanceInMinutes = Math.abs((endTime - startTime) / (1000 * 60)); // Calculate distance in minutes

                            // Check if distanceInMinutes is NaN
                            if (isNaN(distanceInMinutes)) {
                                return '00 min'; // Display '00 minutes' for NaN
                            } else {
                                return distanceInMinutes.toFixed(2) + ' min'; // Display the result with two decimal places
                            }
                        }
                    },
                    {
                        render: function (data, type, row) {
                            var startTime = new Date(row.out_time);
                            var endTime = new Date(row.updated_at);
                            var distanceInMinutes = Math.abs((endTime - startTime) / (1000 * 60)); // Calculate distance in minutes

                            // Check if distanceInMinutes is NaN
                            if (isNaN(distanceInMinutes)) {
                                return '00 min'; // Display '00 minutes' for NaN
                            } else {
                                return distanceInMinutes.toFixed(2) + ' min'; // Display the result with two decimal places
                            }
                        }
                    },
                ], 
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">Time Conflict Report</h6>');
        });
         // Filter Function
         $('#search_form').submit(function(e){
            dataTable.draw();
            e.preventDefault();
        });
    </script>
@endsection
