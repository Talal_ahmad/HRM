<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pessi Monthly Report</title>
</head>
<body>
    <div>
        @php
            $grand_total = [];
        @endphp
        <table border=".5pt">
            <thead>
                <tr>
                    <th style="text-align: center;" colspan="2">Month : {{$date}}</th>
                    <th style="text-align: center;" colspan="2">Calculation Group : {{$cal_group}}</th>
                    <th style="text-align: center;" colspan="2">Printed By : {{auth()->user()->username}}</th>
                    <th style="text-align: center;" colspan="2">Printed At : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    <th>Sr</th>
                    <th>ID</th>
                    <th>Employee Code</th>
                    <th>Name of IP</th>
                    <th>Father's Name</th>
                    <th>Desigination</th>
                    <th>CNIC</th>
                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                        @foreach ($payroll_columns as $column)
                            @php
                                $grand_total[$column->payroll_column_id] = 0;
                            @endphp
                            <th>{{$column->name}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                @php
                    $sr_no = 1;
                    $gr_total_employees = 0;
                @endphp
                @foreach ($departments as $department)
                    @if (count($employees[$department->id]) > 0)
                        @php
                            $net_total_employees = count($employees[$department->id]);
                            $gr_total_employees += count($employees[$department->id]);
                        @endphp
                        <tr>
                            <td colspan="{{7+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                        </tr>
                        @foreach ($employees[$department->id] as $employee)
                            <tr>
                                <td>{{$sr_no++}}</td>
                                <td>{{$employee->employee_id}}</td>
                                <td>{{HandleEmpty($employee->employee_code)}}</td>
                                <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                                <td>{{$employee->father_name}}</td>
                                <td>{{$employee->employement_status}}</td>
                                <td>{{$employee->desigination}}</td>
                                <td style="white-space: nowrap">{{preg_replace('~.*(\d{5})[^\d]{0,7}(\d{7})[^\d]{0,7}(\d{1}).*~', '$1-$2-$3', $employee->nic_num)}}</td>
                                @foreach ($payroll_columns as $col)
                                    <td>{{payrollColumnAmount($payrollIds,$employee->employee,$col->payroll_column_id,$col->is_decimal,$request->employment_status)}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    <tr>
                        <td colspan="3">Sub Total Emp.:</td>
                        <td>{{$net_total_employees}}</td>
                        <td colspan="4" style="font-weight: bold;text-align:end">Sub Total:</td>
                        @foreach ($columns_totals[$department->id] as $item)
                            @php
                                $grand_total[$item->payroll_item] += $item->amount;
                            @endphp
                            @if ($item->is_decimal == 1)
                                <td>{{number_format($item->amount,1)}}</td>
                            @else
                                <td>{{number_format($item->amount)}}</td>
                            @endif
                        @endforeach
                    </tr>
                    @endif
                @endforeach
            @else
                <tr>
                    <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="7">No Record Found!</td>
                </tr>
            @endif
            </tbody>
            @if (isset($payroll_columns) && count($payroll_columns) > 0)
            <tfoot>
                <tr>
                    <td colspan="3" style="font-weight: bold;">Grand Total Emp.:</td>
                    <td>{{$gr_total_employees}}</td>
                    <td colspan="4" style="font-weight: bold;text-align:end">Grand Total</td>
                    @foreach ($grand_total as $total)
                        <td>{{number_format($total)}}</td>
                    @endforeach
                </tr>
            </tfoot>
            @endif
        </table>
    </div>
</body>
</html>