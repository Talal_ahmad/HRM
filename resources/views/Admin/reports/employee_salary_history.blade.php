@extends('Admin.layouts.master')
@section('title', 'Employee Salary History')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Employee_Salary_History')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.reports')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Employee_Salary_History')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">@lang('app.Department')</label>
                                            <select name="department" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value="">Select Department</option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">@lang('app.Employees')</label>
                                            <select name="employee" id="employee" data-placeholder="@lang('app.Select_Employee')" class="select2 form-select" required>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee">Filter By Employee</label>
                                            <select name="employee_id" id="employee_id" class="select2 form-select" data-placeholder="Select Employee" required>
                                                <option value=""></option>
                                                @foreach ($employees as $emp)
                                                    <option value="{{$emp->id}}">{{$emp->first_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-12 col-md-6">
                                        <label class="form-label" for="fromDate">@lang('app.From_Date'):</label>
                                        <input type="text" name="fromDate" id="fromDate" class="form-control flatpickr-basic" value="{{request('fromDate')}}" placeholder="YYYY-MM-DD" required>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label class="form-label" for="toDate">@lang('app.To_Date'):</label>
                                        <input type="text" name="toDate" id="toDate" class="form-control flatpickr-basic" value="{{request('toDate')}}" placeholder="YYYY-MM-DD" required>
                                    </div>
                                    <div class="col-md-1" style="margin-top: 23px;">
                                        <button type="button" class="btn btn-primary" id="salary_history_submit">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card">
                            <div class="row p-1">
                                <div class="col-md-4">
                                    @if (!empty($company_setup->logo))
                                        <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
                                    @else
                                        <img src="{{asset('images/company_logo/')}}" alt="Logo">
                                        
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <h4 style="text-align: center;font-weight: bold;margin: 10px 0px 0px 0px;">Employee Salary History Report</h4>
                                </div>
                                {{-- <div class="col-md-4">
                                    <h4 style="font-weight: bold;text-align:end;text-decoration: underline;text-decoration-thickness: 3px;">EOBI-PR-02-M</h4>
                                </div> --}}
                            </div>
                            <div class="card pb-2">
                                <div class="table-responsive" id="salary_history">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employee').empty();
                            $('#employee').html('<option value="">Select Employee</option>'); 
                            $.each(response, function(index, value) {
                                $('#employee').append(
                                    $('<option></option>').val(value.id).html(
                                        value.employee_id + ' - ' + value.first_name +' '+value.middle_name +' '+ value.last_name + ' - ' + value.designation)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employee').empty();
                }
            });
            $('#get_data').submit(function(e){
                e.preventDefault();
                $('#date').text($('#joining').val());
                $('#name').text($('#employee').val());
            });
        });
        // var employee_id = <?php echo json_encode(5); ?>;
// console.log(employee_id);
        $('#salary_history_submit').on('click', function(e) {
        e.preventDefault();
        blockUI();
        var payroll_id = $('#payrollFilter').val();
        var employee_id = $('#employee').val();
        console.log(employee_id);
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        $.ajax({
            url: "{{route('staff_directory.create')}}",
            type: "GET",
            data: {
                fromdate:fromDate,
                todate:toDate,
                payroll_id: payroll_id,
                employee: employee_id
            },
            success: function(response) {
                $.unblockUI();
                $("#salary_history").empty();
                var html = '';
                if(response.data != ''){
                    html += `<table class="table table-bordered" id="salary_history_table">
                            <thead>
                                <tr><th>Payroll</th>`;
                                    $.each(response.columns, function(index, value) {
                                        html += `<th>${value.name}</th>`;
                                    });
                            html += `</tr>
                            </thead>
                            <tbody>`;
                                $.each(response.data, function(payroll, columns) {
                                    html += `<tr><td>${payroll}</td>`;
                                    $.each(columns, function(index, value) {
                                        html += `<td>${value}</td>`;
                                    });
                                    html += `</tr>`;
                                })
                    html += `<tbody></table>`;
                }
                else{
                    html += `<div class="text-center"><h3>No Record Found!</h3></div>`
                }
                $("#salary_history").append(html);
                // $('#salary_history_table').DataTable();
                var from_date = '{!! request()->query('fromDate') !!}';
            var to_date = '{!! request()->query('toDate') !!}';
            $('#salary_history_table').DataTable({
                ordering: true,
                "columnDefs": [
					{
						// For Responsive
						className: 'control',
						orderable: false,
						targets: 0
					},
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',


                            customize: function (doc) {
                                  //Remove the title created by datatTables
                                  doc.content.splice(0,1);
                                  var now = new Date();
                                  var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                                  doc.pageMargins = [5,60,5,5];
                                  doc.defaultStyle.margin = 0;
                                  doc.defaultStyle.fontSize = 8;
                                  doc.styles.tableHeader.fontSize = 8;
 
                                  doc['header']=(function() {
                                      return {
                                          columns: [
 
                                              {
                                                  alignment: 'left',
                                                  italics: true,
                                                  text: 'Employee Salary History Table ' + now,
                                                  fontSize: 10,
                                                  margin: [10,0]
                                              },
                                              {
                                                  alignment: 'center',
                                                  fontSize: 14,
                                                  text: 'Employee Salary History Report'
                                              }
                                          ],
                                          margin: 20
                                      }
                                  });
 
                                  doc['footer']=(function(page, pages) {
                                      return {
                                          columns: [
                                              {
                                                  alignment: 'left',
                                                  text: ['Created on: ', { text: jsDate.toString() }]
                                              },
                                              {
                                                  alignment: 'right',
                                                  text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                                              }
                                          ],
                                          margin: 20
                                      }
                                  });
 
                                  var objLayout = {};
                                  objLayout['hLineWidth'] = function(i) { return .5; };
                                  objLayout['vLineWidth'] = function(i) { return .5; };
                                  objLayout['hLineColor'] = function(i) { return '#aaa'; };
                                  objLayout['vLineColor'] = function(i) { return '#aaa'; };
                                  objLayout['paddingLeft'] = function(i) { return 4; };
                                  objLayout['paddingRight'] = function(i) { return 4; };
                                  doc.content[0].layout = objLayout;
                          },


                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employee Salary History',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                // responsive: {
				// 	details: {
				// 		display: $.fn.dataTable.Responsive.display.childRowImmediate,
				// 		type: 'column',
				// 	}
				// },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            },
        })
    })
    </script>
@endsection
