<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        tr {
            border: 1px solid black;
        }
        table{
            border-collapse: collapse;
            width: 100%;
        }
        thead tr>th {
            vertical-align: middle !important;
        }
        tr td, tr th{
            border: 1px solid black;
            padding: 8px !important;
        }
    </style>
</head>
<body>
    <table style="border: 1px solid black; white-space:nowrap;">
        <thead>
            <tr rowspan="2" style="background: #f3f2f7;text-align:center">
                <th style="font-size: 20px" colspan="9">{{ env('COMPANY') }} (PVT) Ltd<br>
                <span style="font-size: 16px">Leave Encashment Report</span></th>
            </tr>
            <tr style="background: #f3f2f7">
                <th>Sr.No</th>
                <th>Employee Code</th>
                <th>Employee</th>
                <th>Department</th>
                <th>Basic Salary</th>
                <th>Total Annual</th>
                <th>Availed Annual</th>
                <th>Remaining</th>
                <th>Encashment</th>
            </tr>
        </thead>
        <tbody>
            @if (isset($employees) && count($employees) > 0)
                @foreach ($employees as $key => $employee)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{ HandleEmpty($employee->employee_code) }}</td>
                        <td>{{ $employee->first_name }} {{ $employee->middle_name }}
                            {{ $employee->last_name }} - {{ $employee->designation }}
                        </td>
                        <td>{{ $employee->department }}</td>
                            @php
                                $leave_record = checkLeaveBalance($employee->id, $annual_leave_type->id, request('periodFilter'));
                                $basicSalary = getBasicSalaryFromLatestPayrool($employee->id,$employee->department_id);
                                $remaining = $leave_record['total_balance'] - $leave_record['leaves_taken'];
                            @endphp
                        <td>{{$basicSalary}}</td>
                        <td>{{ $leave_record['total_balance'] }}</td>
                        <td>{{ $leave_record['leaves_taken'] }}</td>
                        <td>{{ $remaining }}</td>
                        <td>{{round(($basicSalary/30)*$remaining,2)}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9" class="text-center">No Record Found</td>
                </tr>
            @endif
        </tbody>
    </table>
</body>
</html>
