<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Eobi Monthly Report</title>
    <head>
        <style>
            #salary_sheet {
                border-collapse: collapse;
                width: 100%;
                font-family:'Segoe UI',sans-serif
            }

            #salary_sheet td, #salary_sheet th {
                border: 1px solid #ddd;
                padding: 5px;
                font-size: 100px;
                width: 5px;
            }

            #salary_sheet td{
                padding: 30px;
            }
            h1 {
                font-size: 200px;
                text-align: center;
                color: black;
                /* background-color: #356e9c; */
            }
            #salary_sheet tr:nth-child(even){background-color: #f2f2f2;}

            #salary_sheet tr:hover {background-color: #ddd;}

            #salary_sheet th {
                padding-top: 12px;
                padding-bottom: 12px;
                background-color: #2e475c;
                color: white;
            }
            img {
                height: 700px;
                width: 20%;
            }
            .txt_rgt{
                text-align: right;
            }
        </style>
    </head>
</head>
<body>
    <div>
        @if (!empty($company_setup->logo))
            <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" alt="Logo">
        @else
            <img src="{{asset('images/company_logo/')}}" alt="Logo">
            
        @endif
        <h1>Eobi Monthly Report</h1>
        <table class="table table-bordered table-striped" id="salary_sheet">
            <thead>
                <tr>
                    <th>Month : {{$date}}</th>
                    <th colspan="6">Calculation Group : {{$cal_group}}</th>
                    <th colspan="2">Printed By : {{auth()->user()->username}}</th>
                    <th colspan="6">Printed At : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    <th>Sr</th>
                    <th>ID</th>
                    <th>Employee Code</th>
                    <th>EOBI No</th>
                    <th>CNIC</th>
                    <th>Name of IP</th>
                    <th>Father's Name</th>
                    <th>DOB</th>
                    <th>DOJ</th>
                    <th>DOE</th>
                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                        @foreach ($payroll_columns as $column)
                            @php
                                $grand_total[$column->payroll_column_id] = 0;
                            @endphp
                            <th>{{$column->name}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($departments) && count($departments) > 0)
                    @php
                        $sr_no = 1;
                        $gr_total_employees = 0;
                    @endphp
                    @foreach ($departments as $department)
                        @if (count($employees[$department->id]) > 0)
                            @php
                                $net_total_employees = count($employees[$department->id]);
                                $gr_total_employees += count($employees[$department->id]);
                            @endphp
                            <tr>
                                <td colspan="{{9+count($payroll_columns)}}" style="font-weight: bold">{{$department->title}}</td>
                            </tr>
                            @foreach ($employees[$department->id] as $employee)
                                <tr>
                                    <td>{{$sr_no++}}</td>
                                    <td>{{$employee->employee_id}}</td>
                                    <td>{{$employee->employee_code}}</td>
                                    <td>{{!empty($employee->eobi) ? $employee->eobi : '-'}}</td>
                                    <td style="white-space: nowrap">{{preg_replace('~.*(\d{5})[^\d]{0,7}(\d{7})[^\d]{0,7}(\d{1}).*~', '$1-$2-$3', $employee->nic_num)}}</td>
                                    <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                    <td>{{$employee->father_name}}</td>
                                    <td style="white-space: nowrap">{{date('d-M-y',strtotime($employee->birthday))}}</td>
                                    <td style="white-space: nowrap">{{date('d-M-y',strtotime($employee->joined_date))}}</td>
                                    <td></td>
                                    @foreach ($payroll_columns as $col)
                                        <td>{{payrollColumnAmount($calculation_group_payroll->payroll_id,$employee->employee,$col->payroll_column_id,$col->is_decimal)}}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        <tr>
                            <td colspan="4"></td>
                            <td style="font-weight: bold;">Sub Total Emp.:</td>
                            <td>{{$net_total_employees}}</td>
                            <td colspan="4" style="font-weight: bold;text-align:end">Sub Total:</td>
                            @if (count($columns_totals[$department->id]) > 0)
                                @foreach ($columns_totals[$department->id] as $item)
                                    @php
                                        $grand_total[$item->payroll_item] += $item->amount;
                                    @endphp
                                    @if ($item->is_decimal == 1)
                                        <td>{{number_format($item->amount,1)}}</td>
                                    @else
                                        <td>{{number_format($item->amount)}}</td>
                                    @endif
                                @endforeach
                            @endif
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td style="font-size: 18px;font-weight: bold;text-align:center" colspan="9">No Record Found!</td>
                    </tr>
                @endif
            </tbody>
            @if (isset($payroll_columns) && count($payroll_columns) > 0)
                <tfoot>
                    <tr>
                        <td colspan="4"></td>
                        <td style="font-weight: bold;">Grand Total Emp.:</td>
                        <td>{{$gr_total_employees}}</td>
                        <td colspan="4" style="font-weight: bold;text-align:end">Grand Total</td>
                        @foreach ($grand_total as $total)
                            <td>{{number_format($total)}}</td>
                        @endforeach
                    </tr>
                </tfoot>
                @endif
        </table>
    </div>
</body>
</html>