@extends('Admin.layouts.master')
@section('title', 'Payslip Templates')
@section('style')
<style>
    .select2-search__field{
        width: initial !important;
    }
</style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">PaySlip Template</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Payslip Templates
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Title</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
    
            <!--Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add PaySlip Template</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Name</label>
                                            <input type="text" id="title" class="form-control" placeholder="Name" name="name" required/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Add Net Salary Column</label>
                                            <select name="net_column" id="net_column" class="select2 form-select" data-placeholder="Select Add Net Salary Column">
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Add Gross Salary Column</label>
                                            <select name="gross_column" id="gross_columns" class="select2 form-select" data-placeholder="Select Add Gross Salary Column">
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Add Basic Salary Column</label>
                                            <select name="basic_column" id="add_columns" class="select2 form-select" data-placeholder="Select Add Basic Salary Column">
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Add Columns</label>
                                            <select name="add_columns[]" id="add_columns" class="select2 form-select" data-placeholder="Select Add Columns" multiple required>
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Deduct Columns</label>
                                            <select name="deduct_columns[]" id="deduct_columns" class="select2 form-select" data-placeholder="Select Deduct Columns" multiple required>
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End Add Modal -->
    
            <!--start edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit PaySlip Template</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Name</label>
                                            <input type="text" id="edit_name" class="form-control" placeholder="Name" name="name" required/>
                                        </div>
                                    </div>


                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Add Net Salary Column</label>
                                            <select name="net_column" id="edit_net_columns" class="select2 form-select"  data-placeholder="Select Add Net Salary Column">
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Add Gross Salary Column</label>
                                            <select name="gross_column" id="edit_gross_columns" class="select2 form-select" data-placeholder="Select Add Gross Salary Column">
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Add Basic Salary Column</label>
                                            <select name="basic_column" id="edit_basic_columns" class="select2 form-select" data-placeholder="Select Add Basic Salary Column">
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Add Columns</label>
                                            <select name="add_columns[]" id="edit_add_columns" class="select2 form-select" data-placeholder="Select Add Columns" multiple required>
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">Deduct Columns</label>
                                            <select name="deduct_columns[]" id="edit_deduct_columns" class="select2 form-select" data-placeholder="Select Deduct Columns" multiple required>
                                                <option value=""></option>
                                                @foreach ($payroll_columns as $payroll_column)
                                                    <option value="{{$payroll_column->id}}">{{$payroll_column->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End edit Modal -->
        </div>
    </section>
@endsection
@section('scripts')
    <script>

        var rowid;
        $(document).ready(function(){
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('payslip_template.index') }}",
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" onclick="delete_item('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'+
                                '<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#edit_modal" onclick="edit('+full.id+')">' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of PaySlip Templates</h6>');

            // Add Data
            $('#add_form').on('submit' , function(e){
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{route('payslip_template.store')}}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#add_modal").modal("hide");
                            document.getElementById("add_form").reset();
                            $(".select2").val('').trigger('change')
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'PaySlip Template has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            // Update Data
            $('#edit_form').on('submit' , function(e){
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{url('payslip_template')}}" + "/" + rowid,
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response){
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Payslip Template has been Updated Successfully!'
                            })
                        }
                    }
                });
            })
        }); 

        // Edit Function
        function edit(id){
            rowid = id;
            $.ajax({
                url: "{{url('payslip_template')}}" + "/" + id + "/edit",
                type: "GET",
                success: function(response){
                    var columns = JSON.parse(response.add_columns);
                    var ded_columns = JSON.parse(response.deduct_columns);
                    console.log(response);
                    $('#edit_name').val(response.name);
                    $('#edit_net_columns').val(response.net_salary).select2({
                        dropdownParent: $('#edit_modal')
                    });
                    $('#edit_gross_columns').val(response.gross_salary).select2({
                        dropdownParent: $('#edit_modal')
                    });
                    $('#edit_basic_columns').val(response.basic_salary).select2({
                        dropdownParent: $('#edit_modal')
                    });
                    $("#edit_deduct_columns").val(ded_columns).select2();
                    $("#edit_add_columns").val(columns).select2();
                }
            });
        }
        // Delete Function
        function delete_item(id){
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "payslip_template/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else{
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'PaySlip Template has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }

    </script>
@endsection