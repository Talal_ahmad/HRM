@extends('Admin.layouts.master')
@section('title', 'Salary Plans')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Salary Plans</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Salary Plans
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit Salary Plans</h4>
                        </div>
                        <div class="card-body">
                            <form class="form" id="edit_form" method="POST" action="{{url('salary_plan/'.$package->id)}}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Name</label>
                                            <input type="text" id="type" class="form-control" value="{{$package->name}}" name="package_name" required>
                                        </div>
                                    </div> 
                                    @foreach ($components as $key => $component)    
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="{{$key}}">{{$component->component_name}}</label>
                                                <input type="number" id="{{$key}}" class="form-control" value="{{$component->amount}}" name="component[{{$component->component_id}}]" required>
                                            </div>
                                        </div>  
                                    @endforeach
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary me-1">Update</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection