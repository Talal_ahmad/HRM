@extends('Admin.layouts.master')
@section('title', 'Add Payroll Column')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Add Payroll Column</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Payroll Columns</a>
                                </li>
                                <li class="breadcrumb-item active">Create
                                </li>
                            </ol>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="add_payroll_col" action="{{route('payrollColumns.store')}}" class="form" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="name">Name</label>
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" required/>
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="deduction_group">Calculation Groups</label>
                                            <select type="select-multi" name="deduction_group" id="deduction_group" class="select2 form-select @error('deduction_group') is-invalid @enderror" data-placeholder="Select Calculation Group" required>
                                                <option value=""></option>
                                                @foreach ($calculationGroup as $group)
                                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('deduction_group')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="salary_components">Salary Components</label>
                                            <select type="select-multi" name="salary_components[]" id="salary_components" class="select2 form-select" data-placeholder="Select Salary Component" multiple>
                                                <option value=""></option>
                                                @foreach ($salaryComponents as $component)
                                                    <option value="{{ $component->id }}">{{ $component->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#salary_components')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#salary_components')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="deductions">Calculation Methods</label>
                                            <select type="select-multi" name="deductions[]" id="deductions" class="select2 form-select"
                                                data-placeholder="Select Calculation Method" multiple>
                                                <option value=""></option>
                                                @foreach ($calculationMethods as $method)
                                                    <option value="{{ $method->id }}">{{ $method->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#deductions')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#deductions')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="column_order">Column Order</label>
                                            <input type="text" name="column_order" id="column_order" class="form-control @error('column_order') is-invalid @enderror" placeholder="Column Order" validation="number" required/>
                                            @error('column_order')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="salary_column_type">Salary Column Type</label>
                                            <select type="select-multi" name="salary_column_type" id="salary_column_type" class="select2 form-select" data-placeholder="Select Column Type" required>
                                                <option value=""></option>
                                                <option value="daily_rate">Daily Rate</option>
                                                <option value="basic_salary">Basic Salary</option>
                                                <option value="gross_salary">Gross Salary</option>
                                                <option value="allownces">Allownces</option>
                                                <option value="net_Salary">Net Salary</option>
                                                <option value="deduction">Deduction</option>
                                                <option value="working_days">Working Days</option>
                                                <option value="total_deductions">Total Deductions</option>
                                                <option value="total_allowances">Total Allowances</option>
                                                <option value="other">Other</option>
                                            </select>
                                            @error('salary_column_type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="default_value">Default Value</label>
                                            <input type="text" name="default_value" id="default_value" class="form-control @error('default_value') is-invalid @enderror" placeholder="Column Order" value="0.00" required>
                                            @error('default_value')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="gl_account_credit">GL Account (Credit)</label>
                                            <input type="text" name="gl_account_credit" id="gl_account_credit" class="form-control" placeholder="GL Creadit Account">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="gl_account_debit">GL Account (Debit)</label>
                                            <input type="text" name="gl_account_debit" id="gl_account_debit" class="form-control" placeholder="GL Debit Account">
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="recursive_function">Recursive Function</label>
                                            <input type="text" id="recursive_function" class="form-control" name="recursive_function" placeholder="Recursive Function Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="payroll_accrual">Payroll Accrual</label>
                                            <select type="select-multi" name="payroll_accrual" id="payroll_accrual" class="select2 form-select" data-placeholder="Select Payroll Accrual">
                                                <option value=""></option>
                                                <option value="pf_accrual">PF Accrual</option>
                                                <option value="eobi_accrual">EOBI Accrual</option>
                                                <option value="pessi_accrual">PESSI Accrual</option>
                                                <option value="gratuity_accrual">Gratuity Accrual</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                        <h4>Calculation Columns</h4>
                                        <hr>
                                        <div class="add_items" id="add_items">
                                            <div data-repeater-list="calculation_columns">
                                                <div data-repeater-item>
                                                    <div class="row d-flex align-items-end">
                                                        <div class="col-md-5 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="name">Name</label>
                                                                <input type="text" name="name" class="form-control" placeholder="Name" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="column">Column</label>
                                                                <select name="column" class="form-select select2" data-placeholder="Select Columns" required>
                                                                    <option value=""></option>
                                                                    @foreach ($payrollColumns as $column)
                                                                        <option value="{{$column->id}}">{{$column->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-12 mb-51">
                                                            <div class="mb-1">
                                                                <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                    <i data-feather="x" class="me-25"></i>
                                                                    <span>Delete</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                        <i data-feather="plus" class="me-25"></i>
                                                        <span>Add New</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1 mt-1">
                                            <label class="form-label" for="calculation_function">Function</label>
                                            <input type="text" name="calculation_function" id="calculation_function" class="form-control" placeholder="Function">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" id="round_nearest" name="round_off" value="1">Round To Nearest
                                                <label class="form-check-label" for="round_nearest"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" id="round_one_digit" name="round_off" value="3">Round Up to One Digit
                                                <label class="form-check-label" for="round_one_digit"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" id="round_two_digit" name="round_off" value="2">Round Up to Two Digit
                                                <label class="form-check-label" for="round_two_digit"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12 text-end">
                                        <div class="mb-1">
                                            <div class="form-check">
                                                <button class="btn btn-danger btn-sm" id="reset_radio">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="employers_column" value="1">
                                                <label class="custom-control-label">Employer's Column</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="comma_seprated" value="1">
                                                <label class="custom-control-label">Comma Seprated</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="total_column" value="1">
                                                <label class="custom-control-label">Total Column</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn btn-secondary" id="unlock">Unlock</button>
                                    <a href="{{route('payrollColumns.index')}}" class="btn btn-outline-danger ms-1">Cancel</a>
                                    <button type="submit" class="form_save btn btn-primary ms-1" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#add_payroll_col").submit(function(){
                ButtonStatus('.form_save',true);
                        blockUI();
            });    
                
            $('#salary_components').on('change',function(e){
                $('#calculation_function').attr('readonly', true);
                $('#deductions').attr('disabled', true);
            });

            $('#deductions').on('change',function(e){
                $('#calculation_function').attr('readonly', true);
                $('#salary_components').attr('disabled', true);
            });

            $('#unlock').on('click', function(e) {
                $('#default_value').removeAttr('readonly');
                $('#calculation_function').attr('readonly', false);
                $('#salary_components').attr('disabled', false);
                $('#deductions').attr('disabled', false);
            });

            $('#reset_radio').on('click' , function(e){
                e.preventDefault();
                $('input[type="radio"]').prop('checked', false);
            });

            var calculation_column = $('#add_items');
            $(calculation_column).repeater({
                initEmpty: true,
                show: function() {
                    $(this).slideDown();
                    calculation_column.find('select').next('.select2-container').remove();
                    calculation_column.find('select').select2();
                    // Feather Icons
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                },
            });
        });
    </script>
@endsection