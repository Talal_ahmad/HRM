@extends('Admin.layouts.master')
@section('title', 'Company Belongings')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Company Belongings</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Company Belongings
                                </li>
                                <li class="breadcrumb-item active">Issue
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="col-12">
            <div class="card mb-1">
                <div class="card-body">
                    <form id="search_form" action="{{url('issue_belonging')}}" method="GET">
                        <div class="row">
                            <div class="col-md-3 col-12">
                                <label class="form-label" for="departmentFilter">Filter By department:</label>
                                <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" required>
                                    <option value=""></option>
                                    <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : '' }}>All Departments</option>
                                    @foreach (departments() as $department)
                                        <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="form-label" for="employeeFilter">Filter By Employee:</label>
                                <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="Select Employee">
                                    <option value=""></option>
                                    @if (!empty(request('departmentFilter')))
                                        @foreach (employees(request('departmentFilter')) as $item)
                                            <option value="{{$item->id}}" {{$item->id == request('employeeFilter') ? 'selected' : ''}}>{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                        @endforeach
                                    @endif 
                                </select>
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="form-label" for="assetFilter">Asset name:</label>
                                <select name="assetFilter" id="assetFilter" class="select2 form-select" data-placeholder="Select Asset">
                                    <option value=""></option>
                                    @foreach ($belonging as $item)
                                        <option value="{{$item->id}}" {{$item->id == request('assetFilter') ? 'selected' : ''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="form-label" for="serialFilter">Serial No.:</label>
                                <select name="serialFilter" id="serialFilter" class="select2 form-select" data-placeholder="Select Serial No.">
                                    <option value=""></option>
                                    @foreach ($belonging as $item)
                                        <option value="{{$item->id}}" {{$item->id == request('serialFilter') ? 'selected' : ''}}>{{$item->serial_number}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 text-end">
                                <a href="{{url('issue_belonging')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                <button type="submit" class="btn btn-primary mt-1">Apply</button>
                            </div>
                        </div>
                    </form>   
                </div> 
            </div>
        </div>
    </div>
    <div class="content-body">
        <section>

            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Department</th>
                                    <th>Employees</th>
                                    <th>Asset Name</th>
                                    <th>Serial Number</th> 
                                    <th>Issue Date</th>                                   
                                    <th>Return Date</th>                                   
                                    <th>Attachment</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>
    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <!--Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Belongings Issue Details</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <label class="form-label" for="departments">Select Department:</label>
                                    <select name="departments" id="departments" class="select2 form-select" data-placeholder="Select Department" required>
                                        <option value=""></option>
                                        @foreach (departments() as $department)
                                            <option value="{{$department->id}}" {{$department->id == request('departments') ? 'selected' : ''}}>{{$department->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 col-12">
                                    <label class="form-label" for="mul_employees">Select Employee:</label>
                                    <select name="mul_employees" id="mul_employees" class="select2 form-select" data-placeholder="Select Employee" required>
                                        <option value=""></option>
                                        @if (!empty(request('departments')))
                                            @foreach (employees(request('departments')) as $item)
                                            <option value="{{$item->id}}">{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                            @endforeach
                                        @endif 
                                    </select>
                                </div>
                                <div class="col-md-12 col-12">
                                    <label class="form-label" for="company_belonging_id">Select Company Belonging:</label>
                                    <select name="company_belonging_id" id="company_belonging_id" class="select2 form-select" data-placeholder="Select Comapny Belonging" required>
                                        <option value=""></option>
                                        @foreach ($belonging as $asset)
                                            <option value="{{$asset->id}}">{{$asset->name}}-{{$asset->serial_number}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Condition</label>
                                        <input type="text" name="condition" id="condition" class="form-control" placeholder="Condition of Belonging"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="sp_instruction">Special Instructions</label>
                                        <input type="text" name="sp_instruction" id="sp_instruction" class="form-control" placeholder="Special Instructions"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="remarks">Remarks</label>
                                        <input type="text" name="remarks" id="remarks" class="form-control" placeholder="Remarks"/>
                                    </div>
                                </div>
                                <div class="col-md-12  col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="attachs">Add Attachment</label>
                                        <input type="file" id="attachs" class="form-control"  name="attachs"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12" id="issue_date">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="issue_date">Issue Date</label>
                                            <input type="text" name="issue_date" id="issue_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12" id="return_date">
                                    <div class="mb-1">
                                        <label class="form-label" for="return_date">Returning Date:</label>
                                        <input type="text" name="return_date" id="return_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Add Modal -->

        <!--start return Modal -->
        <div class="modal fade text-start" id="return_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Returning Information</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="return_form">
                        @csrf
                        @method('POST')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <label class="form-label" for="person_order">Return to Person:</label>
                                    <select name="person_order" id="person_order" class="select2 form-select" data-placeholder="Select Person" required>
                                        <option value=""></option>
                                            @foreach (employees() as $item)
                                                <option value="{{$item->id}}">{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 col-12" id="issue_date">
                                    <div class="mb-1">
                                        <label class="form-label" for="status_return">Return Status:</label>
                                        <select name="status_return" id="status_return" class="select2 form-select" data-placeholder="Select One" required>
                                            <option value=""></option>
                                            <option value="Early">Early</option>
                                            <option value="Timely">Timely</option>
                                            <option value="Late">Late</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12" id="issue_date">
                                    <div class="mb-1">
                                        <label class="form-label" for="issue_date">Condition at return time:</label>
                                        <select name="condition_return" id="condition_return" class="select2 form-select" data-placeholder="Select One" required>
                                            <option value=""></option>
                                            <option value="FIne">FIne</option>
                                            <option value="Damage">Damage</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="remarks_return">Return Remarks</label>
                                        <input type="text" name="remarks_return" id="remarks_return" class="form-control" placeholder="Remarks" required>
                                    </div>
                                </div>
                                {{-- <div class="col-md-12 col-12" id="return_date">
                                    <div class="mb-1">
                                        <label class="form-label" for="return_date">Returning Date:</label>
                                        <input type="text" name="return_date" id="return_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="form_save btn btn-outline-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End return Modal -->

        <!--start edit Modal -->
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Comapny Belonging</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                {{-- <div class="col-md-12 col-12">
                                    <label class="form-label" for="departments">Select Department:</label>
                                    <select name="departments" id="departments_edit" class="select2 form-select" data-placeholder="Select Department" required>
                                        <option value=""></option>
                                        @foreach (departments() as $department)
                                            <option value="{{$department->id}}">{{$department->title}}</option>
                                        @endforeach
                                    </select>
                                </div> --}}
                                <div class="col-md-12 col-12">
                                    <label class="form-label" for="mul_employees">Select Employee:</label>
                                    <select name="mul_employees" id="mul_employees_edit" class="select2 form-select" data-placeholder="Select Employee">
                                        <option value=""></option>
                                        {{-- @if (!empty(request('departments'))) --}}
                                            @foreach ($issue_b as $item)
                                            <option value="{{$item->id}}">{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                            @endforeach
                                        {{-- @endif  --}}
                                    </select>
                                </div>
                                <div class="col-md-12 col-12">
                                    <label class="form-label" for="company_belonging_id_edit">Select Company Belonging:</label>
                                    <select name="company_belonging_id" id="company_belonging_id_edit" class="select2 form-select" data-placeholder="Select Comapny Belonging">
                                        <option value=""></option>
                                        @foreach ($belonging as $asset)
                                            <option value="{{$asset->id}}">{{$asset->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Condition</label>
                                        <input type="text" name="condition" id="condition_edit" class="form-control" placeholder="Condition of Belonging"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="sp_instruction">Special Instructions</label>
                                        <input type="text" name="sp_instruction" id="sp_instruction_edit" class="form-control" placeholder="Special Instructions"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="remarks">Remarks</label>
                                        <input type="text" name="remarks" id="remarks_edit" class="form-control" placeholder="Remarks"/>
                                    </div>
                                </div>
                                <div class="col-md-12  col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="attachs">Add Attachment</label>
                                        <input type="file" id="attachs_edit" class="form-control"  name="attachs"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12" id="issue_date">
                                    <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="issue_date">Issue Date</label>
                                            <input type="text" name="issue_date" id="issue_date_edit" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12" id="return_date">
                                    <div class="mb-1">
                                        <label class="form-label" for="return_date">Returning Date:</label>
                                        <input type="text" name="return_date" id="return_date_edit" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End edit Modal -->
    </div>
@endsection
@section('scripts')
    <script>
        var datatable;
        var rowid;
        $(document).ready(function(){
            var employee = @json(request('employeeFilter'));
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            var selected = employee == value.id ? 'selected' : '';
                            $('#employeeFilter').append(
                                $('<option '+selected+'></option>').val(value.id).html(
                                    value.employee_id + ' - ' +value.employee_code + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation + ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            $("#departments").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#mul_employees').empty();
                        $('#mul_employees').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#mul_employees').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - ' +value.employee_code + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation + ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            // $("#departments_edit").change(function() {
            //     var optVal = $(this).val();
            //     $.ajax({
            //         type: "GET",
            //         url: "{{ url('get_dept_employees') }}",
            //         data: {
            //             department_id: optVal,
            //         },
            //         success: function(response) {
            //             $('#mul_employees_edit').empty();
            //             $('#mul_employees_edit').html('<option value="">Select Employee</option>'); 
            //             $.each(response, function(index, value) {
            //                 $('#mul_employees_edit').append(
            //                     $('<option></option>').val(value.id).html(
            //                         value.employee_id + ' - ' +value.employee_code + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation + ' - ' + value.department)
            //                 );
            //             });
            //         },
            //         error: function() {
            //             alert('Error occured');
            //         }
            //     });
            // });
        var all_emp = <?php echo json_encode($issue_b); ?>;


            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('issue_belonging.index') }}",
                    data: function (filter) {
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.assetFilter = $('#assetFilter').val();
                        filter.serialFilter = $('#serialFilter').val();
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'DT_RowIndex',
                        name : 'DT_RowIndex',
                        searchable: false,
                    },
                    {
                        data : 'department_title',
                        name : 'department_title', 
                    },
                    {
                        render: function(data, type, full, meta) {
                            var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                            var lastName = full['last_name'] != null ? full['last_name'] : '';
                            return full['first_name'] + ' ' + middleName+ ' ' + lastName;
                        },
                        searchable: false
                    },
                    // {
                    //     data: 'mul_employees' ,
                    //     name : 'mul_employees', 
                    // },
                    {
                        data: 'name',
                        name : 'name',
                    },
                    {
                        data: 'serial_number',
                        name : 'serial_number',
                    },
                    {
                        data: 'issue_date',
                        name : 'issue_date',
                    },
                    {
                        data: 'return_date',
                        name : 'return_date',
                    },
                    {
                        "data": "attachs",
                        "render": function (data) {
                            if (data) {
                                var fileExtension = data.split('.').pop().toLowerCase(); // Get file extension
                                var imageURL = "{{ asset('images/belongings') }}" + '/' + data; // Construct image URL
                                if (fileExtension === 'pdf') {
                                    return '<div style="display: flex; align-items: center;"><i class="far fa-file-pdf text-danger" style="font-size:29px;padding: 2px 14px;"></i>' +
                                        '<a class="px-2" href="' + imageURL + '" download><button type="button" class="btn btn-success px-2"><i class="fas fa-download"></i></button></a></span>';
                                } else {
                                    return '<div style="text-wrap:nowrap;"><img src="' + imageURL + '" class="avatar" alt="Failed Loading Image" width="50" height="50"/>' +
                                        '<a class="px-2" href="' + imageURL + '" download><button type="button" class="btn btn-success px-2"><i class="fas fa-download"></i></button></a></div>';
                                }
                            } else {
                                // Handle case where data is null or undefined
                                return '-'; // Return empty string or any other fallback content
                            }
                        }
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            return (
                                ''+
                            @can('Issue Company Belongings Return Date')    
                                '<a href="javascript:;" class="item-edit" onclick=return_v('+full.id+')>' +
                                feather.icons['corner-left-down'].toSvg({ class: 'font-medium-4 text-success' }) +
                                '</a>'+
                            @endcan
                            @can('Issue Company Belongings Edit')    
                                '<a href="javascript:;" class="item-edit" onclick=edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                @endcan
                            @can('Issue Company Belongings Delete')    
                                '<a href="javascript:;" onclick="delete_item('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                                @endcan
                            );
                            
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                        {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                        }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Issue Company Belongings Add New')    
                        {
                            text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#add_modal'
                            },
                            init: function (api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        },
                    @endcan
                    {
                        text: feather.icons['arrow-right'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Company Belongings',
                        className: 'create-new ms-1  btn btn-primary',
                        action: function(e, dt, node, config) {
                            window.location.href = '{{ route('company_belongings.index') }}';
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of Company Belongings</h6>');

            $("#add_form").submit(function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{route('issue_belonging.store')}}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        console.log(response);
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                        if(response.message!='err'){
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Belonging has been Issued Successfully!'
                            })
                        }else{
                            Toast.fire({
                                icon: 'error',
                                title: 'Company Belonging Limit is Eceeded.'
                            })
                        }
                        }
                        
                    }
                });
            });

            // Update record
            $("#edit_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{url('issue_belonging')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#edit_modal").modal("hide");
                            $("#return_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Belonging Updated Successfully!'
                            })
                        }
                    }
                });
            });
            
            // Update return date
            $("#return_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{url('returning_belonging')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Return Date Added Successfully!'
                            })
                        }
                    }
                });
            });
        });

        

         // Return
         function return_v(id)
        {
            rowid = id;
            console.log(rowid);
            $("#return_modal").modal("show");

            // $.ajax({
            //     // url: "{{url('return_belonging')}}" + "/" + id,
            //     type: "get",
            //     success: function (response) {
            //         // departments_edit
            //         $("#departments_edit").val(response.department_title).select2();
            //         $("#condition_edit").val(response.bel_conditions);
            //         $("#sp_instruction_edit").val(response.sp_instruction);
            //         $("#remarks_edit").val(response.remarks);
            //         $("#issue_date_edit").val(response.issue_date);
            //         if(response.mul_employees != null){
            //             console.log(response);
            //             $('#mul_employees_edit').val(JSON.parse(response.mul_employees.replace(/&quot;/g,'"'))).select2();
            //         }
            //         $("#edit_serial_number").val(response.serial_number);
            //         $("#edit_asset_tag").val(response.asset_tag);
            //         $("#edit_description").val(response.description);
            //         $("#edit_modal").modal("show");
            //     },
            // });
        }

        // Edit Record
        function edit(id)
        {
            rowid = id;
            $.ajax({
                url: "{{url('issue_belonging')}}" + "/" + id + "/edit",
                type: "get",
                success: function (response) {
                    // departments_edit
                    $("#departments_edit").val(response.department_title).select2();
                    $("#condition_edit").val(response.bel_conditions);
                    $("#sp_instruction_edit").val(response.sp_instruction);
                    $("#remarks_edit").val(response.remarks);
                    $("#b_limit_edit").val(response.b_limit);
                    $("#issue_date_edit").val(response.issue_date);
                    $("#return_date_edit").val(response.return_date);
                    console.log(response.mul_employees);
                    $('#mul_employees_edit').val(response.mul_employees).select2();
                    $('#company_belonging_id_edit').val(response.company_belonging_id).select2();
                    $("#edit_serial_number").val(response.serial_number);
                    $("#edit_asset_tag").val(response.asset_tag);
                    $("#edit_description").val(response.description);
                    $("#edit_modal").modal("show");
                },
            });
        }

        // Delete Record
        function delete_item(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                            $.ajax({
                                url: "issue_belonging/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}",
                                    type: 'salary_component_type'
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else if(response.code == 300)
                                    {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Company Belonging has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection