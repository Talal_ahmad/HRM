@extends('Admin.layouts.master')

@section('title', 'Overtime Hourly Pay Column')
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Overtime Hourly Pay Column</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Finance</a>
                            </li>
                            <li class="breadcrumb-item active">Overtime Hourly Pay Column
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Overtime Hourly Pay Column</th>
                                    <th>Net Salary Column</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <!--Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Columns</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="salary_component">Salary Component</label>
                                        <select class="form-control select2" id="salary_component" name="overtime_salary_component_id" data-placeholder="Select Salary Component" required>
                                            <option value=""></option>
                                            @foreach($salary_component as $component)
                                            <option value="{{$component->id}}">{{$component->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="salary_component">Payroll Column</label>
                                        <select class="form-control select2" id="payroll_column" name="net_salary_column" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach($payroll_columns as $column)
                                            <option value="{{$column->id}}">{{$column->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Add Modal -->

        <!--start edit Modal -->
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Columns</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="edit_salary_component">Salary Components</label>
                                        <select class="form-control select2" id="edit_salary_component" name="overtime_salary_component_id" data-placeholder="Select Salary Component" required>
                                            <option value=""></option>
                                            @foreach($salary_component as $component)
                                            <option value="{{$component->id}}">{{$component->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="edit_salary_component">Payroll Column</label>
                                        <select class="select2 form-control" id="edit_payroll_column" name="net_salary_column" data-placeholder="Select Payroll Column" required>
                                            <option value=""></option>
                                            @foreach($payroll_columns as $column)
                                            <option value="{{$column->id}}">{{$column->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End edit Modal -->
    </div>
</section>

@endsection
@section('scripts')
<script>
    var rowid;
    $(document).ready(function() {

        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: "{{ route('overtime_hourly_paycolumn.index') }}",
            },
            columns: [{
                    data: 'responsive_id'
                },
                {
                    data: 'rownum',
                    name: 'rownum',
                    searchable: false
                },
                {
                    data: 'overtime_salary_component_id',
                    name: 'salarycomponent.name',
                },
                {
                    data: 'net_salary_column',
                    name: 'payrollcolumns.name',
                },
                {
                    data: '',
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    orderable: false,
                    searchable: false,
                    title: 'Actions',
                    render: function(data, type, full, meta) {
                        return (
                            '<a href="javascript:;" class="item-edit" onclick=edit(' + full.id + ')>' +
                            feather.icons['edit'].toSvg({
                                class: 'font-medium-4'
                            }) +
                            '</a>' +
                            '<a href="javascript:;" onclick="delete_item(' + full.id + ')">' +
                            feather.icons['trash-2'].toSvg({
                                class: 'font-medium-4 text-danger'
                            }) +
                            '</a>'
                        );
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            aLengthMenu: [
                [10,25,50,100,-1],
                [10,25,50,100,"All"]
            ],
            buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Overtime Hourly Pay And Net Salary Column',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Overtime Hourly Pay And Net Salary Column',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Overtime Hourly Pay And Net Salary Column',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Overtime Hourly Pay And Net Salary Column',
                            orientation: 'landscape',
                            pageSize: 'LEGAL',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Overtime Hourly Pay And Net Salary Column',
                            action: newexportaction,
                                exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                        }, 50);
                    }
                },
                {
                    text: feather.icons['plus'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Add New',
                    className: 'create-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Columns</h6>');

        $("#add_form").on("submit", function(e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: "{{route('overtime_hourly_paycolumn.store')}}",
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } 
                    else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $("#add_modal").modal("hide");
                        document.getElementById("add_form").reset();
                        $(".select2").val('').trigger('change')
                        $('#dataTable').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Columns has been Added Successfully!'
                        })
                    }

                }
            });
        });

        $("#edit_form").on("submit", function(e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: "{{url('overtime_hourly_paycolumn')}}" + "/" + rowid,
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $("#edit_modal").modal("hide");
                        document.getElementById("edit_form").reset();
                        $(".select2").val('').trigger('change')
                        $('#dataTable').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Columns has been Updated Successfully!'
                        })
                    }

                }
            });
        });
    });

    function edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('overtime_hourly_paycolumn')}}" + "/" + id + "/edit",
            type: "get",
            success: function(response) {
                $("#edit_salary_component").val(response.overtime_salary_component_id).select2();
                $("#edit_payroll_column").val(response.net_salary_column).select2();
                $("#edit_modal").modal("show");
            },
        });
    }

    function delete_item(id) {
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: 'Are you sure you want to delete!',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function() {
                        $.ajax({
                            url: "overtime_hourly_paycolumn/" + id,
                            type: "DELETE",
                            data: {
                                _token: "{{ csrf_token() }}",
                            },
                            success: function(response) {
                                if (response.error_message) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                } else if (response.code == 300) {
                                    $.alert({
                                        icon: 'far fa-times-circle',
                                        title: 'Oops!',
                                        content: response.message,
                                        type: 'red',
                                        buttons: {
                                            Okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-red',
                                            }
                                        }
                                    });
                                } else {
                                    $('#dataTable').DataTable().ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Columns has been Deleted Successfully!'
                                    })
                                }
                            }
                        });
                    }
                },
                cancel: function() {
                    $.alert('Canceled!');
                },
            }
        });
    }
</script>
@endsection