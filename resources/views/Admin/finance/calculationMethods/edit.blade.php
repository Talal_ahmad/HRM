@extends('Admin.layouts.master')
@section('title', 'Edit Calculation Method')

@section('content')
<section>
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Edit Calculation Method</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashbaord</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Calculation Method</a>
                            </li>
                            <li class="breadcrumb-item active">Edit
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('calculationMethods.update',$CalculationMethod->id)}}" class="form" id="add_form" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Name</label>
                                        <input type="text" name="name" id="name" class="form-control" value="{{$CalculationMethod->name}}" placeholder="Name" required/>
                                        @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="componentTypes">Salary Component Types</label>
                                        <select class="form-select select2" name="componentTypes[]" id="componentTypes" data-placeholder="Select Salary Component Types" required multiple>
                                            <option value=""></option>
                                            @foreach ($salaryComponentTypes as $type)
                                            <option value="{{ $type->id }}" {{in_array($type->id,json_decode($CalculationMethod->componentType)) ? 'selected' : ''}}>{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="button-container mt-1">
                                            <button class="btn btn-sm btn-primary" type="button"
                                                onclick="selectAll('#componentTypes')">Select All</button>
                                            <button class="btn btn-sm btn-danger" type="button"
                                                onclick="deselectAll('#componentTypes')">Deselect All</button>
                                        </div>
                                        @error('componentTypes')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="components">Salary Components</label>
                                        <select class="form-select select2" name="components[]" id="components"
                                            data-placeholder="Select Salary Components" required multiple>
                                            <option value=""></option>
                                            @foreach ($salaryComponent as $item)
                                            <option value="{{ $item->id }}" {{in_array($item->id,json_decode($CalculationMethod->component)) ? 'selected' : ''}}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="button-container mt-1">
                                            <button class="btn btn-sm btn-primary" type="button"
                                                onclick="selectAll('#components')">Select All</button>
                                            <button class="btn btn-sm btn-danger" type="button"
                                                onclick="deselectAll('#components')">Deselect All</button>
                                        </div>
                                        @error('components')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="payrollColumn">Payroll Report Columns</label>
                                        <select class="form-select select2" name="payrollColumn" id="payrollColumns"
                                            data-placeholder="Select Payroll Report Column">
                                            <option value=""></option>
                                            @foreach ($payrollReportColumn as $column)
                                            <option value="{{ $column->id }}" {{$CalculationMethod->payrollColumn == $column->id ? 'selected' : ''}}>{{ $column->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="calculation_group">Calculation Groups</label>
                                        <select class="form-select select2" name="calculation_group" id="calculation_group"
                                            data-placeholder="Select Calculation Group">
                                            <option value=""></option>
                                            @foreach ($calculationGroups as $group)
                                            <option value="{{ $group->id }}" {{$CalculationMethod->deduction_group == $group->id ? 'selected' : ''}}>{{ $group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('calculationMethods.index')}}" class="btn btn-outline-danger">Cancel</a>
                                    <button type="submit" class="btn btn-primary ms-1" id="save">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection