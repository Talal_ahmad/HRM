@extends('Admin.layouts.master')
@section('title', 'Add Calculation Method')

@section('content')
<section>
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Add Calculation Method</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashbaord</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Calculation Method</a>
                            </li>
                            <li class="breadcrumb-item active">Create
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('calculationMethods.store') }}" class="form" id="add_form" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Name</label>
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Name" required/>
                                        @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="componentTypes">Salary Component Types</label>
                                        <select class="form-select select2" name="componentTypes[]" id="componentTypes" data-placeholder="Select Salary Component Types" required multiple>
                                            <option value=""></option>
                                            @foreach ($salaryComponentTypes as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="button-container mt-1">
                                            <button class="btn btn-sm btn-primary" type="button"
                                                onclick="selectAll('#componentTypes')">Select All</button>
                                            <button class="btn btn-sm btn-danger" type="button"
                                                onclick="deselectAll('#componentTypes')">Deselect All</button>
                                        </div>
                                        @error('componentTypes')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="components">Salary Components</label>
                                        <select class="form-select select2" name="components[]" id="components"
                                            data-placeholder="Select Salary Components" required multiple>
                                            <option value=""></option>
                                            @foreach ($salaryComponent as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="button-container mt-1">
                                            <button class="btn btn-sm btn-primary" type="button"
                                                onclick="selectAll('#components')">Select All</button>
                                            <button class="btn btn-sm btn-danger" type="button"
                                                onclick="deselectAll('#components')">Deselect All</button>
                                        </div>
                                        @error('components')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="payrollColumn">Payroll Report Columns</label>
                                        <select class="form-select select2" name="payrollColumn" id="payrollColumns"
                                            data-placeholder="Select Payroll Report Column">
                                            <option value=""></option>
                                            @foreach ($payrollReportColumn as $column)
                                            <option value="{{ $column->id }}">{{ $column->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="calculation_group">Calculation Groups</label>
                                        <select class="form-select select2" name="calculation_group" id="calculation_group"
                                            data-placeholder="Select Calculation Group">
                                            <option value=""></option>
                                            @foreach ($calculationGroups as $group)
                                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                    <h4>Calculation Process</h4>
                                    <hr>
                                    <div class="add_items" id="add_items">
                                        <div data-repeater-list="rangeAmounts">
                                            <div data-repeater-item>
                                                <div class="row d-flex align-items-end">
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="lowerCondition">Lower Limit Condition</label>
                                                            <select name="lowerCondition" class="form-select select2" data-placeholder="Select Lower Limit Condition" required>
                                                                <option value=""></option>
                                                                <option value='No Lower Limit'>No Lower Limit</option>
                                                                <option value='gt'>Greater than</option>
                                                                <option value='gte'>Greater than or Equal</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="lowerLimit">Lower Limit</label>
                                                            <input type="text" name="lowerLimit" class="form-control" placeholder="Lower Limit" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="upperCondition">Upper Limit Condition</label>
                                                            <select name="upperCondition" class="form-select select2" data-placeholder="Select Upper Limit Condition" required>
                                                                <option value=""></option>
                                                                <option value='No Upper Limit'>No Upper Limit</option>
                                                                <option value='lt'>Less than</option>
                                                                <option value='lte'>Less than or Equal</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="upperLimit">Upper Limit</label>
                                                            <input type="text" name="upperLimit" class="form-control" placeholder="Upper Limit" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="amount">Value</label>
                                                            <input type="text" name="amount" class="form-control" placeholder="Amount" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-12 mb-51">
                                                        <div class="mb-1">
                                                            <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                <i data-feather="x" class="me-25"></i>
                                                                <span>Delete</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <button class="btn btn-icon btn-primary" type="button"
                                                    data-repeater-create>
                                                    <i data-feather="plus" class="me-25"></i>
                                                    <span>Add New</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('calculationMethods.index')}}" class="btn btn-outline-danger">Cancel</a>
                                    <button type="submit" class="form_save btn btn-primary ms-1" id="save">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
$(document).ready(function() {
    var calculation_process = $('#add_items');
    $('.add_items').repeater({
        // isFirstItemUndeletable: false,
        initEmpty: true,
        show: function() {
            $(this).slideDown();
            calculation_process.find('select').next('.select2-container').remove();
            calculation_process.find('select').select2();
            // Feather Icons
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        },
        hide: function(deleteElement) {
            $(this).slideUp(deleteElement);
        }
    });
})
</script>
@endsection