@extends('Admin.layouts.master')
@section('title', 'Entitlement Assignment')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Entitlement Assignments</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Entitlement Assignment
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="basic-dataTable">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Department</th>
                                        <th>Entitlement</th>
                                        <th>Date From</th>
                                        <th>Date To</th>
                                        <th>Remarks</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add Entitlement Assignment</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="department_id">Departments</label>
                                            <select name="department_id" id="department_id" class="select2 form-select" data-placeholder="Select Department" required>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee_id">Employees</label>
                                            <select name="employee_id" id="employee_id" class="select2 form-select" data-placeholder="Select Employee" required disabled>
                                                <option value=""></option>
                                                {{-- @foreach(employees() as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id. ' - '.$employee->employee_code.' - '.$employee->first_name.'  '.$employee->last_name}}</option>
                                                @endforeach  --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="entitlement_id">Entitlements</label>
                                            <select name="entitlement_id" id="entitlement_id" class="select2 form-select" data-placeholder="Select Entitlement" required>
                                                <option value=""></option>
                                                @foreach ($entitlements as $entitlement)
                                                    <option value="{{ $entitlement->id }}">{{ $entitlement->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="date_from">Date From</label>
                                            <input type="text" name="date_from" id="date_from" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="date_to">Date To</label>
                                            <input type="text" name="date_to" id="date_to" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="remarks">Remarks</label>
                                            <textarea name="remarks" id="remarks" class="form-control" cols="10" rows="2" placeholder="Type here..." required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit Entitlement Assignment</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_department_id">Departments</label>
                                            <select name="department_id" id="edit_department_id" class="select2 form-select" data-placeholder="Select Department" required>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_employee_id">Employees</label>
                                            <select name="employee_id" id="edit_employee_id" class="select2 form-select" data-placeholder="Select Employee" required readonly>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_entitlement_id">Entitlements</label>
                                            <select name="entitlement_id" id="edit_entitlement_id" class="select2 form-select" data-placeholder="Select Entitlement" required> 
                                                <option value=""></option>
                                                @foreach ($entitlements as $entitlement)
                                                    <option value="{{ $entitlement->id }}">{{ $entitlement->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_date_from">Date From</label>
                                            <input type="text" name="date_from" id="edit_date_from" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_date_to">Date To</label>
                                            <input type="text" name="date_to" id="edit_date_to" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_remarks">Remarks</label>
                                            <textarea name="remarks" id="edit_remarks" class="form-control" cols="20" rows="2"
                                                placeholder="Type here..." required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: true,
                ajax: "{{ route('entitlementAssignment.index') }}",
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'rownum',
                        name: 'rownum',
                        searchable: false,
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        render: function (data, type, full, meta) {
                            var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                            var lastName = full['last_name'] != null ? full['last_name'] : '';
                            return full['first_name'] + ' ' + middleName+ ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'department',
                        name: 'companystructures.title',
                    },
                    {
                        data: 'entitlement',
                        name: 'entitlement.name',
                    },
                    {
                        data: 'date_from',
                        name: 'employeesentitlement.date_from',
                    },
                    {
                        data: 'date_to',
                        name: 'employeesentitlement.date_to',
                    },
                    {
                        data: 'remarks',
                        name: 'employeesentitlement.remarks',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        targets: 0,
                        className: 'control',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            // get department employees
            $('#department_id').on('change', function(){
                var department_id = $(this).val();
                var employee_id = '#employee_id';
                get_department_employees(department_id,employee_id);
            });

            $('#edit_department_id').on('change', function(){
                var department_id = $(this).val();
                var employee_id = '#edit_employee_id';
                get_department_employees(department_id,employee_id);
            });

            // Add Data
            $('#add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('entitlementAssignment.store') }}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Entitlement Assignment has been Assigned Successfully!'
                            })
                        }
                    }
                });
            })

            // Update Data
            $('#edit_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('entitlementAssignment') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#edit_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#edit_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Entitlement Assignment has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
            $('div.head-label').html('<h6 class="mb-0">List of Entitlement Assignments</h6>');
        });

        function get_department_employees(department_id, employee_id)
        {
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: "{{url('get_dept_employees')}}",
                data: {
                    department_id : department_id
                },
                success: function(data)
                {
                    $(employee_id).attr('disabled', false);
                    $(employee_id).html('');
                    $(employee_id).html('<option value="">Select Employee</option>'); 
                    $.each(data, function(index,employee){
                        var opt = $('<option>');
                        opt.val(employee.id);
                        opt.text(employee.employee_id+' - '+employee.employee_code+' - '+employee.first_name+' '+employee.middle_name +' '+employee.last_name + ' - ' + employee.designation);
                        $(employee_id).append(opt);
                    });
                }
            });
        }

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('entitlementAssignment') }}" + "/" + rowid + "/edit",
                type: "get",
                success: function(response) {
                    $('#edit_employee_id').html('');
                    $('#edit_employee_id').html('<option value="">Select Employee</option>'); 
                    $.each(response.employees, function(index,employee){
                        var opt = $('<option>');
                        opt.val(employee.id);
                        opt.text(employee.employee_id+' - '+employee.employee_code+' - '+employee.first_name+' '+employee.middle_name +' '+employee.last_name + ' - ' + employee.desigination);
                        $('#edit_employee_id').append(opt);
                    });
                    $("#edit_employee_id").val(response.data.employee_id).select2();
                    $("#edit_department_id").val(response.data.department_id).select2();
                    $("#edit_entitlement_id").val(response.data.entitlement_id).select2();
                    $("#edit_date_from").val(response.data.date_from);
                    $("#edit_date_to").val(response.data.date_to);
                    $("#edit_remarks").val(response.data.remarks);
                    $("#edit_modal").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "entitlementAssignment/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Entitlement Assignment has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
