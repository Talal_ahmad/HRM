@extends('Admin.layouts.master')
@section('title', 'Incentive Employees')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Incentive Employees</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Finance</a>
                            </li>
                            <li class="breadcrumb-item active">Incentive Employees
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Department</th> 
                                    <th>Employee</th>
                                    <th>Desigination</th>
                                    <th class="not_include">Status</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <!--Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Incentive Employee</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="department">Departments</label>
                                        <select name="department" id="department" class="select2 form-select" data-placeholder="Select Department" required>
                                            <option value=""></option>
                                            @foreach (departments() as $department)
                                            <option value="{{$department->id}}">{{$department->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="employees">Employees</label>
                                        <select name="employee_id[]" id="employees" class="select2 form-select" data-placeholder="Select Employee" required disabled multiple>
                                        <option value=""></option>
                                        </select>
                                        <div class="button-container mt-1">
                                            <button class="btn btn-sm btn-primary" type="button"
                                                onclick="selectAll('#employees')">Select All</button>
                                            <button class="btn btn-sm btn-danger" type="button"
                                                onclick="deselectAll('#employees')">Deselect All</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Add Modal -->
    </div>
</section>

@endsection
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function() {
        datatable = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('IncentiveEmployee.index') }}",
            columns: [
                {
                    data: 'responsive_id'
                },
                {
                    data : 'DT_RowIndex',
                    name : 'DT_RowIndex', 
                    searchable: false
                },
                {
                    data: 'title',
                    name : 'companystructures.title',
                },
                {
                data: 'fullname',
                // render: function (data, type, full, meta) {
                    //     return full['employee_id']+ ' - '+full['employee_code']+ ' - '+full['first_name'] + ' ' + full['last_name'];
                    // },
                },
                {
                    data: 'desigination',
                    name : 'desigination',
                },
                {
                    data: ''
                },
            ],
            search: {
                "regex": true
            },
            "columnDefs": [
                {
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Status
                    targets: -1,
                    title: 'Status',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if(full.status == 1){
                            status = '<input  type="checkbox" data-id="'+full.id+'" class="toggle-class" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="Disable" checked>';
                        }
                        else{
                            status = '<input  type="checkbox" data-id="'+full.id+'" class="toggle-class" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="Disable">';
                        }
                        return status;
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [[0, 'asc']],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            aLengthMenu: [
                [10,25,50,100,-1],
                [10,25,50,100,"All"]
            ],
            buttons: [
                {
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                    buttons: [
                    {
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    }
                    ],
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                        }, 50);
                    }
                },
                {
                    text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                    className: 'create-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Incentive Employees</h6>');

        // get department employees
        $('#department').on('change', function(){
            var department_id = $(this).val();
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: "{{url('get_dept_employees')}}",
                data: {
                    department_id : department_id
                },
                success: function(data)
                {
                    $('#employees').attr('disabled', false);
                    $('#employees').html('');
                    $('#employees').html('<option value="">Select Employee</option>'); 
                    $.each(data, function(index,employee){
                        var opt = $('<option>');
                        opt.val(employee.id);
                        opt.text(employee.employee_id+' - '+employee.employee_code+' - '+employee.first_name+' '+employee.middle_name+'  '+employee.last_name + ' - '+ employee.designation);
                        $('#employees').append(opt);
                    });
                }
            });
        });

        $("#add_form").submit(function (e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            $.ajax({
                url: "{{route('IncentiveEmployee.store')}}",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                responsive: true,
                success: function (response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        $('#add_form')[0].reset();
                        $(".select2").val('').trigger('change')
                        $("#add_modal").modal("hide");
                        datatable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Incentive Employee has been Added Successfully!'
                        })
                    }
                    
                }
            });
        });
    });
    </script>
    @endsection