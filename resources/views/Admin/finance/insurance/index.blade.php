@extends('Admin.layouts.master')
@section('title', 'Insurance Allocation')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Insurance Allocation</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Insurance Allocation
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="basic-dataTable">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Insurance Type</th>
                                        <th>Insurance Sub Type</th>
                                        <th>Policy Number</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Total Amount</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add Insurance Allocation</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee_id">Employees</label>
                                            <select name="employee_id" id="employee_id" class="select2 form-select" data-placeholder="Select Employee" required>
                                                <option value=""></option>
                                                @foreach(employees() as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id. ' - '.$employee->employee_code.' - '.$employee->first_name.'  '.$employee->middle_name.'  '.$employee->last_name. ' - '.$employee->desigination}}</option>
                                                @endforeach 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="type">Insurance Types</label>
                                            <select name="type" id="type" class="select2 form-select" data-placeholder="Select Insurance Type" required>
                                                <option value=""></option>
                                                @foreach ($insurance_types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="sub_type">Insurance SubTypes</label>
                                            <select name="sub_type" id="sub_type" class="select2 form-select" data-placeholder="Select Insurance SubType" required>
                                                <option value=""></option>
                                                @foreach ($insurance_sub_types as $sub_type)
                                                    <option value="{{ $sub_type->id }}">{{ $sub_type->sub_type }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="policy_number">Policy Number </label>
                                            <input type="text" name="policy_number" id="policy_number" class="form-control" placeholder="Policy Number" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="start_date">Start Date</label>
                                            <input type="text" name="start_date" id="start_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="end_date">End Date</label>
                                            <input type="text" name="end_date" id="end_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="amount">Total Amount</label>
                                            <input type="text" name="amount" id="amount" class="form-control" placeholder="Amount" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="attachment" class="form-label">Attachment</label>
                                            <input class="form-control" name="attachment" type="file" id="attachment">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit Insurance Allocation</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_employee_id">Employees</label>
                                            <select name="employee_id" id="edit_employee_id" class="select2 form-select" data-placeholder="Select Employee" required>
                                                <option value=""></option>
                                                @foreach(employees() as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id. ' - '.$employee->employee_code.' - '.$employee->first_name.'  '.$employee->middle_name.'  '.$employee->last_name. ' - '.$employee->desigination}}</option>
                                                @endforeach 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_type">Insurance Types</label>
                                            <select name="type" id="edit_type" class="select2 form-select" data-placeholder="Select Insurance Type" required>
                                                <option value=""></option>
                                                @foreach ($insurance_types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->type }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_sub_type">Insurance SubTypes</label>
                                            <select name="sub_type" id="edit_sub_type" class="select2 form-select" data-placeholder="Select Insurance Sub Type" required>
                                                <option value=""></option>
                                                @foreach ($insurance_sub_types as $item)
                                                    <option value="{{ $item->id }}">{{ $item->sub_type }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_policy_number">Policy Number</label>
                                            <input type="text" name="policy_number" id="edit_policy_number" class="form-control" placeholder="Policy Number" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_start_date">Start Date</label>
                                            <input type="text" name="start_date" id="edit_start_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_end_date">End Date</label>
                                            <input type="text" name="end_date" id="edit_end_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_amount">Total Amount</label>
                                            <input type="text" name="amount" id="edit_amount" class="form-control" placeholder="Amount" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="edit_attachment" class="form-label">Attachment</label>
                                            <input class="form-control" name="attachment" type="file" id="edit_attachment">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('insuranceAllocation.index') }}",
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'rownum',
                        name: 'rownum',
                        searchable: false
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        render: function (data, type, full, meta) {
                            return full['first_name'] + ' ' + full['last_name'];
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'type',
                        name: 'insurance_types.type',
                    },
                    {
                        data: 'sub_type',
                        name: 'insurance_sub_types.sub_type',
                    },
                    {
                        data: 'policy_number',
                        name: 'insurance_allocation.policy_number',
                    },
                    {
                        data: 'start_date',
                        name: 'insurance_allocation.start_date',
                    },
                    {
                        data: 'end_date',
                        name: 'insurance_allocation.end_date',
                    },
                    {
                        data: 'amount',
                        name: 'insurance_allocation.amount',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        targets: 0,
                        className: 'control',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#add_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('insuranceAllocation.store') }}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Insurance Allocation has been Added Successfully!'
                            })
                        }
                    }
                });
            })
            $('#edit_form').submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('insuranceAllocation') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#edit_form')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#edit_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Insurance Allocation has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            $('div.head-label').html('<h6 class="mb-0">List of Insurance Allocations</h6>');
        });

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('insuranceAllocation') }}" + "/" + rowid + "/edit",
                type: "get",
                success: function(response) {
                    console.log(response);
                    $("#edit_employee_id").val(response.employee_id).select2();
                    $("#edit_type").val(response.type).select2();
                    $("#edit_sub_type").val(response.sub_type).select2();
                    $("#edit_policy_number").val(response.policy_number);
                    $("#edit_start_date").val(response.start_date);
                    $("#edit_end_date").val(response.end_date);
                    $("#edit_amount").val(response.amount);
                    $("#edit_modal").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "insuranceAllocation/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Insurance Allocation has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
