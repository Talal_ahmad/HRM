@extends('Admin.layouts.master')
@section('title', 'Employee Payments')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Employee Payments</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Finance</a>
                            </li>
                            <li class="breadcrumb-item active">Employee Payments
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section> 
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Employee ID</th>
                                    <th>Employee Code</th>
                                    <th>Employee</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Amount</th>
                                    <th>Payment Type</th>
                                    <th>Payment Description</th>
                                    <th>Amount Month</th>
                                    <th>Amount Year</th>
                                    <th>Billing Period from</th>
                                    <th>Billing Period TO</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <!--Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Employee Payment</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="employee_id">Employee</label>
                                        <select class="select2 form-control" name="employee_id" id="employee_id" data-placeholder="Select Employee" required>
                                            <option value=""></option>
                                            @foreach(employees() as $employee)
                                            <option value="{{$employee->id}}">{{$employee->employee_id. ' - '.$employee->employee_code.' - '.$employee->first_name.'  '.$employee->middle_name.'  '.$employee->last_name . '-' . $employee->desigination}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="amount">Amount</label>
                                        <input type="text" name="amount" id="amount" class="form-control" placeholder="Amount" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="payment_type">Payment Type</label>
                                        <select class="select2 form-control" name="payment_type" id="payment_type" data-placeholder="Select Payment Type" required>
                                            <option value=""></option>
                                            <option value="Debit">Debit</option>
                                            <option value="Credit">Credit</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="payment_code">Payment Code</label>
                                        <select class="select2 form-control" name="payment_code" id="payment_code" data-placeholder="Select Payment Code" required>
                                            <option value=""></option>
                                            @foreach($payment_codes as $code)
                                            <option value="{{$code->id}}">{{$code->description}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="amount_month">Amount Month</label>
                                        <select class="select2 form-control" name="amount_month" id="amount_month" data-placeholder="Select Amount Month" required>
                                            <option value=""></option>
                                            <option value="January">January</option>
                                            <option value="Febuary">Febuary</option>
                                            <option value="March">March</option>
                                            <option value="April">April</option>
                                            <option value="May">May</option>
                                            <option value="June">June</option>
                                            <option value="July">July</option>
                                            <option value="August">August</option>
                                            <option value="September">September</option>
                                            <option value="October">October</option>
                                            <option value="November">November</option>
                                            <option value="December">December</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="amount_year">Amount Year</label>
                                        <select class="select2 form-control" name="amount_year" id="amount_year" data-placeholder="Select Amount Year" required>
                                            <option value=""></option>
                                            @for($i = 2019; $i < 2030; $i++)
                                            <option value="{{$i}}">{{$i}}</option> 
                                            @endfor 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="billing_period_from">Billing Period From</label>
                                        <input type="text" name="billing_period_from" id="billing_period_from" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="billing_period_to">Billing Period To</label>
                                        <input type="text" name="billing_period_to" id="billing_period_to" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Add Modal -->

        <!--start edit Modal -->
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Employee Payment</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_employee_id">Employee</label>
                                        <select class="select2 form-control" name="employee_id" id="edit_employee_id" data-placeholder="Select Employee" required>
                                            <option value=""></option>
                                            @foreach(employees() as $employee)
                                            <option value="{{$employee->id}}">{{$employee->employee_id. ' - '.$employee->employee_code.' - '.$employee->first_name.'  '.$employee->middle_name.'  '.$employee->last_name . '-' . $employee->desigination}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_amount">Amount</label>
                                        <input type="text" id="edit_amount" class="form-control" placeholder="Enter Amount" name="amount" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_payment_type">Payment Type</label>
                                        <select class="select2 form-control" name="payment_type" id="edit_payment_type" data-placeholder="Select Payemnt" required>
                                            <option value=""></option>
                                            <option value="Debit">Debit</option>
                                            <option value="Credit">Credit</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_payment_code">Payment Description</label>
                                        <select class="form-control" name="payment_code" id="edit_payment_code" data-placeholder="Select Payment Code" required>
                                            <option value=""></option>
                                            @foreach($payment_codes as $code)
                                            <option value="{{$code->id}}">{{$code->description}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_amount_month">Amount Month</label>
                                        <select class="select2 form-control" name="amount_month" id="edit_amount_month" data-placeholder="Select Amount Month" required>
                                            <option value=""></option>
                                            <option value="January">January</option>
                                            <option value="Febuary">Febuary</option>
                                            <option value="March">March</option>
                                            <option value="April">April</option>
                                            <option value="May">May</option>
                                            <option value="June">June</option>
                                            <option value="July">July</option>
                                            <option value="August">August</option>
                                            <option value="September">September</option>
                                            <option value="October">October</option>
                                            <option value="November">November</option>
                                            <option value="December">December</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_amount_year">Amount Year</label>
                                        <select class="select2 form-control" name="amount_year" id="edit_amount_year" data-placeholder="Select Amount Year" required>
                                            <option value=""></option>
                                            @for($i = 2019; $i < 2051; $i++)
                                            <option value="{{$i}}">{{$i}}</option> 
                                            @endfor 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_billing_period_from">Billing Period From</label>
                                        <input type="text" name="billing_period_from" id="edit_billing_period_from" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_billing_period_to">Billing Period To</label>
                                        <input type="text" name="billing_period_to" id="edit_billing_period_to" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End edit Modal -->
    </div>
</section>

@endsection
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function() {
        datatable = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('employeePayments.index') }}",
            columns: [
                {
                    data: 'responsive_id'
                },
                {
                    data : 'DT_RowIndex',
                    name : 'DT_RowIndex',
                    searchable: false,
                },
                {
                    data: "employee_id",
                    name: "employees.employee_id"
                },
                {
                    data: "employee_code",
                    name: "employees.employee_code"
                },
                {
                    render: function (data, type, full, meta) {
                        return full['first_name'] + ' ' + full['last_name'];
                    },
                    searchable: false
                },
                {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                {
                    data: 'amount',
                    name : 'employee_payments.amount',
                },
                {
                    data: 'payment_type',
                    name : 'employee_payments.payment_type',
                },
                {
                    data: 'description',
                    name : 'payment_codes.description',
                },
                {
                    data: 'amount_month',
                    name : 'employee_payments.amount_month',
                },
                {
                    data: 'amount_year',
                    name : 'employee_payments.amount_year',
                },
                {
                    data: 'billing_period_from',
                    name : 'employee_payments.billing_period_from',
                },
                {
                    data: 'billing_period_to',
                    name : 'employee_payments.billing_period_to',
                },
                {
                    data: ''
                },
            ],
            "columnDefs": [
                {
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return (
                            '<a href="javascript:;" class="item-edit" onclick=edit('+full.id+')>' +
                            feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                            '</a>'+
                            '<a href="javascript:;" onclick="delete_item('+full.id+')">' +
                            feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                            '</a>'
                            );
                    }
                },
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }
            ],
            "order": [[0, 'asc']],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            aLengthMenu: [
                [10,25,50,100,-1],
                [10,25,50,100,"All"]
            ],
            buttons: [
                {
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                    buttons: [
                    {
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    }
                    ],
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                        }, 50);
                    }
                    
                },
                {
                    text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                    className: 'create-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });

        $('div.head-label').html('<h6 class="mb-0">List of Employee Payments</h6>');
        // Store Record
        $("#add_form").submit(function (e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            $.ajax({
                url: "{{route('employeePayments.store')}}",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                responsive: true,
                success: function (response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        $('#add_form')[0].reset();
                        $(".select2").val('').trigger('change')
                        $("#add_modal").modal("hide");
                        datatable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Employee Payment has been Added Successfully!'
                        })
                    }
                    
                }
            });
        });

        // Update record
        $("#edit_form").on("submit", function (e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            $.ajax({
                url: "{{url('employeePayments')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        $("#edit_modal").modal("hide");
                        $(".select2").val('').trigger('change')
                        datatable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Employee Payment has been Updated Successfully!'
                        })
                    }
                }
            });
        });
    });

    function edit(id) 
    {
        rowid = id;
        $.ajax({
            url: "{{url('employeePayments')}}" + "/" + id + "/edit",
            type: "get",
            success: function (response) {
                $("#edit_employee_id").val(response.employee_id).select2();
                $("#edit_amount").val(response.amount);
                $("#edit_payment_type").val(response.payment_type).select2();
                $("#edit_payment_code").val(response.payment_code).select2();
                $("#edit_amount_month").val(response.amount_month).select2();
                $("#edit_amount_year").val(response.amount_year).select2();
                $("#edit_billing_period_from").val(response.billing_period_from);
                $("#edit_billing_period_to").val(response.billing_period_to);
                $("#edit_modal").modal("show");
            },
        });
    }

    function delete_item(id)
    {
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: 'Are you sure you want to delete!',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function(){
                            $.ajax({
                            url: "employeePayments/" + id,
                            type: "DELETE",
                            data : {
                                _token: "{{ csrf_token() }}"
                            },
                            success: function (response) {
                                if(response.error_message){
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                }
                                else{
                                    datatable.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Employee Payment has been Deleted Successfully!'
                                    })
                                }
                            }
                        });
                    }
                },
                cancel: function () {
                    $.alert('Canceled!');
                },
            }
        });
    }
</script>
@endsection