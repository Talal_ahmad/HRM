@extends('Admin.layouts.master')
@section('title', 'Payroll Columns Bulk Edit')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Payroll Columns Bulk Edit</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashbaord</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Payroll Columns Bulk Edit
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <form id="edit_form" action="{{url('payroll_bulk_edit')}}" method="POST">
                        @csrf
                        <div class="card">
                            <div class="card-header border-bottom p-1" style="user-select: auto;">
                                <div class="head-label" style="user-select: auto;">
                                    <h6 class="mb-0" style="user-select: auto;">Edit Payroll Columns</h6>
                                </div>
                            </div>
                            <div class="table table-responsive">
                                <table class="table" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Column Order</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($payroll_columns as $key => $payroll_column)
                                            <tr>
                                                <td>{{$payroll_column->name}}</td>
                                                <td>
                                                    <input type="hidden" id="payroll_id" class="form-control" value="{{$payroll_column->id}}" name="participant[payroll_id][]"/>
                                                    <input type="number" id="{{$key}}_colorder" class="form-control" value="{{$payroll_column->colorder}}" name="participant[colorder][]"/>
                                                </td>
                                                <td>
                                                    <select name="participant[status][]" id="{{$key}}_status" class="select2 form-select">
                                                        <option value="1" {{$payroll_column->is_visible == 1 ? 'selected' : "" }}>Yes</option>
                                                        <option value="0" {{$payroll_column->is_visible == 0 ? 'selected' : "" }}>No</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <button type="submit" class="form_save btn btn-primary" id="save">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $("#edit_form").submit(function(){
                ButtonStatus('.form_save',true);
                        blockUI();
            }); 
    </script>
@endsection