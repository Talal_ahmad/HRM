 @extends('Admin.layouts.master')

 @section('title', 'Medical Claims')
 @section('content')
 <section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Medical Claims</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Finance</a>
                            </li>
                            <li class="breadcrumb-item active">Medical Claims
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section> 
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Employee ID</th>
                                    <th>Employee Code</th>
                                    <th>Employee</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Description</th>
                                    <th class="not_include">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <!--Add Modal -->
        <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Medical Claim</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="employee_id">Employee</label>
                                        <select class="select2 form-control" name="employee_id" id="employee_id" data-placeholder="Select Employee" required>
                                            <option value=""></option>
                                            @foreach(employees() as $employee)
                                            <option value="{{$employee->id}}">{{$employee->employee_id. ' - '.$employee->employee_code.' - '.$employee->first_name.' '.$employee->middle_name.'  '.$employee->last_name. ' - '.$employee->desigination}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="type">Insurance Type</label>
                                        <select class="select2 form-control" name="type" id="type" data-placeholder="Select Insurance Type" required>
                                            <option value=""></option>
                                            @foreach($insurance_types as $type)
                                            <option value="{{$type->id}}">{{$type->type}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="sub_type">Insurance Sub Type</label>
                                        <select class="select2 form-control" name="sub_type" id="sub_type" data-placeholder="Select InsuranceSub Type" required>
                                            <option value=""></option>
                                            @foreach($InsuranceSubTypes as $sub_type)
                                            <option value="{{$sub_type->id}}">{{$sub_type->sub_type}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="claim_date">Claim Date</label>
                                        <input type="text" name="claim_date" id="claim_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                    </div>
                                </div>             
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="amount">Claim Amount</label>
                                        <input type="text" name="amount" id="amount" class="form-control" placeholder="Enter Amount" required/>
                                    </div>
                                </div> 
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control" cols="10" rows="2" placeholder="Type here..."></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="attachment" class="form-label">Attachment</label>
                                        <input type="file" class="form-control" name="attachment" id="attachment">
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End Add Modal -->

        <!--start edit Modal -->
        <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Edit Medical Claim</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_employee_id">Employee</label>
                                        <select class="select2 form-control" name="employee_id" id="edit_employee_id" data-placeholder="Select Employee" required>
                                            <option value=""></option>
                                            @foreach(employees() as $employee)
                                            <option value="{{$employee->id}}">{{$employee->employee_id. ' - '.$employee->employee_code.' - '.$employee->first_name.' '.$employee->middle_name.'  '.$employee->last_name. ' - '.$employee->desigination}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_type">Insurance Type</label>
                                        <select class="select2 form-control" name="type" id="edit_type" data-placeholder="Select Insurance Type" required>
                                            <option value=""></option>
                                            @foreach($insurance_types as $type)
                                            <option value="{{$type->id}}">{{$type->type}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_sub_type">Insurance Sub Type</label>
                                        <select class="select2 form-control" name="sub_type" id="edit_sub_type" data-placeholder="Select Insurance Sub Type" required>
                                            <option value=""></option>
                                            @foreach($InsuranceSubTypes as $sub_type)
                                            <option value="{{$sub_type->id}}">{{$sub_type->sub_type}}</option>
                                            @endforeach 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_claim_date">Claim Date</label>
                                        <input type="text" name="claim_date" id="edit_claim_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                    </div>
                                </div>             
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_amount">Claim Amount</label>
                                        <input type="text" name="amount" id="edit_amount" class="form-control" placeholder="Claim Amount" required/>
                                    </div>
                                </div> 
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit_description">Description</label>
                                        <textarea name="description" id="edit_description" class="form-control" cols="10" rows="2" placeholder="Type here..." ></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label for="edit_attachment" class="form-label">Attachment</label>
                                        <input type="file" class="form-control" name="attachment" id="edit_attachment">
                                    </div>
                                </div>                                    
                            </div> 
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End edit Modal -->
    </div>
</section>

@endsection
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function() {
        datatable = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('medicalClaims.index') }}",
            columns: [
                {
                    data: 'responsive'
                },
                {
                    data : 'rownum',
                    name : 'rownum', 
                    searchable: false 
                },
                {
                    data: 'employee_id',
                    name: 'employees.employee_id'
                },
                {
                    data: 'employee_code',
                    name: 'employees.employee_code'
                },
                {
                    data: 'employee',
                    searchable: false
                },
                {
                    data: 'first_name',
                    name: 'employees.first_name',
                    searchable: true,
                    visible: false
                },
                {
                    data: 'last_name',
                    name: 'employees.last_name',
                    searchable: true,
                    visible: false
                },
                {
                    data: 'description',
                    name : 'medical_claims.description',
                },
                
                {
                    data: ''
                },
            ],
            "columnDefs": [
                {
                    // For Responsive
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable:false,
                    render: function (data, type, full, meta) {
                        return (
                            '<a href="javascript:;" class="item-edit" onclick=edit('+full.id+')>' +
                            feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                            '</a>'+
                            '<a href="javascript:;" onclick="delete_item('+full.id+')">' +
                            feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                            '</a>'
                            );
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [[0, 'asc']],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            aLengthMenu: [
                [10,25,50,100,-1],
                [10,25,50,100,"All"]
            ],
            buttons: [
                {
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                    buttons: [
                    {
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                        className: 'dropdown-item',
                        action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                    }
                    ],
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                        }, 50);
                    }
                    
                },
                {
                    text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                    className: 'create-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Medical Claims</h6>');

        // Save Record
        $("#add_form").submit(function (e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            $.ajax({
                url: "{{route('medicalClaims.store')}}",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                responsive: true,
                success: function (response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    console.log(response);
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        $('#add_form')[0].reset();
                        $(".select2").val('').trigger('change')
                        $("#add_modal").modal("hide");
                        datatable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Medical Claim has been Added Successfully!'
                        })
                    }
                }
            });
        });


        // Update record
        $("#edit_form").on("submit", function (e) {
            ButtonStatus('.form_save',true);
                    blockUI();
            e.preventDefault();
            $.ajax({
                url: "{{url('medicalClaims')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (response) {
                    ButtonStatus('.form_save',false);
                        $.unblockUI();
                    if(response.errors){
                        $.each( response.errors, function( index, value ){
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if(response.error_message){
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    else{
                        $("#edit_modal").modal("hide");
                        $(".select2").val('').trigger('change')
                        datatable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Medical Claim has been Updated Successfully!'
                        })
                    }
                }
            });
        });
    });

    function edit(id) 
    {
        rowid = id;
        $.ajax({
            url: "{{url('medicalClaims')}}" + "/" + id + "/edit",
            type: "get",
            success: function (response) {
                $("#edit_employee_id").val(response.employee_id).select2();
                $("#edit_type").val(response.type).select2();
                $("#edit_sub_type").val(response.sub_type).select2();
                $("#edit_claim_date").val(response.claim_date);
                $("#edit_amount").val(response.amount);
                $("#edit_description").val(response.description);
                // $("#edit_attachment").val(response.attachment);
                $("#edit_modal").modal("show");
            },
        });
    }

    function delete_item(id)
    {
        $.confirm({
            icon: 'far fa-question-circle',
            title: 'Confirm!',
            content: 'Are you sure you want to delete!',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                Confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function(){
                            $.ajax({
                            url: "medicalClaims/" + id,
                            type: "DELETE",
                            data : {
                                _token: "{{ csrf_token() }}",
                            },
                            success: function (response) {
                                if(response.error_message){
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'An error has been occured! Please Contact Administrator.'
                                    })
                                }
                                else{
                                    datatable.ajax.reload();
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Medical Claim has been Deleted Successfully!'
                                    })
                                }
                            }
                        });
                    }
                },
                cancel: function () {
                    $.alert('Canceled!');
                },
            }
        });
    }
</script>
@endsection