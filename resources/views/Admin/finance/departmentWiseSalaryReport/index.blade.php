@extends('Admin.layouts.master')
@section('title', 'Department Wise Salary Report')

@section('content')

<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Department Wise Salary Report</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Finance</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <!--Search Form -->
                        <div class="card-body">
                            <form id="search_form">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label" for="monthFilter">Select Month:</label>
                                        <input type="month" id="monthFilter" class="form-control" value="{{request('monthFilter')}}"  name="monthFilter" required/>
                                    </div>
                                    {{-- <div class="col-md-3">
                                        <label class="form-label">Department:</label>
                                        <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department">
                                            <option value=""></option>
                                            @foreach (departments() as $department)
                                                <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                            @endforeach  
                                        </select>
                                    </div> --}}
                                    <div class="col-md-3">
                                        <div class="mb-1">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" multiple>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? in_array($department->id,request('departmentFilter')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#departmentFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="locationFilter">Location:</label>
                                            <select name="locationFilter[]" id="locationFilter" class="select2 form-select" data-placeholder="Select Location" multiple>
                                                <option value=""></option>
                                                @foreach ($locations as $location)
                                                    <option value="{{$location->work_station_id}}" {{!empty(request('locationFilter')) ? in_array($location->work_station_id,request('locationFilter')) ? 'selected' : '' : ''}}>{{$location->work_station_id}}-{{$location->city}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#locationFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#locationFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-3">
                                        <label class="form-label" for="locationFilter">Location</label>
                                        <select name="locationFilter" id="locationFilter" class="select2 form-select" data-placeholder="Select Location">
                                            <option value=""></option>
                                            @foreach ($locations as $location)
                                                <option value="{{$location->work_station_id}}" {{$location->work_station_id == request('locationFilter') ? 'selected' : ''}}>{{$location->city}}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}
                                    <div class="col-md-3">
                                        <label class="form-label">Payroll Type:</label>
                                        <select name="payrollFilter" id="payrollFilter" class="select2 form-select" data-placeholder="Select Payroll Type">
                                            <option value=""></option>
                                            @foreach ($payroll_types as $payroll_type)
                                                <option value="{{$payroll_type->id}}" {{$payroll_type->id == request('payrollFilter') ? 'selected' : ''}}>{{$payroll_type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{url('department_wise_salary_report')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card pb-2">
                        <div class="table-responsive">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        {{-- <th class="not_include"></th> --}}
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>Department</th>
                                        <th>Payroll Type</th>
                                        <th>Gross Salary</th>
                                        <th>Total Loan</th>
                                        <th>Total Advance</th>
                                        <th>Total Deductions</th>
                                        <th>Net Salary</th>
                                        <th>Signature</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $sr=0;
                                    @endphp
                                    @foreach ($data as $key => $value)
                                        @if (!empty($value['payroll']))
                                            <tr> 
                                                {{-- <td></td> --}}
                                                <td>{{$sr++}}</td>
                                                <td>{{$value['employee_id']}}</td>
                                                <td>{{$value['employee_code']}}</td>
                                                <td>{{$value['name']}}</td>
                                                <td>{{$value['department']}}</td>
                                                <td>{{$value['payroll']}}</td>
                                                <td style="text-align: right;">{{number_format($value['gross_salary'])}}</td>
                                                <td style="text-align: right;">{{number_format($value['loan_amount'])}}</td>
                                                <td style="text-align: right;">{{number_format($value['advance_amount'])}}</td> 
                                                <td style="text-align: right;">{{number_format($value['deducations'])}}</td>
                                                <td class="bigCol" style="text-align: right;font-size:14px;font-weight:bold">{{number_format($value['net_payable'])}}</td>
                                                <td style="text-align: right;"></td>
                                            </tr>
                                        @endif 
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="6" class="text-end">Totals</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>

@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('#dataTable').DataTable({
                ordering: true,
                order: [[2, 'asc']],
                // "columnDefs": [
                //     {
                //         // For Responsive
                //         className: 'control',
                //         orderable: false,
                //         targets: 0
                //     },
                // ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            footer: true,
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            footer: true,
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            footer: true,
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            customize: function ( xlsx ) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                $('c[r^="K"]', sheet).attr('s', '2');
                            },
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            footer: true,
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',
                            customize: function(doc) {
                                doc.defaultStyle.fontSize = 11; 
                                var rowCount = doc.content[1].table.body.length;
                                
                                for (i = 1; i < rowCount; i++) {
                                doc.content[1].table.body[i][6].alignment = 'right';
                                doc.content[1].table.body[i][7].alignment = 'right';
                                doc.content[1].table.body[i][8].alignment = 'right';
                                doc.content[1].table.body[i][9].alignment = 'right';
                                doc.content[1].table.body[i][10].alignment = 'right';
                                doc.content[1].table.body[i][10].fontSize = 12;
                                doc.content[1].table.body[i][10].bold = true;
                                }
                                // doc.defaultStyle.bold = true; 
                                // doc.styles.tableFooter.fontSize = 12
                                // $('#dataTable').find('tr').each(function(ix, row) {
                                //     var index = ix;
                                //     $(row).find('td').each(function(ind, column) {
                                //         if ($(column).hasClass('bigCol')) {
                                //             var fontSize = $(column).css('font-size').replace('px', '');
                                //             doc.content[1].table.body[index][ind].style = {
                                //                 // alignment = 'right',
                                //                 fontSize: 12,
                                //                 bold: true
                                //             };
                                //         }
                                //     });
                                // });
                            },
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            footer: true,
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                // responsive: {
                //     details: {
                //         display: $.fn.dataTable.Responsive.display.childRowImmediate,
                //         type: 'column',
                //     }
                // },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                },
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
        
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
        
                    // Total over all pages
                    total_basic_salary = api.column(6).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    total_loan = api.column(7).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    total_advance = api.column(8).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    total_deducations = api.column(9).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    total_netpayable = api.column(10).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    // Update footer
                    $( api.column(6).footer() ).html(
                        numberWithCommas(parseFloat(total_basic_salary).toFixed(2))
                    );

                    $( api.column(7).footer() ).html(
                        numberWithCommas(parseFloat(total_loan).toFixed(2))
                    );

                    $( api.column(8).footer() ).html(
                        numberWithCommas(parseFloat(total_advance).toFixed(2))
                    );

                    $( api.column(9).footer() ).html(
                        numberWithCommas(parseFloat(total_deducations).toFixed(2))
                    );

                    $( api.column(10).footer() ).html(
                        numberWithCommas(parseFloat(total_netpayable).toFixed(2))
                    );
                }
            });
            $('div.head-label').html('<h6 class="mb-0">Department Wise Salary Report</h6>');
        });

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>
@endsection