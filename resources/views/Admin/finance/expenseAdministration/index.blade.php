@extends('Admin.layouts.master')
@section('title', 'Expense Administration')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Expense Administration</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Expense Administration
                                </li>
                            </ol>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="expense-categories-tab" data-bs-toggle="tab" href="#expense-categories" aria-controls="expense-categories" role="tab" aria-selected="true">Expenses Categories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="payment-method-tab" data-bs-toggle="tab" href="#payment-method" aria-controls="payment-method" role="tab" aria-selected="false" onclick="payment_method_dataTable()">Payment Methods</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employee-expense-tab" data-bs-toggle="tab" href="#employee-expense" aria-controls="employee-expense" role="tab" aria-selected="false" onclick="employee_expenses_dataTable()">Employee Expenses</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="expense-categories" aria-labelledby="expense-categories-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <table class="table" id="expense_categories_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Employee Expenses --}}
                    <div class="tab-pane" id="employee-expense" aria-labelledby="employee-expense-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <table class="table" id="employee_expense_datatable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Employee ID</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Date</th>
                                                    <th>Payment Method</th>
                                                    <th>Payee</th>
                                                    <th>Category</th>
                                                    <th>Currency</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Expense Payment Method--}}
                    <div class="tab-pane" id="payment-method" aria-labelledby="payment-method-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <table class="table" id="payment_method">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <!--Expense Categories Add Modal -->
                    <div class="modal fade text-start" id="expense_category_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Add Expense Category</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="expense_category_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="name">Name</label>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Expense Category Name" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Expense Categories Add Modal -->

                    <!--Expense Categories Edit Modal -->
                    <div class="modal fade text-start" id="expense_category_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Edit Expense Category</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="expense_category_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_name">Name</label>
                                                    <input type="text" name="name" id="edit_name" class="form-control" placeholder="Expense Category Name" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Expense Categories Edit Modal -->

                    <!--Payment Methods Add Modal -->
                    <div class="modal fade text-start" id="payment_method_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Add Payment Method</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="payment_method_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="payment_method_name">Name</label>
                                                    <input type="text" name="name" id="payment_method_name" class="form-control" placeholder="Payment Method Name" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End payment Method Add Modal -->

                    <!--Payment Method Edit Modal -->
                    <div class="modal fade text-start" id="payment_method_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Edit Payment Method</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="payment_method_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_payment_method_name">Name</label>
                                                    <input type="text" name="name" id="edit_payment_method_name" class="form-control" placeholder="Payment Method Name" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Payment Method Edit Modal -->

                    <!--Employee Expense Add Modal -->
                    <div class="modal fade text-start" id="employee_expense_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Add Employee Expense</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="employee_expense_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="employee">Employees</label>
                                                    <select name="employee" id="employee" class="select2 form-select" data-placeholder="Select Employee" required>
                                                        <option value=""></option>
                                                        @foreach (employees() as $employee)
                                                            <option value="{{$employee->id}}">{{$employee->employee_id}} - {{$employee->employee_code}} - {{$employee->first_name}} {{$employee->last_name}} - {{$employee->desigination}} - ({{$employee->department}})</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="expense_date">Expense Date</label>
                                                    <input type="text" name="expense_date" id="expense_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="payment_method">Payment Method</label>
                                                    <select name="payment_method" id="payment_method" class="select2 form-select" data-placeholder="Select Payment Method" required>
                                                        <option value=""></option>
                                                        @foreach ($paymentMethod as $method)
                                                            <option value="{{$method->id}}">{{$method->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="transaction_no">Transaction / Ref No</label>
                                                    <input type="text" name="transaction_no" id="transaction_no" class="form-control" placeholder="Transaction / Ref No">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="payee">Payee</label>
                                                    <input type="text" name="payee" id="payee" class="form-control" placeholder="Payee" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="currency">Currency</label>
                                                    <select name="currency" id="currency" class="select2 form-select" data-placeholder="Select Currency" required>
                                                        <option value=""></option>
                                                        @foreach ($currencyTypes as $currency)
                                                            <option value="{{$currency->id}}">{{$currency->code}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="amount">Amount</label>
                                                    <input type="text" name="amount" id="amount" class="form-control" placeholder="Amount" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="category">Expense Category</label>
                                                    <select name="category" id="category" class="select2 form-select" data-placeholder="Select Expense Category" required>
                                                        <option value=""></option>
                                                        @foreach ($expenseCategory as $category)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label for="attachment1" class="form-label">Receipt</label>
                                                    <input class="form-control" name="attachment1" type="file" id="attachment1">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label for="attachment2" class="form-label">Other Attachment 1</label>
                                                    <input class="form-control" name="attachment2" type="file" id="attachment2">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label for="attachment3" class="form-label">Other Attachment 2</label>
                                                    <input class="form-control" name="attachment3" type="file" id="attachment3">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="notes">Note</label>
                                                    <textarea name="notes" id="notes" cols="20" rows="2" class="form-control" placeholder="Type here..." required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Employee Expense Add Modal -->

                    <!--Employee Expense Edit Modal -->
                    <div class="modal fade text-start" id="employee_expense_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Edit Employee Expense</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="employee_expense_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_employee">Employees</label>
                                                    <select name="employee" id="edit_employee" class="select2 form-select" data-placeholder="Select Employee" required>
                                                        <option value=""></option>
                                                        @foreach (employees() as $employee)
                                                            <option value="{{$employee->id}}">{{$employee->employee_id}} - {{$employee->employee_code}} - {{$employee->first_name}} {{$employee->last_name}} - {{$employee->desigination}} - ({{$employee->department}})</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_expense_date">Expense Date</label>
                                                    <input type="text" name="expense_date" id="edit_expense_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_payment_method">Payment Method</label>
                                                    <select name="payment_method" id="edit_payment_method" class="select2 form-select" data-placeholder="Select Payment Method" required>
                                                        <option value=""></option>
                                                        @foreach ($paymentMethod as $method)
                                                            <option value="{{$method->id}}">{{$method->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_transaction_no">Transaction / Ref No</label>
                                                    <input type="text" name="transaction_no" id="edit_transaction_no" class="form-control" placeholder="Transaction / Ref No">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_payee">Payee</label>
                                                    <input type="text" name="payee" id="edit_payee" class="form-control" placeholder="Payee" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_currency">Currency</label>
                                                    <select name="currency" id="edit_currency" class="select2 form-select" data-placeholder="Select Currency" required>
                                                        <option value=""></option>
                                                        @foreach ($currencyTypes as $currency)
                                                            <option value="{{$currency->id}}">{{$currency->code}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_amount">Amount</label>
                                                    <input type="text" name="amount" id="edit_amount" class="form-control" placeholder="Amount" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_category">Expense Category</label>
                                                    <select name="category" id="edit_category" class="select2 form-select" data-placeholder="Select Expense Category" required>
                                                        <option value=""></option>
                                                        @foreach ($expenseCategory as $category)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label for="attachment1" class="form-label">Receipt</label>
                                                    <input class="form-control" name="attachment1" type="file" id="attachment1">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label for="attachment2" class="form-label">Other Attachment 1</label>
                                                    <input class="form-control" name="attachment2" type="file" id="attachment2">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label for="attachment3" class="form-label">Other Attachment 2</label>
                                                    <input class="form-control" name="attachment3" type="file" id="attachment3">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_notes">Note</label>
                                                    <textarea name="notes" id="edit_notes" cols="20" rows="2" class="form-control" placeholder="Type here..." required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Employee Expense Edit Modal -->
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var datatable;
        var rowid;
        $(document).ready(function() {
            datatable = $('#expense_categories_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('expenseAdministration.index') }}",
                    data: {
                        type: "expenseCategories"
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'rownum',
                        name : 'rownum',
                        searchable: false,
                    }, 
                    {
                        data: 'name',
                        name : 'name',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable:false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=expense_category_edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="delete_expense_category('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Expense Categories',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Expense Categories',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Expense Categories',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Expense Categories',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Expense Categories',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#expense_category_add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('#expense-categories div.head-label').html('<h6 class="mb-0">List of Expense Categories</h6>');

            // Save Expense Category form
            $("#expense_category_add_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'expense_categories');
                $.ajax({
                    url: "{{route('expenseAdministration.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#expense_category_add_modal").modal("hide");
                            document.getElementById("expense_category_add_form").reset();
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Expense Category has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Expense Category Update
            $("#expense_category_edit_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'expenseCategories');
                $.ajax({
                    url: "{{url('expenseAdministration')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#expense_category_edit_modal").modal("hide");
                            document.getElementById("expense_category_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Expense Category has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Save Payment Method form
            $("#payment_method_add_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'payment_method');
                $.ajax({
                    url: "{{route('expenseAdministration.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#payment_method_add_modal").modal("hide");
                            document.getElementById("payment_method_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#payment_method').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Payment Method has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Payment Method Update
            $("#payment_method_edit_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'payment_method');
                $.ajax({
                    url: "{{url('expenseAdministration')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#payment_method_edit_modal").modal("hide");
                            document.getElementById("payment_method_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#payment_method').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Payment Method has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Store Employee Expense form
            $("#employee_expense_add_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{route('expenseAdministration.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#employee_expense_add_modal").modal("hide");
                            document.getElementById("employee_expense_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#employee_expense_datatable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Expense has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Employee Expense Update
            $("#employee_expense_edit_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{url('expenseAdministration')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#employee_expense_edit_modal").modal("hide");
                            document.getElementById("employee_expense_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            employee_expenses_dataTable();
                            $('#employee_expense_datatable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Expense has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });
        });

        // Employee Expense DataTable
        function employee_expenses_dataTable()
        {
            $('#employee_expense_datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: "{{ route('expenseAdministration.index') }}",
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'rownum',
                        name : 'rownum',
                        searchable: false
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        render: function (data, type, full, meta) {
                            return full['employee_id']+ ' - '+full['employee_code']+ ' - '+full['first_name'] + ' ' + full['last_name'];
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'expense_date',
                        name : 'employeeexpenses.expense_date',
                    },
                    {
                        data: 'paymentMethod_name',
                        name : 'expensespaymentmethods.name',
                    },
                    {
                        data: 'payee',
                        name : 'employeeexpenses.payee',
                    },
                    {
                        data: 'category_name',
                        name : 'expensescategories.name',
                    },
                    {
                        data: 'code',
                        name : 'currencytypes.code',
                    },
                    {
                        data: 'amount',
                        name : 'employeeexpenses.amount',
                    },
                    {
                        data: 'status',
                        name : 'employeeexpenses.status',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=employee_expense_edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="delete_employee_expense('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employee Expenses',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employee Expenses',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employee Expenses',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employee Expenses',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employee Expenses',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#employee_expense_add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            
            $('#employee-expense div.head-label').html('<h6 class="mb-0">List of Employees Expenses</h6>');
        }

        // Expense Payment Method DataTable
        function payment_method_dataTable()
        {
            $('#payment_method').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering : false,
                destroy: true,
                ajax: {
                    url: "{{ route('expenseAdministration.index') }}",
                    data: {
                        type: "payment_method"
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'rownum',
                        name: 'rownum',
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=payment_method_edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="delete_payment_method('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Payment Methods',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Payment Methods',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Payment Methods',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Payment Methods',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Payment Methods',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#payment_method_add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });

            $('#payment-method div.head-label').html('<h6 class="mb-0">List of Payment Methods</h6>');
        }

        // Expense Category Edit
        function expense_category_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('expenseAdministration')}}" + "/" + id + "/edit",
                type: "get",
                data: {type : 'expenseCategories'},
                success: function (response) {
                    $("#edit_name").val(response.name);
                    $("#expense_category_edit_modal").modal("show");
                },
            });
        }

        // Delete Expense Category
        function delete_expense_category(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                                $.ajax({
                                url: "expenseAdministration/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}",
                                    type: 'expenseCategories'
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Expense Category has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Payment Method Edit
        function payment_method_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('expenseAdministration')}}" + "/" + id + "/edit",
                type: "get",
                data: {type : 'payment_method'},
                success: function (response) {
                    $("#edit_payment_method_name").val(response.name);
                    $("#payment_method_edit_modal").modal("show");
                },
            });
        }

        // Delete Payment Method
        function delete_payment_method(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                                $.ajax({
                                url: "expenseAdministration/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}",
                                    type: 'payment_method'
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        $('#payment_method').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Payment Method has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Employee Expense Edit
        function employee_expense_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('expenseAdministration')}}" + "/" + id + "/edit",
                type: "get",
                success: function (response) {
                    $("#edit_employee").val(response.employee).select2();
                    $('#edit_expense_date').val(response.expense_date);
                    $("#edit_payment_method").val(response.payment_method).select2();
                    $('#edit_transaction_no').val(response.transaction_no);
                    $('#edit_payee').val(response.payee);
                    $("#edit_category").val(response.category).select2();
                    $('#edit_amount').val(response.amount);
                    $("#edit_currency").val(response.currency).select2();
                    $('#edit_notes').text(response.notes);
                    $("#employee_expense_edit_modal").modal("show");
                },
            });
        }

        // Delete Employee Expense
        function delete_employee_expense(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                                $.ajax({
                                url: "expenseAdministration/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } 
                                    else{
                                        $('#employee_expense_datatable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Employee Expense has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection