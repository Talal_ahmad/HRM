@extends('Admin.layouts.master')
@section('title', 'Payroll Accrual Payment')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0"></h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Payroll Accrual Payment
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <!--Search Form -->
                        <div class="card-body">
                            <form action="{{ url('payroll-accrual-payment') }}" action="GET">
                                <div class="row g-1 mb-md-1">
                                    <div class="col-md-4">
                                        <label class="form-label">Payment For Accrual Date From:</label>
                                        <input type="text" name="fromDateFilter" id="fromDateFilter"
                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                            value="{{ request('fromDateFilter') }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Payment For Accrual Date To:</label>
                                        <input type="text" name="toDateFilter" id="toDateFilter"
                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                            value="{{ request('toDateFilter') }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Department:</label>
                                        <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select"
                                            data-placeholder="Select Department" multiple>
                                            <option value="">Select Department</option>
                                            @foreach ($departments as $department)
                                                <option value="{{ $department->id }}"
                                                    {{ in_array($department->id, request('departmentFilter') ?? []) ? 'selected' : '' }}>
                                                    {{ $department->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div class="button-container mt-1">
                                            <button class="btn btn-sm btn-primary" type="button"
                                                onclick="selectAll('#departmentFilter')">Select All</button>
                                            <button class="btn btn-sm btn-danger" type="button"
                                                onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Employee:</label>
                                        <select name="employeeFilter[]" id="employeeFilter" class="select2 form-select"
                                            data-placeholder="Select Employee" multiple>
                                            <option value="">Select Employee</option>
                                            @foreach ($all_employees as $employee)
                                                <option value="{{ $employee->id }}"
                                                    {{ in_array($employee->id, request('employeeFilter') ?? []) ? 'selected' : '' }}>
                                                    {{ $employee->employee_code }} - {{ $employee->first_name }}
                                                    {{ $employee->last_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div class="button-container mt-1">
                                            <button class="btn btn-sm btn-primary" type="button"
                                                onclick="selectAll('#employeeFilter')">Select All</button>
                                            <button class="btn btn-sm btn-danger" type="button"
                                                onclick="deselectAll('#employeeFilter')">Deselect All</button>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Employment Status:</label>
                                        <select name="employmentStatusFilter[]" id="employmentStatusFilter"
                                            class="select2 form-select" data-placeholder="Select Status" multiple>
                                            <option value="">Select Department</option>
                                            @foreach ($employment_statuses as $employment_status)
                                                <option value="{{ $employment_status->id }}"
                                                    {{ in_array($employment_status->id, request('employmentStatusFilter') ?? []) ? 'selected' : '' }}>
                                                    {{ $employment_status->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div class="button-container mt-1">
                                            <button class="btn btn-sm btn-primary" type="button"
                                                onclick="selectAll('#employmentStatusFilter')">Select All</button>
                                            <button class="btn btn-sm btn-danger" type="button"
                                                onclick="deselectAll('#employmentStatusFilter')">Deselect All</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-end">
                                        <a href="{{ url('payroll-accrual-payment') }}" type="button"
                                            class="btn btn-danger">Reset</a>
                                        <button class="btn btn-primary">Filter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @if (isset($employees))
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Payment Form</h4>
                            </div>
                            <div class="card-body">
                                @if (count($employees) > 0)
                                    <form class="form" id="payment_form">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-2 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Accrual Months</label>
                                                    <p class="border p-1">{{ implode(',', $months) }}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Bank</label>
                                                    <select name="bank" onchange="bankName(this)"
                                                        class="form-select select2" data-placeholder="Select Bank"
                                                        id="bank" multiple required>
                                                        <option value=""></option>
                                                        @foreach ($banks as $bank)
                                                            <option value="{{ $bank->account_code }}">
                                                                {{ $bank->account_code . ' - ' . $bank->bank_account_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <input type="hidden" name="bank_accounts" id="bank_accounts">
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label">Voucher Date</label>
                                                    <input type="text" name="voucher_date" id="voucher_date"
                                                        class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                        value="Select Date" required>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        @foreach ($employees as $key => $employee)
                                            <div class="row">
                                                <input name="employee_ids[{{ $employee->employee_id }}]" type="hidden"
                                                    value="{{ $employee->employee_id }}">
                                                <input name="employee_ids[{{ $employee->employee_id }}][payroll_id]"
                                                    type="hidden" value="{{ $employee->payroll_id }}">
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Employee</label>
                                                        <p class="border p-1">{{ $employee->employee }} -
                                                            {{ $employee->designation }} - {{ $employee->department }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Bank</label>
                                                        <p class="border p-1">{{ $employee->employee_bank }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Amount</label>
                                                        <input type="text" class="form-control"
                                                            name="employee_ids[{{ $employee->employee_id }}][amount]"
                                                            placeholder="Employee Total"
                                                            value="{{ abs($employee->amount) }}"
                                                            style="text-align: right" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Left To Allocate</label>
                                                        <input type="text" class="form-control"
                                                            name="employee_ids[{{ $employee->employee_id }}][left_to_allocate]"
                                                            placeholder="Pay" value="{{ $employee->left_to_allocate }}"
                                                            style="text-align: right" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">This Allocation</label>
                                                        <input type="text" class="form-control"
                                                            name="employee_ids[{{ abs($employee->employee_id) }}][allocation]"
                                                            placeholder="Pay" style="text-align: right"
                                                            value="{{ $employee->left_to_allocate }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        @endforeach
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ms-1">Make Payment</button>
                                        </div>
                                    </form>
                                @else
                                    <p class="text-danger text-center">No Record Found!</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Check Accrual Payment Modal -->
                    <div class="modal fade text-start" id="check_payment" tabindex="-1" aria-labelledby="check-payment"
                        aria-hidden="true" data-keyboard="false" data-backdrop="static">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="check-payment">Accrual Payment</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Month</th>
                                                        <th>Employee</th>
                                                        <th>Bank</th>
                                                        <th style="text-align:right">Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="check-payment-table-body"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger"
                                        data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
            @endif
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#payment_form").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.confirm({
                    icon: 'far fa-question-circle',
                    title: 'Confirm!',
                    content: 'Are you sure you want to make Payment!',
                    type: 'orange',
                    typeAnimated: true,
                    buttons: {
                        Confirm: {
                            text: 'Confirm',
                            btnClass: 'btn-orange',
                            action: function() {
                                blockUI();
                                $.ajax({
                                    url: "{{ url('payroll-accrual-payment') }}",
                                    type: "post",
                                    data: formData,
                                    processData: false,
                                    contentType: false,
                                    success: function(response) {
                                        $.unblockUI();
                                        if (response.code == 200) {
                                            Toast.fire({
                                                icon: 'success',
                                                title: 'Payment has been Saved Successfully!'
                                            });
                                            var tableBody = $(
                                                '#check-payment-table-body');
                                            var voucher_date = '';
                                            var voucher_no = '';
                                            var totalAmount = 0;
                                            tableBody
                                                .empty(); // Clear any existing rows

                                            // Loop through the data and create rows
                                            $.each(response.result, function(index,
                                                row) {
                                                var newRow = $('<tr>');
                                                newRow.append($('<td>')
                                                    .text(row
                                                        .entry_of_month)
                                                    );
                                                newRow.append($('<td>')
                                                    .text(row.employee));
                                                newRow.append($('<td>')
                                                    .text(row.bank));
                                                newRow.append($(
                                                    '<td style = "text-align:right">'
                                                ).text(Number(row
                                                        .allocation)
                                                    .toLocaleString()
                                                ));
                                                tableBody.append(newRow);
                                                totalAmount += Number(row
                                                    .allocation);
                                                voucher_date = row
                                                    .voucher_date;
                                                voucher_no = row
                                                    .payment_voucher_no;
                                            });
                                            // Display the total values
                                            var totalRow = $('<tr>');
                                            totalRow.append(
                                                '<td style="font-weight:bold">Voucher Date:  ' +
                                                voucher_date + '</td>'
                                            );
                                            totalRow.append(
                                                '<td style="font-weight:bold">Voucher No: ' +
                                                voucher_no + '</td>'
                                            );
                                            totalRow.append(
                                                '<td style="font-weight:bold">Total</td>'
                                            );
                                            totalRow.append($(
                                                '<td style="text-align:right;font-weight:bold">'
                                            ).text(totalAmount
                                                .toLocaleString()));
                                            tableBody.append(totalRow);
                                            $("#check_payment").modal({
                                                backdrop: 'static'
                                            }).modal("show");
                                        } else if (response.code == 404) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: response.message
                                            })
                                        } else {
                                            Swal.fire({
                                                icon: 'error',
                                                title: response.message
                                            });
                                        }
                                    },
                                });
                            }
                        },
                        cancel: function() {
                            $.alert('Canceled!');
                        },
                    }
                });
            });
        });

        function bankName(object) {
            var options = $(object).find("option:selected");
            var selectedOptions = '';
            options.each(function(index) {
                var account = $.trim($(this).text());
                selectedOptions += account;
                if (index < options.length - 1) {
                    selectedOptions += ',';
                }
            });
            $('#bank_accounts').val(selectedOptions);
        }
    </script>
@endsection
