@extends('Admin.layouts.master')
@section('title', 'Accrual Payment Voucher')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0"></h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Accrual Payment Voucher
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-header">
                            <h5 class="card-title">Voucher Detail</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Voucher Date</label>
                                    <p class="border p-1">{{ $employees[0]->voucher_date }}</p>
                                </div>
                                <div class="col-md-4">
                                    <label>Voucher No</label>
                                    <p class="border p-1">{{ $employees[0]->payment_voucher_no }}</p>
                                </div>
                                <div class="col-md-4">
                                    <label>Voucher Month</label>
                                    <p class="border p-1">{{ $employees[0]->entry_of_month }}</p>
                                </div>
                                <div class="col-md-4">
                                    <label>Bank</label>
                                    <p class="border p-1">{{ $employees[0]->bank }}</p>
                                </div>
                                <div class="col-md-4">
                                    <label>Description</label>
                                    <p class="border p-1">{{ $employees[0]->payment_voucher_desc }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Employees Listing</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th style="text-align:right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $total = 0;
                                            @endphp
                                            @foreach ($employees as $employee)
                                                @php
                                                    $total += $employee->allocation;
                                                @endphp
                                                <tr>
                                                    <td>{{ $employee->employee }} - {{ $employee->designation }}</td>
                                                    <td style="text-align:right;">
                                                        {{ number_format($employee->allocation) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td style="font-weight:bold">Total</td>
                                                <td style="text-align:right;font-weight:bold">
                                                    {{ number_format($total) }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
