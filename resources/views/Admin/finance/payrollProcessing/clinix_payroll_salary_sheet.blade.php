<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salary Sheet</title>
</head>

<body>
    <div>
        <div>
            <table>
                <thead>
                    <tr>
                        <td style="text-align: center;font-size:20px" colspan="{{ 3 + count($payroll_columns) }}">
                            <strong>
                                {{ $payroll->name }} Sheet
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <th><strong>Sr.No</strong></th>
                        <th><strong>Name</strong></th>
                        <th><strong>Active/In-Active</strong></th>
                        @foreach ($payroll_columns as $item)
                            <th><strong>{{ $item->name }}</strong></th>
                        @endforeach
                        <th><strong>Department (Type)</strong></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employees as $key => $employee)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>
                                {{ $employee->first_name . ' ' . $employee->last_name . ' ' . $employee->employee_id . ' ' . $employee->job_title . ' ' . $employee->employee_code }}
                            </td>
                            <td>
                                {{ $employee->emp_active_inactive }}
                            </td>
                            @foreach ($payroll_columns as $column)
                                <td>
                                    {{ !empty($column_amount[$employee->id][$column->id]) ? $column_amount[$employee->id][$column->id] : '-' }}
                                </td>
                            @endforeach
                            <td>{{ $employee->department_name }} ({{ $employee->type }})</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" style="text-align: right; font-weight: bold">Net Totals:</td>
                        @foreach ($payroll_columns as $col)
                            @php
                                $total = '';
                                if (!empty($net_total[$col->id])) {
                                    $total = number_format($net_total[$col->id]);
                                }
                            @endphp
                            <td><strong>{{ $total }}</strong></td>
                        @endforeach
                        {{-- <td></td> --}}
                    </tr>
                    @if (!empty($prev_payroll))
                        <tr>
                            <td colspan="2" style="text-align: right; font-weight: bold">Previous Month Totals:</td>
                            @foreach ($payroll_columns as $col)
                                @php
                                    $prev_total = '';
                                    if (!empty($prev_net_total[$col->id])) {
                                        $prev_total = number_format($prev_net_total[$col->id]);
                                    }
                                @endphp
                                <td><strong>{{ $prev_total }}</strong></td>
                            @endforeach
                        </tr>
                        <tr style="border: none">
                            <td style="font-weight: bold;border: none;white-space: nowrap; font-size:50px;">P. MON.
                                <br>Tot. Emp.:
                            </td>
                            <td style="border: none"><strong>{{ count($prevTotalEmployees) }}</strong></td>
                        </tr>
                    @endif
                </tfoot>
            </table>
            <p style="font-size:55px;"><strong>Remarks: {{ $payroll->remarks }}</strong></p>
        </div>
    </div>
</body>

</html>
