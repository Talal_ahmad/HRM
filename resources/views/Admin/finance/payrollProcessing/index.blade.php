@extends('Admin.layouts.master')
@section('title', 'Payroll Processing')

@section('content')
    <style>
        #payroll_report_datatable tbody tr td:last-child {
            white-space: nowrap;
        }
    </style>
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Payroll Processing</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Payroll Processing
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="alert alert-danger">
                <strong>Note!</strong> Please do not change/remove/delete any report of any department of any previous
                month.
                Please create a new report for each new month for each departemnt for salary processing. Disregarding these
                instructions will lead to incorrect salary calculation and disturbance in historical salary data.
            </div>
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="payroll-report-tab" data-bs-toggle="tab" href="#payroll-report"
                            aria-controls="payroll-report" role="tab" aria-selected="true">Payroll Reports</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="payslip-template-tab" data-bs-toggle="tab" href="#payslip-template"
                            aria-controls="payslip-template" role="tab" aria-selected="false"
                            onclick="payslip_template_datatable()">Payslip Templates</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="payroll-history-tab" data-bs-toggle="tab" href="#payroll-history"
                            aria-controls="payroll-history" role="tab" aria-selected="false"
                            onclick="payroll_history()">Payroll History</a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- Start Payroll Report --}}
                    <div class="tab-pane active" id="payroll-report" aria-labelledby="payroll-report-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="payroll_report_datatable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th>Pay Frequency</th>
                                                    <th>Department</th>
                                                    <th>Date Start</th>
                                                    <th>Date End</th>
                                                    <th>Status</th>
                                                    <th>Process By</th>
                                                    <th>Process At</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!-- Add Payroll Modal -->
                        <div class="modal fade text-start" id="add_modal_payrollReports" tabindex="-1"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">Add Payroll</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="add_form_payrollReports">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="name">Name</label>
                                                        <input type="text" name="name" id="name"
                                                            class="form-control" placeholder="Name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="pay_frequency">Pay Frequency</label>
                                                        <select class="select2 form-control" name="pay_period"
                                                            id="pay_frequency" data-placeholder="Select Payfrequency"
                                                            required>
                                                            <option value=""></option>
                                                            @foreach ($payFrequency as $item)
                                                                <option value="{{ $item->id }}">{{ $item->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="deduction_group">Calculation
                                                            Groups</label>
                                                        <select class="select2 form-control" name="deduction_group"
                                                            id="deduction_group"
                                                            data-placeholder="Select Calculation Group" required>
                                                            <option value=""></option>
                                                            @foreach ($calculationGroup as $group)
                                                                <option value="{{ $group->id }}">{{ $group->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="payslipTemplate">Payslip
                                                            Templates</label>
                                                        <select class="select2 form-control" name="payslipTemplate"
                                                            id="payslipTemplate"
                                                            data-placeholder="Select Payslip Template">
                                                            <option value=""></option>
                                                            @foreach ($PayslipTemplate as $payslip)
                                                                <option value="{{ $payslip->id }}">{{ $payslip->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div> 
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="department">Departments</label>
                                                        <select class="select2 form-control" name="department"
                                                            id="department" data-placeholder="Select Department" required>
                                                            <option value=""></option>
                                                            @foreach ($departments as $department)
                                                                <option value="{{ $department->id }}">
                                                                    {{ $department->title }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="date_start">Start Date</label>
                                                        <input type="date" name="date_start" id="date_start"
                                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                            required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="date_end">End Date</label>
                                                        <input type="date" name="date_end" id="date_end"
                                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                            required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="columns">Payroll Columns</label>
                                                        <select class="select2 form-control" name="columns[]"
                                                            id="columns" data-placeholder="Select Payroll Column"
                                                            required multiple>
                                                            <option value=""></option>
                                                            @foreach ($payrollColumns as $column)
                                                                <option value="{{ $column->id }}">{{ $column->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#columns')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#columns')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="status">Status</label>
                                                        <select class="select2 form-control" name="status"
                                                            id="status" required>
                                                            <option value="Draft">Draft</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="remarks">Remarks</label>
                                                        <textarea class="form-control" rows="4" cols="4" name="remarks" style="resize: none"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger"
                                                data-bs-dismiss="modal">Cancel</button>
                                            <button type="submit" class="form_save btn btn-primary"
                                                id="save">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--End Add Payroll Modal -->

                        <!-- Edit Payroll Modal -->
                        <div class="modal fade text-start" id="edit_modal_payrollReports"
                            aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel17">Edit Payroll</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form class="form" id="edit_form_payrollReports">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_name">Name</label>
                                                        <input type="text" name="name" id="edit_name"
                                                            class="form-control" placeholder="Name" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_pay_frequency">Pay
                                                            Frequency</label>
                                                        <select class="select2 form-control" name="pay_period"
                                                            id="edit_pay_frequency" data-placeholder="Select Payfrequency"
                                                            required>
                                                            <option value=""></option>
                                                            @foreach ($payFrequency as $item)
                                                                <option value="{{ $item->id }}">{{ $item->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_deduction_group">Calculation
                                                            Groups</label>
                                                        <select class="select2 form-control" name="deduction_group"
                                                            id="edit_deduction_group"
                                                            data-placeholder="Select Calculation Group" required>
                                                            <option value=""></option>
                                                            @foreach ($calculationGroup as $group)
                                                                <option value="{{ $group->id }}">{{ $group->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_payslipTemplate">Payslip
                                                            Templates</label>
                                                        <select class="select2 form-control" name="payslipTemplate"
                                                            id="edit_payslipTemplate"
                                                            data-placeholder="Select Payslip Template">
                                                            <option value=""></option>
                                                            @foreach ($PayslipTemplate as $payslip)
                                                                <option value="{{ $payslip->id }}">{{ $payslip->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label"
                                                            for="edit_department">Departments</label>
                                                        <select class="select2 form-control" name="department"
                                                            id="edit_department" data-placeholder="Select Department"
                                                            required>
                                                            <option value=""></option>
                                                            @foreach ($departments as $department)
                                                                <option value="{{ $department->id }}">
                                                                    {{ $department->title }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_date_start">Start Date</label>
                                                        <input type="date" name="date_start" id="edit_date_start"
                                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                            required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_date_end">End Date</label>
                                                        <input type="date" name="date_end" id="edit_date_end"
                                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                            required />
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_columns">Payroll
                                                            Columns</label>
                                                        <select class="select2 form-control" name="columns[]"
                                                            id="edit_columns" data-placeholder="Select Payroll Column"
                                                            required multiple>
                                                            <option value=""></option>
                                                            @foreach ($payrollColumns as $column)
                                                                <option value="{{ $column->id }}">{{ $column->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#edit_columns')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#edit_columns')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_status">Status</label>
                                                        <select class="select2 form-control" name="status"
                                                            id="edit_status" required>
                                                            <option value="Draft">Draft</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="remarks">Remarks</label>
                                                        <textarea class="form-control" rows="4" cols="4" name="remarks" id="edit_remarks"
                                                            style="resize: none"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-danger"
                                                data-bs-dismiss="modal">Cancel</button>
                                            <button type="submit" class="form_save btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--End Edit Payroll Modal -->
                    </div>

                    <!-- Download Muliple Modal -->
                    <div class="modal fade text-start" id="download_multiple_modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Muliple Departments Payroll</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="download_multiple_form">
                                    {{-- @csrf --}}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="departments">Departments</label>
                                                    <select type="select-multi" name="departments[]" id="departments"
                                                        class="select2 form-select" data-placeholder="Select Departments"
                                                        multiple required>
                                                        <option value=""></option>
                                                        @foreach ($departments as $department)
                                                            <option value="{{ $department->id }}">
                                                                {{ $department->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="month">Month</label>
                                                    <input type="month" name="month" id="month"
                                                        class="form-control" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger"
                                            data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary"
                                            id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Download Multiple Modal -->

                    <!-- End Payroll Report -->

                    <!-- Start Payslip Template -->

                    <!-- Add Payslip Template Modal -->
                    <div class="modal fade text-start" id="add_modal_payslipTemplates" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Add</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="add_form_payslipTemplates">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="name">Name</label>
                                                    <input type="text" name="name" id="name"
                                                        class="form-control" placeholder="Name" required />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="basic_salary">Basic Salary</label>
                                                    <select class="select2 form-control" name="basic_salary"
                                                        id="basic_salary" data-placeholder="Select Basic Salary">
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            {{-- <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="status">Status</label>
                                                <select class="select2 form-control" name="status" id="status" required>
                                                    <option value="Draft">Draft</option>
                                                    <option value="Completed">Completed</option>
                                                </select>
                                            </div>
                                        </div> --}}
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="gross_salary">Gross Salary</label>
                                                    <select class="select2 form-control" name="gross_salary"
                                                        id="gross_salary" data-placeholder="Select Gross Salary">
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="net_salary">Net Salary</label>
                                                    <select class="select2 form-control" name="net_salary"
                                                        id="net_salary" data-placeholder="Select Net Salary">
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="allowance">Allowance</label>
                                                    <select type="select-multi" name="allowance[]" id="allowance"
                                                        class="select2 form-select" data-placeholder="Select Allowance"
                                                        multiple>
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="deduction">Deduction</label>
                                                    <select type="select-multi" name="deduction[]" id="deduction"
                                                        class="select2 form-select" data-placeholder="Select Deduction"
                                                        multiple>
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger"
                                            data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary"
                                            id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Add Payslip Template Modal -->

                    <!-- Check Payroll Accrual JV Modal -->
                    <div class="modal fade text-start" id="check_payroll_accrual_jv" tabindex="-1"
                        aria-labelledby="check-payroll-accrual-jv-label" aria-hidden="true" data-keyboard="false"
                        data-backdrop="static">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div id="div_to_print">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="check-payroll-accrual-jv-label">Payroll Accrual JV</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Salary Sheet Column</th>
                                                        <th>GL Account</th>
                                                        <th style="text-align:right">Debit Amount</th>
                                                        <th style="text-align:right">Credit Amount</th>
                                                        <th>No. Of Emloyees</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="check-accrual-table-body"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="modal-footer">
                                    <button type="button" onclick="divToPrint()" class="btn btn-primary">Print</button>
                                    <button type="button" class="btn btn-outline-danger"
                                        data-bs-dismiss="modal">Cancel</button>
                                    <button type="submit" class="form_save btn btn-primary"
                                        onclick=payrollAccrualJV()>Send To Erp</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Add Payroll Modal -->

                    <!-- Edit Payslip Modal -->
                    <div class="modal fade text-start" id="edit_modal_payslip" aria-labelledby="myModalLabel17"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Edit Payslip</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="edit_form_payslipReports">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="name">Name</label>
                                                    <input type="text" name="name" id="edit_payname"
                                                        class="form-control" placeholder="Name" required />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_basic_salary">Basic Salary</label>
                                                    <select class="select2 form-control" name="basic_salary"
                                                        id="edit_basic_salary" data-placeholder="Select Basic Salary">
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_gross_salary">Gross Salary</label>
                                                    <select class="select2 form-control" name="gross_salary"
                                                        id="edit_gross_salary" data-placeholder="Select Gross Salary">
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_net_salary">Net Salary</label>
                                                    <select class="select2 form-control" name="net_salary"
                                                        id="edit_net_salary" data-placeholder="Select Net Salary">
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_allowance">Allowance</label>
                                                    <select type="select-multi" name="allowance[]" id="edit_allowance"
                                                        class="select2 form-select" data-placeholder="Select Allowance"
                                                        multiple>
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_deduction">Deduction</label>
                                                    <select type="select-multi" name="deduction[]" id="edit_deduction"
                                                        class="select2 form-select" data-placeholder="Select Deduction"
                                                        multiple>
                                                        <option value=""></option>
                                                        @foreach ($payrollColumns as $Columns)
                                                            <option value="{{ $Columns->id }}">{{ $Columns->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger"
                                            data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Edit Payslip Modal -->

                    <!-- Manage Payroll Employee Columns Modal -->
                    <div class="modal fade text-start" id="payroll-employee-columns-modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Manage Columns</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form class="form" id="payroll-employee-columns-form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="employee_columns">Employee
                                                        Columns</label>
                                                    <select class="select2 form-control" name="employee_columns[]"
                                                        id="employee_columns" data-placeholder="Select Employee Column"
                                                        required multiple>
                                                        <option value=""></option>
                                                        @foreach (payrollEmployeeColumns() as $key => $column)
                                                            <option value="{{ $key }}">{{ $column }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger"
                                            data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="form_save btn btn-primary"
                                            id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Payroll Processing Meta Data Modal Modal -->
                    <div class="modal fade text-start" id="payroll-processing-meta-data-modal" tabindex="-1"
                        aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Payroll Processing Meta Information</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <p class="text-danger">Note: Please Select Updated Employees If You Are
                                                Processing For The First Time.</p>
                                            <div class="mb-1">
                                                <label class="form-label">Procedure</label>
                                                <select class="select2 form-control" id="payroll_process_meta"
                                                    data-placeholder="Select Procedure">
                                                    <option value=""></option>
                                                    <option value="existing">Process With Existing Employees</option>
                                                    <option value="updated">Process With Updated Employees</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger"
                                        data-bs-dismiss="modal">Cancel</button>
                                    <button type="submit" class="form_save btn btn-primary"
                                        onclick="process_payroll()">Process</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="payslip-template" aria-labelledby="payslip-template-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="payslipTemplates_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- End Payslip Template -->

                    <!-- Start Payroll History -->
                    <div class="tab-pane" id="payroll-history" aria-labelledby="payroll-history-tab" role="tabpanel">
                        <section>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <table class="table" id="payroll_history">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Payroll</th>
                                                    <th>Department</th>
                                                    <th>Date Start</th>
                                                    <th>Date End</th>
                                                    <th class="not_include">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- End Payroll History -->
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    @if (Session::has('message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('message') !!}"
            });
        </script>
    @endif
    <script>
        var rowid;
        var company = "{{env('COMPANY')}}";
        $(document).ready(function() {
            $('#payroll_report_datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('payrollProcessing.index') }}",
                    data: {
                        type: "payrollReports"
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                    },
                    {
                        data: 'name',
                        name: 'payroll.name',
                    },
                    {
                        data: 'payfrequency_name',
                        name: 'payfrequency.name',
                    },
                    {
                        data: 'department',
                        name: 'companystructures.title',
                    },
                    {
                        data: 'date_start',
                        name: 'payroll.date_start',
                    },
                    {
                        data: 'date_end',
                        name: 'payroll.date_end',
                    },
                    {
                        data: 'status',
                        name: 'payroll.status',
                    },
                    {
                        data: 'process_by',
                        name: 'users.username',
                    },
                    {
                        data: 'process_at',
                        name: 'payroll.process_at',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            var btn;
                            btn = '';
                            @can('Payroll Processing Edit Button')
                                btn +=
                                    '<a href="javascript:;" class="item-edit" onclick=edit_payroll(' +
                                    full.id + ') title="Edit Payroll">' +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    ' </a>';
                            @endcan
                            @can('Payroll Processing Copy Payroll Button')
                                btn +=
                                    '<a href="javascript:;" class="item-edit" onclick=copy_payroll(' +
                                    full.id + ') title="Copy Payroll">' +
                                    feather.icons['copy'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    ' </a>';
                            @endcan
                            if (full.status != 'Completed') {
                                @can('Payroll Processing Delete Button')
                                    btn += '<a href="javascript:;" onclick="delete_item(' + full
                                        .id +
                                        ')">' +
                                        feather.icons['trash-2'].toSvg({
                                            class: 'font-medium-4 text-danger'
                                        }) +
                                        '</a>';
                                @endcan
                            }
                            if (full.status != 'Completed') {
                                @can('Payroll Processing Process Payroll Button')
                                    btn +=
                                        '<a href="javascript:;" class="item-edit" onclick=askForPPMetaData(' +
                                        full.id + ') title="Process Payroll">' +
                                        feather.icons['cpu'].toSvg({
                                            class: 'font-medium-4'
                                        }) +
                                        '</a>';
                                @endcan
                            }
                            if (full.status == 'Completed') {
                                @can('Payroll Processing View Payroll Button')
                                    btn += '<a href="{{ url('payrollProcessing') }}' + "/" + full
                                        .id + '" class="item-edit" title="View Payroll">' +
                                        feather.icons['eye'].toSvg({
                                            class: 'font-medium-4'
                                        }) +
                                        ' </a>';
                                @endcan
                                @can('Payroll Processing Download Salary Sheet Button')
                                    btn += '<a href="{{ url('payrollProcessing') }}' + "/" + full
                                        .id +
                                        '?type=pdf" class="item-edit" style="color:red" title="Download Salary Sheet PDF" target="_blank">' +
                                        feather.icons['download'].toSvg({
                                            class: 'font-medium-4'
                                        }) +
                                        ' </a>';
                                    btn += '<a href="{{ url('payrollProcessing') }}' + "/" + full
                                        .id +
                                        '?type=excel" class="item-edit" title="Download Salary Sheet Excel" style="color:green" target="_blank">' +
                                        feather.icons['download'].toSvg({
                                            class: 'font-medium-4'
                                        }) +
                                        ' </a>';
                                @endcan
                                if (full.ledgerised == 0 && (full.erp_trans_no == null || full
                                        .erp_trans_no == '')) {
                                    @can('Payroll Accrual JV Button')
                                        btn +=
                                            '<a href="javascript:;" class="item-edit" onclick=checkPayrollAccrualJV(' +
                                            full.id + ',0) title="Payroll Accrual JV">' +
                                            feather.icons['file-text'].toSvg({
                                                class: 'font-medium-4'
                                            }) +
                                            ' </a>';
                                    @endcan
                                }
                                // if(full.ledgerised == 1 && (full.erp_trans_no == null || full.erp_trans_no == ''))
                                // {
                                @can('Re-Process Payroll Accrual JV Button')
                                    btn +=
                                        '<a href="javascript:;" class="item-edit" onclick=checkPayrollAccrualJV(' +
                                        full.id + ',1) title="Re-Process Payroll Accrual JV">' +
                                        feather.icons['command'].toSvg({
                                            class: 'font-medium-4'
                                        }) +
                                        ' </a>';
                                @endcan
                                // }
                                if (full.ledgerised == 1 && full.erp_trans_no != null && full
                                    .erp_trans_no != '' && full.payment == 0) {
                                    @can('Delete Payroll Accrual JV Button')
                                        btn +=
                                            '<a href="javascript:;" onclick=deletePayrollAccrualJV(' +
                                            full.id +
                                            ') class="item-edit" title="Delete Payroll Accrual JV">' +
                                            feather.icons['trash'].toSvg({
                                                class: 'font-medium-4'
                                            }) +
                                            ' </a>';
                                    @endcan
                                }
                                @can('Payroll Employee Columns')
                                    btn +=
                                        '<a href="javascript:;" onclick=payrollEmployeeColumns(' +
                                        full.id +
                                        ') class="item-edit" title="Manage Payroll Employee Columns">' +
                                        feather.icons['activity'].toSvg({
                                            class: 'font-medium-4'
                                        }) +
                                        ' </a>';
                                @endcan
                            }
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-1',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Payroll Report',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Payroll Report',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Payroll Report',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Payroll Report',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Payroll Report',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Payroll Processing Download Multiple Button')
                        {
                            text: feather.icons['download'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Download Multiple',
                            className: 'create-new btn btn-primary me-1',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#download_multiple_modal'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        },
                    @endcan
                    @can('Payroll Processing Add New')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add New',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#add_modal_payrollReports'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#payroll-report div.head-label').html('<h6 class="mb-0">List of Payroll Reports</h6>');

            // Add Payroll
            $("#add_form_payrollReports").submit(function(e) {
                ButtonStatus('.form_save', true);
                blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'payroll');
                $.ajax({
                    url: "{{ route('payrollProcessing.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save', false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Swal.fire({
                                icon: 'error',
                                title: response.error_message,
                            })
                        } else {
                            $('#add_form_payrollReports')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal_payrollReports").modal("hide");
                            $('#payroll_report_datatable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Payroll has been Added Successfully!'
                            })
                        }

                    }
                });
            });

            // Update Payroll
            $("#edit_form_payrollReports").on("submit", function(e) {
                ButtonStatus('.form_save', true);
                blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'payroll');
                $.ajax({
                    url: "{{ url('payrollProcessing') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save', false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_modal_payrollReports").modal("hide");
                            $(".select2").val('').trigger('change')
                            $('#payroll_report_datatable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Payroll has been Updated Successfully!'
                            })
                        }

                    }
                });
            });

            // Download Multiple
            $("#download_multiple_form").submit(function(e) {
                $("#download_multiple_modal").modal("hide");
                ButtonStatus('.form_save', true);
                blockUI();
                // blockUI();
                e.preventDefault();
                var departments = $('#departments').val();
                var month = $('#month').val();
                // var formData = $(this).serialize();
                // formData.append('type', 'multiple');
                $.ajax({
                    url: "{{ url('downloadMultiple') }}",
                    type: "get",
                    data: {
                        departments: departments,
                        month: month
                    },
                    success: function(response) {
                        ButtonStatus('.form_save', false);
                        $.unblockUI();
                        if (response.errors) {
                            // $.unblockUI();
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            // $.unblockUI();
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            // $.unblockUI();
                            $('#download_multiple_form')[0].reset();
                            window.open(this.url);
                            // Toast.fire({
                            //     icon: 'success',
                            //     title: 'Payroll has been Added Successfully!'
                            // })
                        }

                    }
                });
            });
            $("#payroll-employee-columns-form").on("submit", function(e) {
                e.preventDefault();
                blockUI();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{ url('payroll-employee-columns') }}" + '/' + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        $.unblockUI();
                        if (response.code == 200) {
                            $("#payroll-employee-columns-modal").modal("hide");
                            $(".select2").val('').trigger('change');
                            Toast.fire({
                                icon: 'success',
                                title: response.message
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            });
                        }
                    }
                });
            });
        });

        // Payslip Template DataTable
        function payslip_template_datatable() {
            $('#payslipTemplates_dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('payrollProcessing.index') }}",
                    data: {
                        type: 'payslipTemplates'
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },

                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        bSortable: false,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=payslipTemplates_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_payslip(' +
                                full.id + ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Payslip Template',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Payslip Template',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Payslip Template',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Payslip Template',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Payslip Template',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal_payslipTemplates'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#payslip-template div.head-label').html('<h6 class="mb-0">List of Payslip Templates</h6>');

            // Add Payslip Template
            $("#add_form_payslipTemplates").submit(function(e) {
                ButtonStatus('.form_save', true);
                blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'payslip');
                $.ajax({
                    url: "{{ route('payrollProcessing.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save', false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form_payslipTemplates')[0].reset();
                            $(".select2").val('').trigger('change')
                            $("#add_modal_payslipTemplates").modal("hide");
                            $('#payslipTemplates_dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'PaySlip has been Added Successfully!'
                            })
                        }

                    }
                });
            });
        }

        // Update Payslip
        $("#edit_form_payslipReports").on("submit", function(e) {
            ButtonStatus('.form_save', true);
            blockUI();
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('type', 'payslip');
            $.ajax({
                url: "{{ url('payrollProcessing') }}" + "/" + rowid,
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    ButtonStatus('.form_save', false);
                    $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $("#edit_modal_payslip").modal("hide");
                        $(".select2").val('').trigger('change')
                        $('#payslipTemplates_dataTable').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'PaySlip has been Updated Successfully!'
                        })
                    }

                }
            });
        });

        // Expense Payment Method DataTable
        function payroll_history() {
            $('#payroll_history').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('payrollProcessing.index') }}",
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'id',
                        name: 'payroll.id',
                    },
                    {
                        data: 'payroll',
                        name: 'payroll.name',
                    },
                    {
                        data: 'department',
                        name: 'companystructures.title',
                    },
                    {
                        data: 'date_start',
                        name: 'payroll.date_start',
                    },
                    {
                        data: 'date_end',
                        name: 'payroll.date_end',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=payment_method_edit(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Payroll History',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Payroll History',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Payroll History',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Payroll History',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Payroll History',
                            action: newexportaction,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                'd-inline-flex');
                        }, 50);
                    }
                }],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#payroll-history div.head-label').html('<h6 class="mb-0">Payroll History</h6>');
        }

        // Edit Payslip

        function payslipTemplates_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('payrollProcessing') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'payslip'
                },
                success: function(response) {
                    var columns = JSON.parse(response.add_columns);
                    var deduction = JSON.parse(response.deduct_columns);
                    $("#edit_payname").val(response.name);
                    // console.log(deduction);
                    $("#edit_basic_salary").val(response.basic_salary).select2();
                    $("#edit_gross_salary").val(response.gross_salary).select2();
                    $("#edit_net_salary").val(response.net_salary).select2();
                    $("#edit_allowance").val(columns).select2();
                    $("#edit_deduction").val(deduction).select2();
                    $("#edit_modal_payslip").modal("show");
                },
            });
        }

        // Delete Payslip
        function delete_payslip(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "payrollProcessing/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'payslipTemplates'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#payslipTemplates_dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'PaySlip has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Edit Payroll
        function edit_payroll(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('payrollProcessing') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'payroll'
                },
                success: function(response) {
                    var columns = JSON.parse(response.columns);
                    // console.log(columns);

                    $("#edit_name").val(response.name);
                    $("#edit_pay_frequency").val(response.pay_period).select2();
                    $("#edit_deduction_group").val(response.deduction_group).select2();
                    $("#edit_payslipTemplate").val(response.payslipTemplate).select2();
                    $("#edit_department").val(response.department).select2();
                    $("#edit_date_start").val(response.date_start);
                    $("#edit_date_end").val(response.date_end);
                    $("#edit_columns").val(columns).select2();
                    $("#edit_status").val(response.status).select2();
                    $("#edit_remarks").val(response.remarks);
                    $("#edit_modal_payrollReports").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "payrollProcessing/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'payroll'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#payroll_report_datatable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Payroll has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        // Copy Payroll
        function copy_payroll(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('payrollProcessing') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'payroll'
                },
                success: function(response) {
                    var columns = JSON.parse(response.columns);
                    $("#name").val(response.name);
                    $("#pay_frequency").val(response.pay_period).select2();
                    $("#deduction_group").val(response.deduction_group).select2();
                    $("#payslipTemplate").val(response.payslipTemplate).select2();
                    $("#department").val(response.department).select2();
                    $("#date_start").val(response.date_start);
                    $("#date_end").val(response.date_end);
                    $("#columns").val(columns).select2();
                    $("#status").val(response.status).select2();
                    $("#add_modal_payrollReports").modal("show");
                },
            });
        }
        // Ask For Payroll Processing Meta Data
        function askForPPMetaData(id) {
            rowid = id;
            if(company != 'JSML'){
                process_payroll(false);
            }
            else{
                $("#payroll-processing-meta-data-modal").modal("show");
            }
        }

        // Process Payroll
        function process_payroll(metaCheck = true) {
            if(metaCheck){
                var metainfo = $('#payroll_process_meta').val();
                if (metainfo == '' || metainfo == null) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Please Select Info!'
                    });
                    return false;
                }
                $("#payroll-processing-meta-data-modal").modal("hide");
            }else{
                var metainfo = '';
            }
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are You Sure! You Want To Process Payroll?.',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            Toast.fire({
                                icon: 'success',
                                title: 'Payroll Processing Started!'
                            });
                            blockUI();
                            $.ajax({
                                url: "{{ url('payrollProcessing') }}" + "/" + rowid,
                                type: "post",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    "metainfo": metainfo
                                },
                                success: function(response) {
                                    $.unblockUI();
                                    if (response.code == 200) {
                                        Toast.fire({
                                            icon: 'success',
                                            title: response.message
                                        });
                                        $('#payroll_process_meta').val('').trigger('change');
                                        $('#payroll_report_datatable').DataTable().ajax.reload();
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: response.message
                                        });
                                    }
                                },
                                error: function() {
                                    $.unblockUI();
                                    $('#payroll_process_meta').val('').trigger('change');
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Something went wrong Please try again later!'
                                    });
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $('#payroll_process_meta').val('').trigger('change');
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Check Payroll Accrual JV
        function checkPayrollAccrualJV(id, reprocess) {
            if (reprocess == 0) {
                var note = 'Are You Sure! Accrual Will Be Processed if Not otherwise Accrual Entry Will be Shown.';
            } else {
                var note = 'Are You Sure! Accrual Will Be Re-Processed and Entry Will Be Shown.';
            }
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: note,
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            blockUI();
                            rowid = id;
                            $.ajax({
                                url: "{{ url('check-payroll-accrual-jv') }}",
                                type: "post",
                                data: {
                                    payroll_id: id,
                                    reprocess: reprocess,
                                    "_token": "{{ csrf_token() }}",
                                },
                                success: function(response) {
                                    $.unblockUI();
                                    if (response.code == 200) {
                                        var tableBody = $('#check-accrual-table-body');
                                        var totalDebit = 0;
                                        var totalCredit = 0;
                                        tableBody.empty(); // Clear any existing rows

                                        // Loop through the data and create rows
                                        $.each(response.result, function(index, row) {
                                            var newRow = $('<tr>');
                                            newRow.append($('<td>').text(row.column_name));
                                            newRow.append($('<td>').text(row.gl_account));
                                            if (row.gl_account_type == 'debit') {
                                                newRow.append($(
                                                    '<td style = "text-align:center">'
                                                ).text(row.total_amount
                                                    .toLocaleString()));
                                                totalDebit += row.total_amount;
                                            } else {
                                                newRow.append($(
                                                        '<td style="text-align:center">')
                                                    .text('-'));
                                            }
                                            if (row.gl_account_type == 'credit') {
                                                let credit = row.total_amount < 0 ? -row
                                                    .total_amount : row.total_amount;
                                                newRow.append($(
                                                    '<td style = "text-align:center">'
                                                ).text(credit.toLocaleString()));
                                                totalCredit += credit;
                                            } else {
                                                newRow.append($(
                                                        '<td style="text-align:center">')
                                                    .text('-'));
                                            }
                                            newRow.append($('<td style="text-align:center">').text(row.total_employees
                                                .toLocaleString()));
                                            tableBody.append(newRow);
                                        });
                                        // Display the total values
                                        var totalRow = $('<tr>');
                                        totalRow.append(
                                            '<td colspan="2" style="font-weight:bold">Total</td>'
                                        );
                                        totalRow.append($(
                                            '<td style="text-align:center;font-weight:bold">'
                                        ).text(totalDebit.toLocaleString()));
                                        totalRow.append($(
                                            '<td style="text-align:center;font-weight:bold">'
                                        ).text(totalCredit.toLocaleString()));
                                        totalRow.append('<td></td>');
                                        tableBody.append(totalRow);
                                        $("#check_payroll_accrual_jv").modal({
                                            backdrop: 'static'
                                        }).modal("show");
                                    } else if (response.code == 404) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: response.message
                                        })
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: response.message
                                        });
                                    }
                                },
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.write(`<!DOCTYPE html>
                    <head>
                        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
                        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
                    </head>
                    <body>
                    ${WinPrint.document.write(divToPrint.outerHTML)}
                    </body>
                    </html>
                    `)
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
        // Payroll Accrual JV
        function payrollAccrualJV() {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are You Sure! You Want To Send It To ERP.',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $("#check_payroll_accrual_jv").modal("hide");
                            blockUI();
                            $.ajax({
                                url: "{{ url('payrollAccrualJV') }}",
                                type: "post",
                                data: {
                                    payroll_id: rowid,
                                    "_token": "{{ csrf_token() }}",
                                },
                                success: function(response) {
                                    $.unblockUI();
                                    if (response.code == 200) {
                                        $('#payroll_report_datatable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: response.message
                                        });
                                    } else if (response.code == 404) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: response.message
                                        })
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: response.message
                                        });
                                    }
                                },
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Delete Payroll Accrual JV
        function deletePayrollAccrualJV(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are You Sure! You Want To Delete JV',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            blockUI();
                            $.ajax({
                                url: "{{ url('delete-payroll-accrual-jv') }}" + '/' + id,
                                type: "DELETE",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                },
                                success: function(response) {
                                    $.unblockUI();
                                    if (response.code == 200) {
                                        $('#payroll_report_datatable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: response.message
                                        });
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: response.message
                                        });
                                    }
                                },
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        function payrollEmployeeColumns(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('payroll-employee-columns') }}" + '/' + rowid,
                type: "GET",
                success: function(response) {
                    if (response.code == 200) {
                        $('#employee_columns').val(response.columns).select2();
                        $("#payroll-employee-columns-modal").modal("show");
                    } else if (response.code == 500) {
                        Swal.fire({
                            icon: 'error',
                            title: response.message
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error On Fetching Columns!'
                        });
                    }
                },
            });
        }
    </script>
@endsection
