@extends('Admin.layouts.master')
@section('title', 'Accrual Payment Voucher')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0"></h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Accrual Payment Voucher
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <!--Search Form -->
                        <div class="card-body">
                            <form action="{{ url('accrual-payment-vouchers') }}" action="GET">
                                <div class="row g-1 mb-md-1">
                                    <div class="col-md-4">
                                        <label class="form-label">Vocher Date From:</label>
                                        <input type="text" name="fromDateFilter" id="fromDateFilter"
                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                            value="{{ request('fromDateFilter') }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Vocher Date To:</label>
                                        <input type="text" name="toDateFilter" id="toDateFilter"
                                            class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                            value="{{ request('toDateFilter') }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Voucher:</label>
                                        <input type="text" name="voucherFilter" value="{{ request('voucherFilter') }}"
                                            class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Status:</label>
                                        <select name="statusFilter" id="statusFilter" class="select2 form-select"
                                            data-placeholder="Select Status">
                                            <option value="">Select Status</option>
                                            <option value="paid"
                                                {{ request('statusFilter') == 'paid' ? 'selected' : '' }}>Paid</option>
                                            <option value="unpaid"
                                                {{ request('statusFilter') == 'unpaid' ? 'selected' : '' }}>Un-Paid</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Bank</label>
                                            <select name="bankFilter[]" class="form-select select2"
                                                data-placeholder="Select Bank" id="bank" multiple>
                                                <option value=""></option>
                                                @foreach ($banks as $bank)
                                                    <option value="{{ $bank->account_code }}"
                                                        {{ in_array($bank->account_code, request('bankFilter') ?? []) ? 'selected' : '' }}>
                                                        {{ $bank->account_code . ' - ' . $bank->bank_account_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-end">
                                        <a href="{{ url('accrual-payment-vouchers') }}" type="button"
                                            class="btn btn-danger">Reset</a>
                                        <button class="btn btn-primary">Filter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Voucher No.</th>
                                                <th>Voucher Date</th>
                                                <th>Accrual Month</th>
                                                <th>Bank</th>
                                                <th>Description</th>
                                                <th style="text-align:right">Amount</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($vouchers as $voucher)
                                                <tr>
                                                    <td>{{ $voucher->payment_voucher_no }}</td>
                                                    <td>{{ $voucher->voucher_date }}</td>
                                                    <td>{{ $voucher->entry_of_month }}</td>
                                                    <td>{{ $voucher->bank }}</td>
                                                    <td>{{ $voucher->payment_voucher_desc }}</td>
                                                    <td style="text-align:right;">
                                                        {{ number_format($voucher->amount) }}</td>
                                                    <td>
                                                        @if (!$voucher->payment_trans_no)
                                                            <button class="btn btn-info btn-sm"
                                                                onclick="sendToErp({{ $voucher->payment_voucher_no }})">
                                                                <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                                            </button>
                                                        @endif
                                                        <a class="btn btn-primary btn-sm" target="_blank"
                                                            href="{{ route('view-accrual-voucher', $voucher->payment_voucher_no) }}">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row p-1">
                                <p class="text-success">Showing {{ $from }} to {{ $to }} of
                                    {{ $total }} entries</p>
                                {{ $vouchers->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        function sendToErp(voucher_no) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to Send Voucher!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            blockUI();
                            $.ajax({
                                url: "{{ url('accrual-payment-voucher') }}",
                                type: "POST",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    voucher: voucher_no,
                                },
                                success: function(response) {
                                    $.unblockUI();
                                    if (response.code == 200) {
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Voucher has been Sent Successfully!'
                                        });
                                        setTimeout(() => {
                                            location.reload();
                                        }, 100);
                                    } else if (response.code == 404) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: response.message
                                        })
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: response.message
                                        });
                                    }
                                },
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
