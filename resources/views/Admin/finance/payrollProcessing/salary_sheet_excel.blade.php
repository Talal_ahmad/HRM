<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salary Sheet</title>
</head>
<body>
    <div>
        <table border=".5pt">
            <thead>
                <tr>
                    <th>Sr.No</th>
                    <th>Employee ID</th>
                    @if (env('COMPANY') != 'CLINIX' && env('COMPANY') != 'Ajmal Dawakhana')
                    <th>Employee Code</th>
                    @endif
                    <th>Name</th>
                    @if (env('COMPANY') == 'CLINIX')
                        <th>Active/In-active</th>
                    @endif
                    @if (env('COMPANY') == 'Ajmal Dawakhana')
                        <th>CNIC</th>
                    @endif
                    @if (env('COMPANY') != 'CLINIX')
                        <th>Department</th>
                    @endif
                    <th>Job Title</th>
                    <th>Employement Status</th>
                    @foreach ($payroll_columns as $item)
                        <th>{{$item->name}}</th>
                    @endforeach
                    @if (env('COMPANY') == 'CLINIX')
                        <th>Department</th>
                        <th>Type</th>
                    @else
                    <th>Signature</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach ($employees as $key => $employee)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$employee->employee_id}}</td>
                        @if (env('COMPANY') != 'CLINIX' && env('COMPANY') != 'Ajmal Dawakhana')
                        <td>{{$employee->employee_code}}</td>
                        @endif
                        <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                        @if (env('COMPANY') == 'CLINIX')
                            <td>{{$employee->emp_active_inactive}}</td>
                        @endif
                        @if (env('COMPANY') == 'Ajmal Dawakhana')
                            <td>{{$employee->nic_num}}</td>
                        @endif
                        @if (env('COMPANY') != 'CLINIX')
                        <td>{{$employee->department_name}}</td>
                        @endif
                        <td>{{$employee->job_title}}</td>
                        <td>{{$employee->employment_status}}</td>
                        @foreach ($payroll_data as $data)
                            <td>{{!empty($column_amount[$employee->id][$data->column_id]) ? $column_amount[$employee->id][$data->column_id] : '-'}}</td>
                        @endforeach
                        @if (env('COMPANY') == 'CLINIX')
                            <td>{{$employee->department_name}}</td>
                            <td>{{$employee->type}}</td>
                        @else
                        <td></td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    @if (env('COMPANY') == 'CLINIX')
                    <td colspan="6" style="text-align: right; font-weight: bold">Net Totals:</td>
                    {{-- @elseif(env('COMPANY') == 'Ajmal Dawakhana')
                    <td colspan="6" style="text-align: right; font-weight: bold">Net Totals:</td> --}}
                    @else
                    <td colspan="7" style="text-align: right; font-weight: bold">Net Totals:</td>
                    @endif
                    @foreach ($payroll_columns as $col)
                        @php
                            $total = '';
                            if(!empty($net_total[$col->id]))
                            {
                                $total = $net_total[$col->id];
                            }
                        @endphp
                        <td>{{$total}}</td>
                    @endforeach
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>
</body>
</html>