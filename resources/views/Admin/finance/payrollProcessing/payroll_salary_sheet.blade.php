<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salary Sheet</title>
</head>

<body>
    <div>
        <div>
            @php
                $font = 'sheet1';
                if ($salary_font) {
                    $font = 'sheet2';
                }
                $colspan = count($employee_columns) + 1;
                $signature = false;
            @endphp
            <table style="border-collapse: collapse">
                <thead>
                    <tr>
                        <th colspan="{{ $colspan + 1 + count($payroll_columns) }}"
                            style="text-align: center;font-size:20px">
                            <strong>
                                {{ $payroll->name }} Sheet
                            </strong>
                        </th>
                    </tr>
                    <tr>
                        <th><strong>Sr.No</strong></th>
                        @foreach ($employee_columns as $employee_column)
                            @if($employee_column->display_name != 'Signature')
                                <th>{{ $employee_column->display_name }}</th>
                            @else
                                @php
                                    $signature = true;
                                @endphp
                            @endif
                        @endforeach
                        @foreach ($payroll_columns as $item)
                            @if (env('COMPANY') == 'Ajmal Dawakhana')
                                <th style="font-size: 40px;"><strong>{{ $item->name }}</strong></th>
                            @else
                                <th><strong>{{ $item->name }}</strong></th>
                            @endif
                        @endforeach
                        @if (empty($salary_font))
                            <th width="5%"><strong>Signature</strong></th>
                        @endif
                        @if($signature)
                            <th><strong>Signature</strong></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employees as $key => $employee)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            @foreach ($employee_columns as $employee_column)
                                @if($employee_column->display_name != 'Signature')
                                    @if($employee_column->name == 'full_name')
                                        <td>{{$employee->employee_id }} - {{$employee->first_name}} {{$employee->last_name}}</td>
                                    @else
                                        <td>{{ $employee->{$employee_column->name} }}</td>
                                    @endif
                                @endif
                            @endforeach
                            @foreach ($payroll_columns as $column)
                                <td style="text-align: right">
                                    {{ !empty($column_amount[$employee->id][$column->id]) ? $column_amount[$employee->id][$column->id] : '-' }}
                                </td>
                            @endforeach
                            @if (empty($salary_font))
                                <td></td>
                            @endif
                            @if($signature)
                                <td></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="{{ $colspan }}" style="text-align: right; font-weight: bold">Net Totals:</td>
                        @foreach ($payroll_columns as $col)
                            @php
                                $total = '';
                                if (!empty($net_total[$col->id])) {
                                    $total = number_format($net_total[$col->id]);
                                }
                            @endphp
                            <td style="text-align: right;font-weight: bold">{{ $total }}</td>
                        @endforeach
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</body>

</html>
