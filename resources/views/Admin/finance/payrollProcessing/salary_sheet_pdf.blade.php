<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salary Sheet</title>

    <head>
        <style>
            #salary_sheet {
                border-collapse: collapse;
                width: 100%;
                padding-left: 150px;
                padding-right: 150px;
                font-family: 'Segoe UI', sans-serif
            }

            #sheet1 td,
            #sheet1 th {
                border: 1px solid #ddd;
                padding: 15px;
                font-size: 50px;
            }

            #sheet2 td,
            #sheet2 th {
                border: 1px solid #ddd;
                padding: 15px;
                font-size: 100px;
            }

            #salary_sheet td {
                text-align: center;
                /* padding: 20px; */
            }

            #salary_sheet tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #salary_sheet tr:hover {
                background-color: #ddd;
            }

            #salary_sheet th {
                /* padding-top: 12px; */
                /* padding-bottom: 12px; */
                text-align: left;
                background-color: #2e475c;
                color: white;
            }
        </style>
    </head>
</head>

<body>
    <div>
        <div>
            @php
                $font = 'sheet1';
                if ($salary_font) {
                    $font = 'sheet2';
                }
                $colspan = count($employee_columns) + 1;
                $signature = false;
            @endphp
            <table id="salary_sheet {{ $font }}">
                <thead>
                    <tr>
                        <td colspan="{{$colspan + 1 + count($payroll_columns) }}" style="text-align: center">
                            @if ($salary_font)
                                <p style="font-size: 90px">{{ date('Y-m-d h:i a') }}</p>
                                <h1 style="text-align: center;font-size: 100px">{{ $payroll->name }} Sheet</h1>
                            @else
                                <p style="font-size: 50px">{{ date('Y-m-d h:i a') }}</p>
                                <h1 style="text-align: center;font-size: 55px">{{ $payroll->name }} Sheet</h1>
                            @endif
                        </td>
                    </tr>
                    <th>Sr.No</th>
                    @foreach ($employee_columns as $employee_column)
                        @if($employee_column->display_name != 'Signature')
                            <th>{{ $employee_column->display_name }}</th>
                        @else
                            @php
                                $signature = true;
                            @endphp
                        @endif
                    @endforeach
                    @foreach ($payroll_columns as $item)
                        @if (env('COMPANY') == 'Ajmal Dawakhana')
                            <th style="font-size: 40px;">{{ $item->name }}</th>
                        @else
                            <th>{{ $item->name }}</th>
                        @endif
                    @endforeach
                    @if (empty($salary_font))
                        <th width="5%">Signature</th>
                    @endif
                    @if($signature)
                        <th><strong>Signature</strong></th>
                    @endif
                </thead>
                <tbody>
                    @foreach ($employees as $key => $employee)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            @foreach ($employee_columns as $employee_column)
                                @if($employee_column->display_name != 'Signature')
                                    @if($employee_column->name == 'full_name')
                                        <td>{{$employee->employee_id }} - {{$employee->first_name}} {{$employee->last_name}}</td>
                                    @else
                                        <td>{{ $employee->{$employee_column->name} }}</td>
                                    @endif
                                @endif
                            @endforeach
                            @foreach ($payroll_columns as $column)
                                <td style="text-align: right">
                                    {{ !empty($column_amount[$employee->id][$column->id]) ? $column_amount[$employee->id][$column->id] : '-' }}
                                </td>
                            @endforeach
                            @if (empty($salary_font))
                                <td></td>
                            @endif
                            @if($signature)
                                <td></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="{{$colspan}}" style="text-align: right; font-weight: bold">Net Totals:</td>
                        @foreach ($payroll_columns as $col)
                            @php
                                $total = '';
                                if (!empty($net_total[$col->id])) {
                                    $total = number_format($net_total[$col->id]);
                                }
                            @endphp
                            <td style="text-align: right;">{{ $total }}</td>
                        @endforeach
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</body>

</html>
