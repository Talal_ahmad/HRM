@extends('Admin.layouts.master')
@section('title', 'View Payroll')
@section('style')
    <style>
        /* .pagination{
            justify-content: flex-end;
            padding-right: 10px;
        } */
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">View Payroll</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Payroll Processing
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{empty($type) ? 'active' : ''}}" id="employees-payroll-tab" href="{{route('payrollProcessing.show', $id)}}">Employees Payroll</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{!empty($type) ? 'active' : ''}}" id="employers-column-tab" href="{{route('payrollProcessing.show', $id)}}?type=employers_column">Employer's Column</a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- Start View Payroll --}}
                    <div class="tab-pane {{empty($type) ? 'active' : ''}}" id="employees-payroll" aria-labelledby="employees-payroll-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="employees_payroll_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    @if (env('COMPANY') != 'UNICORN')
                                                        <th>Employee ID</th>
                                                    @endif
                                                    @if (env('COMPANY') != 'Ajmal Dawakhana')
                                                        <th>Employee Code</th>
                                                    @endif
                                                    <th>Name</th>
                                                    @if (env('COMPANY') == 'Ajmal Dawakhana')
                                                        <th>CNIC</th>
                                                    @endif
                                                    @if (env('COMPANY') != 'UNICORN')
                                                        <th>Department</th>
                                                    @endif
                                                    <th>Job Title</th>
                                                    @if (env('COMPANY') != 'UNICORN')
                                                        <th>Employement Status</th>
                                                    @endif
                                                    @if (env('COMPANY') == 'JSML')
                                                        <th>Pay Grade</th>
                                                    @endif
                                                    @foreach ($payroll_columns as $item)
                                                        <th>{{$item->name}}</th>
                                                    @endforeach
                                                    @can('Payroll Process Button')
                                                    <th class="not_include">Actions</th>
                                                    @endcan
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($employees as $key => $employee)
                                                    <tr>
                                                        <td></td>
                                                        <td>{{$key+1}}</td>
                                                        @if (env('COMPANY') != 'UNICORN')
                                                            <td>{{$employee->employee_id}}</td>
                                                        @endif
                                                        @if (env('COMPANY') != 'Ajmal Dawakhana')
                                                        <td>{{$employee->employee_code}}</td>
                                                        @endif
                                                        <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                                                        @if (env('COMPANY') == 'Ajmal Dawakhana')
                                                            <td>{{$employee->nic_num}}</td>
                                                        @endif
                                                        @if (env('COMPANY') != 'UNICORN')
                                                            <td>{{$employee->department_name}}</td>
                                                        @endif
                                                        <td>{{$employee->job_title}}</td>
                                                        @if (env('COMPANY') != 'UNICORN')
                                                            <td>{{$employee->employement_status}}</td>
                                                        @endif
                                                        @if (env('COMPANY') == 'JSML')
                                                            <td>{{$employee->paygrade_name}} ({{$employee->min_salary}}-{{$employee->max_salary}})</td>
                                                        @endif
                                                        @foreach ($payroll_columns as $column)
                                                            <td>{{$column_amount[$employee->id][$column->id]}}</td>
                                                        @endforeach
                                                        @can('Payroll Process Button')
                                                        <td>
                                                            <a href="javascript:;" class="btn btn-sm btn-primary" onclick="process_payroll({{$id}},{{$employee->id}})">Process</a>
                                                        </td>
                                                        @endcan
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        {{-- <div class="d-md-flex justify-content-between">
                                            <p style="padding-top: 10px;padding-left: 10px;">Showing {{$employees->firstItem()}} to {{$key+$employees->firstItem()}} of {{$employees->total()}} entries</p>
                                            <div style="padding-right: 10px;overflow:scroll">{{ $employees->onEachSide(2)->links() }}</div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- End View Payroll -->

                    <!-- Start Payslip Template -->
                    <div class="tab-pane {{!empty($type) ? 'active' : ''}}" id="employers-column" aria-labelledby="employers-column-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="employers_column_dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Employee ID</th>
                                                    @if (env('COMPANY') != 'RoofLine')
                                                    <th>Employee Code</th>
                                                    @endif
                                                    <th>Name</th>
                                                    <th>Department</th>
                                                    @if (env('COMPANY') != 'RoofLine')
                                                    <th>Job Title</th>
                                                    <th>Employement Status</th>
                                                    @endif
                                                    @foreach ($payroll_columns as $item)
                                                        <th>{{$item->name}}</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($employees as $key => $employee)
                                                    <tr>
                                                        <td></td>
                                                        <td>{{$key+1}}</td>
                                                        <td>{{$employee->employee_id}}</td>
                                                        @if (env('COMPANY') != 'RoofLine')
                                                        <td>{{$employee->employee_code}}</td>
                                                        @endif
                                                        <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                                                        <td>{{$employee->department_name}}</td>
                                                        @if (env('COMPANY') != 'RoofLine')
                                                        <td>{{$employee->job_title}}</td>
                                                        <td>{{$employee->employement_status}}</td>
                                                        @endif
                                                        @foreach ($payroll_columns as $column)
                                                            <td>{{$column_amount[$employee->id][$column->id]}}</td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- End Payslip Template -->
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            $('#employees_payroll_dataTable').DataTable({
                ordering: true,
                // "paging": false,
                // "info": false,
                "columnDefs": [
					{
						// For Responsive
						className: 'control',
						orderable: false,
						targets: 0
					},
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employees Payroll',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employees Payroll',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employees Payroll',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employees Payroll',
                            orientation : 'landscape',
                            @if (env('COMPANY') != 'UNICORN')
                            pageSize : 'A2',
                            customize: function (doc) {
                                //Remove the title created by datatTables
                                doc.content.splice(0, 1);
                                //Create a date string that we use in the footer. Format is dd-mm-yyyy
                                var now = new Date();
                                var jsDate = now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();
                                doc.pageMargins = [20, 60, 20, 30];
                                // Set the font size fot the entire document
                                doc.defaultStyle.fontSize = 7;
                                // Set the fontsize for the table header
                                doc.styles.tableHeader.fontSize = 7;
                                // Create a header
                                doc["header"] = function () {
                                    return {
                                        columns: [
                                            {
                                                alignment: "center",
                                                italics: true,
                                                text: "Employees Payroll",
                                                fontSize: 18,
                                                margin: [10, 0]
                                            },
                                        ],
                                        margin: 20
                                    };
                                };
                                doc["footer"] = function (page, pages) {
                                    return {
                                        columns: [
                                            {
                                                alignment: "left",
                                                text: ["Created on: ", { text: jsDate.toString() }]
                                            },
                                            {
                                                alignment: "right",
                                                text: [
                                                    "page ",
                                                    { text: page.toString() },
                                                    " of ",
                                                    { text: pages.toString() }
                                                ]
                                            }
                                        ],
                                        margin: 20
                                    };
                                };
                                var objLayout = {};
                                objLayout["hLineWidth"] = function (i) {
                                    return 0.5;
                                };
                                objLayout["vLineWidth"] = function (i) {
                                    return 0.5;
                                };
                                objLayout["hLineColor"] = function (i) {
                                    return "#aaa";
                                };
                                objLayout["vLineColor"] = function (i) {
                                    return "#aaa";
                                };
                                objLayout["paddingLeft"] = function (i) {
                                    return 4;
                                };
                                objLayout["paddingRight"] = function (i) {
                                    return 4;
                                };
                                doc.content[0].layout = objLayout;
                            },
                            @else
                            pageSize : 'letter',
                            customize: function (doc) {
                                // Remove the title created by DataTables
                                doc.content.splice(0, 1);

                                // Create a date string that we use in the footer. Format is dd-mm-yyyy
                                var now = new Date();
                                var jsDate = now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();

                                // Adjust page margins
                                doc.pageMargins = [5, 5, 5, 5]; // [left, top, right, bottom]

                                // Set the font size for the entire document
                                doc.defaultStyle.fontSize = 10; // Adjust the font size as needed

                                // Set the font size for the table header
                                doc.styles.tableHeader.fontSize = 10; // Adjust the header font size as needed

                                // Create a header
                                doc["header"] = function () {
                                    return {
                                        columns: [
                                            {
                                                alignment: "center",
                                                italics: true,
                                                text: "Employees Payroll",
                                                fontSize: 18,
                                                margin: [0, 10] // Adjust top margin as needed
                                            },
                                        ],
                                        margin: 0
                                    };
                                };

                                // Create a footer
                                doc["footer"] = function (page, pages) {
                                    return {
                                        columns: [
                                            {
                                                alignment: "left",
                                                text: ["Created on: ", { text: jsDate.toString() }],
                                                margin: [5, 0] // Adjust left margin as needed
                                            },
                                            {
                                                alignment: "right",
                                                text: [
                                                    "Page ",
                                                    { text: page.toString() },
                                                    " of ",
                                                    { text: pages.toString() }
                                                ],
                                                margin: [0, 0, 5, 0] // Adjust right margin as needed
                                            }
                                        ],
                                        margin: [1, 0] // Adjust top margin as needed
                                    };
                                };

                                // Adjust the table layout
                                var objLayout = {};
                                objLayout["hLineWidth"] = function (i) {
                                    return 0.5;
                                };
                                objLayout["vLineWidth"] = function (i) {
                                    return 0.5;
                                };
                                objLayout["hLineColor"] = function (i) {
                                    return "#aaa";
                                };
                                objLayout["vLineColor"] = function (i) {
                                    return "#aaa";
                                };
                                objLayout["paddingLeft"] = function (i) {
                                    return 3;
                                };
                                objLayout["paddingRight"] = function (i) {
                                    return 3;
                                };
                                doc.content[0].layout = objLayout;
                            },
                            @endif

                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employees Payroll',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('#employees-payroll div.head-label').html('<h6 class="mb-0">List of Employees Payroll</h6>');

            $('#employers_column_dataTable').DataTable({
                ordering: true,
                "columnDefs": [
					{
						// For Responsive
						className: 'control',
						orderable: false,
						targets: 0
					},
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('#employers-column div.head-label').html('<h6 class="mb-0">List of Employers Columns</h6>');
        });

        // Process Payroll
        function process_payroll(id,employee_id) {
            blockUI();
            $.ajax({
                url: "{{ url('payrollProcessing') }}" + "/" + id,
                type: "post",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "employee_id": employee_id
                },
                success: function(response) {
                    $.unblockUI();
                    Toast.fire({
                        icon: 'success',
                        title: 'Payroll has been Completed Successfully!'
                    });
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                },
                error: function(){
                    $.unblockUI();
                    Toast.fire({
                        icon: 'error',
                        title: 'Something went wrong Please try again later!'
                    })
                }
            });
        }
    </script>
@endsection
