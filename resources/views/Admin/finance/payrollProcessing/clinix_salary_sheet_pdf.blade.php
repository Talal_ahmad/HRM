<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salary Sheet</title>
    <head>
        <style>
            #salary_sheet {
                border-collapse: collapse;
                width: 100%;
                font-family:'Segoe UI',sans-serif
            }

            #salary_sheet td, #salary_sheet th {
                border: 1px solid #ddd;
                padding: 5px;
                font-size: 55px;
                width: 5px;
            }

            #salary_sheet td{
                text-align: center;
                padding: 30px;
            }

            #salary_sheet tr:nth-child(even){background-color: #f2f2f2;}

            #salary_sheet tr:hover {background-color: #ddd;}

            #salary_sheet th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #2e475c;
                color: white;
            }
        </style>
    </head>
</head>
<body>
    <div>
        <h1 style="text-align: center;font-size: 55px">{{$payroll->name}} Sheet</h1>
        <p style="font-size: 50px">{{date('Y-m-d h:i a')}}</p>
        <div>
            <table id="salary_sheet">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Name</th>
                        @foreach ($payroll_columns as $item)
                            <th>{{$item->name}}</th>
                        @endforeach
                        <th>Department</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employees as $key => $employee)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$employee->first_name.' '.$employee->last_name.' '. $employee->employee_id.' '.$employee->job_title.' '.$employee->employee_code}}
                            {{ $employee->emp_active_inactive }}
                            </td>
                            @foreach ($payroll_columns as $column)
                                <td>{{!empty($column_amount[$employee->id][$column->id]) ? $column_amount[$employee->id][$column->id] : '-'}}</td>
                            @endforeach
                            <td>{{$employee->department_name}} ({{$employee->type}})</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="text-align: right; font-weight: bold">Net Totals:</td>
                        @foreach ($payroll_columns as $col)
                            @php
                                $total = '';
                                if(!empty($net_total[$col->id]))
                                {
                                    $total = number_format($net_total[$col->id]);
                                }
                            @endphp
                            <td>{{$total}}</td>
                        @endforeach
                        {{-- <td></td> --}}
                    </tr>
                    @if (!empty($prev_payroll))
                    <tr>
                        <td colspan="2" style="text-align: right; font-weight: bold">Previous Month Totals:</td>
                        @foreach ($payroll_columns as $col)
                            @php
                                $prev_total = '';
                                if(!empty($prev_net_total[$col->id]))
                                {
                                    $prev_total = number_format($prev_net_total[$col->id]);
                                }
                            @endphp
                            <td>{{$prev_total}}</td>
                        @endforeach
                    </tr>
                    <tr style="border: none">
                        <td style="font-weight: bold;border: none;white-space: nowrap; font-size:50px;">P. MON. <br>Tot. Emp.:</td>
                        <td style="border: none">{{count($prevTotalEmployees)}}</td>
                    </tr>
                    @endif
                </tfoot>
            </table>
            <p style="font-size:55px;">Remarks: {{$payroll->remarks}}</p>
        </div>
    </div>
</body>
</html>