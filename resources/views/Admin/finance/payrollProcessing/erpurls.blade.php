@extends('Admin.layouts.master')
@section('title', 'Payroll Erp Address')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0"></h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Payroll Erp Address</a>
                                </li>
                                <li class="breadcrumb-item active">Payroll Erp Address
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Addresses</h4>
                    <div class="alert alert-danger">
                        <strong>Note!</strong> Make Sure Addresses Are Valid Urls i.e,
                        <strong><small>"http://192.168.1.2/test/api/"</small></strong>
                    </div>
                </div>
                <div class="card-body" id="repeater">
                    <form action="{{ url('payrolls-erpurls') }}" class="form" method="POST">
                        @csrf
                        <div data-repeater-list="addresses">
                            @forelse ($addresses as $key => $addresses)
                                <div data-repeater-item>
                                    <div class="row">
                                        <div class="col-md-5 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">Calculation Group</label>
                                                <select name="deduction_group" class="form-select select2"
                                                    data-placeholder="Select Calculation Group" multiple required>
                                                    <option value=""></option>
                                                    @foreach ($calculation_groups as $calculation_group)
                                                        <option value="{{ $calculation_group->id }}"
                                                            {{ in_array($calculation_group->id, $addresses) ? 'selected' : '' }}>
                                                            {{ $calculation_group->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">Erp Address</label>
                                                <input type="text" class="form-control" name="address"
                                                    placeholder="Address" value="{{ $key }}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="my-2">
                                                <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete
                                                    type="button">
                                                    <i data-feather="x" class="me-25"></i>
                                                    <span>Delete</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            @empty
                                <div data-repeater-item>
                                    <div class="row">
                                        <div class="col-md-5 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">Calculation Group</label>
                                                <select name="deduction_group" class="form-select select2"
                                                    data-placeholder="Select Calculation Group" multiple required>
                                                    <option value=""></option>
                                                    @foreach ($calculation_groups as $calculation_group)
                                                        <option value="{{ $calculation_group->id }}">
                                                            {{ $calculation_group->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <div class="mb-1">
                                                <label class="form-label">Erp Address</label>
                                                <input type="text" class="form-control" name="address"
                                                    placeholder="Address" required>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="my-2">
                                                <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete
                                                    type="button">
                                                    <i data-feather="x" class="me-25"></i>
                                                    <span>Delete</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            @endforelse
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                    <i data-feather="plus" class="me-25"></i>
                                    <span>Add New</span>
                                </button>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary ms-1">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    @if (Session::has('message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('message') !!}"
            })
        </script>
    @endif
    @if ($errors->any())
        <script>
            Toast.fire({
                icon: "error",
                title: "Make Sure Payload Is Correct!"
            })
        </script>
    @endif
    <script>
        $(document).ready(function() {
            var repeater = $('#repeater');
            $(repeater).repeater({
                show: function() {
                    $(this).slideDown();
                    repeater.find('select').next('.select2-container').remove();
                    repeater.find('select').select2();
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                },
            });
        });
    </script>
@endsection
