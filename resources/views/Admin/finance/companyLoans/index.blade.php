@extends('Admin.layouts.master')
@section('title', 'Company Loans')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Company Loans</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Company Loans
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="loan-type-tab" data-bs-toggle="tab" href="#loan-type"
                            aria-controls="loan-type" role="tab" aria-selected="true">Loan Types</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employee-loans-tab" data-bs-toggle="tab" href="#employee-loans" aria-controls="employee-loans" role="tab" aria-selected="false" onclick="employee_loans()">Employee Loans</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="loan-type" aria-labelledby="loan-type-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="pb-2">
                                            <table class="table" id="loan_type">
                                                <thead>
                                                    <tr>
                                                        <th class="not_include"></th>
                                                        <th>Sr.No</th>
                                                        <th>Name</th>
                                                        <th class="not_include">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Employee Loans --}}
                    <div class="tab-pane" id="employee-loans" aria-labelledby="employee-loans-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card mb-1">
                                        <!--Search Form -->
                                        <div class="card-body">
                                            <form id="search_form">
                                                <div class="row g-1 mb-md-1">
                                                    <div class="col-md-5">
                                                        <label class="form-label" for="employeeFilter">Employee</label>
                                                        <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="Select Employee">
                                                            <option value="">Select Employee</option>
                                                            @foreach (employees() as $employee)
                                                                <option value="{{$employee->id}}">{{$employee->first_name}} {{$employee->last_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <label class="form-label">Loan Type:</label>
                                                        <select name="loanTypeFilter" id="loanTypeFilter" class="select2 form-select" data-placeholder="Select Loan Type">
                                                            <option value="">Select Loan Type</option>
                                                            @foreach ($loan_type as $item)
                                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div style="margin-top:35px" class="col-md-2 text-md-end">
                                                        <button class="btn btn-primary">Filter</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card pb-2">
                                        <table class="table" id="employee_loans">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Employee</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Loan Type</th>
                                                    <th>Loan Start Date</th>
                                                    <th>Loan Period(Months)</th>
                                                    <th>Currency</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <!--Loan Type Add Modal -->
                <div class="modal fade text-start" id="loan_type_add_modal" tabindex="-1"
                aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Add Loan Type</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form class="form" id="loan_type_add_form">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="name">Name</label>
                                                <input type="text" id="name" name="name" class="form-control" placeholder="Name" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                    <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--Loan Type Add Modal -->

                <!--Loan Type Edit Modal -->
                <div class="modal fade text-start" id="loan_type_edit_modal" tabindex="-1"
                    aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Edit Loan Type</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form class="form" id="loan_type_edit_form">
                                @csrf
                                @method('PUT')
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_name">Name</label>
                                                <input type="text" name="name" id="edit_name" class="form-control" placeholder="Name" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                    <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--End Loan Type Edit Modal -->

                <!--Employee Loans Add Modal -->
                <div class="modal fade text-start" id="employee_loans_add_modal" tabindex="-1"
                    aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Add Employee Loans</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form class="form" id="employee_loans_add_form">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="employee">Employee</label>
                                                <select name="employee" id="employee" class="select2 form-select" data-placeholder="Select Employee" required>
                                                    <option value=""></option>
                                                    @foreach (employees() as $employee)
                                                        <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="loan">Loan Type</label>
                                                <select name="loan" id="loan" class="select2 form-select" data-placeholder="Select Loan Type" required>
                                                    <option value=""></option>
                                                    @foreach ($loan_type as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="start_date">Loan Start Date</label>
                                                <input type="text" name="start_date" id="start_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="last_installment_date">Last Installment Date</label>
                                                <input type="text" name="last_installment_date" id="last_installment_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="period_months">Loan Period(Months)</label>
                                                <input type="text" name="period_months" id="period_months" class="form-control" placeholder="Loan Period" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="currency">Currency</label>
                                                <select name="currency" id="currency" class="select2 form-select" data-placeholder="Select Currency" required>
                                                    <option value=""></option>
                                                    @foreach ($currencytypes as $type)
                                                        <option value="{{ $type->id }}">{{ $type->code }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="amount">Loan Amount</label>
                                                <input type="text" name="amount" id="amount" class="form-control" placeholder="Loan Amount" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="monthly_installment">Monthly Installment</label>
                                                <input type="text" id="monthly_installment" class="form-control" name="monthly_installment" placeholder="Monthly Installment" required />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="status">Status</label>
                                                <select name="status" id="status" class="select2 form-select" data-placeholder="Select Status" required>
                                                    <option value="Approved">Approved</option>
                                                    <option value="Paid">Paid</option>
                                                    <option value="Suspended">Suspended</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="details">Details</label>
                                                <textarea name="details" id="details" cols="10" rows="2" class="form-control" placeholder="Type here..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--End Employee Loan Add Modal -->

                <!--Employee Loan Edit Modal -->
                <div class="modal fade text-start" id="employee_loans_edit_modal" tabindex="-1"
                    aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Edit Employee Loans</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form class="form" id="employee_loans_edit_form">
                                @csrf
                                @method('PUT')
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_employee">Employee</label>
                                                <select name="employee" id="edit_employee" class="select2 form-select" data-placeholder="Select Employee" required>
                                                    <option value=""></option>
                                                    @foreach (employees() as $employee)
                                                        <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->last_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_loan">Loan Type</label>
                                                <select name="loan" id="edit_loan" class="select2 form-select"
                                                    data-placeholder="Select Loan Type" required>
                                                    <option value=""></option>
                                                    @foreach ($loan_type as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_start_date">Loan Start
                                                    Date</label>
                                                <input type="text" name="start_date" id="edit_start_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_last_installment_date">Last Installment Date</label>
                                                <input type="text" name="last_installment_date" id="edit_last_installment_date" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_period_months">Loan Period(Months)</label>
                                                <input type="text" id="edit_period_months" class="form-control" name="period_months" placeholder="Loan Period" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_currency">Currency</label>
                                                <select name="currency" id="edit_currency" class="select2 form-select"
                                                    data-placeholder="Select Currency" required>
                                                    <option value=""></option>
                                                    @foreach ($currencytypes as $type)
                                                        <option value="{{ $type->id }}">{{ $type->code }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_amount">Loan Amount</label>
                                                <input type="text" name="amount" id="edit_amount" class="form-control" placeholder="Loan Amount" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_monthly_installment">Monthly
                                                    Installment</label>
                                                <input type="text" name="monthly_installment" id="edit_monthly_installment" class="form-control" placeholder="Monthly Installment" required />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_status">Status</label>
                                                <select name="status" id="edit_status" class="select2 form-select"
                                                    data-placeholder="Select Status" required>
                                                    <option value="Approved">Approved</option>
                                                    <option value="Paid">Paid</option>
                                                    <option value="Suspended">Suspended</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="edit_details">Details</label>
                                                <textarea name="details" id="edit_details" cols="10" rows="2" class="form-control" placeholder="Type here..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--End Employee Loans Edit Modal -->
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var rowid;
        var dataTable;
        $(document).ready(function() {
            $('#loan_type').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('companyLoans.index') }}",
                    data: {
                        type: "loan_type"
                    },
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'DT_RowIndex',
                        name : 'DT_RowIndex',
                        searchable: false,
                    },
                    {
                        data: 'name',
                        name: 'loan_types.name',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit_loan_type(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_loan_type(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Loan Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Loan Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Loan Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Loan Types',
                                orientation: 'landscape',
                                pageSize: 'LEGAL',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Loan Types',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#loan_type_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#loan-type div.head-label').html('<h6 class="mb-0">List of Loan Types</h6>');

            // Store Loan Type
            $("#loan_type_add_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'loan_type');
                $.ajax({
                    url: "{{ route('companyLoans.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#loan_type_add_modal").modal("hide");
                            document.getElementById("loan_type_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#loan_type').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Loan Type has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Loan Type Update
            $("#loan_type_edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type', 'loan_type');
                $.ajax({
                    url: "{{ url('companyLoans') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#loan_type_edit_modal").modal("hide");
                            document.getElementById("loan_type_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#loan_type').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Loan Type has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            // Store Employee Loans
            $("#employee_loans_add_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{ route('companyLoans.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#employee_loans_add_modal").modal("hide");
                            document.getElementById("employee_loans_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#employee_loans').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Loan has been Added Successfully!'
                            })
                        }
                    }
                });
            });

            // Employee Loans Update
            $("#employee_loans_edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{ url('companyLoans') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#employee_loans_edit_modal").modal("hide");
                            document.getElementById("employee_loans_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#employee_loans').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Loan has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
        });

        // Employee Loans DataTable
        function employee_loans() {
            dataTable = $('#employee_loans').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('companyLoans.index') }}",
                    data: function (filter) {
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.loanTypeFilter = $('#loanTypeFilter').val();
                        type = "employee_loans";
                    }
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'DT_RowIndex',
                        name : 'DT_RowIndex',
                        searchable: false,
                    },
                    {
                        render: function (data, type, full, meta) {
                            return full['employee_id']+ ' - '+full['employee_code']+ ' - '+full['first_name'] + ' ' + full['last_name'];
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'loan',
                        name: 'loan_types.name',
                    },
                    {
                        data: 'start_date',
                        name: 'employeecompanyloans.start_date',
                    },
                    {
                        data: 'period_months',
                        name: 'employeecompanyloans.period_months',
                    },
                    {
                        data: 'currency',
                        name: 'currencytypes.code',
                    },
                    {
                        data: 'amount',
                        name: 'employeecompanyloans.amount',
                    },
                    {
                        data: 'status',
                        name: 'employeecompanyloans.status',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit_employee_loans(' +
                                full.id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_employee_loans(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [2, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                title: 'Employee Loans',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                title: 'Employee Loans',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                title: 'Employee Loans',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                title: 'Employee Loans',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                title: 'Employee Loans',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass(
                                    'd-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#employee_loans_add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('#employee-loans div.head-label').html('<h6 class="mb-0">List of Employee Loans</h6>');
        }

        // loan_type Edit
        function edit_loan_type(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('companyLoans') }}" + "/" + id + "/edit",
                type: "get",
                data: {
                    type: 'loan_type'
                },
                success: function(response) {
                    $("#edit_name").val(response.name);
                    $("#loan_type_edit_modal").modal("show");
                },
            });
        }

        // Delete loan_type
        function delete_loan_type(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "companyLoans/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    type: 'loan_type'
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else if(response.code == 300)
                                    {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } 
                                    else {
                                        $('#loan_type').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Loan Type has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Employee Loans Edit
        function edit_employee_loans(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('companyLoans') }}" + "/" + id + "/edit",
                type: "get",
                success: function(response) {
                    $("#edit_employee").val(response.employee).select2();
                    $("#edit_loan").val(response.loan).select2();
                    $("#edit_start_date").val(response.start_date);
                    $('#edit_last_installment_date').val(response.last_installment_date);
                    $('#edit_period_months').val(response.period_months);
                    $('#edit_currency').val(response.currency).select2();
                    $('#edit_amount').val(response.amount);
                    $('#edit_monthly_installment').val(response.monthly_installment);
                    $('#edit_status').val(response.status);
                    $('#edit_details').val(response.details);
                    $("#employee_loans_edit_modal").modal("show");
                },
            });
        }

        // Delete employee_loans
        function delete_employee_loans(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "companyLoans/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        $('#employee_loans').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Employee Loan has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Filter Function
        $('#search_form').submit(function(e){
            dataTable.draw();
            e.preventDefault();
        });
    </script>
@endsection
