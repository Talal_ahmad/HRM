@extends('Admin.layouts.master')

@section('title', 'Edit Employee Component')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Edit Employee Component</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Salary Setup</a>
                                </li>
                                <li class="breadcrumb-item active">Employee Salary Component
                                </li>
                            </ol>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form" id="employe_component_form">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="department_id">Departments</label>
                                            <select name="department_id" id="department_id" class="select2 form-select" data-placeholder="Select Department" required disabled>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{$department->id == $payroll_employee->department ? 'selected' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employees">Employees</label>
                                            <input type="text" id="employee" class="form-control" value="{{$payroll_employee->employee_id.' - '.$payroll_employee->employee_code.' - '.$payroll_employee->first_name.' '.$payroll_employee->last_name}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Salary Component</th>
                                                        <th>Amount</th>
                                                        <th>Remarks</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($salary_components as $component)
                                                        <tr>
                                                            <td>{{$component->name}}</td>
                                                            <td>
                                                                <input type="hidden" name="components[]" value="{{$component->id}}">
                                                                <input type="text" name="sc_amount[{{$component->id}}]" class="form-control" value="{{$comp[$component->id]}}">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="remarks[{{$component->id}}]" value="{{$remarks[$component->id]}}" class="form-control">
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @if (env('COMPANY') != 'RoofLine')
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="pay_frequency">Pay Frequency</label>
                                            <select name="pay_frequency" id="pay_frequency" class="select2 form-select" data-placeholder="Select Pay Frequency">
                                                <option value=""></option>
                                                @foreach ($payFrequency as $frequency)
                                                    <option value="{{$frequency->id}}" {{$payroll_employee->pay_frequency == $frequency->id ? 'selected' : ''}}>{{$frequency->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="currency">Currency</label>
                                            <select name="currency" id="currency" class="select2 form-select" data-placeholder="Select Currency" required>
                                                <option value=""></option>
                                                @foreach ($currency as $value)
                                                    <option value="{{$value->id}}" {{$payroll_employee->currency == $value->id ? 'selected' : ''}}>{{$value->code}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>    
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="calculation_group">Calculation Group</label>
                                            <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="Select Calculation Group">
                                                <option value=""></option>
                                                @foreach ($calculationGroups as $group)
                                                    <option value="{{$group->id}}" {{$payroll_employee->deduction_group == $group->id ? 'selected' : ''}}>{{$group->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="calculation_exemptions">Calculation Exemptions</label>
                                            <select name="calculation_exemptions" id="calculation_exemptions" class="select2 form-select" data-placeholder="Select Calculation Exemptions">
                                                <option value=""></option>
                                                @foreach ($calculation_Exemptions_Assigned as $value)
                                                    <option value="{{$value->id}}" {{$payroll_employee->deduction_exemptions == $value->id ? 'selected' : ''}}>{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="calculation_assigned">Calculation Assigned</label>
                                            <select name="calculation_assigned" id="calculation_assigned" class="select2 form-select" data-placeholder="Select Calculation Assigned">
                                                <option value=""></option>
                                                @foreach ($calculation_Exemptions_Assigned as $value)
                                                    <option value="{{$value->id}}" {{$payroll_employee->deduction_allowed == $value->id ? 'selected' : ''}}>{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            // Update Employee Salary Component
            $("#employe_component_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
				var formData = new FormData(this);
                formData.append('type' , 'employeeSalaryComponents');
                $.ajax({
                    url: "{{route('salarySetup.update', $payroll_employee->id)}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            Toast.fire({
                                icon: 'success',
                                title: 'Employe Salary Component has been Updated Successfully!'
                            })
                            setTimeout(function(){
                                window.location.href = "{{ url('salarySetup') }}";
                            }, 2000);
                        }
                        
                    }
                });
            });
        });
    </script>
@endsection