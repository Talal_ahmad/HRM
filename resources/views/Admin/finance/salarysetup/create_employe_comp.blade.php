@extends('Admin.layouts.master')

@section('title', 'Add Employee Component')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Add Employee Component</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Salary Setup</a>
                                </li>
                                <li class="breadcrumb-item active">Employee Salary Component
                                </li>
                            </ol>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form" id="employe_component_form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="department_id">Departments</label>
                                            <select name="department_id" id="department_id" class="select2 form-select" data-placeholder="Select Department" required>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employees">Employees</label>
                                            <select name="employee" id="employees" class="select2 form-select" data-placeholder="Select Employee" required disabled>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 d-none" id="component">
                                        <div class="mb-1">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Salary Component</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="append_rows">
            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="pay_frequency">Pay Frequency</label>
                                            <select name="pay_frequency" id="pay_frequency" class="select2 form-select" data-placeholder="Select Pay Frequency">
                                                <option value=""></option>
                                                @foreach ($payFrequency as $frequency)
                                                    <option value="{{$frequency->id}}">{{$frequency->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="currency">Currency</label>
                                            <select name="currency" id="currency" class="select2 form-select" data-placeholder="Select Currency" required>
                                                <option value=""></option>
                                                @foreach ($currency as $value)
                                                    <option value="{{$value->id}}">{{$value->code}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @if (env('COMPANY') != 'RoofLine')    
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="calculation_group">Calculation Group</label>
                                                <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="Select Calculation Group">
                                                    <option value=""></option>
                                                    @foreach ($calculationGroups as $group)
                                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="calculation_exemptions">Calculation Exemptions</label>
                                            <select name="calculation_exemptions" id="calculation_exemptions" class="select2 form-select" data-placeholder="Select Calculation Exemptions">
                                                <option value=""></option>
                                                @foreach ($calculation_Exemptions_Assigned as $value)
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="calculation_assigned">Calculation Assigned</label>
                                            <select name="calculation_assigned" id="calculation_assigned" class="select2 form-select" data-placeholder="Select Calculation Assigned">
                                                <option value=""></option>
                                                @foreach ($calculation_Exemptions_Assigned as $value)
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            // get department employees
            $('#department_id').on('change', function(){
                var department_id = $(this).val();
                $.ajax({
                    type: "GET",
                    dataType: 'json',
                    url: "{{url('get_dept_employees')}}",
                    data: {
                        department_id : department_id
                    },
                    success: function(data)
                    {
                        $('#employees').attr('disabled', false);
                        $('#employees').html('');
                        $('#employees').html('<option value="">Select Employee</option>'); 
                        $.each(data, function(index,employee){
                            var opt = $('<option>');
                            opt.val(employee.id);
                            opt.text(employee.employee_id+' - '+employee.first_name+' '+employee.middle_name+'  '+employee.last_name+' - '+employee.employee_code +' - ' + employee.designation);
                            $('#employees').append(opt);
                        });
                    }
                });
            });

            // get employees components
            $('#employees').on('change', function(){
                var components = <?php echo json_encode($salary_components); ?>;
                $('#component').removeClass('d-none');
                $('#append_rows').html('');
                $.each(components, function(index,component){
                    var html = `<tr>
                        <td>${component.name}</td>
                        <td>
                            <input type="hidden" name="components[]" value="${component.id}">
                            <input type="text" class="form-control" name="sc_amount[${component.id}]" value="0.00">
                        </td>
                    </tr>`
                    $('#append_rows').append(html);
                });
            });

            // Store Salary Component
            $("#employe_component_form").on("submit", function (e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
				var formData = new FormData(this);
                formData.append('type' , 'employeeSalaryComponents');
                $.ajax({
                    url: "{{route('salarySetup.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            document.getElementById("employe_component_form").reset();
                            Toast.fire({
                                icon: 'success',
                                title: 'Employe Salary Component has been Added Successfully!'
                            })
                            setTimeout(function(){
                                window.location.href = "{{ url('salarySetup') }}";
                            }, 2000);
                        }
                        
                    }
                });
            });
        });
    </script>
@endsection