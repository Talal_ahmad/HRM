@extends('Admin.layouts.master')
@section('title', 'Salary Setup')
@section('style')
<style>
    .sticky-column1 {
        position: sticky;
        left: 0;
        z-index: 2;
    }
    .sticky-column2 {
        position: sticky;
        left: 130px;
        z-index: 2;
    }
    .sticky-column3 {
        position: sticky;
        left: 258px;
        z-index: 2;
    }
    .sticky-column4 {
        position: sticky;
        left: 430px;
        z-index: 2;
    }
    .sticky-td-column1 {
        position: sticky;
        left: 0;
        z-index: 0;
        background-color : #283046 !important;
        color: white !important;
    }
    .sticky-td-column2 {
        position: sticky;
        left: 130px;
        z-index: 0;
        background-color : #283046 !important;
        color: white !important;
    }
    .sticky-td-column3 {
        position: sticky;
        left: 258px;
        z-index: 0;
        background-color : #283046 !important;
        color: white !important;
    }
    .sticky-td-column4 {
        position: sticky;
        left: 430px;
        z-index: 0;
        background-color : #283046 !important;
        color: white !important;
    }
</style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Salary Setup</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Salary Setup
                                </li>
                            </ol>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    @can('Employee Salary Component')
                        <li class="nav-item">
                            <a class="nav-link active" id="employee-salary-component-tab" data-bs-toggle="tab" href="#employee-salary-component" aria-controls="employee-salary-component" role="tab" aria-selected="true">Employee Salary Component</a>
                        </li>
                    @endcan
                    @can('Salary Component Types')    
                        <li class="nav-item">
                            <a class="nav-link" id="salary-component-types-tab" data-bs-toggle="tab" href="#salary-component-types" aria-controls="salary-component-types" role="tab" aria-selected="false" onclick="salary_component_types()">Salary Component Types</a>
                        </li>
                    @endcan
                    @can('Salary Components')    
                        <li class="nav-item">
                            <a class="nav-link" id="salary-components-tab" data-bs-toggle="tab" href="#salary-components" aria-controls="salary-components" role="tab" aria-selected="false" onclick="salary_components()">Salary Components</a>
                        </li>
                    @endcan
                    @can('Department Wise Components')    
                        <li class="nav-item">
                            <a class="nav-link" id="dept-wise-component-tab" data-bs-toggle="tab" href="#dept-wise-component" aria-controls="dept-wise-component" role="tab" aria-selected="false">Department Wise Components</a>
                        </li>
                    @endcan
                    @can('Incentive Employees Components')    
                        <li class="nav-item">
                            <a class="nav-link" id="incentive-employee-component-tab" data-bs-toggle="tab" href="#incentive-employee-component" aria-controls="incentive-employee-component" role="tab" aria-selected="false">Incentive Employees Components</a>
                        </li>
                    @endcan
                    @can('Employees Monthly Components')    
                        <li class="nav-item">
                            <a class="nav-link" id="employee-monthly-component-tab" data-bs-toggle="tab" href="#employee-monthly-component" aria-controls="employee-monthly-component" role="tab" aria-selected="false">Employee Monthly Components</a>
                        </li>
                    @endcan
                    @can('Employees Components By Month')    
                    <li class="nav-item">
                        <a class="nav-link" id="component-by-month-tab" data-bs-toggle="tab" href="#component-by-month" aria-controls="component-by-month" role="tab" aria-selected="false">Components By Month</a>
                    </li>
                    @endcan
                    @can('Employees Multiple Components By Month')    
                    <li class="nav-item">
                        <a class="nav-link" id="multiple-component-by-month-tab" data-bs-toggle="tab" href="#multiple-component-by-month" aria-controls="multiple-component-by-month" role="tab" aria-selected="false">Multiple Components By Month</a>
                    </li>
                    @endcan
                </ul>
                <div class="tab-content">
                    @can('Employee Salary Component')    
                        <div class="tab-pane active" id="employee-salary-component" aria-labelledby="employee-salary-component-tab" role="tabpanel">
                            <section id="basic-datatable">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card mb-1">
                                            <div class="card-body">
                                                <form id="search_form" action="{{route('salarySetup.index')}}" method="GET">
                                                    <div class="row">
                                                        <div class="col-md-4 col-12">
                                                            <label class="form-label">Filter By Department:</label>
                                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select filter" data-column="9" data-placeholder="Select Department" onchange="getdepartmentEmployees(this,'#employeeFilter','employee-salary-component')" required>
                                                                <option value=""></option>
                                                                <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : '' }}>All Departments</option>
                                                                @foreach (departments() as $department)
                                                                    <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        @if (env('COMPANY') == 'JSML')        
                                                        <div class="col-md-3 col-12">
                                                                <div class="mb-1">
                                                                    <label class="form-label" for="title">Section</label>
                                                                    <select name="section[]" id="section" data-placeholder="Select Section" class="select2 form-select" multiple required>
                                                                        
                                                                    </select>
                                                                    <div class="button-container mt-1">
                                                                        <button class="btn btn-sm btn-primary" type="button"
                                                                            onclick="selectAll('#section')">Select All</button>
                                                                        <button class="btn btn-sm btn-danger" type="button"
                                                                            onclick="deselectAll('#section')">Deselect All</button>
                                                                    </div>
                                                                </div>
                                                            </div>    
                                                        @endif
                                                        <div class="col-md-4 col-12">
                                                            <label class="form-label">Filter By Employee:</label>
                                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select filter" data-column="8" data-placeholder="Select Employee">
                                                                @if (isset($filterEmployees))
                                                                    @foreach ($filterEmployees as $employee)
                                                                        <option value="{{$employee->id}}" {{$employee->id == request('employeeFilter') ? 'selected' : ''}}>{{$employee->employee_id.' - '.$employee->employee_code.' - '.$employee->first_name}} {{$employee->last_name}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4 col-12">
                                                            <label class="form-label">Filter By Designation:</label>
                                                            <select name="designationFilter" id="designationFilter" class="select2 form-select filter" data-column="9" data-placeholder="Select Designation">
                                                                <option value="">Select Designation</option>
                                                                @foreach (designation() as $jobTitle)
                                                                    <option value="{{$jobTitle->id}}" {{$jobTitle->id == request('designationFilter') ? 'selected' : ''}}>{{$jobTitle->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 text-end">
                                                            {{-- <button type="reset" class="btn btn-danger mt-1">Reset</button> --}}
                                                            <a href="{{url('salarySetup')}}" class="btn btn-danger mt-1">Reset</a>
                                                            <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        @if (Session::has('message'))
                                            <div class="alert alert-danger">{{Session::get('message')}}</div>
                                        @endif
                                        <div class="card pb-2">
                                            <table class="table" id="employee_salary_component">
                                                <thead>
                                                    <tr>
                                                        <th class="not_include"></th>
                                                        <th>Sr.NO</th>
                                                        <th>Emp. ID</th>
                                                        <th>Emp. Code</th>
                                                        @if (env('COMPANY') == 'CLINIX')
                                                            <th>Joined Date</th>
                                                        @endif
                                                        <th>Employee</th>
                                                        <th>Desigination</th>
                                                        @if (env('COMPANY') == 'CLINIX')
                                                            <th>Type</th>
                                                        @endif
                                                        <th>Dept./<br>Branch</th>
                                                        <th>Calculation Group</th>
                                                        @foreach ($salary_components as $item)
                                                            <th>{{$item->name}}</th>
                                                        @endforeach
                                                        @can('Edit Employee Salary Component')
                                                            <th class="not_include">Actions</th>
                                                        @endcan
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (isset($employees) && count($employees) > 0)
                                                        @foreach ($employees as $key => $employee)
                                                            <tr>
                                                                <td></td>
                                                                <td>{{$key+1}}</td>
                                                                <td>{{$employee->employee_id}}</td>
                                                                <td>{{$employee->employee_code}}</td>
                                                                @if (env('COMPANY') == 'CLINIX')
                                                                    <td>{{$employee->joined_date}}</td>
                                                                @endif
                                                                <td>{{$employee->first_name.' '.$employee->middle_name.' '. $employee->last_name}}</td>
                                                                <td>{{$employee->desigination}}</td>
                                                                @if (env('COMPANY') == 'CLINIX')
                                                                    <td>{{$employee->type}}</td>
                                                                @endif
                                                                <td>{{$employee->title}}</td>
                                                                <td>{{$employee->deduction_group_name}}</td>
                                                                @foreach ($salary_components as $item)
                                                                    <td>{{$data[$employee->id][$item->id]}}</td>
                                                                @endforeach
                                                                @can('Edit Employee Salary Component')
                                                                    <td>
                                                                        <a href="{{route('salarySetup.edit', $employee->id)}}" class="item-edit font-medium-3"><i class="far fa-edit"></i></a>
                                                                    </td>
                                                                @endcan
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    @endcan

                    {{-- Salary Component Types --}}
                    <div class="tab-pane" id="salary-component-types" aria-labelledby="salary-component-types-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="salary_component_types">
                                            <thead>
                                                <tr>
													<th class="not_include"></th>
                                                    <th>Sr.NO</th>
                                                    <th>Code</th>
                                                    <th>Name</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- Salary Components --}}
                    <div class="tab-pane" id="salary-components" aria-labelledby="salary-components-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <table class="table" id="salary_components">
                                            <thead>
                                                <tr>
                                                    <th class="not_include"></th>
                                                    <th>Sr.No</th>
                                                    <th>Name</th>
                                                    <th>Salary Component Types</th>
                                                    <th>Tax Code</th>
                                                    <th>Details</th>
                                                    <th class="not_include">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Department Wise Components --}}
                    <div class="tab-pane" id="dept-wise-component" aria-labelledby="dept-wise-component-tab" role="tabpanel">
                        <section id="multiple-column-form">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Department Wise Components</h4>
                                        </div>
                                        <div class="card-body">
                                            <form class="form">
                                                <div class="row">
                                                    <div class="col-md-4 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="department">Departments</label>
                                                            <select name="department" id="department" class="select2 form-select" data-placeholder="Select Department" required>
                                                                <option value=""></option>
                                                                <option value="all">All</option>
                                                                @foreach (departments() as $department)
                                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="dept_salary_comp">Salary Components</label>
                                                            <select name="dept_salary_comp" id="dept_salary_comp" class="select2 form-select" data-placeholder="Select Salary Component">
                                                                <option value=""></option>
                                                                @foreach ($salary_components as $component)
                                                                    <option value="{{$component->id}}">{{$component->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary" id="department_wise_apply">Apply</button>
                                                    <a href="{{url('salarySetup')}}" type="button" class="btn btn-danger">Reset</a>                                
                                                    </div>                                          
                                                    
                                                </div>
                                            </form>
                                            <form id="department_wise_comp_form">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-12" id="salary_comp">
                                                    </div>
                                                    <div class="col-12 text-end d-none" id="save">
                                                        <button type="submit" class="btn btn-primary mt-1">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Incentive Employees Components --}}
                    <div class="tab-pane" id="incentive-employee-component" aria-labelledby="incentive-employee-component-tab" role="tabpanel">
                        <section id="multiple-column-form">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Incentive Employees Components</h4>
                                        </div>
                                        <div class="card-body">
                                            <form class="form">
                                                <div class="row">
                                                    <div class="col-md-4 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="for_incentive">Departments</label>
                                                            <select name="department" id="for_incentive" class="select2 form-select" data-placeholder="Select Department" required>
                                                                <option value=""></option>
                                                                @foreach (departments() as $department)
                                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-12 mt-2">
                                                        <button type="submit" class="btn btn-primary" id="incentive_employee_apply">Apply</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <form id="incentive_employee_comp_form">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-12" id="incentive_emp_component">
                                                    </div>
                                                    <div class="col-12 text-end d-none" id="incentive_save">
                                                        <button type="submit" class="btn btn-primary mt-1">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Employee Monthly Component --}}
                    <div class="tab-pane" id="employee-monthly-component" aria-labelledby="employee-monthly-component-tab" role="tabpanel">
                        <section id="multiple-column-form">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Employee Monthly Component</h4>
                                        </div>
                                        <div class="card-body">
                                            <form class="form">
                                                <div class="row">
                                                    <div class="col-md-3 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="monthly_salary_component">Salary Components</label>
                                                            <select name="salary_component" id="monthly_salary_component" class="select2 form-select" data-placeholder="Select Components" required>
                                                                <option value=""></option>
                                                                @foreach ($monthly_salary_components as $monthly_salary_component)
                                                                    <option value="{{$monthly_salary_component->id}}">{{$monthly_salary_component->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="monthly_departments">Departments</label>
                                                            <select name="department" id="monthly_departments" class="select2 form-select" data-placeholder="Select Department" required>
                                                                <option value=""></option>
                                                                @foreach (departments() as $department)
                                                                    <option value="{{$department->id}}">{{$department->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="monthly_employment_status">Employment Status</label>
                                                            <select name="employment_status" id="monthly_employment_status" class="select2 form-select" data-placeholder="Select Employment Status" required>
                                                                <option value=""></option>
                                                                <option value="all">All</option>
                                                                @foreach ($employment_status as $employment)
                                                                    <option value="{{$employment->id}}">{{$employment->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-12 mt-2">
                                                        <button type="submit" class="btn btn-primary" id="employee_monthly_apply">Apply</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <form id="employee_monthly_comp_form">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-12" id="emp_month_comp">
                                                    </div>
                                                    <div class="col-12 text-end d-none" id="emp_month_comp_save">
                                                        <button type="submit" class="btn btn-primary mt-1">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Components By Month --}}
                    <div class="tab-pane" id="component-by-month" aria-labelledby="component-by-month-tab" role="tabpanel">
                        <section id="multiple-column-form">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Monthly Components</h4>
                                        </div>
                                        <div class="card-body">
                                            <form class="form">
                                                <div class="row">
                                                    <div class="col-md-3 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="salaryComponent">Salary Components</label>
                                                            <select name="salary_component" id="salaryComponent" class="select2 form-select" data-placeholder="Select Components" required>
                                                                <option value=""></option>
                                                                @foreach ($monthly_salary_components as $monthly_salary_component)
                                                                    <option value="{{$monthly_salary_component->id}}">{{$monthly_salary_component->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    @if (env('COMPANY') == 'JSML') 
                                                        <div class="col-md-5 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="byMonthDepartment_1">Departments</label>
                                                                <select name="department" id="byMonthDepartment_1" class="select2 form-select" data-placeholder="Select Department" required>
                                                                    <option value=""></option>
                                                                    @foreach (departments() as $department)
                                                                        <option value="{{$department->id}}" {{!empty(request('department'))}}>{{$department->title}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>       
                                                        <div class="col-md-3 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="title">Section</label>
                                                                <select name="department[]" id="section_1" data-placeholder="Select Section" class="select2 form-select" onchange="getdepartmentEmployees(this,'#employee_Filter','component_by_month')" multiple required>
                                                                    
                                                                </select>
                                                                <div class="button-container mt-1">
                                                                    <button class="btn btn-sm btn-primary" type="button"
                                                                        onclick="selectAll('#section_1')">Select All</button>
                                                                    <button class="btn btn-sm btn-danger" type="button"
                                                                        onclick="deselectAll('#section_1')">Deselect All</button>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    @else
                                                    <div class="col-md-5 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="byMonthDepartment">Departments</label>
                                                            <select name="department[]" id="byMonthDepartment" class="select2 form-select" data-placeholder="Select Department" onchange="getdepartmentEmployees(this,'#employee_Filter','component_by_month')" required multiple>
                                                                <option value=""></option>
                                                                @foreach (departments() as $department)
                                                                    <option value="{{$department->id}}" {{!empty(request('department')) ? in_array($department->id,request('department')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#byMonthDepartment')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#byMonthDepartment')">Deselect All</button>
                                                        </div>
                                                    </div>   
                                                    @endif
                                                    <div class="col-md-4 col-12">
                                                            <label class="form-label">Filter By Employee:</label>
                                                            <select name="employee_Filter" id="employee_Filter" class="select2 form-select filter" data-column="8" data-placeholder="Select Employee">
                                                                <option value="">Select Employee</option>
                                                                @foreach (employees() as $employee)
                                                                    <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->last_name}} {{$employee->designation}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    @if (env('COMPANY') == 'RoofLine' || env('COMPANY') == 'Ajmal Dawakhana')
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="calculationGroup">Calculation Group</label>
                                                            <select name="calculationGroup" id="calculationGroup" class="select2 form-select" data-placeholder="Select Calculation Group" required>
                                                                <option value=""></option>
                                                                @foreach ($calculationGroups as $group)
                                                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    @endif 
                                                    <div class="col-md-3 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="employement_status">Employment Status</label>
                                                            <select name="employment_status[]" id="employement_status" class="select2 form-select" data-placeholder="Select Employment Status" multiple>
                                                                <option value=""></option>
                                                                @foreach ($employment_status as $employment)
                                                                    <option value="{{$employment->id}}" {{!empty(request('employement_status')) ? in_array($employment->id,request('employement_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="button-container mt-1">
                                                                <button class="btn btn-sm btn-primary" type="button"
                                                                    onclick="selectAll('#employement_status')">Select All</button>
                                                                <button class="btn btn-sm btn-danger" type="button"
                                                                    onclick="deselectAll('#employement_status')">Deselect All</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="month">Month</label>
                                                            <input type="month" name="month" id="month" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-2">
                                                    <div class="col-6">
                                                        <input type="checkbox" id="suppress" name="suppress" value="1" {{!empty(request('suppress')) ? 'checked' : ''}}>
                                                        <label for="suppress">Suppress zero</label><br><br>
                                                    </div>
                                                    <div class="col-6 text-end">
                                                        <button type="submit" class="btn btn-primary" id="component_by_month_apply">Apply</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <form class="form" id="component_by_month_form">
                                                @csrf
                                                <div class="row">
                                                    <input type="hidden" name="salary_component" id="month_salary_component">
                                                    <input type="hidden" name="type" value="component_by_month">
                                                    <input type="hidden" name="month" id="component_month">
                                                    <input type="hidden" name="deducationGroup" id="deducationGroup">
                                                    <div class="col-md-12" id="component_by_month_append">
                                                    </div>
                                                    <div class="col-12 text-end d-none" id="component_save">
                                                        <button type="submit" class="btn btn-primary mt-1">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Multiple Components By Month --}}
                    <div class="tab-pane" id="multiple-component-by-month" aria-labelledby="multiple-component-by-month-tab" role="tabpanel">
                        <section id="multiple-column-form">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Multiple Monthly Components</h4>
                                        </div>
                                        <div class="card-body">
                                            <form class="form">
                                                <div class="row">
                                                    <div class="col-md-4 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="multipleComponents">Salary Components</label>
                                                            <select name="salary_component[]" id="multipleComponents" class="select2 form-select" data-placeholder="Select Components" required multiple>
                                                                <option value=""></option>
                                                                @foreach ($monthly_salary_components as $monthly_salary_component)
                                                                    <option value="{{$monthly_salary_component->id}}">{{$monthly_salary_component->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="button-container mt-1">
                                                                <button class="btn btn-sm btn-primary" type="button"
                                                                    onclick="selectAll('#multipleComponents')">Select All</button>
                                                                <button class="btn btn-sm btn-danger" type="button"
                                                                    onclick="deselectAll('#multipleComponents')">Deselect All</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="MultipleMonthDepartment">Departments</label>
                                                            <select name="department[]" id="MultipleMonthDepartment" class="select2 form-select" data-placeholder="Select Department" onchange="getdepartmentEmployees(this,'#multiple_employee_Filter','component_by_month')" required multiple>
                                                                <option value=""></option>
                                                                @foreach (departments() as $department)
                                                                    <option value="{{$department->id}}" {{!empty(request('department')) ? in_array($department->id,request('department')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="button-container mt-1">
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                onclick="selectAll('#MultipleMonthDepartment')">Select All</button>
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="deselectAll('#MultipleMonthDepartment')">Deselect All</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                            <label class="form-label" for="multiple_employee_Filter">Filter By Employee:</label>
                                                            <select name="employee_Filter" id="multiple_employee_Filter" class="select2 form-select filter" data-column="8" data-placeholder="Select Employee">
                                                                <option value="">Select Employee</option>
                                                                @foreach (employees() as $employee)
                                                                    <option value="{{$employee->id}}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{$employee->first_name}} {{$employee->last_name}} {{$employee->designation}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    @if (env('COMPANY') == 'RoofLine' || env('COMPANY') == 'Ajmal Dawakhana')
                                                    <div class="col-md-6 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="MultipleCalculationGroup">Calculation Group</label>
                                                            <select name="calculationGroup" id="MultipleCalculationGroup" class="select2 form-select" data-placeholder="Select Calculation Group" required>
                                                                <option value=""></option>
                                                                @foreach ($calculationGroups as $group)
                                                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    @endif 
                                                    <div class="col-md-3 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="multiple_employement_status">Employment Status</label>
                                                            <select name="employment_status[]" id="multiple_employement_status" class="select2 form-select" data-placeholder="Select Employment Status" multiple>
                                                                <option value=""></option>
                                                                @foreach ($employment_status as $employment)
                                                                    <option value="{{$employment->id}}" {{!empty(request('employement_status')) ? in_array($employment->id,request('employement_status')) ? 'selected' : '' : ''}}>{{$employment->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="button-container mt-1">
                                                                <button class="btn btn-sm btn-primary" type="button"
                                                                    onclick="selectAll('#multiple_employement_status')">Select All</button>
                                                                <button class="btn btn-sm btn-danger" type="button"
                                                                    onclick="deselectAll('#multiple_employement_status')">Deselect All</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="multiple_month">Month</label>
                                                            <input type="month" name="month" id="multiple_month" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-2">
                                                    <div class="col-12 text-end">
                                                        <button type="submit" class="btn btn-primary" id="multiple_component_by_month_apply">Apply</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <form class="form" id="multiple_component_by_month_form">
                                                @csrf
                                                <div class="row">
                                                    <input type="hidden" name="salary_component" id="multiple_month_salary_component">
                                                    <input type="hidden" name="type" value="multiple_component_by_month">
                                                    <input type="hidden" name="month" id="multiple_component_month">
                                                    <input type="hidden" name="deducationGroup" id="multiple_deducationGroup">
                                                    <div class="col-md-12" id="multiple_component_by_month_append">
                                                    </div>
                                                    <div class="col-12 text-end d-none" id="multiple_component_save">
                                                        <button type="submit" class="btn btn-primary mt-1">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <!--Salary Component Types Add Modal -->
                    <div class="modal fade text-start" id="salary_component_type_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Add Salary Component Type</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="salary_component_type_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="code">Code</label>
                                                    <input type="text" id="code" class="form-control" name="code" placeholder="Code" required/>
                                                </div>
                                            </div>
											<div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="name">Name</label>
                                                    <input type="text" id="name" class="form-control" name="name" placeholder="Name" required/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">Reset</button>
                                        <button type="submit" class="btn btn-primary" id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Salary Component Types Add Modal -->

					<!--Salary Component Types Edit Modal -->
                    <div class="modal fade text-start" id="salary_component_type_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Edit Salary Component Type</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="salary_component_type_edit_form">
                                    @csrf
									@method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_code">Code</label>
                                                    <input type="text" id="edit_code" class="form-control" name="code" placeholder="Code" required/>
                                                </div>
                                            </div>
											<div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_name">Name</label>
                                                    <input type="text" id="edit_name" class="form-control" name="name" placeholder="Name" required/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">Reset</button>
                                        <button type="submit" class="btn btn-primary" id="update">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Salary Component Types Edit Modal -->

                    <!--Salary Component Add Modal -->
                    <div class="modal fade text-start" id="salary_component_add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Add Salary Component</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="salary_component_add_form">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="name">Name</label>
                                                    <input type="text" id="name" class="form-control" name="name" placeholder="Name" required/>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="componentType">Salary Component Type</label>
                                                    <select name="componentType" id="componentType" class="select2 form-select" data-placeholder="Select Salary Component Type" required>
                                                        <option value=""></option>
                                                        @foreach ($salary_component_types as $type)
                                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="tax_id">Tax Code</label>
                                                    <select name="tax_id" id="tax_id" class="select2 form-select" data-placeholder="Select Tax Code">
                                                        <option value=""></option>
                                                        @foreach ($taxes as $tax)
                                                            <option value="{{$tax->id}}">{{$tax->code}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
											<div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="details">Details</label>
                                                    <textarea name="details" id="details" cols="20" rows="2" class="form-control"></textarea>
                                                </div>
                                            </div>
											<div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="applied_month">Applied Month</label>
                                                    <input type="text" name="applied_month" id="applied_month" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                                </div>
                                            </div>
											<div class="col-12">
                                                <div class="mb-1">
                                                    <input type="checkbox" class="custom-control-input" value="1" name="is_edit" id="is_edit" value="Approved"/>
                                                    <label class="custom-control-label" for="is_edit">Is Editable</label>
                                                </div>
                                            </div>
											<div class="col-12">
                                                <div class="mb-1">
                                                    <input type="checkbox" class="custom-control-input" value="1" name="is_monthly" id="is_monthly" value="Approved"/>
                                                    <label class="custom-control-label" for="is_monthly">Is Monthly Basis</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">Reset</button>
                                        <button type="submit" class="btn btn-primary" id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Salary Component Add Modal -->

                    <!--Salary Component Edit Modal -->
                    <div class="modal fade text-start" id="salary_component_edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel17">Edit Salary Component</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form class="form" id="salary_component_edit_form">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_component_name">Name</label>
                                                    <input type="text" id="edit_component_name" class="form-control" name="name" placeholder="Name" required/>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_componentType">Salary Component Type</label>
                                                    <select name="componentType" id="edit_componentType" class="select2 form-select" data-placeholder="Select Salary Component Type" required>
                                                        <option value=""></option>
                                                        @foreach ($salary_component_types as $type)
                                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="tax_id">Tax Code</label>
                                                    <select name="tax_id" id="edit_tax_id" class="select2 form-select" data-placeholder="Select Tax Code">
                                                        <option value=""></option>
                                                        @foreach ($taxes as $tax)
                                                            <option value="{{$tax->id}}">{{$tax->code}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
											<div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_details">Details</label>
                                                    <textarea name="details" id="edit_details" cols="20" rows="2" class="form-control"></textarea>
                                                </div>
                                            </div>
											<div class="col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_applied_month">Applied Month</label>
                                                    <input type="text" name="applied_month" id="edit_applied_month" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <input type="checkbox" class="custom-control-input" value="1" name="is_edit" id="edit_is_edit" value="Approved"/>
                                                    <label class="custom-control-label" for="edit_is_edit">Is Editable</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <input type="checkbox" class="custom-control-input" value="1" name="is_monthly" id="edit_is_monthly" value="Approved"/>
                                                    <label class="custom-control-label" for="edit_is_monthly">Is Monthly Basis</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-outline-success">Reset</button>
                                        <button type="submit" class="btn btn-primary" id="update">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--End Salary Component Edit Modal -->
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                // console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $("#byMonthDepartment_1").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section_1').empty();
                            $('#section_1').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                // console.log(value.id);
                                $('#section_1').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $('#employee_salary_component').DataTable({
                ordering: true,
                "columnDefs": [
					{
						// For Responsive
						className: 'control',
						orderable: false,
						targets: 0
					},
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',


                            customize: function (doc) {
                                  //Remove the title created by datatTables
                                  doc.content.splice(0,1);
                                  var now = new Date();
                                  var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                                  doc.pageMargins = [5,60,5,5];
                                  doc.defaultStyle.margin = 0;
                                  doc.defaultStyle.fontSize = 7;
                                  doc.styles.tableHeader.fontSize = 6;
 
                                  doc['header']=(function() {
                                      return {
                                          columns: [
 
                                              {
                                                  alignment: 'left',
                                                  italics: true,
                                                  text: 'Employees Salary Component Table ' + now,
                                                  fontSize: 10,
                                                  margin: [10,0]
                                              },
                                              {
                                                  alignment: 'center',
                                                  fontSize: 14,
                                                  text: 'Employees Salary Component Report'
                                              }
                                          ],
                                          margin: 20
                                      }
                                  });
 
                                  doc['footer']=(function(page, pages) {
                                      return {
                                          columns: [
                                              {
                                                  alignment: 'left',
                                                  text: ['Created on: ', { text: jsDate.toString() }]
                                              },
                                              {
                                                  alignment: 'right',
                                                  text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                                              }
                                          ],
                                          margin: 20
                                      }
                                  });
 
                                  var objLayout = {};
                                  objLayout['hLineWidth'] = function(i) { return .5; };
                                  objLayout['vLineWidth'] = function(i) { return .5; };
                                  objLayout['hLineColor'] = function(i) { return '#aaa'; };
                                  objLayout['vLineColor'] = function(i) { return '#aaa'; };
                                  objLayout['paddingLeft'] = function(i) { return 4; };
                                  objLayout['paddingRight'] = function(i) { return 4; };
                                  doc.content[0].layout = objLayout;
                          },


                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Add Employee Salary Component')
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        action: function ( e, dt, button, config ) {
                            window.location = 'salarySetup/create';
                        },   
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    },
                    @endcan
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Employee Salary Component</h6>');
            // Store Salary Component Type
            $("#salary_component_type_add_form").on("submit", function (e) {
                blockUI();
                e.preventDefault();
				var formData = new FormData(this);
                formData.append('type' , 'salary_component_type');
                $.ajax({
                    url: "{{route('salarySetup.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#salary_component_type_add_modal").modal("hide");
                            document.getElementById("salary_component_type_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#salary_component_types').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Salary Component Type has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

			// Salary Component Type Update
            $("#salary_component_type_edit_form").on("submit", function (e) {
                blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'salary_component_type');
                $.ajax({
                    url: "{{url('salarySetup')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#salary_component_type_edit_modal").modal("hide");
                            document.getElementById("salary_component_type_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#salary_component_types').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Salary Component Type has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Store Salary Component
            $("#salary_component_add_form").on("submit", function (e) {
                blockUI();
                e.preventDefault();
				var formData = new FormData(this);
                formData.append('type' , 'salary_component');
                $.ajax({
                    url: "{{route('salarySetup.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#salary_component_add_modal").modal("hide");
                            document.getElementById("salary_component_add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#salary_components').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Salary Component has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Salary Component Update
            $("#salary_component_edit_form").on("submit", function (e) {
                e.preventDefault();
                blockUI();
                var formData = new FormData(this);
                formData.append('type' , 'salary_component');
                $.ajax({
                    url: "{{url('salarySetup')}}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $.unblockUI();
                        if(response.errors){
                            $.each(response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $("#salary_component_edit_modal").modal("hide");
                            document.getElementById("salary_component_edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#salary_components').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Salary Component has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Department Wise Component
            $('#department_wise_apply').on('click', function(e){
                e.preventDefault();
                var department_id = $('#department').val();
                var dept_salary_comp = $('#dept_salary_comp').val();
                if(department_id != '')
                {
                 
                    blockUI();
                    var components = <?php echo json_encode($salary_components); ?>;
                    $.ajax({
                        url: "{{url('salarySetup')}}" + "/" + department_id,
                        type: "get",
                        data: {
                            type: 'department',
                            dept_salary_comp: dept_salary_comp
                        },
                        success: function(response) {
                            $.unblockUI();
                            $("#salary_comp").empty();
                            $('#save').removeClass('d-none');
                            var html = '';
                            html += `
                                <div class="table-responsive" style="overflow: auto;max-height: 400px;">
                                    <table class="table table-bordered mb-0">
                                        <thead style="position: sticky;top: 0;z-index: 1;">
                                            <tr>
                                                <th class="sticky-column1">Employee ID</th>
                                                <th class="sticky-column2">Employee Code</th>
                                                <th class="sticky-column3">Employee</th>
                                                <th class="sticky-column4">Designation</th>`;
                                                if(dept_salary_comp != ''){
                                                    html += `
                                                    <input type="hidden" name="salary_components[]" value="${dept_salary_comp}">
                                                    <th>${response.component_name.name}</th>
                                                    <th>Remarks</th>`
                                                }
                                                else{
                                                    $.each(components, function(index, component){
                                                        html += `<th>
                                                        <input type="hidden" name="salary_components[]" value="${component.id}">
                                                        ${component.name}
                                                        </th>`;
                                                    })
                                                }
                                            html += `</tr>
                                        </thead>
                                            <tbody>`;
                                            if(response.employees.length != 0)
                                            {
                                                $.each(response.employees, function(key, employee){
                                                    var middleName = employee.middle_name != null ? employee.middle_name : '';
                                                    var lastName = employee.last_name != null ? employee.last_name : '';
                                                    html += `<tr>
                                                        <td class="sticky-td-column1">${employee.employee_id}</td>
                                                        <td class="sticky-td-column2">${employee.employee_code}</td>
                                                        <td class="text-nowrap sticky-td-column3">${employee.first_name + ' ' + middleName + ' ' + lastName}
                                                        <input type="hidden" name="employees[]" value="${employee.id}">
                                                        </td>
                                                        <td class="sticky-td-column4">${employee.designation}</td>`;
                                                        if(dept_salary_comp != ''){
                                                            var comp_value = response.data[employee.id][dept_salary_comp] === null ? 0.00 : response.data[employee.id][dept_salary_comp].amount;
                                                            html += `
                                                            <td style="padding: 0px !important;">
                                                                <input type="text" class="form-control" name="sc_amount[${employee.id}][${dept_salary_comp}][]" value="${comp_value}" style="border: 0px">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="remarks[${employee.id}][${dept_salary_comp}][]" class="form-control" style="width:300px"/>
                                                            </td>
                                                            `;
                                                        }
                                                        else{
                                                            $.each(components, function(index, comp){
                                                                $.each(response.data[employee.id][comp.id], function(index2, item){
                                                                    var value = item === null ? 0.00 : item.amount;
                                                                    html += `<td style="padding: 0px !important;">
                                                                            <input type="text" class="form-control" name="sc_amount[${employee.id}][${comp.id}][]" value="${value}" style="border: 0px">
                                                                    </td>`;
                                                                });
                                                            })
                                                        }
                                                    html += `</tr>`;
                                                });
                                            }
                                            else{
                                                html += `<tr><td colspan="${components.length + 1}" class="text-center">No record found!</td></tr>`
                                            }
                                        html += `</tbody>
                                        </table>
                                    </div>`
                            $("#salary_comp").append(html);
                        },
                    })
                }
            });

            // Store Department Wise Component
            $("#department_wise_comp_form").on("submit", function (e) {
                blockUI();
                e.preventDefault();
				var formData = new FormData(this);
                formData.append('type' , 'department_wise');
                $.ajax({
                    url: "{{route('salarySetup.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Component has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Incnetive Employees Component
            $('#incentive_employee_apply').on('click', function(e){
                e.preventDefault();
                var department_id = $('#for_incentive').val();
                if(department_id != '')
                {
                    blockUI();
                    var components = <?php echo json_encode($salary_components); ?>;
                    $.ajax({
                        url: "{{url('salarySetup')}}" + "/" + department_id,
                        type: "get",
                        success: function(response) {
                            $.unblockUI();
                            $("#incentive_emp_component").empty();
                            $('#incentive_save').removeClass('d-none');
                            var html = '';
                            html += `
                                <div class="table-responsive" style="overflow: auto;max-height: 400px;">
                                    <table class="table table-bordered mb-0">
                                        <thead style="position: sticky;top: 0;z-index: 1;">
                                            <tr>
                                                <th class="sticky-column1">Employee ID</th>
                                                <th class="sticky-column2">Employee Code</th>
                                                <th class="sticky-column3">Employees</th>
                                                <th class="sticky-column4">Designation</th>`;
                                                $.each(components, function(index, component){
                                                    html += `<th>
                                                    <input type="hidden" name="salary_components[]" value="${component.id}">
                                                    ${component.name}
                                                    </th>`;
                                                })
                                            html += `</tr>
                                        </thead>
                                            <tbody>`;
                                            if(response.employees.length > 0)
                                            {
                                                $.each(response.employees, function(key, employee){
                                                    var middleName = employee.middle_name != null ? employee.middle_name : '';
                                                    var lastName = employee.last_name != null ? employee.last_name : '';
                                                    html += `<tr>
                                                        <td class="sticky-td-column1">${employee.employee_id}</td>
                                                        <td class="sticky-td-column2">${employee.employee_code}</td>
                                                        <td class="text-nowrap sticky-td-column3">${employee.first_name + ' ' + middleName + ' ' + lastName}
                                                        <input type="hidden" name="employees[]" value="${employee.id}">
                                                        </td>
                                                        <td class="sticky-td-column4">${employee.designation}</td>`;
                                                        $.each(components, function(index, comp){
                                                            $.each(response.data[employee.id][comp.id], function(index2, item){
                                                                var value = item === null ? 0.00 : item.amount;
                                                                html += `<td style="padding: 0px !important;">
                                                                        <input type="text" class="form-control" name="sc_amount[${employee.id}][${comp.id}][]" value="${value}" style="border: 0px">
                                                                </td>`;
                                                            });
                                                        })
                                                    html += `</tr>`;
                                                });
                                            }
                                            else{
                                                html += `<tr><td colspan="${components.length + 1}" class="text-center">No record found!</td></tr>`
                                            }
                                        html += `</tbody>
                                        </table>
                                    </div>`
                            $("#incentive_emp_component").append(html);
                        },
                    })
                }
            });

            // Store Incentive Employee Component
            $("#incentive_employee_comp_form").on("submit", function (e) {
                blockUI();
                e.preventDefault();
				var formData = new FormData(this);
                formData.append('type' , 'incentive');
                $.ajax({
                    url: "{{route('salarySetup.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            Toast.fire({
                                icon: 'success',
                                title: 'Incentive Employees Component has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });

            // Monthly Employee Component
            $('#employee_monthly_apply').click(function(e){
                e.preventDefault();
                var employment_status = $('#monthly_employment_status').val();
                if(employment_status != '')
                {
                    blockUI();
                    var salary_component = $('#monthly_salary_component').val();
                    var salary_component_name = $('#monthly_salary_component option:selected').text();
                    var department = $('#monthly_departments').val();
                    $.ajax({
                        url: "{{url('get_emp_monthly_comp')}}",
                        type: 'GET',
                        data: {
                            salary_component,
                            department,
                            employment_status
                        },
                        success: function(response) {
                            $.unblockUI();
                            // console.log(response);
                            if(response.code == 200){
                                $("#emp_month_comp").empty();
                                $('#emp_month_comp_save').removeClass('d-none');
                                var html = '';
                                html += `
                                    <div class="table-responsive" style="overflow: auto;max-height: 400px;">
                                        <table class="table table-bordered mb-0">
                                            <thead style="position: sticky;top: 0;z-index: 1;">
                                                <tr>
                                                    <th>Employees</th>
                                                    <th>Employee Code</th>
                                                    <th>Employment Status</th>
                                                    <th>Salary Component</th>
                                                    <th>Amount</th>
                                            </thead>
                                                <tbody>`;
                                                if(response.data)
                                                {
                                                    $.each(response.data, function(key, employee){
                                                        var middleName = employee.middle_name != null ? employee.middle_name : '';
                                                        var lastName = employee.last_name != null ? employee.last_name : '';
                                                        html += `<tr>
                                                            <td class="text-nowrap">${employee.first_name} ${middleName} ${lastName}</td>
                                                            <td class="text-nowrap">${employee.employee_code}</td>
                                                            <td class="text-nowrap">${employee.employment}</td>
                                                            <td class="text-nowrap">${salary_component_name}</td>
                                                            <td style="padding: 0px !important;">
                                                                <input type="hidden" name="employee_id[]" value="${employee.id}">
                                                                <input type="hidden" name="salary_component" value="${salary_component}">
                                                                <input type="text" class="form-control" name="amount[${employee.id}][]" value=${employee.amount}  style="border: 0px">
                                                            </td>
                                                        </tr>`;
                                                    });
                                                }
                                                else{
                                                    html += `<tr><td colspan="2" class="text-center">No record found!</td></tr>`
                                                }
                                            html += `</tbody>
                                            </table>
                                        </div>`
                                $("#emp_month_comp").append(html);
                            }
                            else if(response.code == 404){
                                Toast.fire({
                                    icon: 'error',
                                    title: 'No Record Found!'
                                });
                            }
                            else{
                                Toast.fire({
                                    icon: 'error',
                                    title: 'All Fields are Required!'
                                });
                            }
                        }
                    });
                }
            });

            // Store Monthly Employee Component
            $('#employee_monthly_comp_form').submit(function(e){
                blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'employee_month_comp');
                $.ajax({
                    url: "{{route('salarySetup.store')}}",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Component has been Updated Successfully!'
                            })
                        }
                    }
                });
            })

            // Component by Month
            $('#component_by_month_apply').on('click',function(e){
                e.preventDefault();
                @if (env('COMPANY') == 'JSML')
                    var department_id = $('#section_1').val();
                @else
                    var department_id = $('#byMonthDepartment').val();
                @endif
                var suppress = $('#suppress').prop("checked");

                if(suppress){
                   var suppress_zero=$('#suppress').val();
                }
                var employee_Filter = $('#employee_Filter').val();
                var employment_status = $('#employement_status').val();
                var month = $('#month').val();
                var salary_component = $('#salaryComponent').val();
                var calculationGroup = $('#calculationGroup').val();
                $('#month_salary_component').val(salary_component);
                $('#component_month').val(month);
                $('#deducationGroup').val(calculationGroup);
                $('#component_save').show();
                if(department_id != '')
                {
                    blockUI();
                    $.ajax({
                        url: "{{url('salarySetup')}}" + "/" + department_id,
                        type: "get",
                        data: {
                            department_id: department_id,
                            suppress_zero: suppress_zero,
                            employee_Filter: employee_Filter,
                            employment_status: employment_status,
                            month: month,
                            salary_component: salary_component,
                            calculationGroup: calculationGroup,
                            type: 'component_by_month'
                        },
                        success: function(response) {
                            $.unblockUI();
                            $("#component_by_month_append").empty();
                            $('#component_save').removeClass('d-none');
                            var html = '';
                            html += `
                                <div class="table-responsive" style="overflow: auto;max-height: 400px;">
                                    <table class="table table-bordered mb-0">
                                        <thead style="position: sticky;top: 0;z-index: 1;">
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Employee Code</th>
                                                <th>Employee</th>
                                                <th>Designation</th>
                                                <th>Component</th>
                                                <th>Amount</th>
                                                `;
                                                // $.each(components, function(index, component){
                                                //     html += `<th>
                                                //     <input type="hidden" name="salary_components[]" value="${component.id}">
                                                //     ${component.name}
                                                //     </th>`;
                                                // })
                                            html += `</tr>
                                        </thead>
                                            <tbody>`;
                                            if(response.employees)
                                            {
                                                $.each(response.employees, function(key, employee){
                                                    var middleName = employee.middle_name != null ? employee.middle_name : '';
                                                    var lastName = employee.last_name != null ? employee.last_name : '';
                                                    html += `<tr>
                                                            <td>${employee.employee_id}</td>
                                                            <td>${employee.employee_code}</td>
                                                            <td class="text-nowrap">${employee.first_name + ' ' + middleName + ' ' + lastName}
                                                            <input type="hidden" name="employee_id[]" value="${employee.id}">
                                                            </td>
                                                            <td>${employee.designation}</td>
                                                            <td>${employee.component_name}</td>
                                                            <td>
                                                                <input type="text" class="form-control" name="amount[${employee.id}][]" value=${employee.amount}  style="border: 0px">
                                                            </td>
                                                        </tr>`;
                                                    //     $.each(components, function(index, comp){
                                                    //         $.each(response.data[employee.id][comp.id], function(index2, item){
                                                    //             var value = item === null ? 0.00 : item.amount;
                                                    //             html += `<td style="padding: 0px !important;">
                                                    //                     <input type="text" class="form-control" name="sc_amount[${employee.id}][${comp.id}][]" value="${value}" style="border: 0px">
                                                    //             </td>`;
                                                    //         });
                                                    //     })
                                                    // html += `</tr>`;
                                                });
                                            }
                                            else{
                                                html += `<tr><td colspan="6" class="text-center">No record found!</td></tr>`
                                            }
                                        html += `</tbody>
                                        </table>
                                    </div>`
                            $("#component_by_month_append").append(html);
                        },
                    })
                }
            });

            $('#component_by_month_form').submit(function(e){
                blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('type' , 'component_by_month');
                $.ajax({
                    url: "{{route('salarySetup.store')}}",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Component has been Updated Successfully!'
                            })
                            $('#component_by_month_append').empty();
                            $('#component_save').addClass('d-none');
                        }
                    }
                });
            })

            // Multiple Component by Month
            $('#multiple_component_by_month_apply').on('click',function(e){
                e.preventDefault();
                var department_id = $('#MultipleMonthDepartment').val();
                var employee_Filter = $('#multiple_employee_Filter').val();
                var employment_status = $('#multiple_employement_status').val();
                var month = $('#multiple_month').val();
                var salary_component = $('#multipleComponents').val();
                var calculationGroup = $('#MultipleCalculationGroup').val();
                $('#multiple_month_salary_component').val(salary_component);
                $('#multiple_component_month').val(month);
                $('#multiple_deducationGroup').val(calculationGroup);
                $('#multiple_component_save').show();
                if(department_id != '')
                {
                    blockUI();
                    $.ajax({
                        url: "{{url('salarySetup')}}" + "/" + department_id,
                        type: "get",
                        data: {
                            department_id: department_id,
                            employee_Filter: employee_Filter,
                            employment_status: employment_status,
                            month: month,
                            salary_component: salary_component,
                            calculationGroup: calculationGroup,
                            type: 'multiple_component_by_month'
                        },
                        success: function(response) {
                            $.unblockUI();
                            $("#multiple_component_by_month_append").empty();
                            $('#multiple_component_save').removeClass('d-none');
                            var html = '';
                            html += `
                                <div class="table-responsive" style="overflow: auto;max-height: 400px;">
                                    <table class="table table-bordered mb-0">
                                        <thead style="position: sticky;top: 0;z-index: 1;">
                                            <tr>
                                                <th class="sticky-column1">Employee ID</th>
                                                <th class="sticky-column2">Employee Code</th>
                                                <th class="sticky-column3">Employee</th>
                                                <th class="sticky-column4">Designation</th>
                                                `;
                                                $.each(response.salary_components, function(index, component){
                                                    html += `<th>
                                                    <input type="hidden" name="salary_components[]" value="${component.id}">
                                                    ${component.name}
                                                    </th>`;
                                                })
                                            html += `</tr>
                                        </thead>
                                            <tbody>`;
                                            if(response.employees)
                                            {
                                                $.each(response.employees, function(key, employee){
                                                    var middleName = employee.middle_name != null ? employee.middle_name : '';
                                                    var lastName = employee.last_name != null ? employee.last_name : '';
                                                    html += `<tr>
                                                            <td class="sticky-td-column1">${employee.employee_id}</td>
                                                            <td class="sticky-td-column2">${employee.employee_code}</td>
                                                            <td class="text-nowrap sticky-td-column3">${employee.first_name + ' ' + middleName + ' ' + lastName}
                                                            <input type="hidden" name="employees[]" value="${employee.id}">
                                                            </td>
                                                            <td class="sticky-td-column4">${employee.designation}</td>`;
                                                        $.each(response.salary_components, function(index, comp){
                                                            $.each(response.data[employee.id][comp.id], function(index2, item){
                                                                var value = item === null ? 0.00 : item.amount;
                                                                html += `<td style="padding: 0px !important;">
                                                                        <input type="text" class="form-control" name="sc_amount[${employee.id}][${comp.id}][]" value="${value}" style="border: 0px">
                                                                </td>`;
                                                            });
                                                        })
                                                    html += `</tr>`;
                                                });
                                            }
                                            else{
                                                html += `<tr><td colspan="6" class="text-center">No record found!</td></tr>`
                                            }
                                        html += `</tbody>
                                        </table>
                                    </div>`
                            $("#multiple_component_by_month_append").append(html);
                        },
                    })
                }
            });
        });
        $("#multiple_component_by_month_form").on("submit", function (e) {
                blockUI();
                e.preventDefault();
				var formData = new FormData(this);
                formData.append('type' , 'multiple_component_by_month');
                $.ajax({
                    url: "{{route('salarySetup.store')}}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $.unblockUI();
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Component has been Updated Successfully!'
                            })
                        }
                        
                    }
                });
            });


        // Salary Component Types DataTable
        function salary_component_types()
        {
            $('#salary_component_types').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('salarySetup.index') }}",
                    data: {
                        type: "salaryComponentTypes"
                    }
                },
                columns: [
					{
                        data: 'responsive_id'
                    }, 
                    {
                        data : 'DT_RowIndex',
                        name : 'DT_RowIndex',
                        searchable: false,
                    }, 
                    {
                        data: 'code',
                        name: 'code'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [
					{
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function (data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=salary_component_type_edit('+full.id+')>' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'+
                                '<a href="javascript:;" onclick="delete_salary_component_type('+full.id+')">' +
                                feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
					[2, 'asc']
				],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Salary Component Types',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Salary Component Types',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Salary Component Types',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Salary Component Types',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Salary Component Types',
                            action: newexportaction,
                                exportOptions: { columns: [0,1] }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#salary_component_type_add_modal'
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
				responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('#salary-component-types div.head-label').html('<h6 class="mb-0">List of Salary Component Types</h6>');
        }

        // Salary Components DataTable
        function salary_components()
        {
            $('#salary_components').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                destroy: true,
                ajax: {
                    url: "{{ route('salarySetup.index') }}"
                },
                columns: [
                    { 
                        data: 'responsive_id' 
                    },
                    {
                        data : 'DT_RowIndex',
                        name : 'DT_RowIndex',
                        searchable: false,
                    }, 
                    {
                        data: 'name',
                        name : 'salarycomponent.name',
                    },
                    {
                        data: 'type_name',
                        name : 'salarycomponenttype.name',
                    },
                    {
                        data: 'tax_code',
                        name : 'taxes.code',
                    },
                    {
                        data: 'details',
                        name : 'salarycomponent.details',
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        searchable: false,
                        orderable: false,
                        title: 'Actions',
                        render: function (data, type, full, meta) {
                            var btn;
                            btn = '<a href="javascript:;" class="item-edit" onclick=salary_component_edit('+full.id+')>' +
                                    feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                    '</a>'+
                                    '<a href="javascript:;" onclick="delete_salary_component('+full.id+')">' +
                                    feather.icons['trash-2'].toSvg({ class: 'font-medium-4 text-danger' }) +
                                    '</a>';
                                    if(full.is_monthly == 1){
                                        btn += '<a href="{{url("clear_salary_comp")}}'+"/"+full.id+'" title="Clear Salary Component">' +
                                        feather.icons['file-minus'].toSvg({ class: 'font-medium-4 me-1 text-warning' }) +
                                        '</a>';
                                    }
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Salary Component',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Salary Component',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Salary Component',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Salary Component',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Salary Component',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
					{
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-target': '#salary_component_add_modal',
                            'data-bs-toggle': 'modal',
                            'data-bs-backdrop': 'static',
                            'data-bs-keyboard': 'false',
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });

            $('#salary-components div.head-label').html('<h6 class="mb-0">List of Salary Components</h6>');
        }

        // Salary Component Type Edit
        function salary_component_type_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('salarySetup')}}" + "/" + id + "/edit",
                type: "get",
                data: {type : 'salary_component_type'},
                success: function (response) {
                    $("#edit_code").val(response.code);
                    $("#edit_name").val(response.name);
                    $("#salary_component_type_edit_modal").modal("show");
                },
            });
        }

        // Delete Salary Component Type
        function delete_salary_component_type(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                                $.ajax({
                                url: "salarySetup/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}",
                                    type: 'salary_component_type'
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else if(response.code == 300)
                                    {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        $('#salary_component_types').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Salary Component Type has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Salary Component Edit
        function salary_component_edit(id) {
            rowid = id;
            $.ajax({
                url: "{{url('salarySetup')}}" + "/" + id + "/edit",
                type: "get",
                data: {type : 'salary_component'},
                success: function (response) {
                    $("#edit_component_name").val(response.name);
                    $("#edit_componentType").val(response.componentType).select2();
                    $("#edit_tax_id").val(response.tax_id).select2();
                    $('#edit_details').text(response.details);
                    $("#edit_applied_month").val(response.applied_month);
                    $('#edit_is_edit').prop('checked', false);
                    $('#edit_is_monthly').prop('checked', false);
                    if(response.is_edit == 1){
                        $('#edit_is_edit').prop('checked', true);
                    }
                    if(response.is_monthly == 1){
                        $('#edit_is_monthly').prop('checked', true);
                    }
                    $("#salary_component_edit_modal").modal("show");
                },
            });
        }

        // Delete Salary Component
        function delete_salary_component(id)
        {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function(){
                                $.ajax({
                                url: "salarySetup/" + id,
                                type: "DELETE",
                                data : {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function (response) {
                                    if(response.error_message){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    else if(response.code == 300)
                                    {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        $('#salary_components').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Salary Component has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        function getdepartmentEmployees(object,employee_id,type=''){
            if($(object).val().length>0){
                var optVal = $(object).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        type: type,
                        department_id: optVal,
                    },
                    success: function(response) {
                        $(employee_id).empty();
                        $(employee_id).html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $(employee_id).append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id +' - '+value.employee_code +' - '+ value.first_name +' '+ value.last_name + ' - ' + value.designation)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            }
        }
    </script>
@endsection