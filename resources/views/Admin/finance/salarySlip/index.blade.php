@extends('Admin.layouts.master')
@section('title', 'Salary Slip')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Salary Slip</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Finance</a>
                            </li>
                            <li class="breadcrumb-item active">Salary Slip
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="multiple-column-form">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Salary Slip Form</h4>
                </div>
                <div class="card-body">
                    <form class="form" action="{{route('salary_slip.create')}}" method="GET">
                        <div class="row">
                            <div class=" col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="salary">Salary</label>
                                    <select name="salary" id="salary" class="select2 form-select" required>
                                        <option value="">Select Salary</option>
                                        @foreach ($payrolls as $payroll)
                                            <option value="{{$payroll->id}}">{{$payroll->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class=" col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="employment">Select Employee</label>
                                    <select name="employee" id="employment" class="select2 form-select">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class=" col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="date_start">Date</label>
                                    <input type="text" name="date" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary me-1">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
    @if(Session::has('message'))
        <script>
            Toast.fire({
                icon: 'error',
                title: "{!! Session::get('message') !!}"
            });
        </script>
    @endif
    <script>
        $(document).ready(function(){
            $("#salary").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET", 
                        url: "{{ url('salary_slip') }}" + "/" + optVal,
                        success: function(response) {
                            $('#employment').empty();
                            $('#employment').html('<option value="">Select Employee</option>'); 
                            $.each(response, function(index, value) {
                                $('#employment').append(
                                    $('<option></option>').val(value.id).html(value.employee_id +'-'+ value.employee_code +'-'+ value.first_name +' '+value.middle_name +' '+ value.last_name +'-'+ value.department +'-'+ value.desigination)
                                );
                            });
                            // $('#employment').prop("disabled", false);
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employment').empty();
                }
            });
        });
    </script>
@endsection