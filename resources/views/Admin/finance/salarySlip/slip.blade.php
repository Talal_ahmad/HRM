@extends('Admin.layouts.master')
@section('title', 'Salary Slip')
@section('style')
<style>
    table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid black;
    }
    tbody tr{
        border: 1px solid black;
    }
</style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Salary Slip</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Salary Slip
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12 text-end mb-1">
                <button class="btn btn-danger" onclick="printDiv()"><i class="fas fa-print" style="margin-right: 2px"></i></i>Print</button>
            </div>
            <div class="col-12" id="slip">
                @foreach ($employee_info as $employee)    
                    <div class="card p-2" id="{{$employee->employee}}">
                        <h3 class="text-center mb-2">Salary Slip</h3>
                        <table>
                            <thead>
                                <tr>
                                    <th>Employee ID: {{$employee->employee_code}}</th>
                                    <th>Employee Name: {{$employee->first_name}} {{$employee->last_name}}</th>
                                </tr>
                                <tr>
                                    <th>Department: {{$employee->department}}</th>
                                    <th>Designation: {{$employee->designation}}</th>
                                </tr>
                                <tr>
                                    <th>Salary Month: {{$employee->month_salary}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($add_cols_arr[$employee->employee] as $key => $value)
                                    <tr>
                                        <td>{{$key}}</td>
                                        <td>{{$value}}</td>
                                    </tr>
                                @endforeach
                                @foreach ($deduct_cols_arr[$employee->employee] as $key2 => $value)
                                    <tr>
                                        <td>{{$key2}}</td>
                                        <td>{{$value}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="text-center">Net</td>
                                    @if (!in_array(63 , $add_cols_arr[$employee->employee]) && $emp_dept == 4)
                                        @if (env('COMPANY') == 'CLINIX')
                                            <td>{{$add_cols_arr[$employee->employee]['Net Salary (For Warehouse)']}}</td>
                                        @else
                                            <td>{{$add_cols_arr[$employee->employee]['Net Salary']}}</td>
                                        @endif
                                    @else
                                        @if (env('COMPANY') == 'CLINIX')
                                            <td>{{$add_cols_arr[$employee->employee]['net salary after deduction new']}}</td>
                                        @else
                                            <td>{{$add_cols_arr[$employee->employee]['Net Salary']}}</td>
                                        @endif
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                        <p class="text text-danger mt-1">This is computer generated document and does not require any stump or signature</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script>
        function printDiv(){
            var printContents = document.getElementById('slip').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
		}
    </script>
@endsection