@extends('Admin.layouts.master')
@section('title', 'Salary Slip')
@section('style')
<style>
    table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid black;
    }
    table tr,td,th{
        border-style: none;
        color: black;

    }
    #net_salary{
        height: auto;
    }
    .txt-rgt{
        text-align: right;
    }
</style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Salary Slip</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Salary Slip
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card p-2">
                        <form action="{{route('salary_slip.index')}}" method="GET">
                            <div class="row mb-1">
                                <div class="col-md-6 col-12">
                                    <label for="payroll" class="form-label">Payrolls:</label>
                                    <select name="payroll" id="payroll" class="select2 form-select filter" data-column="9" data-placeholder="Select Payroll" required>
                                        <option value="">Select Department</option>
                                        @foreach ($all_payrolls as $item)
                                            <option value="{{$item->id}}" {{$item->id == request('payroll') ? 'selected' : ''}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 col-12">
                                    <label for="employee" class="form-label">Employees</label>
                                    <select name="employee" id="employee" class="select2 form-select filter" data-column="8" data-placeholder="Select Employee">

                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-12">
                                    <a href="{{url('salary_slip')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                    <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                </div>
                            </div>
                        </form>
                        <div class="col-xs-12 col-12 text-end">
                            <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">Print</button>
                        </div>
                    </div>
                </div>
                @if (isset($employees) && count($employees) > 0)
                    <div id="div_to_print">
                        @foreach ($employees as $employee)
                            <div class="col-12">
                                <div class="card p-1">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-4">
                                        @if (!empty($company_setup->logo))
                                            <img src="{{asset('images/company_logo/'.$company_setup->logo)}}" height="60" alt="Logo">
                                            @else
                                            <img src="{{asset('images/company_logo/')}}" alt="Logo">
                                        @endif
                                        </div>
                                        @if (env('COMPANY') == 'RoofLine')
                                            <div class="col-lg-8 col-md-4 col-4 mb-1">
                                                <h4 style="font-weight: bold;margin: 10px 0px 0px 0px;text-align:center;">{{$employee->department}}</h4>
                                                <h4 style="font-weight: bold;margin: 15px 0px 0px 0px;text-align:center;">Salary Slip For Month: {{date('F', strtotime($payroll->date_start))}} {{date('Y', strtotime($payroll->date_start))}}</h4>
                                            </div>
                                        @else
                                            <div class="col-lg-8 col-md-4 col-4 mb-1">
                                                <h4 style="font-weight: bold;margin: 10px 0px 0px 0px;text-align:center;">{{$company_setup->company_name}}</h4>
                                                <h4 style="font-weight: bold;margin: 15px 0px 0px 0px;text-align:center;">Salary Slip For Month: {{date('F', strtotime($payroll->date_start))}} {{date('Y', strtotime($payroll->date_start))}}</h4>
                                            </div>
                                        @endif
                                        <div class="col-lg-2 col-md-4 col-4 mb-1">
                                            <p id="date">{{date('Y-m-d')}}</p> 
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-8">
                                            <table class="table table-bordered mb-1">
                                                <tr>
                                                    <td style="padding: 0;">Name</td>
                                                    <td style="padding: 0;">{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0;">CNIC</td>
                                                    <td style="padding: 0;">{{$employee->nic_num}}</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0;">Emloyee_ID</td>
                                                    <td style="padding: 0;">{{$employee->employee_id}}</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0;">Emloyee Code</td>
                                                    <td style="padding: 0;">{{$employee->employee_code}}</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0;">Department</td>
                                                    <td style="padding: 0;">{{$employee->department}}</td>
                                                </tr>
                                                <tr>
                                                    @if (!empty($total_days)) 
                                                        <td style="padding: 0;">Days</td>  
                                                        <td style="padding: 0;">{{number_format(getColumnValue($payroll->id, $total_days->id,$employee->id),1)}}</td>
                                                    @else
                                                        <td>-</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0;">Designation</td>
                                                    <td style="padding: 0;">{{$employee->desigination}}</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0;">Bank</td>
                                                    <td style="padding: 0;">{{$employee->bank_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0;">Account</td>
                                                    <td style="padding: 0;">{{$employee->account_number}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        @if (env('COMPANY') != 'JSML')
                                        <div class="col-4">
                                            <div class="text-center mb-1">
                                                <a href="#">
                                                    @if (file_exists(public_path('images/employees/' . $employee->image)))
                                                        <img src="{{asset('images/employees/' . $employee->image)}}" id="account-upload-img" class="me-50" alt="profile image" height="95" width="95" />
                                                    @else
                                                        <img src="{{asset('images/employees/default_employee.png')}}" id="account-upload-img" class="me-50" alt="profile image" height="95" width="95" />
                                                    @endif
                                                </a>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-8">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th style="background:#F3F2F7;padding: 2px 20px;font-size: 12px" colspan="2" class="text-center">
                                                            Payments
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (env('COMPANY') != 'Prince HRM')
                                                        <tr>
                                                        @if (env('COMPANY') == 'JSML')
                                                            <td style="padding: 2px 20px;">Gross Salary</td>
                                                        @else
                                                            <td style="padding: 2px 20px;">Basic Salary</td>
                                                        @endif
                                                            <td class="text-end" style="padding: 1px 20px;">{{number_format(getColumnValue($payroll->id,$payslip->basic_salary,$employee->id))}}</td>
                                                        </tr>
                                                    @endif
                                                    <tr>
                                                        <th style="padding: 2px 20px;background:#F3F2F7;text-align: center;font-size:12px" colspan="2">
                                                            Allowances
                                                        </th>
                                                    </tr>
                                                    @foreach ($allownaces_columns as $allownce)
                                                    <tr>
                                                        <td style="padding: 1px 20px;">{{$allownce->name}}</td>
                                                        <td style="padding: 1px 20px;" class="text-end">{{number_format(getColumnValue($payroll->id,$allownce->id,$employee->id),2)}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-4">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th style="background:#F3F2F7;padding: 2px 20px;font-size:12px;text-align:center;" colspan="2">
                                                            Deductions
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($deductions_columns as $deduction)
                                                    <tr>
                                                        <td style="padding: 1px 20px;">{{$deduction->name}}</td>
                                                        <td class="pr-2 text-end" style="padding: 1px 20px;">{{number_format(getColumnValue($payroll->id,$deduction->id,$employee->id))}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @if (env('COMPANY') != 'Prince HRM')
                                        <div class="col-8">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th style="padding: 1px 20px;">Gross Salary</th>
                                                        <td style="padding: 1px 20px; font-weight:bold;" class="text-end">{{number_format(getColumnValue($payroll->id,$payslip->gross_salary,$employee->id))}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-4">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        @php
                                                            // dd($payslip->net_salary);
                                                        @endphp
                                                        <th style="padding: 1px 20px;">Net Salary</th>
                                                        <td style="padding: 1px 20px; font-weight:bold;" class="text-end">{{number_format(getColumnValue($payroll->id,$payslip->net_salary,$employee->id))}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="padding: 1px 20px; font-weight:bold;">Rupees: {{numberTowords(getColumnValue($payroll->id,$payslip->net_salary,$employee->id))}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        @endif
                                        <p class="text-center">
                                            This is system generated document does not require signature.
                                        </p>
                                    </div>
                                    {{-- <div class="row mb-1">
                                        <div class="col-8">
                                            <table class="table">
                                                <tr>
                                                    <td>Department</td>
                                                    <td>{{$employee->department}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Cell No</td>
                                                    <td>{{$employee->mobile_phone}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Designation</td>
                                                    <td>{{$employee->desigination}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-4">
                                            <table id="net_salary" class="table">
                                                <tr rowspan="3">
                                                    <td>Net</td>
                                                    <td>{{number_format(getColumnValue($payroll->id,$net_Salary->id,$employee->id),2)}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            @endforeach
                    </div>
                @else
                <div class="card p-2">
                    <div class="text-center">No Record Found!</div>
                </div>
                @endif
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        n =  new Date();
        m = n.getMonth() + 1;
        d = n.getDate();
        y = n.getFullYear();
        // document.getElementById("date").innerHTML = m + "/" + d + "/" + y;
        $(document).ready(function(){
            $("#payroll").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('salary_slip') }}" + "/" + optVal,
                        success: function(response) {
                            $('#employee').attr('disabled', false);
                            $('#employee').empty();
                            $('#employee').html('<option value="">Select Employee</option>'); 
                            $.each(response, function(index, value) {
                                $('#employee').append(
                                    $('<option></option>').val(value.id).html(value.employee_id +'-'+ value.employee_code +'-'+ value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.desigination)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employee').empty();
                }
            });
        });
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open();
            WinPrint.document.write(`<!DOCTYPE html>`);
            WinPrint.document.write(`<head>`);
            WinPrint.document.write(`<link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">`);
            WinPrint.document.write(`<style>
                        *{  
                            font-size: 10px;
                            padding: 0;
                            color: black;
                        }
                        table tr{
                            border: 1px solid black;
                        }
                        `);
            WinPrint.document.write(`</style>`);
            WinPrint.document.write(`</head><body>`);
            WinPrint.document.write(`${divToPrint.innerHTML}`);
            WinPrint.document.write(`</body></html>`);
            WinPrint.document.close();
            setTimeout(function(){
                WinPrint.print();
                WinPrint.close();
            },1000);
            WinPrint.focus();
        }
    </script>
@endsection