@extends('Admin.layouts.master')
@section('title', 'Company Payroll')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Company Payroll</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashbaord</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Company Payroll
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header row"></div>
        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Payment Frequency</th>
                                        <th>Calculation Group</th>
                                        <th>Currency</th>
                                        <th>Created By</th>
                                        <th>Created At</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!--Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add Company Payroll</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="employee">Employees</label>
                                            <select name="employee" id="employee" class="select2 form-select" data-placeholder="Select Employee" required>
                                                <option value=""></option>
                                                @foreach (employees() as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->middle_name." ".$employee->last_name . " - " . $employee->desigination}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="pay_frequency">Pay Frequency</label>
                                            <select name="pay_frequency" id="pay_frequency" class="select2 form-select" data-placeholder="Select Pay Frequency" required>
                                                <option value=""></option>
                                                @foreach ($payFrequency as $frequency)
                                                    <option value="{{ $frequency->id }}">{{ $frequency->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="currency">Currency</label>
                                            <select name="currency" id="currency" class="select2 form-select" data-placeholder="Select Currency" required>
                                                <option value=""></option>
                                                @foreach ($currencies as $currency)
                                                    <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="deduction_group">Calculation Group</label>
                                                <select name="deduction_group" id="deduction_group" class="select2 form-select" data-placeholder="Select Calculation Group" required>
                                                    <option value=""></option>
                                                    @foreach ($calculationGroup as $group)
                                                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="deduction_exemptions">Calculation Exemptions</label>
                                            <select name="deduction_exemptions" id="deduction_exemptions" class="select2 form-select"
                                                data-placeholder="Select Calculation Exemptions">
                                                <option value=""></option>
                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="deduction_allowed">Calculation Assigned</label>
                                            <select name="deduction_allowed" id="deduction_allowed" class="select2 form-select"
                                                data-placeholder="Select Calculation Assigned">
                                                <option value=""></option>
                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End Add Modal -->

            <!--start edit Modal -->
            <div class="modal fade text-start" id="edit_modal" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit Company Payroll</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_employee">Employee</label>
                                            <select name="employee" id="edit_employee" class="select2 form-select" data-placeholder="Select Employee" required>
                                                <option value=""></option>
                                                @foreach (employees() as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->employee_id." - ".$employee->employee_code." - ".$employee->first_name." ".$employee->middle_name." ".$employee->last_name . " - " . $employee->desigination}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_pay_frequency">Pay Frequency</label>
                                            <select name="pay_frequency" id="edit_pay_frequency" class="select2 form-select" data-placeholder="Select Pay Frequency" required>
                                                <option value=""></option>
                                                @foreach ($payFrequency as $frequency)
                                                    <option value="{{ $frequency->id }}">{{ $frequency->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_currency">Currency</label>
                                            <select name="currency" id="edit_currency" class="select2 form-select"
                                                data-placeholder="Select Currency" required>
                                                <option value=""></option>
                                                @foreach ($currencies as $currency)
                                                    <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_deduction_group">Calculation Group</label>
                                            <select name="deduction_group" id="edit_deduction_group" class="select2 form-select"
                                                data-placeholder="Select Calculation Group" required>
                                                <option value=""></option>
                                                @foreach ($calculationGroup as $group)
                                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_deduction_exemptions">Calculation Exemptions</label>
                                            <select name="deduction_exemptions" id="edit_deduction_exemptions" class="select2 form-select"
                                                data-placeholder="Select Calculation Exemptions">
                                                <option value=""></option>
                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_deduction_allowed">Calculation Assigned</label>
                                            <select name="deduction_allowed" id="edit_deduction_allowed" class="select2 form-select"
                                                data-placeholder="Select Calculation Assigned">
                                                <option value=""></option>
                                                @foreach ($calculation_Exemptions_Assigned as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                                <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End edit Modal -->
        </div>
    </section>

@endsection
@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('companyPayroll.index') }}",
                },
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'DT_RowIndex',
                        name : 'DT_RowIndex',
                        searchable: false,
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id',
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code',
                    },
                    {
                        render: function (data, type, full, meta) {
                            var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                            var lastName = full['last_name'] != null ? full['last_name'] : '';
                            return full['first_name'] + ' ' + middleName+ ' ' + lastName;
                        },
                        searchable: false
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'payFrequency',
                        name: 'payfrequency.name',
                    },
                    {
                        data: 'calculationGroup',
                        name: 'deductiongroup.name',
                    },
                    {
                        data: 'currency',
                        name: 'currencytypes.code',
                    },
                    {
                        data: 'created_by',
                        searchable: false,
                    },
                    {
                        data: 'formatted_created_at',
                        name: 'payrollemployees.created_at',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        searchable: false,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Company Payrolls</h6>');

            $("#add_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{ route('companyPayroll.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.code == 419){
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            })
                        } 
                        else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#add_modal").modal("hide");
                            document.getElementById("add_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Payroll has been Added Successfully!'
                            })
                        }

                    }
                });
            });

            $("#edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "{{ url('companyPayroll') }}" + "/" + rowid,
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.code == 419){
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            })
                        }  
                        else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_modal").modal("hide");
                            document.getElementById("edit_form").reset();
                            $(".select2").val('').trigger('change')
                            $('#dataTable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Payroll has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
        });

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('companyPayroll') }}" + "/" + id + "/edit",
                type: "get",
                success: function(response) {
                    $("#edit_employee").val(response.employee).select2();
                    $("#edit_pay_frequency").val(response.pay_frequency).select2();
                    $("#edit_currency").val(response.currency).select2();
                    $("#edit_deduction_group").val(response.deduction_group).select2();
                    $("#edit_deduction_exemptions").val(response.deduction_exemptions).select2();
                    $("#edit_deduction_allowed").val(response.deduction_allowed).select2();
                    $("#edit_modal").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "companyPayroll/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } 
                                    else {
                                        $('#dataTable').DataTable().ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Company Payroll has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
