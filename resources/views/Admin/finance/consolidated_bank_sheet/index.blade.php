@extends('Admin.layouts.master')
@section('title', 'Consolidated Bank Sheet')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Consolidated Bank Sheet</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Finance</a>
                            </li>
                            <li class="breadcrumb-item active">Consolidated Bank Sheet
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card mb-1">
                    <!--Search Form -->
                    <div class="card-body">
                        <form id="search_form" action="{{ route('consolidated_bank_sheet.index') }}">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <label class="form-label">Filter By Payroll:</label>
                                    <select name="payrollFilter[]" id="payrollFilter" class="select2 form-select" data-placeholder="Select Payroll" required multiple>
                                        <option value="">Select Payroll</option>
                                        @foreach ($payrolls as $payroll)
                                        <option value="{{$payroll->id}}">{{$payroll->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 text-end" style="margin-top: 10px">
                                    <a href="{{route('consolidated_bank_sheet.index')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                    <button type="submit" class="btn btn-primary mt-1">See Report</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @php
                $total_gross_amount = 0;
                $total_net_amount = 0;
                @endphp
                <div class="card">
                    <div class="table table-responsive">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Employee ID</th>
                                    <th>Employee Department</th>
                                    <th>Employee Name</th>
                                    <th>Designation</th>
                                    <th>Account Title</th>
                                    <th>Bank Name</th>
                                    <th>Account Number</th>
                                    <th>CNIC Number</th>
                                    <th>Mobile Number</th>
                                    {{-- <th>Gross Amount</th>
                                    <th>Amount</th> --}}
                                    @if (isset($payroll_columns) && count($payroll_columns) > 0)
                                        @foreach ($payroll_columns as $column)
                                            <th>{{$column->name}}</th>
                                        @endforeach
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @if (isset($employees) && count($employees) > 0)
                                @foreach ($employees as $key => $employee)
                                @php
                                $total_gross_amount += $employee->gross_amount;
                                $total_net_amount += $employee->amount;
                                @endphp
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$employee->employee_id}}</td>
                                    <td>{{$employee->employee_Department}}</td>
                                    <td>{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}</td>
                                    <td>{{$employee->designation}}</td>
                                    <td>{{$employee->accountName}}</td>
                                    <td>{{$employee->bankName}}</td>
                                    <td>{{$employee->accountNum}}</td>
                                    <td>{{$employee->nic_num}}</td>
                                    <td>{{$employee->mobile_phone}}</td>
                                    {{-- <td>{{$employee->gross_amount}}</td>
                                    <td>{{$employee->amount}}</td> --}}
                                    @foreach ($payroll_columns as $col)
                                        <td class="text-end">{{consolidatedpayrollColumnAmount(request()->payrollFilter,$employee->id,$col->id,0,$col->round_off)}}</td>
                                    @endforeach
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center" colspan="10">No Record Found</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                    <td class="fw-bolder">{{$total_gross_amount}}</td>
                                    <td class="fw-bolder">{{$total_net_amount}}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({

            ordering: true,
            "columnDefs": [{
                // For Responsive
                className: 'control',
                orderable: false,
                targets: 0
            }],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({
                    class: 'font-small-4 me-50'
                }) + 'Export',

                buttons: [{

                        extend: 'print',
                        text: feather.icons['printer'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Print',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Csv',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({
                            class: 'font-smaller me-50'
                        }) + 'Pdf',
                        className: 'dropdown-item',
                        orientation: 'landscape',
                        footer: true,
                        pageSize: 'A3',
                        customize: function(doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();
                            doc.pageMargins = [20, 60, 20, 30];
                            // Set the font size fot the entire document
                            doc.defaultStyle.fontSize = 12;
                            // Set the fontsize for the table header
                            doc.styles.tableHeader.fontSize = 10;
                            // Create a header
                            doc["footer"] = function(page, pages) {
                                return {
                                    columns: [{
                                            alignment: "left",
                                            text: ["Created on: ", {
                                                text: jsDate.toString()
                                            }]
                                        },
                                        {
                                            alignment: "right",
                                            text: [
                                                "page ",
                                                {
                                                    text: page.toString()
                                                },
                                                " of ",
                                                {
                                                    text: pages.toString()
                                                }
                                            ]
                                        }
                                    ],
                                    margin: 20
                                };
                            };
                            var objLayout = {};
                            objLayout["hLineWidth"] = function(i) {
                                return 0.5;
                            };
                            objLayout["vLineWidth"] = function(i) {
                                return 0.5;
                            };
                            objLayout["hLineColor"] = function(i) {
                                return "#aaa";
                            };
                            objLayout["vLineColor"] = function(i) {
                                return "#aaa";
                            };
                            objLayout["paddingLeft"] = function(i) {
                                return 4;
                            };
                            objLayout["paddingRight"] = function(i) {
                                return 4;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        // action: newexportaction,
                        exportOptions: {
                            columns: ':not(.not_include)',
                            // tfoot:true,
                        }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    }
                ],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            }],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0 ms-0">Consolidated Bank Sheet</h6>');
    });
</script>
@endsection