@extends('Admin.layouts.master')
@section('title', 'All Location Salary Report')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Location Wise Report</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Location Wise Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <!--Search Form -->
                            <div class="card-body">
                                <form id="search_form">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-label" for="monthFilter">Select Month:</label>
                                            <input type="month" id="monthFilter" class="form-control"  name="monthFilter" value="{{request('monthFilter')}}" required/>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">Location:</label>
                                            <select name="locationFilter" id="locationFilter" class="select2 form-select" data-placeholder="Select Location">
                                                <option value=""></option>
                                                @foreach ($locations as $location)
                                                    <option value="{{$location->work_station_id}}" {{$location->work_station_id == request('locationFilter') ? 'selected' : ''}}>{{$location->city}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">Department:</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department">
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{url('location_wise_report')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                            <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Location Code</th>
                                        <th>City</th>
                                        <th>Net Payable</th>
                                        <th>Signature</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($locations as $key => $location)
                                        @php
                                            $value = getLocationNetAmount($location->work_station_id,$payrolls,$net_column,$filter_department)
                                        @endphp
                                        @if (!empty($value->work_station_id) && !empty($value->total_amount))    
                                            <tr>
                                                <td></td>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$value->work_station_id}}</td>
                                                <td>{{$value->city}}</td>
                                                <td>{{number_format($value->total_amount)}}</td>
                                                <td></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th class="text-end">Total Amount:</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('#dataTable').DataTable({
                ordering: false,
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        targets: 0
                    },
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            footer: true,
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            footer: true,
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            footer: true,
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            footer: true,
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation : 'portrait',
                            pageSize : 'A4',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            footer: true,
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                },
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
        
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
        
                    // Total over all pages
                    total_amount = api.column(4).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    // Update footer
                    $( api.column(4).footer() ).html(
                        numberWithCommas(parseFloat(total_amount).toFixed(2))
                    );
                }
            });
            $('div.head-label').html('<h6 class="mb-0">All Location Salary Report</h6>');
        });
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>
@endsection