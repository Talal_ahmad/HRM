@extends('Admin.layouts.master')
@section('title', 'Department Wise Salary')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Department Wise Salary</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashbaord</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Department Wise Salary
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="active_summary-tab" data-bs-toggle="tab" href="#active_summary"
                            aria-controls="active_summary" role="tab" aria-selected="true">Department Wise Salary Summary</a>
                    </li>
                    @can('Department Wise Salary Summary History')
                    <li class="nav-item">
                        <a class="nav-link" id="history-tab" data-bs-toggle="tab" href="#history"
                            aria-controls="history" onclick="historyDataTable()" role="tab" aria-selected="false">Department Wise Salary Summary History</a>
                    </li>
                    @endcan
                </ul>
                <div class="card mb-1">
                    <div class="card-body">
                        <form id="search_form" action="{{route('department_wise_salary.index')}}">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <label class="form-label">Filter By Department:</label>
                                    <select name="departmentFilter" id="departmentFilter" class="select2 form-select"
                                        data-placeholder="Select Department">
                                        <option value="">Select Department</option>
                                        <option value="all">All Departments</option>
                                        @foreach (departments() as $department)
                                            <option value="{{ $department->id }}">{{ $department->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 col-12">
                                    <label class="form-label" for="date">Filter By Date</label>
                                    <input type="month" id="date" class="form-control" placeholder="Job Title" name="date"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-end">
                                    <a href="{{route('department_wise_salary.index')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                    <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-content">
                    {{-- Department Wise Summary --}}
                    <div class="tab-pane active" id="active_summary" aria-labelledby="active_summary-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card pb-2">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="summary_dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.No</th>
                                                        <th>Department\Branch</th>
                                                        <th>Net Salary</th>
                                                        <th>Gross Salary</th>
                                                        <th>Loan Total</th>
                                                        <th>Loan Deduction</th>
                                                        <th>Overtime Value</th>
                                                        <th>Incentive</th>
                                                        <th>Tax</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (isset($payroll_data) && count($payroll_data) > 0)
                                                        @php
                                                            $total_net_salary = 0;
                                                            $total_gross_amount = 0;
                                                            $total_loan_amount = 0;
                                                            $total_loan_deduction = 0;
                                                            $total_overtime_value = 0;
                                                            $total_incentive = 0;
                                                            $total_tax_amount = 0;
                                                        @endphp
                                                        @foreach ($payroll_data as $key => $data)
                                                            <tr>
                                                                <td>{{$key + 1}}</td>
                                                                <td>{{$data->title}}</td>
                                                                <td>{{!empty($data->net_salary) ? number_format($data->net_salary , 2) : 0}}</td>
                                                                <td>{{!empty($data->gross_amount) ? number_format($data->gross_amount , 2) : 0}}</td>
                                                                <td>{{!empty($data->loan_amount) ? number_format($data->loan_amount , 2) : 0}}</td>
                                                                <td>{{!empty($data->loan_deduction) ? number_format($data->loan_deduction , 2) : 0}}</td>
                                                                <td>{{!empty($data->overtime_value) ? number_format($data->overtime_value , 2) : 0}}</td>
                                                                <td>{{!empty($data->incentive) ? number_format($data->incentive , 2) : 0}}</td>
                                                                <td>{{!empty($data->tax_amount) ? number_format($data->tax_amount , 2) : 0}}</td>
                                                            </tr>
                                                            @php
                                                                $total_net_salary += $data->net_salary;
                                                                $total_gross_amount += $data->gross_amount;
                                                                $total_loan_amount += $data->loan_amount;
                                                                $total_loan_deduction += $data->loan_deduction;
                                                                $total_overtime_value += $data->overtime_value;
                                                                $total_incentive += $data->incentive;
                                                                $total_tax_amount += $data->tax_amount;
                                                            @endphp
                                                        @endforeach
                                                        <tfoot>
                                                            <tr>
                                                                <td></td>
                                                                <td class="fw-bolder">Total Net Salary</td>
                                                                <td class="fw-bolder">{{number_format($total_net_salary , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_gross_amount , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_loan_amount , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_loan_deduction , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_overtime_value , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_incentive , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_tax_amount , 2)}}</td>
                                                            </tr>
                                                        </tfoot>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    {{-- Department Wise Summary History --}}
                    <div class="tab-pane" id="history" aria-labelledby="history-tab" role="tabpanel">
                        <section id="basic-datatable">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="history_dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.No</th>
                                                        <th>Department\Branch</th>
                                                        <th>Net Salary</th>
                                                        <th>Gross Salary</th>
                                                        <th>Loan Total</th>
                                                        <th>Loan Deduction</th>
                                                        <th>Overtime Value</th>
                                                        <th>Incentive</th>
                                                        <th>Tax</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (isset($payroll_history_data) && count($payroll_history_data) > 0)
                                                        @php
                                                            $total_net_salary = 0;
                                                            $total_gross_amount = 0;
                                                            $total_loan_amount = 0;
                                                            $total_loan_deduction = 0;
                                                            $total_overtime_value = 0;
                                                            $total_incentive = 0;
                                                            $total_tax_amount = 0;
                                                        @endphp
                                                        @foreach ($payroll_history_data as $key => $data)
                                                            <tr>
                                                                <td>{{$key + 1}}</td>
                                                                <td>{{$data->title}}</td>
                                                                <td>{{!empty($data->net_salary) ? number_format($data->net_salary , 2) : 0}}</td>
                                                                <td>{{!empty($data->gross_amount) ? number_format($data->gross_amount , 2) : 0}}</td>
                                                                <td>{{!empty($data->loan_amount) ? number_format($data->loan_amount , 2) : 0}}</td>
                                                                <td>{{!empty($data->loan_deduction) ? number_format($data->loan_deduction , 2) : 0}}</td>
                                                                <td>{{!empty($data->overtime_value) ? number_format($data->overtime_value , 2) : 0}}</td>
                                                                <td>{{!empty($data->incentive) ? number_format($data->incentive , 2) : 0}}</td>
                                                                <td>{{!empty($data->tax_amount) ? number_format($data->tax_amount , 2) : 0}}</td>
                                                            </tr>
                                                            @php
                                                                $total_net_salary += $data->net_salary;
                                                                $total_gross_amount += $data->gross_amount;
                                                                $total_loan_amount += $data->loan_amount;
                                                                $total_loan_deduction += $data->loan_deduction;
                                                                $total_overtime_value += $data->overtime_value;
                                                                $total_incentive += $data->incentive;
                                                                $total_tax_amount += $data->tax_amount;
                                                            @endphp
                                                        @endforeach
                                                        <tfoot>
                                                            <tr>
                                                                <td></td>
                                                                <td class="fw-bolder">Total Net Salary</td>
                                                                <td class="fw-bolder">{{number_format($total_net_salary , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_gross_amount , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_loan_amount , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_loan_deduction , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_overtime_value , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_incentive , 2)}}</td>
                                                                <td class="fw-bolder">{{number_format($total_tax_amount , 2)}}</td>
                                                            </tr>
                                                        </tfoot>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>                 
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('#summary_dataTable').DataTable({
                ordering: true,
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        targets: 0
                    },
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">Department Wise Salary Summary</h6>');
        });
        function historyDataTable(){
            $('#history_dataTable').DataTable({
                ordering: false,
                destroy: true,
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        targets: 0
                    },
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            title: 'Employees Salary Component',
                            action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">Department Wise Salary Summary History</h6>');
        }
    </script>
@endsection