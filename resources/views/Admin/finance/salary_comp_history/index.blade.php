@extends('Admin.layouts.master')
@section('title', 'Salary Component History')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Salary Component History</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Finance</a>
                                </li>
                                <li class="breadcrumb-item active">Salary Component History
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <!--Search Form -->
                            <div class="card-body">
                                <form id="search_form">
                                    <div class="row g-1 mb-md-1">
                                        <div class="col-md-4 col-12">
                                            <label class="form-label">Filter By Salary Component:</label>
                                            <select name="salaryFilter" id="salaryFilter" class="select2 form-select"
                                                data-placeholder="Select Salary Component">
                                                <option value="">Select Salary Component</option>
                                                @foreach ($salary_components as $salary_component)
                                                    <option value="{{ $salary_component->id }}"
                                                        {{ $salary_component->id == request('salaryFilter') ? 'selected' : '' }}>
                                                        {{ $salary_component->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label">Department:</label>
                                            <select name="departmentFilter" id="departmentFilter"
                                                class="select2 form-select" data-placeholder="Select Department" required>
                                                <option value="">Select Department</option>
                                                <option value="all">All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}"
                                                        {{ $department->id == request('departmentFilter') ? 'selected' : '' }}>
                                                        {{ $department->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="date">Filter By Month</label>
                                            <input type="month"
                                                value="{{ !empty(request('dateFilter')) ? request('dateFilter') : date('Y-m') }}"
                                                id="dateFilter" class="form-control" name="dateFilter" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-end">
                                            <a href="{{ route('salary_comp_history.index') }}" type="button"
                                                class="btn btn-danger mt-1">Reset</a>
                                            <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card pb-2">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th class="not_include"></th>
                                            <th>Sr.No</th>
                                            <th>Employee ID</th>
                                            <th>Employee Code</th>
                                            <th>Employee</th>
                                            <th class="not_include">First Name</th>
                                            <th class="not_include">Last Name</th>
                                            <th>Designation</th>
                                            <th>Department</th>
                                            <th>Component</th>
                                            <th>Old Value</th>
                                            <th>Increament</th>
                                            <th>New Value</th>
                                            <th>Remarks</th>
                                            {{-- <th>Submited By</th> --}}
                                            {{-- <th>Updated At</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        datatable = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{ route('salary_comp_history.index') }}",
                data: function(filter) {
                    filter.salaryFilter = $('#salaryFilter').val();
                    filter.departmentFilter = $('#departmentFilter').val();
                    filter.dateFilter = $('#dateFilter').val();
                }
            },
            columns: [{
                    data: 'responsive_id',
                    searchable: false,
                    orderable: false
                },
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    searchable: false,
                    orderable: false,
                },
                {
                    data: 'employee_id',
                    name: 'employees.employee_id',
                },
                {
                    data: 'employee_code',
                    name: 'employees.employee_code',
                },
                {
                    render: function(data, type, full, meta) {
                        var middleName = full['middle_name'] != null ? full['middle_name'] : '';
                        var lastName = full['last_name'] != null ? full['last_name'] : '';
                        return full['first_name'] + ' ' + middleName + ' ' + lastName;
                    },
                    searchable: false
                },
                {
                    data: 'first_name',
                    name: 'employees.first_name',
                    searchable: true,
                    visible: false
                },
                {
                    data: 'last_name',
                    name: 'employees.last_name',
                    searchable: true,
                    visible: false
                },
                {
                    data: 'designation',
                    name: 'jobtitles.name',
                },
                {
                    data: 'department',
                    name: 'companystructures.title',
                },
                {
                    data: 'component_name',
                    name: 'salarycomponent.name',
                },
                {
                    data: 'old_amount',
                    name: 'salary_component_history.old_amount',
                },
                {
                    data: null,
                    name: 'difference',
                    render: function(data, type, row) {
                        var oldAmount = parseFloat(row.old_amount);
                        var newAmount = parseFloat(row.new_amount);
                        if (oldAmount > 0) {
                            return Math.abs(oldAmount - newAmount);
                        } else {
                            return '-'; // Or whatever logic you want when old_amount is not greater than 0
                        }
                    }
                },
                {
                    data: 'new_amount',
                    name: 'salary_component_history.new_amount',
                },
                {
                    data: 'remarks',
                    name: 'salary_component_history.remarks',
                }
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: -1,
            buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({
                    class: 'font-small-4 me-50'
                }) + 'Export',
                buttons: [{
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Print',
                        className: 'dropdown-item',
                        // title: 'Employees',
                        // action: newexportaction,
                        exportOptions: {
                            columns: ':not(.not_include)',
                        }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Csv',
                        className: 'dropdown-item',
                        // title: 'Employees',
                        // action: newexportaction,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                        // title: 'Employees',
                        // action: newexportaction,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Pdf',
                        className: 'dropdown-item',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        // title: 'Employees',
                        // action: newexportaction,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Copy',
                        className: 'dropdown-item',
                        // title: 'Employees',
                        // action: newexportaction,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    }
                ],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group')
                            .addClass('d-inline-flex');
                    }, 50);
                }
            }],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">Salary Component History</h6>');

        $('#search_form').submit(function(e) {
            e.preventDefault();
            datatable.draw();
        });
    </script>
@endsection
