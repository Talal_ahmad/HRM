@extends('Admin.layouts.master')
@section('title', 'Tax Report')
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Tax Report</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Finance
                            </li>
                            <li class="breadcrumb-item active">Tax Report
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <!--Search Form -->
                        <div class="card-body">
                            <form id="search_form" action="{{ route('tax_report.index') }}">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <label class="form-label" for="date">Filter By Date</label>
                                        <input type="month" id="date" class="form-control" placeholder="Job Title" name="date"/>
                                    </div>
                                    <div class="col-md-6 text-end" style="margin-top: 10px">
                                        <a href="{{route('tax_report.index')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">See Report</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php
        $total_gross_amount = 0;
        $total_tax_amount = 0;
        @endphp 
        <section id="basic-dataTable">
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>Designation</th>
                                        <th>Department/Branch</th>
                                        <th>CNIC</th>
                                        <th>Bank Name</th>
                                        <th>Account Title</th>
                                        <th>Account Number</th>
                                        <th>Gross Amount</th>
                                        <th>Tax Amount</th>
                                    </tr>
                            </thead>
                            <tbody>
                            @if (isset($data) && count($data) > 0)   
                                @foreach ($data as $key => $value)
                            @php
                            $total_gross_amount += $value->gross_amount;
                            $total_tax_amount += $value->tax_amount;
                            @endphp
                                  
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$value->employee_id}}</td>
                                                <td>{{$value->employee_code}}</td>
                                                <td>{{$value->first_name}} {{$value->last_name}}</td>
                                                <td>{{$value->designation}}</td>
                                                <td>{{$value->title}}</td>
                                                <td>{{$value->nic_num}}</td>
                                                <td>{{$value->bankName}}</td>
                                                <td>{{$value->accountName}}</td>
                                                <td>{{$value->accountNum}}</td>
                                                <td>{{number_format($value->gross_amount , 2)}}</td>
                                                <td>{{number_format($value->tax_amount , 2)}}</td>
                                            </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-center">No Matches Found</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                               
                             @endif
                            </tbody>
                            <tfoot>
                                      <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="fw-bolder">Net Total</td>
                                            <td class="fw-bolder">{{number_format($total_gross_amount , 2)}}</td>
                                            <td class="fw-bolder">{{number_format($total_tax_amount , 2)}}</td>
                                        </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>



</section>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
      
            ordering: true,
            "columnDefs": [{
                // For Responsive
                className: 'control',
                orderable: false,
                targets: 0
            }],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({
                    class: 'font-small-4 me-50'
                }) + 'Export',
                
                buttons: [{
                    
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Print',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Csv',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({
                            class: 'font-smaller me-50'
                        }) + 'Pdf',
                        className: 'dropdown-item',
                        orientation: 'landscape',
                        footer: true,
                        pageSize: 'A3',
                        customize: function(doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();
                            doc.pageMargins = [20, 60, 20, 30];
                            // Set the font size fot the entire document
                            doc.defaultStyle.fontSize = 12;
                            // Set the fontsize for the table header
                            doc.styles.tableHeader.fontSize = 10;
                            // Create a header
                            doc["header"] = function() {
                                return {
                                    columns: [{
                                        alignment: "center",
                                        italics: true,
                                        text: "Tax Report",
                                        fontSize: 18,
                                        margin: [10, 0]
                                    }, ],
                                    margin: 20
                                };
                            };
                            doc["footer"] = function(page, pages) {
                                return {
                                    columns: [{
                                            alignment: "left",
                                            text: ["Created on: ", {
                                                text: jsDate.toString()
                                            }]
                                        },
                                        {
                                            alignment: "right",
                                            text: [
                                                "page ",
                                                {
                                                    text: page.toString()
                                                },
                                                " of ",
                                                {
                                                    text: pages.toString()
                                                }
                                            ]
                                        }
                                    ],
                                    margin: 20
                                };
                            };
                            var objLayout = {};
                            objLayout["hLineWidth"] = function(i) {
                                return 0.5;
                            };
                            objLayout["vLineWidth"] = function(i) {
                                return 0.5;
                            };
                            objLayout["hLineColor"] = function(i) {
                                return "#aaa";
                            };
                            objLayout["vLineColor"] = function(i) {
                                return "#aaa";
                            };
                            objLayout["paddingLeft"] = function(i) {
                                return 4;
                            };
                            objLayout["paddingRight"] = function(i) {
                                return 4;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        // action: newexportaction,
                        exportOptions: {
                            columns: ':not(.not_include)',
                            // tfoot:true,
                        }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    }
                ],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            }],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">Tax Report</h6>');
    });




    
</script>








@endsection