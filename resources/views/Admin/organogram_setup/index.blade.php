@extends('Admin.layouts.master')
@section('title', 'Organogram Setup')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.Organogram_Setup')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.setup')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.Organogram_Setup')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Name</th>
                                        <th>Title</th>
                                        <th>Parent</th>
                                        <th>User Name</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!--Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Add_Organogram_Setup')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="name">@lang('app.Name')</label>
                                            <input type="text" id="name" class="form-control" placeholder="@lang('app.Name')"
                                                name="name" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">@lang('app.Title')</label>
                                            <input type="text" id="title" class="form-control" placeholder="@lang('app.Title')"
                                                name="title" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="parent">@lang('app.Parent_ID')</label>
                                            <select name="pid" id="parent" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Parent_ID')" >
                                                <option value=""></option>
                                                @foreach ($Organogram as $org)
                                                    <option value="{{ $org->id }}">{{ $org->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="user">@lang('app.Select_Employee')</label>
                                            <select name="user_id" id="user" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_User')" >
                                                <option value=""></option>
                                                @foreach ($users as $user)
                                                    <option value="{{ $user->id }}">{{ $user->employee_code }} {{ $user->employee_code }} {{ $user->first_name }} {{ $user->last_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">@lang('app.Save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End Add Modal -->

            <!--start edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">@lang('app.Edit_Category')</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="name">@lang('app.Name')</label>
                                            <input type="text" id="edit_name" class="form-control" placeholder="@lang('app.Name')"
                                                name="name" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="title">@lang('app.Title')</label>
                                            <input type="text" id="edit_title" class="form-control" placeholder="@lang('app.Title')"
                                                name="title" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="parent">@lang('app.Parent_ID')</label>
                                            <select name="pid" id="edit_parent" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_Parent_ID')" >
                                                <option value=""></option>
                                                @foreach ($Organogram as $org)
                                                    <option value="{{ $org->id }}">{{ $org->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="user">@lang('app.Select_Employee')</label>
                                            <select name="user_id" id="edit_user" class="select2 form-select"
                                                data-placeholder="@lang('app.Select_User')" >
                                                <option value=""></option>
                                                @foreach ($users as $user)
                                                    <option value="{{ $user->id }}">{{ $user->first_name }} {{ $user->last_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">@lang('app.Close')</button>
                                <button type="submit" class="form_save btn btn-primary" id="update">@lang('app.Update')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End edit Modal -->
        </div>
    </section>

@endsection
@section('scripts')
    <script>
        var datatable;
        var rowid;
        $(document).ready(function() {
            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('organoGramSetup.index') }}",
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name',
                        name: 'organogram_setup.name',
                    },
                    {
                        data: 'title',
                        name: 'organogram_setup.title',
                    },
                    {
                        data: 'parent_name',
                        name: 'organogram_setup.parent_name',
                    },
                    {
                        data: 'user_name',
                        name: 'users.user_name',
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Company Structures</h6>');

            $("#add_form").submit(function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('organoGramSetup.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $("#add_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'OrganoGram Setup has been Added Successfully!'
                            })
                        }

                    }
                });
            });

            // Update record
            $("#edit_form").on("submit", function(e) {
                ButtonStatus('.form_save',true);
                    blockUI();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('organoGramSetup') }}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_modal").modal("hide");
                            $(".select2").val('').trigger('change')
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'OrganoGram Setup has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
        });

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('organoGramSetup') }}" + "/" + id + "/edit",
                type: "get",
                success: function(response) {
                    $("#edit_title").val(response.title);
                    $("#edit_name").val(response.name);
                    $("#edit_parent").val(response.pid).select2();
                    $("#edit_user").val(response.user_id).select2();;
                    $("#edit_modal").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "organoGramSetup/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Organogram Setup has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
