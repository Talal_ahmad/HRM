@extends('Admin.layouts.master')

@section('title', 'Dashboard')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">@lang('app.Admin')</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!--Bar Chart Start -->
            <h1 class="text-center">@lang('app.Turnover_Report')</h1>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                        {{-- <div class="header-left">
                        <h4 class="card-title">Latest Statistics</h4>
                    </div>
                    <div class="header-right d-flex align-items-center mt-sm-0 mt-1">
                        <i data-feather="calendar"></i>
                        <input type="text" class="form-control flat-picker border-0 shadow-none bg-transparent pe-0" placeholder="YYYY-MM-DD" />
                    </div> --}}
                    </div>
                    <div class="card-body">
                        <canvas id="outChart" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="myChart" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="lineoutChart" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="linemyChart" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="radaroutChart" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="radarmyChart" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
                <!-- Bar Chart End -->
                {{-- {{dd($Countmonths)}} --}}
                <!-- Horizontal Bar Chart Start -->
                {{-- <div class="col-xl-6 col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                        <div class="header-left">
                            <p class="card-subtitle text-muted mb-25">Balance</p>
                            <h4 class="card-title">$74,123</h4>
                        </div>
                        <div class="header-right d-flex align-items-center mt-sm-0 mt-1">
                            <i data-feather="calendar"></i>
                            <input type="text" class="form-control flat-picker border-0 shadow-none bg-transparent pe-0" placeholder="YYYY-MM-DD" />
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas class="horizontal-bar-chart-ex chartjs" data-height="400"></canvas>
                    </div>
                </div>
            </div> --}}
            <h1 class="text-center">@lang('app.Annual_Turnover_Report')</h1>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="outChart1" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="myChart1" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="lineoutChart1" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="linemyChart1" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="radaroutChart1" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <canvas id="radarmyChart1" class="chartjs"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div
                        class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    </div>
                    <div class="card-body">
                        <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                    </div>
                </div>
            </div>
            <!-- Horizontal Bar Chart End -->
        </div>

        <!-- Line Chart Starts-->
        <div class="content-body row">
            <a href="{{ route('companyStructures.index') }}" target="_blank"
                class="text-secondary col-xl-4 col-md-6 col-12">
                <div class="card card-statistics" style="height:175px;">
                    <div class="card-header">
                        <h4 class="card-title">@lang('app.Departments')</h4>
                    </div>
                    <div class="card-body statistics-body">
                        <div class="row">
                            <div class="">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-info me-2">
                                        <div class="avatar-content">
                                            <i data-feather="user" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        @if (env('COMPANY') == 'JSML')
                                            <h4 class="fw-bolder mb-0">{{ $parent }}</h4>
                                        @else
                                            <h4 class="fw-bolder mb-0">{{ departments()->count() }}</h4>
                                        @endif
                                        <p class="card-text font-small-3 mb-0">@lang('app.Total_Departments')</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <a href="{{ route('getEmployees') }}#active_employee" target="_blank"
                class="text-secondary col-xl-4 col-md-6 col-12">
                <div class="card card-statistics" style="height:175px;">
                    <div class="card-header">
                        @if (env('COMPANY') == 'JSML')
                            <h4 class="card-title">@lang('app.On_Role_Employees')</h4>
                        @else
                            <h4 class="card-title">@lang('app.Employees')</h4>
                        @endif
                    </div>
                    <div class="card-body statistics-body">
                        <div class="row">
                            <div class="">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-info me-2">
                                        <div class="avatar-content">
                                            <i data-feather="user" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">{{ employees()->count() }}</h4>
                                        @if (env('COMPANY') == 'JSML')
                                            <p class="card-text font-small-3 mb-0">@lang('app.Total_On_Role_Employees')</p>
                                        @else
                                            <p class="card-text font-small-3 mb-0">@lang('app.Total_Employees')</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @if (env('COMPANY') == 'JSML')
                <a href="{{ route('getEmployees') }}#off_roll" target="_blank"
                    class="text-secondary col-xl-4 col-md-6 col-12">
                    <div class="card card-statistics" style="height:175px;">
                        <div class="card-header">
                            <h4 class="card-title">@lang('app.Off_Role_Employees')</h4>
                        </div>
                        <div class="card-body statistics-body">
                            <div class="row">
                                <div class="">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-info me-2">
                                            <div class="avatar-content">
                                                <i data-feather="user" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h4 class="fw-bolder mb-0">{{ $off_employees }}</h4>
                                            <p class="card-text font-small-3 mb-0">@lang('app.Total_Off_Role_Employees')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('getEmployees') }}#reg_emp" target="_blank"
                    class="text-secondary col-xl-4 col-md-6 col-12">
                    <div class="card card-statistics" style="height:175px;">
                        <div class="card-header">
                            <h4 class="card-title">@lang('app.Registered_Employees')</h4>
                        </div>
                        <div class="card-body statistics-body">
                            <div class="row">
                                <div class="">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-info me-2">
                                            <div class="avatar-content">
                                                <i data-feather="user" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h4 class="fw-bolder mb-0">{{ $reg_employees }}</h4>
                                            <p class="card-text font-small-3 mb-0">@lang('app.Total_Registered_Employees')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('getEmployees') }}#terminate" target="_blank"
                    class="text-secondary col-xl-4 col-md-6 col-12">
                    <div class="card card-statistics" style="height:175px;">
                        <div class="card-header">
                            <h4 class="card-title">@lang('app.Terminated_Employees')</h4>
                        </div>
                        <div class="card-body statistics-body">
                            <div class="row">
                                <div class="">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-info me-2">
                                            <div class="avatar-content">
                                                <i data-feather="user" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h4 class="fw-bolder mb-0">{{ $ter_employees }}</h4>
                                            <p class="card-text font-small-3 mb-0">@lang('app.Total_Terminated_Employees')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            @endif
            <a href="{{ route('employee_leave_requests.index') }}" target="_blank"
                class="text-secondary col-xl-4 col-md-6 col-12">
                <div class="card card-statistics" style="height:175px;">
                    <div class="card-header">
                        <h4 class="card-title">@lang('app.Leaves')</h4>
                    </div>
                    <div class="card-body statistics-body">
                        <div class="row">
                            <div class="">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-info me-2">
                                        <div class="avatar-content">
                                            <i data-feather="user" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">{{ $employees_on_leave }}</h4>
                                        <p class="card-text font-small-3 mb-0">@lang('app.Total_Employees_On_Leave')</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            var _ydata = {!! json_encode($months) !!};
            var _xdata = {!! json_encode($Countmonths) !!};
            const ctx = document.getElementById('myChart');
            new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: _ydata,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(2,117,216,1)",
                        bordergroundColor: "rgba(2,117,216,1)",
                        label: '# Total In Employees',
                        data: _xdata,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _yodata = {!! json_encode($out_months) !!};
            var _xodata = {!! json_encode($out_Countmonths) !!};
            const ctx1 = document.getElementById('outChart');
            new Chart(ctx1, {
                type: 'bar',
                data: {
                    labels: _yodata,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(255, 99, 71, 1)",
                        bordergroundColor: "rgba(255, 99, 71, 1)",
                        label: '# Total Out Employees',
                        data: _xodata,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _ydata = {!! json_encode($months) !!};
            var _xdata = {!! json_encode($Countmonths) !!};
            const ctx2 = document.getElementById('linemyChart');
            new Chart(ctx2, {
                type: 'line',
                data: {
                    labels: _ydata,
                    datasets: [{
                        minBarLength: 2,
                        backgroundColor: "rgba(2,117,216,1)",
                        bordergroundColor: "rgba(2,117,216,1)",
                        label: ' # Total In Employees',
                        data: _xdata,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _yodata = {!! json_encode($out_months) !!};
            var _xodata = {!! json_encode($out_Countmonths) !!};
            const ctx3 = document.getElementById('lineoutChart');
            new Chart(ctx3, {
                type: 'line',
                data: {
                    labels: _yodata,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(255, 99, 71, 1)",
                        bordergroundColor: "rgba(255, 99, 71, 1)",
                        label: '# Total Out Employees',
                        data: _xodata,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _ydata = {!! json_encode($months) !!};
            var _xdata = {!! json_encode($Countmonths) !!};
            const ctx4 = document.getElementById('radarmyChart');
            new Chart(ctx4, {
                type: 'radar',
                data: {
                    labels: _ydata,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(2,117,216,1)",
                        bordergroundColor: "rgba(2,117,216,1)",
                        label: '# Total In Employees',
                        data: _xdata,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _yodata = {!! json_encode($out_months) !!};
            var _xodata = {!! json_encode($out_Countmonths) !!};
            const ctx5 = document.getElementById('radaroutChart');
            new Chart(ctx5, {
                type: 'radar',
                data: {
                    labels: _yodata,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(255, 99, 71, 1)",
                        bordergroundColor: "rgba(255, 99, 71, 1)",
                        label: '# Total Out Employees',
                        data: _xodata,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            //Annual Turnover Report

            var _ydata_1 = {!! json_encode($months_1) !!};
            var _xdata_1 = {!! json_encode($Countmonths_1) !!};
            const ctx01 = document.getElementById('myChart1');
            new Chart(ctx01, {
                type: 'bar',
                data: {
                    labels: _ydata_1,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(2,117,216,1)",
                        bordergroundColor: "rgba(2,117,216,1)",
                        label: '# Total In Employees',
                        data: _xdata_1,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _yodata_1 = {!! json_encode($out_months_1) !!};
            var _xodata_1 = {!! json_encode($out_Countmonths_1) !!};
            const ctx02 = document.getElementById('outChart1');
            new Chart(ctx02, {
                type: 'bar',
                data: {
                    labels: _yodata_1,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(255, 99, 71, 1)",
                        bordergroundColor: "rgba(255, 99, 71, 1)",
                        label: '# Total Out Employees',
                        data: _xodata_1,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _ydata_1 = {!! json_encode($months_1) !!};
            var _xdata_1 = {!! json_encode($Countmonths_1) !!};
            const ctx03 = document.getElementById('linemyChart1');
            new Chart(ctx03, {
                type: 'line',
                data: {
                    labels: _ydata_1,
                    datasets: [{
                        minBarLength: 2,
                        backgroundColor: "rgba(2,117,216,1)",
                        bordergroundColor: "rgba(2,117,216,1)",
                        label: '# Total In Employees',
                        data: _xdata_1,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _yodata_1 = {!! json_encode($out_months_1) !!};
            var _xodata_1 = {!! json_encode($out_Countmonths_1) !!};
            const ctx04 = document.getElementById('lineoutChart1');
            new Chart(ctx04, {
                type: 'line',
                data: {
                    labels: _yodata_1,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(255, 99, 71, 1)",
                        bordergroundColor: "rgba(255, 99, 71, 1)",
                        label: '# Total Out Employees',
                        data: _xodata_1,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _ydata_1 = {!! json_encode($months_1) !!};
            var _xdata_1 = {!! json_encode($Countmonths_1) !!};
            const ctx041 = document.getElementById('radarmyChart1');
            new Chart(ctx041, {
                type: 'radar',
                data: {
                    labels: _ydata_1,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(2,117,216,1)",
                        bordergroundColor: "rgba(2,117,216,1)",
                        label: '# Total In Employees',
                        data: _xdata_1,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            var _yodata_1 = {!! json_encode($out_months_1) !!};
            var _xodata_1 = {!! json_encode($out_Countmonths_1) !!};
            const ctx06 = document.getElementById('radaroutChart1');
            new Chart(ctx06, {
                type: 'radar',
                data: {
                    labels: _yodata_1,
                    datasets: [{
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        minBarLength: 2,
                        backgroundColor: "rgba(255, 99, 71, 1)",
                        bordergroundColor: "rgba(255, 99, 71, 1)",
                        label: '# Total Out Employees',
                        data: _xodata_1,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            // Javascript to enable link to tab
            var hash = location.hash.replace(/^#/, ''); // ^ means starting, meaning only match the first hash
            if (hash) {
                // if(hash=='off_roll'){
                //     $('#active_employee-tab').removeClass('active');
                //     $('#off_roll-tab').addClass('active');
                // }else if(hash=='terminate'){
                //     $('#active_employee-tab').removeClass('active');
                //     $('#terminate-tab').addClass('active');
                // }
                $('.nav-tabs a[href="#' + hash + '"]').tab('show');
                $('.nav-tabs a[href="#' + hash + '"]').addClass('active');

            }
            // Change hash for page-reload
            $('.nav-tabs a').on('shown.bs.tab', function(e) {
                window.location.hash = e.target.hash;
            });
        });
        window.onload = function() {
            var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "Employees TurnOver Month Wise"
                },
                axisX: {
                    valueFormatString: "MMM",
                    interval: 1,
                    intervalType: "month"
                },
                toolTip: {
                    shared: true,
                    contentFormatter: function(e) {
                        var content =
                            '<div style="width: auto; height: auto; min-width: 50px; line-height: normal; margin: 0px; padding: 5px; font-family: &quot;Trebuchet MS&quot;, Helvetica, sans-serif; font-weight: normal; font-style: normal; font-size: 14px; color: black; text-shadow: rgba(0, 0, 0, 0.1) 1px 1px 1px; text-align: left; border: 1px solid rgb(79, 129, 188); background: rgba(255, 255, 255, 0.9); text-indent: 0px; white-space: nowrap; border-radius: 0px; user-select: none;">';

                        if (e.entries.length > 0) {
                            var dataPoint = e.entries[0].dataPoint;
                            var formattedDate = formatDate(dataPoint.x);
                            content += formattedDate + '<br>';
                        }

                        for (var i = 0; i < e.entries.length; i++) {
                            var color = (e.entries[i].dataSeries.name === "Suspended" ?
                                "rgb(79, 129, 188)" : "rgb(192, 80, 78)");
                            var formattedValue = e.entries[i].dataPoint.y
                                .toLocaleString();

                            content += '<span data-color="' + color + '" style="color: ' + color + ';">' + e
                                .entries[i].dataSeries.name + ':</span>&nbsp;&nbsp;<strong>' +
                                formattedValue + '</strong>';
                            content += '<br>';
                        }

                        content += '</div>';
                        return content;
                    }
                },
                data: [{
                        type: "stackedColumn",
                        legendText: "Suspended",
                        showInLegend: true,
                        name: "Suspended",
                        indexLabel: "{y}",
                        indexLabelPlacement: "auto",
                        dataPoints: [{!! $dataPoints['suspended'] !!}]
                    },
                    {
                        type: "stackedColumn",
                        legendText: "Terminated",
                        showInLegend: true,
                        name: "Terminated",
                        indexLabel: "{y}",
                        indexLabelPlacement: "auto",
                        dataPoints: [{!! $dataPoints['terminated'] !!}]
                    }
                ]
            });

            chart.render();
        }

        function formatDate(date) {
            var options = {
                year: 'numeric',
                month: 'short'
            };
            return new Intl.DateTimeFormat('en-US', options).format(date);
        }
        setTimeout(function() {
            toastr['success'](
                'You have successfully logged in to HRM.',
                '👋 Welcome {{ Auth::user()->name }}!', {
                    closeButton: true,
                    tapToDismiss: false
                }
            );
        }, 2000);
    </script>
    <script src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
@endsection
