<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Attendance Sheet</title>
    <head>
        <style>
            #attendance_sheet {
                border-collapse: collapse;
                width: 100%;
                font-family:'Segoe UI',sans-serif
            }
            #attendance_sheet td {
                border: 1px solid #ddd;
                padding: 5px;
                font-size: 16px;
            }

            #attendance_sheet td{
                text-align: center;
            }

            #attendance_sheet tr:nth-child(even){background-color: #f2f2f2;}

            #attendance_sheet tr:hover {background-color: #ddd;}

            .px-2 {
                padding-right: 1.5rem !important;
                padding-left: 1.5rem !important;
            }
            #attendance_sheet th {
                padding-top: 12px;
                padding-bottom: 12px;
                border: 1px solid #ddd;
                padding: 5px;
                /* font-size: 8px; */
                text-align: left;
                background-color: #2e475c;
                color: white;
            }
        </style>
    </head>
</head>
<body>
    <div>
        <h1 style="text-align: center; margin-bottom: 5px;">Attendance Sheet</h1>
        @if ($dateDay)
            <span style="font-weight: bold;">Date : </span>{{$start_date}}
        @else
            <span style="font-weight: bold">Month : </span>{{$year.'-'.$month}}
        @endif
        @if (isset($dept))
            <span style="font-weight: bold">Department : </span>{{$dept}}
        @endif
        @if (isset($desg))
            <span style="font-weight: bold">Designation : </span>{{$desg}}
        @endif
        <span style="font-weight: bold">Printed By : </span>{{auth()->user()->username}}
        <span style="font-weight: bold">Printed At : </span>{{date('Y-m-d H:i:s')}}

        @php
            $style = '';
            $padding_style = '';
            $font_size = '';
            if(empty($dateDay))
            {
                $style = 'white-space: nowrap';
            }
            if(env('COMPANY') == 'CIDEX')
            {
                $font_size = 'font-size: 12px !important';
                $style = 'white-space: nowrap';
            }
            else {
                $font_size = 'font-size: 16px !important';
            }
            if(env('COMPANY') != 'CIDEX')
            {
                $padding_style = 'padding: 1rem 0.5rem;';
            }
        @endphp
        <div>
            <table id="attendance_sheet" style="{{$font_size}}; margin-bottom: 5px;">
                <thead>
                    <th>Sr.No</th>
                    <th>Employee ID</th>
                    @if (env('COMPANY') != 'CIDEX')
                    <th>Employee Code</th>
                    @endif
                    <th>Employee</th>
                    <th>Desigination</th>
                    <th>Department</th>
                    @for ($start = $start_date; $start <= $end_date; $start++)
                        <th colspan="2">{{date('d', strtotime($start))}} ({{date('D' , strtotime($start))}})</th>
                    @endfor
                    @if (env('COMPANY') != 'CIDEX')
                    <th>Total Overtime Hours</th>
                    @endif
                    <th>Total Working Hours</th>
                </thead>
                <tbody style="{{$style}}">
                    @if (isset($employees) && count($employees) > 0)
                        @php
                            $absent = 0;
                            $present = 0;
                            $total = 0;
                        @endphp
                        @foreach ($employees as $key => $employee)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$employee->employee_id}}</td>
                            @if (env('COMPANY') != 'CIDEX')
                            <td>{{$employee->employee_code}}</td>
                            @endif
                            <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                            <td>{{$employee->desigination}}</td>
                            <td>{{$employee->title}}</td>
                            @for ($date = $start_date; $date <= $end_date; $date++)
                                @if (count($employe_attendance[$employee->id][$date]) > 2)
                                    @php
                                        if($employe_attendance[$employee->id][$date]['is_manual'] == 1)
                                        {
                                            $color = 'yellow';
                                        }
                                        else {
                                            $color = '';
                                        }

                                        if($employe_attendance[$employee->id][$date]['in_time'] > $employe_attendance[$employee->id][$date]['grace_period'])
                                        {
                                            $tcolor = 'red';
                                        }
                                        else {
                                            $tcolor = 'green';
                                        }
                                    @endphp
                                    <td colspan="2" style="{{$padding_style}}">
                                        {{-- @if (env('COMPANY') != 'CIDEX')
                                        <label style="font-size: 12px;">Work Hrs:</label>
                                        <span>{{number_format($employe_attendance[$employee->id][$date]['working_hours'],2)}}</span>
                                        @endif
                                        @if (empty($dateDay))
                                        <br>
                                        <hr>
                                        <label style="font-size: 12px;">Total Work Hrs:</label> --}}
                                        <span>{{number_format($employe_attendance[$employee->id][$date]['total_working_hours'],2)}}</span>
                                        {{-- <br>
                                        <hr>
                                        <label style="font-size: 12px;">Overtime Hrs:</label>
                                        <span>{{number_format($employe_attendance[$employee->id][$date]['overtime_hours'],2)}}</span>
                                        @endif --}}
                                    </td>
                                    @php
                                        $present++;
                                    @endphp
                                @else
                                    @php
                                        $absent++;
                                    @endphp
                                    <td class="px-2" colspan="2"><span class="px-2" style="color:white; background: {{$employe_attendance[$employee->id][$date]['absent_value_color']}}">{{$employe_attendance[$employee->id][$date]['absent_value']}}</span></td>
                                @endif
                                @php
                                    $total++;
                                @endphp
                            @endfor
                            @if (env('COMPANY') != 'CIDEX')
                            <td>{{number_format($employe_attendance[$employee->id]['total_overtime_hours'],2)}}</td>
                            @endif
                            <td>{{number_format($employe_attendance[$employee->id]['total_working_hours'],2)}}</td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="{{($month_days*2) + 7}}" style="text-align: center">No Record Found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @if (!empty($dateDay))    
        <div>
            <span style="font-weight: bold">Total Employees : </span>{{$total}}
            <span style="font-weight: bold">Total Present : </span>{{$present}}
            <span style="font-weight: bold">Total Absent : </span>{{$absent}}
        </div>
    @endif
</body>
</html>