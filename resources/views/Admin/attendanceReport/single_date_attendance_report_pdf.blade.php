<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Attendance Sheet</title>
    <head>
        <style>
            #attendance_sheet {
                border-collapse: collapse;
                width: 100%;
                font-family:'Segoe UI',sans-serif
            }
            #attendance_sheet td {
                border: 1px solid #ddd;
                padding: 2px;
                font-size: 13px;
                white-space: normal !important;
            }
            .px-2 {
                padding-right: 1.5rem !important;
                padding-left: 1.5rem !important;
            }
            #attendance_sheet tr:nth-child(even){background-color: #f2f2f2;}

            #attendance_sheet tr:hover {background-color: #ddd;}

            #attendance_sheet th {
                /* padding-top: 12px; */
                /* padding-bottom: 12px; */
                border: 1px solid #ddd;
                padding: 2px;
                font-size: 13px;
                text-align: left;
                background-color: #2e475c;
                color: white;
            }
        </style>
    </head>
</head>
<body>
    <div>
        <p>
            <span style="font-weight: bold; font-size: 20px">Attendance Report</span> (
            @if ($dateDay)
                <span style="font-weight: bold;">Date : </span>{{$start_date}}
            @else
                <span style="font-weight: bold">Month : </span>{{$year.'-'.$month}}
            @endif
            @if (isset($dept))
                <span style="font-weight: bold">Department : </span>{{$dept}}
            @endif
            @if (isset($desg))
                <span style="font-weight: bold">Designation : </span>{{$desg}}
            @endif
            <span style="font-weight: bold">Printed By : </span>{{auth()->user()->username}}
            <span style="font-weight: bold">Printed At : </span>{{date('Y-m-d H:i:s')}} )
        </p>

        @php
            $rows = ceil(count($employees)/2);
        @endphp

        <div>
            <table id="attendance_sheet" style="font-size: 20px; white-space: nowrap; margin-bottom: 20px;">
                <thead>
                    <th>Sr.No</th>
                    <th>Employee</th>
                    <th colspan="2">Status</th>
                    <th>Sr.No</th>
                    <th>Employee</th>
                    <th colspan="2">Status</th>
                </thead>
                <tbody>
                    @if (isset($employees) && count($employees) > 0)
                        @php
                            $absent = 0;
                            $present = 0;
                            $total = 0;
                            $itrate = 0;
                        @endphp
                        @for ($i = 0; $i < $rows; $i++)    
                            <tr>
                                @foreach ($employees as $key => $employee)
                                    @if ($itrate < 2)
                                        <td>{{$key + 1}}</td>
                                        <td>{{$employee->employee_id}}-{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}-{{$employee->title}}</td>
                                        @for ($date = $start_date; $date <= $end_date; $date++)
                                            @if (count($employe_attendance[$employee->id][$date]) > 2)
                                                @php
                                                    if($employe_attendance[$employee->id][$date]['is_manual'] == 1)
                                                    {
                                                        $color = 'yellow';
                                                    }
                                                    else {
                                                        $color = '';
                                                    }

                                                    if($employe_attendance[$employee->id][$date]['in_time'] > $employe_attendance[$employee->id][$date]['grace_period'])
                                                    {
                                                        $tcolor = 'red';
                                                    }
                                                    else {
                                                        $tcolor = 'green';
                                                    }
                                                @endphp
                                                <td colspan="2">
                                                    {{-- <label style="font-size: 12px;" >In Time:</label> --}}
                                                    <span style="background:{{$color}};color:{{$tcolor}};">
                                                        {{$employe_attendance[$employee->id][$date]['in_time']}}
                                                    </span>
                                                    <br>
                                                    {{-- <label style="font-size: 12px;">Out Time:</label> --}}
                                                    <span>
                                                        {{$employe_attendance[$employee->id][$date]['out_time']}}
                                                    </span>
                                                    <br>
                                                    @if (env('COMPANY') != 'CIDEX')
                                                    {{-- <label style="font-size: 12px;">Work Hrs:</label> --}}
                                                    <span>{{number_format($employe_attendance[$employee->id][$date]['working_hours'],2)}}</span>
                                                    @endif
                                                    @if (empty($dateDay))
                                                    <br>
                                                    <hr>
                                                        @if (isset($employe_attendance[$employee->id][$date]['short_leave_time_from']))
                                                            {{-- <label style="font-size: 12px;">Short Leave From:</label> --}}
                                                            <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_from']}}</span>
                                                            <br>
                                                            {{-- <label style="font-size: 12px;">Short Leave To:</label> --}}
                                                            <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_to']}}</span>
                                                        <hr>
                                                        @endif
                                                        {{-- <label style="font-size: 12px;">Shift:</label> --}}
                                                        <span style="color:#A52A2A">{{$employe_attendance[$employee->id][$date]['shift_desc']}}</span>
                                                        <br>
                                                        {{-- <label style="font-size: 11px;">Shift Start:</label> --}}
                                                        <span>{{$employe_attendance[$employee->id][$date]['shift_start_time']}}</span>
                                                        <br>
                                                        {{-- <label style="font-size: 12px;">Shift End:</label> --}}
                                                        <span>{{$employe_attendance[$employee->id][$date]['shift_end_time']}}</span>
                                                        <hr>
                                                        {{-- <label style="font-size: 12px;">Total Work Hrs:</label> --}}
                                                        <span>{{number_format($employe_attendance[$employee->id][$date]['total_working_hours'],2)}}</span>
                                                        <br>
                                                        {{-- <label style="font-size: 12px;">Overtime Hrs:</label> --}}
                                                        <span>{{number_format($employe_attendance[$employee->id][$date]['overtime_hours'],2)}}</span>
                                                    @endif
                                                </td>
                                                @php
                                                    $present++;
                                                @endphp
                                            @else
                                                @php
                                                    $absent++;
                                                @endphp
                                                    <td colspan="2"><span class="px-2" style="color:white; background: {{$employe_attendance[$employee->id][$date]['absent_value_color']}}">{{$employe_attendance[$employee->id][$date]['absent_value']}}</span></td>
                                            @endif
                                            @php
                                                $total++;
                                            @endphp
                                        @endfor
                                        @php
                                            unset($employees[$key]);
                                        @endphp
                                    @endif
                                    @php
                                        $itrate = $itrate + 1;
                                    @endphp
                                @endforeach
                                @php
                                    $itrate = 0;
                                @endphp
                            </tr>
                        @endfor
                    @else
                        <tr>
                            <td colspan="{{($month_days*2) + 7}}" style="text-align: center">No Record Found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @if (!empty($dateDay))    
        <div>
            <span style="font-weight: bold">Total Employees : </span>{{$total}}
            <span style="font-weight: bold">Total Present : </span>{{$present}}
            <span style="font-weight: bold">Total Absent : </span>{{$absent}}
        </div>
    @endif
</body>
</html>