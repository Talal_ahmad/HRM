@extends('Admin.layouts.master')
@section('title', 'Absentees Report')
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">@lang('app.Absentees_Report')</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('app.dashboard')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">@lang('app.attendance')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('app.Absentees_Report')
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-12">
                    <!--Search Form -->
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{url('absentessReport')}}" method="GET">
                                <input type="hidden" name="type" value="{{request('type')}}">
                                <div class="row">
                                    @if (env('COMPANY') == 'JSML')        
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="dptfilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="dptfilter" id="department" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all" {{request('dptfilter') == 'all' ? 'selected' : ''}}>All</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('dptfilter')) ? $department->id == request('dptfilter') ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Section'):</label>
                                            <select name="departmentFilter[]" id="section"
                                                class="select2 form-select" data-placeholder="@lang('app.Select_Department')"
                                                multiple>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}"
                                                        {{ !empty(request('department')) ? (in_array($department->id, request('department')) ? 'selected' : '') : '' }}>
                                                        {{ $department->title }}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="departmentFilter">@lang('app.Filter_By_Department'):</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Department')" required>
                                                <option value=""></option>
                                                <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : '' }}>All Departments</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" for="employeeFilter">@lang('app.Filter_By_Employee'):</label>
                                        <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Employee')">
                                            <option value=""></option>
                                            @if (!empty(request('departmentFilter')))
                                                @foreach (employees(request('departmentFilter')) as $item)
                                                <option value="{{$item->id}}" {{$item->id == request('employeeFilter') ? 'selected' : ''}}>{{$item->first_name.' '.$item->last_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" for="designationFilter">Filter By Designation:</label>
                                        <select name="designationFilter" id="designationFilter" class="select2 form-select" data-placeholder="Select Designation">
                                            <option value=""></option>
                                            <option value="all" {{request('designationFilter') == 'all' ? 'selected' : '' }}>All</option>
                                            @foreach (designation() as $designation)
                                                <option value="{{$designation->id}}" {{$designation->id == request('designationFilter') ? 'selected' : ''}}>{{$designation->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" for="fromDate">From Date:</label>
                                        <label class="form-label" for="fromDate">@lang('app.From_Date'):</label>
                                        <input type="text" name="fromDate" id="fromDate" class="form-control flatpickr-basic" value="{{request('fromDate')}}" placeholder="YYYY-MM-DD" required>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <label class="form-label" for="toDate">@lang('app.To_Date'):</label>
                                        <input type="text" name="toDate" id="toDate" class="form-control flatpickr-basic" value="{{request('toDate')}}" placeholder="YYYY-MM-DD" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{url('absentessReport')}}?type={{request('type')}}" type="button" class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                        <button type="submit" class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card pb-2">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="absentees_table">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Employee Id</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>Desigination</th>
                                        <th>Department</th>
                                        <th>Joined Date</th>
                                        <th>Total Month Days</th>
                                        <th>Total Days Present</th>
                                        <th>Manual Attendance</th>
                                        <th>Total Days Absent</th>
                                        <th>Total Days Leave</th>
                                        <th>Total Off Days</th>
                                        <th>Total Days Late</th>
                                        <th>Total Working Hours</th>
                                        <th>Total Overtime Hours</th>
                                        <th>Total Missing CheckOut</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($employees) && count($employees) > 0)
                                        @foreach ($employees as $key => $employee)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$employee->employee_id}}</td>
                                            <td>{{$employee->employee_code}}</td>
                                            <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                            <td>{{$employee->desigination}}</td>
                                            <td>{{$employee->title}}</td>
                                            <td>{{$employee->joined_date}}</td>
                                            <td>{{$month_days}}</td>
                                            <td>{{$employe_attendance[$employee->id]['total_days_present']}}</td>
                                            <td>{{ $employe_attendance[$employee->id]['total_manual_attendance'] }}</td>
                                            <td>{{$employe_attendance[$employee->id]['total_days_absent']}}</td>
                                            <td>{{$employe_attendance[$employee->id]['total_leaves']}}</td>
                                            <td>{{$employe_attendance[$employee->id]['total_off_days']}}</td>
                                            <td>{{$employe_attendance[$employee->id]['total_days_late']}}</td>
                                            <td>{{number_format($employe_attendance[$employee->id]['total_working_hours'], 2)}}</td>
                                            <td>{{$employe_attendance[$employee->id]['total_overtime_hours']}}</td>
                                            <td>
                                                {{$employe_attendance[$employee->id]['out_time']}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</section>
@endsection
@section('scripts')
    @if(Session::has('error'))
        <script>
            Toast.fire({
                icon: "error",
                title: "{!! Session::get('error') !!}"
            })
        </script>
    @endif
    <script>
        $(document).ready(function(){
            var from_date = '{!! request()->query('fromDate') !!}';
            var to_date = '{!! request()->query('toDate') !!}';
            $('#absentees_table').DataTable({
                ordering: false,
                // "columnDefs": [
				// 	{
				// 		// For Responsive
				// 		className: 'control',
				// 		orderable: false,
				// 		targets: 0
				// 	},
                // ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            messageTop: `
                                From Date: ${from_date}
                                To Date: ${to_date}
                            `,
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            orientation : 'landscape',
                            pageSize : 'LEGAL',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            // action: newexportaction,
                                exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                ],
                // responsive: {
				// 	details: {
				// 		display: $.fn.dataTable.Responsive.display.childRowImmediate,
				// 		type: 'column',
				// 	}
				// },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Absentees</h6>');
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id +' - '+ value.employee_code+ ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            $("#department").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $("#section").change(function() {
                var optVal = $(this).val();

                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                        type : 'component_by_month',
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id +' - '+ value.employee_code+ ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
        }); 
        
    </script>
@endsection