@extends('Admin.layouts.master')
@section('title', 'Attendance Report')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">@lang('app.attendance_report')</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('app.Dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@lang('app.attendance')</a>
                                </li>
                                <li class="breadcrumb-item active">@lang('app.attendance_report')
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="employees-payroll-tab"
                            href="{{ route('attendance_report.index') }}?type={{ request('type') }}">@lang('app.attendance_report')</a>
                    </li>
                    @if (userRoles() != 'Self Service')
                        <li class="nav-item">
                            <a class="nav-link" id="employers-column-tab"
                                href="{{ route('attendance_report.index') }}?type=multiple">@lang('app.Attendance_Report_Multiple')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="employers-column-tab"
                                href="{{ route('attendance_report.index') }}?type=staffWiseAttendanceReport">@lang('app.Staff_Wise_Report')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="employers-column-tab"
                                href="{{ route('attendance_report.index') }}?type=Minimized">@lang('app.Minimized_Report')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="employers-column-tab"
                                href="{{ route('attendance_report.index') }}?type=from_to_date">@lang('app.From_&_to_Date_Report')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="employers-column-tab"
                                href="{{ route('attendance_report.index') }}?type=day_wise_hours">@lang('app.Day_Wise_Hours_Report')</a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content">
                    <!-- Maximize Attendance Report -->
                    <div class="tab-pane active" id="employees-payroll" aria-labelledby="employees-payroll-tab"
                        role="tabpanel">
                        <section>
                            @if (userRoles() != 'Self Service')
                                <!-- Search Form -->
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card mb-1">
                                            <div class="card-body">
                                                <form id="search_form" action="{{ url('attendance_report') }}"
                                                    method="GET">
                                                    <input type="hidden" name="type" value="{{ request('type') }}">
                                                    <div class="row">
                                                        @if (env('COMPANY') == 'CLINIX')
                                                            <div class="col-md-3 col-12">
                                                                <label class="form-label" for="type">@lang('app.Type')</label>
                                                                <select name="typeFilter" id="typeFilter" class="select2 form-select" data-placeholder="@lang('app.Select_Type')">
                                                                    <option value="">@lang('app.Select_Type')</option>
                                                                    <option value="Company" {{ request('typeFilter') == 'Company' ? 'selected' : '' }}>@lang('app.Company')</option>
                                                                    <option value="Franchise" {{ request('typeFilter') == 'Franchise' ? 'selected' : '' }}>Franchise</option>
                                                                    <option value="Head Office" {{ request('typeFilter') == 'Head Office' ? 'selected' : '' }}>@lang('app.Head_Office')</option>
                                                                    <option value="Regional Office" {{ request('typeFilter') == 'Regional Office' ? 'selected' : '' }}>@lang('app.Regional_Office')</option>
                                                                    <option value="Department" {{ request('typeFilter') == 'Department' ? 'selected' : '' }}>@lang('app.Department')</option>
                                                                    <option value="Unit" {{ request('typeFilter') == 'Unit' ? 'selected' : '' }}>@lang('app.Unit')</option>
                                                                    <option value="Sub Unit" {{ request('typeFilter') == 'Sub Unit' ? 'selected' : '' }}>@lang('app.Sub_Unit')</option>
                                                                    <option value="Other" {{ request('typeFilter') == 'Other' ? 'selected' : '' }}>@lang('app.Other')</option>
                                                                </select>
                                                            </div>
                                                        @endif
                                                        <div class="col-md-3 col-12">
                                                            <label class="form-label"
                                                                for="departmentFilter">@lang('app.Filter_By_Department')</label>
                                                            <select name="departmentFilter" id="departmentFilter"
                                                                class="select2 form-select"
                                                                data-placeholder="@lang('app.Select_Department')" required>
                                                                <option value=""></option>
                                                                <option value="all"
                                                                    {{ request('departmentFilter') == 'all' ? 'selected' : '' }}>
                                                                    All Departments</option>
                                                                @foreach (departments() as $department)
                                                                    <option value="{{ $department->id }}"
                                                                        {{ $department->id == request('departmentFilter') ? 'selected' : '' }}>
                                                                        {{ $department->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        @if (env('COMPANY') == 'JSML')
                                                            <div class="col-md-3 col-12">
                                                                <div class="mb-1">
                                                                    <label class="form-label"
                                                                        for="title">@lang('app.Section')</label>
                                                                    <select name="section[]" id="section"
                                                                        data-placeholder="@lang('app.Select_Section')"
                                                                        class="select2 form-select" multiple required>

                                                                    </select>
                                                                    <div class="button-container mt-1">
                                                                        <button class="btn btn-sm btn-primary"
                                                                            type="button"
                                                                            onclick="selectAll('#section')">@lang('app.Select_All')</button>
                                                                        <button class="btn btn-sm btn-danger" type="button"
                                                                            onclick="deselectAll('#section')">@lang('app.Deselect_All')</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-12">
                                                                <label class="form-label"
                                                                    for="employeeFilter">@lang('app.Filter_By_Employee'):</label>
                                                                <select name="employeeFilter" id="employeeFilter"
                                                                    class="select2 form-select"
                                                                    data-placeholder="@lang('app.Select_Employee')">
                                                                    <option value=""></option>
                                                                    @foreach (employees() as $item)
                                                                        <option value="{{ $item->id }}"
                                                                            {{ $item->id == request('employeeFilter') ? 'selected' : '' }}>
                                                                            {{ $item->employee_id . ' - ' . $item->employee_code . ' - ' . $item->first_name . ' ' . $item->last_name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        @else
                                                            <div class="col-md-3 col-12">
                                                                <label class="form-label"
                                                                    for="employeeFilter">@lang('app.Filter_By_Employee'):</label>
                                                                <select name="employeeFilter" id="employeeFilter"
                                                                    class="select2 form-select"
                                                                    data-placeholder="@lang('app.Select_Employee')">
                                                                    <option value=""></option>
                                                                    @if (!empty(request('departmentFilter')))
                                                                        @foreach (employees(request('departmentFilter')) as $item)
                                                                            <option value="{{ $item->id }}"
                                                                                {{ $item->id == request('employeeFilter') ? 'selected' : '' }}>
                                                                                {{ $item->employee_id . ' - ' . $item->employee_code . ' - ' . $item->first_name . ' ' . $item->last_name }}
                                                                            </option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        @endif
                                                        <div class="col-md-3 col-12">
                                                            <label class="form-label"
                                                                for="designationFilter">@lang('app.Filter_By_Designation'):</label>
                                                            <select name="designationFilter" id="designationFilter"
                                                                class="select2 form-select"
                                                                data-placeholder="@lang('app.Select_Designation')">
                                                                <option value=""></option>
                                                                <option value="all"
                                                                    {{ request('designationFilter') == 'all' ? 'selected' : '' }}>
                                                                    All</option>
                                                                @foreach (designation() as $designation)
                                                                    <option value="{{ $designation->id }}"
                                                                        {{ $designation->id == request('designationFilter') ? 'selected' : '' }}>
                                                                        {{ $designation->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3 col-12">
                                                            <label class="form-label"
                                                                for="employmentStatusFilter">@lang('app.Filter_By_Employment_Status'):</label>
                                                            <select name="employmentStatusFilter[]"
                                                                id="employmentStatusFilter" class="select2 form-select"
                                                                data-placeholder="@lang('app.Select_Status')" multiple>
                                                                <option value=""></option>
                                                                @foreach (getAllEmployementStatuses() as $status)
                                                                    <option value="{{ $status->id }}"
                                                                        {{ in_array($status->id, request('employmentStatusFilter') ?? []) ? 'selected' : '' }}>
                                                                        {{ $status->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3 col-12">
                                                            <label class="form-label"
                                                                for="shiftFilter">@lang('app.Filter_By_Shift'):</label>
                                                            <select name="shiftFilter" id="shiftFilter"
                                                                class="select2 form-select"
                                                                data-placeholder="@lang('app.Select_Shift')">
                                                                <option value=""></option>
                                                                @foreach (getAllShifts() as $shift)
                                                                    <option value="{{ $shift->id }}"
                                                                        {{ $shift->id == request('shiftFilter') ? 'selected' : '' }}>
                                                                        {{ $shift->shift_desc }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3 col-12">
                                                            <label class="form-label"
                                                                for="date">@lang('app.Filter_By_Month'):</label>
                                                            <input type="month" id="date"
                                                                class="form-control date" name="dateFilter"
                                                                value="{{ empty(request('dateFilter')) ? date('Y-m') : request('dateFilter') }}"
                                                                required />
                                                        </div>
                                                        <div class="col-md-3 col-12">
                                                            <label class="form-label"
                                                                for="singleDate">@lang('app.Filter_By_Date:')</label>
                                                            <input type="text" name="singleDate" id="singleDate"
                                                                class="form-control flatpickr-basic singleDate"
                                                                value="{{ request('singleDate') }}"
                                                                placeholder="YYYY-MM-DD" required>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <input type="checkbox" class="form-check-input mt-1"
                                                                name="old_attendance" id="old_attendance" value="1"
                                                                {{ request()->old_attendance == 1 ? 'checked' : '' }} />
                                                            <label class="custom-control-label mt-1"
                                                                for="old_attendance">@lang('app.Is_old_Attendance')</label>
                                                        </div>
                                                        <div class="col-6 text-end">
                                                            <a href="{{ url('attendance_report') }}?type={{ request('type') }}"
                                                                type="button"
                                                                class="btn btn-danger mt-1">@lang('app.Reset')</a>
                                                            <button type="submit"
                                                                class="btn btn-primary mt-1">@lang('app.Apply')</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Search Form End -->
                            @endif
                            <!-- Report Start -->
                            <div class="card pb-1">
                                <div class="card-header border-bottom p-1">
                                    <div class="head-label">
                                        <div class="mb-0">
                                            <h6>@lang('app.attendance_report')</h6>
                                        </div>
                                    </div>
                                    @if (isset($employees) && count($employees) > 0)
                                        <div class="dt-action-buttons text-end">
                                            <div class="dt-buttons d-inline-flex">
                                                <div class="dt-buttons d-inline-flex">
                                                    <button type="button" class="btn btn-primary export"
                                                        id="pdf">Download Pdf</button>
                                                </div>
                                            </div>
                                            <div class="dt-buttons d-inline-flex">
                                                <button type="button" class="btn btn-success export"
                                                    id="excel">Download Excel</button>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>Employee ID</th>
                                                <th>Code</th>
                                                <th>Employee</th>
                                                <th>Desigination</th>
                                                @if (env('COMPANY') == 'JSML')
                                                    <th>Section</th>
                                                @else
                                                    <th>Department</th>
                                                @endif
                                                @for ($start = $start_date; $start <= $end_date; $start++)
                                                    <th colspan="2">{{ date('d', strtotime($start)) }}
                                                        ({{ date('D', strtotime($start)) }})</th>
                                                @endfor
                                                <th>Total Overtime Hours</th>
                                                <th>Total Working Hours</th>
                                                @if (env('COMPANY') == 'HEALTHWISE')
                                                    <th>Total Late Hours</th>
                                                @endif
                                                <th>Total Days Late</th>
                                                <th>Total Leaves</th>
                                                <th>Total Absents</th>
                                                <th>Total Off days in month</th>
                                                <th>Total Presents</th>
                                                @if (env('COMPANY') != 'JSML')
                                                    <th>Total Days</th>
                                                @endif
                                                @if (env('COMPANY') == 'JSML')
                                                    <th>Total Working Days</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody style="white-space: nowrap">
                                            @if (isset($employees) && count($employees) > 0)
                                                @php
                                                    $count = 1;
                                                @endphp
                                                @foreach ($employees as $employee)
                                                    @if (!array_key_exists($employee->id, $employe_attendance) || count($employe_attendance[$employee->id]) === 9)
                                                        @continue
                                                    @endif
                                                    <tr>
                                                        <td>{{ $count }}</td>
                                                        <td>{{ $employee->employee_id }}</td>
                                                        <td>{{ HandleEmpty($employee->employee_code) }}</td>
                                                        <td>{{ $employee->first_name }} {{ $employee->middle_name }}
                                                            {{ $employee->last_name }}</td>
                                                        <td>{{ $employee->desigination }}</td>
                                                        <td>{{ $employee->title }}</td>
                                                        @for ($date = $start_date; $date <= $end_date; $date++)
                                                            @if (!array_key_exists($date, $employe_attendance[$employee->id]))
                                                                <td colspan="2"></td>
                                                                @continue
                                                            @endif
                                                            @if (count($employe_attendance[$employee->id][$date]) > 2 &&
                                                                    array_key_exists('in_time', $employe_attendance[$employee->id][$date]))
                                                                @php
                                                                    if (
                                                                        $employe_attendance[$employee->id][$date][
                                                                            'is_manual'
                                                                        ] == 1
                                                                    ) {
                                                                        $color = 'yellow';
                                                                    } else {
                                                                        $color = '';
                                                                    }

                                                                    if (
                                                                        $employe_attendance[$employee->id][$date][
                                                                            'in_time'
                                                                        ] >
                                                                            $employe_attendance[$employee->id][$date][
                                                                                'grace_period'
                                                                            ] &&
                                                                        $employe_attendance[$employee->id][$date][
                                                                            'in_time'
                                                                        ] < '23:00:00' &&
                                                                        $employe_attendance[$employee->id][$date][
                                                                            'shift_desc'
                                                                        ] != 'SS (0001 ~ 2359)'
                                                                    ) {
                                                                        $tcolor = 'red';
                                                                    } else {
                                                                        $tcolor = 'green';
                                                                    }
                                                                @endphp
                                                                <td colspan="2">
                                                                    @if (env('COMPANY') == 'UNICORN' && in_array($employe_attendance[$employee->id][$date]['shift_desc'], getunicornDoubleAttendanceShiftsNames()))
                                                                        <label style="font-size: 12px;">In Time:</label>
                                                                        <span
                                                                            style="background:{{ $color }};color:{{ $tcolor }};">
                                                                            {{ $employe_attendance[$employee->id][$date]['in_time'][0] }}
                                                                        </span>
                                                                        <br>
                                                                        <label style="font-size: 12px;">Out Time:</label>
                                                                        @if ($employe_attendance[$employee->id][$date]['out_time'][0] == 'Nil')
                                                                            <span style="background-color: #A52A2A">
                                                                                Missing CheckOut
                                                                            </span>
                                                                        @else
                                                                            <span>
                                                                                {{ $employe_attendance[$employee->id][$date]['out_time'][0] }}
                                                                            </span>
                                                                        @endif
                                                                        <br>
                                                                        @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['in_time']))
                                                                            <label style="font-size: 12px;">In
                                                                                Time:</label>
                                                                            <span
                                                                                style="background:{{ $color }};color:{{ $tcolor }};">
                                                                                {{ $employe_attendance[$employee->id][$date]['in_time'][1] }}
                                                                            </span>
                                                                            <br>
                                                                        @endif
                                                                        @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['out_time']))
                                                                            <label style="font-size: 12px;">Out
                                                                                Time:</label>
                                                                            @if ($employe_attendance[$employee->id][$date]['out_time'][1] == 'Nil')
                                                                                <span style="background-color: #A52A2A">
                                                                                    Missing CheckOut
                                                                                </span>
                                                                            @else
                                                                                <span>
                                                                                    {{ $employe_attendance[$employee->id][$date]['out_time'][1] }}
                                                                                </span>
                                                                            @endif
                                                                        @endif
                                                                    @else
                                                                        <label style="font-size: 12px;">In Time:</label>
                                                                        <span
                                                                            style="background:{{ $color }};color:{{ $tcolor }};">
                                                                            {{ date('H:i', strtotime($employe_attendance[$employee->id][$date]['in_time'])) }}
                                                                        </span>
                                                                        <br>
                                                                        <label style="font-size: 12px;">Out Time:</label>
                                                                        @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                                            <span style="background-color: #A52A2A">
                                                                                Missing CheckOut
                                                                            </span>
                                                                        @elseif(env('COMPANY') == 'CLINIX' && isset($employe_attendance[$employee->id][$date]['color']))
                                                                            <span
                                                                                style="background-color: orange; color:white;">
                                                                                LCO
                                                                                ({{ date('H:i', strtotime($employe_attendance[$employee->id][$date]['out_time'])) }})
                                                                            </span>
                                                                        @else
                                                                            <span>
                                                                                {{ date('H:i', strtotime($employe_attendance[$employee->id][$date]['out_time'])) }}
                                                                            </span>
                                                                            @if (env('COMPANY') == 'JSML' && array_key_exists('od', $employe_attendance[$employee->id][$date]))
                                                                                <span class="badge bg-success">OD</span>
                                                                            @endif
                                                                            @if (env('COMPANY') == 'JSML' && array_key_exists('half_leave_flag', $employe_attendance[$employee->id][$date]))
                                                                                <span class="badge bg-success">HL</span>
                                                                            @endif
                                                                            @if (env('COMPANY') == 'JSML' && array_key_exists('ret', $employe_attendance[$employee->id][$date]))
                                                                                <span class="badge bg-success">RET</span>
                                                                            @endif
                                                                        @endif
                                                                    @endif

                                                                    <br>
                                                                    <br>
                                                                    @if (env('COMPANY') == 'JSML' && isset($employe_attendance[$employee->id][$date]['short_leave']))
                                                                        <label style="font-size: 12px;">SL: </label>
                                                                        <span>{{ $employe_attendance[$employee->id][$date]['short_leave'] }}</span>
                                                                        <br>
                                                                    @endif
                                                                    <label style="font-size: 12px;">Work Hrs:</label>
                                                                    <span>{{ number_format($employe_attendance[$employee->id][$date]['working_hours'], 2) }}</span>
                                                                    <br>
                                                                    <label style="font-size: 12px;">Department:</label>
                                                                    <span>{{ departments($employe_attendance[$employee->id][$date]['department_id'])->title }}</span>
                                                                    <br>
                                                                    <hr>
                                                                    @if (env('COMPANY') == 'UNICORN' && in_array($employe_attendance[$employee->id][$date]['shift_desc'], getunicornDoubleAttendanceShiftsNames()))
                                                                        {{-- first attendance --}}
                                                                        <label style="font-size: 12px;">In Date:</label>
                                                                        <span>
                                                                            {{ $employe_attendance[$employee->id][$date]['in_datetime'][0] }}
                                                                        </span>
                                                                        <br>
                                                                        @if ($employe_attendance[$employee->id][$date]['distance_in'][0] != null)
                                                                            @if ($employe_attendance[$employee->id][$date]['distance_in'][0] < 100)
                                                                                <span
                                                                                    style="background-color: green; color:white;">Location
                                                                                    In
                                                                                    ({{ number_format($employe_attendance[$employee->id][$date]['distance_in'][0], 2) }})</span>
                                                                            @else
                                                                                <span
                                                                                    style="background-color: red; color:white;">Location
                                                                                    In
                                                                                    ({{ number_format($employe_attendance[$employee->id][$date]['distance_in'][0], 2) }})</span>
                                                                            @endif
                                                                            <br>
                                                                        @endif
                                                                        <label style="font-size: 12px;">Out Date:</label>
                                                                        @if ($employe_attendance[$employee->id][$date]['out_time'][0] == 'Nil')
                                                                            <span style="background-color: #A52A2A">
                                                                                Missing CheckOut
                                                                            </span>
                                                                        @else
                                                                            <span>
                                                                                {{ $employe_attendance[$employee->id][$date]['out_datetime'][0] }}
                                                                            </span>
                                                                        @endif
                                                                        <br>
                                                                        @if ($employe_attendance[$employee->id][$date]['distance_out'][0] != null)
                                                                            @if ($employe_attendance[$employee->id][$date]['distance_out'][0] < 100)
                                                                                <span
                                                                                    style="background-color: green; color:white;">Location
                                                                                    Out
                                                                                    ({{ number_format($employe_attendance[$employee->id][$date]['distance_out'][0], 2) }})</span>
                                                                            @else
                                                                                <span
                                                                                    style="background-color: red; color:white;">Location
                                                                                    Out
                                                                                    ({{ number_format($employe_attendance[$employee->id][$date]['distance_out'][0], 2) }})</span>
                                                                            @endif
                                                                            <br>
                                                                        @endif
                                                                        {{-- second attendance --}}
                                                                        @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['in_datetime']))
                                                                            <label style="font-size: 12px;">In
                                                                                Date:</label>
                                                                            <span>
                                                                                {{ $employe_attendance[$employee->id][$date]['in_datetime'][1] }}
                                                                            </span>
                                                                            <br>
                                                                            @if ($employe_attendance[$employee->id][$date]['distance_in'][1] != null)
                                                                                @if ($employe_attendance[$employee->id][$date]['distance_in'][1] < 100)
                                                                                    <span
                                                                                        style="background-color: green; color:white;">Location
                                                                                        In
                                                                                        ({{ number_format($employe_attendance[$employee->id][$date]['distance_in'][1], 2) }})</span>
                                                                                @else
                                                                                    <span
                                                                                        style="background-color: red; color:white;">Location
                                                                                        In
                                                                                        ({{ number_format($employe_attendance[$employee->id][$date]['distance_in'][1], 2) }})</span>
                                                                                @endif
                                                                                <br>
                                                                            @endif
                                                                        @endif
                                                                        @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['out_datetime']))
                                                                            <label style="font-size: 12px;">Out
                                                                                Date:</label>
                                                                            @if ($employe_attendance[$employee->id][$date]['out_datetime'][1] == 'Nil')
                                                                                <span style="background-color: #A52A2A">
                                                                                    Missing CheckOut
                                                                                </span>
                                                                            @else
                                                                                <span>
                                                                                    {{ $employe_attendance[$employee->id][$date]['out_datetime'][1] }}
                                                                                </span>
                                                                            @endif
                                                                            <br>
                                                                            @if ($employe_attendance[$employee->id][$date]['distance_out'][1] != null)
                                                                                @if ($employe_attendance[$employee->id][$date]['distance_out'][1] < 100)
                                                                                    <span
                                                                                        style="background-color: green; color:white;">Location
                                                                                        Out
                                                                                        ({{ number_format($employe_attendance[$employee->id][$date]['distance_out'][1], 2) }})</span>
                                                                                @else
                                                                                    <span
                                                                                        style="background-color: red; color:white;">Location
                                                                                        Out
                                                                                        ({{ number_format($employe_attendance[$employee->id][$date]['distance_out'][1], 2) }})</span>
                                                                                @endif
                                                                                <br>
                                                                            @endif
                                                                        @endif
                                                                    @else
                                                                        <label style="font-size: 12px;">In Date:</label>
                                                                        <span>
                                                                            {{ date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime'])) }}
                                                                        </span>
                                                                        <br>
                                                                        @if ($employe_attendance[$employee->id][$date]['distance_in'] != null && env('COMPANY') == 'UNICORN')
                                                                            @if ($employe_attendance[$employee->id][$date]['distance_in'] < 100)
                                                                                <span
                                                                                    style="background-color: green; color:white;">Location
                                                                                    In
                                                                                    ({{ number_format($employe_attendance[$employee->id][$date]['distance_in'], 2) }})</span>
                                                                            @else
                                                                                <span
                                                                                    style="background-color: red; color:white;">Location
                                                                                    In
                                                                                    ({{ number_format($employe_attendance[$employee->id][$date]['distance_in'], 2) }})</span>
                                                                            @endif
                                                                            <br>
                                                                        @endif
                                                                        <label style="font-size: 12px;">Out Date:</label>
                                                                        @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                                            <span style="background-color: #A52A2A">
                                                                                Missing CheckOut
                                                                            </span>
                                                                        @else
                                                                            <span>
                                                                                {{ date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime'])) }}
                                                                            </span>
                                                                        @endif
                                                                        <br>
                                                                        @if ($employe_attendance[$employee->id][$date]['distance_out'] != null && env('COMPANY') == 'UNICORN')
                                                                            @if ($employe_attendance[$employee->id][$date]['distance_out'] < 100)
                                                                                <span
                                                                                    style="background-color: green; color:white;">Location
                                                                                    Out
                                                                                    ({{ number_format($employe_attendance[$employee->id][$date]['distance_out'], 2) }})</span>
                                                                            @else
                                                                                <span
                                                                                    style="background-color: red; color:white;">Location
                                                                                    Out
                                                                                    ({{ number_format($employe_attendance[$employee->id][$date]['distance_out'], 2) }})</span>
                                                                            @endif
                                                                            <br>
                                                                        @endif
                                                                    @endif
                                                                    <hr>
                                                                    <label style="font-size: 12px;">Shift:</label>
                                                                    <span
                                                                        style="color:#A52A2A">{{ $employe_attendance[$employee->id][$date]['shift_desc'] }}</span>
                                                                    <br>
                                                                    <label style="font-size: 11px;">Shift Start:</label>
                                                                    <span>{{ $employe_attendance[$employee->id][$date]['shift_start_time'] }}</span>
                                                                    <br>
                                                                    <label style="font-size: 12px;">Shift End:</label>
                                                                    <span>{{ $employe_attendance[$employee->id][$date]['shift_end_time'] }}</span>
                                                                    <hr>
                                                                    <label style="font-size: 12px;">Total Work Hrs:</label>
                                                                    <span>{{ number_format($employe_attendance[$employee->id][$date]['total_working_hours'], 2) }}</span>
                                                                    <br>
                                                                    <label style="font-size: 12px;">Overtime Hrs:</label>
                                                                    <span>{{ number_format($employe_attendance[$employee->id][$date]['overtime_hours'], 2) }}</span>
                                                                </td>
                                                            @else
                                                                <td colspan="2"><span class="px-2"
                                                                        style="color:white; background: {{ $employe_attendance[$employee->id][$date]['absent_value_color'] }};cursor:pointer"
                                                                        title="{{ $employe_attendance[$employee->id][$date]['absentMessageGuide'] }}"
                                                                        data-toggle="tooltip"
                                                                        data-placement="top">{{ $employe_attendance[$employee->id][$date]['absent_value'] }}</span>
                                                                </td>
                                                            @endif
                                                        @endfor
                                                        <td>{{ number_format($employe_attendance[$employee->id]['total_overtime_hours'], 2) }}
                                                        </td>
                                                        <td>{{ number_format($employe_attendance[$employee->id]['total_working_hours'], 2) }}
                                                        </td>
                                                        @if (env('COMPANY') == 'HEALTHWISE')
                                                            <td>{{ $employe_attendance[$employee->id]['total_late_hours'] }}
                                                        </td>
                                                        @endif
                                                        <td>{{ number_format($employe_attendance[$employee->id]['total_days_late']) }}
                                                        </td>
                                                        <td onclick="leave_check({{ $employee->id }})"
                                                            style="cursor: pointer; color:cornflowerblue;">
                                                            {{ $employe_attendance[$employee->id]['total_leaves'] }}</td>
                                                        <td>{{ number_format($employe_attendance[$employee->id]['total_days_absent']) }}
                                                        </td>
                                                        <td>{{ number_format($employe_attendance[$employee->id]['total_off_days']) }}
                                                        </td>
                                                        <td>{{ $employe_attendance[$employee->id]['total_days_present'] }}
                                                        </td>
                                                        @if (env('COMPANY') != 'JSML')
                                                            <td>
                                                                {{ $employe_attendance[$employee->id]['total_days_present'] +
                                                                    $employe_attendance[$employee->id]['total_leaves'] +
                                                                    $employe_attendance[$employee->id]['total_off_days'] }}
                                                            </td>
                                                        @endif
                                                        @if (env('COMPANY') == 'JSML')
                                                            <td>
                                                                {{ $employe_attendance[$employee->id]['total_working_days'] }}
                                                            </td>
                                                        @endif
                                                    </tr>
                                                    @php
                                                        $count += 1;
                                                    @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="10" class="text-center">No Record Found</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- Report End -->
                        </section>
                    </div>
                </div>
            </section>
            <div class="modal fade text-start" id="leave_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title modal_heading" id="myModalLabel17"></h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <h4 class="text-center font-weight-bold" id="emp_name"></h4>
                            <div class="row">
                                <div class="col-12">
                                    <div id="leave_table" class="table-responsive">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @php
        $departmentFilter = request('departmentFilter');
        $section = request('section');
    @endphp
@endsection
@section('scripts')
    <script>
        var rowid;
        var departmentFilter = @json($departmentFilter);
        var section = @json($section);
        var employee = @json(request('employeeFilter'));
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>');
                            $.each(response, function(index, value) {
                                var selected = (section ? section : []).includes('' +
                                    value.id + '') ? 'selected' : '';
                                $('#section').append(
                                    $('<option ' + selected + '></option>').val(
                                        value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            $("#typeFilter").change(function() {
                var optVals = $(this).val();
                if (optVals.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_by_type') }}",
                        data: {
                            typeFilter: optVals,
                        },
                        success: function(response) {
                            $('#departmentFilter').empty();
                            $('#departmentFilter').html('<option value="">Select Department</option>');
                            response.forEach(function(item) {
                                $('#departmentFilter').append(
                                    $('<option></option>').val(item.id).html(item.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });
            @if (env('COMPANY') == 'JSML')
                $("#section").change(function() {
                    var optVal = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_employees_from_section') }}",
                        data: {
                            department_id: optVal,
                            type: 'attendance_report',
                        },
                        success: function(response) {
                            $('#employeeFilter').empty();
                            $('#employeeFilter').html(
                                '<option value="">Select Employee</option>');
                            $.each(response, function(index, value) {
                                var selected = employee == value.id ? 'selected' : '';
                                $('#employeeFilter').append(
                                    $('<option ' + selected + '></option>').val(
                                        value.id).html(
                                        value.employee_id + ' - ' + value
                                        .employee_code + ' - ' + value.first_name +
                                        ' ' + value.middle_name + ' ' + value
                                        .last_name + ' - ' + value.designation +
                                        ' - ' + value.department)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                });
            @else
                $("#departmentFilter").change(function() {
                    var optVal = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employeeFilter').empty();
                            $('#employeeFilter').html(
                                '<option value="">Select Employee</option>');
                            $.each(response, function(index, value) {
                                var selected = employee == value.id ? 'selected' : '';
                                $('#employeeFilter').append(
                                    $('<option ' + selected + '></option>').val(
                                        value.id).html(
                                        value.employee_id + ' - ' + value
                                        .employee_code + ' - ' + value.first_name +
                                        ' ' + value.middle_name + ' ' + value
                                        .last_name + ' - ' + value.designation +
                                        ' - ' + value.department)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                });
            @endif
            $('.date').change(function() {
                $('.singleDate').prop('required', false);
                $('.singleDate').prop('disabled', true);
            })
            $('#singleDate').change(function() {
                $('.date').prop('required', false);
                $('.date').prop('disabled', true);
                $('.date').val('');
            });
            $('#departmentFilter').trigger("change");
        });
        $(document).on('click', '.export', function() {
            blockUI();
            var export_type = $(this).attr('id');
            var size_pdf = '';
            if (export_type == 'mini_pdf') {
                size_pdf = 'cidpdf';
            }
            $.ajax({
                type: "GET",
                url: "{{ url('attendance_report') }}",
                data: {
                    export_type: export_type,
                    size_pdf: size_pdf,
                    type: "{!! request('type') !!}",
                    departmentFilter: departmentFilter,
                    employeeFilter: "{!! request('employeeFilter') !!}",
                    designationFilter: "{!! request('designationFilter') !!}",
                    section: section,
                    dateFilter: "{!! request('dateFilter') !!}",
                    old_attendance: "{!! request('old_attendance') !!}",
                    shiftFilter: "{!! request('shiftFilter') !!}",
                    singleDate: "{!! request('singleDate') !!}"
                },
                success: function(response, status, xhr) {
                    $.unblockUI();
                    window.open(this.url);
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });

        function leave_check(id) {
            rowid = id;
            var date = '{!! request()->query('dateFilter') !!}';
            if (date == '') {
                date = '{!! request()->query('singleDate') !!}';
            }
            $.ajax({
                url: "{{ url('attendance_report') }}" + "/" + id,
                type: "GET",
                data: {
                    date: date
                },
                success: function(response) {
                    var html = '';
                    html = `
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>    
                                    <th>Employee</th>    
                                    <th>Leave Type</th>    
                                    <th>Leave Start Date</th>  
                                    <th>Leave End Date</th>    
                                    <th>Leave Status</th>    
                                    <th>Created By</th>    
                                </tr>
                            </thead>    
                        <tbody>
                    `;
                    if (response.leaves.length > 0 || response.short_leaves.length > 0) {
                        $.each(response.leaves, function(index, value) {
                            html += `
                                    <tr>
                                        <td>${index + 1}</td>    
                                        <td>${value.first_name} ${value.last_name}</td>    
                                        <td>${value.leave}</td>    
                                        <td>${value.date_start}</td>    
                                        <td>${value.date_end}</td>    
                                        <td>${value.status}</td>    
                                        <td>${value.username}</td>    
                                    </tr>
                                `;
                        });
                        $.each(response.short_leaves, function(index, value) {
                            html += `
                                    <tr>
                                        <td>${index + 1}</td>    
                                        <td>${value.employee.first_name} ${value.employee.last_name}</td>    
                                        <td>${value.leave_type.name} (Short Leave)</td>    
                                        <td>${value.date}: ${value.time_from}</td>    
                                        <td>${value.date}: ${value.time_to}</td>    
                                        <td>${value.status}</td>    
                                        <td>${value.user.username}</td>    
                                    </tr>
                                `;
                        });
                    } else {
                        html += `<tr>
                                    <td colspan='7' class='text-center'>No Record Found</td>
                                </tr>`;
                    }
                    $('.modal_heading').html(`Leave On <span class='fw-bolder'>${date}</span>`);
                    $('#leave_table').html(html);
                    $("#leave_modal").modal('show');
                }
            });
        }
    </script>
@endsection
