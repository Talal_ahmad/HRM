@extends('Admin.layouts.master')
@section('title', 'Attendance Report')
@section('style')
    <style>
        #dataTable td, #dataTable th{
            padding: 2px;
            font-size: 12px;
        }
        .px-2 {
                padding-right: 1.5rem !important;
                padding-left: 1.5rem !important;
            }
        hr {
            margin: 2px !important;
        }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Attendance Report</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Attendance</a>
                                </li>
                                <li class="breadcrumb-item active">Attendance Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="employees-payroll-tab" href="{{route('attendance_report.index')}}?type=single">Attendance Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=multiple">Attendance Report Multiple</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=staffWiseAttendanceReport">Staff Wise Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=Minimized">Minimized Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=from_to_date">From & to Date Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=day_wise_hours">Day Wise Hours Report</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- Maximize Attendance Report -->
                    <div class="tab-pane active" id="employees-payroll" aria-labelledby="employees-payroll-tab" role="tabpanel">
                        <section>
                            <!-- Search Form -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="card mb-1">
                                        <div class="card-body">
                                            <form id="search_form" action="{{url('attendance_report')}}" method="GET">
                                                <input type="hidden" name="type" value="{{request('type')}}">
                                                <div class="row">
                                                    <div class="col-md-3 col-12">
                                                        <label class="form-label" for="departmentFilter">Filter By department:</label>
                                                        <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" required>
                                                            <option value=""></option>
                                                            <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : '' }}>All Departments</option>
                                                            @foreach (departments() as $department)
                                                                <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <label class="form-label" for="employeeFilter">Filter By Employee:</label>
                                                        <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="Select Employee">
                                                            <option value=""></option>
                                                            @if (!empty(request('departmentFilter')))
                                                                @foreach (employees(request('departmentFilter')) as $item)
                                                                <option value="{{$item->id}}" {{$item->id == request('employeeFilter') ? 'selected' : ''}}>{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <label class="form-label" for="date">Filter By Month:</label>
                                                        <input type="month" id="date" class="form-control date"  name="dateFilter" value="{{empty(request('dateFilter')) ? date('Y-m') : request('dateFilter')}}" required/>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <label class="form-label" for="singleDate">Filter By Date:</label>
                                                        <input type="text" name="singleDate" id="singleDate" class="form-control flatpickr-basic singleDate" placeholder="YYYY-MM-DD" required>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <input type="checkbox" class="form-check-input mt-1" name="old_attendance" id="old_attendance" value="1" {{ request()->old_attendance == 1 ? 'checked' : '' }}/>
                                                        <label class="custom-control-label mt-1" for="old_attendance">Is old Attendance</label>
                                                    </div>
                                                    <div class="col-6 text-end">
                                                        <a href="{{url('attendance_report')}}?type={{request('type')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Search Form End -->

                            <!-- Report Start -->
                            <div class="card pb-1">
                                <div class="card-header border-bottom p-1">
                                    <div class="head-label">
                                        <div class="mb-0">
                                            <h6>Attendance Report</h6>
                                        </div>
                                    </div>
                                    @if (isset($employees) && count($employees) > 0)
                                    <div class="dt-action-buttons text-end">
                                        <div class="dt-buttons d-inline-flex">
                                            <div class="dt-buttons d-inline-flex">
                                                <button type="button" class="btn btn-primary export" id="pdf">Download Pdf</button>
                                            </div>
                                        </div>
                                        <div class="dt-buttons d-inline-flex">
                                            <button type="button" class="btn btn-success export" id="excel">Download Excel</button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>EMP.</th>
                                                <th>DEPT.</th>
                                                @for ($start = $start_date; $start <= $end_date; $start++)
                                                    <th colspan="2">{{date('d', strtotime($start))}} ({{date('D' , strtotime($start))}})</th>
                                                @endfor
                                                <th>O.H.</th>
                                                <th>W.H.</th>
                                                <th>Late</th>
                                                <th>Overall Working Days</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (isset($employees) && count($employees) > 0)
                                                @foreach ($employees as $key => $employee)
                                                <tr>
                                                    <td>{{$employee->employee_id.'-'.HandleEmpty($employee->employee_code).'-'.$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name.'-'.$employee->desigination}}</td>
                                                    <td>{{$employee->title}}</td>
                                                    @for ($date = $start_date; $date <= $end_date; $date++)
                                                        @if (count($employe_attendance[$employee->id][$date]) > 2 && array_key_exists('in_time',$employe_attendance[$employee->id][$date]))
                                                            @php
                                                                if($employe_attendance[$employee->id][$date]['is_manual'] == 1)
                                                                {
                                                                    $color = 'yellow';
                                                                }
                                                                else {
                                                                    $color = '';
                                                                }

                                                                if($employe_attendance[$employee->id][$date]['in_time'] > $employe_attendance[$employee->id][$date]['grace_period'])
                                                                {
                                                                    $tcolor = 'red';
                                                                }
                                                                else {
                                                                    $tcolor = 'green';
                                                                }
                                                            @endphp
                                                            <td colspan="2">
                                                                @if (env('COMPANY') == 'UNICORN' && in_array($employe_attendance[$employee->id][$date]['shift_desc'], getunicornDoubleAttendanceShiftsNames()))
                                                                <span style="background:{{$color}};color:{{$tcolor}};">
                                                                    {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['in_time'][0]))}}
                                                                </span>
                                                                <br>
                                                                @if ($employe_attendance[$employee->id][$date]['out_time'][0] == 'Nil')
                                                                <span style="background-color: #A52A2A">
                                                                    Missing CheckOut
                                                                </span>
                                                                @else
                                                                <span>
                                                                    {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['out_time'][0]))}}
                                                                </span>
                                                                @endif
                                                                @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['in_time']))
                                                                <br>
                                                                <span style="background:{{$color}};color:{{$tcolor}};">
                                                                    {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['in_time'][1]))}}
                                                                </span>
                                                                @endif
                                                                @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['in_time']))
                                                                @if ($employe_attendance[$employee->id][$date]['out_time'][1] == 'Nil')
                                                                <span style="background-color: #A52A2A">
                                                                    Missing CheckOut
                                                                </span>
                                                                @else
                                                                <span>
                                                                    {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['out_time'][1]))}}
                                                                </span>
                                                                @endif
                                                                @endif
                                                                <br>
                                                                <span>{{number_format($employe_attendance[$employee->id][$date]['working_hours'],2)}}</span>
                                                                <br>
                                                                <hr>
                                                                <span>
                                                                    {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime'][0]))}}
                                                                </span>
                                                                <br>
                                                                @if ($employe_attendance[$employee->id][$date]['out_time'][0] == 'Nil')
                                                                <span style="background:yellow;">
                                                                    {{$employe_attendance[$employee->id][$date]['out_time'][0]}}
                                                                </span>
                                                                @else
                                                                <span>
                                                                    {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime'][0]))}}
                                                                </span>
                                                                @endif
                                                                @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['in_datetime']))
                                                                <br>
                                                                <span>
                                                                    {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime'][1]))}}
                                                                </span>
                                                                @endif
                                                                @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['out_datetime']))
                                                                @if ($employe_attendance[$employee->id][$date]['out_time'][1] == 'Nil')
                                                                <span style="background:yellow;">
                                                                    {{$employe_attendance[$employee->id][$date]['out_time'][1]}}
                                                                </span>
                                                                @else
                                                                <span>
                                                                    {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime'][1]))}}
                                                                </span>
                                                                @endif
                                                                @endif
                                                                @else
                                                                <span style="background:{{$color}};color:{{$tcolor}};">
                                                                    {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['in_time']))}}
                                                                </span>
                                                                <br>
                                                                @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                                <span style="background-color: #A52A2A">
                                                                    Missing CheckOut
                                                                </span>
                                                                @elseif(env('COMPANY') == 'CLINIX' && isset($employe_attendance[$employee->id][$date]['color']))
                                                                    <span style="background-color: orange; color:white;">
                                                                        LCO ({{date('H:i', strtotime($employe_attendance[$employee->id][$date]['out_time']))}})
                                                                    </span>
                                                                @else
                                                                <span>
                                                                    {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['out_time']))}}
                                                                </span>
                                                                @if (env('COMPANY') == 'JSML' && array_key_exists('od', $employe_attendance[$employee->id][$date]))
                                                                    <span class="badge bg-success">OD</span>
                                                                @endif
                                                                @if (env('COMPANY') == 'JSML' && array_key_exists('half_leave_flag', $employe_attendance[$employee->id][$date]))
                                                                    <span class="badge bg-success">HL</span>
                                                                @endif
                                                                @endif
                                                                <br>
                                                                <span>{{number_format($employe_attendance[$employee->id][$date]['working_hours'],2)}}</span>
                                                                <br>
                                                                <hr>
                                                                <span>
                                                                    {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime']))}}
                                                                </span>
                                                                <br>
                                                                @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                                <span style="background:yellow;">
                                                                    {{$employe_attendance[$employee->id][$date]['out_time']}}
                                                                </span>
                                                                @else
                                                                <span>
                                                                    {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime']))}}
                                                                </span>
                                                                @endif
                                                                @endif
                                                                <br>
                                                                <hr>
                                                                @if (isset($employe_attendance[$employee->id][$date]['short_leave_time_from']))
                                                                    <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_from']}}</span>
                                                                    <br>
                                                                    <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_to']}}</span>
                                                                    <hr>
                                                                @endif
                                                                <span style="color:#A52A2A">{{$employe_attendance[$employee->id][$date]['shift_desc']}}</span>
                                                                <br>
                                                                <span>{{$employe_attendance[$employee->id][$date]['shift_start_time']}}</span>
                                                                <br>
                                                                <span>{{$employe_attendance[$employee->id][$date]['shift_end_time']}}</span>
                                                                <hr>
                                                                <span>{{number_format($employe_attendance[$employee->id][$date]['total_working_hours'],2)}}</span>
                                                                <br>
                                                                <span>{{number_format($employe_attendance[$employee->id][$date]['overtime_hours'],2)}}</span>
                                                            </td>
                                                        @else
                                                            <td colspan="2"><span class="px-2" style="color:white; background: {{$employe_attendance[$employee->id][$date]['absent_value_color']}}">{{$employe_attendance[$employee->id][$date]['absent_value']}}</span></td>
                                                        @endif
                                                    @endfor
                                                    <td>{{number_format($employe_attendance[$employee->id]['total_overtime_hours'],2)}}</td>
                                                    <td>{{number_format($employe_attendance[$employee->id]['total_working_hours'],2)}}</td>
                                                    <td>{{number_format($employe_attendance[$employee->id]['total_days_late'])}}</td>
                                                    @if (env('COMPANY') == 'JSML')
                                                        <td>
                                                            {{ $employe_attendance[$employee->id]['total_working_days'] }}
                                                        </td>
                                                    @else
                                                        <td>{{$employe_attendance[$employee->id]['over_all_working_days']}}</td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="9" class="text-center">No Record Found</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- Report End -->
                        </section>
                    </div>
                    <!-- End Maximized Report -->
                </div>
            </section>
        </div>
    </section>
    @php
        $departmentFilter = request('departmentFilter');
    @endphp
@endsection
@section('scripts')
    <script>
        var rowid;
        var departmentFilter = @json($departmentFilter);
        $(document).ready(function(){
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>');
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation + ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            $('.date').change(function(){
                $('.singleDate').prop('required',false);
                $('.singleDate').prop('disabled',true);
            })
            $('#singleDate').change(function(){
                $('.date').prop('required',false);
                $('.date').prop('disabled',true);
                $('.date').val('');
            })
        });
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var size_pdf = '';
            var minimized_check = 1;
            if (export_type == 'mini_pdf') {
                size_pdf = 'cidpdf';
            }
            $.ajax({
                type: "GET",
                url: "{{ url('attendance_report') }}",
                data: {
                    export_type : export_type,
                    minimized_check : minimized_check,
                    size_pdf : size_pdf,
                    type: "{!! request('type') !!}",
                    departmentFilter: departmentFilter,
                    employeeFilter: "{!! request('employeeFilter') !!}",
                    dateFilter: "{!! request('dateFilter') !!}",
                    old_attendance: "{!! request('old_attendance') !!}",
                    singleDate: "{!! request('singleDate') !!}"
                },
                success: function(response,status,xhr) {
                    $.unblockUI();
                    window.open(this.url);
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
    </script>
@endsection
