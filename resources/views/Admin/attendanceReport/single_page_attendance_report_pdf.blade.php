<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Attendance Sheet</title>

    <head>
        <style>
            #attendance_sheet {
                border-collapse: collapse;
                width: 100%;
                font-family: 'Segoe UI', sans-serif
            }

            #attendance_sheet td {
                border: 1px solid #ddd;
                padding: 2px;
                font-size: 12px;
            }

            #attendance_sheet tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #attendance_sheet tr:hover {
                background-color: #ddd;
            }

            #attendance_sheet th {
                border: 1px solid #ddd;
                padding: 2px;
                font-size: 16px;
                text-align: left;
                background-color: #2e475c;
                color: white;
            }
        </style>
    </head>
</head>

<body>
    <div>
        <h1 style="text-align: center; margin-bottom: 5px;">Attendance Sheet</h1>
        @if (isset($dateDay))
            <span style="font-weight: bold;">Date : </span>{{ $start_date }}
        @else
            <span style="font-weight: bold">Month : </span>{{ $year . '-' . $month }}
        @endif
        @if (isset($dept))
            <span style="font-weight: bold">Department : </span>{{ $dept }}
        @endif
        @if (isset($desg))
            <span style="font-weight: bold">Designation : </span>{{ $desg }}
        @endif
        <span style="font-weight: bold">Printed By : </span>{{ auth()->user()->username }}
        <span style="font-weight: bold">Printed At : </span>{{ date('Y-m-d H:i:s') }}

        @php
            $style = '';
            $padding_style = '';
            $font_size = '';
        @endphp
        <div>
            <table id="attendance_sheet" style="margin-bottom: 5px;">
                <thead>
                    <th>EMP.</th>
                    <th>DEPT.</th>
                    @for ($start = $start_date; $start <= $end_date; $start++)
                        <th colspan="2">{{ date('d', strtotime($start)) }} ({{ date('D', strtotime($start)) }})</th>
                    @endfor
                    @if (env('COMPANY') != 'CIDEX')
                        <th>O.H.</th>
                    @endif
                    <th>W.H.</th>
                    @if (env('COMPANY') != 'CIDEX')
                        <th>Late</th>
                    @endif
                </thead>
                <tbody>
                    @if (isset($employees) && count($employees) > 1)
                        @php
                            $absent = 0;
                            $present = 0;
                            $total = 0;
                        @endphp
                        @foreach ($employees as $key => $employee)
                            <tr>
                                <td>{{ $employee->employee_id }}
                                    {{ $employee->employee_code }}<br>{{ $employee->first_name }}
                                    {{ $employee->middle_name }}
                                    {{ $employee->last_name }}<br>{{ $employee->desigination }}</td>
                                <td>{{ $employee->title }}</td>
                                @for ($date = $start_date; $date <= $end_date; $date++)
                                    @if (count($employe_attendance[$employee->id][$date]) > 2 &&
                                            array_key_exists('in_time', $employe_attendance[$employee->id][$date]))
                                        @php
                                            if ($employe_attendance[$employee->id][$date]['is_manual'] == 1) {
                                                $color = 'yellow';
                                            } else {
                                                $color = '';
                                            }

                                            if (
                                                $employe_attendance[$employee->id][$date]['in_time'] >
                                                $employe_attendance[$employee->id][$date]['grace_period']
                                            ) {
                                                $tcolor = 'red';
                                            } else {
                                                $tcolor = 'green';
                                            }
                                        @endphp
                                        <td colspan="2">
                                            @if (env('COMPANY') == 'UNICORN' &&
                                                    in_array($employe_attendance[$employee->id][$date]['shift_desc'], getunicornDoubleAttendanceShiftsNames()))
                                                <span
                                                    style="background:{{ $color }};color:{{ $tcolor }};">
                                                    {{ $employe_attendance[$employee->id][$date]['in_time'][0] }}
                                                </span>
                                                <br>
                                                <span>
                                                    {{ $employe_attendance[$employee->id][$date]['out_time'][0] }}
                                                </span>
                                                @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['in_time']))
                                                    <br>
                                                    <span
                                                        style="background:{{ $color }};color:{{ $tcolor }};">
                                                        {{ $employe_attendance[$employee->id][$date]['in_time'][1] }}
                                                    </span>
                                                @endif
                                                @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['out_time']))
                                                    <span>
                                                        {{ $employe_attendance[$employee->id][$date]['out_time'][1] }}
                                                    </span>
                                                @endif
                                                <br>
                                                <span>{{ number_format($employe_attendance[$employee->id][$date]['working_hours'], 2) }}</span>
                                                @if (empty($dateDay))
                                                    <br>
                                                    <hr>
                                                    <span>
                                                        {{ date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime'][0])) }}
                                                    </span>
                                                    <br>
                                                    @if ($employe_attendance[$employee->id][$date]['out_time'][0] == 'Nil')
                                                        <span>
                                                            {{ $employe_attendance[$employee->id][$date]['out_time'][0] }}
                                                        </span>
                                                    @else
                                                        <span>
                                                            {{ date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime'][0])) }}
                                                        </span>
                                                    @endif
                                                    @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['in_datetime']))
                                                        <br>
                                                        <span>
                                                            {{ date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime'][1])) }}
                                                        </span>
                                                    @endif
                                                    @if (array_key_exists(1, $employe_attendance[$employee->id][$date]['out_datetime']))
                                                        <br>
                                                        @if ($employe_attendance[$employee->id][$date]['out_time'][1] == 'Nil')
                                                            <span>
                                                                {{ $employe_attendance[$employee->id][$date]['out_time'][1] }}
                                                            </span>
                                                        @else
                                                            <span>
                                                                {{ date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime'][1])) }}
                                                            </span>
                                                        @endif
                                                    @endif
                                                @endif
                                            @else
                                                <span
                                                    style="background:{{ $color }};color:{{ $tcolor }};">
                                                    {{ $employe_attendance[$employee->id][$date]['in_time'] }}
                                                </span>
                                                <br>
                                                <span>
                                                    {{ $employe_attendance[$employee->id][$date]['out_time'] }}
                                                </span>
                                                <br>
                                                @if (env('COMPANY') != 'CIDEX')
                                                    <span>{{ number_format($employe_attendance[$employee->id][$date]['working_hours'], 2) }}</span>
                                                @endif
                                                @if (empty($dateDay))
                                                    <br>
                                                    <hr>
                                                    <span>
                                                        {{ date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime'])) }}
                                                    </span>
                                                    <br>
                                                    @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                        <span>
                                                            {{ $employe_attendance[$employee->id][$date]['out_time'] }}
                                                        </span>
                                                    @else
                                                        <span>
                                                            {{ date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime'])) }}
                                                        </span>
                                                    @endif
                                                @endif
                                                <br>
                                                <hr>
                                                @if (isset($employe_attendance[$employee->id][$date]['short_leave_time_from']))
                                                    <span>{{ $employe_attendance[$employee->id][$date]['short_leave_time_from'] }}</span>
                                                    <br>
                                                    <span>{{ $employe_attendance[$employee->id][$date]['short_leave_time_to'] }}</span>
                                                    <hr>
                                                @endif
                                                <span
                                                    style="color:#A52A2A">{{ $employe_attendance[$employee->id][$date]['shift_desc'] }}</span>
                                                <br>
                                                <span>{{ $employe_attendance[$employee->id][$date]['shift_start_time'] }}</span>
                                                <br>
                                                <span>{{ $employe_attendance[$employee->id][$date]['shift_end_time'] }}</span>
                                                <hr>
                                                <span>{{ number_format($employe_attendance[$employee->id][$date]['total_working_hours'], 2) }}</span>
                                                <br>
                                                <span>{{ number_format($employe_attendance[$employee->id][$date]['overtime_hours'], 2) }}</span>
                                            @endif
                                        </td>
                                        @php
                                            $present++;
                                        @endphp
                                    @else
                                        @php
                                            $absent++;
                                        @endphp
                                        <td colspan="2"><span
                                                style="color:white; background: {{ $employe_attendance[$employee->id][$date]['absent_value_color'] }}">{{ $employe_attendance[$employee->id][$date]['absent_value'] }}</span>
                                        </td>
                                    @endif
                                    @php
                                        $total++;
                                    @endphp
                                @endfor
                                @if (env('COMPANY') != 'CIDEX')
                                    <td>{{ number_format($employe_attendance[$employee->id]['total_overtime_hours'], 2) }}
                                    </td>
                                @endif
                                <td>{{ number_format($employe_attendance[$employee->id]['total_working_hours'], 2) }}
                                </td>
                                @if (env('COMPANY') != 'CIDEX')
                                    <td>{{ number_format($employe_attendance[$employee->id]['total_days_late']) }}</td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="{{ $month_days * 2 + 7 }}" style="text-align: center">No Record Found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @if (!empty($dateDay))
        <div>
            <span style="font-weight: bold">Total Employees : </span>{{ $total }}
            <span style="font-weight: bold">Total Present : </span>{{ $present }}
            <span style="font-weight: bold">Total Absent : </span>{{ $absent }}
        </div>
    @endif
</body>

</html>
