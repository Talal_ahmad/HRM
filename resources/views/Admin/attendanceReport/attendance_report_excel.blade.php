<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Attendance Sheet</title>
</head>
<body>
    {{-- @php
        dd($minimized_undetail);
    @endphp --}}
    <div>
        <table border=".5pt">
            <thead>
                <tr>
                    @if (!empty($dept))
                        <th style="text-align: center" colspan="3">Department : {{$dept}}</th>    
                    @endif
                    @if (!empty($desg))
                        <th style="text-align: center" colspan="3">Designation : {{$desg}}</th>  
                    @endif
                    @if (!empty($dateFilter))
                        <th style="text-align: center" colspan="3">Month : {{$dateFilter}}</th>  
                    @endif
                    @if (!empty($singleDate))
                        <th style="text-align: center" colspan="3">Date : {{$singleDate}}</th>  
                    @endif
                    <th style="text-align: center" colspan="3">Printed By : {{auth()->user()->username}}</th>
                    <th style="text-align: center" colspan="3">Printed Date : {{date('Y-m-d H:i:s')}}</th>
                </tr>
                <tr>
                    {{-- <th><strong>Sr.No</strong></th> --}}
                    <th>Employee ID</th>
                    <th>Employee code</th>
                    <th colspan="3">Employee</th>
                    <th>Designation</th>
                    {{-- <th><strong>Employee Code</strong></th>
                    <th><strong>Employee</strong></th>
                    <th><strong>Desigination</strong></th> --}}
                    @if (env('COMPANY') == 'JSML')        
                        <th>Section</th>
                    @else
                        <th>Department</th>
                    @endif
                    @for ($start = $start_date; $start <= $end_date; $start++)
                        <th>{{date('d', strtotime($start))}} ({{date('D' , strtotime($start))}})</th>
                    @endfor
                    <th>T.O.H</th>
                    <th>T.W.H</th>
                    <th>T.D.L</th>
                    <th>Total Leaves</th>
                    <th>Total Absents</th>
                    <th>Total Off days in month</th>
                    <th>Total Presents</th>
                    @if (env('COMPANY') != 'JSML')        
                        <th>T.D</th>
                    @endif
                    @if (env('COMPANY') == 'JSML')        
                        <th>T.W.D</th>
                    @endif
                    @if (env('COMPANY') == 'JSML')        
                        <th>O.W.D</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @if (isset($employees) && count($employees) > 0)
                    @foreach ($employees as $key => $employee)
                        @if(!array_key_exists($employee->id, $employe_attendance) || count($employe_attendance[$employee->id]) === 9)
                            @continue
                        @endif
                        <tr valign="center">
                            {{-- <td valign="center">{{$key+1}}</td> --}}
                            <td>{{$employee->employee_id}}</td>
                            <td>{{$employee->employee_code}}</td>
                            <td valign="center" colspan="3">{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                            <td>{{$employee->desigination}}</td>
                            {{-- <td>{{$employee->employee_code}}</td>
                            <td>{{$employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                            <td>{{$employee->desigination}}</td> --}}
                            <td valign="center">{{$employee->title}}</td>
                            @for ($date = $start_date; $date <= $end_date; $date++)
                                @if (!array_key_exists($date, $employe_attendance[$employee->id]))
                                    <td></td>
                                    <td></td>
                                    @continue
                                @endif
                                @if (count($employe_attendance[$employee->id][$date]) > 2 && array_key_exists('in_time',$employe_attendance[$employee->id][$date]))
                                    @php
                                        if($employe_attendance[$employee->id][$date]['is_manual'] == 1)
                                        {
                                            $color = 'yellow';
                                        }
                                        else {
                                            $color = '';
                                        }
            
                                        if($employe_attendance[$employee->id][$date]['in_time'] > $employe_attendance[$employee->id][$date]['grace_period'])
                                        {
                                            $tcolor = 'red';
                                        }
                                        else {
                                            $tcolor = 'green';
                                        }
                                    @endphp
                                    <td>
                                        @if (request()->type !='day_wise_hours')
                                            @if(env('COMPANY') == 'UNICORN' && in_array($employe_attendance[$employee->id][$date]['shift_desc'], getunicornDoubleAttendanceShiftsNames()))
                                            @if (empty(request()->minimized_check))
                                                <label>In Time:</label>
                                            @endif
                                            <span style="background:{{$color}};color:{{$tcolor}};">
                                                {{$employe_attendance[$employee->id][$date]['in_time'][0]}}
                                            </span>
                                            <br style="mso-data-placement:same-cell;" />
                                            @if (empty(request()->minimized_check))
                                                <label>Out Time:</label>
                                            @endif
                                            <span>
                                                {{$employe_attendance[$employee->id][$date]['out_time'][0]}}
                                            </span>
                                            <br style="mso-data-placement:same-cell;" />
                                            @if(array_key_exists(1, $employe_attendance[$employee->id][$date]['in_time']))
                                            @if (empty(request()->minimized_check))
                                                <label>In Time:</label>
                                            @endif
                                            <span style="background:{{$color}};color:{{$tcolor}};">
                                                {{$employe_attendance[$employee->id][$date]['in_time'][1]}}
                                            </span>
                                            @endif
                                            @if(array_key_exists(1, $employe_attendance[$employee->id][$date]['out_time']))
                                            <br style="mso-data-placement:same-cell;" />
                                            @if (empty(request()->minimized_check))
                                                <label>Out Time:</label>
                                            @endif
                                            <span>
                                                {{$employe_attendance[$employee->id][$date]['out_time'][1]}}
                                            </span>
                                            @endif
                                            @else
                                            @if (empty(request()->minimized_check))
                                                <label>In Time:</label>
                                            @endif
                                            <span style="background:{{$color}};color:{{$tcolor}};">
                                                {{$employe_attendance[$employee->id][$date]['in_time']}}
                                            </span>
                                            <br style="mso-data-placement:same-cell;" />
                                            @if (empty(request()->minimized_check))
                                                <label>Out Time:</label>
                                            @endif
                                            <span>
                                                {{$employe_attendance[$employee->id][$date]['out_time']}}
                                            </span>
                                            @if (env('COMPANY') == 'JSML' && array_key_exists('od', $employe_attendance[$employee->id][$date]))
                                                <span> (OD)</span>
                                            @endif
                                            @if (env('COMPANY') == 'JSML' && array_key_exists('half_leave_flag', $employe_attendance[$employee->id][$date]))
                                                <span> (HL)</span>
                                            @endif
                                            @if (env('COMPANY') == 'JSML' && array_key_exists('ret', $employe_attendance[$employee->id][$date]))
                                                <span> (RET)</span>
                                            @endif
                                            @endif
                                            @if (empty($minimized_undetail))
                                                <br style="mso-data-placement:same-cell;" />
                                            @endif
                                            
                                            @if(env('COMPANY') == 'JSML' && isset($employe_attendance[$employee->id][$date]['short_leave']))
                                                <label style="font-size: 12px;">SL: </label>
                                                <span>{{$employe_attendance[$employee->id][$date]['short_leave']}}</span>
                                            @endif
                                            @if (empty(request()->minimized_check))
                                                <br style="mso-data-placement:same-cell;">
                                                <label>Work Hrs:</label>
                                            @endif
                                            
                                            @if (empty($minimized_undetail))
                                                <span>{{number_format($employe_attendance[$employee->id][$date]['working_hours'],2)}}</span>
                                                <br style="mso-data-placement:same-cell;" />
                                            @endif
                                            
                                            @if (isset($employe_attendance[$employee->id][$date]['short_leave_time_from']))
                                                @if (empty(request()->minimized_check))
                                                    <label>Short Leave From:</label>
                                                    <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_from']}}</span>
                                                @endif
                                                @if (empty($minimized_undetail))
                                                    <br>
                                                @endif
                                                
                                                @if (empty(request()->minimized_check))
                                                    <label>Short Leave To:</label>
                                                    
                                                @endif
                                                @if (empty($minimized_undetail))
                                                <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_to']}}</span>
                                                <hr>
                                            @endif
                                                
                                            @endif
                                            @if (empty(request()->minimized_check))
                                                <label>Shift:</label>
                                                <span style="color:#A52A2A">{{$employe_attendance[$employee->id][$date]['shift_desc']}}</span>
                                            @endif
                                            @if (empty($minimized_undetail))
                                                <br style="mso-data-placement:same-cell;" />
                                            @endif
                                            
                                            @if (empty(request()->minimized_check))
                                                <label>Shift Start:</label>
                                            @endif
                                            @if (empty($minimized_undetail))
                                                <span>{{$employe_attendance[$employee->id][$date]['shift_start_time']}}</span>
                                                <br style="mso-data-placement:same-cell;" />
                                            @endif
                                            
                                            @if (empty(request()->minimized_check))
                                                <label>Shift End:</label>    
                                            @endif
                                            
                                            @if (empty($minimized_undetail))
                                                <span>{{$employe_attendance[$employee->id][$date]['shift_end_time']}}</span>
                                                <br style="mso-data-placement:same-cell;" />
                                            @endif
                                            
                                            @if (empty(request()->minimized_check))
                                                <br style="mso-data-placement:same-cell;" />
                                                <label>Total Work Hrs:</label>
                                                
                                            @endif
                                            @if (empty($minimized_undetail))
                                                <br style="mso-data-placement:same-cell;" />
                                                <span>{{number_format($employe_attendance[$employee->id][$date]['total_working_hours'],2)}}</span>
                                            @endif
                                            
                                            @if (empty(request()->minimized_check))
                                                <br style="mso-data-placement:same-cell;" />
                                                <label>Overtime Hrs:</label>
                                            @endif
                                            @if (empty($minimized_undetail))
                                                <br style="mso-data-placement:same-cell;" />
                                                <span>{{number_format($employe_attendance[$employee->id][$date]['overtime_hours'],2)}}</span>
                                            @endif
                                        @else
                                            @if (empty($minimized_undetail))
                                                <br style="mso-data-placement:same-cell;" />
                                                <span>{{number_format($employe_attendance[$employee->id][$date]['working_hours'],2)}}</span>
                                            @endif
                                        @endif
                                    </td>
                                @else
                                    <td>{{$employe_attendance[$employee->id][$date]['absent_value']}}</td>
                                @endif
                            @endfor
                            <td valign="center">{{number_format($employe_attendance[$employee->id]['total_overtime_hours'],2)}}</td>
                            <td valign="center">{{number_format($employe_attendance[$employee->id]['total_working_hours'],2)}}</td>
                            <td valign="center">{{number_format($employe_attendance[$employee->id]['total_days_late'])}}</td>
                            <td valign="center">{{$employe_attendance[$employee->id]['total_leaves']}}</td>
                            <td valign="center">{{number_format($employe_attendance[$employee->id]['total_days_absent'])}}</td>
                            <td valign="center">{{number_format($employe_attendance[$employee->id]['total_off_days'])}}</td>
                            <td valign="center">{{number_format($employe_attendance[$employee->id]['total_days_present'])}}</td>
                            @if (env('COMPANY') != 'JSML')        
                                <td valign="center">
                                    {{ 
                                        $employe_attendance[$employee->id]['total_days_present'] +
                                        $employe_attendance[$employee->id]['total_leaves'] +
                                        $employe_attendance[$employee->id]['total_off_days'],
                                    }}
                                </td>
                            @endif
                            @if (env('COMPANY') == 'JSML')        
                                <td valign="center">
                                    {{ 
                                        $employe_attendance[$employee->id]['total_working_days']
                                    }}
                                </td>
                            @endif
                            @if (env('COMPANY') == 'JSML')        
                                <td valign="center">
                                    {{ 
                                        $employe_attendance[$employee->id]['over_all_working_days']
                                    }}
                                </td>
                            @endif
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</body>
</html>