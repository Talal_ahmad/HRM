<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Attendance Sheet</title>
    <style>
        span,
        td {
            border: 1px solid #ddd;
        }

        #attendance_sheet tr:nth-child(even) {
            background-color: #f2f2f2;
        }
    </style>
</head>

<body>
    <div>
        <h1 style="text-align: center; margin-bottom: 5px;font-size: 80px">Attendance Sheet</h1>
        @if ($dateDay)
            <span style="font-weight: bold;font-size: 40px;">Date : {{ $start_date }}</span>
        @else
            <span style="font-weight: bold;font-size: 40px;">Month : {{ $year . '-' . $month }}</span>
        @endif
        @if (isset($dept))
            <span style="font-weight: bold;font-size: 40px;">Department : {{ $dept }}</span>
        @endif
        @if (isset($desg))
            <span style="font-weight: bold;font-size: 40px;">Designation : {{ $desg }}</span>
        @endif
        <span style="font-weight: bold;font-size: 40px;">Printed By : {{ auth()->user()->username }}</span>
        <span style="font-weight: bold;font-size: 40px;">Printed At : {{ date('Y-m-d H:i:s') }}</span>

        @php
            $style = '';
            $padding_style = '';
            $font_size = '';
            if (empty($dateDay)) {
                $style = 'white-space: nowrap';
            }
            if (env('COMPANY') == 'CIDEX') {
                $font_size = 'font-size: 12px !important';
                $style = 'white-space: nowrap';
            } elseif(env('COMPANY') == 'JSML' && !empty(request()->minimized_check)) {
                $font_size = 'font-size: 40px !important';
            }else{
                $font_size = 'font-size: 40px !important'; 
            }
            if (env('COMPANY') != 'CIDEX') {
                $padding_style = 'padding: 1rem 0.5rem;';
            }
        @endphp
        <div>
            <table id="attendance_sheet"
                style="{{ $font_size }}; margin-bottom: 5px;border-collapse: collapse;
            width: 100%;
            font-family: 'Segoe UI', sans-serif">
                <thead>
                    <th
                        style="padding-top: 12px;
                    padding-bottom: 12px;
                    border: 1px solid #ddd;
                    padding: 5px;
                    font-size: 40px;
                    text-align: left;
                    background-color: #2e475c;
                    color: white;">
                        Sr.No</th>
                    <th
                        style="padding-top: 12px;
                    padding-bottom: 12px;
                    border: 1px solid #ddd;
                    padding: 5px;
                    font-size: 40px;
                    text-align: left;
                    background-color: #2e475c;
                    color: white;">
                        Emp.</th>
                    @for ($start = $start_date; $start <= $end_date; $start++)
                        <th colspan="2"
                            style="padding-top: 12px;
                        padding-bottom: 12px;
                        border: 1px solid #ddd;
                        padding: 5px;
                        font-size: 40px;
                        text-align: left;
                        background-color: #2e475c;
                        color: white;">
                            {{ date('d', strtotime($start)) }} ({{ date('D', strtotime($start)) }})</th>
                    @endfor
                    @if (env('COMPANY') != 'CIDEX')
                        <th
                            style="padding-top: 12px;
                        padding-bottom: 12px;
                        border: 1px solid #ddd;
                        padding: 5px;
                        font-size: 40px;
                        text-align: left;
                        background-color: #2e475c;
                        color: white;">
                            T.O.H.</th>
                    @endif
                    <th style="padding-top: 12px;
                    padding-bottom: 12px;
                    border: 1px solid #ddd;
                    padding: 5px;
                    font-size: 40px;
                    text-align: left;
                    background-color: #2e475c;
                    color: white;">T.W.H.</th>
                    @if (env('COMPANY') != 'CIDEX')
                        <th
                            style="padding-top: 12px;
                        padding-bottom: 12px;
                        border: 1px solid #ddd;
                        padding: 5px;
                        font-size: 40px;
                        text-align: left;
                        background-color: #2e475c;
                        color: white;">
                            T.D.L.</th>
                    @endif
                    @if (env('COMPANY') != 'JSML')        
                        <th
                            style="padding-top: 12px;
                            padding-bottom: 12px;
                            border: 1px solid #ddd;
                            padding: 5px;
                            font-size: 40px;
                            text-align: left;
                            background-color: #2e475c;
                            color: white;">
                            Total Days
                        </th>
                    @endif
                    @if (env('COMPANY') == 'JSML')        
                        <th
                            style="padding-top: 12px;
                            padding-bottom: 12px;
                            border: 1px solid #ddd;
                            padding: 5px;
                            font-size: 40px;
                            text-align: left;
                            background-color: #2e475c;
                            color: white;">
                            Total Working Days
                        </th>
                    @endif
                    @if (env('COMPANY') == 'JSML')        
                        <th
                            style="padding-top: 12px;
                            padding-bottom: 12px;
                            border: 1px solid #ddd;
                            padding: 5px;
                            font-size: 40px;
                            text-align: left;
                            background-color: #2e475c;
                            color: white;">
                            Overall Working Days
                        </th>
                    @endif
                </thead>
                <tbody style="{{ $style }}">
                    @if (isset($employees) && count($employees) > 0)
                        @php
                            $absent = 0;
                            $present = 0;
                            $total = 0;
                        @endphp
                        @foreach ($employees as $key => $employee)
                            @if(!array_key_exists($employee->id, $employe_attendance) || count($employe_attendance[$employee->id]) === 9)
                                @continue
                            @endif
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $employee->employee_code }}<br>{{ $employee->employee_id }}<br>{{ $employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name }}<br>{{ $employee->desigination }}<br>{{ $employee->title }}
                                </td>
                                @for ($date = $start_date; $date <= $end_date; $date++)
                                    @if (!array_key_exists($date, $employe_attendance[$employee->id]))
                                        <td colspan="2"></td>
                                        @continue
                                    @endif
                                    @if (count($employe_attendance[$employee->id][$date]) > 2 && array_key_exists('in_time',$employe_attendance[$employee->id][$date]))
                                        @php
                                            if ($employe_attendance[$employee->id][$date]['is_manual'] == 1) {
                                                $color = 'yellow';
                                            } else {
                                                $color = '';
                                            }
                                            
                                            if ($employe_attendance[$employee->id][$date]['in_time'] > $employe_attendance[$employee->id][$date]['grace_period'] && $employe_attendance[$employee->id][$date]['shift_desc'] != 'SS (0001 ~ 2359)') {
                                                $tcolor = 'red';
                                            } else {
                                                $tcolor = 'green';
                                            }
                                        @endphp
                                        <td colspan="2" style="{{ $padding_style }}">
                                            @if(env('COMPANY') == 'UNICORN' && in_array($employe_attendance[$employee->id][$date]['shift_desc'], getunicornDoubleAttendanceShiftsNames()))
                                            <span style="background:{{ $color }};color:green;">
                                                {{ $employe_attendance[$employee->id][$date]['in_time'][0] }}
                                            </span>
                                            <br>
                                            <span>
                                                {{ $employe_attendance[$employee->id][$date]['out_time'][0] }}
                                            </span>
                                            @if(array_key_exists(1, $employe_attendance[$employee->id][$date]['in_time']))
                                            <br>
                                            <span style="background:{{ $color }};color:green;">
                                                {{ $employe_attendance[$employee->id][$date]['in_time'][1] }}
                                            </span>
                                            @endif
                                            @if(array_key_exists(1, $employe_attendance[$employee->id][$date]['out_time']))
                                            <br>
                                            <span>
                                                {{ $employe_attendance[$employee->id][$date]['out_time'][1] }}
                                            </span>
                                            @endif
                                            @else
                                            <span style="background:{{ $color }};color:{{ $tcolor }};">
                                                {{ $employe_attendance[$employee->id][$date]['in_time'] }}
                                            </span>
                                            <br>
                                            <span>
                                                {{ $employe_attendance[$employee->id][$date]['out_time'] }}
                                            </span>
                                            @if (env('COMPANY') == 'JSML' && array_key_exists('od', $employe_attendance[$employee->id][$date]))
                                                <span class="badge" style="background-color: #28C76F">OD</span>
                                            @endif
                                            @if (env('COMPANY') == 'JSML' && array_key_exists('half_leave_flag', $employe_attendance[$employee->id][$date]))
                                                <span class="badge" style="background-color: #28C76F">HL</span>
                                            @endif
                                            @if (env('COMPANY') == 'JSML' && array_key_exists('ret', $employe_attendance[$employee->id][$date]))
                                                <span class="badge" style="background-color: #28C76F">RET</span>
                                            @endif
                                            @endif
                                            <br>
                                            @if(env('COMPANY') == 'JSML' && isset($employe_attendance[$employee->id][$date]['short_leave']))
                                                <label style="font-size: 12px;">SL: </label>
                                                <span>{{$employe_attendance[$employee->id][$date]['short_leave']}}</span>
                                                <br>
                                            @endif
                                            @if (env('COMPANY') != 'CIDEX')
                                                <span>{{ number_format($employe_attendance[$employee->id][$date]['working_hours'], 2) }}</span>
                                            @endif
                                            @if (empty($dateDay))
                                                <br>
                                                @if (empty(request()->minimized_check))
                                                    <hr>
                                                @endif
                                                @if(env('COMPANY') == 'UNICORN' && in_array($employe_attendance[$employee->id][$date]['shift_desc'], getunicornDoubleAttendanceShiftsNames()))
                                                <span>
                                                    {{ $employe_attendance[$employee->id][$date]['in_datetime'][0] }}
                                                </span>
                                                <br>
                                                @if ($employe_attendance[$employee->id][$date]['out_datetime'][0] == 'Nil')
                                                    <span style="background:yellow;">
                                                       Missing Checkout
                                                    </span>
                                                @else
                                                    <span>
                                                        {{ $employe_attendance[$employee->id][$date]['out_datetime'][0] }}
                                                    </span>
                                                @endif
                                                {{-- second attendance --}}
                                                <br>
                                                @if(array_key_exists(1, $employe_attendance[$employee->id][$date]['in_datetime']))
                                                <span>
                                                    {{ $employe_attendance[$employee->id][$date]['in_datetime'][1] }}
                                                </span>
                                                <br>
                                                @endif
                                                @if(array_key_exists(1, $employe_attendance[$employee->id][$date]['out_datetime']))
                                                @if ($employe_attendance[$employee->id][$date]['out_datetime'][1] == 'Nil')
                                                    <span style="background:yellow;">
                                                       Missing Checkout
                                                    </span>
                                                @else
                                                    <span>
                                                        {{ $employe_attendance[$employee->id][$date]['out_datetime'][1] }}
                                                    </span>
                                                @endif
                                                @endif
                                                @else
                                                <span>
                                                    {{ date('m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime'])) }}
                                                </span>
                                                <br>
                                                @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                    <span style="background:yellow;">
                                                        {{ $employe_attendance[$employee->id][$date]['out_time'] }}
                                                    </span>
                                                @else
                                                    <span>
                                                        {{ date('m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime'])) }}
                                                    </span>
                                                @endif
                                                @endif
                                                <br>
                                                @if (empty(request()->minimized_check))
                                                    <hr>
                                                @endif
                                                @if (isset($employe_attendance[$employee->id][$date]['short_leave_time_from']))
                                                    <label style="font-size: 40px;">S.L.F.:</label>
                                                    <span>{{ $employe_attendance[$employee->id][$date]['short_leave_time_from'] }}</span>
                                                    <br>
                                                    <label style="font-size: 40px;">S.L.T.:</label>
                                                    <span>{{ $employe_attendance[$employee->id][$date]['short_leave_time_to'] }}</span>
                                                    <hr>
                                                @endif
                                                @if (empty(request()->minimized_check))
                                                    <span
                                                        style="color:#A52A2A">{{ $employe_attendance[$employee->id][$date]['shift_desc'] }}</span>
                                                    <br>
                                                    <span>{{ $employe_attendance[$employee->id][$date]['shift_start_time'] }}</span>
                                                    <br>
                                                    <span>{{ $employe_attendance[$employee->id][$date]['shift_end_time'] }}</span>
                                                    <hr>
                                                    <span>{{ number_format($employe_attendance[$employee->id][$date]['total_working_hours'], 2) }}</span>
                                                    <br>
                                                    <span>{{ number_format($employe_attendance[$employee->id][$date]['overtime_hours'], 2) }}</span>
                                                @else
                                                    <br>
                                                    <span>{{ $employe_attendance[$employee->id][$date]['shift_start_time'] }}</span>
                                                    <br>
                                                    <span>{{ $employe_attendance[$employee->id][$date]['shift_end_time'] }}</span>
                                                    <br>
                                                    <span>{{ number_format($employe_attendance[$employee->id][$date]['total_working_hours'], 2) }}</span>
                                                    <br>
                                                    <span>{{ number_format($employe_attendance[$employee->id][$date]['overtime_hours'], 2) }}</span>
                                                @endif
                                            @endif
                                        </td>
                                        @php
                                            $present++;
                                        @endphp
                                    @else
                                        @php
                                            $absent++;
                                        @endphp
                                        <td style="padding: 0px 30px !important;" colspan="2"><span
                                                style="color:white;padding: 0px 30px !important;; background: {{ $employe_attendance[$employee->id][$date]['absent_value_color'] }}">{{ $employe_attendance[$employee->id][$date]['absent_value'] }}</span>
                                        </td>
                                    @endif
                                    @php
                                        $total++;
                                    @endphp
                                @endfor
                                @if (env('COMPANY') != 'CIDEX')
                                    <td>{{ number_format($employe_attendance[$employee->id]['total_overtime_hours'], 2) }}
                                    </td>
                                @endif
                                <td>{{ number_format($employe_attendance[$employee->id]['total_working_hours'], 2) }}
                                </td>
                                @if (env('COMPANY') != 'CIDEX')
                                    <td>{{ number_format($employe_attendance[$employee->id]['total_days_late']) }}</td>
                                @endif
                                @if (env('COMPANY') != 'JSML')      
                                    <td>
                                        {{ 
                                            $employe_attendance[$employee->id]['total_days_present'] +
                                            $employe_attendance[$employee->id]['total_leaves'] +
                                            $employe_attendance[$employee->id]['total_off_days'],
                                        }}
                                    </td>
                                @endif
                                @if (env('COMPANY') == 'JSML')      
                                    <td>
                                        {{ 
                                            $employe_attendance[$employee->id]['total_working_days']
                                        }}
                                    </td>
                                @endif
                                @if (env('COMPANY') == 'JSML')      
                                    <td>
                                        {{ 
                                            $employe_attendance[$employee->id]['over_all_working_days']
                                        }}
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="{{ $month_days * 2 + 8 }}" style="text-align: center">No Record Found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @if (!empty($dateDay))
        <div>
            <span style="font-weight: bold">Total Employees : </span>{{ $total }}
            <span style="font-weight: bold">Total Present : </span>{{ $present }}
            <span style="font-weight: bold">Total Absent : </span>{{ $absent }}
        </div>
    @endif
</body>

</html>
