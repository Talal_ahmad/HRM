@extends('Admin.layouts.master')
@if (request()->type == 'single')
    @section('title', 'Attendance Report')
@elseif (request()->type == 'multiple')
    @section('title', 'Attendance Report Multiple')
@elseif (request()->type == 'Minimized' OR request()->query('type') == 'Minimized_multiple')
    @section('title', 'Minimized Attendance Report')
@else
    @section('title', 'Staff Wise Attendance Report')
@endif
@section('style')
    <style>
        #dataTable td, #dataTable th{
            padding: 2px;
            font-size: 8px;
        }
        .px-2 {
                padding-right: 1.5rem !important;
                padding-left: 1.5rem !important;
            }
        hr {
            margin: 2px !important;
        }
    </style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    @if (request()->type == 'single')
                        <h2 class="content-header-title float-start mb-0">Attendance Report</h2>
                    @elseif(request()->type == 'multiple')
                        <h2 class="content-header-title float-start mb-0">Attendance Report Multiple</h2>
                    @elseif(request()->type == 'Minimized' OR request()->query('type') == 'Minimized_multiple')
                    <h2 class="content-header-title float-start mb-0">Minimized Attendance Report</h2>
                    @else
                        <h2 class="content-header-title float-start mb-0">Staff Wise Attendance Report</h2>
                    @endif
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Attendance</a>
                            </li>
                            <li class="breadcrumb-item active">Attendance Report
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-12 p-0">
                    <!--Search Form -->
                    <div class="card mb-1">
                        <div class="card-body">
                            @php
                                if(request()->query('type') == 'single' OR request()->query('type') == 'Minimized')
                                {
                                    $url = 'attendance_report';
                                }
                                elseif(request()->query('type') == 'multiple' OR request()->query('type') == 'Minimized_multiple') {
                                    $url = 'attendance_report_multiple';
                                }
                                else{
                                    $url = 'staffWiseAttendanceReport';
                                }
                            @endphp
                            <form id="search_form" action="{{url($url)}}" method="GET">
                                <input type="hidden" name="type" value="{{request('type')}}">
                                <div class="row">
                                    @if (request()->type == 'single' OR request()->query('type') == 'Minimized')
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" required>
                                                <option value=""></option>
                                                <option value="all" {{request('departmentFilter') == 'all' ? 'selected' : '' }}>All Departments</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{$department->id == request('departmentFilter') ? 'selected' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="employeeFilter">Filter By Employee:</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select" data-placeholder="Select Employee">
                                                {{-- <option value="">bfgbd</option> --}}
                                                @if (!empty(request('departmentFilter')))
                                                    @foreach (employees(request('departmentFilter')) as $item)
                                                    <option value="{{$item->id}}" {{$item->id == request('employeeFilter') ? 'selected' : ''}}>{{$item->employee_id.' - '.$item->employee_code.' - '.$item->first_name.' '.$item->last_name}}</option>
                                                    @endforeach
                                                @endif 
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="date">Filter By Month:</label>
                                            <input type="month" id="date" class="form-control date"  name="dateFilter" value="{{empty(request('dateFilter')) ? date('Y-m') : request('dateFilter')}}" required/>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <label class="form-label" for="singleDate">Filter By Date:</label>
                                            <input type="text" name="singleDate" id="singleDate" class="form-control flatpickr-basic singleDate" placeholder="YYYY-MM-DD" required>
                                        </div>
                                    @elseif(request()->type == 'multiple' OR request()->query('type') == 'Minimized_multiple')
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="departmentFilter">Filter By department:</label>
                                            <select name="departmentFilter[]" id="departmentFilter" class="select2 form-select" data-placeholder="Select Department" multiple required>
                                                <option value=""></option>
                                                @foreach (departments() as $department)
                                                    <option value="{{$department->id}}" {{!empty(request('departmentFilter')) ? in_array($department->id,request('departmentFilter')) ? 'selected' : '' : ''}}>{{$department->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="button-container mt-1">
                                                <button class="btn btn-sm btn-primary" type="button"
                                                    onclick="selectAll('#departmentFilter')">Select All</button>
                                                <button class="btn btn-sm btn-danger" type="button"
                                                    onclick="deselectAll('#departmentFilter')">Deselect All</button>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="date">Filter By Month:</label>
                                            <input type="month" id="date" class="form-control date" placeholder="Job Title" name="dateFilter" value="{{empty(request('dateFilter')) ? date('Y-m') : request('dateFilter')}}" required/>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="singleDate">Filter By Date:</label>
                                            <input type="text" name="singleDate" id="singleDate" class="form-control flatpickr-basic singleDate" placeholder="YYYY-MM-DD" required>
                                        </div>
                                    @else
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="designationFilter">Filter By Designation:</label>
                                            <select name="designationFilter" id="designationFilter" class="select2 form-select" data-placeholder="Select Designation" required>
                                                <option value=""></option>
                                                @foreach (designation() as $designation)
                                                    <option value="{{$designation->id}}" {{$designation->id == request('designationFilter') ? 'selected' : ''}}>{{$designation->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="date">Filter By Month:</label>
                                            <input type="month" id="date" class="form-control date" placeholder="Job Title" name="dateFilter" value="{{empty(request('dateFilter')) ? date('Y-m') : request('dateFilter')}}" required/>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label class="form-label" for="singleDate">Filter By Date:</label>
                                            <input type="text" name="singleDate" id="singleDate" class="form-control flatpickr-basic singleDate" placeholder="YYYY-MM-DD" required>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{url($url)}}?type={{request('type')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card pb-1">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <div class="mb-0">
                                    <h6>Attendance Report</h6>
                                </div>
                            </div>
                            <div class="dt-action-buttons text-end">
                                @if (request()->query('type') == 'Minimized')
                                    <div class="dt-buttons d-inline-flex py-2">
                                        <div class="dt-buttons d-inline-flex">
                                            <a href="{{url($url)}}?type=single" class="btn btn-secondary">Maxmize Report</a>
                                        </div>
                                    </div> 
                                @endif
                                @if (isset($employees) && count($employees) > 0)
                                    
                                    @if (!empty(request()->singleDate))
                                        
                                        <div class="dt-buttons d-inline-flex">
                                            <div class="dt-buttons d-inline-flex">
                                                <button type="button" class="btn btn-primary export" id="mini_pdf">Download Minimized Pdf</button>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="dt-buttons d-inline-flex">
                                        <div class="dt-buttons d-inline-flex">
                                            <button type="button" class="btn btn-primary export" id="pdf">Download Pdf</button>
                                        </div>
                                    </div>
                                    <div class="dt-buttons d-inline-flex">
                                        <button type="button" class="btn btn-success export" id="excel">Download Excel</button>
                                    </div>
                                </div>
                                @endif
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" style="width: 100%;">
                                <thead>
                                    <tr>
                                        {{-- <th>Sr.No</th> --}}
                                        {{-- <th>ID</th> --}}
                                        {{-- <th>Code</th> --}}
                                        <th>Employee</th>
                                        {{-- <th>Desigination</th> --}}
                                        <th>Department</th>
                                        @for ($start = $start_date; $start <= $end_date; $start++)
                                            <th colspan="2">{{date('d', strtotime($start))}} ({{date('D' , strtotime($start))}})</th>
                                        @endfor
                                        <th>Total Overtime Hours</th>
                                        <th>Total Working Hours</th>
                                        <th>Total Days Late</th>
                                        {{-- <th>Total Leaves</th>
                                        <th>Total Absents</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($employees) && count($employees) > 0)
                                        @foreach ($employees as $key => $employee)
                                        <tr>
                                            {{-- <td>{{$key+1}}</td> --}}
                                            {{-- <td>{{$employee->employee_id}}</td> --}}
                                            {{-- <td>{{HandleEmpty($employee->employee_code)}}</td> --}}
                                            <td>{{$employee->employee_id.'-'.HandleEmpty($employee->employee_code).'-'.$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name.' - '.$employee->desigination}}</td>
                                            {{-- <td>{{$employee->desigination}}</td> --}}
                                            <td>{{$employee->title}}</td>
                                            @for ($date = $start_date; $date <= $end_date; $date++)
                                                @if (count($employe_attendance[$employee->id][$date]) > 2)
                                                    @php
                                                        if($employe_attendance[$employee->id][$date]['is_manual'] == 1)
                                                        {
                                                            $color = 'yellow';
                                                        }
                                                        else {
                                                            $color = '';
                                                        }

                                                        if($employe_attendance[$employee->id][$date]['in_time'] > $employe_attendance[$employee->id][$date]['grace_period'])
                                                        {
                                                            $tcolor = 'red';
                                                        }
                                                        else {
                                                            $tcolor = 'green';
                                                        }
                                                    @endphp
                                                    <td colspan="2">
                                                        {{-- <label style="font-size: 12px;" >In Time:</label> --}}
                                                        <span style="background:{{$color}};color:{{$tcolor}};">
                                                            {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['in_time']))}}
                                                        </span>
                                                        <br>
                                                        {{-- <label style="font-size: 12px;">Out Time:</label> --}}
                                                        @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                        <span>
                                                            {{$employe_attendance[$employee->id][$date]['out_time']}}
                                                        </span>
                                                        @else
                                                        <span>
                                                            {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['out_time']))}}
                                                        </span>
                                                        @endif
                                                        <br>
                                                        {{-- <label style="font-size: 12px;">Work Hrs:</label> --}}
                                                        <span>{{number_format($employe_attendance[$employee->id][$date]['working_hours'],2)}}</span>
                                                        <br>
                                                        <hr>
                                                        {{-- <label style="font-size: 12px;">In Date:</label> --}}
                                                        <span>
                                                            {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime']))}}
                                                        </span>
                                                        <br>
                                                        {{-- <label style="font-size: 12px;">Out Date:</label> --}}
                                                        @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                        <span>
                                                            {{$employe_attendance[$employee->id][$date]['out_time']}}
                                                        </span>
                                                        @else
                                                        <span>
                                                            {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime']))}}
                                                        </span>
                                                        @endif
                                                        <br>
                                                        <hr>
                                                        @if (isset($employe_attendance[$employee->id][$date]['short_leave_time_from']))
                                                            {{-- <label style="font-size: 12px;">Short Leave From:</label> --}}
                                                            <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_from']}}</span>
                                                            <br>
                                                            {{-- <label style="font-size: 12px;">Short Leave To:</label> --}}
                                                            <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_to']}}</span>
                                                            <hr>
                                                        @endif
                                                        {{-- <label style="font-size: 12px;">Shift:</label> --}}
                                                        <span style="color:#A52A2A">{{$employe_attendance[$employee->id][$date]['shift_desc']}}</span>
                                                        <br>
                                                        {{-- <label style="font-size: 11px;">Shift Start:</label> --}}
                                                        <span>{{$employe_attendance[$employee->id][$date]['shift_start_time']}}</span>
                                                        <br>
                                                        {{-- <label style="font-size: 12px;">Shift End:</label> --}}
                                                        <span>{{$employe_attendance[$employee->id][$date]['shift_end_time']}}</span>
                                                        <hr>
                                                        {{-- <label style="font-size: 12px;">Total Work Hrs:</label> --}}
                                                        <span>{{number_format($employe_attendance[$employee->id][$date]['total_working_hours'],2)}}</span>
                                                        <br>
                                                        {{-- <label style="font-size: 12px;">Overtime Hrs:</label> --}}
                                                        <span>{{number_format($employe_attendance[$employee->id][$date]['overtime_hours'],2)}}</span>
                                                    </td>
                                                @else
                                                    {{-- <td colspan="2"><span style="color:;">{{$employe_attendance[$employee->id][$date]['absent_value']}}</span></td> --}}
                                                    <td colspan="2"><span class="px-2" style="color:white; background: {{$employe_attendance[$employee->id][$date]['absent_value_color']}}">{{$employe_attendance[$employee->id][$date]['absent_value']}}</span></td>
                                                @endif
                                            @endfor
                                            <td>{{number_format($employe_attendance[$employee->id]['total_overtime_hours'],2)}}</td>
                                            <td>{{number_format($employe_attendance[$employee->id]['total_working_hours'],2)}}</td>
                                            <td>{{number_format($employe_attendance[$employee->id]['total_days_late'])}}</td>
                                            {{-- <td onclick="leave_check({{$employee->id}})" style="cursor: pointer; color:cornflowerblue;">{{number_format($employe_attendance[$employee->id]['total_leaves'])}}</td> --}}
                                            {{-- <td>{{number_format($employe_attendance[$employee->id]['total_days_absent'])}}</td> --}}
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9" class="text-center">No Record Found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal fade text-start" id="leave_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title modal_heading" id="myModalLabel17"></h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <h4 class="text-center font-weight-bold" id="emp_name"></h4>
                                <div class="row">
                                    <div class="col-12">
                                        <div id="leave_table" class="table-responsive">
        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @php
            $departmentFilter = request('departmentFilter');
        @endphp
</section>
@endsection
@section('scripts')
    <script>
        var rowid;
        var departmentFilter = @json($departmentFilter);
        $(document).ready(function(){
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation + ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            $('.date').change(function(){
                $('.singleDate').prop('required',false);
                $('.singleDate').prop('disabled',true);
            })
            $('#singleDate').change(function(){
                $('.date').prop('required',false);
                $('.date').prop('disabled',true);
                $('.date').val('');
            })
        });
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var size_pdf = '';
            if (export_type == 'mini_pdf') {
                size_pdf = 'cidpdf';
            }
            $.ajax({
                type: "GET",
                url: "{{ url('attendance_report') }}",
                data: {
                    export_type : export_type,
                    size_pdf : size_pdf,
                    type: "{!! request('type') !!}",
                    departmentFilter: departmentFilter,
                    designationFilter: "{!! request('designationFilter') !!}",
                    employeeFilter: "{!! request('employeeFilter') !!}",
                    dateFilter: "{!! request('dateFilter') !!}",
                    singleDate: "{!! request('singleDate') !!}"
                },
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function leave_check(id){
            rowid = id;
            var date = '{!! request()->query('dateFilter') !!}';
            if(date == ''){
                date = '{!! request()->query('singleDate') !!}';
            } 
            $.ajax({
                url: "{{url('attendance_report')}}" + "/" + id,
                type: "GET",
                data: {
                    date: date
                },
                success: function(response){
                    var html = '';
                    html = `
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>    
                                    <th>Employee</th>    
                                    <th>Leave Type</th>    
                                    <th>Leave Start Date</th>  
                                    <th>Leave End Date</th>    
                                    <th>Leave Status</th>    
                                    <th>Created By</th>    
                                </tr>
                            </thead>    
                        <tbody>
                    `;
                    if(response.length > 0){
                        $.each(response , function(index , value){
                            html += `
                                    <tr>
                                        <td>${index + 1}</td>    
                                        <td>${value.first_name} ${value.last_name}</td>    
                                        <td>${value.leave}</td>    
                                        <td>${value.date_start}</td>    
                                        <td>${value.date_end}</td>    
                                        <td>${value.status}</td>    
                                        <td>${value.username}</td>    
                                    </tr>
                                `;
                        })
                    }
                    else{
                        html += `<tr>
                                    <td colspan='7' class='text-center'>No Record Found</td>
                                </tr>`;
                    }
                    $('.modal_heading').html(`Leave On <span class='fw-bolder'>${date}</span>`);
                    $('#leave_table').html(html);
                    $("#leave_modal").modal('show');
                }
            });
        }
    </script>
@endsection