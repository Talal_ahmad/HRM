@extends('Admin.layouts.master')
@section('title', 'Attendance Report')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Attendance Report</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Attendance</a>
                                </li>
                                <li class="breadcrumb-item active">Attendance Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="content-body">
            <section class="basic-tabs-components">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="employees-payroll-tab" href="{{route('attendance_report.index')}}?type=single">Attendance Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=multiple">Attendance Report Multiple</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=staffWiseAttendanceReport">Staff Wise Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=Minimized">Minimized Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=from_to_date">From & to Date Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="employers-column-tab" href="{{route('attendance_report.index')}}?type=day_wise_hours">Day Wise Hours Report</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- Maximize Attendance Report -->
                    <div class="tab-pane active" id="employees-payroll" aria-labelledby="employees-payroll-tab" role="tabpanel">
                        <section>
                            <!-- Search Form -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="card mb-1">
                                        <div class="card-body">
                                            <form id="search_form" action="{{url('attendance_report')}}" method="GET">
                                                <input type="hidden" name="type" value="{{request('type')}}">
                                                <div class="row">
                                                    <div class="col-md-4 col-12">
                                                        <label class="form-label" for="designationFilter">Filter By Designation:</label>
                                                        <select name="designationFilter" id="designationFilter" class="select2 form-select" data-placeholder="Select Designation" required>
                                                            <option value=""></option>
                                                            @foreach (designation() as $designation)
                                                                <option value="{{$designation->id}}" {{$designation->id == request('designationFilter') ? 'selected' : ''}}>{{$designation->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <label class="form-label" for="date">Filter By Month:</label>
                                                        <input type="month" id="date" class="form-control date" placeholder="Job Title" name="dateFilter" value="{{empty(request('dateFilter')) ? date('Y-m') : request('dateFilter')}}" required/>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <label class="form-label" for="singleDate">Filter By Date:</label>
                                                        <input type="text" name="singleDate" id="singleDate" class="form-control flatpickr-basic singleDate" placeholder="YYYY-MM-DD" required>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <input type="checkbox" class="form-check-input mt-1" name="old_attendance" id="old_attendance" value="1" {{ request()->old_attendance == 1 ? 'checked' : '' }}/>
                                                        <label class="custom-control-label mt-1" for="old_attendance">Is old Attendance</label>
                                                    </div>
                                                    <div class="col-6 text-end">
                                                        <a href="{{url('attendance_report')}}?type={{request('type')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Search Form End -->

                            <!-- Report Start -->
                            <div class="card pb-1">
                                <div class="card-header border-bottom p-1">
                                    <div class="head-label">
                                        <div class="mb-0">
                                            <h6>Attendance Report</h6>
                                        </div>
                                    </div>
                                    @if (isset($employees) && count($employees) > 0)
                                    <div class="dt-action-buttons text-end">
                                        <div class="dt-buttons d-inline-flex">
                                            <div class="dt-buttons d-inline-flex">
                                                <button type="button" class="btn btn-primary export" id="pdf">Download Pdf</button>
                                            </div>
                                        </div>
                                        <div class="dt-buttons d-inline-flex">
                                            <button type="button" class="btn btn-success export" id="excel">Download Excel</button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>Employee ID</th>
                                                <th>Code</th>
                                                <th>Employee</th>
                                                <th>Desigination</th>
                                                <th>Department</th>
                                                @for ($start = $start_date; $start <= $end_date; $start++)
                                                    <th colspan="2">{{date('d', strtotime($start))}} ({{date('D' , strtotime($start))}})</th>
                                                @endfor
                                                <th>Total Overtime Hours</th>
                                                <th>Total Working Hours</th>
                                                <th>Total Days Late</th>
                                                <th>Total Leaves</th>
                                                <th>Total Absents</th>
                                            </tr>
                                        </thead>
                                        <tbody style="white-space: nowrap">
                                            @if (isset($employees) && count($employees) > 0)
                                                @foreach ($employees as $key => $employee)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{$employee->employee_id}}</td>
                                                    <td>{{HandleEmpty($employee->employee_code)}}</td>
                                                    <td>{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}</td>
                                                    <td>{{$employee->desigination}}</td>
                                                    <td>{{$employee->title}}</td>
                                                    @for ($date = $start_date; $date <= $end_date; $date++)
                                                        @if (count($employe_attendance[$employee->id][$date]) > 2 && array_key_exists('in_time',$employe_attendance[$employee->id][$date]))
                                                            @php
                                                                if($employe_attendance[$employee->id][$date]['is_manual'] == 1)
                                                                {
                                                                    $color = 'yellow';
                                                                }
                                                                else {
                                                                    $color = '';
                                                                }
        
                                                                if($employe_attendance[$employee->id][$date]['in_time'] > $employe_attendance[$employee->id][$date]['grace_period'])
                                                                {
                                                                    $tcolor = 'red';
                                                                }
                                                                else {
                                                                    $tcolor = 'green';
                                                                }
                                                            @endphp
                                                            <td colspan="2">
                                                                <label style="font-size: 12px;" >In Time:</label>
                                                                <span style="background:{{$color}};color:{{$tcolor}};">
                                                                    {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['in_time']))}}
                                                                </span>
                                                                <br>
                                                                <label style="font-size: 12px;">Out Time:</label>
                                                                @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                                <span>
                                                                    {{$employe_attendance[$employee->id][$date]['out_time']}}
                                                                </span>
                                                                @else
                                                                <span>
                                                                    {{date('H:i', strtotime($employe_attendance[$employee->id][$date]['out_time']))}}
                                                                </span>
                                                                @endif
                                                                <br>
                                                                <label style="font-size: 12px;">Work Hrs:</label>
                                                                <span>{{number_format($employe_attendance[$employee->id][$date]['working_hours'],2)}}</span>
                                                                <br>
                                                                <hr>
                                                                <label style="font-size: 12px;">In Date:</label>
                                                                <span>
                                                                    {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['in_datetime']))}}
                                                                </span>
                                                                <br>
                                                                <label style="font-size: 12px;">Out Date:</label>
                                                                @if ($employe_attendance[$employee->id][$date]['out_time'] == 'Nil')
                                                                <span>
                                                                    {{$employe_attendance[$employee->id][$date]['out_time']}}
                                                                </span>
                                                                @else
                                                                <span>
                                                                    {{date('Y-m-d', strtotime($employe_attendance[$employee->id][$date]['out_datetime']))}}
                                                                </span>
                                                                @endif
                                                                <br>
                                                                <hr>
                                                                @if (isset($employe_attendance[$employee->id][$date]['short_leave_time_from']))
                                                                    <label style="font-size: 12px;">Short Leave From:</label>
                                                                    <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_from']}}</span>
                                                                    <br>
                                                                    <label style="font-size: 12px;">Short Leave To:</label>
                                                                    <span>{{$employe_attendance[$employee->id][$date]['short_leave_time_to']}}</span>
                                                                    <hr>
                                                                @endif
                                                                <label style="font-size: 12px;">Shift:</label>
                                                                <span style="color:#A52A2A">{{$employe_attendance[$employee->id][$date]['shift_desc']}}</span>
                                                                <br>
                                                                <label style="font-size: 11px;">Shift Start:</label>
                                                                <span>{{$employe_attendance[$employee->id][$date]['shift_start_time']}}</span>
                                                                <br>
                                                                <label style="font-size: 12px;">Shift End:</label>
                                                                <span>{{$employe_attendance[$employee->id][$date]['shift_end_time']}}</span>
                                                                <hr>
                                                                <label style="font-size: 12px;">Total Work Hrs:</label>
                                                                <span>{{number_format($employe_attendance[$employee->id][$date]['total_working_hours'],2)}}</span>
                                                                <br>
                                                                <label style="font-size: 12px;">Overtime Hrs:</label>
                                                                <span>{{number_format($employe_attendance[$employee->id][$date]['overtime_hours'],2)}}</span>
                                                            </td>
                                                        @else
                                                            <td colspan="2"><span class="px-2" style="color:white; background: {{$employe_attendance[$employee->id][$date]['absent_value_color']}}">{{$employe_attendance[$employee->id][$date]['absent_value']}}</span></td>
                                                        @endif
                                                    @endfor
                                                    <td>{{number_format($employe_attendance[$employee->id]['total_overtime_hours'],2)}}</td>
                                                    <td>{{number_format($employe_attendance[$employee->id]['total_working_hours'],2)}}</td>
                                                    <td>{{number_format($employe_attendance[$employee->id]['total_days_late'])}}</td>
                                                    <td onclick="leave_check({{$employee->id}})" style="cursor: pointer; color:cornflowerblue;">{{number_format($employe_attendance[$employee->id]['total_leaves'])}}</td>
                                                    <td>{{number_format($employe_attendance[$employee->id]['total_days_absent'])}}</td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="9" class="text-center">No Record Found</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- Report End -->
                        </section>
                    </div>
                    <!-- End Maximized Report -->
                </div>
                <div class="modal fade text-start" id="leave_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title modal_heading" id="myModalLabel17"></h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <h4 class="text-center font-weight-bold" id="emp_name"></h4>
                                <div class="row">
                                    <div class="col-12">
                                        <div id="leave_table" class="table-responsive">
        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var rowid;
        $(document).ready(function(){
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('get_dept_employees') }}",
                    data: {
                        department_id: optVal,
                    },
                    success: function(response) {
                        $('#employeeFilter').empty();
                        $('#employeeFilter').html('<option value="">Select Employee</option>'); 
                        $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation + ' - ' + value.department)
                            );
                        });
                    },
                    error: function() {
                        alert('Error occured');
                    }
                });
            });
            $('.date').change(function(){
                $('.singleDate').prop('required',false);
                $('.singleDate').prop('disabled',true);
            })
            $('#singleDate').change(function(){
                $('.date').prop('required',false);
                $('.date').prop('disabled',true);
                $('.date').val('');
            })
        });
        $(document).on('click', '.export', function(){
            blockUI();
            var export_type = $(this).attr('id');
            var size_pdf = '';
            if (export_type == 'mini_pdf') {
                size_pdf = 'cidpdf';
            }
            $.ajax({
                type: "GET",
                url: "{{ url('attendance_report') }}",
                data: {
                    export_type : export_type,
                    size_pdf : size_pdf,
                    type: "{!! request('type') !!}",
                    designationFilter: "{!! request('designationFilter') !!}",
                    employeeFilter: "{!! request('employeeFilter') !!}",
                    dateFilter: "{!! request('dateFilter') !!}",
                    old_attendance: "{!! request('old_attendance') !!}",
                    singleDate: "{!! request('singleDate') !!}"
                },
                success: function(response,status,xhr) {
                    $.unblockUI();
                    // if(export_type == 'excel')
                    // {
                        window.open(this.url);
                    // }
                },
                error: function() {
                    $.unblockUI();
                    alert('Error occured');
                }
            });
        });
        function leave_check(id){
            rowid = id;
            var date = '{!! request()->query('dateFilter') !!}';
            if(date == ''){
                date = '{!! request()->query('singleDate') !!}';
            } 
            $.ajax({
                url: "{{url('attendance_report')}}" + "/" + id,
                type: "GET",
                data: {
                    date: date
                },
                success: function(response){
                    var html = '';
                    html = `
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>    
                                    <th>Employee</th>    
                                    <th>Leave Type</th>    
                                    <th>Leave Start Date</th>  
                                    <th>Leave End Date</th>    
                                    <th>Leave Status</th>    
                                    <th>Created By</th>    
                                </tr>
                            </thead>    
                        <tbody>
                    `;
                    if(response.length > 0){
                        $.each(response , function(index , value){
                            html += `
                                    <tr>
                                        <td>${index + 1}</td>    
                                        <td>${value.first_name} ${value.last_name}</td>    
                                        <td>${value.leave}</td>    
                                        <td>${value.date_start}</td>    
                                        <td>${value.date_end}</td>    
                                        <td>${value.status}</td>    
                                        <td>${value.username}</td>    
                                    </tr>
                                `;
                        })
                    }
                    else{
                        html += `<tr>
                                    <td colspan='7' class='text-center'>No Record Found</td>
                                </tr>`;
                    }
                    $('.modal_heading').html(`Leave On <span class='fw-bolder'>${date}</span>`);
                    $('#leave_table').html(html);
                    $("#leave_modal").modal('show');
                }
            });
        }
    </script>
@endsection