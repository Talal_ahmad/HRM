<div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
        <li class="nav-item me-auto">
            <a class="navbar-brand" href="{{url('/')}}">
                @if (!empty(env('COMPANY_LOGO')))

                @else
                    @if (env('COMPANY') == 'Ajmal Dawakhana')
                        <h2 class="brand-text" style="font-size: 17px;">{{env('COMPANY')}}</h2>
                    @else
                        <h2 class="brand-text">{{env('COMPANY')}}</h2>
                    @endif
                @endif
            </a>
        </li>
        <li class="nav-item nav-toggle">
            <a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse">
                <i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4 text-primary" data-feather="disc" data-ticon="disc"></i>
            </a>
        </li>
    </ul>
</div>
<div class="shadow-bottom"></div>
<div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        @can('News Page View')
        @if (env('COMPANY') == 'CLINIX')
        <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == '/' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{url('/')}}">
                <i data-feather="bell"></i><span class="menu-title text-truncate">@lang('app.news_page')</span>
            </a>
        </li>
        @else
        <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'news_dashboard' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{url('news_dashboard')}}">
                <i data-feather="bell"></i><span class="menu-title text-truncate">@lang('app.news_page')</span>
            </a>
        </li>
        @endif
        @endcan
        @can('Dashboard')
        @if (env('COMPANY') == 'CLINIX')
        <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'dashboard' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{url('dashboard')}}">
                <i data-feather="home"></i><span class="menu-title text-truncate">@lang('app.dashboard')</span>
            </a>
        </li>
        @else
        <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == '/' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{url('/')}}">
                <i data-feather="home"></i><span class="menu-title text-truncate">@lang('app.dashboard')</span>
            </a>
        </li>
        @endif
        @endcan
        @can('KPI')
            <li class="nav-item">
                <a class="d-flex align-items-center" href="#"><i data-feather='bar-chart-2'></i><span class="menu-title text-truncate">@lang('app.kpi')</span></a>
                <ul class="menu-content">
                    @can('Create_KPI')
                    <li class="nav-item {{ request()->segment(1) == 'kpis' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right">
                        <a class="d-flex align-items-center" href="{{url('kpis')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.create_kpi')</span></a>
                    </li>
                    @endcan
                    @can('KPI Evaluation Form')
                    <li class="nav-item {{ request()->segment(1) == 'kpi_evaluation_form' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right">
                        <a class="d-flex align-items-center" href="{{url('kpi_evaluation_form')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.kpi_evaluationForm')</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @if (env('COMPANY') == 'CLINIX' || env('COMPANY') == 'HEALTHWISE' || env('COMPANY') == 'KITCHEN' || env('COMPANY') == 'OPUS' || env('COMPANY') == 'UNICORN')
        @can('On Boarding')
        <li class="nav-item">
            <a class="d-flex align-items-center" href="#"><i data-feather='briefcase'></i><span class="menu-title text-truncate">@lang('app.on_boarding')</span></a>
            <ul class="menu-content">
                @can('CV-Bank')
                <li class="nav-item {{ request()->segment(1) == 'cv_bank_list' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{url('cv_bank_list')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.cv_bank')</span></a>
                </li>
                @endcan
                @can('Interview Schedule')
                <li class="nav-item {{ request()->segment(1) == 'app_schedule' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{url('app_schedule')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.interview_schedule')</span></a>
                </li>
                @endcan
                @can('Interview Appointment')
                <li class="nav-item {{ request()->segment(1) == 'appointment' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{url('appointment')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.appointment')</span></a>
                </li>
                @endcan
                @can('Interview List')
                <li class="nav-item {{ request()->segment(1) == 'interview_result' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{url('interview_result')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.interview_result')</span></a>
                </li>
                @endcan
            </ul>
        </li>
        @endcan
        @endif
        @can('Staff Directory')
        <li class="nav-item {{ request()->segment(1) == 'staff_directory' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{route('staff_directory.index')}}">
                <i data-feather="users"></i><span class="menu-title text-truncate">@lang('app.staff_directory')</span>
            </a>
        </li>
        @endcan
        @can('Attendance')
        <li class="nav-item">
            <a class="d-flex align-items-center" href="#"><i data-feather='sliders'></i><span class="menu-title text-truncate">@lang('app.attendance')</span></a>
            <ul class="menu-content">
                @can('Attendance Dashboard')
                <li class="nav-item {{ request()->segment(1) == 'attendance_dashboard' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Attendance Dashboard">
                    <a class="d-flex align-items-center" href="{{route('attendance_dashboard.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.attendance_dashboard')</span></a>
                </li>
                @endcan
                @can('Attendance Report')
                <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'attendance_report' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('attendance_report.index')}}?type=single"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.attendance_report')</span></a>
                </li>
                @endcan
                {{-- @can('Attendance Report Multiple')
                        <li class="nav-item {{ request()->segment(1) == 'attendance_report_multiple' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Multiple Attendance Report">
                <a class="d-flex align-items-center" href="{{url('attendance_report_multiple')}}?type=multiple"><i data-feather="circle"></i><span class="menu-item text-truncate">Multiple Attendance Report</span></a>
        </li>
        @endcan --}}
        @can('Absentees Report')
        <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'absentessReport' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{url('absentessReport')}}?type=absentees"><i data-feather="circle"></i><span class="menu-item text-truncate" title="@lang('app.absentees_report')">@lang('app.absentees_report')</span></a>
        </li>
        @endcan
        @can('Missing Checkout Report')
        <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'missing_checkout_report' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{url('missing_checkout_report')}}?type=missing_checkout_report"><i data-feather="circle"></i><span class="menu-item text-truncate" title="@lang('app.Missing_Checkout_Report')">@lang('app.Missing_Checkout_Report')</span></a>
        </li>
        @endcan
        {{-- @can('Attendance History')
                        <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'attendance_history' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{url('attendance_history')}}?type=attendance_history"><i data-feather="circle"></i><span class="menu-item text-truncate" title="Attendance History">Attendance History</span></a>
        </li>
        @endcan --}}
        {{-- @can('Staff Wise Attendance Report')
                        <li class="nav-item {{ request()->segment(1) == 'staffWiseAttendanceReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Staff Wise Attendance Report">
        <a class="d-flex align-items-center" href="{{url('staffWiseAttendanceReport')}}?type=staffWiseAttendanceReport"><i data-feather="circle"></i><span class="menu-item text-truncate">Staff Wise Attendance Report</span></a>
        </li>
        @endcan --}}
        @if (env('COMPANY') == 'CLINIX')
            @can('Add Manual Attendance')
                <li class="nav-item {{ request()->segment(1) == 'manualAttendance' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('manualAttendance.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Manual_Attendance')</span></a>
                </li>
            @endcan
        @else
            @can('Manual Attendance')
                <li class="nav-item {{ request()->segment(1) == 'manualAttendance' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('manualAttendance.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Manual_Attendance')</span></a>
                </li>
            @endcan
        @endif
        @can('Disciplinaries Issue')
        <li class="nav-item {{ request()->segment(1) == 'disciplinaries' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{url('disciplinaries')}}"><i data-feather='key'></i><span class="menu-item text-truncate">@lang('app.Issue_Disciplinaries')</span></a>
        </li>
        @endcan
        @can('Disciplinaries View')
        <li class="nav-item {{ request()->segment(1) == 'disciplinariestable' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{url('disciplinariestable')}}">
                <i data-feather="key"></i><span class="menu-title text-truncate">@lang('app.Disciplinaries_Records')</span>
            </a>
        </li>
        @endcan
    </ul>
    </li>
    @endcan
    @can('Employees')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="#"><i data-feather='user'></i><span class="menu-title text-truncate">@lang('app.Employees')</span></a>
        <ul class="menu-content">
            @can('List')
                <li class="nav-item {{ request()->segment(1) == 'getEmployees' || request()->segment(1) == 'create' ? 'active' : '' }}">
                    <a class="d-flex align-items-center" href="{{route('getEmployees')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Employees_list')</span></a>
                </li>
            @endcan
            @can('Employees History')
            <li class="nav-item {{ request()->segment(1) == 'employee_history' || request()->segment(1) == 'employee_history' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{route('employee_history.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Employees_History')</span></a>
            </li>
            @endcan
            @can('Disciplinary/Inquiry')
            <li class="nav-item {{ request()->segment(1) == 'employee_disciplinary_action' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('employee_disciplinary_action.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Disciplinary_Inquiry')</span></a>
            </li>
            @endcan
            @can('Joining Report')
            <li class="nav-item {{ request()->segment(1) == 'emp_join_report' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('emp_join_report.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Joining_Report')</span></a>
            </li>
            @endcan
            @can('Appointment Letter Issuance')
            <li class="nav-item {{ request()->segment(1) == 'appoint_letter' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Appointment_Letter_Issuance')">
                <a class="d-flex align-items-center" href="{{route('appoint_letter.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Appointment_Letter_Issuance')</span></a>
            </li>
            @endcan
            @can('Final Settlement Letter')
            <li class="nav-item {{ request()->segment(1) == 'settlement_letter' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Final_Settlement_Letter')">
                <a class="d-flex align-items-center" href="{{url('settlement_history')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Final_Settlement_Letter')</span></a>
            </li>
            @endcan
            @can('Job Position Report')
            <li class="nav-item {{ request()->segment(1) == 'job_positions' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('job_positions.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Job_Position_Report')</span></a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('Finance')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="#"><i data-feather='file-text'></i><span class="menu-title text-truncate">@lang('app.finance')</span></a>
        <ul class="menu-content">
            @can('Asset Issuance')
            <li class="nav-item {{ request()->segment(1) == 'assetIssuance' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('assetIssuance.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Asset_Issuance')</span></a>
            </li>
            @endcan
            @can('Calculation Groups')
            <li class="nav-item {{ request()->segment(1) == 'calculationGroups' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('calculationGroups.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Calculation_Groups')</span></a>
            </li>
            @endcan
            @can('Company Belongings')
            <li class="nav-item {{ request()->segment(1) == 'company_belongings' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Company_Belongings')">
                <a class="d-flex align-items-center" href="{{route('company_belongings.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Company_Belongings')</span></a>
            </li>
            @endcan
            @can('Consolidated Bank Sheet')
            <li class="nav-item {{ request()->segment(1) == 'consolidated_bank_sheet' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Consolidated_Bank_Sheet')">
                <a class="d-flex align-items-center" href="{{route('consolidated_bank_sheet.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Consolidated_Bank_Sheet')</span></a>
            </li>
            @endcan
            @can('Calculation Methods')
            <li class="nav-item {{ request()->segment(1) == 'calculationMethods' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('calculationMethods.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Calculation_Methods')</span></a>
            </li>
            @endcan
            @can('Company Loans')
            <li class="nav-item {{ request()->segment(1) == 'companyLoans' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('companyLoans.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Company_Loans')</span></a>
            </li>
            @endcan
            @can('Company Payroll')
            <li class="nav-item {{ request()->segment(1) == 'companyPayroll' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('companyPayroll.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Company_Payroll')</span></a>
            </li>
            @endcan
            @if (env('COMPANY') == 'CLINIX')
            @can('Department Wise Salary Summary')
            <li class="nav-item {{ request()->segment(1) == 'department_wise_salary' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Department_Wise_Salary_Summery')">
                <a class="d-flex align-items-center" href="{{route('department_wise_salary.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Department_Wise_Salary_Summery')</span></a>
            </li>
            @endcan
            @endif
            @can('Expense Administration')
            <li class="nav-item {{ request()->segment(1) == 'expenseAdministration' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Expense_Administration')">
                <a class="d-flex align-items-center" href="{{route('expenseAdministration.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Expense_Administration')</span></a>
            </li>
            @endcan
            @can('Employee Payments')
            <li class="nav-item {{ request()->segment(1) == 'employeePayments' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('employeePayments.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Employee_Payments')</span></a>
            </li>
            @endcan
            @can('Entitlement')
            <li class="nav-item {{ request()->segment(1) == 'entitlement' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('entitlement.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.entitlement')</span></a>
            </li>
            @endcan
            @can('Entitlement Assignment')
            <li class="nav-item {{ request()->segment(1) == 'entitlementAssignment' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Entitlement_Assignment')">
                <a class="d-flex align-items-center" href="{{route('entitlementAssignment.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Entitlement_Assignment')</span></a>
            </li>
            @endcan
            @if (env('COMPANY') == 'JSML')
            @can('Fuel Rate')
            <li class="nav-item {{ request()->segment(1) == 'fuel_rate' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('fuel_rate.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Fuel_Rate')</span></a>
            </li>
            @endcan
            @endif
            @can('Incentive Employees')
            <li class="nav-item {{ request()->segment(1) == 'IncentiveEmployee' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('IncentiveEmployee.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Incentive_Employees')</span></a>
            </li>
            @endcan
            @can('Insurance Types')
            <li class="nav-item {{ request()->segment(1) == 'insuranceTypes' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('insuranceTypes.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Insurance_Types')</span></a>
            </li>
            @endcan
            @can('Insurance Sub Types')
            <li class="nav-item {{ request()->segment(1) == 'insuranceSubTypes' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('insuranceSubTypes.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Insurance_Sub_Types')</span></a>
            </li>
            @endcan
            @can('Insurance')
            <li class="nav-item {{ request()->segment(1) == 'insuranceAllocation' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('insuranceAllocation.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.insurance')</span></a>
            </li>
            @endcan
            {{-- @can('Key Performance Indicator')
                        <li class="nav-item {{ request()->segment(1) == 'performanceIndicator' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{route('performanceIndicator.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Key Performance Indicator</span></a>
    </li>
    @endcan --}}
    {{-- @can('Loans')
                        <li class="nav-item {{ request()->segment(1) == 'loans' ? 'active' : ''}}">
    <a class="d-flex align-items-center" href="{{route('loans.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Loans</span></a>
    </li>
    @endcan --}}

    @can('Medical Claim')
    <li class="nav-item {{ request()->segment(1) == 'medicalClaims' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{route('medicalClaims.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Medical Claim @lang('app.Medical_Claim')</span></a>
    </li>
    @endcan
    @can('Overtime Hourly Pay And Net Salary Column')
    <li class="nav-item {{ request()->segment(1) == 'overtime_hourly_paycolumn' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Overtime_Hourly_Pay_And_Net_Salary_Column')">
        <a class="d-flex align-items-center" href="{{route('overtime_hourly_paycolumn.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Overtime_Hourly_Pay_And_Net_Salary_Column')</span></a>
    </li>
    @endcan
    @can('Payslip Templates')
    <li class="nav-item {{ request()->segment(1) == 'payslip_template' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Payslip_Templates')">
        <a class="d-flex align-items-center" href="{{route('payslip_template.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Payslip_Templates')</span></a>
    </li>
    @endcan
    @can('Payment Codes')
    <li class="nav-item {{ request()->segment(1) == 'paymentCodes' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{route('paymentCodes.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Payment_Codes')</span></a>
    </li>
    @endcan
    @can('Payroll Processing')
    <li class="nav-item {{ request()->segment(1) == 'payrollProcessing' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{route('payrollProcessing.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Payroll_Processing')</span></a>
    </li>
    @endcan
    @can('JV Payment')
    <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'payroll-accrual-payment' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{url('payroll-accrual-payment')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Payroll_Accrual_Payment')</span></a>
    </li>
    @endcan
    @can('JV Voucher')
    <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'accrual-payment-vouchers' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{url('accrual-payment-vouchers')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Accrual_Payment_Vouchers')</span></a>
    </li>
    @endcan
    @can('Payroll Columns')
    <li class="nav-item {{ request()->segment(1) == 'payrollColumns' || request()->segment(1) == 'create' ? 'active' : '' }}">
        <a class="d-flex align-items-center" href="{{route('payrollColumns.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Payroll_Columns')</span></a>
    </li>
    @endcan
    @can('Payroll Columns Bulk Edit')
    <li class="nav-item {{ request()->segment(1) == 'payroll_bulk_edit' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Payroll_Columns_Bulk_Edit')">
        <a class="d-flex align-items-center" href="{{route('payroll_bulk_edit.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Payroll_Columns_Bulk_Edit')</span></a>
    </li>
    @endcan
    @can('Report Columns')
    <li class="nav-item {{ request()->segment(1) == 'report_columns' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{route('report_columns.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Report_Columns')</span></a>
    </li>
    @endcan
    @can('Salary Setup')
    <li class="nav-item {{ request()->segment(1) == 'salarySetup' || request()->segment(1) == 'create' ? 'active' : '' }}">
        <a class="d-flex align-items-center" href="{{route('salarySetup.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Salary_Setup')</span></a>
    </li>
    @endcan
    @can('Salary Slip')
    <li class="nav-item {{ request()->segment(1) == 'salary_slip' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{route('salary_slip.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Salary_Slip')</span></a>
    </li>
    @endcan
    @can('Salary Component History')
    <li class="nav-item {{ request()->segment(1) == 'salary_comp_history' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Salary_Component_History')">
        <a class="d-flex align-items-center" href="{{route('salary_comp_history.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Salary_Component_History')</span></a>
    </li>
    @endcan
    @can('Salary Plans')
    <li class="nav-item {{ request()->segment(1) == 'salary_plan' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{route('salary_plan.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Salary_Plans')</span></a>
    </li>
    @endcan
    @can('Tax Report')
    <li class="nav-item {{ request()->segment(1) == 'tax_report' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{route('tax_report.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Tax_Report')</span></a>
    </li>
    @endcan
    </li>
    </ul>
    </li>
    @endcan

    @can('Reports')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="#"><i data-feather='file'></i><span class="menu-title text-truncate">@lang('app.reports')</span></a>
        <ul class="menu-content">
            @if (env('COMPANY') == 'Ajmal Dawakhana')
            @can('All Location Salary Report')
            <li class="nav-item {{ request()->segment(1) == 'location_wise_report' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.All_Location_Salary_Report')">
                <a class="d-flex align-items-center" href="{{url('location_wise_report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.All_Location_Salary_Report')</span></a>
            </li>
            @endcan
            @can('Department Wise Salary Report')
            <li class="nav-item {{ request()->segment(1) == 'department_wise_salary_report' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Department_Wise_Salary_Report')">
                <a class="d-flex align-items-center" href="{{url('department_wise_salary_report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Department_Wise_Salary_Report')</span></a>
            </li>
            @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('UIF Monthly Report')
                    <li class="nav-item {{ request()->segment(1) == 'uifMonthlyReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.UIF_Monthly_Report')">
                        <a class="d-flex align-items-center" href="{{url('uifMonthlyReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.UIF_Monthly_Report')</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('UIF Report')
                    <li class="nav-item {{ request()->segment(1) == 'uifReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.UIF_Report')">
                        <a class="d-flex align-items-center" href="{{url('uifReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.UIF_Report')</span></a>
                    </li>
                @endcan
            @endif
             @if (env('COMPANY') == 'HGNHRM')
                @can('UIF File')
                    <li class="nav-item {{ request()->segment(1) == 'UIFFile' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.UIF_File')">
                        <a class="d-flex align-items-center" href="{{url('UIFFile')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.UIF_File')</span></a>
                    </li>
                @endcan
            @endif
            {{-- @if (env('COMPANY') == 'HGNHRM')
                @can('IRP5 Report')
                    <li class="nav-item {{ request()->segment(1) == 'IRP5Report' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="IRP5 Report">
                        <a class="d-flex align-items-center" href="{{url('IRP5Report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">IRP5 Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('EMP501 Report')
                    <li class="nav-item {{ request()->segment(1) == 'EMP501Report' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="EMP501 Report">
                        <a class="d-flex align-items-center" href="{{url('EMP501Report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">EMP501 Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('EMP501 ETI Breakdown Report')
                    <li class="nav-item {{ request()->segment(1) == 'EMP501ETIBreakdownReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="EMP501 ETI Breakdown Report">
                        <a class="d-flex align-items-center" href="{{url('EMP501ETIBreakdownReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">EMP501 ETI Breakdown Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('EMP201 Breakdown Report')
                    <li class="nav-item {{ request()->segment(1) == 'EMP201BreakdownReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="EMP201 Breakdown Report">
                        <a class="d-flex align-items-center" href="{{url('EMP201BreakdownReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">EMP201 Breakdown Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('EMP201')
                    <li class="nav-item {{ request()->segment(1) == 'EMP201' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="EMP201">
                        <a class="d-flex align-items-center" href="{{url('EMP201')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">EMP201</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('EEA2 Report')
                    <li class="nav-item {{ request()->segment(1) == 'EEA2Report' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="EEA2 Report">
                        <a class="d-flex align-items-center" href="{{url('EEA2Report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">EEA2 Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('EEA2 Detail Report')
                    <li class="nav-item {{ request()->segment(1) == 'EEA2DetailReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="EEA2 Detail Report">
                        <a class="d-flex align-items-center" href="{{url('EEA2DetailReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">EEA2 Detail Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('EEA4 Report')
                    <li class="nav-item {{ request()->segment(1) == 'EEA4Report' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="EEA4 Report">
                        <a class="d-flex align-items-center" href="{{url('EEA4Report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">EEA4 Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('EEA4 Detail Report')
                    <li class="nav-item {{ request()->segment(1) == 'EEA4DetailReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="EEA4 Detail Report">
                        <a class="d-flex align-items-center" href="{{url('EEA4DetailReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">EEA4 Detail Report</span></a>
                    </li>
                @endcan
            @endif --}}
            @if (env('COMPANY') == 'HGNHRM')
                @can('Directive Number Report')
                    <li class="nav-item {{ request()->segment(1) == 'DirectiveNumberReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Directive_Number_Report')">
                        <a class="d-flex align-items-center" href="{{url('DirectiveNumberReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Directive Number Report @lang('app.Directive_Number_Report')</span></a>
                    </li>
                @endcan
            @endif
            {{-- @if (env('COMPANY') == 'HGNHRM')
                @can('COIDA Report')
                    <li class="nav-item {{ request()->segment(1) == 'COIDAReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="COIDA Report">
                        <a class="d-flex align-items-center" href="{{url('COIDAReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">COIDA Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('COIDA Breakdown Report')
                    <li class="nav-item {{ request()->segment(1) == 'COIDABreakdownReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="COIDA Breakdown Report">
                        <a class="d-flex align-items-center" href="{{url('COIDABreakdownReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">COIDA Breakdown Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('Component Variance Report')
                    <li class="nav-item {{ request()->segment(1) == 'ComponentVarianceReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Component Variance Report">
                        <a class="d-flex align-items-center" href="{{url('ComponentVarianceReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Component Variance Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('Component Variance Totals Report')
                    <li class="nav-item {{ request()->segment(1) == 'ComponentVarianceTotalsReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Component Variance Totals Report">
                        <a class="d-flex align-items-center" href="{{url('ComponentVarianceTotalsReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Component Variance Totals Report</span></a>
                    </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'HGNHRM')
                @can('Payroll Recon Report')
                    <li class="nav-item {{ request()->segment(1) == 'PayrollReconReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Payroll Recon Report">
                        <a class="d-flex align-items-center" href="{{url('PayrollReconReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Payroll Recon Report</span></a>
                    </li>
                @endcan
            @endif --}}
            @if (env('COMPANY') == 'RoofLine')
                @can('Department Wise Salary')
                <li class="nav-item {{ request()->segment(1) == 'departmentWiseSalary' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Department_Wise_Salary')">
                    <a class="d-flex align-items-center" href="{{url('departmentWiseSalary')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Department_Wise_Salary')</span></a>
                </li>
                @endcan
                @can('Department Wise Salary Summery')
                <li class="nav-item {{ request()->segment(1) == 'departmentWiseSalarySummery' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Department_Wise_Salary_Summary')">
                    <a class="d-flex align-items-center" href="{{url('departmentWiseSalarySummery')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Department_Wise_Salary_Summary')</span></a>
                </li>
                @endcan
            @endif
            @if (env('COMPANY') == 'JSML')
            @can('Bank Salary Report')
            <li class="nav-item {{ request()->segment(1) == 'bank_salary_report' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Cash_Salary_Sheet')">
                <a class="d-flex align-items-center" href="{{url('bank_salary_report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Cash_Salary_Sheet')</span></a>
            </li>
            @endcan
            @can('Bank Salary Sheet')
            <li class="nav-item {{ request()->segment(1) == 'bank_salary_sheet' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Bank_Salary_Sheet')">
                <a class="d-flex align-items-center" href="{{url('bank_salary_sheet')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Bank_Salary_Sheet')</span></a>
            </li>
            @endcan
            @can('Department Wise Salary')
            <li class="nav-item {{ request()->segment(1) == 'departmentWiseSalary' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Department_Wise_Salary')">
                <a class="d-flex align-items-center" href="{{url('departmentWiseSalary')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Department_Wise_Salary')</span></a>
            </li>
            @endcan
            @can('Department Wise Salary Summery')
            <li class="nav-item {{ request()->segment(1) == 'departmentWiseSalarySummery' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Department_Wise_Salary_Summary')">
                <a class="d-flex align-items-center" href="{{url('departmentWiseSalarySummery')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Department_Wise_Salary_Summary')</span></a>
            </li>
            @endcan
            @can('Employee Salary History Report')
                <li class="nav-item {{ request()->segment(1) == 'salaryHistory' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Employee_Salary_History_Report')">
                    <a class="d-flex align-items-center" href="{{url('salaryHistory')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Employee_Salary_History_Report')</span></a>
                </li>
            @endcan
            @can('EOBI Monthly Report')
            <li class="nav-item {{ request()->segment(1) == 'eobiMonthlyReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.EOBI_Monthly_Report')">
                <a class="d-flex align-items-center" href="{{url('eobiMonthlyReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.EOBI_Monthly_Report')</span></a>
            </li>
            @endcan
            @can('EOBI Monthly Summary')
            <li class="nav-item {{ request()->segment(1) == 'eobiMonthlySummery' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.EOBI_Monthly_Summary')">
                <a class="d-flex align-items-center" href="{{url('eobiMonthlySummery')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.EOBI_Monthly_Summary')</span></a>
            </li>
            @endcan
            @can('Gross Salary Report')
            <li class="nav-item {{ request()->segment(1) == 'gross_salary_report' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Gross_Salary_Report')">
                <a class="d-flex align-items-center" href="{{url('gross_salary_report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Gross_Salary_Report')</span></a>
            </li>
            @endcan
            @can('PESSI Monthly Report')
            <li class="nav-item {{ request()->segment(1) == 'pessiMonthlyReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.PESSI_Monthly_Report')">
                <a class="d-flex align-items-center" href="{{url('pessiMonthlyReport')}}?type=pessi"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.PESSI_Monthly_Report')</span></a>
            </li>
            @endcan
            @can('PESSI Monthly Summary')
            <li class="nav-item {{ request()->segment(1) == 'pessiMonthlySummery' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.PESSI_Monthly_Summary')">
                <a class="d-flex align-items-center" href="{{url('pessiMonthlySummery')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.PESSI_Monthly_Summary')</span></a>
            </li>
            @endcan
            @can('Provident Fund Report')
            <li class="nav-item {{ request()->segment(1) == 'providentFundReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Provident_Fund_Report')">
                <a class="d-flex align-items-center" href="{{url('providentFundReport')}}?type=pf_fund"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Provident_Fund_Report')</span></a>
            </li>
            @endcan
            @endif
            @if (env('COMPANY') == 'CLINIX')
                @can('Designation Wise Salary')
                    <li class="nav-item {{ request()->segment(1) == 'designationWiseSalary' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Designation_Wise_Salary')">
                        <a class="d-flex align-items-center" href="{{url('designationWiseSalary')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Designation_Wise_Salary')</span></a>
                    </li>
                @endcan
                @can('Employee Salary History Report')
                    <li class="nav-item {{ request()->segment(1) == 'salaryHistory' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Employee_Salary_History_Report')">
                        <a class="d-flex align-items-center" href="{{url('salaryHistory')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Employee_Salary_History_Report')</span></a>
                    </li>
                @endcan
            @endif
            @can('Loan Report')
            <li class="nav-item {{ request()->segment(1) == 'loanReport' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('loanReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Loan_Report')</span></a>
            </li>
            @endcan
            @can('Leave Report')
            <li class="nav-item {{ request()->segment(1) == 'leave_report' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('leave_report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Leave_Report')</span></a>
            </li>
            @endcan
            @if (env('COMPANY') == 'Ajmal Dawakhana')
                @can('Leave Report')
                <li class="nav-item {{ request()->segment(1) == 'annualLeaveEncashmentReport' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('Annual Leave Encashment Report')">
                    <a class="d-flex align-items-center" href="{{url('annualLeaveEncashmentReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('Annual Leave Encashment Report')</span></a>
                </li>
                @endcan
            @endif
            @can('Deduction Report')
            <li class="nav-item {{ request()->segment(1) == 'deductionReport' ? 'active' : ''}}">
                @if (env('COMPANY') == 'Ajmal Dawakhana')
                    <a class="d-flex align-items-center" href="{{url('deductionReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Overtime_Report')</span></a>
                @else
                    <a class="d-flex align-items-center" href="{{url('deductionReport')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Deduction_Report')</span></a>
                @endif
            </li>
            @endcan
            @can('Login Report')
            <li class="nav-item {{ request()->segment(1) == 'Login Report' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('login-report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Login_Report')</span></a>
            </li>
            @endcan
            {{-- @can('Deduction Report') --}}
            <li class="nav-item {{ request()->segment(1) == 'payrollLedger' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('payrollLedger')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Payroll_Ledger')</span></a>
            </li>
            {{-- @endcan --}}
            @if (env('COMPANY') == 'CLINIX')
                @can('Time Conflict Report')
                    <li class="nav-item {{ request()->segment(1) == 'time_conflict_report' ? 'active' : ''}}">
                        <a class="d-flex align-items-center" href="{{url('time_conflict_report')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Time Conflict Report</span></a>
                    </li>
                @endcan
            @endcan
        </ul>
    </li>
    @endcan
    @can('Request & Approvals')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="#"><i data-feather='git-pull-request'></i><span class="menu-title text-truncate">@lang('app.Request_Approvals')</span></a>
        <ul class="menu-content">
            @can('Approvals')
                <li class="nav-item {{ request()->segment(1) == 'approvals' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('approvals.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.approvals')</span></a>
                </li>
            @endcan
            @can('Add Salary')
                <li class="nav-item {{ request()->segment(1) == 'addSalary' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('addSalary.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Add Salary Component Request</span></a>
                </li>
            @endcan
            @can('Bulk OverTime Management')
            <li class="nav-item {{ request()->segment(1) == 'bulk_overtime_management' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Bulk_OverTime_Management')">
                <a class="d-flex align-items-center" href="{{route('bulk_overtime_management.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Bulk_OverTime_Management')</span></a>
            </li>
            @endcan
            @can('Bulk Advance Approval')
            <li class="nav-item {{ request()->segment(1) == 'bulk_advance_approval' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Bulk_Advance_Approval')">
                <a class="d-flex align-items-center" href="{{route('bulk_advance_approval.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Bulk_Advance_Approval')</span></a>
            </li>
            @endcan
            @can('Department Change')
            <li class="nav-item {{ request()->segment(1) == 'department_change' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('department_change.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Department_Change')</span></a>
            </li>
            @endcan
            @can('Loan/Advance')
            <li class="nav-item {{ request()->segment(1) == 'loan_request' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('loan_request.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Loan_Advance')</span></a>
            </li>
            @endcan
            @can('Leave Request')
            <li class="nav-item {{ request()->segment(1) == 'employee_leave_requests' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('employee_leave_requests.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Leave_Request')</span></a>
            </li>
            @endcan
            @if (env('COMPANY') == 'CLINIX')
                @can('Manual Attendance Approve Request')
                <li class="nav-item {{ request()->segment(1) == 'attendance_approval' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Manual_Attendance_Approval')">
                    <a class="d-flex align-items-center" href="{{route('attendance_approval.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Manual_Attendance_Approval')</span></a>
                </li>
                @endcan
            @else
                @can('Manual Attendance Approval')
                <li class="nav-item {{ request()->segment(1) == 'attendance_approval' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Manual_Attendance_Approval')">
                    <a class="d-flex align-items-center" href="{{route('attendance_approval.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Manual_Attendance_Approval')</span></a>
                </li>
                @endcan
            @endif
            @can('Miscellaneous Requests')
            <li class="nav-item {{ request()->segment(1) == 'MiscellaneousRequestType' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Miscellaneous_Request_Type')">
                <a class="d-flex align-items-center" href="{{route('MiscellaneousRequestType.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Miscellaneous_Request_Type')</span></a>
            </li>
            @endcan
            @can('Miscellaneous Requests')
            <li class="nav-item {{ request()->segment(1) == 'miscellaneous' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Miscellaneous_Requests')">
                <a class="d-flex align-items-center" href="{{route('miscellaneous.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Miscellaneous_Requests')</span></a>
            </li>
            @endcan
            @can('Overtime Management')
            <li class="nav-item {{ request()->segment(1) == 'overtime_managements' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Overtime_Management')">
                <a class="d-flex align-items-center" href="{{route('overtime_managements.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Overtime_Management')</span></a>
            </li>
            @endcan
            @can('Short Leave Request')
            <li class="nav-item {{ request()->segment(1) == 'short_leave_requests' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('short_leave_requests.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Short_Leave_Request')</span></a>
            </li>
            @endcan
            @can('Shift Change Requests')
            <li class="nav-item {{ request()->segment(1) == 'shift_change' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Shift_Change_Requests')">
                <a class="d-flex align-items-center" href="{{route('shift_change.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Shift_Change_Requests')</span></a>
            </li>
            @endcan
            @if (env('COMPANY') == 'CLINIX')
                {{-- @can('Shift Change Requests') --}}
                <li class="nav-item {{ request()->segment(1) == 'fineReversal' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Fine_Reversal')">
                    <a class="d-flex align-items-center" href="{{route('fineReversal.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Fine_Reversal')</span></a>
                </li>
                {{-- @endcan --}}
            @endif
            @can('Travel Requests')
            <li class="nav-item {{ request()->segment(1) == 'travel_requests' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('travel_requests.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Travel_Requests')</span></a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('Setup')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="#"><i data-feather='file-text'></i><span class="menu-title text-truncate">@lang('app.setup')</span></a>
        <ul class="menu-content">
            @can('Banks')
            <li class="nav-item {{ request()->segment(1) == 'bank' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('bank.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.banks')</span></a>
            </li>
            @endcan
            @can('City')
            <li class="nav-item {{ request()->segment(1) == 'city' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('city.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.city')</span></a>
            </li>
            @endcan
            @can('Company Organogram')
            {{-- <li class="nav-item {{ request()->segment(1) == 'organogram' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('organogram')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Company Organogram</span></a>
            </li> --}}
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'organogram' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{url('organogram')}}">
                    <i data-feather="circle"></i><span class="menu-title text-truncate"
                    >@lang('app.Company_Organogram')</span>
                </a>
            </li>
            @endcan
            @can('Company Setup')
            <li class="nav-item {{ request()->segment(1) == 'companysetup' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('companysetup.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Company_Setup')</span></a>
            </li>
            @endcan
            @can('Company Banks')
            <li class="nav-item {{ request()->segment(1) == 'company_banks' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('company_banks.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Company_Banks')</span></a>
            </li>
            @endcan
            @can('Company Structures')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'companyStructures' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('companyStructures.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Company_Structures')</span></a>
            </li>
            @endcan
            @can('Company Graph')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'companyStructures/create' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('companyStructures.create')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Company_Graph')</span></a>
            </li>
            @endcan
            @can('CurrencyType')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'currency-types' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('currency-types.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Currency_Type')</span></a>
            </li>
            @endcan
            @can('industryCode')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'industry-codes' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('industry-codes.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Industry_Code')</span></a>
            </li>
            @endcan
            @can('industryGroup')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'industry-groups' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('industry-groups.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Industry_Group')</span></a>
            </li>
            @endcan

            @can('Document Management')
            <li class="nav-item {{ request()->segment(1) == 'document_management' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Document_Management')">
                <a class="d-flex align-items-center" href="{{route('document_management.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Document_Management')</span></a>
            </li>
            @endcan
            @can('ENV Checks')
            <li class="nav-item {{ request()->segment(1) == 'env_check' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.ENV_Checks')">
                <a class="d-flex align-items-center" href="{{route('env_check.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.ENV_Checks')</span></a>
            </li>
            @endcan
            @can('Final Settlement Setup')
                <li class="nav-item {{ request()->segment(1) == 'final_settlement_setup' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Final_Settlement_Setup')">
                    <a class="d-flex align-items-center" href="{{route('final_settlement_setup.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Final_Settlement_Setup')</span></a>
                </li>
            @endcan
            @can('Job Titles')
            <li class="nav-item {{ request()->segment(1) == 'job_titles' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('job_titles.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Job_Titles')</span></a>
            </li>
            @endcan
            @can('Job Details Setup')
            <li class="nav-item {{ request()->segment(1) == 'jobDetailsSetup' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('jobDetailsSetup.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Job_Details_Setup')</span></a>
            </li>
            @endcan
            @can('Job Position Setup')
            <li class="nav-item {{ request()->segment(1) == 'job_positions' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('job_positions.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Job_Position')</span></a>
            </li>
            @endcan
            @can('Leave Settings')
            <li class="nav-item {{ request()->segment(1) == 'leave_types' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('leave_types.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Leave_Settings')</span></a>
            </li>
            @endcan
            @can('Maschine Management')
            <li class="nav-item {{ request()->segment(1) == 'machine_management' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Machine_Management')">
                <a class="d-flex align-items-center" href="{{route('machine_management.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Machine_Management')</span></a>
            </li>
            @endcan
            @can('Manage Metadata')
            <li class="nav-item {{ request()->segment(1) == 'manageMetadata' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('manageMetadata.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Manage_Metadata')</span></a>
            </li>
            @endcan
            @can('Organogram Setup')
                <li
                    class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'organoGramSetup' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('organoGramSetup.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Organogram_Setup')</span></a>
                </li>
            @endcan
            @can('Overtime Administration')
            <li class="nav-item {{ request()->segment(1) == 'overtimeAdministration' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Overtime_Administration')">
                <a class="d-flex align-items-center" href="{{route('overtimeAdministration.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Overtime_Administration')</span></a>
            </li>
            @endcan
            @can('Tax')
            <li class="nav-item {{ request()->segment(1) == 'taxes' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Tax_Management')">
                <a class="d-flex align-items-center" href="{{route('taxes.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Tax_Management')</span></a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('Settings')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="#"><i data-feather='settings'></i><span class="menu-title text-truncate">@lang('app.settings')</span></a>
        <ul class="menu-content">
            @if (env('COMPANY') == 'JSML')
            @can('GL Settings')
            <li class="nav-item {{ request()->segment(1) == 'glSettings' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('glSettings')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.GL_Settings')</span></a>
            </li>
            @endcan
            {{-- @can('Loan Advance GL Settings')
            <li class="nav-item {{ request()->segment(1) == 'loan-advance-glsettings' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('loan-advance-glsettings')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Loan Advance GL Settings</span></a>
            </li>
            @endcan --}}
            @can('GL Accounts')
            <li class="nav-item {{ request()->segment(1) == 'glAccounts' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('glAccounts')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.GL_Accounts')</span></a>
            </li>
            @endcan
            @can('Payroll ERP Address')
            <li class="nav-item {{ request()->segment(1) == 'get-payrolls-erpurls' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('get-payrolls-erpurls')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Payroll_ERP_Address')</span></a>
            </li>
            @endcan
            @endif
            @can('Permission Title')
                <li class="nav-item {{ request()->segment(1) == 'permission-titles' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('permission-titles.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Permission_Titles')</span></a>
                </li>
            @endcan
            @can('Roles & Permission')
            <li class="nav-item {{ request()->segment(1) == 'roles' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('roles.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Roles_&_Permission')</span></a>
            </li>
            @endcan
            @can('Users')
            <li class="nav-item {{ request()->segment(1) == 'users' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('users.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.users')</span></a>
            </li>
            @endcan
            @can('News Page Settings')
            <li class="nav-item {{ request()->segment(1) == 'news_page' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('news_page')}}"><i data-feather='bell'></i><span class="menu-item text-truncate">@lang('app.News_Page_Settings')</span></a>
            </li>
            @endcan
            @can('Add Headlines')
            <li class="nav-item {{ request()->segment(1) == 'add_headlines' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('add_headlines')}}"><i data-feather='alert-circle'></i><span class="menu-item text-truncate">@lang('app.Add_Headlines')</span></a>
            </li>
            @endcan
            @can('DisciplinariesRules')
            <li class="nav-item {{ request()->segment(1) == 'disciplinariesRules' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('disciplinariesRules')}}"><i data-feather='key'></i><span class="menu-item text-truncate">@lang('app.Disciplinaries_Rules')</span></a>
            </li>
            @endcan

        </ul>
    </li>
    @endcan
    @can('Training')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="#"><i data-feather='file-text'></i><span class="menu-title text-truncate">@lang('app.training')</span></a>
        <ul class="menu-content">
            @can('Inner Training')
                <li class="nav-item {{ request()->segment(1) == 'training' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('training.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.training')</span></a>
                </li>
            @endcan
            @can('Training Setup')
                <li class="nav-item {{ request()->segment(1) == 'training_setup' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('training_setup.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Training_Setup')</span></a>
                </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('Time Management')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="#"><i data-feather='clock'></i><span class="menu-title text-truncate">@lang('app.Time_Management')</span></a>
        <ul class="menu-content">
            @if (env('COMPANY') == 'JSML')
                @can('Bulk Time Out')
                    <li class="nav-item {{ request()->segment(1) == 'bulk_time_out_jsml' ? 'active' : ''}}">
                        <a class="d-flex align-items-center" href="{{route('bulk_time_out_jsml.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Bulk_Time_Out')</span></a>
                    </li>
                @endcan
            @else
                @can('Bulk Time Out')
                    <li class="nav-item {{ request()->segment(1) == 'bulk_time_out' ? 'active' : ''}}">
                        <a class="d-flex align-items-center" href="{{route('bulk_time_out.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Bulk_Time_Out')</span></a>
                    </li>
                @endcan
            @endif
            @can('Bulk Attendance')
            <li class="nav-item {{ request()->segment(1) == 'bulk_attendance_job_wise/create' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('bulk_attendance_job_wise.create')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Bulk_Attendance')</span></a>
            </li>
            @endcan
            @can('Bulk Shift Management')
            <li class="nav-item {{ request()->segment(1) == 'bulk_shift_management' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Bulk_Shift_Management')">
                <a class="d-flex align-items-center" href="{{route('bulk_shift_management.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Bulk_Shift_Management')</span></a>
            </li>
            @endcan
            @can('Bulk Time In Department Wise')
            <li class="nav-item {{ request()->segment(1) == 'bulk_time_in_department' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Bulk_Time_In_Department_Wise')">
                <a class="d-flex align-items-center" href="{{route('bulk_time_in_department.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Bulk_Time_In_Department_Wise')</span></a>
            </li>
            @endcan
            @can('Bulk Attendance Job Title Wise')
            <li class="nav-item {{ request()->segment(1) == 'bulk_attendance_job_wise' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Bulk_Attendance_Job_Title_Wise')">
                <a class="d-flex align-items-center" href="{{route('bulk_attendance_job_wise.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Bulk_Attendance_Job_Title_Wise')</span></a>
            </li>
            @endcan
            {{-- @can('Bulk Time In Employee Wise')
                        <li class="nav-item {{ request()->segment(1) == 'bulk_time_in_employee' ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{route('bulk_time_in_employee.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Bulk Time In Employee Wise</span></a>
    </li>
    @endcan --}}
    @can('Shift Types')
    <li class="nav-item {{ request()->segment(1) == 'shiftTypes' ? 'active' : ''}}">
        <a class="d-flex align-items-center" href="{{route('shiftTypes.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Shift_Types')</span></a>
    </li>
    @endcan
    @can('Shift Emp Management')
    <li class="nav-item {{ request()->segment(1) == 'shiftManagement' || request()->segment(1) == 'create' ? 'active' : '' }}" data-bs-toggle="tooltip" data-bs-placement="right" title="@lang('app.Shift_Employee_Management')">
        <a class="d-flex align-items-center" href="{{route('shiftManagement.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">@lang('app.Shift_Employee_Management')</span></a>
    </li>
    @endcan
    </ul>
    </li>
    @endcan
    </ul>
</div>
