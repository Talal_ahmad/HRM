<div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
        <li class="nav-item me-auto">
            <a class="navbar-brand" href="{{url('/')}}">
                @if (!empty(env('COMPANY_LOGO')))
                    
                @else
                    @if (env('COMPANY') == 'Ajmal Dawakhana')
                        <h2 class="brand-text" style="font-size: 17px;">{{env('COMPANY')}}</h2>
                    @else
                        <h2 class="brand-text">{{env('COMPANY')}}</h2>
                    @endif
                @endif
            </a>
        </li>
        <li class="nav-item nav-toggle">
            <a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse">
                <i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4 text-primary" data-feather="disc" data-ticon="disc"></i>
            </a>
        </li>
    </ul>
</div>
<div class="shadow-bottom"></div>
<div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        @if (env('COMPANY') == 'CLINIX' || env('COMPANY') == 'HGNHRM')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == '/' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('/')}}">
                    <i data-feather="bell"></i><span class="menu-title text-truncate" data-i18n="Dashboards">News Page</span>
                </a>
            </li>
        @else
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'news_dashboard' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('news_dashboard')}}">
                    <i data-feather="bell"></i><span class="menu-title text-truncate" data-i18n="Dashboards">News Page</span>
                </a>
            </li>
        @endif
        {{-- @if (env('COMPANY') == 'CLINIX')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'dashboard' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('dashboard')}}">
                    <i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Dashboard</span>
                </a>
            </li>
        @else
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == '/' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{url('/')}}">
                    <i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Dashboard</span>
                </a>
            </li>
        @endif --}}
        <li class="nav-item {{ Request::url() == url('staff_directory/' . Auth::user()->employee) ? 'active' : '' }}">
            <a class="d-flex align-items-center" href="{{ url('staff_directory/' . Auth::user()->employee) }}"><i data-feather='user'></i><span class="menu-title text-truncate" data-i18n="Invoice">Profile</span></a>
        </li>
        <li class="nav-item">
            <a class="d-flex align-items-center" href="#"><i data-feather='sliders'></i><span class="menu-title text-truncate" data-i18n="Invoice">Attendance</span></a>
            <ul class="menu-content">
                <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'attendance_report' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{route('attendance_report.index')}}?type=single"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Attendance Report</span></a>
                </li>
                <li class="nav-item {{ request()->segment(1) == 'disciplinariestable' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{url('disciplinariestable')}}">
                        <i data-feather="key"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Disciplinaries Records</span>
                    </a>
                </li>
            </ul>
        </li>
    <li class="nav-item">
        <a class="d-flex align-items-center" href="#"><i data-feather='git-pull-request'></i><span class="menu-title text-truncate" data-i18n="Invoice">Request/Approvals</span></a>
        <ul class="menu-content">
            <li class="nav-item {{ request()->segment(1) == 'approvals' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('approvals.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Approvals</span></a>
            </li>
            <li class="nav-item {{ request()->segment(1) == 'bulk_overtime_management' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Bulk OverTime Management">
                <a class="d-flex align-items-center" href="{{route('bulk_overtime_management.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Bulk OverTime Management</span></a>
            </li>
            <li class="nav-item {{ request()->segment(1) == 'bulk_advance_approval' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Bulk Advance Approval">
                <a class="d-flex align-items-center" href="{{route('bulk_advance_approval.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Bulk Advance Approval</span></a>
            </li>
            <li class="nav-item {{ request()->segment(1) == 'department_change' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('department_change.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Department Change</span></a>
            </li>
            <li class="nav-item {{ request()->segment(1) == 'loan_request' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('loan_request.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Loan/Advance</span></a>
            </li>
            <li class="nav-item {{ request()->segment(1) == 'employee_leave_requests' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('employee_leave_requests.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Leave Request</span></a>
            </li>
            @if (env('COMPANY') == 'CLINIX')
                @can('Manual Attendance Approve Request')
                <li class="nav-item {{ request()->segment(1) == 'attendance_approval' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Manual Attendance Approval">
                    <a class="d-flex align-items-center" href="{{route('attendance_approval.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Manual Attendance Approval</span></a>
                </li>
                @endcan
            @else
                <li class="nav-item {{ request()->segment(1) == 'attendance_approval' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Manual Attendance Approval">
                    <a class="d-flex align-items-center" href="{{route('attendance_approval.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Manual Attendance Approval</span></a>
                </li>
            @endif
            <li class="nav-item {{ request()->segment(1) == 'MiscellaneousRequestType' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Miscellaneous Request Type">
                <a class="d-flex align-items-center" href="{{route('MiscellaneousRequestType.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Miscellaneous Request Type</span></a>
            </li>
            <li class="nav-item {{ request()->segment(1) == 'miscellaneous' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Miscellaneous Requests">
                <a class="d-flex align-items-center" href="{{route('miscellaneous.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Miscellaneous Requests</span></a>
            </li>
            <li class="nav-item {{ request()->segment(1) == 'overtime_managements' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Overtime Management">
                <a class="d-flex align-items-center" href="{{route('overtime_managements.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Overtime Management</span></a>
            </li>
            <li class="nav-item {{ request()->segment(1) == 'short_leave_requests' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('short_leave_requests.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Short Leave Request</span></a>
            </li>
            <li class="nav-item {{ request()->segment(1) == 'shift_change' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Shift Change Requests">
                <a class="d-flex align-items-center" href="{{route('shift_change.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Shift Change Requests</span></a>
            </li>
            @if (env('COMPANY') == 'CLINIX')
                <li class="nav-item {{ request()->segment(1) == 'fineReversal' ? 'active' : ''}}" data-bs-toggle="tooltip" data-bs-placement="right" title="Fine Reversal Request">
                    <a class="d-flex align-items-center" href="{{route('fineReversal.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Fine Reversal</span></a>
                </li>
            @endif
            <li class="nav-item {{ request()->segment(1) == 'travel_requests' ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{route('travel_requests.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Travel Requests</span></a>
            </li>
        </ul>
    </li>
    </ul>
</div>