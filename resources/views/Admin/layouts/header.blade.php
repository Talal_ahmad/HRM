<!-- BEGIN: Header-->
{{-- @if (!empty(auth()->user()->id))
        
    @endif --}}
@php
use App\Models\Disciplinarie;
use App\Models\News_Page;
if(!empty(Auth::check() ? Auth::user()->username : '')){
    $disciplinaries= Disciplinarie::whereIn('employee_department_id',login_user_departments())->whereRaw('FIND_IN_SET(?, watched_by) = 0', auth()->user()->id)
    ->get(['warning_type','employee_department_id','attachment_image','status','id','watched_by']);
    $Allnews= News_Page::whereRaw("FIND_IN_SET(department_ids, '" . implode(',', login_user_departments()) . "') = 0")->whereRaw('FIND_IN_SET(?, watched_by) = 0', auth()->user()->id)
    ->get(['message','id','status','department_ids','watched_by']);
    $id=auth()->user()->id;
    $id="$id";
}
@endphp       
@if(!empty(Auth::check() ? Auth::user()->username : ''))
<nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
    <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav d-xl-none">
                <li class="nav-item">
                    <a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a>
                </li>
            </ul>
            <ul class="nav navbar-nav bookmark-icons">
                <li class="nav-item d-none d-lg-block">
                    <a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a>
                </li>
            </ul>
        </div>
        <ul class="nav navbar-nav align-items-center ms-auto">
            <li class="nav-item dropdown dropdown-notification me-25">
                <a class="nav-link" href="#" data-bs-toggle="dropdown">
                    <i class="ficon" data-feather="bell"></i>
                    <span class="badge rounded-pill bg-danger badge-up">{{Disciplinarie::whereIn('employee_department_id',login_user_departments())->whereRaw('FIND_IN_SET(?, watched_by) = 0', auth()->user()->id)->count()+News_Page::whereRaw("FIND_IN_SET(department_ids, '" . implode(',', login_user_departments()) . "') = 0")->whereRaw('FIND_IN_SET(?, watched_by) = 0', auth()->user()->id)->count()}}</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end custom-scrollbar-style" style="max-height: 500px; width:300px; overflow-y: auto;">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                            <div class="badge rounded-pill badge-light-primary">{{Disciplinarie::whereIn('employee_department_id',login_user_departments())->whereRaw('FIND_IN_SET(?, watched_by) = 0', auth()->user()->id)->count()+News_Page::whereRaw("FIND_IN_SET(department_ids, '" . implode(',', login_user_departments()) . "') = 0")->whereRaw('FIND_IN_SET(?, watched_by) = 0', auth()->user()->id)->count()}} New</div>
                        </div>
                    </li>
                    @foreach($disciplinaries as $disciplinarie)
                    @if(in_array($disciplinarie->employee_department_id,login_user_departments()) && !strpos($disciplinarie->watched_by,auth()->user()->id))
                
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="#">
                            <div class="list-item d-flex align-items-start">
                                <div class="me-1">
                                    <div class="avatar"><img src="{{asset($disciplinarie->attachment_image)}}" alt="avatar" width="32" height="32"></div>
                                </div>
                                <div class="list-item-body flex-grow-1">
                                    <a class="dropdown-item" onclick="ChangeStatus({{$disciplinarie->id}},{{auth()->user()->id}})" id="disciplinarielink" href="{{url('disciplinariestable')}}">
                                        <p class="media-heading"><span class="fw-bolder">{{$disciplinarie->warning_type}}</span></p>
                                    </a>
                                </div>
                            </div>
                        </a>
                    </li>
                    @endif
                    @endforeach
                    @foreach($Allnews as $news)
                    @if(!empty(array_intersect(login_user_departments(),explode(',' ,$news->department_ids))) && !strpos($news->watched_by,auth()->user()->id))
                
                    <div class="list-item d-flex align-items-center p-2">
                        <h4 class="fw-bolder me-auto mb-0">News Notifications</h4>
                    </div>
                    <a class="d-flex" href="#">
                        <div class="list-item d-flex align-items-start">
                            <div class="me-1">
                                <div class="avatar bg-light-success ms-1">
                                    <div class="avatar-content "><i class="avatar-icon" data-feather="check"></i></div>
                                </div>
                            </div>
                            <div class="list-item-body flex-grow-1">
                                @if((env('COMPANY') == 'CLINIX'))
                                <a class="dropdown-item" onclick="ChangeNewsStatus({{$news->id}},{{auth()->user()->id}})"  href="{{url('/')}}">
                                    {{"Check latest News"}}
                                </a>
                                @else
                                <a class="dropdown-item" onclick="ChangeNewsStatus({{$news->id}},{{auth()->user()->id}})"  href="{{url('news_dashboard')}}">
                                    {{"Check latest News"}}
                                </a>
                                @endif
                            </div>
                        </div>
                    </a>
                    @endif
                    @endforeach
                </ul>
            </li>
            <li class="nav-item dropdown dropdown-language">
                <a class="nav-link dropdown-toggle" id="dropdown-flag" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="flag-icon flag-icon-{{ app()->getLocale() === 'en' ? 'us' : (app()->getLocale() === 'fr' ? 'fr' : 'sa') }}"></i>
                    <span class="selected-language">{{ app()->getLocale() === 'en' ? 'English' : (app()->getLocale() === 'fr' ? 'French' : 'Arabic') }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item {{ app()->getLocale() === 'en' ? 'active' : '' }}" href="{{ route('changeLanguage', ['locale' => 'en']) }}"><i class="flag-icon flag-icon-us"></i> English</a><a class="dropdown-item {{ app()->getLocale() === 'fr' ? 'active' : '' }}" href="{{ route('changeLanguage', ['locale' => 'fr']) }}"><i class="flag-icon flag-icon-fr"></i> French</a>
                    <a class="dropdown-item {{ app()->getLocale() === 'ar' ? 'active' : '' }}" href="{{ route('changeLanguage', ['locale' => 'ar']) }}"><i class="flag-icon flag-icon-sa"></i> Arabic</a>
                </div>
            </li>
            <li class="nav-item dropdown dropdown-user">
                <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="user-nav d-sm-flex d-none">
                        <span class="user-name fw-bolder">{{Auth::check() ? Auth::user()->username : ''}}</span>
                        <span class="user-status">{{Auth::check() ? userRoles() : ''}}</span>
                    </div>
                    <span class="avatar">
                        <img class="round" src="{{asset('images/employees/default_employee.png')}}" alt="avatar" height="40" width="40" />
                        <span class="avatar-status-online"></span>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                        <i class="me-50" data-feather="power"></i> Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
            <li>
        </ul>
    </div>
</nav>
@else
<nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
    <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav d-xl-none">
                <li class="nav-item">
                    <a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a>
                </li>
            </ul>
            <ul class="nav navbar-nav bookmark-icons">
                <li class="nav-item d-none d-lg-block">
                    <a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a>
                </li>
            </ul>
        </div>
        <ul class="nav navbar-nav align-items-center ms-auto">
            <li class="nav-item d-none d-lg-block">
                <img class="nav-link dropdown-toggle dropdown-user-link" src="{{asset('images/company_logo/clinix.png')}}" alt="profile image" height="50" />
            </li>
        </ul>
    </div>
</nav>
@endif
<!-- END: Header-->
<script>
    function ChangeStatus(id,user_id){
        $.ajax({
            url: "ChangeStatus/" + id,
            data:{user_id:user_id},
            success: function() {
            //    window.reload();
            }
        });

    }
    function ChangeNewsStatus(id,user_id){
        $.ajax({
            url: "ChangeNewsStatus/" + id,
            data:{user_id:user_id},
            success: function() {
            //    window.reload();
            }
        });

    }
</script>