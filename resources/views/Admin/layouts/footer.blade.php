<!-- BEGIN: Footer-->
@if(!empty(Auth::check() ? Auth::user()->username : ''))
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-md-start d-block d-md-inline-block mt-25">COPYRIGHT &copy; {{date('Y')}} {{env('COMPANY_NAME','')}}<span class="d-none d-sm-inline-block">, All rights Reserved</span></span><span class="float-md-end d-none d-md-block">Powered By: Bright Lines Solution (PVT) LTD. (www.bls.com.pk, Waqar Safdar)</span></p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
@endif
<!-- END: Footer-->