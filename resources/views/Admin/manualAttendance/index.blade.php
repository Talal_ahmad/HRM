@extends('Admin.layouts.master')
@section('title', 'Manual Attendance')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Manual Attendance</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Manual Attendance</a>
                                </li>
                                <li class="breadcrumb-item active">Manual Attendance
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header row"></div>
        <div class="content-body">
            <section id="basic-dataTable">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <!--Search Form -->
                            <div class="card-body">
                                <form id="search_form">
                                    <div class="row g-1 mb-md-1">
                                        <div class="col-md-4">
                                            <label class="form-label">Department:</label>
                                            <select name="departmentFilter" onchange="employeeFun()" id="departmentFilter"
                                                class="select2 form-select" data-placeholder="Select Department" required>
                                                <option value="">Select Department</option>
                                                @foreach (departments() as $department)
                                                    <option value="{{ $department->id }}">{{ $department->title }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if (env('COMPANY') == 'JSML')        
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="title">Section</label>
                                                    <select name="section[]" id="section" data-placeholder="Select Section" class="select2 form-select" multiple required>
                                                        
                                                    </select>
                                                    <div class="button-container mt-1">
                                                        <button class="btn btn-sm btn-primary" type="button"
                                                            onclick="selectAll('#section')">Select All</button>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                            onclick="deselectAll('#section')">Deselect All</button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-md-4">
                                            <label class="form-label" for="employeeFilter">Employee</label>
                                            <select name="employeeFilter" id="employeeFilter" class="select2 form-select"
                                                data-placeholder="Select Employee" disabled>

                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">From Date:</label>
                                            <input type="text" name="fromDateFilter" id="fromDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">To Date:</label>
                                            <input type="text" name="toDateFilter" id="toDateFilter" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" required />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-end">
                                            <a href="{{url('manualAttendance')}}" type="button" class="btn btn-danger">Reset</a>
                                            <button class="btn btn-primary">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Employee ID</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Designation</th>
                                        @if (env('COMPANY') == 'JSML')        
                                            <th>Section</th>
                                        @else
                                            <th>Department</th>
                                        @endif
                                        <th>In Time</th>
                                        <th>Out Time</th>
                                        <th>Remarks</th>
                                        <th hidden>Employee Id</th>
                                        <th hidden>Department Id</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <div class="modal-size-lg d-inline-block">
                <!-- Add Modal -->
                <div class="modal fade text-start" id="add_modal" data-bs-focus="false" tabindex="-1"
                    aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Add Manual Attendance</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form class="form" id="manual_attendance_add_form">
                                @csrf
                                <div class="modal-body">
                                    <div id="addErrorContainer"></div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="employee">Employee</label>
                                                    <select name="employee" id="employee" class="select2 form-select"
                                                        required>
                                                        <option value="">Select Employee</option>
                                                        @foreach (employees('', '', true) as $employee)
                                                            <option value="{{ $employee->id }}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{ $employee->first_name }} {{ $employee->last_name }}-{{$employee->designation}}-{{$employee->department}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6" id="in_time_div">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="in_time">In Time</label>
                                                    <input type="text" id="in_time"
                                                        class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                        name="in_time" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6" id="out_time_div">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="out_time">Out Time</label>
                                                    <input type="text" id="out_time"
                                                        class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                        name="out_time" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6" id="in_date_div">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="in_date">In Date</label>
                                                    <input type="text" name="in_date" id="in_date"
                                                        class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                        required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6" id="out_date_div">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="out_date">Out Date</label>
                                                    <input type="text" name="out_date" id="out_date"
                                                        class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                        required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="note">Remarks</label>
                                                    <textarea id="note" class="form-control" cols="20" rows="2" placeholder="Type here..." name="note"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        @if(env('COMPANY') == 'JSML')
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="od" value="1"/>
                                                        <label class="custom-control-label">OD</label>
                                                        <input type="checkbox" class="custom-control-input" name="ret" value="1"/>
                                                        <label class="custom-control-label">RET</label>
                                                    </div> 
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                    <button type="submit" class="btn btn-primary form_save" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Edit Modal -->
                <div class="modal fade text-start" id="edit_modal" tabindex="-1"
                    aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Edit Manual Attendance</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form class="form" id="manual_attendance_edit_form">
                                @csrf
                                @method('PUT')
                                <div class="modal-body">
                                    <div id="editErrorContainer"></div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_employee">Employee</label>
                                                    <select name="employee" id="edit_employee" class="select2 form-select"
                                                        required>
                                                        <option value="">Select Employee</option>
                                                        @foreach (employees('', '', true) as $employee)
                                                            <option value="{{ $employee->id }}">{{$employee->employee_id}}-{{$employee->employee_code}}-{{ $employee->first_name }} {{ $employee->last_name }}-{{$employee->designation}}-{{$employee->department}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_in_time">In Time</label>
                                                    <input type="text" id="edit_in_time"
                                                        class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                        name="in_time" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_out_time">Out Time</label>
                                                    <input type="text" id="edit_out_time"
                                                        class="form-control flatpickr-time text-left" placeholder="HH:MM"
                                                        name="out_time" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_in_date">In Date</label>
                                                    <input type="date" name="in_date" id="edit_in_date"
                                                        class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                        required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_out_date">Out Date</label>
                                                    <input type="date" name="out_date" id="edit_out_date"
                                                        class="form-control flatpickr-basic" placeholder="YYYY-MM-DD"
                                                        required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <div class="mb-1">
                                                    <label class="form-label" for="edit_note">Remarks</label>
                                                    <textarea id="edit_note" class="form-control" cols="20" rows="2" placeholder="Type here..." name="note"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        @if(env('COMPANY') == 'JSML')
                                            <div class="col-12">
                                                <div class="mb-1">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="od" value="1" id="edit_od"/>
                                                        <label class="custom-control-label">OD</label>
                                                        <input type="checkbox" class="custom-control-input" name="ret" value="1" id="edit_ret"/>
                                                        <label class="custom-control-label">RET</label>
                                                    </div> 
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                    <button type="submit" class="btn btn-primary form_save" id="save">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
    var company = "{{env('COMPANY')}}"
    @if (env('COMPANY') == 'RoofLine')        
        $("#in_time").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            defaultDate: "9:00"
        });
        $("#out_time").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            defaultDate: "17:30"
        });
    @endif
        var rowid;
        $(document).ready(function() {
            $(document).ready(function() {
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_department_child') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#section').empty();
                            $('#section').html('<option value="">Select Section</option>'); 
                            $.each(response, function(index, value) {
                                
                                console.log(value.id);
                                $('#section').append(
                                    $('<option {{!empty(request('section')) ? in_array($department->id,request('section')) ? 'selected' : '' : ''}}></option>').val(value.id).html(
                                        value.title)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#section').empty();
                }
            });

            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employeeFilter').empty();
                            $('#employeeFilter').html(
                                '<option value="">Select Employee</option>');
                            $.each(response, function(index, value) {
                                $('#employeeFilter').append(
                                    $('<option></option>').val(value.id).html(
                                    value.employee_id + '-' + value.employee_code + '-' + value.first_name + ' ' + value.last_name+ ' ' + value.department)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    // $('#employee').empty();
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Employee Current Shift</h6>');
        });

            datatable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: true,
                ajax: {
                    url: "{{ route('manualAttendance.index') }}",
                    data: function(filter) {
                        filter.departmentFilter = $('#departmentFilter').val();
                        filter.employeeFilter = $('#employeeFilter').val();
                        filter.dateFilter = $('#dateFilter').val();
                        filter.section = $('#section').val();
                        filter.fromDateFilter = $('#fromDateFilter').val();
                        filter.toDateFilter = $('#toDateFilter').val();
                    }
                },
                columns: [{
                        data: 'responsive_id',
                    },
                    {
                        "title": "Sr.No",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'employee_id',
                        name: 'employees.employee_id'
                    },
                    {
                        data: 'employee_code',
                        name: 'employees.employee_code'
                    },
                    {
                        data: 'full_name' , render: function(data , type , row){
                            var middleName = row.middle_name != null ? row.middle_name : '';
                            var lastName = row.last_name != null ? row.last_name : '';
                            return row.first_name + " " + middleName + " " + lastName;
                        },
                        searchable: false,
                        orderable:false,
                    },
                    {
                        data: 'first_name',
                        name: 'employees.first_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'last_name',
                        name: 'employees.last_name',
                        searchable: true,
                        visible: false
                    },
                    {
                        data: 'designation',
                        name: 'jobtitles.name'
                    },
                    {
                        data: 'department',
                        name: 'companystructures.title'
                    },
                    {
                        data: 'in_time',
                    },
                    {
                        data: 'out_time',
                    },
                    {
                        data: 'note',
                    },
                    {
                        data: 'employee',
                        "visible": false,
                    },
                    {
                        data: 'department_id',
                        "visible": false,
                    },
                    {
                        data: '',
                        searchable: false
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        render: function(data, type, full, meta) {
                            return (
                            @can('Manual Attendance Edit Button')    
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                            @endcan
                            @can('Manual Attendance Delete Button')    
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                                +
                            @endcan
                                ''
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10,25,50,100,-1],
                    [10,25,50,100,"All"]
                ],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-1',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                action: newexportaction,
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('Manual Attendance Add New Button')    
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            // Add Data
            $('#manual_attendance_add_form').submit(function(e) {
                
                    // $('.form_save').prop('disabled', true);
                    ButtonStatus('.form_save',true);
                    blockUI();
                
                e.preventDefault();
                $.ajax({
                    url: "{{ route('manualAttendance.store') }}",
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        // $('.form_save').prop('disabled', false);
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            var errorHtml = '<ul>';
                            $.each(response.errors, function(field, messages) {
                                $.each(messages, function(index, value) {
                                    errorHtml += '<li style="color: red;">' + value + '</li>';
                                });
                            });
                            errorHtml += '</ul>';

                            // Append error messages to a div with id "errorContainer"
                            $('#addErrorContainer').html(errorHtml);
                        }
                        else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#manual_attendance_add_form')[0].reset();
                            @if (env('COMPANY') == 'RoofLine')        
                                $("#in_time").flatpickr({
                                    enableTime: true,
                                    noCalendar: true,
                                    dateFormat: "H:i",
                                    defaultDate: "9:00"
                                });
                                $("#out_time").flatpickr({
                                    enableTime: true,
                                    noCalendar: true,
                                    dateFormat: "H:i",
                                    defaultDate: "17:30"
                                });
                            @endif
                            $(".select2").val('').trigger('change')
                            $("#add_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Manual Attendance has been Added Successfully!'
                            })
                            // location.reload();
                        }
                    }
                });
            })
            // Update Data
            $('#manual_attendance_edit_form').submit(function(e) {
                e.preventDefault();
                ButtonStatus('.form_save',true);
                blockUI();
                $.ajax({
                    url: "{{ url('manualAttendance') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        ButtonStatus('.form_save',false);
                        $.unblockUI();
                        if (response.errors) {
                            var errorHtml = '<ul>';
                            $.each(response.errors, function(field, messages) {
                                $.each(messages, function(index, value) {
                                    errorHtml += '<li style="color: red;">' + value + '</li>';
                                });
                            });
                            errorHtml += '</ul>';

                            // Append error messages to a div with id "errorContainer"
                            $('#editErrorContainer').html(errorHtml);
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#manual_attendance_edit_form')[0].reset();
                            // $(".select2").val('').trigger('change');
                            $("#edit_modal").modal("hide");
                            datatable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Manual Attendance has been Updated Successfully!'
                            })
                        }
                    }
                });
            });

            $('div.head-label').html('<h6 class="mb-0">List of Present Employees</h6>');
        });

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('manualAttendance') }}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    var in_time = new Date(response.in_time);
                    var out_time = new Date(response.out_time);
                    $("#edit_employee").val(response.employee).select2();
                    $("#edit_in_time").val(in_time.getMinutes() > 10 ? `${in_time.getHours()}:${in_time.getMinutes()}` : `${in_time.getHours()}:0${in_time.getMinutes()}`);
                    $("#edit_out_time").val(out_time.getMinutes() > 10 ? `${out_time.getHours()}:${out_time.getMinutes()}` : `${out_time.getHours()}:0${out_time.getMinutes()}`);
                    $("#edit_in_date").val(new Date(response.in_time).toISOString().slice(0, 10));
                    $("#edit_out_date").val(datePart);
                    if(company == 'JSML' && response.od==1){
                        $('#edit_od').prop('checked', true);
                    }else{
                        $('#edit_od').prop('checked', false);
                    }
                    if(company == 'JSML' && response.ret==1){
                        $('#edit_ret').prop('checked', true);
                    }else{
                        $('#edit_ret').prop('checked', false);
                    }
                    if (response.out_time !== null) {
                        var dateTimeString = response.out_time;
                        var parts = dateTimeString.split(' ');
                        var datePart = parts[0];
                        $("#edit_out_date").val(datePart);
                        // $("#edit_out_date").val(new Date(response.out_time).toISOString().slice(0, 10));
                    }
                    $("#edit_note").val(response.note);
                    $("#edit_modal").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "manualAttendance/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        datatable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Manual Attendance has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // Get Employees
        function employeeFun() {
            $('#employeeFilter').removeAttr("disabled");
            var dept_id = $('#departmentFilter').val();
            console.log(dept_id);
            $.ajax({
                // url: "{{ url('manualAttendance') }}" + "/" + dept_id,

                url: "{{ url('get_dept_employees') }}",
                type: 'GET',
                data: {
                        department_id: dept_id,
                    },
                success: function(response) {
                    $('#employeeFilter').html('');
                    var opt = $('<option>');
                    // opt.text('Select Employee');
                    $('#employeeFilter').append(opt);
                    // $.each(response, function(id, curEmp) {
                    //     var opt = $('<option>'); 
                    //     opt.val(curEmp.id);
                    //     opt.text(curEmp.first_name + " " + curEmp.last_name + " - " + curEmp.job_title);
                    //     $('#employeeFilter').append(opt);
                    // });
                    $.each(response, function(index, value) {
                            $('#employeeFilter').append(
                                $('<option></option>').val(value.id).html(
                                    value.employee_id + ' - '+value.employee_code + ' - ' + value.first_name +' '+value.middle_name +' '+ value.last_name +' - '+ value.designation)
                            );
                    });
                }
            });
        }            
        // Filter Function
        $('#search_form').submit(function(e) {
            datatable.draw();
            e.preventDefault();
        });
    </script>
@endsection
