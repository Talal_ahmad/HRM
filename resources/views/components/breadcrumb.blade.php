
<div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a>
                                </li>
                                @foreach(array_filter(explode("/",$crumb)) as $crumb)
                                <li class="breadcrumb-item"><a href="/{{ strtolower(ucfirst(str_replace(array('.php','_'),array('',' '),$crumb) . ' '));}}">{{ ucfirst(str_replace(array(".php","_"),array(""," "),$crumb) . ' ');}}</a>
                                </li>
                                @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
