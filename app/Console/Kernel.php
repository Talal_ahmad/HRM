<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AttendanceHistory::class,
        Commands\IncrementSeasonalEmployeesLeavesBalance::class,
        Commands\JsmlShiftChange::class,
        Commands\ShiftAttendance::class,
        Commands\ShiftEagleEyeAttendance::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if(env('COMPANY') == 'CLINIX'){
            $schedule->command('backup:run --only-db --disable-notifications')->daily()->at('00:30');
            $schedule->command('attendance:history')->monthlyOn(1, '03:00');
            $schedule->command('clinixoldserver:attendance')->everyFiveMinutes();
        }
        else{
            if(env('COMPANY') == 'JSML'){
                $schedule->command('shift:change')->sundays()->at('23:00');
                $schedule->command('make:IncrementSeasonalEmployeesLeavesBalance')->dailyAt('00:10');
            }
            if(env('COMPANY') == 'Prince HRM' || env('COMPANY') == 'CLINIX'){
                $schedule->command('shifteagleeye:attendance')->everyFiveMinutes();
            }

            // Hanvan shift attendance api for JSML,AJMAL and Roofline HRM written in Api.php
            // if(env('COMPANY') == 'Ajmal Dawakhana' || env('COMPANY') == 'JSML'){
            //     $schedule->command('shift:attendance')->everyFiveMinutes();
            // }
            $schedule->command('backup:run --only-db --disable-notifications')->daily();
            $schedule->command('attendance:history')->monthly();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
