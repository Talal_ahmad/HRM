<?php

namespace App\Console\Commands;

use App\Models\ShiftManagement;
use App\Models\ShiftType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class JsmlShiftChange extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shift:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'JSML Shift Change';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Shift task started.');
        ini_set('memory_limit', '-1');
        $rotating_shifts = ShiftType::where([['active', 1], ['is_rotating', 1]])->whereNotNull('rotating_order')->orderBy('rotating_order', 'ASC')->get(['id', 'is_rotating', 'rotating_order', 'shift_start_time', 'shift_end_time']);
        $first_shift = ShiftType::where([['active', 1], ['is_rotating', 1]])->orderBy('rotating_order', 'ASC')->first();
        $last_shift = ShiftType::where([['active', 1], ['is_rotating', 1]])->orderBy('rotating_order', 'DESC')->first();
        foreach ($rotating_shifts as $key => $shift) {
            $employees = ShiftManagement::select('shift_management.*', 'employees.first_name', 'employees.employee_code', 'employees.last_name')->join('employees', 'employees.id', '=', 'shift_management.employee_id')
                ->where([
                    ['shift_management.shift_from_date', '<=', date('Y-m-d')],
                    ['shift_management.shift_to_date', '>=', date('Y-m-d')],
                    ['employees.status', 'Active'],
                    ['shift_management.status', 'Approved'],
                    ['shift_management.shift_id', $shift->id],
                    ['shift_management.shiftChange', 0],
                    ['shift_management.deleted_at', null],
                ])->groupBy('employees.id')->get();
            $next_order = $shift->rotating_order + 2;
            $next_shift = ShiftType::where([['active', 1], ['rotating_order', $next_order]])->first();
            foreach ($employees as $key => $employee) {
                // Check if record exists for the employee on the same start date and end date
                $existingRecord = ShiftManagement::where([
                    ['employee_id', $employee->employee_id],
                    ['shift_from_date', date('Y-m-d', strtotime("+1 day"))],
                    ['shift_to_date', date('Y-m-d', strtotime("+7 day"))],
                    ['status', 'Approved'],
                    ['deleted_at', null],
                ])->exists();
                if (!$existingRecord) {
                    $shiftManagement = new ShiftManagement();
                    $shiftManagement->employee_id = $employee->employee_id;
                    $shiftManagement->work_week_id = $employee->work_week_id;
                    $shiftManagement->shift_from_date = date('Y-m-d', strtotime("+1 day"));
                    $shiftManagement->shift_to_date = date('Y-m-d', strtotime("+7 day"));
                    $shiftManagement->late_time_allowed = $employee->late_time_allowed;
                    $shiftManagement->active = 1;
                    $shiftManagement->status = 'Approved';
                    $shiftManagement->shiftChange = 1;
                    if (!empty($next_shift)) {
                        $shiftManagement->shift_desc = $next_shift->shift_desc;
                        $shiftManagement->shift_id = $next_shift->id;
                    } else {
                        if ($shift->rotating_order == $last_shift->rotating_order) {
                            $rotating_order = $first_shift->rotating_order + 1;
                            $next_shift = ShiftType::where([['active', 1], ['rotating_order', $rotating_order]])->first();
                            $shiftManagement->shift_id = $next_shift->id;
                            $shiftManagement->shift_desc = $next_shift->shift_desc;
                        } else {
                            $shiftManagement->shift_id = $first_shift->id;
                            $shiftManagement->shift_desc = $first_shift->shift_desc;
                        }
                    }
                    $shiftManagement->save();
                }
            }
        }
        DB::table('shift_management')->where('shiftChange', 1)->update(['shiftChange' => 0]);
        $this->info('Shift task ended.');
        return 0;
    }
}
