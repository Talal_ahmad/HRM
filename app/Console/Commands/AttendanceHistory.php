<?php

namespace App\Console\Commands;

use DB;
use App\Models\Attendance;
use Illuminate\Console\Command;

class AttendanceHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendance:history';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer records from attendance to attendance history';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Command task started.');
        ini_set('memory_limit', '-1');
        $date = date('Y-m-d', strtotime(date('Y-m-01'). ' - 3 months'));
        $attendances = Attendance::whereDate('in_time','<',$date)->get(['id','employee','in_time','out_time','note','shift_id','shift_start_time','shift_end_time','late_time_allowed','work_week_id','is_manual','department_id','is_online']);
        foreach ($attendances as $key => $attendance) {
            DB::table('attendance_history')->insert([
                'employee' => $attendance->employee,
                'in_time' => $attendance->in_time,
                'out_time' => $attendance->out_time,
                'note' => $attendance->note,
                'shift_id' => $attendance->shift_id,
                'shift_start_time' => $attendance->shift_start_time,
                'shift_end_time' => $attendance->shift_end_time,
                'late_time_allowed' => $attendance->late_time_allowed,
                'work_week_id' => $attendance->work_week_id,
                'is_manual' => $attendance->is_manual,
                'department_id' => $attendance->department_id,
                'is_online' => $attendance->is_online,
                'distance_in' => $attendance->distance_in,
                'distance_out' => $attendance->distance_out,
                'mac' => $attendance->mac,
            ]);
            $attendance->delete();
        }
        $this->info('Command task ended.');
        return 0;
    }
}
