<?php

namespace App\Console\Commands;

use DB;
use App\Models\Attendance;
use Illuminate\Console\Command;

class ShiftAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shift:attendance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer attendance From one server to other';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Command task started.');
        \Log::info("Job Started");
        ini_set('memory_limit', '-1');
        $curl = curl_init();
        $date = date('Y-m-d', strtotime(' -1 day'));
        if(env('COMPANY') == 'Ajmal Dawakhana'){
            $url = 'http://213.136.91.200:8033/ajmalhrm_moved_to_server_209/public/attendance_report/'.$date.'/edit';
        }
        else{
            $url = 'http://213.136.91.200:8033/jsmlhrm_old/public/attendance_report/'.$date.'/edit';
        }
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $employee_attendances = json_decode($response, true);
        if(count($employee_attendances) > 0){
            foreach($employee_attendances as $attendance_row)
            {
                $date = date('Y-m-d', strtotime($attendance_row['in_time']));
                $check_attendance = Attendance::where('employee', $attendance_row['employee'])->whereDate('in_time', "$date")->where('is_manual',0)->first(['id', 'employee']);
                if(!empty($check_attendance)){
                    $check_out_time = Attendance::where('id', $check_attendance->id)->whereNull('out_time')->first(['id']);
                    if(!empty($check_out_time)){
                        if(!empty($attendance_row['out_time'])){
                            // \Log::debug('attendance_rowId - ',[$attendance_row['id']]);
                            $attendance = Attendance::find($check_out_time->id);
                            $attendance->out_time = $attendance_row['out_time'];
                            $attendance->update();
                        }
                    }
                }
                else{
                    $attendance = new Attendance();
                    $attendance->employee = $attendance_row['employee'];
                    $attendance->in_time = $attendance_row['in_time'];
                    if(!empty($attendance_row['out_time'])){
                        $attendance->out_time = $attendance_row['out_time'];
                    }
                    $attendance->note = $attendance_row['note'];
                    $attendance->shift_id = $attendance_row['shift_id'];
                    $attendance->shift_start_time = $attendance_row['shift_start_time'];
                    $attendance->shift_end_time = $attendance_row['shift_end_time'];
                    $attendance->late_time_allowed = $attendance_row['late_time_allowed'];
                    $attendance->work_week_id = $attendance_row['work_week_id'];
                    $attendance->department_id = $attendance_row['department_id'];
                    $attendance->is_manual = $attendance_row['is_manual'];
                    $attendance->is_online = $attendance_row['is_online'];
                    $attendance->save();
                }
            }
        }
        $this->info('Command task ended.');
        \Log::info("Job Completed");
        return 0;
    }
}
