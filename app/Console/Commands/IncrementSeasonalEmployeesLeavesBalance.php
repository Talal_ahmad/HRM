<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class IncrementSeasonalEmployeesLeavesBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:IncrementSeasonalEmployeesLeavesBalance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will increment Seasonal Employees Casual Leave by 1 after 36 days & Medical Leave by 1 after 45 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            \Log::info("IncrementSeasonalEmployeesLeavesBalance Command Initiated");
            $employees = \App\Models\Employee::where('employment_status', 14)->where('seasonal_status', 'On Roll')->where('status', 'Active')->get();
            foreach ($employees as $employee) {
                $today = now();
                $date = null;
                if (!empty($employee->restore_date) && $employee->restore_date != '0000-00-00') {
                    $date = $employee->restore_date;
                } else {
                    if (!empty($employee->joined_date) && $employee->joined_date != '0000-00-00') {
                        $date = $employee->joined_date;
                    }
                }
                if (!empty($date)) {
                    $today = \Carbon\Carbon::parse('2023-10-01');
                    $dateTimestamp = \Carbon\Carbon::parse('2023-12-11');
                    $daysPassed = $today->diffInDays($dateTimestamp);
                    // For Casual Leave Type After 36 Days
                    $increments = (int) floor($daysPassed / 36);

                    if ($increments > 0) {
                        $employee->leaveTypeBalances()->updateOrCreate([
                            'leave_type' => 2,
                        ], [
                            'leave_type' => 2,
                            'balance' => $increments,
                        ]);
                    }

                    // For Medical Leave Type After 45 Days
                    $increments = (int) floor($daysPassed / 45);

                    if ($increments > 0) {
                        $employee->leaveTypeBalances()->updateOrCreate([
                            'leave_type' => 3,
                        ], [
                            'leave_type' => 3,
                            'balance' => $increments,
                        ]);
                    }
                }
            }
            $this->info("Balances Incremented Successfully!");
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
            \Log::error("IncrementSeasonalEmployeesLeavesBalance Command Error - $th");
        }
    }
}
