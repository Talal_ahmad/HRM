<?php

namespace App\Console\Commands;

use DB;
use App\Models\Employee;
use App\Models\Attendance;
use Illuminate\Console\Command;

class ShiftEagleEyeAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shifteagleeye:attendance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer attendance from eagleeye database to hrm database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Eagle Eye task started.');
        \Log::info("Eagle Eye Job Started");
        ini_set('memory_limit', '-1');
        // $employees = Employee::where('status','Active')->get(['id']);
        if(env('COMPANY') == 'CLINIX'){
            $machines = ['C260C8850B302A37','C260C8850B280838','C26124864B18092C','C260C8850B2B0938','C26124864B2F0F2F','C260C8850B263D37','C261A044132B2C38','C260C8850B2A0838','C261A04413252F38','C261A044132C2C38','C261A04413112838','C261A04413302938','C261A04413342438','C261A044130F2C38','C261A044131B2538','C261A044130E2538','C260C8850B110A38','C261A04413162438','C261A044132D2638','C261A04413262438','C261A044132E2538','C261A04413072438','C261A04413152A38','C261A044130C2A38','C261A04413151439','C261A04413312C38','C261A04413192638','C261A04413202738','C261A04413142037','C261A04413142938','C261A04413072538','C260CCD5271C2E31','C2609020CB0B1428'];
        }
        else {
            $machines = ['C261A04413092137'];
        }
        $attendances = DB::connection('eagleeye')->table('eduportal_limton_emp_attendance')->whereIn('device_number',$machines)->where('hrm_process_status',0)->get();
        foreach ($attendances as $key => $attendance) {
            $employee = Employee::where('id',$attendance->employee_id)->first(['id','department']);
            if(!empty($employee)){
                $date = date('Y-m-d', strtotime($attendance->attendance_date));
                $previous_date = date ("Y-m-d", strtotime("-1 day", strtotime($date)));
                $shift_data = DB::table('shift_management')->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                        ->where([
                            ['shift_management.employee_id' , $employee->id],
                            ['shift_management.shift_from_date', '<=', $date],
                            ['shift_management.shift_to_date', '>=', $date]
                        ])
                        ->select('shift_management.shift_id','shift_management.work_week_id','shift_management.shift_id','shift_type.shift_start_time', 'shift_type.shift_end_time','shift_management.late_time_allowed')
                        ->orderBy('shift_management.id', 'DESC')
                        ->first();
                if(!empty($shift_data))
                {
                    $shift_id = $shift_data->shift_id;
                    $work_week_id = $shift_data->work_week_id;
                    $shift_start_time = $shift_data->shift_start_time;
                    $shift_end_time = $shift_data->shift_end_time;
                    $late_time_allowed = $shift_data->late_time_allowed;
                }
                else
                {
                    $shift_id = 1;
                    $shift_start_time = '09:00:00';
                    $shift_end_time = '17:00:00';
                    $late_time_allowed = 11;
                    $work_week_id = 1;
                }
                if($attendance->status == 1){
                    $check_attendance = Attendance::where('employee', $employee->id)->whereDate('in_time', "$date")->where('is_manual',0)->first(['id', 'employee']);
                    if(empty($check_attendance)){
                        $attend = new Attendance();
                        $attend->employee = $attendance->employee_id;
                        $attend->in_time = $attendance->attendance_date;
                        $attend->note = 0;
                        $attend->shift_id = $shift_id;
                        $attend->shift_start_time = $shift_start_time;
                        $attend->shift_end_time = $shift_end_time;
                        $attend->late_time_allowed = $late_time_allowed;
                        $attend->work_week_id = $work_week_id;
                        $attend->department_id = $employee->department;
                        $attend->is_manual = 0;
                        $attend->is_online = 0;
                        $attend->save();
                        // goto updateEagleEye;
                    }
                    goto updateEagleEye;
                }
                else{
                    $check_out_time = Attendance::where('employee', $employee->id)->whereDate('in_time', "$date")->whereNull('out_time')->first(['id']);
                    if(!empty($check_out_time)){
                        $attend = Attendance::find($check_out_time->id);
                        $attend->out_time = $attendance->attendance_date;
                        $attend->update();
                        goto updateEagleEye;
                    }
                    $check_emp_previous = Attendance::where('employee', $employee->id)->whereDate('in_time', "$previous_date")->whereNull('out_time')->first(['id']);
                    if(!empty($check_emp_previous)){
                        $attend = Attendance::find($check_emp_previous->id);
                        $attend->out_time = $attendance->attendance_date;
                        $attend->update();
                        goto updateEagleEye;
                    }
                }
            }
            else{
                goto updateEagleEye;
            }
            updateEagleEye:
                DB::connection('eagleeye')->table('eduportal_limton_emp_attendance')->where('emp_attnd_id',$attendance->emp_attnd_id)->update(['hrm_process_status' => 1]);
        }
        $this->info('Eagle Eye task ended.');
        \Log::info("Eagle Eye Job Completed");
        return 0;
    }
}
