<?php

namespace App\Console\Commands;

use DB;
use App\Models\Attendance;
use Illuminate\Console\Command;

class ShiftClinixAttendanceFromOldServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clinixoldserver:attendance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shift attendance from old server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Old server task started.');
        \Log::info("Old server job started");
        $attendances = DB::connection('clinix_old_server')->table('attendance')->where('process_status',0)->get();
        foreach ($attendances as $key => $attendanceRec) {
            $date = date('Y-m-d', strtotime($attendanceRec->in_time));
            $previous_date = date ("Y-m-d", strtotime("-1 day", strtotime($date)));
            $checkAttendance = Attendance::where('employee', $attendanceRec->employee)->whereDate('in_time', $date)->orderBy('id', 'DESC')->first();
            if (empty($checkAttendance)) {
                $attendance = new Attendance;
                $attendance->employee = $attendanceRec->employee;
                $attendance->in_time = $attendanceRec->in_time;
                $attendance->out_time  = $attendanceRec->out_time;
                $attendance->note = $attendanceRec->note;
                $attendance->shift_id = $attendanceRec->shift_id;
                $attendance->shift_start_time = $attendanceRec->shift_start_time;
                $attendance->shift_end_time = $attendanceRec->shift_end_time;
                $attendance->late_time_allowed = $attendanceRec->late_time_allowed;
                $attendance->work_week_id = $attendanceRec->work_week_id;
                $attendance->department_id = $attendanceRec->department_id;
                $attendance->is_online = $attendanceRec->is_online;
                $attendance->distance_in = $attendanceRec->distance_in;
                $attendance->distance_out = $attendanceRec->distance_out;
                $attendance->thumb_matched_insert = $attendanceRec->thumb_matched_insert;
                $attendance->save();
                goto updateOldServer;
            }
            else {
                $check_out_time = Attendance::where('employee', $attendanceRec->employee)->whereDate('in_time', $date)->whereNull('out_time')->first(['id']);
                if(!empty($check_out_time)){
                    $attend = Attendance::find($check_out_time->id);
                    $attend->out_time = $attendanceRec->out_time;
                    $attend->distance_out = $attendanceRec->distance_out;
                    $attend->update();
                    goto updateOldServer;
                }
                $check_emp_previous = Attendance::where('employee', $attendanceRec->employee)->whereDate('in_time', $previous_date)->whereNull('out_time')->first(['id']);
                if(!empty($check_emp_previous)){
                    $attend = Attendance::find($check_emp_previous->id);
                    $attend->out_time = $attendanceRec->out_time;
                    $attend->distance_out = $attendanceRec->distance_out;
                    $attend->update();
                    goto updateOldServer;
                }
            }
            updateOldServer:
                DB::connection('clinix_old_server')->table('attendance')->where('id',$attendanceRec->id)->update(['process_status' => 1]);
            // DB::connection('clinix_old_server')->table('attendance')->where('id',$attendanceRec->id)->WhereNotNull('out_time')->update(['process_status' => 1]);
            // DB::connection('clinix_old_server')->table('attendance')->whereDate('in_time','<=',date('Y-m-d', strtotime(' -2 day')))->WhereNull('out_time')->update(['process_status' => 1]);
        }
        $this->info('Old server task started.');
        \Log::info("Old server job started");
        return 0;
    }
}
