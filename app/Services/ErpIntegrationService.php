<?php

namespace App\Services;

use App\Models\GlSetting;
use App\Models\Ledger;
use Carbon\Carbon;

class ErpIntegrationService
{
    public static function getErpAddress()
    {
        $app_env = env('APP_ENV');
        if ($app_env == 'production') {
            $address = env('ERP_ADDRESS');
        } else {
            $address = env('ERP_TEST_ADDRESS');
        }
        return $address;
    }

    public static function ProcessPayrollAccrualJvToErp($payroll)
    {
        try {
            if (!empty($payroll->erp_address)) {
                $address = $payroll->erp_address;
            } else {
                $address = self::getErpAddress();
            }
            if (!empty($address)) {
                $address = $address . "create_hrm_accrual_entry";
                $month_year = Carbon::parse($payroll->date_start)->format('F') . ' ' . Carbon::parse($payroll->date_start)->format('Y');
                $period = Carbon::parse($payroll->date_start)->format('d F') . ' to ' . Carbon::parse($payroll->date_end)->format('d F');
                $accounts_sum = Ledger::where('payroll_id', $payroll->id)
                    ->groupBy('gl_account')
                    ->groupBy('gl_account_type')
                    ->groupBy('column_id')
                    ->select('gl_account_type', 'gl_account', \DB::raw('SUM(amount) as total_amount'))
                    ->get();
                $chart_master = [];
                $gl_debit = [];
                $gl_credit = [];
                $debit_sum = 0;
                $credit_sum = 0;
                foreach ($accounts_sum as $account) {
                    array_push($chart_master, $account->gl_account);
                    if ($account->gl_account_type == 'credit') {
                        array_push($gl_credit, abs($account->total_amount));
                        array_push($gl_debit, '');
                        $credit_sum += abs($account->total_amount);
                    } else {
                        array_push($gl_debit, $account->total_amount);
                        array_push($gl_credit, '');
                        $debit_sum += $account->total_amount;
                    }
                }
                if (($debit_sum == 0)) {
                    throw new \Exception('Debit Is Not a Valid Amount(Zero)!');
                }
                if ($credit_sum == 0) {
                    throw new \Exception('Credit Is Not a Valid Amount(Zero)!');
                }
                if (($debit_sum != $credit_sum)) {
                    throw new \Exception('Credit & Debit Is Not Balanced!');
                }
                $post_fields = array(
                    'main_memo' => "Payroll ($payroll->name) Accrual for the Month of {$month_year} (Period from {$period})",
                    'gl_memo' => "Payroll ($payroll->name) Accrual for the Month of {$month_year} (Period from {$period})",
                    'journal_date' => $payroll->date_end,
                    'document_date' => date('Y-m-d'),
                    'event_date' => date('Y-m-d'),
                    'currency' => 'PKR',
                    'source_ref' => '',
                    'dt' => date('Y-m-d'),
                    'reference' => '',
                    'referenceOrg' => '',
                    'chart_masters' => $chart_master,
                    'gl_debit' => $gl_debit,
                    'gl_credit' => $gl_credit,
                );
                if (empty($payroll->erp_trans_no) && empty($payroll->erp_reference)) {
                    $post_fields['edit_je_id'] = 0;
                    $post_fields['reference'] = 0;
                } else {
                    $post_fields['edit_je_id'] = $payroll->erp_trans_no;
                    $post_fields['reference'] = $payroll->erp_reference;
                }
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $address,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => http_build_query($post_fields),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                $data = json_decode($response);
                if (empty($data)) {
                    throw new \Exception('Error on Accrual Entry to Erp!');
                }
                if ($data->status != 'sucess') {
                    throw new \Exception($data->message);
                }
                $payroll->update([
                    'erp_trans_no' => $data->trans_no,
                    'erp_reference' => $data->reference,
                ]);
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            throw new \Exception("Error - $message");
        }
    }

    public static function voidPayrollAccrualJv($payroll)
    {
        try {
            if (!empty($payroll->erp_address)) {
                $address = $payroll->erp_address;
            } else {
                $address = self::getErpAddress();
            }
            if (!empty($address)) {
                $address = $address . "void_hrml_accrual_entry";
                $post_fields = [
                    "trans_no" => $payroll->erp_trans_no,
                    "user_id_hrm" => auth()->user()->id,
                ];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $address,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $post_fields,
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                $data = json_decode($response);
                if (empty($data)) {
                    throw new \Exception('Error on Voiding Accrual JV Entry from Erp!');
                }
                if ($data->status != 'sucess') {
                    throw new \Exception($data->message);
                }
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            throw new \Exception("Error - $message");
        }
    }

    public static function accrualVoucherToErp($request, $employees)
    {
        $address = self::getErpAddress() . "create_hrm_accual_payment_entry";
        $total_amount = 0;
        $chart_master = [];
        $gl_amount = [];
        foreach ($employees as $employee) {
            $total_amount += $employee->allocation;
            array_push($chart_master, $employee->gl_account);
            array_push($gl_amount, $employee->allocation);
        }
        $post_fields = [
            "voucher_no" => $request->voucher,
            "payment_date" => date('Y-m-d'),
            "payment_to" => 1,
            "person_id" => 'Payroll Payment',
            "check_date" => date('Y-m-d'),
            "bank_account" => $employees[0]->payment_banks_id,
            "dimension2" => 1,
            "chart_masters" => $chart_master,
            "gl_amount" => $gl_amount,
            "gl_memo" => $employees[0]->payment_voucher_desc ?? "Test Memo",
            "main_memo" => $employees[0]->payment_voucher_desc ?? "Test Memo",
            "accountType" => 2,
            "dt" => date('Y-m-d'),
            "total_amount" => $total_amount,
        ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $address,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($post_fields),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response);
        if (empty($data)) {
            throw new \Exception('Error on Sending Accrual Voucher to Erp!');
        }
        if ($data->status != 'sucess') {
            throw new \Exception('Invalid Response From Erp!');
        }
        Ledger::where('payment_voucher_no', $request->voucher)->get()->each->update([
            'payment_trans_no' => $data->trans_no,
            'payment_reference_no' => $data->reference,
        ]);
        return true;
    }

    public static function SalaryLoanAdvancePaymentEntry($request, $ledger, $loan_payment)
    {
        $address = self::getErpAddress() . "create_hrm_salry_loan_advance_payment_entry";
        if ($request->loan_type == 'Loan') {
            $gl_memo = "Loan Paid to Employee: $request->employee_code, - $request->employee \nRemarks: $request->remarks";
        } else {
            $gl_memo = "Advance Paid to Employee: $request->employee_code, - $request->employee \nRemarks: $request->remarks";
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $address,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'process_now' => '0',
                'payment_date' => $request->payment_date,
                'payment_to' => '1',
                'person_id' => $request->employee,
                'supplier' => '',
                'bank_account' => $loan_payment->companyBank->erp_bank_id,
                'cheque_books' => !empty($request->cheque_book) ? $request->cheque_book : null,
                'check_no' => !empty($request->cheque_no) ? $request->cheque_no : null,
                'check_date' => !empty($request->checque_date) ? $request->checque_date : null,
                'chart_masters[0]' => $ledger->gl_account,
                'items[0]' => '',
                'gl_amount[0]' => $request->loan_amount,
                'gl_memo[0]' => $gl_memo,
                'main_memo' => $request->payment_method == 'Bank' ? "Paid Cheque # $request->cheque_no from Bank: $request->bank_name to Employee: $request->employee_code - $request->employee" : $gl_memo,
                'accountType' => $request->payment_method == 'Bank' ? 3 : 4,
                'dt' => date('Y-m-d'),
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response);
        if (empty($data)) {
            throw new \Exception('Error on Sending Loan to Erp!');
        }
        if ($data->status != 'sucess') {
            throw new \Exception('Invalid Response From Erp!');
        }
        $loan_payment->update([
            'erp_trans_no' => $data->trans_no,
            'erp_reference' => $data->reference,
        ]);
        return true;
    }

    public static function createFairPriceAdvanceJvEntry($request, $ledger, $loan_payment)
    {
        try {
            $address = self::getErpAddress() . "create_fair_price_advance_jv_entry";
            $gl_memo = "Fair Price Entry of {$ledger->employee} \n $request->remarks";
            $post_fields = [
                "payment_date" => $request->payment_date,
                "payment_to" => $loan_payment->fair_price_customer_id,
                "person_id" => "Fair Price",
                "check_date" => $loan_payment->cheque_date,
                'bank_account' => $loan_payment->companyBank->erp_bank_id,
                "dimension2" => 1,
                "chart_masters" => [
                    0 => $ledger->gl_account,
                ],
                "gl_amount" => [
                    0 => $loan_payment->loan_amount
                ],
                "gl_memo" => $gl_memo,
                "main_memo" => $gl_memo,
                "accountType" => 2,
                "edit_je_id" => 0,
                "dt" => date('Y-m-d'),
                "journal_date" => date('Y-m-d'),
                "total_amount" => $loan_payment->loan_amount,
            ];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $address,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => http_build_query($post_fields),
            ));
            $response = curl_exec($curl);
            dd($response);
            curl_close($curl);
            $data = json_decode($response);
            if (empty($data)) {
                throw new \Exception('Error on Sending Fair Price Entry to Erp!');
            }
            if ($data->status != 'sucess') {
                throw new \Exception('Invalid Response From Erp!');
            }
            $loan_payment->update([
                'erp_trans_no' => $data->trans_no,
                'erp_reference' => $data->reference,
            ]);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            throw new \Exception("Error - $message");
        }
    }
}
