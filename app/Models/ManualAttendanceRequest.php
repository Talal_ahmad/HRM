<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManualAttendanceRequest extends Model
{
    use HasFactory;
    protected $table = 'manual_attendance_request'; 
    protected $guarded = [];
}
