<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class JobTitle extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'jobtitles';
    protected $guarded = [];

    public static function InsertImage($file){
        $fileOrignalName = $file->getClientOriginalName();
        $file_path = '/jobtitles';
        $path = public_path() . $file_path;
        $filename = time().'_'.rand(000 ,999).'.'.$file->getClientOriginalExtension();
        $file->move($path, $filename);
        $path = $file_path.'/'.$filename;
        return $path;
    }
}
