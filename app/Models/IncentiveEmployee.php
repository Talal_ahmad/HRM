<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IncentiveEmployee extends Model
{
    use HasFactory;
    protected $table = 'incentive_employees';
    protected $guarded = [];
}
