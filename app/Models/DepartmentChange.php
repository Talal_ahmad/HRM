<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentChange extends Model
{
    use HasFactory;
    protected $table = 'department_requests';
    protected $guarded = [];
}
