<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeesHistory extends Model
{
    use HasFactory;
    protected $table = 'employees_history';
    protected $guarded = [];
}
