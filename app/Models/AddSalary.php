<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddSalary extends Model
{
    use HasFactory;
    protected $table = 'employeesalary_pendings';
    protected $guarded = ['type'];
}
