<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EmployeeForm extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'employeeforms';
    protected $guarded = ['type'];
}
