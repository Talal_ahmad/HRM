<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShiftManagement extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable, SoftDeletes;
    protected $table = 'shift_management';
    protected $guarded = ['type'];

    public function workWeek()
    {
        return $this->belongsTo(WorkWeek::class);
    }
}
