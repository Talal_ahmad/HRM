<?php

namespace App\Models;

use App\Models\ManualAttendance;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Employee extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;
    protected $table = 'employees';
    protected $guarded = [];

    // protected $appends = ['companystructure', 'employmentstatus'];

    public function companystructure()
    {
        return $this->belongsTo(CompanyStructure::class, 'department');
    }

    public function jobtitle()
    {
        return $this->belongsTo(JobTitle::class, 'job_title');
    }

    public static function InsertFile($file,$path){
        $fileOrignalName = $file->getClientOriginalName();
        $file_path = $path;
        $path = public_path() . $file_path;
        $filename = time().'_'.rand(000 ,999).'.'.$file->getClientOriginalExtension();
        $file->move($path, $filename);
        return $filename;
    }

    public function manualattendance()
    {
        return $this->hasMany(ManualAttendance::class,'employee', 'id');
    }

    public function attendance()
    {
        return $this->hasMany(Attendance::class, 'employee', 'id');
    }

    public function newEmployeesLeavesSettings()
    {
        return $this->hasMany(NewEmployeesLeavesSetting::class, 'employee_id', 'id');
    }

    public function calculationGroups()
    {
        return $this->hasMany(PayrollEmployee::class, 'employee', 'id');
    }

    public function leaveGroups()
    {
        return $this->belongsToMany(LeaveGroup::class, 'leavegroupemployees', 'employee', 'leave_group');
    }

    public function leaveTypeBalances()
    {
        return $this->hasMany(EmployeeLeaveBalance::class, 'emp_id', 'id');
    }

    public function shortLeaves()
    {
        return $this->hasMany(ShortLeaveRequest::class);
    }

    public function shiftManagements()
    {
        return $this->hasMany(ShiftManagement::class);
    }

    // public function employmentstatus()
    // {
    //     return $this->belongsTo(EmploymentStatus::class);
    // }

    // public function getCompanyStructureAttribute()
    // {
    //     if($this->companystructure()->exists()){
	// 		return $this->companystructure()->first()->title;
	// 	}else{
	// 		return '';
	// 	}
    // }

    // public function getEmploymentStatusAttribute()
    // {
    //     if($this->employmentstatus()->exists()){
	// 		return $this->employmentstatus()->first()->name;
	// 	}else{
	// 		return '';
	// 	}
    // }
}
