<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class EmployeeDisciplinaryAction extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable; 
    protected $table = 'disciplinary_inquiry'; 
    protected $guarded = [];

    public static function petitionInsert($image){
        $temp_name = rand(0 , 999). time(). "." . $image->getClientOriginalExtension();
        $image->move(public_path('images/disciplinary/') , $temp_name);
        return $temp_name;
    }
    public static function statementInsert($image){
        $temp_name = rand(0 , 999). time(). "." . $image->getClientOriginalExtension();
        $image->move(public_path('images/disciplinary/') , $temp_name);
        return $temp_name;
    }
    public static function petitionUpdate($oldImage , $image){
        unlink(public_path('images/disciplinary/'.$oldImage));
        $temp_name = rand(0 , 999). time() . "." . $image->getClientOriginalExtension();
        $image->move(public_path('images/disciplinary/') , $temp_name);
        return $temp_name;
    }
    public static function statementUpdate($oldImage , $image){
        unlink(public_path('images/disciplinary/'.$oldImage));
        $temp_name = rand(0 , 999). time() . "." . $image->getClientOriginalExtension();
        $image->move(public_path('images/disciplinary/') , $temp_name);
        return $temp_name;
    }
}
