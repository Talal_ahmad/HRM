<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class CompanyBank extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;
    protected $table = 'company_banks';
    protected $fillable = [
        'account_name',
        'type',
        'erp_bank_id',
        'bank_name',
        'erp_gl_account_id',
        'gl_account_number',
        'bank_phone',
        'bank_address',
        'status',
    ]; 
}
