<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreviousPayroll extends Model
{
    use HasFactory;
    protected $table = 'prev_payrolls';
    protected $guarded = [];
}
