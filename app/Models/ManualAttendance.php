<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Employee;


class ManualAttendance extends Model implements Auditable
{
    use HasFactory,\OwenIt\Auditing\Auditable;
    protected $table = 'attendance'; 
    protected $guarded = [];

    // public function getActivitylogOptions(): LogOptions
    // {
    //     $login_user = \Auth::user()->username;
    //     return LogOptions::defaults()
    //     ->logOnly(['is_manual', 'employee.first_name', 'department_id', 'in_time', 'out_time','shift_id', 'shift_start_time','shift_end_time','work_week_id', 'late_time_allowed'])
    //     ->useLogName('manual_attendance')
    //     ->logOnlyDirty()
    //     ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Manual Attendance");
    // }

    public function employee()
    {
        return $this->belongsTo(Employee::class,'employee');
    }
}
