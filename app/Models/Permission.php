<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function permissionTitle()
    {
        return $this->belongsTo(PermissionTitle::class, 'permission_titles_id');
    }
}
