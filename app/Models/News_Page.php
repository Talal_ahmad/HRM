<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class News_Page extends Model
{
    use HasFactory;
    protected $table='news_pages';
    protected $guarded=[];
    public function users(){
        return $this->belongsTo(User::class ,'user_id','id');

    }
}
