<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class InsuranceAllocation extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'insurance_allocation'; 
    protected $guarded = [];

    public static function Attachment($file){
        $fileOrignalName = $file->getClientOriginalName();
        $file_path = '/insurance_allocation';
        $path = public_path() . $file_path;
        $filename = time().'_'.rand(000 ,999).'.'.$file->getClientOriginalExtension();
        $file->move($path, $filename);
        $path = $file_path.'/'.$filename;
        return $path;
    }
}
