<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class LoanRequest extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;
    protected $table = 'loan_requests';
    protected $guarded = ['type'];

    public function loanType()
    {
        return $this->belongsTo(LoanType::class, 'loan_type', 'id');
    }
}
