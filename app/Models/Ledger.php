<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Ledger extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'gl_setting_id',
        'payroll_id',
        'name',
        'employee_id',
        'employee',
        'designation_id',
        'designation',
        'department_id',
        'department',
        'employee_bank',
        'account_title',
        'account_number',
        'type',
        'column_id',
        'column_name',
        'gl_account_type',
        'gl_account',
        'payment_banks_id',
        'payment_banks',
        'net_payable',
        'amount',
        'balance',
        'payroll_duration',
        'user_id',
        'trans_type',
        'trans_date',
        'loan_payment_detail_id',
        'description',
        'employement_status_id',
        'employee_code',
        'employee_joining_date',
        'employee_eobi_status',
        'employee_pessi_status',
        'employee_provident_fund_status',
        'found_status',
        'accrual_date',
        'payment_date',
        'setting_type',
        'allocation',
        'left_to_allocate',
        'voucher_date',
        'payment_voucher_no',
        'payment_trans_no',
        'payment_reference_no',
        'entry_of_month',
        'payment_voucher_desc',
    ];
}