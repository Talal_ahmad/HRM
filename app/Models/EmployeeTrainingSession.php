<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class EmployeeTrainingSession extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'employeetrainingsessions';
    protected $guarded = ['type'];
    protected $fillable = ['employee', 'training_sessions', 'status'];

}
