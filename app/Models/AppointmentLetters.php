<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppointmentLetters extends Model
{
    use HasFactory;protected 
    $table = 'appointment_letter_records';
    protected $guarded = [];
}
