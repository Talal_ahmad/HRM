<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class WorkWeek extends Model
{
    use HasFactory;
    // use SoftDeletes;
    protected $table = "work_week";
    protected $guarded = ['type'];

    public function days()
    {
        return $this->hasMany(WorkDay::class, 'work_week_id', 'id');
    }
}
 