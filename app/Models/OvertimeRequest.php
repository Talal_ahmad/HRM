<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class OvertimeRequest extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'employeeovertime';
    protected $guarded = ['type'];

    public static function InsertImage($image){
        $fileOrignalName = $image->getClientOriginalName();
        $image_path = '/employee_expenses';
        $path = public_path() . $image_path;
        $filename = time().'_'.rand(000 ,999).'.'.$image->getClientOriginalExtension();
        $image->move($path, $filename);
        $paths = $image_path.'/'.$filename;
        return $paths;
    }
}
