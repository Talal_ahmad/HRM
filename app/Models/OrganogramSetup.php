<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class OrganogramSetup extends Model
{
    use HasFactory;
    // use SoftDeletes;
    protected $table = 'organogram_setup';
    protected $guarded = [];
}
