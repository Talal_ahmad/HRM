<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FineReversal extends Model
{
    use HasFactory;

    protected $table = 'fine_reversal_requests';
    protected $guarded=[];
}
