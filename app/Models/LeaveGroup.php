<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class LeaveGroup extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'leavegroups';
    protected $guarded = ['type'];

    public function leaveTypes()
    {
        return $this->hasMany(LeaveTypes::class, 'leave_group');
    }
}
