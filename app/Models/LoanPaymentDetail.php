<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanPaymentDetail extends Model
{
    use HasFactory;
    protected $table = 'loan_payment_details';
    protected $guarded = [];

    public function companyBank()
    {
        return $this->belongsTo(CompanyBank::class);
    }
}
