<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class PayrollEmployee extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;
    protected $table = 'payrollemployees';
    protected $guarded = [];

    protected $appends = [
        'created_by'
    ];

    public function getCreatedByAttribute()
    {
        $audit = $this->audits()->where('old_values', '[]')->latest()->first();

        if ($audit) {
            return optional($audit->user)->username;
        }

        return null;
    }
}
