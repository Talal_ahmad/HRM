<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class HolidaySetting extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;
    protected $guarded = [];
    
    public function getDepartmentsAttribute($value)
    {
        if(!empty($value)){
            return json_decode($value, true);
        }
        else{
            return [];
        }
    }

    public function getSectionsAttribute($value)
    {
        if(!empty($value)){
            return json_decode($value, true);
        }
        else{
            return [];
        }
    }

    public function getEmployementStatusesAttribute($value)
    {
        if(!empty($value)){
            return json_decode($value, true);
        }
        else{
            return [];
        }
    }

    public function getHolidaysAttribute($value)
    {
        if(!empty($value)){
            return json_decode($value, true);
        }
        else{
            return [];
        }
    }
}
