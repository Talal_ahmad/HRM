<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnBoarding extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'on_boarding';
    protected $guarded=['type'];
    public static function InsertFile($file,$path){
        if ($file) {
            $fileOrignalName = $file->getClientOriginalName();
            $file_path = $path;
            $path = public_path() . $file_path;
            $filename = time().'_'.rand(000 ,999).'.'.$file->getClientOriginalExtension();
            $file->move($path, $filename);
            return $filename;
        }else{
            return null;
        }
    }
}

