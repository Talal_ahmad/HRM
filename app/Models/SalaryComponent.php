<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class SalaryComponent extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'salarycomponent';
    protected $guarded = ['type'];

    public function tax()
    {
        return $this->belongsTo(Tax::class);
    }
}
