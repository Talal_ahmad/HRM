<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MonthlyKPI extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'monthly_kpi';  
    protected $guarded = []; 
}
