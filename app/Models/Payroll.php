<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Payroll extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'payroll';
    
    protected $guarded = ['type']; 

    public function employeeColumns()
    {
        return $this->hasMany(payrollEmployeeColumn::class, 'payroll_id', 'id');
    }
}
