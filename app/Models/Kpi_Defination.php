<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kpi_Defination extends Model
{
    use HasFactory;
    protected $table = 'kpi_definition';
    protected $guarded = [];
}
