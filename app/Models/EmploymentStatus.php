<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class EmploymentStatus extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'employmentstatus';
    protected $guarded = ['type'];

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
