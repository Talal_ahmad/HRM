<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'country';
    protected $guarded = ['type'];

    public function companystructure()
    {
        return $this->hasMany(CompanyStructure::class);
    }
}
