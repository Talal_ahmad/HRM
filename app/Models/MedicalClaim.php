<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalClaim extends Model
{
    use HasFactory;
    protected $table = 'medical_claims';
    protected $guarded = [];

    public static function Attachment($file){
        $fileOrignalName = $file->getClientOriginalName();
        $file_path = '/medical_claim';
        $path = public_path() . $file_path;
        $filename = time().'_'.rand(000 ,999).'.'.$file->getClientOriginalExtension();
        $file->move($path, $filename);
        $path = $file_path.'/'.$filename;
        return $path;
    }
}
