<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeExpense extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'employeeexpenses';
    protected $guarded = ['type'];

    public static function InsertImage($file){
        $fileOrignalName = $file->getClientOriginalName();
        $file_path = '/employee_expense';
        $path = public_path() . $file_path;
        $filename = time().'_'.rand(000 ,999).'.'.$file->getClientOriginalExtension();
        $file->move($path, $filename);
        $path = $file_path.'/'.$filename;
        return $path;
    }
}
