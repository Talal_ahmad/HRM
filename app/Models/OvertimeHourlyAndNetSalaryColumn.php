<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OvertimeHourlyAndNetSalaryColumn extends Model
{
    use HasFactory;
    protected $table = 'overtime_hourly_paycolumn';  
    protected $guarded = []; 
}
