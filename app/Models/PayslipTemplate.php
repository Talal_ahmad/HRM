<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayslipTemplate extends Model
{
    use HasFactory;
    protected $table = 'new_paysliptemplates';
    protected $guarded = ['type'];
}
