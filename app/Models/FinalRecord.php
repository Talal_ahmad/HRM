<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinalRecord extends Model
{
    use HasFactory;
    protected $table = 'final_settlement_records';
    protected $guarded = [];
}
