<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Legal extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'legals';
    // protected $fillable = [
    //     'legal_action',
    //     'title',
    //     'legal_counsel_company',
    //     'employee_id',
    //     'legal_counsel_employee',
    //     'case_filed_on',
    //     'court',
    //     'case_type',
    //     'petition_case_copy',
    // ];
    protected $guarded = [];
}
