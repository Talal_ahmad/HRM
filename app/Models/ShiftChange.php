<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ShiftChange extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'shift_management';
    protected $guarded = [];

    public function shiftType(){
        return $this->belongsTo(ShiftType::class, 'shift_id', 'id');
    }

    public function employee(){
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }
}
