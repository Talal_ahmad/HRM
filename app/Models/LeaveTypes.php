<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class LeaveTypes extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;
    protected $table = 'leavetypes';
    protected $guarded = ['type'];

    public function newEmployeesLeavesSettings()
    {
        return $this->hasMany(NewEmployeesLeavesSetting::class, 'leave_type_id', 'id');
    }
}
