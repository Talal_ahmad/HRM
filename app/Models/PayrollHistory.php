<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayrollHistory extends Model
{
    use HasFactory;
    protected $table = 'payroll_history';
    protected $guarded = []; 
}

