<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class GlSetting extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;
    
    protected $table = 'gl_settings';

    protected $fillable = [
        'employment_status_id',
        'department_id',
        'payroll_column_id',
        'column_type',
        'category',
        'gl_account_id',
        'gl_accounts',
        'loan_advance',
        'bank_accounts_id',
        'bank_accounts',
        'type',
        'locked',
    ];
    
    public function getEmploymentStatusIdAttribute($value)
    {
        return json_decode($value, true);
    }

    public function getDepartmentIdAttribute($value)
    {
        return json_decode($value, true);
    }

    public function getPayrollColumnIdAttribute($value)
    {
        if(!empty($value)){
            return json_decode($value, true);
        }
        else{
            return [];
        }
    }

    public function getGlAccountIdAttribute($value)
    {
        if(!empty($value)){
            return json_decode($value, true);
        }
        else{
            return [];
        }
    }

    public function getBankAccountsIdAttribute($value)
    {
        return $value ? json_decode($value, true) : [];
    }

    public function getDepartmentIdStringAttribute($value)
    {
        return implode(',', $this->department_id);
    }

    public function getEmploymentStatusIdStringAttribute($value)
    {
        return implode(',', $this->employment_status_id);
    }

    public function getPayrollColumnIdStringAttribute($value)
    {
        return implode(',', $this->payroll_column_id);
    }
}
