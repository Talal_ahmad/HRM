<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinalSettlementSetup extends Model
{
    use HasFactory;protected
    $table = 'final_settlement_setup';
    protected $guarded = [];
}
