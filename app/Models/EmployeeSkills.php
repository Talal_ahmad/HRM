<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class EmployeeSkills extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'employeeskills';
    protected $guarded = ['qualification_info_skill'];
}
