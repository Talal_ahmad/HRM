<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MiscellaneousRequestType extends Model
{
    use HasFactory;

    protected $table = 'miscellaneous_request_type';
    protected $fillable = ['name','description'];
}
