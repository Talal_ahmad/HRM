<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IssueBelonging extends Model
{
    use HasFactory;
    protected $table = 'issue_belongings'; 
    protected $guarded = [];
}
