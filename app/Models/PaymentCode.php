<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PaymentCode extends Model
{
    use HasFactory;
    use SoftDeletes; 
    protected $table = 'payment_codes';
    protected $guarded = [];
}
