<?php

use Carbon\Carbon;
use App\Models\Employee;
use App\Models\JobTitle;
use App\Models\LeaveTypes;
use App\Models\Attendance;
use App\Models\GlAccount;
use App\Models\LeavePeriods;
use App\Models\CompanyStructure;
use App\Models\ManualAttendance;
use Illuminate\Support\Facades\DB;
use App\Models\EmployeeLeaveBalance;
use App\Models\EmployeeLeaveRequest;
use App\Models\LeaveTypeSetting;
use App\Models\User;
use App\Models\LeaveGroup;
use App\Models\HolidaySetting;
use App\Models\ShiftType;
use App\Models\EmploymentStatus;
use App\Models\Payroll;
use App\Models\PayrollColumn;
use App\Models\PayrollEmployee;
use Illuminate\Support\Facades\Auth;

if(!function_exists('getErpCompanies')){
    function getErpCompanies(){
        return ['JSML', 'ROOFLINE'];
    }
}

if(!function_exists('getErpCompanyAddress')){
    function getErpCompanyAddress(){
        if(env('COMPANY') == 'JSML'){
            return 'http://173.212.206.138/jsmlerp/api/';
        }
        if(env('COMPANY') == 'ROOFLINE'){
            return 'http://194.163.145.90/rooflineerp/api/';
        }
    }
}

if(!function_exists('login_user_departments')){
    function login_user_departments(){

        if(Auth::user()->hasRole('Admin'))
        {
            $user_departments = CompanyStructure::pluck('id')->toArray();
        }
        else{
            $user_departments = json_decode(Auth::user()->user_departments);
            if(empty($user_departments)){
                return [];
            }
        }
        return $user_departments;
    }
}

if(!function_exists('employees')){
    function employees($department_id = '',$module='', $allEmployees = false){
        $employees = Employee::leftjoin('companystructures', 'companystructures.id', '=', 'employees.department')
        ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
        ->whereIn('employees.department', login_user_departments());
        if (userRoles()=="Self Service") {
            $employees->where('employees.id' , Auth::user()->employee);
        }
        if(env('COMPANY') == 'CLINIX'){
            if($module == 'loanReport' || $module == 'loanRequest'){
                $employees->whereIn('employees.status', ['Active','Terminated']);
            }
            else{
                $employees->where('employees.status', 'Active');
            }
        }
        else{
            if (env('COMPANY')=='HEALTHWISE') {
                $employees->whereIn('employees.status', ['Active','Terminated'])->whereIn('employees.suspended', [0,1])->whereIn('employees.seasonal_status' , ['On Roll', 'Off Roll']);
            }else{
                $employees->where('employees.suspended' , 0)->where('employees.status', 'Active');
            }
        }
        if(env('COMPANY') == 'JSML'){
            if($allEmployees){
                $employees->whereIn('employees.status', ['Active','Terminated'])->whereIn('employees.suspended', [0,1])->whereIn('employees.seasonal_status' , ['On Roll', 'Off Roll']);
            }
            else{
                $employees->where('employees.seasonal_status' , 'On Roll');
            }
        }
        if(!empty($department_id))
        {
            $employees->where('employees.department', $department_id);
        }
        $data = $employees->get(['employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name' , 'employees.last_name','jobtitles.name as desigination', 'companystructures.title as department']);
        return $data;
    }
}
if(!function_exists('getbadge')){
    function getbadge($badgetype)
    {
        if ($badgetype == "Pending") {
            $badge = 'badge bg-light-info';
            return $badge;
        }
        elseif ($badgetype == "Approved") {
            $badge = 'badge bg-light-success';
            return $badge;

        }
        elseif ($badgetype == "Cancelled") {
            $badge = 'badge bg-light-danger';
            return $badge;

        }
        elseif ($badgetype == "Rejected") {
            $badge = 'badge bg-light-warning';
            return $badge;

        }else {
            # code...
        }
    }
}


if(!function_exists('departments')){
    function departments($department_id = ''){
        if(!empty($department_id)){
            $departments = CompanyStructure::where('id',$department_id)->first(['title']);
        }
        else{
            $departments = CompanyStructure::whereIn('id',login_user_departments())->get(['id', 'title']);
        }
        return $departments;
    }
}

if(!function_exists('designation')){
    function designation(){
        $designation = JobTitle::get(['id' , 'name']);
        return $designation;
    }
}

if(!function_exists('payrollColumnAmount')){
    function payrollColumnAmount($payroll_id,$employee_id,$column_id,$is_decimal='',$round_off='',$status=''){
        if(env('COMPANY') == 'CLINIX'){
            $column_amount = \DB::table('payrolldata')->where('payroll', $payroll_id)->where('employee', $employee_id)->where('payroll_item', $column_id)->first(['amount']);
        }else{
            $column_amount = \DB::table('payrolldata')->whereIn('payroll', $payroll_id)->where('employee', $employee_id)->where('payroll_item', $column_id);
            if(!empty($status)){
                $column_amount->whereIn('payrolldata.employment_status_id',$status);
            }
            $column_amount = $column_amount->first([DB::raw('SUM(amount) as amount')]);
        }
        if(!empty($column_amount)){
            if(is_numeric($column_amount->amount)){
                if($is_decimal == 1){
                    $amount = number_format($column_amount->amount,1);
                }
                else{
                    if($round_off== 3){
                        $amount = number_format($column_amount->amount,1);
                    }
                    elseif($round_off== 2){
                        $amount = number_format($column_amount->amount,2);
                    }
                    else{
                        $amount = number_format($column_amount->amount);
                    }
                }
            }
            else
            {
                $amount = $column_amount->amount;
            }
        }
        $amount = isset($amount) ? $amount : '-';
        // $amount = !empty($column_amount) ? (is_numeric($column_amount->amount) ? number_format($column_amount->amount) : $column_amount->amount) : '-';
        return $amount;
    }
}

if(!function_exists('consolidatedpayrollColumnAmount')){
    function consolidatedpayrollColumnAmount($payroll_id,$employee_id,$column_id,$is_decimal='',$round_off='',$status=''){
        // dd($payroll_id);
        if(env('COMPANY') == 'CLINIX'){
            $column_amount = \DB::table('payrolldata')->whereRaw("FIND_IN_SET(payroll, '" . implode(',', $payroll_id) . "') = 0")
            ->where('employee', $employee_id)->where('payroll_item', $column_id)->first([DB::raw('SUM(amount) as amount')]);
        }else{
            $column_amount = \DB::table('payrolldata')->whereIn('payroll', $payroll_id)->where('employee', $employee_id)->where('payroll_item', $column_id);
            if(!empty($status)){
                $column_amount->whereIn('payrolldata.employment_status_id',$status);
            }
            $column_amount = $column_amount->first([DB::raw('SUM(amount) as amount')]);
        }
        if(!empty($column_amount)){
            if(is_numeric($column_amount->amount)){
                if($is_decimal == 1){
                    $amount = number_format($column_amount->amount,1);
                }
                else{
                    if($round_off== 3){
                        $amount = number_format($column_amount->amount,1);
                    }
                    elseif($round_off== 2){
                        $amount = number_format($column_amount->amount,2);
                    }
                    else{
                        $amount = number_format($column_amount->amount);
                    }
                }
            }
            else
            {
                $amount = $column_amount->amount;
            }
        }
        $amount = isset($amount) ? $amount : '-';
        // $amount = !empty($column_amount) ? (is_numeric($column_amount->amount) ? number_format($column_amount->amount) : $column_amount->amount) : '-';
        return $amount;
    }
}

if(!function_exists('payrollColumnAmount_pf')){
    function payrollColumnAmount_pf($payroll_id,$employee_id,$column_id,$is_decimal='',$round_off=''){
        $column_amount = \DB::table('payrolldata')->where('payroll', $payroll_id)->where('employee', $employee_id)->where('payroll_item', $column_id)->first([DB::raw('SUM(amount) as amount')]);
        if(!empty($column_amount)){
            if(is_numeric($column_amount->amount)){
                if($is_decimal == 1){
                    $amount = number_format($column_amount->amount,1);
                }
                else{
                    if($round_off== 3){
                        $amount = number_format($column_amount->amount,1);
                    }
                    elseif($round_off== 2){
                        $amount = number_format($column_amount->amount,2);
                    }
                    else{
                        $amount = number_format($column_amount->amount);
                    }
                }
            }
            else
            {
                $amount = $column_amount->amount;
            }
        }
        $amount = isset($amount) ? $amount : '-';
        // $amount = !empty($column_amount) ? (is_numeric($column_amount->amount) ? number_format($column_amount->amount) : $column_amount->amount) : '-';
        return $amount;
    }
}

if(!function_exists('HandleEmpty')){
    function HandleEmpty($value){
        return !empty($value) ? $value : '-';
    }
}

if(!function_exists('InsertFile')){
    function InsertFile($file,$folder){
        $fileOrignalName = $file->getClientOriginalName();
        $file_path = $folder;
        $path = public_path() . $file_path;
        $filename = time().'_'.rand(000 ,999).'.'.$file->getClientOriginalExtension();
        $file->move($path, $filename);
        $path = $file_path.'/'.$filename;
        return $path;
    }
}

if(!function_exists('InsertBase64Image')){
    function InsertBase64Image($file,$folder){
        $image_base64 = base64_decode($file);
        $file_path = $folder;
        $path = public_path() . $file_path;
        $filename = time().'_'.rand(000 ,999).'.jpg';
        $file = $path.$filename;
        file_put_contents($file, $image_base64);
        $path = $file_path.'/'.$filename;
        return $path;
    }
}

if(!function_exists('userRoles')){
    function userRoles(){
        if(auth()->check()){
            $user_roles = Auth::user()->getRoleNames()->toArray();
            return implode(",", $user_roles);
        }
    }
}

if(!function_exists('checkLeaveBalance')){
    function checkLeaveBalance($employee,$leave_type,$leave_period_id){
        $employeeObj = Employee::find($employee);
        $total_balance = 0;
        $balance = 0;
        $openingBalance = checkEmployeeLeaveTypeOpeningBalance($employee, $leave_type);
        $balance += $openingBalance;
        $leave_taken = 0;
        $carried_forward_balance = 0;
        $prev_leave_taken = 0;
        $default_balance = LeaveTypes::find($leave_type);
        if(env('COMPANY') == 'JSML'){
            $defaultYearBalance = getLeaveTypeDefaultYearBalanceBySetting($default_balance, $employeeObj);
        }
        else{
            $defaultYearBalance = $default_balance->default_per_year;
        }
        $leave_period = LeavePeriods::where('id', $leave_period_id)->first();
        $previousPeriod = LeavePeriods::whereYear('date_start',date('Y', strtotime(date($leave_period->date_start). ' - 1 Year')))->whereYear('date_end',date('Y', strtotime(date($leave_period->date_start). ' - 1 Year')))->first(['id', 'name']);
        if(!empty($previousPeriod)){
            $employee_leaves_previous_year = EmployeeLeaverequest::join('leavetypes' , 'leavetypes.id' , '=' , 'employeeleaves.leave_type')
            ->where([
                ['employeeleaves.employee',$employee],
                ['employeeleaves.leave_type',$leave_type],
                ['leavetypes.carried_forward','Yes'],
                ['employeeleaves.status','Approved'],
                ['employeeleaves.leave_period',$previousPeriod->id]
            ])->get(['employeeleaves.date_start' , 'employeeleaves.date_end' , 'leavetypes.name','leavetypes.carried_forward']);
            if(count($employee_leaves_previous_year) > 0){
                foreach($employee_leaves_previous_year as $leave){
                    $leave_diff = Carbon::parse($leave->date_start)->diffInDays(Carbon::parse($leave->date_end));
                    if($leave_diff == 0){
                        $prev_leave_taken = $prev_leave_taken + 1;
                    }
                    else{
                        $prev_leave_taken = $prev_leave_taken + $leave_diff + 1;
                    }
                }
                $carried_forward_balance = $defaultYearBalance - $prev_leave_taken;
            }
        }
        $balance += $defaultYearBalance;
        $newEmployeeLeaveSetting = $employeeObj->newEmployeesLeavesSettings()->where('leave_type_id', $leave_type)->where('expiry', '<=' ,now()->endOfYear())->whereYear('expiry' , date('Y'))->first();
        if($newEmployeeLeaveSetting){
            $balance = $newEmployeeLeaveSetting->amount;
        }
        $total_balance = $balance;
        if($default_balance->carried_forward == 'Yes'){
            $total_balance += $carried_forward_balance;
        }
        $employee_leaves_this_year = EmployeeLeaverequest::join('leavetypes' , 'leavetypes.id' , '=' , 'employeeleaves.leave_type')
        ->where('employeeleaves.employee',$employee)->where('employeeleaves.leave_type',$leave_type)->where('employeeleaves.status','Approved')->where('employeeleaves.leave_period',$leave_period_id);
        if(env('COMPANY') == 'JSML'){
            $employee_leaves_this_year = $employee_leaves_this_year->get(['employeeleaves.date_start' , 'employeeleaves.date_end' , 'leavetypes.name','leavetypes.carried_forward', 'employeeleaves.is_half']);
        }
        else{
            $employee_leaves_this_year = $employee_leaves_this_year->get(['employeeleaves.date_start' , 'employeeleaves.date_end' , 'leavetypes.name','leavetypes.carried_forward']);
        }
        
        foreach($employee_leaves_this_year as $leave){
            if(isset($leave->is_half) && $leave->is_half == 1){
                $leave_taken += 0.5;
            }
            else{
                $leave_diff = Carbon::parse($leave->date_start)->diffInDays(Carbon::parse($leave->date_end));
                if($leave_diff == 0){
                    $leave_taken = $leave_taken + 1;
                }
                else{
                    $leave_taken = $leave_taken + $leave_diff + 1;
                }
            }
        }
        $balance -= $leave_taken;
        if(env('COMPANY') == 'JSML'){
            $short_leave_total = $employeeObj
            ->shortLeaves()
            ->where('status', 'Approved')
            ->where('deduction_leave_type_id', $leave_type)
            ->selectRaw('SUM(0.5) AS total')
            ->value('total');
            $leave_taken +=$short_leave_total;
            if($employeeObj->employment_status == 14){
                $balance = $openingBalance;
                $total_balance = $openingBalance;
            }
        }
        return array('balance' => $balance , 'leaves' => $employee_leaves_this_year , 'total_balance' => $total_balance , 'leaves_taken' => $leave_taken);
    }
}

if(!function_exists('checkLeaveHistory')){
    function checkLeaveHistory($employee,$leave_type,$date_start,$date_end){
        $balance = 0;
        $leave_taken = 0;
        $default_balance = LeaveTypes::find($leave_type)->default_per_year;
        $carried_forward_balance = EmployeeLeaveBalance::where('emp_id',$employee)->where('leave_type',$leave_type)->first();
        // if(!empty($carried_forward_balance)){
        //     $balance = $default_balance + $carried_forward_balance->balance;
        // }
        // else{
            $balance += $default_balance;
        // }

        $leave_history = EmployeeLeaveRequest::join('employees' , 'employees.id' , '=' , 'employeeleaves.employee')
        ->join('leavetypes' , 'leavetypes.id' , '=' , 'employeeleaves.leave_type')
        ->join('users' , 'users.id' , '=' , 'employeeleaves.submitted_by')
        ->where('employeeleaves.employee' , $employee)
        ->where('employeeleaves.leave_type' , $leave_type)
        ->where('employeeleaves.status' , 'Approved')
        ->whereYear('employeeleaves.date_start' , date('Y' , strtotime($date_start)))
        ->whereYear('employeeleaves.date_end' , date('Y' , strtotime($date_end)))
        ->get(['employeeleaves.id' , 'employeeleaves.date_start' , 'employeeleaves.date_end' , 'employeeleaves.remarks' , 'employeeleaves.status' , 'employeeleaves.submitted_by' , 'employees.first_name' , 'employees.last_name' , 'leavetypes.name AS leave_name' , 'users.username AS creator']);

        foreach($leave_history as $leave){
            $leave_diff = Carbon::parse($leave->date_start)->diffInDays(Carbon::parse($leave->date_end));
            // dd($leave_diff);
            if($leave_diff == 0){
                $leave_taken = $leave_taken + 1;
            }
            else{
                $leave_taken = $leave_taken + $leave_diff + 1;
            }
        }
        $balance -= $leave_taken;
        return array('balance' => $balance , 'leaves' => $leave_history);
    }
}

if(!function_exists('getColumn_empValue')){
    function getColumn_empValue($emp_id){
        $employee_component = DB::table('employeesalary')->where('employee', $emp_id)->where('component', 72)->value('amount');
        return $employee_component;
    }
}

if(!function_exists('getColumnValue')){
    function getColumnValue($payroll_id,$column_id,$employee_id){
        $column_amount = DB::table('payrolldata')->where([
            ['payroll', $payroll_id],
            ['payroll_item', $column_id],
            ['employee', $employee_id]
        ])->first();
        $amount = !empty($column_amount) ? $column_amount->amount : 0;
        return $amount;
    }
}

if(!function_exists('getMultipleColumnValue')){
    function getMultipleColumnValue($payroll_id,$column_id,$employee_id){
        $amount = DB::table('payrolldata')->where('payroll_item', $column_id)
        ->where('employee', $employee_id)
        ->whereIn('payroll', explode(',',$payroll_id))
        ->select('amount')
        ->sum('amount');
        // dd($column_amount);
        // $amount = !empty($column_amount->first()) ? $column_amount->first()->amount : 0;
        // $amount = !empty($column_amount) ? $column_amount->amount : 0;
        return $amount;
    }
}
if(!function_exists('getMultipleColumnValues')){
    function getMultipleColumnValues($payroll_id,$employee_id,$multiple_columns){
        $columns_totals = DB::table('payrolldata')->where([
                ['payroll', $payroll_id],
                ['employee', $employee_id]
            ])
            ->whereIn('payroll_item', $multiple_columns)
            ->pluck(DB::raw('SUM(amount) as amount'))->toArray();
            $amount = $columns_totals[0];
        return $amount;
    }
}

if(!function_exists('getLocationNetAmount')){
    function getLocationNetAmount($work_station_id, $payrolls,$net_column,$department_id=''){
        $net_amount = DB::table('employees')->join('payrolldata', 'payrolldata.employee', '=', 'employees.id')
        ->where('employees.work_station_id', $work_station_id)
        ->whereIn('payrolldata.payroll', $payrolls)
        ->where('payrolldata.payroll_item', $net_column);
        if(!empty($department_id)){
            $net_amount->where('employees.department', $department_id);
        }
        $net_amount = $net_amount->select(DB::raw('SUM(payrolldata.amount) as total_amount'),'employees.work_station_id','employees.city')->first();
        return $net_amount;
    }
}
if(!function_exists('getChildren')){
    function getChildren($id, $ids = [], $leave_type_setting = false)
    {
        if($leave_type_setting){
            return CompanyStructure::whereIn('parent', $id)->pluck('id')->toArray();
        }
        $final = [];
        $query = DB::select("SELECT id FROM companystructures WHERE parent='$id'");
        if (count($query) > 0) {
            foreach ($query as $deparment) {
                array_push($ids, $deparment->id);
                array_push($final, $deparment->id);
            }
        }
        while (!empty($ids)) {
            foreach ($ids as $id) {

                $loop = getChildren2($id);
                foreach ($loop as $l) {
                    array_push($ids, $l);
                    array_push($final, $l);
                }
                array_shift($ids);
            }

        }
        return $final;
    }
}
if(!function_exists('getChildren2')){
    function getChildren2($id)
    {
        $loop = [];

        $query = DB::select("SELECT id FROM companystructures WHERE parent='$id'");
        if (count($query) > 0) {

            foreach ($query as $department) {
                array_push($loop, $department->id);
            }
        }
        return $loop;

    }
}
if(!function_exists('getMonthInstallment')){
    function getMonthInstallment($loan_id,$month)
    {
        $installment = DB::table('loan_installments')->where('loan_request_id' , '=' , $loan_id)
        ->whereMonth('month' , '=' , date('m', strtotime($month)))
        ->whereYear('month' , '=' , date('Y', strtotime($month)))
        ->orderBy('id', 'desc')
        ->first(['amount', 'month', 'status']);
        return $installment;

    }
}
if(!function_exists('getMonthInstallment_total')){
    function getMonthInstallment_total($loan_id,$month)
    {
        $installment_total = DB::table('loan_installments')->where('loan_request_id' , '=' , $loan_id)
        ->whereMonth('month' , '=' , date('m', strtotime($month)))
        ->whereYear('month' , '=' , date('Y', strtotime($month)))
        ->where('loan_installments.status','!=','cancelled')
        ->orderBy('id', 'desc')
        ->first(['amount', 'month', 'status']);
        return $installment_total;

    }
}

if(!function_exists('getLoanDeductedAmount')){
    function getLoanDeductedAmount($loan_id)
    {
        $total = DB::table('loan_installments')->where('loan_request_id', $loan_id)
        ->where('status', 'deducted')
        ->sum('amount');
        return $total;
    }
}

if(!function_exists('getUser')){
    function getUser($id)
    {
        $user = User::where('id', $id )->select('username')->first();
        // dd($user->username);
        return $user->username;
    }
}

if(!function_exists('replicate_attendance')){
    function replicate_attendance(){
        $query = ManualAttendance::whereYear('created_at' , date('Y'))->whereMonth('created_at' , date('m'))->get([
            'is_manual' , 'is_online' , 'employee' , 'department_id' , 'in_time' , 'out_time' , 'note' , 'image_in' , 'image_out' , 'shift_id' , 'shift_start_time' , 'shift_end_time' , 'work_week_id', 'late_time_allowed' , 'added_using'
        ])->each(function($old_attendance){
            $new_attendance = $old_attendance->replicate();
            $new_attendance->setTable('attendance_history');
            $new_attendance->save();
            $old_attendance->delete();
        });
    }
}

if(!function_exists('EmployeeAttendanceRecord')){
    function EmployeeAttendanceRecord($employee,$date)
    {
        $empTotalLeaves = 0;
        $total_working_hours = 0;
        $total_days_present = 0;
        $total_manual_attendance = 0;
        $total_days_absent = 0;
        $total_days_late = 0;
        $total_overtime_hours = 0;
        $total_off_days = 0;
        $shift_data = DB::table('shift_management')->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                    ->join('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
                    ->where([
                        ['shift_management.employee_id' , $employee->id],
                        ['shift_management.shift_from_date', '<=', $date],
                        ['shift_management.shift_to_date', '>=', $date]
                    ])
                    ->select('shift_management.work_week_id','shift_management.shift_id','shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc','shift_type.sandwich_absent','work_week.desc as work_week','shift_management.late_time_allowed')
                    ->orderBy('shift_management.id', 'DESC')
                    ->first();
        if(!empty($shift_data))
        {
            $shift_desc = $shift_data->shift_desc;
            $work_week_id = $shift_data->work_week_id;
            $shift_start_time = $shift_data->shift_start_time;
            $shift_end_time = $shift_data->shift_end_time;
            $late_time_allowed = $shift_data->late_time_allowed;
        }
        else
        {
            $shift_desc = 'Nil';
            $shift_start_time = '09:00:00';
            $shift_end_time = '17:00:00';
            $late_time_allowed = 11;
            $work_week_id = 1;
        }
        $shift_start_datetime = "$date ".$shift_start_time;
        $shift_end_datetime = "$date ".$shift_end_time;
        if (date('H', strtotime($shift_start_time)) > date('H', strtotime($shift_end_time))) {
            $shift_end_datetime = date('Y-m-d H:i:s', strtotime($shift_end_datetime . ' +1 day'));
        }
        $short_leave = DB::table('short_leave_requests')->where([
            ['employee_id', $employee->id],
            ['date', "$date"],
            ['status', "Approved"]
        ])->first();
        // if(!empty($short_leave))
        // {
        //     $employe_attendance[$employe->id][$date]['short_leave_time_from'] =  $short_leave->time_from;
        //     $employe_attendance[$employe->id][$date]['short_leave_time_to'] =  $short_leave->time_to;
        // }
        $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$date")->first();
        if(!empty($attendance))
        {
            if($attendance->is_manual == 1){
                $total_manual_attendance = $total_manual_attendance + 1;
            }
            $total_days_present = $total_days_present+1;
            $overtime_hours = '0.00';
            $working_hours  =  '0.00';
            $grace_period = date("H:i",strtotime("+$late_time_allowed minutes", strtotime($shift_start_time)));
            if(!empty($attendance->in_time) && !empty($attendance->out_time))
            {
                $attend_time_in = $attendance->in_time;
                $attend_time_out = $attendance->out_time;
                $e_grace_period = date("Y-m-d H:i:s",strtotime("+$late_time_allowed minutes", strtotime($shift_start_datetime)));
                if(strtotime($attend_time_in) < strtotime($e_grace_period)){
                    $attend_time_in = $shift_start_datetime;
                }
                if(strtotime($attend_time_out) > strtotime($shift_end_datetime)){
                    $attend_time_out = $shift_end_datetime;
                }
                $working_hours = Carbon::parse($attend_time_in)->floatDiffInHours($attend_time_out);
                $total_working_hours = $total_working_hours + $working_hours;
                $overtime = DB::table('overtime_management')->where('employee_id', $employee->id)->where('overtime_date', "$date")->where('status', '=', 'Approved')->first(['overtime']);
                if(!empty($overtime))
                {
                    $overtime_hours = $overtime->overtime;
                    $total_overtime_hours = $total_overtime_hours + $overtime_hours;
                }
            }
            $grace_period = date("H:i:s",strtotime("+$late_time_allowed minutes", strtotime($shift_start_time)));
            if(date('H:i:s',strtotime($attendance->in_time)) > $grace_period)
            {
                $total_days_late = $total_days_late+1;
            }
            $employe_attendance['in_time'] =  date('H:i:s',strtotime($attendance->in_time));
            $employe_attendance['out_time'] =  !empty($attendance->out_time) ? date('H:i:s',strtotime($attendance->out_time)) : 'Nil';
            $employe_attendance['in_datetime'] =  $attendance->in_time;
            $employe_attendance['out_datetime'] =  $attendance->out_time;
            $employe_attendance['shift_start_time'] =  date('H:i',strtotime($shift_start_time));
            $employe_attendance['shift_end_time'] =  date('H:i',strtotime($shift_end_time));
            $employe_attendance['shift_desc'] =  $shift_desc;
            $employe_attendance['is_manual'] =  $attendance->is_manual;
            $employe_attendance['department_id'] =  $attendance->department_id;
            $employe_attendance['grace_period'] =  $grace_period;
            $employe_attendance['working_hours'] =  $working_hours;
            $employe_attendance['total_working_hours'] =  $total_working_hours;
            $employe_attendance['overtime_hours'] =  $overtime_hours;
        }
        else{
            $absent_value = 'A';
            $absent_value_color = 'red';
            if($date > date('Y-m-d')){
                $absent_value = '-';
                $absent_value_color = 'grey';
            }
            $employee_leave = DB::table('employeeleaves')
                ->join('leavetypes', 'leavetypes.id', '=','employeeleaves.leave_type')
                ->where([
                ['employee', $employee->id],
                ['date_start', '<=', "$date"],
                ['date_end', '>=', "$date"],
                ['status', '=', 'Approved']
            ])->first(['leavetypes.name as leave_type' , 'leavetypes.abbreviation' , 'leavetypes.leave_color']);
            $work_week_days = DB::table('workdays')->where([
                ['work_week_id', $work_week_id],
                ['status', '=', 'Non-working Day']
            ])->pluck('name')->toArray();
            if(in_array(date('l', strtotime("$date")), $work_week_days))
            {
                $absent_value = 'OFF';
                $absent_value_color = '#28c76f';
                $previous_date = date('Y-m-d', strtotime(''.$date.' -1 day'));
                $next_date = date('Y-m-d', strtotime(''.$date.' +1 day'));
                if(!empty($shift_data) && $shift_data->sandwich_absent == 1)
                {
                    $previous_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$previous_date")->first(['id']);
                    // if($next_date <= date('Y-m-d'))
                    // {
                        $next_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$next_date")->first(['id']);
                    // }
                    $employee_previous_leave = DB::table('employeeleaves')
                        ->join('leavetypes', 'leavetypes.id', '=','employeeleaves.leave_type')
                        ->where([
                        ['employee', $employee->id],
                        ['date_start', '<=', "$previous_date"],
                        ['date_end', '>=', "$previous_date"],
                        ['status', '=', 'Approved']
                    ])->first(['leavetypes.name as leave_type' , 'leavetypes.abbreviation' , 'leavetypes.leave_color']);
                    $employee_next_leave = DB::table('employeeleaves')
                        ->join('leavetypes', 'leavetypes.id', '=','employeeleaves.leave_type')
                        ->where([
                        ['employee', $employee->id],
                        ['date_start', '<=', "$next_date"],
                        ['date_end', '>=', "$next_date"],
                        ['status', '=', 'Approved']
                    ])->first(['leavetypes.name as leave_type' , 'leavetypes.abbreviation' , 'leavetypes.leave_color']);
                    if(empty($employee_previous_leave) && empty($employee_next_leave) && empty($previous_attendance) && empty($next_attendance))
                    {
                        $absent_value = 'A';
                        $absent_value_color = 'red';
                    }
                }
            }
            if(!empty($employee_leave))
            {
                $absent_value = !empty($employee_leave->abbreviation) ? $employee_leave->abbreviation : 'L';
                $absent_value_color = !empty($employee_leave->leave_color) ? $employee_leave->leave_color : 'green';
                $empTotalLeaves = $empTotalLeaves + 1;
            }
            $holiday = DB::table('holidays')->where('dateh', "$date")->first();
            if(!empty($holiday))
            {
                if(empty($previous_attendance) && empty($next_attendance)){
                    $absent_value = 'NH';
                    $absent_value_color = 'skyblue';
                }
            }
            $checked_date = "$date";
            if(strtotime($employee->joined_date) > strtotime($checked_date)){
                $absent_value = '-';
                $absent_value_color = 'grey';
            }
            if(!empty($employee->suspension_date) && $employee->suspension_date != '0000-00-00' && $employee->suspended == 1){
                if(strtotime($employee->suspension_date) < strtotime($checked_date)){
                    $absent_value = '-' ;
                    $absent_value_color = 'grey';
                }
            }
            if(!empty($employee->restore_date) && $employee->restore_date != '0000-00-00'){
                if(strtotime($checked_date) < strtotime($employee->restore_date)){
                    $absent_value = '-' ;
                    $absent_value_color = 'grey';
                }
            }
            if($absent_value == 'A')
            {
                $total_days_absent = $total_days_absent+1;
            }
            if($absent_value == 'OFF' || $absent_value == 'NH')
            {
                $total_off_days = $total_off_days+1;
            }
            $employe_attendance['absent_value'] =  $absent_value;
            $employe_attendance['absent_value_color'] =  $absent_value_color;
        }
        // dd($employe_attendance);
        return $employe_attendance;
    }
}


if(!function_exists('getGlAccountsList')){
    function getGlAccountsList(){
        if(in_array(env('COMPANY'), getErpCompanies())){
            $address = getErpCompanyAddress().'getGlAccountsList';
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $address,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            return json_decode($response)->data;
        }
        return GlAccount::get();
    }
}

if(!function_exists('getBankAccounts')){
    function getBankAccounts(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://173.212.206.138/jsmlerp/api/get_bank_accounts',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        // dd(json_decode($response)->data);
        return json_decode($response)->data;
    }
}

if(!function_exists('getCheckBookByBanks')){
    function getCheckBookByBanks($bank_id){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://173.212.206.138/jsmlerptest/api/get_check_books_by_bank?erp_bank_id='.$bank_id.'',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($response);
        if($result->status != 'failed'){
            return $result->cheque_books;
        }
        else{
            throw new \Exception($result->error);
        }
    }
}

if(!function_exists('getCheckBookByLeafs')){
    function getCheckBookByLeafs($cheque_book_id){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://173.212.206.138/jsmlerptest/api/get_all_cheque_book_leafs?cheque_book_id='.$cheque_book_id.'',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response)->leafs;
    }
}


if(!function_exists('getFairPriceShopCustomersList')){
    function getFairPriceShopCustomersList(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://173.212.206.138/jsmlerp/api/get_fair_price_shop_customers_list',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response)->data;
    }
}

function numberTowords(float $amount)
{
    $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
    // Check if there is any number after decimal
    $amt_hundred = null;
    $count_length = strlen($num);
    $x = 0;
    $string = array();
    $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
        3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
        7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
        10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
        13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
        16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
        19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
        40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
        70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
    $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore','Arab');
    while( $x < $count_length ) {
        $get_divider = ($x == 2) ? 10 : 100;
        $amount = floor($num % $get_divider);
        $num = floor($num / $get_divider);
        $x += $get_divider == 10 ? 1 : 2;
        if ($amount) {
            $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
            $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
            $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.'
            '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. '
            '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
            }else $string[] = null;
        }
    $implode_to_Rupees = implode('', array_reverse($string));
    $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . "
    " . $change_words[$amount_after_decimal % 10]) . ' Paise' : '';
    return ($implode_to_Rupees ? $implode_to_Rupees . 'Rupees ' : '') . $get_paise;
}

function calculateDistance($x1, $y1, $x2, $y2) {
    $distance = sqrt(pow($x2 - $x1, 2) + pow($y2 - $y1, 2));
    return $distance;
}
function degreesToMeters($latitudeDegrees, $longitudeDegrees) {
    // Earth's radius in meters
    $earthRadius = 6371000;
    // Convert latitude and longitude to radians
    $latitudeRad = deg2rad($latitudeDegrees);
    $longitudeRad = deg2rad($longitudeDegrees);

    // Calculate the meters per degree at the given latitude
    $metersPerDegreeLatitude = $earthRadius * cos($latitudeRad) * 2 * pi() / 360;
    $metersPerDegreeLongitude = $earthRadius * 2 * pi() / 360;

    // Calculate the distance in meters
    $distanceMetersLatitude = $metersPerDegreeLatitude * $latitudeDegrees;
    $distanceMetersLongitude = $metersPerDegreeLongitude * $longitudeDegrees;

    return [
        'latitude_meters' => $distanceMetersLatitude,
        'longitude_meters' => $distanceMetersLongitude,
    ];
}

if(!function_exists('calculateLeavesForNewEmployee')){
    function calculateLeavesForNewEmployee($leaveGroups, $calculation_date)
    {
        $leaveGroups = LeaveGroup::with('leaveTypes')->whereIn('id', $leaveGroups)->get();

        $leaveTypesArray = [];
        foreach ($leaveGroups as $leaveGroup) {
            $leaveTypes = $leaveGroup->leaveTypes()->get(['id', 'name', 'default_per_year'])->toArray();
            $leaveTypesArray = array_merge($leaveTypesArray, $leaveTypes);
        }

        $leavePeriod = LeavePeriods::where('status', 'Active')->first();
        $calculationDate = Carbon::parse($calculation_date);
        $endOfYear = Carbon::parse($leavePeriod->date_end);
        $remainingMonths = $calculationDate->diffInMonths($endOfYear) + 1; // Add 1 to include the joining date month

        $leaveData = [];

        foreach ($leaveTypesArray as $leaveType) {
            $availableLeaves = round($leaveType['default_per_year'] / 12 * $remainingMonths);

            $leaveData[] = [
                'leaveTypeID' => $leaveType['id'],
                'leaveType' => $leaveType['name'],
                'allowed' => $availableLeaves,
                'availed' => 0,
                'remaining' => $availableLeaves,
            ];
        }

        return $leaveData;
    }
}

if(!function_exists('payrollEmployeeColumns')){
    function payrollEmployeeColumns($find = null)
    {
        $columns = [
            'employee_id' => 'Employee Id',
            'employee_code' => 'Employee Code',
            'full_name' => 'Full Name',
            'nic_num' => 'CNIC',
            'department_name' => 'Department Name',
            'job_title' => 'Designation',
            'employment_status' => 'Employment Status',
        ];

        if(env('COMPANY') == 'HEALTHWISE'){
            $columns['signature'] = 'Signature';
        }

        if($find){
            if (array_key_exists($find, $columns)) {
                return $columns[$find];
            }
        }

        return $columns;
    }
}

if(!function_exists('checkEmployeeLeaveTypeOpeningBalance')){
    function checkEmployeeLeaveTypeOpeningBalance($employee, $leaveType)
    {
        $balance = EmployeeLeaveBalance::where('emp_id', $employee)->where('leave_type', $leaveType)->first();
        if(!empty($balance)){
            return $balance->balance;
        }
        else{
            return 0;
        }
    }
}

if(!function_exists('getAllShifts')){
    function getAllShifts()
    {
        return ShiftType::all();
    }
}

if(!function_exists('getAllEmployementStatuses')){
    function getAllEmployementStatuses()
    {
        return EmploymentStatus::all();
    }
}

if(!function_exists('getEmployeeShift')){
    function getEmployeeShift($employee, $date)
    {
        $shift = DB::table('shift_management')->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
            ->where([
                ['shift_management.employee_id', $employee],
                ['shift_management.shift_from_date', '<=', $date],
                ['shift_management.shift_to_date', '>=', $date],
                ['shift_management.status', '=', 'Approved'],
                ['shift_management.deleted_at', '=', null],
            ])
            ->select('shift_management.shift_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'shift_management.late_time_allowed', 'open_working_hours' , 'shift_type.hours_per_month')
            ->orderBy('shift_management.id', 'DESC')
            ->first();
        return $shift;
    }
}

if(!function_exists('employeeWorkingHoursInUnicornForMultipleAttendance')){
    function employeeWorkingHoursInUnicornForMultipleAttendance($employee, $startDate, $endDate)
    {
        $workingHours = 0;
        for ($date = $startDate; $date <= $endDate; $date++) {
            $dailyWorkingHours = 0;
            $shift = getEmployeeShift($employee, $date);
            $attendance = DB::table('attendance')->where('employee', $employee)->whereDate('in_time', "$date")->orderBy('id', 'ASC')->get();
            foreach ($attendance as $attendanceRecord) {
                $attend_time_in_carbon = Carbon::parse($attendanceRecord->in_time);
                if(!empty($attendanceRecord->out_time)){
                    $attend_time_out_carbon = Carbon::parse($attendanceRecord->out_time);
                }
                else{
                    $attend_time_out_carbon = Carbon::parse($date.' '.$shift->shift_end_time);
                    $shiftStartTime = Carbon::parse($date.' '.$shift->shift_start_time);
                    $shiftEndTime = Carbon::parse($date.' '.$shift->shift_end_time);
                    if($shiftStartTime > $shiftEndTime){
                        $attend_time_out_carbon->addDay();
                    }
                }
                $dailyWorkingHours += $attend_time_in_carbon->floatDiffInHours($attend_time_out_carbon);
            }
            $dailyOpenWorkingHours = $shift->open_working_hours ?? 0;
            $workingHours += $dailyWorkingHours <= $dailyOpenWorkingHours ? $dailyWorkingHours : $dailyOpenWorkingHours;
        }
        $MonthOpenWorkingHours = $shift->hours_per_month ?? 0;
        $workingHours = $workingHours <= $MonthOpenWorkingHours ? $workingHours : $MonthOpenWorkingHours;
        return $workingHours;
    }
}

if(!function_exists('getunicornDoubleAttendanceShiftsNames')){
    function getunicornDoubleAttendanceShiftsNames()
    {
        // List of names having double attendance
        return [
            'RPI Ravi Shift',
            'SC',
            'RPI Bhawalpur',
            'RPI Multan',
            'Shahlatif RPI',
            'Monjodaro RPI',
            'Deranawab Sb',
            'Mehrab Pur',
            'MirPur Mathelo',
            'Rohri',
            'Shahdad Pur',
            'Tando Adam',
            'Sadiqabad',
            'Liaquat Pur'
        ];
    }
}

if (!function_exists('getUnicornDoubleAttendanceShifts')) {
    function getUnicornDoubleAttendanceShifts()
    {
        // List of names having double attendance
        $names = getunicornDoubleAttendanceShiftsNames();

        $shifts = ShiftType::whereIn('shift_desc', $names)->get();

        return $shifts;
    }
}

if(!function_exists('insertInSalaryComponentHistory')){
    function insertInSalaryComponentHistory($employee,$component,$amount,$calculation_group,$remarks = NULL)
    {
        $checkOldValue = DB::table('salary_component_history')->where([['employee_id',$employee],['component_id',$component]])->orderBy('id','DESC')->first();
        $flag = !empty($checkOldValue) && $checkOldValue->new_amount != $amount ? true : (empty($checkOldValue) && $amount > 0 ? true : false);
        if($flag == true){
            DB::table('salary_component_history')->insert([
                'employee_id' => $employee,
                'component_id' => $component,
                'calculation_group_id' => $calculation_group,
                'old_amount' => !empty($checkOldValue) ? $checkOldValue->new_amount : 0.00,
                'new_amount' => $amount,
                'remarks' => $remarks,
                'created_by' => Auth::id(),
            ]);
        }
    }
}

if(!function_exists('getBasicSalaryFromLatestPayrool')){
    function getBasicSalaryFromLatestPayrool($employee,$department)
    {
        $calculationGroup = PayrollEmployee::where('employee',$employee)->value('deduction_group');
        // $basicSalaryColumn = PayrollColumn::where([['salary_column_type','basic_salary'],['deduction_group',$calculationGroup]])->value('id');
        // For getting basic salary,first need to get previous month payroll
        // We cannot get basic salary component value because for some calculation groups the basic salary payroll column of other type e.g calculation. So, that's why getting basic salary from the payroll
        $previousPayroll = Payroll::where([['deduction_group',$calculationGroup],['status', 'Completed']])->latest()->value('id');
        if(empty($previousPayroll)){
            $previousPayroll = Payroll::where([['deduction_group',$calculationGroup],['status', 'Completed']])->latest()->skip(1)->take(1)->value('id');
        }
        $amount = \DB::table('payrolldata')->where([['payroll', $previousPayroll],['employee', $employee],['payroll_item', 8]])->first(['amount']);
        $amount = !empty($amount) ? $amount->amount : 0;
        return $amount;
    }
}

if(!function_exists('getEmployeeLeaveTypesBySettings')){
    function getEmployeeLeaveTypesBySettings($employee)
    {
        $employeeObj = Employee::find($employee);
        $settingIds = LeaveTypeSetting::whereRaw("JSON_SEARCH(departments, 'one', '$employeeObj->department') IS NOT NULL")
        ->orWhereRaw("JSON_SEARCH(sections, 'one', '$employeeObj->department') IS NOT NULL")
        ->orWhereRaw("JSON_SEARCH(employement_statuses, 'one', '$employeeObj->employment_status') IS NOT NULL")
        ->orWhereRaw("JSON_SEARCH(employee, 'one', '$employeeObj->id') IS NOT NULL")
        ->pluck('leave_type_id')->toArray();
        if(count($settingIds) > 0){
            return LeaveTypes::whereIn('id', $settingIds)->get();
        }
        else{
            return getEmployeeLeaveTypes($employee);
        }
    }
}

if(!function_exists('getEmployeeLeaveTypes')){
    function getEmployeeLeaveTypes($employee)
    {
        $leave_groups = DB::table('leavegroupemployees')->where('employee', $employee)->pluck('leave_group')->toArray();
        if(count($leave_groups) == 2){
            $data = LeaveTypes::where('leavetype_status',1)->select('leavetypes.*')->get();
        }
        else{
            $data = LeaveTypes::where('leavetype_status',1)->whereIn('leave_group', $leave_groups)->select('leavetypes.*')->get();
        }
        return $data;
    }
}

if(!function_exists('getLeaveTypeDefaultYearBalanceBySetting')){
    function getLeaveTypeDefaultYearBalanceBySetting($leaveType, $employeeObj)
    {
        $LeaveTypeSetting = LeaveTypeSetting::where('employee', $employeeObj->id)->where('leave_type_id', $leaveType->id)->first();
        $setting = LeaveTypeSetting::where('leave_type_id', $leaveType->id)
            ->where(function ($query) use ($employeeObj, $LeaveTypeSetting) {
                if (!empty($LeaveTypeSetting)) {
                    $query->orWhereRaw("JSON_SEARCH(employee, 'one', '$employeeObj->id') IS NOT NULL");
                } else {
                    $query->whereRaw("JSON_SEARCH(departments, 'one', '$employeeObj->department') IS NOT NULL")
                        ->orWhereRaw("JSON_SEARCH(sections, 'one', '$employeeObj->department') IS NOT NULL")
                        ->orWhereRaw("JSON_SEARCH(employement_statuses, 'one', '$employeeObj->employment_status') IS NOT NULL");
                }
            })
            ->first();
        if(!empty($setting)){
            return $setting->amount;
        }
        else{
            return $leaveType->default_per_year;
        }
    }
}

if(!function_exists('getEmployeeLeaveTypesBySettingsForStaffDirectory')){
    function getEmployeeLeaveTypesBySettingsForStaffDirectory($employee)
    {
        $employeeObj = Employee::find($employee);
        $settingIds = LeaveTypeSetting::whereRaw("JSON_SEARCH(departments, 'one', '$employeeObj->department') IS NOT NULL")
        ->orWhereRaw("JSON_SEARCH(sections, 'one', '$employeeObj->department') IS NOT NULL")
        ->orWhereRaw("JSON_SEARCH(employement_statuses, 'one', '$employeeObj->employment_status') IS NOT NULL")
        ->orWhereRaw("JSON_SEARCH(employee, 'one', '$employeeObj->id') IS NOT NULL")
        ->pluck('leave_type_id')->toArray();
        if(count($settingIds) > 0){
            return LeaveTypes::whereIn('id', $settingIds)->with(['newEmployeesLeavesSettings' => function($q) use ($employee) {
                $q->where('employee_id', $employee)->where('expiry', '>=', now()->format('Y-m-d'));
            }]);
        }
        else{
            return getEmployeeLeaveTypesForStaffDirectory($employee);
        }
    }
}

if(!function_exists('getEmployeeLeaveTypesForStaffDirectory')){
    function getEmployeeLeaveTypesForStaffDirectory($employee)
    {
        $leave_groups = DB::table('leavegroupemployees')->where('employee', $employee)->pluck('leave_group')->toArray();
        if(count($leave_groups) > 0){
            $query = LeaveTypes::with(['newEmployeesLeavesSettings' => function($q) use ($employee) {
                $q->where('employee_id', $employee)->where('expiry', '>=', now()->format('Y-m-d'));
            }]);
        }
        else{
            $leave_groups = DB::table('leavegroupemployees')->pluck('leave_group')->toArray();
            $query = LeaveTypes::with(['newEmployeesLeavesSettings' => function($q) use ($employee) {
                $q->where('employee_id', $employee);
            }])->whereNotIn('leave_group', $leave_groups);
        }

        return $query;
    }
}

if(!function_exists('holidaySettingForEmployee')){
    function holidaySettingForEmployee($employee, $holiday)
    {
        if(isHolidaySettingAllowed()){
            if(is_null($holiday)){
                return false;
            }
            return HolidaySetting::where(function($query) use ($employee) {
                $query->whereRaw("JSON_SEARCH(departments, 'one', '$employee->department') IS NOT NULL")
                ->orWhereRaw("JSON_SEARCH(sections, 'one', '$employee->department') IS NOT NULL")
                ->orWhereRaw("JSON_SEARCH(employement_statuses, 'one', '$employee->employment_status') IS NOT NULL");
                if(!empty($holiday)){
                    $query->orWhereRaw("JSON_SEARCH(holidays, 'one', '$holiday->id') IS NOT NULL");
                }
            })->exists();
        }
        else{
            return true;
        }
    }
}

if(!function_exists('isHolidaySettingAllowed')){
    function isHolidaySettingAllowed()
    {
        $companies = [
            'JSML',
            'THEOPUS',
            'KITCHEN'
        ];
        
        if(in_array(env('COMPANY'), $companies)){
            return true;
        }
        else{
            return false;
        }
    }
}

if(!function_exists('checkHalfLeave')){
    function checkHalfLeave($employee, $date)
    {
        // because in jsml friday is half day for off season
        $carbonDate = Carbon::parse($date);
        if ($carbonDate->isFriday()) {
            return false;
        }
        return DB::table('employeeleaves')
        ->where([
            ['employee', $employee],
            ['date_start', '<=', "$date"],
            ['date_end', '>=', "$date"],
            ['status', '=', 'Approved'],
            ['is_half', '=', 1],
        ])->exists();
    }
}

if (!function_exists('employeeLateHoursInAMonth')) {
    function employeeLateHoursInAMonth($employee, $startDate, $endDate)
    {
        $totalLateHours = 0;
        for ($date = Carbon::parse($startDate); $date->lte(Carbon::parse($endDate)); $date->addDay()) {
            $shift = getEmployeeShift($employee, $date->toDateString());
            $attendance = DB::table('attendance')->where('employee', $employee)->whereDate('in_time', $date->toDateString())->orderBy('id', 'ASC')->first();

            if ($attendance) {
                $attendTime = Carbon::parse($attendance->in_time);
                $shiftStartTime = Carbon::parse($date->toDateString() . ' ' . $shift->shift_start_time);

                // Only count late hours if attendance time is after shift start time
                if ($attendTime->gt($shiftStartTime)) {
                    $lateHours = $attendTime->floatDiffInHours($shiftStartTime);
                    $totalLateHours += $lateHours;
                }
            }
        }

        return round($totalLateHours, 2);
    }
}

if (!function_exists('employeeLateHoursInADay')) {
    function employeeLateHoursInADay($employee, $date)
    {
        $totalLateHours = 0;
        $date = Carbon::parse($date);
        $shift = getEmployeeShift($employee, $date->toDateString());
        $attendance = DB::table('attendance')->where('employee', $employee)->whereDate('in_time', $date->toDateString())->orderBy('id', 'ASC')->first();

        if ($attendance) {
            $attendTime = Carbon::parse($attendance->in_time);
            $shiftStartTime = Carbon::parse($date->toDateString() . ' ' . $shift->shift_start_time);

            // Only count late hours if attendance time is after shift start time
            if ($attendTime->gt($shiftStartTime)) {
                $lateHours = $attendTime->floatDiffInHours($shiftStartTime);
                $totalLateHours += $lateHours;
            }
        }

        return round($totalLateHours, 2);
    }
}

if (!function_exists('listPayrollFunctionsNames')) {
    function listPayrollFunctionsNames() {
        $traitName = 'App\Traits\PayrollFunctions';
        $methods = [];

        if (trait_exists($traitName)) {
            $reflection = new ReflectionClass($traitName);
            foreach ($reflection->getMethods() as $method) {
                $methods[] = $method->name;
            }
        }

        return $methods;
    }
}
