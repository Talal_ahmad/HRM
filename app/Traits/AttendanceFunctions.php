<?php 
namespace App\Traits;
use App\Models\JobTitle;
use App\Models\PayrollData;
use App\Models\CompanyStructure;
use DB;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

trait AttendanceFunctions
{
    public $total_count = 0;
    public function checkDayAttendanceAndHours($employee,$attendance_date,$type)
    {
        // $next_attendance_date = $attendance_date->addDays(1);
        $data = [];
        $data['late_checkin'] = '0';
        $data['is_manual'] = '0';
        // dd($data);
        
        $attendance_date = $attendance_date->toDateString();
        // dd($attendance_date);
        $next_attendance_date = date('Y-m-d', strtotime($attendance_date. ' + 1 days'));
        // dd($next_attendance_date);
        $shift_desc                 = 'Nil';
        $shift_start_time           = '09:00:00';
        $shift_end_time             = '09:00:00';
        $shift_start_date           = $attendance_date . ' ' . $shift_start_time;
        $shift_end_date             = $attendance_date . ' ' . $shift_end_time;
        $late_t_allowed             = 11;
        
        $max_shift_query = DB::select("SELECT id,shift_id,shift_from_date,shift_to_date,late_time_allowed,work_week_id FROM shift_management
        WHERE '$attendance_date' between shift_from_date AND shift_to_date
        AND employee_id = '$employee->id' order by id desc limit 1");
        
        if(count($max_shift_query) > 0){
            
            $shift_management = $max_shift_query[0];
            $late_t_allowed  = $shift_management->late_time_allowed;
            $shift_type_query = DB::select("SELECT * from shift_type where id = '$shift_management->shift_id'
            ");
            
            if(count($shift_type_query) > 0){
                
                $shift_type_query       = $shift_type_query[0];
                $shift_desc             = $shift_type_query->shift_desc;
                $shift_start_time       = $shift_type_query->shift_start_time;
                $shift_end_time         = $shift_type_query->shift_end_time;
                $shift_start_date       = $attendance_date . ' ' . $shift_type_query->shift_start_time;
                $shift_end_date         = $attendance_date . ' ' . $shift_type_query->shift_end_time;
                if($shift_start_time > $shift_end_time){
                    $shift_end_date         = $next_attendance_date . ' ' . $shift_type_query->shift_end_time;
                }
                $late_t_allowed         = 11;
                
            }
        }
        
        $shift_start_date_carbon    = new Carbon($shift_start_date);
        $shift_end_date_carbon      = new Carbon($shift_end_date);
        
        // if($shift_start_time > $shift_end_time){
            //     $shift_end_date_carbon      = new Carbon($shift_end_date);
            // }
            // dd($shift_end_date_carbon);
            
            
            // dd($shift_type_query);
        $min_checkin_time       = 'nil';
        $max_checkout_time       = 'nil';
        $min_checkin_date        = 'nil';
        $max_checkout_date        = 'nil';
        $html = '';
        
        $min_checkin_query = 
        DB::select("SELECT id, in_time,is_manual
        FROM attendance 
        WHERE employee = '$employee->id' 
        AND date(in_time)='$attendance_date' order by id asc limit 1");
        
        $per_day_hours = 0;
        
        if(count($min_checkin_query) > 0){
            if($min_checkin_query[0]->is_manual == 1){
                $data['is_manual'] = '1';
                
            }
            $min_checkin_query_carbon = new Carbon($min_checkin_query[0]->in_time);
            $min_checkin_time       = $min_checkin_query_carbon->format('H:i');
            $min_checkin_date       = $min_checkin_query_carbon->format('d');
            // dd($min_checkin_query_carbon);
            $check_late_time_allowed = $shift_start_date_carbon->addMinutes($late_t_allowed);
            // dd($check_late_time_allowed);
            
            if($min_checkin_query_carbon->gt($check_late_time_allowed)){
                // dd('ss');
                // $min_checkin_query_carbon = $shift_start_date_carbon;
                $data['late_checkin'] = '1';
            }
            
            
        if($shift_start_time > $shift_end_time){
            // dd('hit');
            $max_checkout_query = 
            DB::select("SELECT id, out_time
            FROM attendance 
            WHERE employee = '$employee->id' 
            AND date(in_time)='$attendance_date'
            AND (date(out_time)='$attendance_date' OR date(out_time)='$next_attendance_date') order by id desc limit 1");
        }else{
            $max_checkout_query = 
            DB::select("SELECT id, out_time
            FROM attendance 
            WHERE employee = '$employee->id' 
            AND date(out_time)='$attendance_date' order by id desc limit 1");
            // dd($max_checkout_query);
        }
        // dd($max_checkout_query);
        if(count($max_checkout_query) > 0){
            
            $max_checkout_query_carbon          = new Carbon($max_checkout_query[0]->out_time);
            $max_checkout_time                  = $max_checkout_query_carbon->format('H:i');
            $max_checkout_date                  = $max_checkout_query_carbon->format('d');
            
            if($min_checkin_query_carbon->lt($shift_start_date_carbon)){
                $min_checkin_query_carbon = $shift_start_date_carbon;
                // $data['late_checkin'] = '0';
            }
            // else{
                //     $data['late_checkin'] = '1';
                // }
                if($max_checkout_query_carbon->gt($shift_end_date_carbon)){
                    $max_checkout_query_carbon = $shift_end_date_carbon;
                }
                
                $per_day_hours = ($min_checkin_query_carbon->diffInSeconds($max_checkout_query_carbon,false)/3600);
                // dd($per_day_hours);
                if($per_day_hours < 0){
                    $per_day_hours = 0;
                }
            }
            // dd($data);
            
            // $data = [
                $data['attendance_value'] = 'P';
                $data['max_checkout_time'] = $max_checkout_time;
                // 'max_checkout_time' => $max_checkout_time,
                $data['min_checkin_time'] = $min_checkin_time;
                $data['min_checkin_date'] = $min_checkin_date;
                $data['max_checkout_date'] = $max_checkout_date;
                $data['shift_start_time'] = $shift_start_time;
                $data['shift_end_time'] = $shift_end_time;
                $data['per_day_hours'] = $per_day_hours;
                
                // 'min_checkin_time'  => $min_checkin_time,
                // 'min_checkin_date'  => $min_checkin_date,
                // 'max_checkout_date' => $max_checkout_date,
                // 'shift_start_time'  => $shift_start_time,
                // 'shift_end_time'    => $shift_end_time,
                // 'per_day_hours'     => $per_day_hours
                // ];
                // dd($data);
                
                
            }
            
            
            
            
            
            
            
            
            
            else{
                $absent_value = 'A';
                $attendance_date = new Carbon($attendance_date);
                if($attendance_date->gt(Carbon::now())){
                    $absent_value = '-';
                }
                
                $attendance_date = $attendance_date->toDateString();
                $check_leave_query = DB::select("SELECT id FROM employeeleaves 
                where '$attendance_date' BETWEEN date_start and date_end
                and employee = $employee->id 
                AND status = 'Approved'");
                
                if(count($check_leave_query) > 0){
                    $absent_value = 'L';
                }
                
                
                // $attendance_date = $attendance_date->toDateString();
                $off_days[] = 'Sunday';
                
                if(count($max_shift_query) > 0){
                    $check_work_week_query = DB::select("SELECT * FROM workdays where work_week_id = '$shift_management->work_week_id'
                    AND workdays.`status` = 'Non-working Day'
                    ");
                    
                    if(count($check_work_week_query) > 0){
                        foreach($check_work_week_query as $off_day){
                            $off_days[] = $off_day->name;
                        }
                    }
                }
                $attendance_date_day_name_carbon = new Carbon($attendance_date);
                $attendance_date_day_name = $attendance_date_day_name_carbon->format('l');
                // dd($today_day_name_carbon);
                if(in_array($attendance_date_day_name,$off_days)){
                    $absent_value = 'Off';
                }
                
                
                
                
                $data = [
                    'attendance_value' => $absent_value
                ];
            }
            
            
            
            
            
            
            
            
            $overtime_query = DB::select("SELECT overtime from overtime_management where employee_id = '$employee->id' and overtime_date = '$attendance_date' and `status` = 'Approved'");
            if(count($overtime_query)>0){
                $data['overtime_hours'] = $overtime_query[0]->overtime;
            }else{
                $data['overtime_hours'] = 0;
            }
            
            // dd($data);
            return $data;
                
                
    }
            
}