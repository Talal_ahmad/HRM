<?php
namespace App\Traits;
use App\Models\JobTitle;
use App\Models\LoanType;
use App\Models\Attendance;
use App\Models\PayrollData;
use App\Models\CompanyStructure;
use App\Models\Employee;
use App\Models\EmploymentStatus;
use App\Models\FineReversal;
use App\Traits\AttendanceFunctions;
use DateTime;
use Carbon\CarbonPeriod;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait PayrollFunctions
{
    // use AttendanceFunctions;

    public $total_count = 0;
    public function employee_id($employee,$payroll)
    {
       return $employee->employee_id;
    }

    public function employee_code($employee,$payroll)
    {
       return $employee->employee_code;
    }



    public function name($employee,$payroll)
    {
       return $employee->first_name .' '. $employee->last_name;
    }


    public function designation($employee,$payroll)
    {
        $designation = JobTitle::find($employee->job_title);
        if($designation){
            return $designation->name;
        }else{
            return null;
        }
    }

    public function department($employee,$payroll)
    {
        $department = CompanyStructure::find($employee->department);
        if($department){
            return $department->title;
        }else{
            return null;
        }
    }

    public function employment_status($employee,$payroll)
    {
        $employment_status = EmploymentStatus::find($employee->employment_status);
        if($employment_status){
            return $employment_status->name;
        }else{
            return null;
        }
    }

    public function joined_date($employee,$payroll)
    {
        return $employee->joined_date;
    }

    public function account_details($employee,$payroll)
    {
        return $employee->account_title .'-'.$employee->bank_name .'-'. $employee->account_number;
    }

    public function bank_name($employee,$payroll)
    {
        return $employee->bank_name;
    }

    public function account_title($employee,$payroll)
    {
        return $employee->account_title;
    }

    public function account_number($employee,$payroll)
    {
        return $employee->account_number;
    }


    public function payment_type($employee,$payroll)
    {
        if(empty($employee->account_number) OR is_null($employee->account_number)){
            return 'cash';
        }else{
            return 'bank';
        }
        // return $employee->bank_name .'-'. $employee->account_number;
    }



    public function calculate_employee_data($payroll,$employee,$payroll_columns){
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $period                 = CarbonPeriod::create($start_date, $end_date);
        foreach ($payroll_columns as $column){
            foreach ($period as $start_date) {
                $this->{$column->function_name}($employee,$payroll);
            }
            $payroll_data                           = new PayrollData;
            $payroll_data->payroll                  = $payroll->id;
            $payroll_data->employee                 = $employee->id;
            $payroll_data->payroll_item             = $column->id;
            $payroll_data->amount                   = $this->total_count;
            $payroll_data->save();

            $this->total_count = 0;
            // dd($this->total_count);
        }
    }

    public function total_days_with_joined_date($employee,$payroll)
    {
        $total_days = 0;
        $payroll_month                      = $payroll->payroll_month;
		$payroll_year                       = $payroll->payroll_year;
		$joined_date                        = $employee->joined_date;
		$joined_month                       = date('m', strtotime($employee->joined_date));
        $joined_year                        = date('Y', strtotime($employee->joined_date));
        $month_days = Carbon::now()->month($payroll_month)->daysInMonth;
        if($payroll_month == $joined_month AND $payroll_year == $joined_year){
			$start_date = $joined_date;
            $start_date                         = strtotime($start_date);
            $end_date                           = strtotime($payroll->date_end);
            $diff = $end_date - $start_date;
            $total_days = floor($diff/(60*60*24)) + 1;
		}
        else
        {
            $total_days = $month_days;
        }
        return $total_days;
    }



    function total_absents($employee, $payroll) {
        $start_date                         = $payroll->date_start;
        $payroll_month                      = $payroll->payroll_month;
		$payroll_year                       = $payroll->payroll_year;
        if(env('COMPANY') == 'RoofLine')
        {
            $end_date = $payroll_year.'-'.$payroll_month.'-25';
        }
        else
        {
            $end_date = $payroll->date_end;
        }
		$suspension_date                    = $employee->suspension_date;
		$joined_date                        = $employee->joined_date;
		$joined_month                       = date('m', strtotime($employee->joined_date));
        $joined_year                        = date('Y', strtotime($employee->joined_date));


        if (!empty($suspension_date)) {
            if ($suspension_date != '' AND $suspension_date != '0000-00-00') {
                $end_date = $suspension_date;
            }
		}
		if($payroll_month == $joined_month AND $payroll_year == $joined_year){
			$start_date = $joined_date;
		}
        $period                             = CarbonPeriod::create($start_date, $end_date);
        $total_absents = 0;
        $designation = JobTitle::find($employee->job_title);
        if(!empty($designation))
        {
            if($designation->exempt_working_days == 1){
                return $total_absents;
            }
        }
        if($employee->employment_status == 17){
            return $total_absents;
        }
        foreach ($period as $start_date) {
            if(!empty($employee->restore_date) && $employee->restore_date != '0000-00-00'){
                if(strtotime($start_date) < strtotime($employee->restore_date)){
                    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                    continue;
                }
            }
            $attendance_qry =  DB::select("SELECT * from attendance where employee = '$employee->id' and date(in_time) = '$start_date' limit 1");
            if (count($attendance_qry) < 1) {
                $empLeaveStatusQry = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$start_date' AND date_end >= '$start_date'  AND status = 'Approved'");
                if (count($empLeaveStatusQry) > 0) {
                } else {

                    $max_shift_data = DB::select("SELECT id FROM shift_management
                    WHERE '$start_date' between shift_from_date AND shift_to_date
                    AND employee_id = $employee->id AND status = 'Approved' AND deleted_at IS NULL ORDER BY id DESC LIMIT 1");

                    if($max_shift_data AND !empty($max_shift_data)){
                        $max_shift_data = $max_shift_data[0];
                        $employeeOffDayQry = DB::select("SELECT shift_management.id,workdays.name as off_days,workdays.`status`,shift_type.shift_desc
                        FROM shift_management
                        join workdays on workdays.work_week_id = shift_management.work_week_id
                        join shift_type on shift_type.id = shift_management.shift_id
                        where employee_id = '$employee->id'
                        AND shift_management.id = '$max_shift_data->id'
                        AND workdays.`status` = 'Non-working Day'");
                        if (count($employeeOffDayQry) > 0) {
                            foreach ($employeeOffDayQry as $employeeOffDayTest) {
                                $dayName[] = $employeeOffDayTest->off_days;
                            }
                        } else {
                            $dayName[] = 'Sunday';
                        }
                    }else{
                        $dayName[] = 'Sunday';
                    }
                    $current_day_name = date('l', strtotime("$start_date"));
                    if (!in_array($current_day_name, $dayName)) {
                        $holiday_qry = DB::select("SELECT * FROM holidays
                        WHERE dateh = '$start_date'");

                        $holidaySetting = holidaySettingForEmployee($employee, $holiday_qry);

                        if(count($holiday_qry) == 0 && $holidaySetting){
                            $total_absents = $total_absents + 1;
                        }
                    }
                    $dayName = [];
                }
            }
        }
        return $total_absents;
    }





    function total_minutes_late($employee, $payroll) {

        // $payroll_qry = DB::select("SELECT * from payroll where id = '$payroll_id'")[0];
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $period                 = CarbonPeriod::create($start_date, $end_date);
        $total_working_hours = 0;
        foreach ($period as $start_date) {
            $start_date = $start_date->format('Y-m-d');
            $attendance_qry = DB::select("SELECT * from attendance where employee = '$employee->id' and date(in_time) = '$start_date' limit 1");
            if (count($attendance_qry) > 0) {
                $attendance = $attendance_qry[0];
                $emp_time_in = date_create($attendance->in_time);



                $max_shift_data = DB::select("SELECT id FROM shift_management
                WHERE '$start_date' between shift_from_date AND shift_to_date
                AND employee_id = $employee->id ORDER BY id DESC LIMIT 1");


                $shift_s_datetime = '09:00:00';
                $shift_e_datetime = '09:00:00';
                $shift_s_datetime2 = $start_date. ' ' .'09:00:00';
                $shift_e_datetime2 = '09:00:00';

                if($max_shift_data AND !empty($max_shift_data)){
                    $max_shift_data = $max_shift_data[0];

                    $shift_query = DB::select("SELECT shift_management.id,shift_type.shift_desc,shift_type.shift_start_time,shift_type.shift_end_time,shift_management.late_time_allowed
                    FROM shift_management
                    join shift_type on shift_type.id = shift_management.shift_id
                    where shift_management.employee_id = '$employee->id'
                    AND shift_management.id = '$max_shift_data->id'");

                    if (count($shift_query) > 0) {
                        $shift = $shift_query[0];
                        $shift_s_datetime = $shift->shift_start_time;
                        $shift_e_datetime = $shift->shift_end_time;

                        $shift_s_datetime2 = $start_date. ' ' .$shift->shift_start_time;
                        $shift_e_datetime2 = $shift->shift_end_time;
                    }
                }


                $project = explode('/', $_SERVER['REQUEST_URI']) [1];
                if ($project == 'faplhrm') {
                    $grace_time = date("H:i:s", strtotime("+15 minutes", strtotime($shift_s_datetime)));
                } else {
                    $grace_time = date("H:i:s", strtotime("+12 minutes", strtotime($shift_s_datetime)));
                }
                $test = $start_date . ' ' . $grace_time;
                $grace_time_object = date_create($test);
                $diff = date_diff($grace_time_object, $emp_time_in);
                if ($diff->invert == 0) {
                    $grace_time_object2 = date_create($shift_s_datetime2);
                    $diff2 = date_diff($grace_time_object2, $emp_time_in);

                    $days = $diff2->d;
                    $sec1 = $days * 24 * 3600;
                    $hours = $diff2->h;
                    $sec2 = $hours * 3600;
                    $minutes = $diff2->i;
                    $sec3 = $minutes * 60;
                    $total_sec = $sec1 + $sec2 + $sec3;
                    $working_hours = $total_sec / 60;
                    $total_working_hours = $total_working_hours + $working_hours;
                }
            }
        }
        return $total_working_hours;
    }


    function only_checkins_minutes_late($employee, $payroll){

        // $payroll_qry = DB::select("SELECT * from payroll where id = '$payroll_id'")[0];
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $period                 = CarbonPeriod::create($start_date, $end_date);
        $total_working_hours = 0;
        foreach ($period as $start_date) {
            $start_date = $start_date->format('Y-m-d');
            $attendance_qry = DB::select("SELECT * from attendance where employee = '$employee->id' and date(in_time) = '$start_date' limit 1");
            if (count($attendance_qry) > 0) {
                $attendance = $attendance_qry[0];
                $emp_time_in = date_create($attendance->in_time);



                $max_shift_data = DB::select("SELECT id FROM shift_management
                WHERE '$start_date' between shift_from_date AND shift_to_date
                AND employee_id = $employee->id ORDER BY id DESC LIMIT 1");


                $shift_s_datetime = '09:00:00';
                $shift_e_datetime = '09:00:00';
                $shift_s_datetime2 = $start_date. ' ' .'09:00:00';
                $shift_e_datetime2 = '09:00:00';

                if($max_shift_data AND !empty($max_shift_data)){
                    $max_shift_data = $max_shift_data[0];

                    $shift_query = DB::select("SELECT shift_management.id,shift_type.shift_desc,shift_type.shift_start_time,shift_type.shift_end_time,shift_management.late_time_allowed
                    FROM shift_management
                    join shift_type on shift_type.id = shift_management.shift_id
                    where shift_management.employee_id = '$employee->id'
                    AND shift_management.id = '$max_shift_data->id'");

                    if (count($shift_query) > 0) {
                        $shift = $shift_query[0];
                        $shift_s_datetime = $shift->shift_start_time;
                        $shift_e_datetime = $shift->shift_end_time;

                        $shift_s_datetime2 = $start_date. ' ' .$shift->shift_start_time;
                        $shift_e_datetime2 = $shift->shift_end_time;
                    }
                }


                $project = explode('/', $_SERVER['REQUEST_URI']) [1];
                if ($project == 'faplhrm') {
                    $grace_time = date("H:i:s", strtotime("+15 minutes", strtotime($shift_s_datetime)));
                } else {
                    $grace_time = date("H:i:s", strtotime("+12 minutes", strtotime($shift_s_datetime)));
                }
                $test = $start_date . ' ' . $grace_time;
                $grace_time_object = date_create($test);
                $diff = date_diff($grace_time_object, $emp_time_in);
                if ($diff->invert == 0) {
                    $grace_time_object2 = date_create($shift_s_datetime2);
                    $diff2 = date_diff($grace_time_object2, $emp_time_in);

                    $days = $diff2->d;
                    $sec1 = $days * 24 * 3600;
                    $hours = $diff2->h;
                    $sec2 = $hours * 3600;
                    $minutes = $diff2->i;
                    $sec3 = $minutes * 60;
                    $total_sec = $sec1 + $sec2 + $sec3;
                    $working_hours = $total_sec / 60;
                    $total_working_hours = $total_working_hours + $working_hours;
                }
            }
        }
        return $total_working_hours;
    }

    function overtime_hours($employee, $payroll) {
        // $payroll_qry = DB::select("SELECT * from payroll where id = '$payroll_id'");
        // $payroll = $payroll_qry[0];
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $total_overtime_hours = 0;
        while (strtotime($start_date) <= strtotime($end_date)) {
            $overtime_qry = DB::select("SELECT * from overtime_management where overtime_date = '$start_date' and employee_id = '$employee->id' and `status` = 'Approved'");
            if (count($overtime_qry) > 0) {
                foreach ($overtime_qry as $overtime_details) {
                    $overtime_hours = $overtime_details->overtime;
                    $total_overtime_hours = $total_overtime_hours + $overtime_hours;
                }
            }
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }
        return $total_overtime_hours;
    }

    function total_days_late_within_an_hour($employee, $payroll) {
        // $payroll_qry = DB::select("SELECT * from payroll where id = '$payroll_id'");
        // $payroll = $payroll_qry[0];
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $start_day = date("d", strtotime($start_date));
        $end_day = date("d", strtotime($end_date));
        $count = 0;
        for ($i = $start_day;$i <= $end_day;$i++) {
            $shift_details_qry = DB::select("SELECT * from attendance where employee = '$employee->id' and date(in_time) = '$start_date' limit 1");
            // $shift_details_qry_execute = $this->conn->query($shift_details_qry);
            if (count($shift_details_qry) > 0) {
                $shift_details = $shift_details_qry[0];
                if ($shift_details->shift_id != 0) {
                    $shift_start_time = $shift_details->shift_start_time;
                    $late_time_allowed = $shift_details->late_time_allowed;
                } else {
                    $shift_start_time = '09:00:00';
                    $late_time_allowed = 12;
                }
                $time = strtotime($shift_start_time);
                $Late_time = date("H:i:s", strtotime("+$late_time_allowed minutes", $time));
                $test2 = date("H:i:s", strtotime("+60 minutes", $time));
                if (date("H:i:s", strtotime($shift_details->in_time)) > $Late_time AND date("H:i:s", strtotime($shift_details->in_time)) <= $test2) {
                    $count++;
                }
            }
            $start_date = date("Y-m-d", strtotime('+1 day', strtotime($start_date)));
        }
        return $count;
    }

    function total_days_late_upto_two_hours($employee, $payroll) {
        // $payroll_qry = DB::select("SELECT * from payroll where id = '$payroll_id'");
        // $payroll = $payroll_qry[0];
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $start_day = date("d", strtotime($start_date));
        $end_day = date("d", strtotime($end_date));
        $count = 0;
        for ($i = $start_day;$i <= $end_day;$i++) {
            $shift_details_qry = DB::select("SELECT * from attendance where employee = '$employee->id' and date(in_time) = '$start_date' limit 1");
            // $shift_details_qry_execute = $this->conn->query($shift_details_qry);
            if (count($shift_details_qry) > 0) {
                $shift_details = $shift_details_qry[0];
                if ($shift_details->shift_id != 0) {
                    $shift_start_time = $shift_details->shift_start_time;
                    $late_time_allowed = $shift_details->late_time_allowed;
                } else {
                    $shift_start_time = '09:00:00';
                    $late_time_allowed = 12;
                }
                $time = strtotime($shift_start_time);
                // $Late_time = date("H:i:s", strtotime("+$late_time_allowed minutes", $time));
                $Late_time = date("H:i:s", strtotime("+60 minutes", $time));
                $test2 = date("H:i:s", strtotime("+120 minutes", $time));
                // $test1 = strtotime($Late_time);
                // $test2 = date("H:i:s", strtotime("+60 minutes", $test1));
                // echo $Late_time;
                if (date("H:i:s", strtotime($shift_details->in_time)) > $Late_time AND date("H:i:s", strtotime($shift_details->in_time)) <= $test2) {
                    $count++;
                }
            }
            $start_date = date("Y-m-d", strtotime('+1 day', strtotime($start_date)));
        }
        return $count;
    }

    // function shift_working_hours($employee, $payroll) {

    //     // $payroll_qry = DB::select("SELECT * from payroll where id = '$payroll_id'");
    //     // $payroll = $payroll_qry[0];
    //     // $start_date = $payroll->date_start;
    //     // $end_date = $payroll->date_end;
    //     $total_working_hours = 0;
    //     $payroll_period = CarbonPeriod::create($payroll->date_start, $payroll->date_end);

    //     foreach ($payroll_period as $attendance_date) {
    //         $per_day_data = $this->checkDayAttendanceAndHours($employee,$attendance_date,'payroll');
    //         if($per_day_data['attendance_value'] == 'P'){
    //             $total_working_hours = $total_working_hours + $per_day_data['per_day_hours'];
    //         }
    //     }


    //     return number_format($total_working_hours, 2);
    // }

    function shift_working_hours($employee, $payroll) {
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $total_working_hours = 0;
        while (strtotime($start_date) <= strtotime($end_date)) {
            $shift = getEmployeeShift($employee->id, $start_date);
            if(env('COMPANY') == 'JSML' || env('COMPANY') == 'UNICORN'){
                if(empty($shift)){
                    throw new \Exception("No shift has been assigned to the mentioned employee on $start_date. Please assign a shift and re-process the salary.");
                }
            }
            $unicornDoubleAttendanceShifts = [];
            if (env('COMPANY') == 'UNICORN'){
                $unicornDoubleAttendanceShifts = getUnicornDoubleAttendanceShifts()->pluck('id')->toArray();
            }
            if (env('COMPANY') == 'UNICORN' && in_array($shift->shift_id, $unicornDoubleAttendanceShifts)) {
                return employeeWorkingHoursInUnicornForMultipleAttendance($employee->id, $start_date, $end_date);
            }
            else{
                $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$start_date")->orderBy('id', 'ASC')->first();
                if (!empty($attendance)) {
                    if (!empty($attendance->in_time) AND !empty($attendance->out_time)) {
                        $max_shift_data = DB::select("SELECT id FROM shift_management
                        WHERE '$start_date' between shift_from_date AND shift_to_date
                        AND employee_id = $employee->id AND status = 'Approved' AND `deleted_at` IS NULL ORDER BY id DESC LIMIT 1");
                        $shift_s_datetime = $start_date . ' 09:00:00';
                        $shift_e_datetime = $start_date . ' 17:00:00';
                        $late_t_allowed   = 11;
                        $work_week_id = 1;
                        $night_shift = false;
                        if($max_shift_data AND !empty($max_shift_data)){
                            $max_shift_data = $max_shift_data[0];
                            $shift_query = DB::select("SELECT shift_management.id,shift_type.shift_desc,shift_type.shift_start_time,shift_type.shift_end_time,shift_management.late_time_allowed,shift_management.work_week_id
                            FROM shift_management
                            join shift_type on shift_type.id = shift_management.shift_id
                            where shift_management.employee_id = '$employee->id'
                            AND shift_management.id = '$max_shift_data->id' AND shift_management.deleted_at IS NULL
                            ");
                                $shift = $shift_query[0];
                                $shift_s_datetime = $start_date . ' ' . $shift->shift_start_time;
                                $shift_e_datetime = $start_date . ' ' . $shift->shift_end_time;
                                $late_t_allowed   = $shift->late_time_allowed;
                                $work_week_id   = $shift->work_week_id;
                                if (date('H', strtotime($shift->shift_start_time)) > date('H', strtotime($shift->shift_end_time))) {
                                    $night_shift = true;
                                    $shift_e_datetime = date('Y-m-d H:i:s', strtotime($shift_e_datetime . ' +1 day'));
                                }
                        }

                        $e_grace_period = date("Y-m-d H:i:s",strtotime("+$late_t_allowed minutes", strtotime($shift_s_datetime)));
                        $a_time_in = $attendance->in_time;
                        $a_time_out = $attendance->out_time;
                        if (strtotime($a_time_in) < strtotime($e_grace_period)) {
                            $a_time_in = $shift_s_datetime;
                        }
                        if (strtotime($a_time_out) > strtotime($shift_e_datetime)) {
                            $a_time_out = $shift_e_datetime;
                        }
                        $working_hours = Carbon::parse($a_time_in)->floatDiffInHours($a_time_out);
                        if(env('COMPANY') == 'HEALTHWISE'){
                            $work_week_days = DB::table('workdays')->where([
                                ['work_week_id', $work_week_id],
                                ['status', '=', 'Non-working Day'],
                            ])->pluck('name')->toArray();
                            if (in_array(date('l', strtotime("$start_date")), $work_week_days)) {
                                $working_hours = 0;
                            }
                        }
                        $total_working_hours += $working_hours;
                    }
                }
            }
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }
        return round($total_working_hours, 2);
    }

    function total_hours_worked_in_office_during_the_month($employee, $payroll)
    {
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $total_working_hours = 0;
        while (strtotime($start_date) <= strtotime($end_date)) {
            $attendance_qry = DB::select("SELECT * from attendance where employee = '$employee->id' and date(in_time) = '$start_date'");
            if (count($attendance_qry) > 0) {
                foreach ($attendance_qry as $attendance) {
                    if (!empty($attendance->in_time) AND !empty($attendance->out_time)) {

                        $emp_time_in = date_create($attendance->in_time);
                        $emp_time_out = date_create($attendance->out_time);
                        $diff = date_diff($emp_time_in, $emp_time_out);
                        if ($diff->invert == 0) {
                            $days = $diff->d;
                            $sec1 = $days * 24 * 3600;
                            $hours = $diff->h;
                            $sec2 = $hours * 3600;
                            $minutes = $diff->i;
                            $sec3 = $minutes * 60;
                            $total_sec = $sec1 + $sec2 + $sec3;
                            $working_hours = $total_sec / 3600;
                        } else {
                            $working_hours = 0;
                        }
                        $total_working_hours = $total_working_hours + $working_hours;
                    }
                }
            }
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }
        return number_format($total_working_hours, 2);
    }

    function short_leave_hours($employee, $payroll) {
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $total_short_leave_hours = 0;
        while (strtotime($start_date) <= strtotime($end_date)) {
            $attendance_qry = DB::select("SELECT * from short_leave_requests where employee_id = '$employee->id' AND date = '$start_date' AND status = 'Approved'");
            if (count($attendance_qry) > 0) {
                foreach ($attendance_qry as $key => $value) {
                    $date = $value->date;
                    $next_date = '';
                    if(date('H : i : s',strtotime($value->time_to)) < date('H : i : s',strtotime($value->time_from)) ){
                        $next_date = date('Y-m-d', strtotime($date . ' +1 day'));
                    }
                    $start = Carbon::parse($date.$value->time_from);
                    $end =  Carbon::parse($next_date ? $next_date.$value->time_to : $date.$value->time_to);
                    $hours = $start->diff($end)->format('%h');
                    $minutes = $start->diff($end)->format('%I');
                    $duration = $hours + $minutes / 60;
                    $total_short_leave_hours += round($duration,1);
                }
            }
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }
        return number_format($total_short_leave_hours, 2);
    }

    function short_leave_days($employee, $payroll) {
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $total_short_leave_hours = 0;
        while (strtotime($start_date) <= strtotime($end_date)) {
            $attendance_qry = DB::select("SELECT * from short_leave_requests where employee_id = '$employee->id' AND date = '$start_date' AND status = 'Approved'");
            if (count($attendance_qry) > 0) {
                foreach ($attendance_qry as $key => $value) {
                    $date = $value->date;
                    $next_date = '';
                    if(date('H : i : s',strtotime($value->time_to)) < date('H : i : s',strtotime($value->time_from)) ){
                        $next_date = date('Y-m-d', strtotime($date . ' +1 day'));
                    }
                    $start = Carbon::parse($date.$value->time_from);
                    $end =  Carbon::parse($next_date ? $next_date.$value->time_to : $date.$value->time_to);
                    $hours = $start->diff($end)->format('%h');
                    $minutes = $start->diff($end)->format('%I');
                    $duration = $hours + $minutes / 60;
                    $total_short_leave_hours += round($duration,1);
                }
            }
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }
        return number_format($total_short_leave_hours / 24, 2);
    }

    function total_leaves($employee, $payroll) {
        $empLeaveStatusQry = DB::select("SELECT * FROM employeeleaves WHERE employee = '$employee->id' AND date_start >= '$payroll->date_start' AND date_end <= '$payroll->date_end' AND status = 'Approved'");

        $totalDays = 0;

        foreach ($empLeaveStatusQry as $leave) {
            $startDate = strtotime($leave->date_start);
            $endDate = strtotime($leave->date_end);
            $totalDays += floor(($endDate - $startDate) / (60 * 60 * 24)) + 1; // Adding 1 to include both start and end dates
        }

        return (int) $totalDays; // Cast to integer
    }

    function total_paid_leaves($employee,$payroll){
        $total_paid_leaves = 0;
        $employeeLeaves = DB::table('employeeleaves')->where('employee',$employee->id)->whereDate('date_start' ,'>=', $payroll->date_start)->whereDate('date_end' ,'<=', $payroll->date_end)->where('status','=','Approved')->get(['id','employee','date_start','date_end']);
        foreach ($employeeLeaves as $key => $leave) {
            $paidLeaves = DB::table('paid_leave')->where('leave_id',$leave->id)->count();
            $total_paid_leaves += $paidLeaves;
        }
        return $total_paid_leaves;
    }

    function total_paid_days($employee, $payroll) {
        // $payroll_qry = DB::select("SELECT * from payroll where id = '$payroll_id'");
        // $payroll = $payroll_qry[0];
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $payroll_month = $payroll->payroll_month;
		$payroll_year = $payroll->payroll_year;

        $paid_days = 0;
        $paid_days_query = DB::select("SELECT id
											from employeeleaves
											where is_paid = 1
											and employee = '$employee->id'
                                            and year(date_start) = '$payroll_year'
											and month(date_start) = '$payroll_month'
											");
        // $paid_days_query_result = $this->conn->query($paid_days_query);
        if (count($paid_days_query) > 0) {
            foreach ($paid_days_query as $paid_days_data) {
                $child_paid_days_query = DB::select("SELECT id from paid_leave where leave_id = '$paid_days_data->id' AND month(leave_date) = '$start_date'");
                // $child_paid_days_query_result = $this->conn->query($child_paid_days_query);
                $paid_days = $paid_days + count($child_paid_days_query);
            }
        }
        return $paid_days;
        // return $paid_days_data->paid_days;

    }

    function total_paid_days_hours($employee, $payroll) {
        // $payroll_qry = DB::select("SELECT * from payroll where id = '$payroll_id'");
        // $payroll = $payroll_qry[0];
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $total_paid_hours = 0;
        while (strtotime($start_date) <= strtotime($end_date)) {
            $paid_days_query = DB::select("SELECT id
												from employeeleaves
												where is_paid = 1
												and employee = '$employee->id'
												and ('$start_date' between  date_start and date_end)
												");
            // $paid_days_query_result = $this->conn->query($paid_days_query);
            if (count($paid_days_query) > 0) {
                foreach ($paid_days_query as $paid_days_data) {
                    $child_paid_days_query = DB::select("SELECT id from paid_leave where leave_id = '$paid_days_data->id' AND date(leave_date) = '$start_date'");
                    // $child_paid_days_query_result = $this->conn->query($child_paid_days_query);
                    if (count($child_paid_days_query) > 0) {
                        foreach ($child_paid_days_query as $child_paid_days) {

                            $max_shift_data = DB::select("SELECT id FROM shift_management
                            WHERE '$start_date' between shift_from_date AND shift_to_date
                            AND employee_id = $employee->id ORDER BY id DESC LIMIT 1");

                            if($max_shift_data AND !empty($max_shift_data)){
                                $max_shift_data = $max_shift_data[0];

                                $shift_management_query = DB::select("SELECT shift_management.id,shift_type.shift_start_time,shift_type.shift_end_time ,
                                timediff(shift_type.shift_end_time,shift_type.shift_start_time) as total_hours
                                from shift_management
                                join shift_type on shift_management.shift_id = shift_type.id
                                where shift_management.id = '$max_shift_data->id'
                                and shift_management.employee_id = '$employee->id'
                                ");

                                if (count($shift_management_query) > 0) {
                                    $shift_management_data = $shift_management_query[0];
                                    if ($shift_management_data->total_hours < 0) {
                                        $shift_management_data->total_hours;
                                        $paid_hours = $this->getTimeDiff($shift_management_data->shift_start_time, $shift_management_data->shift_end_time);
                                    } else {
                                        $paid_hours = $shift_management_data->total_hours;
                                    }
                                } else {
                                    $paid_hours = 8;
                                }
                            }else{
                                $paid_hours = 8;
                            }
                            $total_paid_hours = $total_paid_hours + (float)$paid_hours;
                        }
                    }
                }
            }
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }
        return $total_paid_hours;
    }

    public function internee_working_hours($employee, $payroll) {
        $employee_id = $employee->id;
        // $payroll_qry = DB::select("SELECT date_start,date_end,month(date_start) as payroll_month from payroll where id = '$payroll_id'");
        // $payroll = $payroll_qry[0];
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $payroll_month = $payroll->payroll_month;
        $total_working_hours = 0;
        $get_employee_id_query = DB::select("SELECT id,joined_date,confirmation_date,suspension_date,month(confirmation_date) as confirmation_month from employees where id = '$employee_id'");

        // $get_employee_id_object = $this->conn->query($get_employee_id_query);
        $employee_id_object = $get_employee_id_query[0];
        if (!empty($employee_id_object->suspension_date)) {
            if ($employee_id_object->suspension_date != '' AND $employee_id_object->suspension_date != '0000-00-00') {
                $end_date = $employee_id_object->suspension_date;
            }
		}
        if (isset($employee_id_object->confirmation_date) AND !empty($employee_id_object->confirmation_date)) {
            $confirmation_month = $employee_id_object->confirmation_month;
            $confirmation_date = $employee_id_object->confirmation_date;
            $today = date('m');
            if ($confirmation_month == $payroll_month) {
                $today_date = date('Y-m-d');
                $todate = date_create($today_date);
                while (strtotime($start_date) < strtotime($confirmation_date)) {
                    $attendance_qry = DB::select("SELECT * from attendance where employee = '$employee_id' and date(in_time) = '$start_date'");
                    // $attendance_qry_execute = $this->conn->query($attendance_qry);
                    if (count($attendance_qry) > 0) {
                        foreach ($attendance_qry as $attendance) {



                            $max_shift_data = DB::select("SELECT id FROM shift_management
                            WHERE '$start_date' between shift_from_date AND shift_to_date
                            AND employee_id = $employee->id ORDER BY id DESC LIMIT 1");


                            $shift_desc = 'Nil';
                            $shift_s_time = '09:00:00';
                            $shift_e_time = '09:00:00';
                            $late_t_allowed = 11;

                            if($max_shift_data AND !empty($max_shift_data)){
                                $max_shift_data = $max_shift_data[0];

                                $check_shift_query = DB::select("SELECT shift_management.id,shift_type.shift_desc,shift_type.shift_start_time,shift_type.shift_end_time,shift_management.late_time_allowed
                                FROM shift_management

                                join shift_type on shift_type.id = shift_management.shift_id
                                where employee_id = '$employee_id' AND status='Approved'

                                AND shift_management.id = '$max_shift_data->id'
                                ");

                                if(count($check_shift_query)>0){
                                    $check_shift_data = $check_shift_query[0];
                                    $shift_desc 	= $check_shift_data->shift_desc;
                                    $shift_s_time 	= $check_shift_data->shift_start_time;
                                    $shift_e_time 	= $check_shift_data->shift_end_time;
                                    $late_t_allowed =  $check_shift_data->late_time_allowed;
                                }
                            }

                            $shift_s_datetime = "$start_date " .$shift_s_time;
                            $shift_e_datetime = "$start_date " .$shift_e_time;
                            if(date('H', strtotime($shift_s_time)) > date('H', strtotime($shift_e_time))){
                                $shift_e_datetime = date('Y-m-d H:i:s', strtotime($shift_e_datetime . ' +1 day'));
                            }
                            if (!empty($attendance->out_time)) {
                                $emp_time_in = date_create($attendance->in_time);
                                $emp_time_out = date_create($attendance->out_time);
                                if (!empty($emp_time_out)) {
                                    if (date($attendance->out_time) >= date($attendance->in_time)) {
                                        if(strtotime($attendance->out_time) > strtotime($shift_e_datetime)){
                                            $emp_time_out = date_create($shift_e_datetime);
										}
                                        $diff = date_diff($emp_time_in, $emp_time_out);
                                        $days = $diff->d;
                                        $sec1 = $days * 24 * 3600;
                                        $hours = $diff->h;
                                        $sec2 = $hours * 3600;
                                        $minutes = $diff->i;
                                        $sec3 = $minutes * 60;
                                        $total_sec = $sec1 + $sec2 + $sec3;
                                        $working_hours = $total_sec / 3600;
                                        $total_working_hours = $total_working_hours + $working_hours;
                                    }
                                }
                            }
                        }
                    }
                    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                }
            }
            // $todate = date_create($today);
            // $diff=date_diff($joining_date,$todate);
            // return $diff->days;
            // // echo $diff->format("%R%a days");

        }
        return $total_working_hours;
    }

    public function internee_working_days($employee, $payroll) {
        $employee_id = $employee->id;
        // $payroll_qry = DB::select("SELECT date_start,date_end,month(date_start) as payroll_month,year(date_start) as payroll_year from payroll where id = '$payroll_id'");
        // $payroll_qry_execute = $this->conn->query($payroll_qry);
        // $payroll = $payroll_qry[0];
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        $tota_presents = 0;
        $get_employee_id_query = DB::select("SELECT id,joined_date,suspension_date,month(joined_date) as joined_month,job_title,confirmation_date,month(confirmation_date) as confirmation_month, year(joined_date) as joined_year from employees where id = '$employee_id'");
        // $get_employee_id_object = $this->conn->query($get_employee_id_query);
        $employee_id_object = $get_employee_id_query[0];
        if (!empty($employee_id_object->suspension_date)) {
            if ($employee_id_object->suspension_date != '' AND $employee_id_object->suspension_date != '0000-00-00') {
                $end_date = $employee_id_object->suspension_date;
            }
		}
        if (isset($employee_id_object->confirmation_date) AND !empty($employee_id_object->confirmation_date) AND $employee_id_object->confirmation_date != '0000-00-00') {
            $confirmation_month = $employee_id_object->confirmation_month;
            $confirmation_date = $employee_id_object->confirmation_date;
            $today = date('m');
            if ($confirmation_month == $payroll_month) {
                $confirmation_date1 = date_create($employee_id_object->confirmation_date);
                $payroll_start_date = date_create($start_date);
                $diff = date_diff($confirmation_date1, $payroll_start_date);
                return $diff->days;
            }
            // echo $diff->format("%R%a days");

        } else if (isset($employee_id_object->joined_date) AND !empty($employee_id_object->joined_date) AND $employee_id_object->joined_date != '0000-00-00') {
            $joined_month = $employee_id_object->joined_month;
            $joined_year = $employee_id_object->joined_year;
            $today = date('m');
            if ($joined_month == $payroll_month AND $joined_year == $payroll_year) {
                $internee_working_days_query = DB::select("SELECT id from attendance where employee = '$employee_id' and month(in_time) = '$payroll_month' and year(in_time) = '$payroll_year' GROUP BY DATE(in_time)");
                // $internee_working_days_query_result = $this->conn->query($internee_working_days_query);
                $tota_presents = count($internee_working_days_query);
                $start_date = $employee_id_object->joined_date;
                // $end_date = $employee_id_object->joined_date;
                while (strtotime($start_date) <= strtotime($end_date)) {
                    $off_days = [];

                    $shift_emp_query = DB::select("SELECT * from shift_management where employee_id = '$employee_id' and '$start_date' between shift_from_date and shift_to_date order by id desc limit 1");
                    // $shift_emp_query_result = $this->conn->query($shift_emp_query);
                    if (count($shift_emp_query) > 0) {
                        $shift_emp = $shift_emp_query[0];
                        $work_week_id = $shift_emp->work_week_id;
                        $work_days_query = DB::select("SELECT * from workdays where work_week_id = '$work_week_id' AND `status` = 'Non-working Day'");
                        // $work_days_query_result = $this->conn->query($work_days_query);
                        foreach ($work_days_query as $od) {
                            $off_days[] = $od->name;
                        }
                    } else {
                        $off_days[] = ['Sunday'];
                    }
                    $off_days[] = ['Sunday'];

                    $current_day_name = date('l', strtotime("$start_date"));
                    if (in_array($current_day_name, $off_days)) {
                        $check_att_query = DB::select("SELECT id from attendance where employee = '$employee_id' AND date(in_time) = '$start_date'");
                        // $check_att_query_result = $this->conn->query($check_att_query);
                        if (count($check_att_query) == 0) {
                            $tota_presents = $tota_presents + 1;
                        }
                    }
                    $off_days = [];
                    $leaves_query = DB::select("SELECT id from employeeleaves where employee = '$employee_id' AND `status` = 'Approved' AND ('$start_date' BETWEEN date_start and	date_end)");
                    // $leaves_query_result = $this->conn->query($leaves_query);
                    if (count($leaves_query) > 0) {
                        $check_att_query = DB::select("SELECT id from attendance where employee = '$employee_id' AND date(in_time) = '$start_date'");
                        // $check_att_query_result = $this->conn->query($check_att_query);
                        if (count($check_att_query) == 0) {
                            $tota_presents = $tota_presents + 1;
                        }
                    }
                    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                }
                return $tota_presents;
            } else if ($employee_id_object->job_title == 32 OR $employee_id_object->job_title == 41) {
                $internee_working_days_query = DB::select("SELECT id from attendance where employee = '$employee_id' and month(in_time) = '$payroll_month' and year(in_time) = '$payroll_year' GROUP BY DATE(in_time)");
                // $internee_working_days_query_result = $this->conn->query($internee_working_days_query);
                $tota_presents = count($internee_working_days_query);
                // $start_date = $employee_id_object->joined_date;
                while (strtotime($start_date) <= strtotime($end_date)) {
                    $off_days = [];

                    $leaves_query = DB::select("SELECT id from employeeleaves where employee = '$employee_id' AND `status` = 'Approved' AND ('$start_date' BETWEEN date_start and	date_end)");
                    // $leaves_query_result = $this->conn->query($leaves_query);
                    if (count($leaves_query) > 0) {
                        $check_att_query = DB::select("SELECT id from attendance where employee = '$employee_id' AND date(in_time) = '$start_date'");
                        // $check_att_query_result = $this->conn->query($check_att_query);
                        if (count($check_att_query) == 0) {
                            $tota_presents = $tota_presents + 1;
                        }
                    }else{
                        $shift_emp_query = DB::select("SELECT * from shift_management where employee_id = '$employee_id' and '$start_date' between shift_from_date and shift_to_date order by id desc limit 1");
                        // $shift_emp_query_result = $this->conn->query($shift_emp_query);
                        if (count($shift_emp_query) > 0) {
                            $shift_emp = $shift_emp_query[0];
                            $work_week_id = $shift_emp->work_week_id;
                            $work_days_query = DB::select("SELECT * from workdays where work_week_id = '$work_week_id' AND `status` = 'Non-working Day'");
                            // $work_days_query_result = $this->conn->query($work_days_query);
                            foreach ($work_days_query as $od) {
                                $off_days[] = $od->name;
                            }
                        } else {
                            $off_days = ['0' => 'Sunday'];
                        }
                        $current_day_name = date('l', strtotime("$start_date"));
                        if (in_array($current_day_name, $off_days)) {
                            $check_att_query = DB::select("SELECT id from attendance where employee = '$employee_id' AND date(in_time) = '$start_date'");
                            // $check_att_query_result = $this->conn->query($check_att_query);
                            if (count($check_att_query) == 0) {
                                $tota_presents = $tota_presents + 1;
                            }
                        }
                        $off_days = [];
                    }
                    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                }
                return $tota_presents;
            }
        } else if ($employee_id_object->job_title == 32 OR $employee_id_object->job_title == 41) {
            $internee_working_days_query = DB::select("SELECT id from attendance where employee = '$employee_id' and month(in_time) = '$payroll_month' and year(in_time) = '$payroll_year' GROUP BY DATE(in_time)");
            // $internee_working_days_query_result = $this->conn->query($internee_working_days_query);
            $tota_presents = count($internee_working_days_query);
            $start_date = $employee_id_object->joined_date;
            while (strtotime($start_date) <= strtotime($end_date)) {
                $off_days = [];

                $shift_emp_query = DB::select("SELECT * from shift_management where employee_id = '$employee_id' and '$start_date' between shift_from_date and shift_to_date order by id desc limit 1");
                // $shift_emp_query_result = $this->conn->query($shift_emp_query);
                if (count($shift_emp_query) > 0) {
                    $shift_emp = $shift_emp_query[0];
                    $work_week_id = $shift_emp->work_week_id;
                    $work_days_query = DB::select("SELECT * from workdays where work_week_id = '$work_week_id' AND `status` = 'Non-working Day'");
                    // $work_days_query_result = $this->conn->query($work_days_query);
                    foreach ($work_days_query as $od) {
                        $off_days[] = $od->name;
                    }
                } else {
                    $off_days = ['0' => 'Sunday'];
                }
                $current_day_name = date('l', strtotime("$start_date"));
                if (in_array($current_day_name, $off_days)) {
                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee_id' AND date(in_time) = '$start_date'");
                    // $check_att_query_result = $this->conn->query($check_att_query);
                    if (count($check_att_query) == 0) {
                        $tota_presents = $tota_presents + 1;
                    }
                }
                $off_days = [];
                $leaves_query = DB::select("SELECT id from employeeleaves where employee = '$employee_id' AND `status` = 'Approved' AND ('$start_date' BETWEEN date_start and	date_end)");
                // $leaves_query_result = $this->conn->query($leaves_query);
                if (count($leaves_query) > 0) {
                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee_id' AND date(in_time) = '$start_date'");
                    // $check_att_query_result = $this->conn->query($check_att_query);
                    if (count($check_att_query) == 0) {
                        $tota_presents = $tota_presents + 1;
                    }
                }
                $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }
            return $tota_presents;
        } else {
            return 0.00;
        }
        return $tota_presents;
    }

    public function absent_hours($employee, $payroll){
        $employee_id = $employee->id;
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $payroll_month = $payroll->payroll_month;
		$payroll_year = $payroll->payroll_year;

        $suspension_date_query = DB::select("SELECT suspension_date,joined_date,month(joined_date) as joined_month,year(joined_date) as joined_year from employees where id = '$employee_id'");
        $suspension_date_object = $suspension_date_query[0];
		$suspension_date = $suspension_date_object->suspension_date;
		$joined_date = $suspension_date_object->joined_date;
		$joined_month = $suspension_date_object->joined_month;
		$joined_year = $suspension_date_object->joined_year;

        \Log::debug('employee',[json_encode($employee)]);

        if (!empty($suspension_date)) {
            if ($suspension_date != '' AND $suspension_date != '0000-00-00') {
                $end_date = $suspension_date;
            }
		}
		if($payroll_month == $joined_month AND $payroll_year == $joined_year){
			$start_date = $joined_date;
		}

        $total_absents = 0;
        while (strtotime($start_date) <= strtotime($end_date)) {

            if(!empty($employee->restore_date) && $employee->restore_date != '0000-00-00'){
                if(strtotime($start_date) < strtotime($employee->restore_date)){
                    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                    continue;
                }
            }
            $attendance_qry = DB::select("SELECT * from attendance where employee = '$employee_id' and date(in_time) = '$start_date' limit 1");
            // dd($attendance_qry);
            if (count($attendance_qry) < 1) {
                $empLeaveStatusQry = DB::select("SELECT * FROM employeeleaves where employee = '$employee_id' AND date_start <= '$start_date' AND date_end >= '$start_date'  AND status = 'Approved'");
                if (count($empLeaveStatusQry) > 0) {

                } else {
                    $max_shift_data = DB::select("SELECT id FROM shift_management
                    WHERE '$start_date' between shift_from_date AND shift_to_date
                    AND employee_id = $employee->id AND status = 'Approved' AND deleted_at IS NULL ORDER BY id DESC LIMIT 1");
                    // dd($max_shift_data);

                    if($max_shift_data AND !empty($max_shift_data)){
                        // dd('hit');
                        $max_shift_data = $max_shift_data[0];
                        $employeeOffDayQry = DB::select("SELECT shift_management.id,workdays.name as off_days,workdays.`status`,shift_type.shift_desc
                        FROM shift_management
                        join workdays on workdays.work_week_id = shift_management.work_week_id
                        join shift_type on shift_type.id = shift_management.shift_id
                        where employee_id = '$employee_id'

                        AND shift_management.id = '$max_shift_data->id'
                        AND workdays.`status` = 'Non-working Day'
                        ");

                        if (count($employeeOffDayQry) > 0) {
                            foreach ($employeeOffDayQry as $employeeOffDayTest) {
                                $dayName[] = $employeeOffDayTest->off_days;
                            }
                        } else {
                            $dayName[] = 'Sunday';
                        }
                    }else{
                        // dd('hit2');
                        $dayName[] = 'Sunday';
                    }
                    // dd('hit2');

                    $current_day_name = date('l', strtotime("$start_date"));
                    if (!in_array($current_day_name, $dayName)) {



                        $max_shift_data = DB::select("SELECT id FROM shift_management
                        WHERE '$start_date' between shift_from_date AND shift_to_date
                        AND employee_id = $employee->id ORDER BY id DESC LIMIT 1");

                        $shift_s_datetime = $start_date . ' 09:00:00';
                        $shift_e_datetime = $start_date . ' 05:00:00';
                        $late_t_allowed   = 11;
                        // if($max_shift_data->isEmpty()){
                        //     return 'empty';
                        // }else{
                        //     return 'not empty';
                        // }
                        // $test_array2[$employee->id] = [$max_shift_data->id,$start_date];
                        // print_r($test_array2);
                        // $test_array[$employee->id] = [$max_shift_data,$start_date];
                        // print_r($test_array);
                        if($max_shift_data AND !empty($max_shift_data)){
                            $max_shift_data = $max_shift_data[0];

                            $shift_query = DB::select("SELECT shift_management.id,shift_type.shift_desc,shift_type.shift_start_time,shift_type.shift_end_time,shift_management.late_time_allowed
                            FROM shift_management

                            join shift_type on shift_type.id = shift_management.shift_id
                            where employee_id = '$employee_id'

                            AND shift_management.id = '$max_shift_data->id'");

                            if (count($shift_query) > 0) {
                                $shift = $shift_query[0];
                                $shift_s_datetime = $start_date . ' ' . $shift->shift_start_time;
                                $shift_e_datetime = $start_date . ' ' . $shift->shift_end_time;
                                $late_t_allowed   = $shift->late_time_allowed;
                                if (date('H', strtotime($shift->shift_start_time)) > date('H', strtotime($shift->shift_end_time))) {
                                    $shift_e_datetime = date('Y-m-d H:i:s', strtotime($shift_e_datetime . ' +1 day'));
                                }
                            }
                        }

                        $sh_st_time = date_create($shift_s_datetime);
                        $sh_end_time = date_create($shift_e_datetime);
                        $sh_diff = date_diff($sh_st_time, $sh_end_time);
                        if ($sh_diff->invert == 0) {
                            $days = $sh_diff->d;
                            $sec1 = $days * 24 * 3600;
                            $hours = $sh_diff->h;
                            $sec2 = $hours * 3600;
                            $minutes = $sh_diff->i;
                            $sec3 = $minutes * 60;
                            $total_sec = $sec1 + $sec2 + $sec3;
                            $working_hours = $total_sec / 3600;
                        } else {
                            $working_hours = '0.00';
                            $hours = 8;

                        }
                        $total_absents = $total_absents+$hours * 1;
                    }
                    $dayName = [];

                }
            }
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }
        return $total_absents;
    }

    public function loan_deduction($employee, $payroll)
    {
        $employee_id = $employee->id;
        // $payroll_qry = "SELECT date_start,date_end,month(date_start) as payroll_month,year(date_start) as payroll_year from payroll where id = '$payroll_id'";
        // $payroll_qry_execute = $this->conn->query($payroll_qry);
        // $payroll = $payroll_qry_execute->fetch_object();
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;

        $total_amount = 0;
        $loan_types = LoanType::pluck('id')->toArray();
        if(env('COMPANY') == 'Ajmal Dawakhana' OR env('COMPANY') == 'RoofLine' OR env('COMPANY') == 'Prince HRM' OR env('COMPANY') == 'UNICORN' OR env('COMPANY') == 'JSML' OR env('COMPANY') == 'HEALTHWISE')
        {
            $loan_types = 3;
        }
        else
        {
            $loan_types = implode(',', $loan_types);
        }

        if(env('COMPANY') == 'RoofLine')
        {
            $lr_sql = DB::select("SELECT loan_installments.amount,loan_installments.id,loan_requests.id as loan_id,loan_requests.remaining_amount,loan_installments.status FROM loan_requests
            JOIN loan_installments ON loan_installments.loan_request_id = loan_requests.id
            WHERE loan_requests.employee_id='$employee_id' AND loan_requests.STATUS='Approved' AND loan_requests.deduction_group = '$payroll->deduction_group' AND loan_requests.loan_type IN ($loan_types)
            AND loan_installments.status != 'cancelled' AND loan_installments.month BETWEEN '$start_date' AND '$end_date'");
        }
        else
        {
            $lr_sql = DB::select("SELECT loan_installments.amount,loan_installments.id,loan_requests.id as loan_id,loan_requests.remaining_amount,loan_installments.status FROM loan_requests
            JOIN loan_installments ON loan_installments.loan_request_id = loan_requests.id
            WHERE loan_requests.employee_id='$employee_id' AND loan_requests.STATUS='Approved' AND loan_requests.loan_type IN ($loan_types)
            AND loan_installments.status != 'cancelled' AND loan_installments.month BETWEEN '$start_date' AND '$end_date'");
        }
       // New Loan
        // $lr_result = $this->conn->query($lr_sql);
        if(count($lr_sql) > 0){
            // while($loan_run = $lr_sql)
            // return count($lr_sql);
            foreach($lr_sql as $loan_run){
                // return $loan_run->id;
                if($loan_run->status == 'active'){
                    //Remaining amount calcution
                    $remaining_amount=$loan_run->remaining_amount-$loan_run->amount;
                    $sqlloan = DB::select("UPDATE loan_requests SET remaining_amount='$remaining_amount' WHERE id='$loan_run->loan_id'");
                    // $this->conn->query($sqlloan);

                    $sqlloan = DB::select("UPDATE loan_installments SET status='deducted',system_deducted='1' WHERE id='$loan_run->id'");
                    // $this->conn->query($sqlloan);
                    }

                // return $loan_run->amount;
                $total_amount = $total_amount + $loan_run->amount;
            }
            // $loan_run = $lr_sql[0];
            return $total_amount;

        }else{
            return 0;
        }
    }

    public function fairPriceShopDeducation($employee, $payroll)
    {
        $employee_id = $employee->id;
        // $payroll_qry = "SELECT date_start,date_end,month(date_start) as payroll_month,year(date_start) as payroll_year from payroll where id = '$payroll_id'";
        // $payroll_qry_execute = $this->conn->query($payroll_qry);
        // $payroll = $payroll_qry_execute->fetch_object();
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;

        $total_amount = 0;
        // $loan_types = LoanType::where('name', 'Fair Price Shop')->pluck('id')->toArray();
        $loan_types = 5;
        // if(env('COMPANY') == 'Ajmal Dawakhana' OR env('COMPANY') == 'RoofLine' OR env('COMPANY') == 'Prince HRM')
        // {
        //     $loan_types = 3;
        // }
        // else
        // {
        //     $loan_types = implode(',', $loan_types);
        // }
            $lr_sql = DB::select("SELECT loan_installments.amount,loan_installments.id,loan_requests.id as loan_id,loan_requests.remaining_amount,loan_installments.status FROM loan_requests
            JOIN loan_installments ON loan_installments.loan_request_id = loan_requests.id
            WHERE loan_requests.employee_id='$employee_id' AND loan_requests.STATUS='Approved' AND loan_requests.loan_type IN ($loan_types)
            AND loan_installments.status != 'cancelled' AND loan_installments.month BETWEEN '$start_date' AND '$end_date'");
       // New Loan
        // $lr_result = $this->conn->query($lr_sql);
        if(count($lr_sql) > 0){
            // while($loan_run = $lr_sql)
            // return count($lr_sql);
            foreach($lr_sql as $loan_run){
                // return $loan_run->id;
                if($loan_run->status == 'active'){
                    //Remaining amount calcution
                    $remaining_amount=$loan_run->remaining_amount-$loan_run->amount;
                    $sqlloan = DB::select("UPDATE loan_requests SET remaining_amount='$remaining_amount' WHERE id='$loan_run->loan_id'");
                    // $this->conn->query($sqlloan);

                    $sqlloan = DB::select("UPDATE loan_installments SET loan_installments.status='deducted',system_deducted='1' WHERE id='$loan_run->id'");
                    // $this->conn->query($sqlloan);
                    }

                // return $loan_run->amount;
                $total_amount = $total_amount + $loan_run->amount;
            }
            // $loan_run = $lr_sql[0];
            return $total_amount;

        }else{
            return 0;
        }
    }

    public function advance_loan($employee, $payroll)
    {
        $employee_id = $employee->id;
        // $payroll_qry = "SELECT date_start,date_end,month(date_start) as payroll_month,year(date_start) as payroll_year from payroll where id = '$payroll_id'";
        // $payroll_qry_execute = $this->conn->query($payroll_qry);
        // $payroll = $payroll_qry_execute->fetch_object();
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;

        $total_amount = 0;
       // New Loan
        if(env('COMPANY') == 'RoofLine')
        {
            $lr_sql = DB::select("SELECT loan_installments.amount,loan_installments.id,loan_requests.id as loan_id,loan_requests.remaining_amount,loan_installments.status FROM loan_requests
            JOIN loan_installments ON loan_installments.loan_request_id = loan_requests.id
            WHERE loan_requests.employee_id='$employee_id' AND loan_requests.STATUS='Approved' AND loan_requests.deduction_group = '$payroll->deduction_group' AND loan_requests.loan_type='4'
            AND loan_installments.status != 'cancelled' AND loan_installments.month BETWEEN '$start_date' AND '$end_date'");
        }
        else
        {
            $lr_sql = DB::select("SELECT loan_installments.amount,loan_installments.id,loan_requests.id as loan_id,loan_requests.remaining_amount,loan_installments.status FROM loan_requests
            JOIN loan_installments ON loan_installments.loan_request_id = loan_requests.id
            WHERE loan_requests.employee_id='$employee_id' AND loan_requests.STATUS='Approved' AND loan_requests.loan_type='4'
            AND loan_installments.status != 'cancelled' AND loan_installments.month BETWEEN '$start_date' AND '$end_date'");
        }
        // $lr_result = $this->conn->query($lr_sql);
        if(count($lr_sql) > 0){
            // while($loan_run = $lr_sql)
            // return count($lr_sql);
            foreach($lr_sql as $loan_run){
                // return $loan_run->id;
                if($loan_run->status == 'active'){
                    //Remaining amount calcution
                    $remaining_amount=$loan_run->remaining_amount-$loan_run->amount;
                    $sqlloan = DB::select("UPDATE loan_requests SET remaining_amount='$remaining_amount' WHERE id='$loan_run->loan_id'");
                    // $this->conn->query($sqlloan);

                    $sqlloan = DB::select("UPDATE loan_installments SET status='deducted',system_deducted='1' WHERE id='$loan_run->id'");
                    // $this->conn->query($sqlloan);
                    }

                // return $loan_run->amount;
                $total_amount = $total_amount + $loan_run->amount;
            }
            // $loan_run = $lr_sql[0];
             return $total_amount;

        }else{
            return 0;
        }

    }

    function total_payroll_days($employee, $payroll) {
        // $payroll_qry = DB::select("SELECT date_start,date_end,month(date_start) as payroll_month,year(date_start) as payroll_year from payroll where id = '$payroll_id'");
        // $payroll_qry_execute = $this->conn->query($payroll_qry);
        // $payroll = $payroll_qry_execute->fetch_object();
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        $start_date1 = date_create($start_date);
        $end_date1 = date_create($end_date);
        $diff = date_diff($start_date1, $end_date1);
        $days = $diff->days;
        return $days + 1;
    }


    function mobile_usage_deduction($employee, $payroll) {
        // $payroll_qry = "SELECT month(date_start) as `month`, year(date_start) as `year` from payroll where id = '$payroll_id'";
        // $payroll_qry_execute = $this->conn->query($payroll_qry);
        // $payroll = $payroll_qry_execute->fetch_object();
        $month = $payroll->payroll_month;
        $year = $payroll->payroll_year;
        $amount_query = DB::select("SELECT amount from employee_payments
												where amount_month = '$month'
												AND payment_type = 2
												AND payment_code = 2
												AND amount_year = '$year' AND employee_id = '$employee->id'");
        // $amount_query_result = $this->conn->query($amount_query);
        // $amounts_data = $amount_query_result->fetch_object();
        if($amount_query AND !empty($amount_query)){
            $amounts_data = $amount_query[0];
            return $amounts_data->amount;
        }
        else {
            return 0.00;
        }
    }


    function calculate_arrears($employee, $payroll) {
        // $payroll_qry = "SELECT month(date_start) as `month`, year(date_start) as `year` from payroll where id = '$payroll_id'";
        // $payroll_qry_execute = $this->conn->query($payroll_qry);
        // $payroll = $payroll_qry_execute->fetch_object();
        $month = $payroll->payroll_month;
        $year = $payroll->payroll_year;
        $amount_query = DB::select("SELECT amount from employee_payments
												where amount_month = '$month'
												AND payment_type = 2
												AND payment_code = 42
												AND amount_year = '$year' AND employee_id = '$employee->id'");
        // $amount_query_result = $this->conn->query($amount_query);
        // $amounts_data = $amount_query_result->fetch_object();
        if($amount_query AND !empty($amount_query)){
            $amounts_data = $amount_query[0];
            return $amounts_data->amount;
        } else {
            return 0.00;
        }
    }

    function getTimeDiff($dtime, $atime) {
        $nextDay = $dtime > $atime ? 1 : 0;
        $dep = EXPLODE(':', $dtime);
        $arr = EXPLODE(':', $atime);
        $diff = ABS(MKTIME($dep[0], $dep[1], 0, DATE('n'), DATE('j'), DATE('y')) - MKTIME($arr[0], $arr[1], 0, DATE('n'), DATE('j') + $nextDay, DATE('y')));
        $hours = FLOOR($diff / (60 * 60));
        $mins = FLOOR(($diff - ($hours * 60 * 60)) / (60));
        $secs = FLOOR(($diff - (($hours * 60 * 60) + ($mins * 60))));
        IF (STRLEN($hours) < 2) {
            $hours = "0" . $hours;
        }
        IF (STRLEN($mins) < 2) {
            $mins = "0" . $mins;
        }
        IF (STRLEN($secs) < 2) {
            $secs = "0" . $secs;
        }
        return $hours;
    }

    public function total_working_days_with_off_and_leaves_new($employee,$payroll)
    {
        $total_working_days = 0;
        $start_date = $payroll->date_start;
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        $month_days = Carbon::now()->month($payroll_month)->daysInMonth;
        $end_date = $payroll->date_end;
        $joined_date                        = $employee->joined_date;
		$joined_month                       = date('m', strtotime($employee->joined_date));
        $joined_year                        = date('Y', strtotime($employee->joined_date));
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        if($payroll_month == $joined_month AND $payroll_year == $joined_year){
			$start_date = $joined_date;
		}
        $designation = JobTitle::find($employee->job_title);
        if(!empty($designation)){
            if($designation->exempt_working_days == 1){
                return $month_days;
            }
        }
        if($employee->employment_status == 17){
            return $month_days;
        }
        $termination_date = Employee::where('id',$employee->id)->value('termination_date');
        if (!empty($termination_date) && $termination_date != '0000-00-00') {
            $end_date = $termination_date;
        }
        if (env('COMPANY') == 'UNICORN'){
            $unicornDoubleAttendanceShifts = getUnicornDoubleAttendanceShifts()->pluck('id')->toArray();
        }
        else{
            $unicornDoubleAttendanceShifts = [];
        }
        for ($date = $start_date; $date <= $end_date; $date++) {
            $shift_data = DB::table('shift_management')->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
                ->join('work_week', 'work_week.id', '=', 'shift_management.work_week_id')
                ->where([
                    ['shift_management.employee_id', $employee->id],
                    ['shift_management.shift_from_date', '<=', $date],
                    ['shift_management.shift_to_date', '>=', $date],
                    ['shift_management.status', '=', 'Approved'],
                    ['shift_management.deleted_at', '=', null],
                ])
                ->select('shift_management.id','shift_management.work_week_id', 'shift_management.shift_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'shift_type.sandwich_absent', 'work_week.desc as work_week', 'shift_management.late_time_allowed', 'open_working_hours','shift_type.is_24_hour_shift')
                ->orderBy('shift_management.id', 'DESC')
                ->first();
            if (!empty($shift_data)) {
                $work_week_id = $shift_data->work_week_id;
                $shift_start_time = $shift_data->shift_start_time;
                $shift_end_time = $shift_data->shift_end_time;
                $late_time_allowed = $shift_data->late_time_allowed;
                $sandwich_absent = $shift_data->sandwich_absent;
            } else {
                $shift_start_time = '09:00:00';
                $shift_end_time = '17:00:00';
                $late_time_allowed = 11;
                $work_week_id = 1;
                $sandwich_absent = 0;
            }
            $shift_start_datetime = "$date " . $shift_start_time;
            $shift_end_datetime = "$date " . $shift_end_time;
            $night_shift = false;
            if (date('H', strtotime($shift_start_time)) > date('H', strtotime($shift_end_time))) {
                $night_shift = true;
                $shift_end_datetime = date('Y-m-d H:i:s', strtotime($shift_end_datetime . ' +1 day'));
            }
            $attendance_date = $date;
            if (env('COMPANY') == 'JSML' && (date('H:i', strtotime($shift_start_time)) == '00:00' || date('H:i', strtotime($shift_start_time)) == '00:01')) {
                $attendance_date2 = date('Y-m-d', strtotime($date . ' -1 day'));
                // $shift_start_datetime = date('Y-m-d', strtotime($shift_start_datetime . ' -1 day'));
                $currentDayattendance = DB::table('attendance')
                    ->where('employee', $employee->id)
                    ->whereDate('in_time', "$attendance_date")
                    ->whereRaw('TIME(in_time) < ?', ['23:00:00'])
                    ->first();
                $previousDayAttendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$attendance_date2")->whereRaw('TIME(in_time) > ?', ['23:00:00'])->first();
                $attendance = null;
                if (!empty($currentDayattendance)) {
                    $attendance = $currentDayattendance;
                }
                if (!empty($previousDayAttendance)) {
                    $attendance = $previousDayAttendance;
                }
            } else {
                if (env('COMPANY') == 'JSML') {
                    $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$attendance_date")->whereRaw('TIME(in_time) < ?', ['23:00:00'])->orderBy('id', 'ASC')->first();
                } else {
                    if (env('COMPANY') == 'UNICORN' && $shift_data && in_array($shift_data->shift_id, $unicornDoubleAttendanceShifts)) {
                        $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$attendance_date")->orderBy('id', 'ASC')->get();
                    }
                    else{
                        $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$attendance_date")->orderBy('id', 'ASC')->first();
                    }
                }
            }
            if(!empty($attendance)){
                if (!empty($attendance->in_time) && !empty($attendance->out_time)) {
                    $attend_time_in = $attendance->in_time;
                    $attend_time_out = $attendance->out_time;
                    $e_grace_period = date("Y-m-d H:i:s", strtotime("+$late_time_allowed minutes", strtotime($shift_start_datetime)));
                    // because JSML want to calculate hours according to in time
                    if (strtotime($attend_time_in) < strtotime($e_grace_period) && env('COMPANY') != 'JSML') {
                        $attend_time_in = $shift_start_datetime;
                    }
                    // because JSML want to calculate hours according to out time
                    if (strtotime($attend_time_out) > strtotime($shift_end_datetime) && env('COMPANY') != 'JSML') {
                        $attend_time_out = $shift_end_datetime;
                    }
                    $working_hours = Carbon::parse($attend_time_in)->floatDiffInHours($attend_time_out);
                    $shift_working_hours = Carbon::parse($shift_start_datetime)->floatDiffInHours($shift_end_datetime);
                    if(!empty($shift_data) && $shift_data->is_24_hour_shift == 1){
                        $shift_working_hours = 8;
                    }
                    // if(env('COMPANY') == 'HEALTHWISE'){
                    //     $work_week_days = DB::table('workdays')->where([
                    //         ['work_week_id', $work_week_id],
                    //         ['status', '=', 'Non-working Day'],
                    //     ])->pluck('name')->toArray();
                    //     if (in_array(date('l', strtotime("$date")), $work_week_days)) {
                    //         $working_hours = 0;
                    //     }
                    // }
                    if(env('COMPANY') == 'JSML'){
                        $carbonDate = Carbon::parse($date);
                        $shift_working_hours = $shift_working_hours - 0.5;
                        if($working_hours == ($shift_working_hours / 2) && !empty($attendance->ret)){
                            $total_working_days += 0.5;
                        }
                        // Exclude Half Hour From Shift Time
                        elseif (($working_hours >= ($shift_working_hours / 2) && $working_hours < $shift_working_hours) && (!empty($shift_data) && in_array($shift_data->shift_id, [4, 5, 6]) || (!$carbonDate->isFriday()))) {
                            $total_working_days += 0.5;
                        }
                        else{
                            $total_working_days = checkHalfLeave($employee->id, $date) ? $total_working_days + 0.5 : ($working_hours >= ($shift_working_hours / 2) ? $total_working_days + 1 : $total_working_days+0);
                        }
                    }
                    else{
                        $total_working_days += 1;
                    }
                }
                else{
                    $total_working_days += 1;
                }
            }
            else{
                $employee_leave = DB::table('employeeleaves')->where([['employee', $employee->id],['date_start', '<=', "$date"],['date_end', '>=', "$date"],['status', '=', 'Approved']])->first();
                if(!empty($employee_leave)){
                    $total_working_days = isset($employee_leave->is_half) && $employee_leave->is_half == 1 ? $total_working_days+0.5 : $total_working_days + 1;
                }
                else{
                    $work_week_days = DB::table('workdays')->where([
                        ['work_week_id', $work_week_id],
                        ['status', '=', 'Non-working Day'],
                    ])->pluck('name')->toArray();
                    $previous_date = date('Y-m-d', strtotime('' . $date . ' -1 day'));
                    $next_date = date('Y-m-d', strtotime('' . $date . ' +1 day'));
                    if (!empty($shift_data) && $shift_data->sandwich_absent == 1) {
                        // Previous Attendance
                        $previous_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$previous_date")->first(['id']);
                        // Next Attendance
                        $next_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$next_date")->first(['id']);
                        // Previous leave
                        $previousLeave = DB::table('employeeleaves')
                            ->where([
                                ['employee', $employee->id],
                                ['date_start', '<=', "$previous_date"],
                                ['date_end', '>=', "$previous_date"],
                                ['status', '=', 'Approved'],
                            ])->value('id');
                        // Next leave
                        $nextLeave = DB::table('employeeleaves')
                            ->where([
                                ['employee', $employee->id],
                                ['date_start', '<=', "$next_date"],
                                ['date_end', '>=', "$next_date"],
                                ['status', '=', 'Approved'],
                            ])->value('id');
                        // previous day holiday
                        $previous_holiday = DB::table('holidays')->where('dateh', "$previous_date")->first();
                        $previousHolidaySetting = holidaySettingForEmployee($employee, $previous_holiday);
                        // next day holiday
                        $next_holiday = DB::table('holidays')->where('dateh', "$next_date")->first();
                        $nextHolidaySetting = holidaySettingForEmployee($employee, $next_holiday);

                        $sanwichAbsentCheck = !empty($previousLeave) || !empty($nextLeave) || !empty($previous_attendance) || !empty($next_attendance) || (!empty($previous_holiday) && $previousHolidaySetting) || (!empty($next_holiday) && $nextHolidaySetting);
                    }
                    if (in_array(date('l', strtotime("$date")), $work_week_days)) {
                        if (!empty($shift_data) && $shift_data->sandwich_absent == 1) {
                            if($sanwichAbsentCheck){
                                $total_working_days = (isset($previousLeave->is_half) && $previousLeave->is_half == 1) || (isset($nextLeave->is_half) && $nextLeave->is_half == 1) ? $total_working_days + 0.5 : $total_working_days + 1;
                            }
                            else{
                                if($this->checkHoliday($date,$employee->id,$employee) == 'yes'){
                                    $total_working_days = $total_working_days + 1;
                                }
                                else{
                                    if($start_date == $end_date){
                                        if($this->checkIfLastDayIsOff($date,$employee->id) == 'yes'){
                                            $total_working_days = $total_working_days + 1;
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            $total_working_days = $total_working_days + 1;
                        }
                    }
                    else{
                        if($this->checkHoliday($date,$employee->id,$employee) == 'yes'){
                            if (($sandwich_absent == 1 && $sanwichAbsentCheck) || ($sandwich_absent == 0)) {
                                $total_working_days = $total_working_days + 1;
                            }
                            // else{
                            //     if($date == '2024-05-28'){
                            //         dd($date);
                            //     }
                            //     $total_working_days = $total_working_days + 1;
                            // }
                        }
                    }
                }
            }
        }
        return $total_working_days;
    }

    public function total_working_days_with_off_and_leaves($employee,$payroll){
        $start_date = $payroll->date_start;
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        $month_days = Carbon::now()->month($payroll_month)->daysInMonth;
        if(env('COMPANY') == 'RoofLine')
        {
            $end_date = $payroll_year.'-'.$payroll_month.'-25';
        }
        else
        {
            $end_date = $payroll->date_end;
        }
        $joined_date                        = $employee->joined_date;
		$joined_month                       = date('m', strtotime($employee->joined_date));
        $joined_year                        = date('Y', strtotime($employee->joined_date));
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        if($payroll_month == $joined_month AND $payroll_year == $joined_year){
			$start_date = $joined_date;
		}
        $total_working_days = 0;
        $designation = JobTitle::find($employee->job_title);
        if(!empty($designation))
        {
            if($designation->exempt_working_days == 1){
                return $month_days;
            }
        }
        if($employee->employment_status == 17){
            return $month_days;
        }
        $termination_date_query = DB::select("SELECT termination_date from employees where id = '$employee->id'");
        $termination_date_object = $termination_date_query[0];

        $termination_date = $termination_date_object->termination_date;
        if ($termination_date == '0000-00-00' OR $termination_date == null OR empty($termination_date)) {
            while (strtotime($start_date) <= strtotime($end_date)) {
                $attendance = null;
                $shift_data = DB::table('shift_management')->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
                    ->join('work_week', 'work_week.id', '=', 'shift_management.work_week_id')
                    ->where([
                        ['shift_management.employee_id', $employee->id],
                        ['shift_management.shift_from_date', '<=', $start_date],
                        ['shift_management.shift_to_date', '>=', $start_date],
                        ['shift_management.status', '=', 'Approved'],
                    ])
                    ->select('shift_management.shift_id','shift_type.shift_start_time','shift_type.shift_end_time')
                    ->orderBy('shift_management.id', 'DESC')
                    ->first();
                if(env('COMPANY') == 'JSML'){
                    if($shift_data && ($shift_data->shift_id == 6 || $shift_data->shift_id == 10)){
                        $attendance_date2 = date('Y-m-d', strtotime($start_date . ' -1 day'));
                        // $currentDayattendance = DB::table('attendance')
                        //     ->where('employee', $employee->id)
                        //     ->whereDate('in_time', "$start_date")
                        //     ->first();
                        $currentDayattendance = DB::table('attendance')
                        ->where('employee', $employee->id)
                        ->whereDate('in_time', "$start_date")
                        ->whereRaw('TIME(in_time) < ?', ['23:00:00'])
                        ->first();
                        $previousDayAttendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$attendance_date2")->whereRaw('TIME(in_time) > ?', ['23:00:00'])->first();
                        if (!empty($currentDayattendance)) {
                            $attendance = $currentDayattendance;
                        }
                        if (!empty($previousDayAttendance)) {
                            $attendance = $previousDayAttendance;
                        }
                    }
                    else{
                        $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$start_date")->whereRaw('TIME(in_time) < ?', ['23:00:00'])->orderBy('id', 'ASC')->first();
                    }
                }
                else{
                    $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$start_date")->orderBy('id', 'ASC')->first();
                }
                if (!empty($attendance)) {
                    if(env('COMPANY') == 'JSML'){
                        $shift_start_datetime = "$start_date " . $shift_data->shift_start_time;
                        $shift_end_datetime = "$start_date " . $shift_data->shift_end_time;
                        if (date('H', strtotime($shift_data->shift_start_time)) > date('H', strtotime($shift_data->shift_end_time))) {
                            $shift_end_datetime = date('Y-m-d H:i:s', strtotime($shift_end_datetime . ' +1 day'));
                        }
                        $working_hours = Carbon::parse($attendance->in_time)->floatDiffInHours($attendance->out_time);
                        $shift_working_hours = Carbon::parse($shift_start_datetime)->floatDiffInHours($shift_end_datetime);
                        $carbonDate = Carbon::parse($start_date);
                        if($working_hours == 4 && !empty($attendance->ret)){
                            $total_working_days += 0.5;
                        }
                        // Exclude Half Hour From Shift Time
                        else if($working_hours >= ($shift_working_hours / 2) && $working_hours < ($shift_working_hours - 0.5) && !$carbonDate->isFriday()){
                            $total_working_days += 0.5;
                        }
                        else{
                            if(checkHalfLeave($employee->id, $start_date)){
                                $total_working_days += 0.5;
                            }
                            else{
                                $total_working_days += 1;
                            }
                        }
                        // if($working_hours == 4 && !empty($attendance->ret)){
                        //     $total_working_days += 0.5;
                        // }
                        // else{
                        //     $total_working_days += 1;
                        // }
                    }
                    else{
                        $total_working_days += 1;
                    }
                    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                    continue;
                }
                $off_days = [];
                $empLeaveStatusQry = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$start_date' AND date_end >= '$start_date'  AND status = 'Approved'");
                if (count($empLeaveStatusQry) > 0) {
                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                    if (count($check_att_query) == 0) {
                        if(isset($empLeaveStatusQry[0]->is_half) && $empLeaveStatusQry[0]->is_half == 1){
                            $total_working_days = $total_working_days + 0.5;
                        }
                        else{
                            $total_working_days = $total_working_days + 1;
                        }
                    }
                } else {
                    $shift_emp_query = DB::select("SELECT shift_management.*,shift_type.sandwich_absent from shift_management join shift_type on shift_type.id = shift_management.shift_id
                    where shift_management.employee_id = '$employee->id'
                    and '$start_date' between (shift_management.shift_from_date and shift_management.shift_to_date)
                    and shift_management.deleted_at is null
                    and shift_management.status = 'Approved'
                    order by shift_management.id desc limit 1");
                    if (count($shift_emp_query) > 0) {
                        $shift_emp = $shift_emp_query[0];
                        $work_week_id = $shift_emp->work_week_id;
                        $work_days_query = DB::select("SELECT * from workdays where work_week_id = '$work_week_id' AND `status` = 'Non-working Day'");
                        foreach($work_days_query as $od){
                            $off_days[] = $od->name;
                        }
                    } else {
                        $off_days[] = ['Sunday'];
                    }
                    $off_days[] = ['Sunday'];
                    $current_day_name = date('l', strtotime("$start_date"));
                    if (in_array($current_day_name, $off_days)) {
                        // Sandwich Absent
                        $previous_date = date('Y-m-d', strtotime(''.$start_date.' -1 day'));
                        $next_date = date('Y-m-d', strtotime(''.$start_date.' +1 day'));
                        if (count($shift_emp_query) > 0) {
                            $shift_emp = $shift_emp_query[0];
                            if($shift_emp->sandwich_absent == 1){
                                $previous_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$previous_date")->first(['id']);
                                $next_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$next_date")->first(['id']);
                                $previousLeave = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$previous_date' AND date_end >= '$previous_date'  AND status = 'Approved'");
                                $nextLeave = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$next_date' AND date_end >= '$next_date'  AND status = 'Approved'");
                                // previous day holiday
                                $previous_holiday_qry = DB::select("SELECT * FROM holidays
                                WHERE dateh = '$previous_date'");

                                $previousHolidaySetting = holidaySettingForEmployee($employee, $previous_holiday_qry);

                                // next day holiday
                                $next_holiday_qry = DB::select("SELECT * FROM holidays
                                WHERE dateh = '$next_date'");

                                $nextHolidaySetting = holidaySettingForEmployee($employee, $next_holiday_qry);

                                if(!empty($previousLeave) || !empty($nextLeave) || !empty($previous_attendance) || !empty($next_attendance) || (!empty($previous_holiday_qry) && $previousHolidaySetting) || (!empty($next_holiday_qry) && $nextHolidaySetting))
                                {
                                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                                    if (count($check_att_query) == 0) {
                                        if((isset($previousLeave[0]->is_half) && $previousLeave[0]->is_half == 1) || (isset($nextLeave[0]->is_half) && $nextLeave[0]->is_half == 1)){
                                            $total_working_days = $total_working_days + 0.5;
                                        }
                                        else{
                                            $total_working_days = $total_working_days + 1;
                                        }
                                    }
                                }
                                else{
                                    if($this->checkHoliday($start_date,$employee->id,$employee) == 'yes'){
                                        $total_working_days = $total_working_days + 1;
                                    }
                                    else{
                                        if($start_date == $end_date){
                                            if($this->checkIfLastDayIsOff($start_date,$employee->id) == 'yes'){
                                                $total_working_days = $total_working_days + 1;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                $total_working_days = $total_working_days + 1;
                            }
                        }
                        else{
                            $total_working_days = $total_working_days + 1;
                        }
                    }
                    if (!in_array($current_day_name, $off_days)) {
                        if($this->checkHoliday($start_date,$employee->id,$employee) == 'yes'){
                            $total_working_days = $total_working_days + 1;
                        }
                    }
                }
                $off_days = [];
                $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }
            return $total_working_days;
        } else {
            while (strtotime($start_date) <= strtotime($termination_date)) {
                $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$start_date")->orderBy('id', 'ASC')->first();
                if (!empty($attendance)) {
                    $total_working_days = $total_working_days+1;
                    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                    continue;
                }
                $off_days = [];
                $empLeaveStatusQry = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$start_date' AND date_end >= '$start_date'  AND status = 'Approved'");
                if (count($empLeaveStatusQry) > 0) {
                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                    if (count($check_att_query) == 0) {
                        if(isset($empLeaveStatusQry[0]->is_half) && $empLeaveStatusQry[0]->is_half == 1){
                            $total_working_days = $total_working_days + 0.5;
                        }
                        else{
                            $total_working_days = $total_working_days + 1;
                        }
                    }
                } else {
                    $shift_emp_query = DB::select("SELECT shift_management.*,shift_type.sandwich_absent from shift_management join shift_type on shift_type.id = shift_management.shift_id
                    where shift_management.employee_id = '$employee->id'
                    and '$start_date' between (shift_management.shift_from_date and shift_management.shift_to_date)
                    and shift_management.deleted_at is null
                    and shift_management.status = 'Approved'
                    order by shift_management.id desc limit 1");
                    if (count($shift_emp_query) > 0) {
                        $shift_emp = $shift_emp_query[0];
                        $work_week_id = $shift_emp->work_week_id;
                        $work_days_query = DB::select("SELECT * from workdays where work_week_id = '$work_week_id' AND `status` = 'Non-working Day'");
                        foreach ($work_days_query as $od) {
                            $off_days[] = $od->name;
                        }
                    } else {
                        $off_days[] = ['Sunday'];
                    }
                    $current_day_name = date('l', strtotime("$start_date"));
                    if (in_array($current_day_name, $off_days)) {
                        // Sandwich Absent
                        $previous_date = date('Y-m-d', strtotime(''.$start_date.' -1 day'));
                        $next_date = date('Y-m-d', strtotime(''.$start_date.' +1 day'));
                        if (count($shift_emp_query) > 0) {
                            $shift_emp = $shift_emp_query[0];
                            if($shift_emp->sandwich_absent == 1)
                            {
                                $previous_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$previous_date")->first(['id']);
                                $next_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$next_date")->first(['id']);
                                $previousLeave = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$previous_date' AND date_end >= '$previous_date'  AND status = 'Approved'");
                                $nextLeave = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$next_date' AND date_end >= '$next_date'  AND status = 'Approved'");
                                // previous day holiday
                                $previous_holiday_qry = DB::select("SELECT * FROM holidays
                                WHERE dateh = '$previous_date'");

                                $previousHolidaySetting = holidaySettingForEmployee($employee, $previous_holiday_qry);

                                // next day holiday
                                $next_holiday_qry = DB::select("SELECT * FROM holidays
                                WHERE dateh = '$next_date'");

                                $nextHolidaySetting = holidaySettingForEmployee($employee, $next_holiday_qry);

                                if(!empty($previousLeave) || !empty($nextLeave) || !empty($previous_attendance) || !empty($next_attendance) || (!empty($previous_holiday_qry) && $previousHolidaySetting) || (!empty($next_holiday_qry) && $nextHolidaySetting))
                                {
                                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                                    if (count($check_att_query) == 0) {
                                        if((isset($previousLeave[0]->is_half) && $previousLeave[0]->is_half == 1) || (isset($nextLeave[0]->is_half) && $nextLeave[0]->is_half == 1)){
                                            $total_working_days = $total_working_days + 0.5;
                                        }
                                        else{
                                            $total_working_days = $total_working_days + 1;
                                        }
                                    }
                                }
                                else{
                                    if($this->checkHoliday($start_date,$employee->id,$employee) == 'yes'){
                                        $total_working_days = $total_working_days + 1;
                                    }
                                    else{
                                        if($start_date == $end_date){
                                            if($this->checkIfLastDayIsOff($start_date,$employee->id) == 'yes'){
                                                $total_working_days = $total_working_days + 1;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                                if (count($check_att_query) == 0) {
                                    $total_working_days = $total_working_days + 1;
                                }
                            }
                        }
                        else{
                            $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                            if (count($check_att_query) == 0) {
                                $total_working_days = $total_working_days + 1;
                            }
                        }
                    }
                    if (!in_array($current_day_name, $off_days)) {
                        if($this->checkHoliday($start_date,$employee->id,$employee) == 'yes'){
                            $total_working_days = $total_working_days + 1;
                        }
                    }
                }
                $off_days = [];
                $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }
            return $total_working_days;
        }
    }

    public function total_working_days_with_off_and_leaves_complete_month($employee,$payroll){
        $start_date = $payroll->date_start;
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        // $month_days = Carbon::now()->month($payroll_month)->daysInMonth;
        $end_date = $payroll->date_end;
        $joined_date                        = $employee->joined_date;
		$joined_month                       = date('m', strtotime($employee->joined_date));
        $joined_year                        = date('Y', strtotime($employee->joined_date));
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        // $working_days_query = DB::select("SELECT id from attendance where employee = '$employee->id' and month(in_time) = '$payroll_month' and year(in_time) = '$payroll_year' GROUP BY DAY(in_time)");
        $working_days_query = DB::select("SELECT id from attendance where employee = '$employee->id' and month(in_time) = '$payroll_month' and year(in_time) = '$payroll_year' and date(in_time) <= '$end_date'  GROUP BY DAY(in_time)");
        if($payroll_month == $joined_month AND $payroll_year == $joined_year){
			$start_date = $joined_date;
		}
        $total_working_days = count($working_days_query);
        $designation = JobTitle::find($employee->job_title);
        if(!empty($designation))
        {
            if($designation->exempt_working_days == 1){
            return $month_days;
        }
        }
        if($employee->employment_status == 15){
            return $total_working_days;
        }
        if($employee->employment_status == 17){
            return $month_days;
        }
        $termination_date_query = DB::select("SELECT termination_date from employees where id = '$employee->id'");
        $termination_date_object = $termination_date_query[0];
        $termination_date = $termination_date_object->termination_date;
        if ($termination_date == '0000-00-00' OR $termination_date == null OR empty($termination_date)) {
            while (strtotime($start_date) <= strtotime($end_date)) {
                $off_days = [];
                $empLeaveStatusQry = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$start_date' AND date_end >= '$start_date'  AND status = 'Approved'");
                if (count($empLeaveStatusQry) > 0) {
                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                    if (count($check_att_query) == 0) {
                        if(isset($empLeaveStatusQry[0]->is_half) && $empLeaveStatusQry[0]->is_half == 1){
                            $total_working_days = $total_working_days + 0.5;
                        }
                        else{
                            $total_working_days = $total_working_days + 1;
                        }
                    }
                } else {
                    $previous_date = date('Y-m-d', strtotime(''.$start_date.' -1 day'));
                    $next_date = date('Y-m-d', strtotime(''.$start_date.' +1 day'));
                    $shift_emp_query = DB::select("SELECT shift_management.*,shift_type.sandwich_absent from shift_management join shift_type on shift_type.id = shift_management.shift_id
                    where shift_management.employee_id = '$employee->id' and '$start_date' between shift_management.shift_from_date and shift_management.shift_to_date order by shift_management.id desc limit 1");
                    if (count($shift_emp_query) > 0) {
                        $shift_emp = $shift_emp_query[0];
                        $work_week_id = $shift_emp->work_week_id;
                        $work_days_query = DB::select("SELECT * from workdays where work_week_id = '$work_week_id' AND `status` = 'Non-working Day'");
                        foreach($work_days_query as $od){
                            $off_days[] = $od->name;
                        }
                    } else {
                        $off_days[] = ['Sunday'];
                    }
                    $off_days[] = ['Sunday'];
                    $current_day_name = date('l', strtotime("$start_date"));
                    if (in_array($current_day_name, $off_days)) {
                        // Sandwich Absent
                        if (count($shift_emp_query) > 0) {
                            $shift_emp = $shift_emp_query[0];
                            if($shift_emp->sandwich_absent == 1)
                            {
                                $previous_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$previous_date")->first(['id']);
                                $next_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$next_date")->first(['id']);
                                $previousLeave = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$previous_date' AND date_end >= '$previous_date'  AND status = 'Approved'");
                                $nextLeave = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$next_date' AND date_end >= '$next_date'  AND status = 'Approved'");
                                // previous day holiday
                                $previous_holiday_qry = DB::select("SELECT * FROM holidays
                                WHERE dateh = '$previous_date'");

                                $previousHolidaySetting = holidaySettingForEmployee($employee, $previous_holiday_qry);

                                // next day holiday
                                $next_holiday_qry = DB::select("SELECT * FROM holidays
                                WHERE dateh = '$next_date'");

                                $nextHolidaySetting = holidaySettingForEmployee($employee, $next_holiday_qry);

                                if(!empty($previousLeave) || !empty($nextLeave) || !empty($previous_attendance) || !empty($next_attendance) || (!empty($previous_holiday_qry) && $previousHolidaySetting) || (!empty($next_holiday_qry) && $nextHolidaySetting))
                                {
                                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                                    if (count($check_att_query) == 0) {
                                        if((isset($previousLeave[0]->is_half) && $previousLeave[0]->is_half == 1) || (isset($nextLeave[0]->is_half) && $nextLeave[0]->is_half == 1)){
                                            $total_working_days = $total_working_days + 0.5;
                                        }
                                        else{
                                            $total_working_days = $total_working_days + 1;
                                        }
                                    }
                                }
                                else{
                                    if($this->checkHoliday($start_date,$employee->id,$employee) == 'yes'){
                                        $total_working_days = $total_working_days + 1;
                                    }
                                    else{
                                        if($start_date == $end_date){
                                            if($this->checkIfLastDayIsOff($start_date,$employee->id) == 'yes'){
                                                $total_working_days = $total_working_days + 1;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                                if (count($check_att_query) == 0) {
                                    $total_working_days = $total_working_days + 1;
                                }
                            }
                        }
                        else{
                            $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                            if (count($check_att_query) == 0) {
                                $total_working_days = $total_working_days + 1;
                            }
                        }
                    }

                    if (!in_array($current_day_name, $off_days)) {
                        if($this->checkHoliday($start_date,$employee->id,$employee) == 'yes'){
                            $total_working_days = $total_working_days + 1;
                        }
                        // $holiday_qry = DB::select("SELECT * FROM holidays
                        // WHERE dateh = '$start_date'");
                        // if (count($holiday_qry) > 0) {
                        //     $holiday_attendance_query = DB::select("SELECT id from attendance where employee = '$employee->id' and date(in_time) = '$start_date' GROUP BY DAY(in_time)");
                        //     $previous_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$previous_date")->first(['id']);
                        //     $next_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$next_date")->first(['id']);
                        //     if(!empty($previous_attendance) || !empty($next_attendance)){
                        //         if(count($holiday_attendance_query) == 0)
                        //         {
                        //             $total_working_days = $total_working_days + 1;
                        //         }
                        //     }
                        // }
                    }
                }
                $off_days = [];
                $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }
            return $total_working_days;
        } else {
            while (strtotime($start_date) <= strtotime($termination_date)) {
                $off_days = [];
                $empLeaveStatusQry = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$start_date' AND date_end >= '$start_date'  AND status = 'Approved'");
                if (count($empLeaveStatusQry) > 0) {
                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                    if (count($check_att_query) == 0) {
                        if(isset($empLeaveStatusQry[0]->is_half) && $empLeaveStatusQry[0]->is_half == 1){
                            $total_working_days = $total_working_days + 0.5;
                        }
                        else{
                            $total_working_days = $total_working_days + 1;
                        }
                    }
                } else {
                    $previous_date = date('Y-m-d', strtotime(''.$start_date.' -1 day'));
                    $next_date = date('Y-m-d', strtotime(''.$start_date.' +1 day'));
                    $shift_emp_query = DB::select("SELECT shift_management.*,shift_type.sandwich_absent from shift_management join shift_type on shift_type.id = shift_management.shift_id where shift_management.employee_id = '$employee->id' and '$start_date' between shift_management.shift_from_date and shift_management.shift_to_date order by shift_management.id desc limit 1");
                    if (count($shift_emp_query) > 0) {
                        $shift_emp = $shift_emp_query[0];
                        $work_week_id = $shift_emp->work_week_id;
                        $work_days_query = DB::select("SELECT * from workdays where work_week_id = '$work_week_id' AND `status` = 'Non-working Day'");
                        foreach ($work_days_query as $od) {
                            $off_days[] = $od->name;
                        }
                    } else {
                        $off_days[] = ['Sunday'];
                    }
                    $current_day_name = date('l', strtotime("$start_date"));
                    if (in_array($current_day_name, $off_days)) {
                        // Sandwich Absent
                        if (count($shift_emp_query) > 0) {
                            $shift_emp = $shift_emp_query[0];
                            if($shift_emp->sandwich_absent == 1)
                            {
                                $previous_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$previous_date")->first(['id']);
                                $next_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$next_date")->first(['id']);
                                $previousLeave = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$previous_date' AND date_end >= '$previous_date'  AND status = 'Approved'");
                                $nextLeave = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$next_date' AND date_end >= '$next_date'  AND status = 'Approved'");
                                // previous day holiday
                                $previous_holiday_qry = DB::select("SELECT * FROM holidays
                                WHERE dateh = '$previous_date'");

                                $previousHolidaySetting = holidaySettingForEmployee($employee, $previous_holiday_qry);

                                // next day holiday
                                $next_holiday_qry = DB::select("SELECT * FROM holidays
                                WHERE dateh = '$next_date'");
                                $nextHolidaySetting = holidaySettingForEmployee($employee, $next_holiday_qry);

                                if(!empty($previousLeave) || !empty($nextLeave) || !empty($previous_attendance) || !empty($next_attendance) || (!empty($previous_holiday_qry) && $previousHolidaySetting) || (!empty($next_holiday_qry) && $nextHolidaySetting))
                                {
                                    $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                                    if (count($check_att_query) == 0) {
                                        if (count($check_att_query) == 0) {
                                            if((isset($previousLeave[0]->is_half) && $previousLeave[0]->is_half == 1) || (isset($nextLeave[0]->is_half) && $nextLeave[0]->is_half == 1)){
                                                $total_working_days = $total_working_days + 0.5;
                                            }
                                            else{
                                                $total_working_days = $total_working_days + 1;
                                            }
                                        }
                                    }
                                }
                                else{
                                    if($this->checkHoliday($start_date,$employee->id,$employee) == 'yes'){
                                        $total_working_days = $total_working_days + 1;
                                    }
                                    else{
                                        if($start_date == $end_date){
                                            if($this->checkIfLastDayIsOff($start_date,$employee->id) == 'yes'){
                                                $total_working_days = $total_working_days + 1;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                                if (count($check_att_query) == 0) {
                                    $total_working_days = $total_working_days + 1;
                                }
                            }
                        }
                        else{
                            $check_att_query = DB::select("SELECT id from attendance where employee = '$employee->id' AND date(in_time) = '$start_date'");
                            if (count($check_att_query) == 0) {
                                $total_working_days = $total_working_days + 1;
                            }
                        }
                    }
                    if (!in_array($current_day_name, $off_days)) {
                        if($this->checkHoliday($start_date,$employee->id,$employee) == 'yes'){
                            $total_working_days = $total_working_days + 1;
                        }
                    }
                }
                $off_days = [];
                $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }
            return $total_working_days;
        }
    }

    public function checkHoliday($date,$employee_id,$employee){
        $holiday = 'No';
        $holiday_qry = DB::select("SELECT * FROM holidays WHERE dateh = '$date'");
        $holidaySetting = holidaySettingForEmployee($employee, $holiday_qry);
        if (count($holiday_qry) > 0 && $employee->employment_status != 9 && $holidaySetting) {
            $holiday_attendance_query = DB::select("SELECT id from attendance where employee = '$employee_id' and date(in_time) = '$date' GROUP BY DAY(in_time)");
            if(count($holiday_attendance_query) == 0){
                $holiday = 'yes';
            }
        }
        return $holiday;
    }

    public function checkIfLastDayIsOff($date,$employee_id){
        $holiday = 'No';
        $attendance_query = DB::select("SELECT id from attendance where employee = '$employee_id' and date(in_time) = '$date' GROUP BY DAY(in_time)");
        if(count($attendance_query) == 0){
            $holiday = 'yes';
        }
        return $holiday;
    }

    function roofline_advance_days($employee, $payroll)
    {
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        $month_days = Carbon::now()->month($payroll_month)->daysInMonth;
        $advance_days = $month_days - 25;
        return $advance_days;
    }

    function roofline_previous_absents($employee,$payroll)
    {
        $payroll_month                      = $payroll->payroll_month;
        $payroll_year                       = $payroll->payroll_year;
        if($payroll_month==01){
            $payroll_year                       = $payroll_year-1;
        }
        $previous_month = date('m', strtotime($payroll->date_start. ' -1 months'));
        $start_date = $payroll_year.'-'.$previous_month.'-26';
        $date = strtotime($start_date);
        $previous_month_lastdate = strtotime(date("Y-m-t", $date ));
        $previous_month_lastday = date("d", $previous_month_lastdate);
        $total_absents = 0;
        // $start_date                         = $payroll->date_start;
        // $end_date                           = $payroll->date_end;
        $end_date                           = $payroll_year.'-'.$previous_month.'-'.$previous_month_lastday;
		$suspension_date                    = $employee->suspension_date;
		$joined_date                        = $employee->joined_date;
		$joined_month                       = date('m', strtotime($employee->joined_date));
        $joined_year                        = date('Y', strtotime($employee->joined_date));
        if($payroll_month == $joined_month AND $payroll_year == $joined_year)
        {
            return $total_absents;
        }

        if (!empty($suspension_date)) {
            if ($suspension_date != '' AND $suspension_date != '0000-00-00') {
                $end_date = $suspension_date;
            }
		}
		if($previous_month == $joined_month AND $payroll_year == $joined_year){
            if($joined_date > $start_date)
            {
                $start_date = $joined_date;
            }
		}
        $period                             = CarbonPeriod::create($start_date, $end_date);
        $designation = JobTitle::find($employee->job_title);
        if(!empty($designation))
        {
            if($designation->exempt_working_days == 1){
                return $total_absents;
            }
        }
        if($employee->employment_status == 17){
            return $total_absents;
        }
        foreach ($period as $start_date) {
            if(!empty($employee->restore_date) && $employee->restore_date != '0000-00-00'){
                if(strtotime($start_date) < strtotime($employee->restore_date)){
                    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                    continue;
                }
            }
            $attendance_qry =  DB::select("SELECT * from attendance where employee = '$employee->id' and date(in_time) = '$start_date' limit 1");
            if (count($attendance_qry) < 1) {
                $empLeaveStatusQry = DB::select("SELECT * FROM employeeleaves where employee = '$employee->id' AND date_start <= '$start_date' AND date_end >= '$start_date'  AND status = 'Approved'");
                if (count($empLeaveStatusQry) > 0) {
                } else {

                    $max_shift_data = DB::select("SELECT id FROM shift_management
                    WHERE '$start_date' between shift_from_date AND shift_to_date
                    AND employee_id = $employee->id ORDER BY id DESC LIMIT 1");

                    if($max_shift_data AND !empty($max_shift_data)){
                        $max_shift_data = $max_shift_data[0];
                        $employeeOffDayQry = DB::select("SELECT shift_management.id,workdays.name as off_days,workdays.`status`,shift_type.shift_desc
                        FROM shift_management
                        join workdays on workdays.work_week_id = shift_management.work_week_id
                        join shift_type on shift_type.id = shift_management.shift_id
                        where employee_id = '$employee->id'
                        AND shift_management.id = '$max_shift_data->id'
                        AND workdays.`status` = 'Non-working Day'");
                        if (count($employeeOffDayQry) > 0) {
                            foreach ($employeeOffDayQry as $employeeOffDayTest) {
                                $dayName[] = $employeeOffDayTest->off_days;
                            }
                        } else {
                            $dayName[] = 'Sunday';
                        }
                    }else{
                        $dayName[] = 'Sunday';
                    }
                    $current_day_name = date('l', strtotime("$start_date"));
                    if (!in_array($current_day_name, $dayName)) {
                        $holiday_qry = DB::select("SELECT * FROM holidays
                        WHERE dateh = '$start_date'");
                        if(count($holiday_qry) == 0){
                            $total_absents = $total_absents + 1;
                        }
                    }
                    $dayName = [];
                }
            }
        }
        return $total_absents;
    }

    function roofline_previous_overtime_hours($employee,$payroll)
    {
        $payroll_month                      = $payroll->payroll_month;
		$payroll_year                       = $payroll->payroll_year;
        $previous_month = date('m', strtotime($payroll->date_start. ' -1 months'));
        $start_date = $payroll_year.'-'.$previous_month.'-26';
        $date = strtotime($start_date);
        $previous_month_lastdate = strtotime(date("Y-m-t", $date ));
        $previous_month_lastday = date("d", $previous_month_lastdate);
        // $start_date                         = $payroll->date_start;
        // $end_date                           = $payroll->date_end;
        $end_date                           = $payroll_year.'-'.$previous_month.'-'.$previous_month_lastday;
        $total_overtime_hours = 0;
        while (strtotime($start_date) <= strtotime($end_date)) {
            $overtime_qry = DB::select("SELECT * from overtime_management where overtime_date = '$start_date' and employee_id = '$employee->id' and `status` = 'Approved'");
            if (count($overtime_qry) > 0) {
                foreach ($overtime_qry as $overtime_details) {
                    $overtime_hours = $overtime_details->overtime;
                    $total_overtime_hours = $total_overtime_hours + $overtime_hours;
                }
            }
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }
        return $total_overtime_hours;
    }
    function basic_compute_jsml($employee, $payroll)
    {
        $emp_salary_component = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 72)->first();
        $employment_status = DB::table('employmentstatus')->where('id', $employee->employment_status)->first();
        $amount = 0.00;
        if(!empty($emp_salary_component))
        {
            // if($employment_status->name == "Manager's (Permanent)" || $employment_status->name == "Manager's (Contract)" || $employment_status->name == "Manager's (Contract)" || $employment_status->name == "Manager's (Retired)")
            // {
                $amount = round($emp_salary_component->amount * 35/100);
            // }
            // else
            // {
                // $amount = round($emp_salary_component->amount * 25/100);
            // }
        }
        else{
            $amount = 0.00;
        }
        return $amount;
    }

    function daily_rate($employee, $payroll)
    {
        $emp_salary_component = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 72)->first();
        $employment_status = DB::table('employmentstatus')->where('id', $employee->employment_status)->first();
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        $amount = 0.00;
        if(!empty($emp_salary_component))
        {
            if($employment_status->name == 'Daily Wages')
            {
                $amount = round($emp_salary_component->amount/26, 2);
            }
            else{
                $amount = round($emp_salary_component->amount/$month_days, 2);
            }
        }
        else{
            $amount = 0.00;
        }
        return $amount;
    }

    function gross_salary($employee, $payroll)
    {
        $columns = DB::table('payrolldata')->where('payroll', $payroll->id)->where('employee', $employee->id)->whereIn('payroll_item', [11,20,38,40,82])->get();
        $net_working_hours = 0;
        $hour_rate_amount = 0;
        $internee_stipend_amount = 0;
        $amount = 0;
        $salary_calculated = 0;
        $internee_per_day_stipend = 0;
        $stipend_during_month = 0;
        $horly_rate_amount = 0;
        $paid_days_salary = 0;
        $arrear_hour_comp_amount = 0;
        $arrear_amount = 0;
        $meal_amount = 0;
        $rent_amount = 0;
        $travel_amount = 0;
        $mobile_allownace_amount = 0;
        $baby_marriage_gift = 0;
        $horly_rate_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 8)->first();
        if(!empty($horly_rate_comp))
        {
            $horly_rate_amount = $horly_rate_comp->amount;
        }
        $internee_stipend = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 13)->first();
        if(!empty($internee_stipend))
        {
            $internee_stipend_amount = $internee_stipend->amount;
        }

        // Arrear Hour Amount
        $arrear_hour_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 22)->first();
        if(!empty($arrear_hour_comp))
        {
            $arrear_hour_comp_amount = $arrear_hour_comp->amount;
        }
        $arrear_hour_amount = $arrear_hour_comp_amount * $horly_rate_amount;

        // Arrear Amount
        $arrear_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 20)->first();
        if(!empty($arrear_comp))
        {
            $arrear_amount = $arrear_comp->amount;
        }

        // Meal Amount
        $meal_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 25)->first();
        if(!empty($meal_comp))
        {
            $meal_amount = $meal_comp->amount;
        }

        // Rent Amount
        $rent_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 24)->first();
        if(!empty($rent_comp))
        {
            $rent_amount = $rent_comp->amount;
        }

        // Tarvel Allowance
        $travel_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 27)->first();
        if(!empty($travel_comp))
        {
            $travel_amount = $travel_comp->amount;
        }

        // Mobile Allowance
        $mobile_allownace_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 26)->first();
        if(!empty($mobile_allownace_comp))
        {
            $mobile_allownace_amount = $mobile_allownace_comp->amount;
        }

        // Baby Marriage Gifts
        $baby_marriage_gift_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 28)->first();
        if(!empty($baby_marriage_gift_comp))
        {
            $baby_marriage_gift = $baby_marriage_gift_comp->amount;
        }

        $shift_working_hours = $this->shift_working_hours($employee,$payroll);
        $internee_working_hours = $this->internee_working_hours($employee,$payroll);
        $overtime_hours = $this->overtime_hours($employee,$payroll);
        $total_paid_days = $this->total_paid_days($employee,$payroll);

        $internee_working_days = $this->internee_working_days($employee,$payroll);
        $total_paid_days_hours = $this->total_paid_days_hours($employee,$payroll);
        $net_working_hours = $total_paid_days-$internee_working_hours+$shift_working_hours+$overtime_hours;
        $salary_calculated = $net_working_hours * $horly_rate_amount;
        $internee_per_day_stipend = $internee_stipend_amount/30;

        $stipend_during_month = $internee_per_day_stipend * $internee_working_days;
        $paid_days_salary = $horly_rate_amount * $total_paid_days_hours;
        if(count($columns) > 0)
        {
            foreach($columns as $column)
            {
                if(!empty($column))
                {
                    $amt = $column->amount;
                }
                else{
                    $amt = 0.00;
                }
                $amount += $amt;
            }
        }
        else{
            $amount = 0.00;
        }
        $amount = $amount + $salary_calculated + $stipend_during_month + $paid_days_salary + $arrear_hour_amount + $meal_amount + $rent_amount+$mobile_allownace_amount+$travel_amount+$arrear_amount+$baby_marriage_gift;
        return $amount;
    }

    function gross_salary_warehouse($employee,$payroll){
        $amount = 0;
        $total_salary = 0;
        $horly_rate_amount = 0;
        // $paid_days_salary = 0;
        $arrear_hour_comp_amount = 0;
        $arrear_amount = 0;
        $meal_amount = 0;
        $rent_amount = 0;
        $travel_amount = 0;
        $mobile_allownace_amount = 0;
        $baby_marriage_gift = 0;
        $overtime_days = 0;
        $overtime_days_wise_amount = 0;

        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        // $total_present_days = $this->total_present_days($employee,$payroll);
        $total_working_days = $this->total_working_days_with_off_and_leaves($employee,$payroll);
        $basic_salary = DB::table('payrolldata')->where('payroll', $payroll->id)->where('employee', $employee->id)->where('payroll_item',20)->first(['id','payroll','employee','amount']);
        $columns = DB::table('payrolldata')->where('payroll', $payroll->id)->where('employee', $employee->id)->whereIn('payroll_item', [11,38,40,82,104])->get();

        $horly_rate_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 8)->first();
        if(!empty($horly_rate_comp))
        {
            $horly_rate_amount = $horly_rate_comp->amount;
        }
        // $internee_stipend = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 13)->first();
        // if(!empty($internee_stipend))
        // {
        //     $internee_stipend_amount = $internee_stipend->amount;
        // }

        // Arrear Hour Amount
        $arrear_hour_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 22)->first();
        if(!empty($arrear_hour_comp))
        {
            $arrear_hour_comp_amount = $arrear_hour_comp->amount;
        }
        $arrear_hour_amount = $arrear_hour_comp_amount * $horly_rate_amount;

        // Arrear Amount
        $arrear_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 20)->first();
        if(!empty($arrear_comp))
        {
            $arrear_amount = $arrear_comp->amount;
        }

        // Meal Amount
        $meal_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 25)->first();
        if(!empty($meal_comp))
        {
            $meal_amount = $meal_comp->amount;
        }

        // Rent Amount
        $rent_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 24)->first();
        if(!empty($rent_comp))
        {
            $rent_amount = $rent_comp->amount;
        }

        // Tarvel Allowance
        $travel_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 27)->first();
        if(!empty($travel_comp))
        {
            $travel_amount = $travel_comp->amount;
        }

        // Mobile Allowance
        $mobile_allownace_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 26)->first();
        if(!empty($mobile_allownace_comp))
        {
            $mobile_allownace_amount = $mobile_allownace_comp->amount;
        }

        // Baby Marriage Gifts
        $baby_marriage_gift_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 28)->first();
        if(!empty($baby_marriage_gift_comp))
        {
            $baby_marriage_gift = $baby_marriage_gift_comp->amount;
        }

        $overtime_days_comp = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 30)->first();
        if(!empty($overtime_days_comp))
        {
            $overtime_days = $overtime_days_comp->amount;
        }
        $one_day_salary = $basic_salary->amount/$month_days;
        $overtime_days_wise_amount = $overtime_days*$one_day_salary;
        $total_salary = $one_day_salary * $total_working_days;
        if(count($columns) > 0)
        {
            foreach($columns as $column)
            {
                if(!empty($column))
                {
                    $amt = $column->amount;
                }
                else{
                    $amt = 0.00;
                }
                $amount += $amt;
            }
        }
        else{
            $amount = 0.00;
        }
        $amount = $amount + $total_salary + $arrear_hour_amount + $meal_amount + $rent_amount+$mobile_allownace_amount+$travel_amount+$arrear_amount+$baby_marriage_gift+$overtime_days_wise_amount;
        return $amount;
    }

    function total_present_days($employee, $payroll){
        $present_days = Attendance::where('employee',$employee->id)->whereDate('in_time','>=',$payroll->date_start)->whereDate('in_time','<=',$payroll->date_end)->count();
        return $present_days;
    }

    function basic_computed($employee, $payroll)
    {
        $amount = 0.00;
        $daily_rate = $this->daily_rate($employee, $payroll);
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        // $emp_salary_component = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 90)->first();
        $emp_salary_component = DB::select("SELECT *,month(component_month) as component_month,year(component_month) as component_year from monthly_salary_component where component_id = 90 AND employee_id =  $employee->id AND year(component_month) = $payroll->payroll_year AND Month(component_month) = $payroll->payroll_month");
        $employment_status = DB::table('employmentstatus')->where('id', $employee->employment_status)->first();
        if(!empty($emp_salary_component))
        {
            if($employment_status->name == 'Daily Wages')
            {
                $gross_pay = round($daily_rate * $emp_salary_component[0]->amount, 2);
            }
            else{
                $gross_pay = round($daily_rate * $emp_salary_component[0]->amount, 2);
            }

            // if($employment_status->name == "Manager's (Permanent)" || $employment_status->name == "Manager's (Contract)" || $employment_status->name == "Manager's (Contract)" || $employment_status->name == "Manager's (Retired)")
            // {
                $amount = round($gross_pay * 35/100);
            // }
            // else
            // {
            //     $amount = round($gross_pay * 25/100);
            // }
        }
        else{
            $amount = 0.00;
        }
        return $amount;
    }
    function provident_fund_mills($employee, $payroll)
    {
        $amount = 0.00;
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        // $employee_working_days = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 90)->first();
        $employee_working_days = DB::select("SELECT *,month(component_month) as component_month,year(component_month) as component_year from monthly_salary_component where component_id = 90 AND employee_id =  $employee->id AND year(component_month) = $payroll->payroll_year AND Month(component_month) = $payroll->payroll_month");
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        if(count($employee_working_days) > 0)
        {
            if($employee->provident_fund == 'yes')
            {
                $workingDays = $employee_working_days[0]->amount;
                if(!empty($employee->confirmation_date) && $employee->confirmation_date != '0000-00-00'){
                    $confirmation_month = date('m',strtotime($employee->confirmation_date));
                    $confirmation_year = date('Y',strtotime($employee->confirmation_date));
                    if($payroll_year == $confirmation_year && $payroll_month == $confirmation_month){
                        $fromDate = Carbon::parse($employee->confirmation_date);
                        $toDate = Carbon::parse($payroll->date_end);
                        $days = $toDate->diffInDays($fromDate);
                        $workingDays = $days+1;
                    }
                }
                $basic_pay = $this->basic_compute_jsml($employee, $payroll);
                $pf_fund = $basic_pay * 10/100;
                $amount = round(($workingDays/$month_days) * $pf_fund);
            }
        }
        return $amount;
    }

    function clinix_tax($employee, $payroll)
    {
        $gross_salary = $this->gross_salary($employee, $payroll);
        if($gross_salary > 50000 && $gross_salary <= 100000)
        {
            $applied_tax_amount = $gross_salary - 50000;
            $amount = $applied_tax_amount * 2.5/100;
        }
        elseif($gross_salary > 100000 && $gross_salary <= 200000)
        {
            $tax_on_exempted_amount_month = 15000/12;
            $applied_tax_amount = $gross_salary - 100000;
            $tax_amount = $applied_tax_amount * 12.5/100;
            $amount = $tax_on_exempted_amount_month + $tax_amount;
        }
        elseif($gross_salary > 200000 && $gross_salary <= 300000)
        {
            $tax_on_exempted_amount_month = 165000/12;
            $applied_tax_amount = $gross_salary - 200000;
            $tax_amount = $applied_tax_amount * 20/100;
            $amount = $tax_on_exempted_amount_month + $tax_amount;
        }
        elseif($gross_salary > 300000 && $gross_salary <= 500000)
        {
            $tax_on_exempted_amount_month = 405000/12;
            $applied_tax_amount = $gross_salary - 300000;
            $tax_amount = $applied_tax_amount * 25/100;
            $amount = $tax_on_exempted_amount_month + $tax_amount;
        }
        elseif($gross_salary > 500000 && $gross_salary <= 1000000)
        {
            $tax_on_exempted_amount_month = 1005000/12;
            $applied_tax_amount = $gross_salary - 500000;
            $tax_amount = $applied_tax_amount * 32.5/100;
            $amount = $tax_on_exempted_amount_month + $tax_amount;
        }
        elseif($gross_salary > 1000000)
        {
            $tax_on_exempted_amount_month = 2955000/12;
            $applied_tax_amount = $gross_salary - 1000000;
            $tax_amount = $applied_tax_amount * 35/100;
            $amount = $tax_on_exempted_amount_month + $tax_amount;
        }
        else
        {
            $amount = 0.00;
        }
        return round($amount);
    }
    function fuel_rate($employee, $payroll)
    {
        $amount = 0.00;
        $emp_salary_component = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 99)->first();
        $fuel_rates = DB::table('fuel_rate')->whereMonth('from_date', $payroll->payroll_month)->latest()->first();
        if(!empty($emp_salary_component))
        {
            if(!empty($fuel_rates))
            {
                $amount = $emp_salary_component->amount * $fuel_rates->rate;
            }
        }
        return round($amount);
    }
    function eobi_calculate($employee, $payroll) {
        $amount = 0.00;
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        $birthDate = new DateTime($employee->birthday);
        $toDate = new Datetime($payroll->date_end);
        $ageObject = $toDate->diff($birthDate);
        $Age = $ageObject->y + $ageObject->m/12;
        $employment_status = DB::table('employmentstatus')->where('id', $employee->employment_status)->first();
        $employee_working_days = DB::select("SELECT *,month(component_month) as component_month,year(component_month) as component_year from monthly_salary_component where component_id = 90 AND employee_id =  $employee->id AND year(component_month) = $payroll->payroll_year AND Month(component_month) = $payroll->payroll_month");
        if($employee->eobi_status == 'yes'){
            if(!empty($employment_status)){
                if($employment_status->name == 'Daily Wages'){
                    $daily_rate = 8000/26;
                }
                else{
                    $daily_rate = 8000/$month_days;
                }
            }
            else{
                $daily_rate = 0;
            }
            if(count($employee_working_days) > 0)
            {
                if($employment_status->name == 'Daily Wages'){
                    $working_days = $employee_working_days[0]->amount > 26 ? 26 : $employee_working_days[0]->amount;
                }
                else{
                    $working_days = $employee_working_days[0]->amount;
                }
                if($ageObject->y == 60 && $ageObject->m == 0){
                    $days = $month_days - ($ageObject->d + 1); // Adding 1 Because DateTime Object Is not Calculating age including the end date
                    if($days < $working_days){
                        $working_days = $days;
                    }
                }
                if($Age < 60 || ($ageObject->y == 60 && $ageObject->m == 0)){
                    $total_amount = $daily_rate * $working_days;
                    $amount = $total_amount * 1/100;
                }
            }
            else{
                $amount = 0.00;
            }
        }
        return round($amount);
    }

    function eobi_lahore_office($employee, $payroll)
    {
        $amount = 0.00;
        $eobi = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 25)->first();
        if(!empty($eobi))
        {
            if($employee->eobi_status == 'yes')
            {
                $amount = $eobi->amount;
            }
        }
        return round($amount);
    }

    function eobi_employers_contribution($employee, $payroll)
    {
        $amount = 0;
        $employment_status = DB::table('employmentstatus')->where('id', $employee->employment_status)->first();
        // $employee_working_days = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 90)->first();
        $employee_working_days = DB::select("SELECT *,month(component_month) as component_month,year(component_month) as component_year from monthly_salary_component where component_id = 90 AND employee_id =  $employee->id AND year(component_month) = $payroll->payroll_year AND Month(component_month) = $payroll->payroll_month");
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        $birthDate = new DateTime($employee->birthday);
        $toDate = new Datetime($payroll->date_start);
        $ageObject = $toDate->diff($birthDate);
        $Age = $ageObject->y + $ageObject->m/12;
        if($employee->eobi_status == 'yes'){
            if(!empty($employment_status))
            {
                if($employment_status->name == 'Daily Wages')
                {
                    $daily_rate = 8000/26;
                }
                else{
                    $daily_rate = 8000/$month_days;
                }
            }
            else{
                $daily_rate = 0;
            }
            if(count($employee_working_days) > 0)
            {
                if($employment_status->name == 'Daily Wages')
                {
                    $working_days = $employee_working_days[0]->amount > 26 ? 26 : $employee_working_days[0]->amount;
                }
                else{
                    $working_days = $employee_working_days[0]->amount;
                }
                if($ageObject->y == 59 && $ageObject->m==11){
                    if($working_days > date('d',strtotime($employee->birthday))){
                        $working_days = $month_days-$ageObject->d;
                    }
                }
                if($Age < 60){
                    $total_amount = $daily_rate * $working_days;
                    $amount = $total_amount * 5/100;
                }
            }
            else{
                $amount = 0.00;
            }
        }
        return round($amount);
    }

    function pessi_contribution($employee, $payroll)
    {
        $amount = 0;
        $pessi_range = 0;
        $employment_status = DB::table('employmentstatus')->where('id', $employee->employment_status)->first();
        // $employee_working_days = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 90)->first();
        $employee_working_days = DB::select("SELECT *,month(component_month) as component_month,year(component_month) as component_year from monthly_salary_component where component_id = 90 AND employee_id =  $employee->id AND year(component_month) = $payroll->payroll_year AND Month(component_month) = $payroll->payroll_month");
        if($employee->pessi == 'yes')
        {
            if(count($employee_working_days) > 0)
            {
                $salary_daily_rate = $this->daily_rate($employee, $payroll);
                $gross_pay = $salary_daily_rate*$employee_working_days[0]->amount;
                $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
                if($gross_pay < 22000)
                {
                    $pessi_range = $gross_pay;
                }
                else
                {
                    if($employee_working_days[0]->amount < $month_days)
                    {
                        $pessi_range = 22000/$month_days*$employee_working_days[0]->amount;
                    }
                    else
                    {
                        $pessi_range = 22000;
                    }
                }
                $amount = $pessi_range*6/100;
                // if(!empty($employment_status))
                // {
                //     if($employment_status->name == 'Daily Wages')
                //     {
                //         $daily_rate = $pessi_range/26;
                //     }
                //     else{
                //         $daily_rate = $pessi_range/$month_days;
                //     }
                // }
                // else{
                //     $daily_rate = 0;
                // }
                // if(!empty($employee_working_days))
                // {
                //     if($employment_status->name == 'Daily Wages')
                //     {
                //         $working_days = $employee_working_days->amount > 26 ? 26 : $employee_working_days->amount;
                //     }
                //     else{
                //         $working_days = $employee_working_days->amount;
                //     }
                //     $total_amount = $daily_rate * $working_days;
                //     $amount = $total_amount * 6/100;
                // }
                // else{
                //     $amount = 0.00;
                // }
            }
        }
        return round($amount);
    }

    function eobi_wages_report($employee, $payroll)
    {
        $amount = 0;
        $employment_status = DB::table('employmentstatus')->where('id', $employee->employment_status)->first();
        // $employee_working_days = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 90)->first();
        $employee_working_days = DB::select("SELECT *,month(component_month) as component_month,year(component_month) as component_year from monthly_salary_component where component_id = 90 AND employee_id =  $employee->id AND year(component_month) = $payroll->payroll_year AND Month(component_month) = $payroll->payroll_month");
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        $birthDate = new DateTime($employee->birthday);
        $toDate = new Datetime($payroll->date_start);
        $ageObject = $toDate->diff($birthDate);
        $Age = $ageObject->y + $ageObject->m/12;
        if($employee->eobi_status == 'yes'){
            if(!empty($employment_status))
            {
                if($employment_status->name == 'Daily Wages')
                {
                    $daily_rate = 8000/26;
                }
                else{
                    $daily_rate = 8000/$month_days;
                }
            }
            else{
                $daily_rate = 0;
            }
            if(count($employee_working_days) > 0)
            {
                if($employment_status->name == 'Daily Wages')
                {
                    $working_days = $employee_working_days[0]->amount > 26 ? 26 : $employee_working_days[0]->amount;
                }
                else{
                    $working_days = $employee_working_days[0]->amount;
                }
                if($ageObject->y == 59 && $ageObject->m==11){
                    if($working_days > date('d',strtotime($employee->birthday))){
                        $working_days = $month_days-$ageObject->d;
                    }
                }
                if($Age < 60){
                    $amount = $daily_rate * $working_days;
                }
            }
            else{
                $amount = 0;
            }
        }
        return round($amount);
    }

    function ss_wages_pessi_report($employee, $payroll)
    {
        $amount = 0;
        $employment_status = DB::table('employmentstatus')->where('id', $employee->employment_status)->first();
        // $employee_working_days = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 90)->first();
        $employee_working_days = DB::select("SELECT *,month(component_month) as component_month,year(component_month) as component_year from monthly_salary_component where component_id = 90 AND employee_id =  $employee->id AND year(component_month) = $payroll->payroll_year AND Month(component_month) = $payroll->payroll_month");
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        if($employee->pessi == 'yes'){
            if(count($employee_working_days) > 0)
            {
                $salary_daily_rate = $this->daily_rate($employee, $payroll);
                $gross_pay = $salary_daily_rate*$employee_working_days[0]->amount;
                if($gross_pay < 22000)
                {
                    $pessi_range = $gross_pay;
                }
                else
                {
                    if($employee_working_days[0]->amount < $month_days)
                    {
                        $pessi_range = 22000/$month_days*$employee_working_days[0]->amount;
                    }
                    else
                    {
                        $pessi_range = 22000;
                    }
                }

                $amount = $pessi_range;
                // if(!empty($employment_status))
                // {
                //     if($employment_status->name == 'Daily Wages')
                //     {
                //         $daily_rate = $pessi_range/26;
                //     }
                //     else{
                //         $daily_rate = $pessi_range/$month_days;
                //     }
                // }
                // else{
                //     $daily_rate = 0;
                // }
                // if(!empty($employee_working_days))
                // {
                //     if($employment_status->name == 'Daily Wages')
                //     {
                //         $working_days = $employee_working_days->amount > 26 ? 26 : $employee_working_days->amount;
                //     }
                //     else{
                //         $working_days = $employee_working_days->amount;
                //     }
                //     $amount = $daily_rate * $working_days;
                // }
                // else{
                //     $amount = 0;
                // }
            }
        }
        return round($amount);
    }
    function provident_fund_employers_contribution($employee, $payroll)
    {
        $amount = $this->provident_fund_mills($employee, $payroll);
        return round($amount);
    }
    function provident_fund_LO($employee, $payroll)
    {
        $amount = 0.00;
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        $working_days = $this->total_working_days_with_off_and_leaves($employee,$payroll);
        $basic_salary = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 33)->first();
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        if(!empty($basic_salary))
        {
            if($employee->provident_fund == 'yes')
            {
                if(!empty($employee->confirmation_date) && $employee->confirmation_date != '0000-00-00'){
                    $confirmation_month = date('m',strtotime($employee->confirmation_date));
                    $confirmation_year = date('Y',strtotime($employee->confirmation_date));
                    if($payroll_year == $confirmation_year && $payroll_month == $confirmation_month){
                        $fromDate = Carbon::parse($employee->confirmation_date);
                        $toDate = Carbon::parse($payroll->date_end);
                        $days = $toDate->diffInDays($fromDate);
                        $working_days = $days+1;
                    }
                }
                $pf_fund = $basic_salary->amount * 10/100;
                $amount = round(($working_days/$month_days) * $pf_fund);
            }
        }
        return $amount;
    }

    function daily_salary_as_per_month($employee, $payroll)
    {
        $amount = 0.00;
        $basic_salary = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 33)->first();
        $other_allowances = DB::table('employeesalary')->where('employee', $employee->id)->where('component', '=', 34)->first();
        $other_allowance = !empty($other_allowances) ? $other_allowances->amount : 0;
        $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
        if(!empty($basic_salary))
        {
            $gross_salary = $basic_salary->amount + $other_allowance;
            $amount = $gross_salary/$month_days;
        }
        return $amount;
    }
    public function calculateSalaryDuration($employee_id, $payroll_id) {
        $payroll_qry = "SELECT date_start,date_end,month(date_start) as payroll_month,year(date_start) as payroll_year from payroll where id = '$payroll_id'";
        $payroll_qry_execute = $this->conn->query($payroll_qry);
        $payroll = $payroll_qry_execute->fetch_object();
        $start_date = $payroll->date_start;
        $end_date = $payroll->date_end;
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        $start_date1 = date_create($start_date);
        $end_date1 = date_create($end_date);
        $diff = date_diff($start_date1, $end_date1);
        $days = $diff->days;
        return $days + 1;
    }

    public function total_month_days($employee,$payroll){
        $payroll_month = $payroll->payroll_month;
        $payroll_year = $payroll->payroll_year;
        return Carbon::now()->month($payroll_month)->daysInMonth;
    }

    public function clinix_previous_month_salary($employee,$payroll)
    {
        $date = date('Y-m-d', strtotime($payroll->date_start. ' - 1 month'));
        $amount = 0;
        $payroll_month = date('m',strtotime($date));
        $payroll_year = date('Y',strtotime($date));
        $previous_payroll = DB::table('payroll')->where('department',$payroll->department)->where('status','Completed')->WhereYear('date_start',$payroll_year)->WhereMonth('date_start',$payroll_month)->first(['id']);
        if(!empty($previous_payroll)){
            $previous_salary = DB::table('payrolldata')->where([
                ['payroll',$previous_payroll->id],
                ['employee',$employee->id],
                ['payroll_item',74]
            ])->first(['amount']);
            if(!empty($previous_salary)){
                $amount = $previous_salary->amount;
            }
        }
        return $amount;
    }

    public function clinix_sc($employee,$payroll)
    {
        $amount = 0;
        $net_salary = DB::table('payrolldata')->where([
            ['payroll',$payroll->id],
            ['employee',$employee->id],
            ['payroll_item',74]
        ])->first(['amount']);
        if(!empty($net_salary)){
            $previous_salary = $this->clinix_previous_month_salary($employee,$payroll);
            if($previous_salary > 0){
                $amount = (($net_salary->amount/$previous_salary)*100)-100;
            }
        }
        return $amount;
    }

    public function fineReversal($employee,$payroll)
    {
        $amount = 0;
        // $previousMonth = date('Y-m', strtotime($payroll->date_start. ' -1 months'));
        FineReversal::where([['employee_id',$employee->id],['fine_reverse_payroll_id',$payroll->id]])->update(['process_status' => 0]);
        $reverseFines = FineReversal::where([['employee_id', $employee->id],['process_status', 0],['status', '=','Approved']])->get(['id','amount']);
        if(count($reverseFines) > 0){
            foreach ($reverseFines as $key => $fine) {
                $fine->fine_reverse_payroll_id = $payroll->id;
                $fine->fine_reverse_month = $payroll->payroll_year.'-'.$payroll->payroll_month;
                $fine->process_status = 1;
                $fine->update();
                $amount += $fine->amount;
            }
        }
        return $amount;
    }



    // public function total_working_days_with_off_and_leaves_new($employee,$payroll)
    // {
    //     $total_working_days = 0;
    //     $start_date = $payroll->date_start;
    //     $payroll_month = $payroll->payroll_month;
    //     $payroll_year = $payroll->payroll_year;
    //     $month_days = Carbon::now()->month($payroll_month)->daysInMonth;
    //     $end_date = $payroll->date_end;
    //     $joined_date                        = $employee->joined_date;
	// 	$joined_month                       = date('m', strtotime($employee->joined_date));
    //     $joined_year                        = date('Y', strtotime($employee->joined_date));
    //     $month_days = Carbon::parse($payroll->date_start)->daysInMonth;
    //     if($payroll_month == $joined_month AND $payroll_year == $joined_year){
	// 		$start_date = $joined_date;
	// 	}
    //     $designation = JobTitle::find($employee->job_title);
    //     if(!empty($designation)){
    //         if($designation->exempt_working_days == 1){
    //             return $month_days;
    //         }
    //     }
    //     if($employee->employment_status == 17){
    //         return $month_days;
    //     }
    //     $termination_date = Employee::where('id',$employee->id)->value('termination_date');
    //     if (!empty($termination_date) && $termination_date != '0000-00-00') {
    //         $end_date = $termination_date;
    //     }
    //     if (env('COMPANY') == 'UNICORN'){
    //         $unicornDoubleAttendanceShifts = getUnicornDoubleAttendanceShifts()->pluck('id')->toArray();
    //     }
    //     else{
    //         $unicornDoubleAttendanceShifts = [];
    //     }
    //     for ($date = $start_date; $date <= $end_date; $date++) {
    //         $shift_data = DB::table('shift_management')->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
    //             ->join('work_week', 'work_week.id', '=', 'shift_management.work_week_id')
    //             ->where([
    //                 ['shift_management.employee_id', $employee->id],
    //                 ['shift_management.shift_from_date', '<=', $date],
    //                 ['shift_management.shift_to_date', '>=', $date],
    //                 ['shift_management.status', '=', 'Approved'],
    //                 ['shift_management.deleted_at', '=', null],
    //             ])
    //             ->select('shift_management.id','shift_management.work_week_id', 'shift_management.shift_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'shift_type.sandwich_absent', 'work_week.desc as work_week', 'shift_management.late_time_allowed', 'open_working_hours','shift_type.is_24_hour_shift')
    //             ->orderBy('shift_management.id', 'DESC')
    //             ->first();
    //         if (!empty($shift_data)) {
    //             $work_week_id = $shift_data->work_week_id;
    //             $shift_start_time = $shift_data->shift_start_time;
    //             $shift_end_time = $shift_data->shift_end_time;
    //             $late_time_allowed = $shift_data->late_time_allowed;
    //         } else {
    //             $shift_start_time = '09:00:00';
    //             $shift_end_time = '17:00:00';
    //             $late_time_allowed = 11;
    //             $work_week_id = 1;
    //         }
    //         $shift_start_datetime = "$date " . $shift_start_time;
    //         $shift_end_datetime = "$date " . $shift_end_time;
    //         $night_shift = false;
    //         if (date('H', strtotime($shift_start_time)) > date('H', strtotime($shift_end_time))) {
    //             $night_shift = true;
    //             $shift_end_datetime = date('Y-m-d H:i:s', strtotime($shift_end_datetime . ' +1 day'));
    //         }
    //         $attendance_date = $date;
    //         if (env('COMPANY') == 'JSML' && (date('H:i', strtotime($shift_start_time)) == '00:00' || date('H:i', strtotime($shift_start_time)) == '00:01')) {
    //             $attendance_date2 = date('Y-m-d', strtotime($date . ' -1 day'));
    //             // $shift_start_datetime = date('Y-m-d', strtotime($shift_start_datetime . ' -1 day'));
    //             $currentDayattendance = DB::table('attendance')
    //                 ->where('employee', $employee->id)
    //                 ->whereDate('in_time', "$attendance_date")
    //                 ->whereRaw('TIME(in_time) < ?', ['23:00:00'])
    //                 ->first();
    //             $previousDayAttendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$attendance_date2")->whereRaw('TIME(in_time) > ?', ['23:00:00'])->first();
    //             $attendance = null;
    //             if (!empty($currentDayattendance)) {
    //                 $attendance = $currentDayattendance;
    //             }
    //             if (!empty($previousDayAttendance)) {
    //                 $attendance = $previousDayAttendance;
    //             }
    //         } else {
    //             if (env('COMPANY') == 'JSML') {
    //                 $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$attendance_date")->whereRaw('TIME(in_time) < ?', ['23:00:00'])->orderBy('id', 'ASC')->first();
    //             } else {
    //                 if (env('COMPANY') == 'UNICORN' && $shift_data && in_array($shift_data->shift_id, $unicornDoubleAttendanceShifts)) {
    //                     $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$attendance_date")->orderBy('id', 'ASC')->get();
    //                 }
    //                 else{
    //                     $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$attendance_date")->orderBy('id', 'ASC')->first();
    //                 }
    //             }
    //         }
    //         if(!empty($attendance)){
    //             if (!empty($attendance->in_time) && !empty($attendance->out_time)) {
    //                 $attend_time_in = $attendance->in_time;
    //                 $attend_time_out = $attendance->out_time;
    //                 $e_grace_period = date("Y-m-d H:i:s", strtotime("+$late_time_allowed minutes", strtotime($shift_start_datetime)));
    //                 // because JSML want to calculate hours according to in time
    //                 if (strtotime($attend_time_in) < strtotime($e_grace_period) && env('COMPANY') != 'JSML') {
    //                     $attend_time_in = $shift_start_datetime;
    //                 }
    //                 // because JSML want to calculate hours according to out time
    //                 if (strtotime($attend_time_out) > strtotime($shift_end_datetime) && env('COMPANY') != 'JSML') {
    //                     $attend_time_out = $shift_end_datetime;
    //                 }
    //                 $working_hours = Carbon::parse($attend_time_in)->floatDiffInHours($attend_time_out);
    //                 $shift_working_hours = Carbon::parse($shift_start_datetime)->floatDiffInHours($shift_end_datetime);
    //                 if(!empty($shift_data) && $shift_data->is_24_hour_shift == 1){
    //                     $shift_working_hours = 8;
    //                 }
    //                 // if(env('COMPANY') == 'HEALTHWISE'){
    //                 //     $work_week_days = DB::table('workdays')->where([
    //                 //         ['work_week_id', $work_week_id],
    //                 //         ['status', '=', 'Non-working Day'],
    //                 //     ])->pluck('name')->toArray();
    //                 //     if (in_array(date('l', strtotime("$date")), $work_week_days)) {
    //                 //         $working_hours = 0;
    //                 //     }
    //                 // }
    //                 if(env('COMPANY') == 'JSML'){
    //                     $carbonDate = Carbon::parse($date);
    //                     $shift_working_hours = $shift_working_hours - 0.5;
    //                     if($working_hours == ($shift_working_hours / 2) && !empty($attendance->ret)){
    //                         $total_working_days += 0.5;
    //                     }
    //                     // Exclude Half Hour From Shift Time
    //                     else if($working_hours >= ($shift_working_hours / 2) && $working_hours < $shift_working_hours && !$carbonDate->isFriday()){
    //                         $total_working_days += 0.5;
    //                     }
    //                     else{
    //                         $total_working_days = checkHalfLeave($employee->id, $date) ? $total_working_days + 0.5 : ($working_hours >= ($shift_working_hours / 2) ? $total_working_days + 1 : $total_working_days+0);
    //                     }
    //                 }
    //                 else{
    //                     $total_working_days += 1;
    //                 }
    //             }
    //             else{
    //                 $total_working_days += 1;
    //             }
    //         }
    //         else{
    //             $employee_leave = DB::table('employeeleaves')->where([['employee', $employee->id],['date_start', '<=', "$date"],['date_end', '>=', "$date"],['status', '=', 'Approved']])->first();
    //             if(!empty($employee_leave)){
    //                 $total_working_days = isset($employee_leave->is_half) && $employee_leave->is_half == 1 ? $total_working_days+0.5 : $total_working_days + 1;
    //             }
    //             else{
    //                 $work_week_days = DB::table('workdays')->where([
    //                     ['work_week_id', $work_week_id],
    //                     ['status', '=', 'Non-working Day'],
    //                 ])->pluck('name')->toArray();
    //                 $previous_date = date('Y-m-d', strtotime('' . $date . ' -1 day'));
    //                 $next_date = date('Y-m-d', strtotime('' . $date . ' +1 day'));
    //                 if (in_array(date('l', strtotime("$date")), $work_week_days)) {
    //                     if (!empty($shift_data) && $shift_data->sandwich_absent == 1) {
    //                         // Previous Attendance
    //                         $previous_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$previous_date")->first(['id']);
    //                         // Next Attendance
    //                         $next_attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', "$next_date")->first(['id']);
    //                         // Previous leave
    //                         $previousLeave = DB::table('employeeleaves')
    //                             ->where([
    //                                 ['employee', $employee->id],
    //                                 ['date_start', '<=', "$previous_date"],
    //                                 ['date_end', '>=', "$previous_date"],
    //                                 ['status', '=', 'Approved'],
    //                             ])->value('id');
    //                         // Next leave
    //                         $nextLeave = DB::table('employeeleaves')
    //                             ->where([
    //                                 ['employee', $employee->id],
    //                                 ['date_start', '<=', "$next_date"],
    //                                 ['date_end', '>=', "$next_date"],
    //                                 ['status', '=', 'Approved'],
    //                             ])->value('id');
    //                         // previous day holiday
    //                         $previous_holiday = DB::table('holidays')->where('dateh', "$previous_date")->first();
    //                         $previousHolidaySetting = holidaySettingForEmployee($employee, $previous_holiday);
    //                         // next day holiday
    //                         $next_holiday = DB::table('holidays')->where('dateh', "$next_date")->first();
    //                         $nextHolidaySetting = holidaySettingForEmployee($employee, $next_holiday);

    //                         if(!empty($previousLeave) || !empty($nextLeave) || !empty($previous_attendance) || !empty($next_attendance) || (!empty($previous_holiday_qry) && $previousHolidaySetting) || (!empty($next_holiday_qry) && $nextHolidaySetting)){
    //                             $total_working_days = (isset($previousLeave->is_half) && $previousLeave->is_half == 1) || (isset($nextLeave->is_half) && $nextLeave->is_half == 1) ? $total_working_days + 0.5 : $total_working_days + 1;
    //                         }
    //                         else{
    //                             if($this->checkHoliday($date,$employee->id,$employee) == 'yes'){
    //                                 $total_working_days = $total_working_days + 1;
    //                             }
    //                             else{
    //                                 if($start_date == $end_date){
    //                                     if($this->checkIfLastDayIsOff($date,$employee->id) == 'yes'){
    //                                         $total_working_days = $total_working_days + 1;
    //                                     }
    //                                 }
    //                             }
    //                         }
    //                     }
    //                     else{
    //                         $total_working_days = $total_working_days + 1;
    //                     }
    //                 }
    //                 else{
    //                     if($this->checkHoliday($date,$employee->id,$employee) == 'yes'){
    //                         if (!empty($shift_data) && $shift_data->sandwich_absent == 1) {
    //                             if(!empty($previousLeave) || !empty($nextLeave) || !empty($previous_attendance) || !empty($next_attendance) || (!empty($previous_holiday_qry) && $previousHolidaySetting) || (!empty($next_holiday_qry) && $nextHolidaySetting)){
    //                                 $total_working_days = $total_working_days + 1;
    //                             }
    //                         }
    //                         else{
    //                             $total_working_days = $total_working_days + 1;
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //         // if($date == '2024-05-01'){
    //         //     dd($total_working_days);
    //         // }
    //     }
    //     return $total_working_days;
    // }

    public function late_hours($employee, $payroll){
        return employeeLateHoursInAMonth($employee->id, $payroll->date_start, $payroll->date_end);
    }
}


