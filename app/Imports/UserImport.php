<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Employee;
use App\Models\User;

class UserImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $employees = [];
        
    $rows = $rows->skip(1);


    foreach ($rows as $row) {
        // dd($row);
        $employee = new Employee();
        $employee->employee_id = $row[0];
        $employee->employee_code = $row[0];
        $employee->initials = $row[1];
        $employee->first_name = $row[2];
        $excelDate2 = $row[8];

        // Excel's base date is January 1, 1900
        $excelBaseDate = \DateTime::createFromFormat('Y-m-d', '1899-12-30');
        
        // Add the number of days to the base date
        $regularDate = $excelBaseDate->modify("+$excelDate2 days");
        
        $employee->birthday = $regularDate->format('Y-m-d');
        $employee->middle_name = $row[62];
        $employee->last_name = $row[3];
        $employee->gender = $row[9];
        $employee->private_email = $row[5];
        $employee->work_email = $row[5];
        $employee->nic_num = $row[12];
        $employee->mobile_phone = $row[7];
        $employee->address1 = $row[16] . ' ' . $row[17] . ' ' . $row[18];
        $employee->postal_code = $row[19];        
        $employee->postal_add_1 = $row[27];
        $employee->postal_add_2 = $row[29];
        $employee->postal_add_3 = $row[30];
        $employee->postal_code_1 = $row[31];
        $employee->country = $row[24];
        $employee->province = $row[23];
        $employee->postal_province = $row[35];
        $employee->postal_country = $row[36];
        $excelDate = $row[39];

        // Excel's base date is January 1, 1900
        $excelBaseDate = \DateTime::createFromFormat('Y-m-d', '1899-12-30');
        
        // Add the number of days to the base date
        $regularDate = $excelBaseDate->modify("+$excelDate days");
        
        $employee->position_effective_date = $regularDate->format('Y-m-d');
        // dd($employee->position_effective_date);
        $employee->tax_status_description = $row[40];
        $employee->account_number = $row[45];
        $employee->bank_id = $row[42];
        $employee->bank_name = $row[42];
        $employee->account_type = $row[43];
        $employee->branch_code = $row[44];
        $employee->tax_no = $row[48];
        $employee->title = $row[56];
        $employee->marital_status = $row[59];
        $employee->region_description = $row[63];
        $employee->preferred_name = $row[64];
        $employee->eefunction_description = $row[69];
        $employee->language = $row[76];
        $employee->citizenship = $row[77];
        $employee->job_title = $row[80];
        // dd($row[80]);
        $excelDate1 = $row[81];

        // Excel's base date is January 1, 1900
        $excelBaseDate = \DateTime::createFromFormat('Y-m-d', '1899-12-30');
        
        // Add the number of days to the base date
        $regularDate = $excelBaseDate->modify("+$excelDate1 days");
        
        $employee->position_filled_since = $regularDate->format('Y-m-d');

        $employee->not_re_employable = $row[82];
        $employee->uif_exemption_reason = $row[83];
        $employee->sdl_exemption_reason = $row[84];
        $employee->not_qualify_employment_tax = $row[97];
        $employee->coida_reference_number = $row[98];
        $employee->department=$row[101];
        $employee->director_for_coida = $row[102];
        $employee->exclude_for_coida = $row[103];
        $employee->exclude_from_eea_report = $row[104];
        $employee->exclude_from_uif_submission_file = $row[105];
        $employee->suspended=0;
        $employee->employment_status=$row[106];
        $employee->status=$row[107];
        $employee->nationality= $row[11];
        $employee->ethnicity= $row[10];
        $employee->save();
        $employees[] = $employee;
    }
    return $employees;
       
    }
}
