<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PayrollSalarySheetExport implements FromView, ShouldAutoSize
{
    protected $payroll = '';
    protected $column_amount = '';
    protected $employee_columns = '';
    protected $payroll_columns = '';
    protected $employees = '';
    protected $net_total = '';
    protected $salary_font = '';
    protected $prev_total_columns = '';
    protected $prev_net_total = '';

    public function __construct($payroll, $column_amount, $employee_columns, $payroll_columns, $employees, $net_total, $salary_font = '', $prev_total_columns = [], $prev_net_total = [])
    {
        $this->payroll = $payroll;
        $this->column_amount = $column_amount;
        $this->employee_columns = $employee_columns;
        $this->payroll_columns = $payroll_columns;
        $this->employees = $employees;
        $this->net_total = $net_total;
        $this->salary_font = $salary_font;
        $this->prev_total_columns = $prev_total_columns;
        $this->prev_net_total = $prev_net_total;
    }

    public function view(): View
    {
        if (env('COMPANY') == 'CLINIX') {
            $view = 'Admin.finance.payrollProcessing.clinix_payroll_salary_sheet';
        } else {
            $view = 'Admin.finance.payrollProcessing.payroll_salary_sheet';

        }
        return view($view, [
            'payroll' => $this->payroll,
            'column_amount' => $this->column_amount,
            'employee_columns' => $this->employee_columns,
            'payroll_columns' => $this->payroll_columns,
            'employees' => $this->employees,
            'net_total' => $this->net_total,
            'salary_font' => $this->salary_font,
            'prev_total_columns' => $this->prev_total_columns,
            'prev_net_total' => $this->prev_net_total,
        ]);
    }
}
