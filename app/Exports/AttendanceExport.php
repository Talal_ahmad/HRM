<?php

namespace App\Exports;

use App\Models\Attendance;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class AttendanceExport implements FromView, WithStyles
{
    protected $start_date = '';
    protected $end_date = '';
    protected $employees = '';
    protected $employe_attendance = '';
    protected $dept = '';
    protected $desg = '';
    protected $dateFilter = '';
    protected $singleDate = '';
    protected $minimized_undetail = '';

    public function __construct($start_date, $end_date, $employees, $employe_attendance, $dept, $desg, $dateFilter, $singleDate, $minimized_undetail='')
    {
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->employees = $employees;
        $this->employe_attendance = $employe_attendance;
        $this->dept = $dept;
        $this->desg = $desg;
        $this->dateFilter = $dateFilter;
        $this->singleDate = $singleDate;
        $this->minimized_undetail = $minimized_undetail;
    }
    public function view(): View
    {
        return view('Admin.attendanceReport.attendance_report_excel', [
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'employees' => $this->employees,
            'employe_attendance' => $this->employe_attendance,
            'dept' => $this->dept,
            'desg' => $this->desg,
            'dateFilter' => $this->dateFilter,
            'singleDate' => $this->singleDate,
            'minimized_undetail' => $this->minimized_undetail
        ]);
    }


    public function styles(Worksheet $sheet)
    {
        $sheet->getPageMargins()
            ->setTop(.2)
            ->setRight(.2)
            ->setBottom(.2)
            ->setLeft(.2);
        $sheet->getPageSetup()
            ->setOrientation(PageSetup::ORIENTATION_LANDSCAPE)
            ->setPaperSize(PageSetup::PAPERSIZE_A4)
            ->setFitToWidth(1) // Scale to fit all columns on one page
            ->setFitToHeight(0);
        $sheet->getStyle('A')->getAlignment()->setVertical('center');
        $sheet->getStyle('B')->getAlignment()->setVertical('center');
        $sheet->getStyle('C')->getAlignment()->setVertical('center');
        $sheet->getStyle('D')->getAlignment()->setVertical('center');
        $sheet->getStyle('E')->getAlignment()->setVertical('center');
        $sheet->getStyle('F')->getAlignment()->setVertical('center');
        $sheet->getStyle('G')->getAlignment()->setVertical('center');
        $sheet->getStyle('H')->getAlignment()->setVertical('center');
        $sheet->getStyle('I')->getAlignment()->setVertical('center');
        $sheet->getStyle('J')->getAlignment()->setVertical('center');
        $sheet->getStyle('K')->getAlignment()->setVertical('center');
        $sheet->getStyle('L')->getAlignment()->setVertical('center');
        $sheet->getStyle('M')->getAlignment()->setVertical('center');
        $sheet->getStyle('N')->getAlignment()->setVertical('center');
        $sheet->getStyle('O')->getAlignment()->setVertical('center');
        $sheet->getStyle('P')->getAlignment()->setVertical('center');
        $sheet->getStyle('Q')->getAlignment()->setVertical('center');
        $sheet->getStyle('R')->getAlignment()->setVertical('center');
        $sheet->getStyle('S')->getAlignment()->setVertical('center');
        $sheet->getStyle('T')->getAlignment()->setVertical('center');
        $sheet->getStyle('U')->getAlignment()->setVertical('center');
        $sheet->getStyle('V')->getAlignment()->setVertical('center');
        $sheet->getStyle('W')->getAlignment()->setVertical('center');
        $sheet->getStyle('X')->getAlignment()->setVertical('center');
        $sheet->getStyle('Y')->getAlignment()->setVertical('center');
        $sheet->getStyle('Z')->getAlignment()->setVertical('center');
        $sheet->getStyle('AA')->getAlignment()->setVertical('center');
        $sheet->getStyle('AB')->getAlignment()->setVertical('center');
        $sheet->getStyle('AC')->getAlignment()->setVertical('center');
        $sheet->getStyle('AD')->getAlignment()->setVertical('center');
        $sheet->getStyle('AE')->getAlignment()->setVertical('center');
        $sheet->getStyle('AF')->getAlignment()->setVertical('center');
        $sheet->getStyle('AG')->getAlignment()->setVertical('center');
        $sheet->getStyle('AH')->getAlignment()->setVertical('center');
        $sheet->getStyle('AI')->getAlignment()->setVertical('center');
        $sheet->getStyle('AJ')->getAlignment()->setVertical('center');
        $sheet->getStyle('AK')->getAlignment()->setVertical('center');
        $sheet->getStyle('AL')->getAlignment()->setVertical('center');

        $sheet->getStyle('A1')->getFont()->setSize(14); // Set the font size for the header row
        $sheet->getStyle('A2:' . $sheet->getHighestDataColumn() . $sheet->getHighestDataRow())->getFont()->setSize(14); // Set the font size for the data rows

        $sheet->calculateWorksheetDimension();
        $dataRange = $sheet->getHighestDataColumn() . $sheet->getHighestDataRow();

        $sheet->getStyle('A1:' . $dataRange)
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN);
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
            2    => ['font' => ['bold' => true]],
            'A' => ['alignment' => ['wrapText' => true]],
            'B' => ['alignment' => ['wrapText' => true]],
            'C' => ['alignment' => ['wrapText' => true]],
            'D' => ['alignment' => ['wrapText' => true]],
            'E' => ['alignment' => ['wrapText' => true]],
            'F' => ['alignment' => ['wrapText' => true]],
            'G' => ['alignment' => ['wrapText' => true]],
            'H' => ['alignment' => ['wrapText' => true]],
            'I' => ['alignment' => ['wrapText' => true]],
            'J' => ['alignment' => ['wrapText' => true]],
            'K' => ['alignment' => ['wrapText' => true]],
            'L' => ['alignment' => ['wrapText' => true]],
            'M' => ['alignment' => ['wrapText' => true]],
            'N' => ['alignment' => ['wrapText' => true]],
            'O' => ['alignment' => ['wrapText' => true]],
            'P' => ['alignment' => ['wrapText' => true]],
            'Q' => ['alignment' => ['wrapText' => true]],
            'R' => ['alignment' => ['wrapText' => true]],
            'S' => ['alignment' => ['wrapText' => true]],
            'T' => ['alignment' => ['wrapText' => true]],
            'U' => ['alignment' => ['wrapText' => true]],
            'V' => ['alignment' => ['wrapText' => true]],
            'W' => ['alignment' => ['wrapText' => true]],
            'X' => ['alignment' => ['wrapText' => true]],
            'Y' => ['alignment' => ['wrapText' => true]],
            'Z' => ['alignment' => ['wrapText' => true]],
            'AA' => ['alignment' => ['wrapText' => true]],
            'AB' => ['alignment' => ['wrapText' => true]],
            'AC' => ['alignment' => ['wrapText' => true]],
            'AD' => ['alignment' => ['wrapText' => true]],
            'AE' => ['alignment' => ['wrapText' => true]],
            'AF' => ['alignment' => ['wrapText' => true]],
            'AG' => ['alignment' => ['wrapText' => true]],
            'AH' => ['alignment' => ['wrapText' => true]],
            'AI' => ['alignment' => ['wrapText' => true]],
            'AJ' => ['alignment' => ['wrapText' => true]],
            'AK' => ['alignment' => ['wrapText' => true]],
            'AL' => ['alignment' => ['wrapText' => true]],
        ];
    }
}
