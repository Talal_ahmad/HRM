<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;

class SalarySheetExport implements FromView
{
    protected $calculation_group_payroll = '';
    protected $net_salary = '';
    protected $departments = '';
    protected $employees = '';
    protected $columns_totals = '';
    protected $deducation_column = '';
    protected $bank_show = '';
    // SalarySheetExport
    protected $where = '';
    protected $payrol_ids_str = '';

    public function __construct($calculation_group_payroll,$net_salary,$total_days,$gross_salary,$sub_total_net_gross_salary,$sub_total_deducations,$sub_total_net_salary,$deducation_column,$departments,$employees,$bank_show,$where,$payrol_ids_str) {
        $this->calculation_group_payroll = $calculation_group_payroll;
        $this->net_salary = $net_salary;
        $this->total_days = $total_days;
        $this->gross_salary = $gross_salary;
        $this->sub_total_net_gross_salary = $sub_total_net_gross_salary;
        $this->sub_total_net_salary = $sub_total_net_salary;
        $this->sub_total_deducations = $sub_total_deducations;
        $this->deducation_column = $deducation_column;
        $this->departments = $departments;
        $this->employees = $employees;
        $this->bank_show = $bank_show;
        $this->where = $where;
        $this->payrol_ids_str = $payrol_ids_str;
        // $where
        // $employement_status
    }

    public function view(): View
    {
        return view('Admin.reports.bank_salary_sheet_report_excel', [
            'calculation_group_payroll' =>$this->calculation_group_payroll, 
            'total_days' =>$this->total_days, 
            'net_salary' =>$this->net_salary, 
            'departments' => $this->departments,
            'deducation_column' => $this->deducation_column,
            'sub_total_net_gross_salary' => $this->sub_total_net_gross_salary,
            'sub_total_net_salary' => $this->sub_total_net_salary,
            'sub_total_deducations' => $this->sub_total_deducations,
            'gross_salary' => $this->gross_salary,
            'employees' => $this->employees,
            'bank_show' => $this->bank_show,
            'where' => $this->where,
            'payrol_ids_str' => $this->payrol_ids_str
        ]);
    }
}
