<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\WithStyles;
// use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class BankSalaryExport implements FromView
{
    protected $calculation_group_payroll = '';
    protected $net_salary = '';
    protected $departments = '';
    protected $employees = '';
    protected $columns_totals = '';
    protected $employement_status = '';
    protected $payrol_ids_str = '';

    public function __construct($calculation_group_payroll,$net_salary,$departments,$employees,$columns_totals,$payrol_ids_str) {
        $this->calculation_group_payroll = $calculation_group_payroll;
        $this->net_salary = $net_salary;
        $this->departments = $departments;
        $this->columns_totals = $columns_totals;
        $this->employees = $employees;
        $this->payrol_ids_str = $payrol_ids_str;
    }

    public function view(): View
    {
        return view('Admin.reports.bank_salary_report_excel', [
            'calculation_group_payroll' =>$this->calculation_group_payroll, 
            'net_salary' =>$this->net_salary, 
            'departments' => $this->departments,
            'columns_totals' => $this->columns_totals,
            'employees' => $this->employees,
            'payrol_ids_str' => $this->payrol_ids_str,
        ]);
    }
}