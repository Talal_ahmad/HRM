<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DepartmentWiseSalaryExport implements FromView,WithColumnWidths,WithStyles
{
    protected $calculation_group_payroll = '';
    protected $payroll_columns = '';
    protected $departments = '';
    protected $employees = '';
    protected $columns_totals = '';
    protected $payrollIds = '';
    protected $request = '';
    protected $date = '';
    protected $cal_group = '';

    public function __construct($calculation_group_payroll,$payroll_columns,$departments,$employees,$columns_totals, $date , $cal_group , $payrollIds , $request) {
        $this->calculation_group_payroll = $calculation_group_payroll;
        $this->payroll_columns = $payroll_columns;
        $this->departments = $departments;
        $this->employees = $employees;
        $this->columns_totals = $columns_totals;
        $this->request = $request;
        $this->date = $date;
        $this->cal_group = $cal_group;
        $this->payrollIds = $payrollIds;
    }

    public function view(): View
    {
        return view('Admin.reports.department_wise_salary_excel', [
            'calculation_group_payroll' =>$this->calculation_group_payroll, 
            'payroll_columns' =>$this->payroll_columns, 
            'payroll_columns' =>$this->payroll_columns, 
            'departments' => $this->departments,
            'employees' => $this->employees,
            'columns_totals' => $this->columns_totals,
            'request' => $this->request,
            'date' => $this->date,
            'cal_group' => $this->cal_group,
            'payrollIds' => $this->payrollIds,
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 10,
            'B' => 35,            
            'C' => 22,            
            'D' => 22,            
            'E' => 22,            
            'F' => 22,            
            'G' => 22,            
            'H' => 22,            
            'I' => 22,            
            'J' => 22,            
            'K' => 22,            
            'L' => 22,            
            'M' => 22,            
            'N' => 22,            
            'O' => 22,            
            'P' => 22,            
            'Q' => 22,            
            'R' => 22,            
            'S' => 22,            
            'T' => 22,            
            'U' => 22,            
            'V' => 22,            
            'W' => 22,            
            'X' => 22,            
            'Y' => 22,            
            'Z' => 22,            
            'AA' => 22,            
            'AB' => 22,            
            'AC' => 22,            
            'AD' => 22,            
            'AE' => 22,            
            'AF' => 22,            
            'AG' => 22,            
            'AH' => 22,            
            'AI' => 22,            
            'AJ' => 10,            
            'AK' => 10,            
            'AL' => 10            
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A')->getAlignment()->setVertical('center');
        $sheet->getStyle('B')->getAlignment()->setVertical('center');
        $sheet->getStyle('C')->getAlignment()->setVertical('center');
        $sheet->getStyle('D')->getAlignment()->setVertical('center');
        $sheet->getStyle('AJ')->getAlignment()->setVertical('center');
        $sheet->getStyle('AK')->getAlignment()->setVertical('center');
        $sheet->getStyle('AL')->getAlignment()->setVertical('center');
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
            'A' => ['alignment' => ['wrapText' => true]],
            'B' => ['alignment' => ['wrapText' => true]],
            'C' => ['alignment' => ['wrapText' => true]],
            'D' => ['alignment' => ['wrapText' => true]],
            'E' => ['alignment' => ['wrapText' => true]],
            'F' => ['alignment' => ['wrapText' => true]],
            'G' => ['alignment' => ['wrapText' => true]],
            'H' => ['alignment' => ['wrapText' => true]],
            'I' => ['alignment' => ['wrapText' => true]],
            'J' => ['alignment' => ['wrapText' => true]],
            'K' => ['alignment' => ['wrapText' => true]],
            'L' => ['alignment' => ['wrapText' => true]],
            'M' => ['alignment' => ['wrapText' => true]],
            'N' => ['alignment' => ['wrapText' => true]],
            'O' => ['alignment' => ['wrapText' => true]],
            'P' => ['alignment' => ['wrapText' => true]],
            'Q' => ['alignment' => ['wrapText' => true]],
            'R' => ['alignment' => ['wrapText' => true]],
            'S' => ['alignment' => ['wrapText' => true]],
            'T' => ['alignment' => ['wrapText' => true]],
            'U' => ['alignment' => ['wrapText' => true]],
            'V' => ['alignment' => ['wrapText' => true]],
            'W' => ['alignment' => ['wrapText' => true]],
            'X' => ['alignment' => ['wrapText' => true]],
            'Y' => ['alignment' => ['wrapText' => true]],
            'Z' => ['alignment' => ['wrapText' => true]],
            'AA' => ['alignment' => ['wrapText' => true]],
            'AB' => ['alignment' => ['wrapText' => true]],
            'AC' => ['alignment' => ['wrapText' => true]],
            'AD' => ['alignment' => ['wrapText' => true]],
            'AE' => ['alignment' => ['wrapText' => true]],
            'AF' => ['alignment' => ['wrapText' => true]],
            'AG' => ['alignment' => ['wrapText' => true]],
            'AH' => ['alignment' => ['wrapText' => true]],
            'AI' => ['alignment' => ['wrapText' => true]],
            'AJ' => ['alignment' => ['wrapText' => true]],
            'AK' => ['alignment' => ['wrapText' => true]],
            'AL' => ['alignment' => ['wrapText' => true]]
        ];
    }
}