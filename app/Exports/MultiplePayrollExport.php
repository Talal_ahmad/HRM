<?php

namespace App\Exports;

use App\Models\Payroll;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class MultiplePayrollExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $payroll_columns = '';
    protected $employees = '';
    protected $payroll_data = '';
    protected $column_amount = '';
    protected $net_total = '';

    public function __construct($payroll_columns,$employees,$payroll_data,$column_amount,$net_total) {
            $this->payroll_columns = $payroll_columns;
            $this->employees = $employees;
            $this->payroll_data = $payroll_data;
            $this->column_amount = $column_amount;
            $this->net_total = $net_total;
    }
    public function view(): View
    {
        return view('Admin.finance.payrollProcessing.salary_sheet_excel', [
            'payroll_columns' =>$this->payroll_columns, 
            'employees' => $this->employees, 
            'payroll_data' => $this->payroll_data,
            'column_amount' => $this->column_amount,
            'net_total' => $this->net_total
        ]);
    }
}
