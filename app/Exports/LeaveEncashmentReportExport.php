<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;

class LeaveEncashmentReportExport implements FromView
{
    public function __construct(protected $employees,protected $annual_leave_type)
    {
        $this->employees = $employees;
        $this->annual_leave_type = $annual_leave_type;
    }
    public function view(): View
    {
        return view('Admin.reports.annual_leave_encashment_report_excel', [
            'employees' => $this->employees,
            'annual_leave_type' => $this->annual_leave_type
        ]);
    }
}
