<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;

class GrossSalaryExport implements FromView
{
    protected $calculation_group_payroll = '';
    protected $departments = '';
    protected $employees = '';
    protected $columns_totals = '';
    protected $bank_show = '';
    protected $total_days = '';
    protected $gross_salary = '';
    protected $sub_total_net_gross_salary = '';
    // SalarySheetExport
    protected $where = '';

    public function __construct($calculation_group_payroll,$total_days,$gross_salary,$sub_total_net_gross_salary,$departments,$employees,$bank_show,$where) {
        $this->calculation_group_payroll = $calculation_group_payroll;
        $this->total_days = $total_days;
        $this->gross_salary = $gross_salary;
        $this->sub_total_net_gross_salary = $sub_total_net_gross_salary;
        $this->departments = $departments;
        $this->employees = $employees;
        $this->bank_show = $bank_show;
        $this->where = $where;
        // $where
        // $employement_status
    }

    public function view(): View
    {
        return view('Admin.reports.gross_salary_sheet_report_excel', [
            'calculation_group_payroll' =>$this->calculation_group_payroll, 
            'total_days' =>$this->total_days, 
            'departments' => $this->departments,
            'sub_total_net_gross_salary' => $this->sub_total_net_gross_salary,
            'gross_salary' => $this->gross_salary,
            'employees' => $this->employees,
            'bank_show' => $this->bank_show,
            'where' => $this->where
        ]);
    }
}
