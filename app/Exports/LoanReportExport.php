<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LoanReportExport implements FromView, ShouldAutoSize
{
    public function __construct(protected $loans, protected $months)
    {
        $this->loans = $loans;
        $this->months = $months;
    }
    public function view(): View
    {
        return view('Admin.request&approvals.loanReport.excel', [
            'loans' => $this->loans,
            'months' => $this->months
        ]);
    }
}
