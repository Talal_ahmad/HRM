<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidateUrlAndDb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $project_name = explode('/', $_SERVER['REQUEST_URI'])[1];
        $project_name = explode('hrm',$project_name)[0];
        $db_name = $project_name.'_hrm';
        $session_db = \Session::get('db_name');
        if ($db_name != $session_db) {
            $request->session()->flush();
            \Auth::logout();
        }
        return $next($request);
    }
}
