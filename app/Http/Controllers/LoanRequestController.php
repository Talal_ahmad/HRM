<?php

namespace App\Http\Controllers;

use DataTables;
use GuzzleHttp\Client;
use App\Models\Employee;
use App\Models\LoanRequest;
use App\Models\GlSetting;
use App\Models\Ledger;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\LoanInstallment;
use App\Models\CalculationGroup;
use App\Models\LoanPaymentDetail;
use App\Models\CompanyBank;
use App\Services\ErpIntegrationService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;

class LoanRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    } 
    public function index(Request $request)
    {
        $env = env('COMPANY');
        if($request->ajax()){
            $query = LoanRequest::join('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
            ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->leftjoin('loan_installments' , 'loan_requests.id' , '=' , 'loan_installments.loan_request_id')
            ->leftjoin('deductiongroup' , 'loan_requests.deduction_group' , '=' , 'deductiongroup.id')
            ->join('loan_types' , 'loan_types.id' , '=' , 'loan_requests.loan_type')
            ->leftjoin('loan_payment_details' , 'loan_payment_details.loan_request_id' , '=' , 'loan_requests.id')
            ->whereIn('employees.department' , login_user_departments())
            ->select('loan_requests.*' , 'deductiongroup.name as deduction' ,'employees.suspended' , 'employees.status AS emp_status' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.employee_id as employeeId', 'employees.employee_code', 'companystructures.title','loan_types.name as loan_type_name','loan_payment_details.id as payment_id',DB::raw('count(*) as total_installment, loan_installments.id as installment_id'))->groupBy('loan_requests.id');
            if (userRoles()=="Self Service") {
                $query->where('loan_requests.employee_id' , Auth::user()->employee);
            }
            return DataTables::eloquent($query)->filter(function ($query) use ($request,$env) {
                if(!empty(request()->departmentFilter)){
                    if ($request->departmentFilter != 'all') {
                        if($env == 'JSML')
                        {
                            // $all_childs =  getChildren($request->departmentFilter);
                            // array_push($all_childs,$request->departmentFilter);
                            $query->whereIn('employees.department',$request->section);
                        }
                        else{
                                $query->where('employees.department' , request()->departmentFilter);
                            }
                    }
                }
                if($request->typeFilter !='all' && userRoles()!="Self Service") {
                    $query->where('loan_requests.loan_type' , request()->typeFilter);
                }
                if(!empty(request()->employeeFilter)){
                    $query->where('loan_requests.employee_id' , request()->employeeFilter);
                }
                if(!empty($request->dedFromDateFilter) && !empty($request->dedToDateFilter)){
                    $query->whereBetween('loan_requests.start_date', [$request->dedFromDateFilter , $request->dedToDateFilter]);
                }
                if (userRoles()!="Self Service") {
                    if(!empty($request->fromDateFilter) && !empty($request->toDateFilter)){
                        $query->whereBetween(DB::raw("DATE(loan_requests.created_at)") , [$request->fromDateFilter , $request->toDateFilter]);
                        // $query->where(function ($query) use ($date) {
                        //     $query->whereNull('employees.termination_date')->orWhere(function ($query) use ($date) {
                        //         $month = !empty(request()->date) ?  date('m' , strtotime(request()->date)) : date('m');
                        //         $year = !empty(request()->date) ?  date('Y' , strtotime(request()->date)) : date('Y');
                        //         $query->whereNotNull('employees.termination_date');
                        //         $query->where(DB::raw("MONTH(employees.termination_date)"), '>=', $month);
                        //     });
                        // });
                    }
                }
                if(!empty($request->statusFilter)){
                    if($request->statusFilter == "unpaid"){
                        $query->whereNull('loan_payment_details.id');
                    }
                    else{
                        $query->whereNotNull('loan_payment_details.id');
                    }
                }
                
            },true)->addIndexColumn()->make(true);
        }
        $loan_types = DB::table('loan_types')->get();
        $calculationGroup = CalculationGroup::get(['id', 'name']);
        $company_banks = CompanyBank::get(['id','type', 'bank_name','erp_bank_id']);
        return view('Admin.request&approvals.loan_request.index' , \get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            if($request->type == 'Payment'){
                $this->validate($request , [
                    'loan_request_id' => 'required',
                    'payment_method' => 'required',
                    'payment_date' => 'required',
                ]);
                
                $loan_payment = new LoanPaymentDetail();
                $loan_payment->loan_request_id = $request->loan_request_id;
                $loan_payment->fair_price_customer_id = $request->payment_method == 'fair_price_advance' ? $request->fair_price_customer_id : NULL;
                $loan_payment->fair_price_customer = $request->fair_price_customer ?? NULL;
                $loan_payment->loan_amount = $request->loan_amount;
                $loan_payment->payment_method = $request->payment_method;
                $loan_payment->payment_date = $request->payment_date;
                $loan_payment->company_bank_id = !empty($request->bank_id) ? $request->bank_id : $request->cash_account;
                $loan_payment->cheque_book = !empty($request->cheque_book) ? $request->cheque_book : NULL;
                $loan_payment->checque_no = !empty($request->cheque_no) ? $request->cheque_no : $request->cheque_no;
                $loan_payment->cheque_date = $request->checque_date;
                if($request->hasFile('attachment')){
                    $file = $request->file('attachment');
                    $extension = $file->getClientOriginalExtension();
                    $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                    $file->move('images/loan_details/' , $temp_name);
                    $loan_payment->attachment = $temp_name;
                }
                $loan_payment->remarks = $request->remarks;
                $loan_payment->created_by = Auth::id();
                $loan_payment->save();
                $loanRequest = LoanRequest::with('loanType')->find($loan_payment->loan_request_id);
                $employee = DB::select("SELECT employees.first_name,employees.last_name,employees.job_title as designation_id,employees.employee_code,employees.bank_name,employees.account_title,employees.account_number,jobtitles.name as designation,employees.department as department_id,companystructures.title as department_name,employees.employment_status
                from employees
                join companystructures on employees.department = companystructures.id
                join jobtitles on employees.job_title = jobtitles.id
                where employees.id  = $loanRequest->employee_id")[0];
                if($request->payment_method == 'fair_price_advance' && $loanRequest->loanType->id == 5){
                    $setting = GlSetting::whereRaw("(JSON_SEARCH(employment_status_id, 'one', $employee->employment_status) IS NOT NULL OR JSON_SEARCH(department_id, 'one', $employee->department_id) IS NOT NULL) AND `category` = 'fair_price_advance'")->first();
                }
                if($loanRequest->loanType->id == 3){
                    $setting = GlSetting::whereRaw("(JSON_SEARCH(employment_status_id, 'one', $employee->employment_status) IS NOT NULL OR JSON_SEARCH(department_id, 'one', $employee->department_id) IS NOT NULL) AND `category` = 'loan_payment'")->first();
                }
                if($loanRequest->loanType->id == 4){
                    $setting = GlSetting::whereRaw("(JSON_SEARCH(employment_status_id, 'one', $employee->employment_status) IS NOT NULL OR JSON_SEARCH(department_id, 'one', $employee->department_id) IS NOT NULL) AND `category` = 'advance_payment'")->first();
                }
                if (empty($setting)) {
                    throw new \Exception('Gl Setting Not Found!');
                }
                if($setting->column_type == 'credit'){
                    $amount = -$loan_payment->loan_amount;
                }
                else{
                    $amount = $loan_payment->loan_amount;
                }
                $balance = $amount; 
                $ledger = Ledger::create([
                    'loan_payment_detail_id' => $loan_payment->id,
                    'employee_id' => $loanRequest->employee_id,
                    'employee' => "{$employee->employee_code}-{$employee->first_name} {$employee->last_name}",
                    'designation_id' => $employee->designation_id,
                    'designation' => $employee->designation,
                    'department_id' => $employee->department_id,
                    'department' => $employee->department_name,
                    'employee_bank' => $employee->bank_name,
                    'account_title' => $employee->account_title,
                    'account_number' => $employee->account_number,
                    'type' => $setting->category,
                    'payment_banks_id' => json_encode($loan_payment->companyBank->erp_bank_id),
                    'payment_banks' => $loan_payment->companyBank->bank_name,
                    'amount' => $amount,
                    'balance' => $balance,
                    'gl_account_type' => $setting->column_type,
                    'gl_account' => $setting->gl_accounts,
                    'description' => $request->remarks,
                    'user_id' => Auth::id(),
                    'trans_date' => date('Y-m-d'),
                ]);
                if(env('COMPANY') == 'JSML'){
                    if($request->payment_method != 'fair_price_advance'){
                        ErpIntegrationService::SalaryLoanAdvancePaymentEntry($request, $ledger, $loan_payment);
                    }
                    if($request->payment_method == 'fair_price_advance'){
                        ErpIntegrationService::createFairPriceAdvanceJvEntry($request, $ledger, $loan_payment);
                    }
                }
                DB::commit();
                return ['code' => 200 , 'message' => 'success'];
            }
            else{
                $this->validate($request , [
                    'employee_id' => 'required',
                    'loan_type' => 'required',
                    'start_date' => 'required',
                    'amount' => 'required',
                    'time_duration' => 'required',
                    'remarks' => 'required',
                ]);

                $loanRequest = $request->all();
                $loanRequest['submitted_by'] =  Auth::id();
                $monthly_installment = $request->amount/$request->time_duration;
                $loanRequest['monthly_installment'] = $monthly_installment;
                $loanRequest['remaining_amount'] = $request->amount;
                $loanRequest['loan_no'] = !empty(LoanRequest::max('id')) ? LoanRequest::max('id') : 1;
                $loanRequest['status'] = isset($request->status) ? $request->status : 'Pending';
                $loanRequest['status_changed_by'] = Auth::id();
                $loanRequest['status_changed_at'] = Carbon::now();
                $loan = LoanRequest::create($loanRequest);
                if(!empty($request->status)){
                    $start_date = new Carbon($request->start_date);
                    for ($i=0; $i < $request->time_duration; $i++) { 
                        $checkInstallment = DB::table('loan_installments')->where('loan_request_id', $loan['id'])->whereYear('month', $start_date->year)->whereMonth('month', $start_date->format('m'))->first(['id']);
                        if(empty($checkInstallment)){
                            DB::table('loan_installments')->insert([
                                'loan_request_id' => $loan['id'],
                                'month' => $start_date->startOfMonth(),
                                'amount' => $monthly_installment,
                                'status' => 'active'
                            ]);
                        }
                        $start_date->addMonth();
                    }
                }
                DB::commit();
                return ['code' => 200 , 'message' => 'success'];
            }
        }
        catch(\Exception | ValidationException $e){
            DB::rollback();
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax()){
            $query = LoanRequest::join('loan_installments' , 'loan_installments.loan_request_id' , '=' , 'loan_requests.id')
            ->leftjoin('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
            ->leftjoin('users' , 'users.id' , '=' , 'loan_installments.changed_by')
            ->where('loan_requests.id' , $id)
            ->select('loan_installments.*' , 'users.username as changed_by');
            return DataTables::of($query)->addIndexColumn()->make(true);
        }
        $employee_data = LoanRequest::leftjoin('loan_types' , 'loan_types.id' , 'loan_requests.loan_type')->leftjoin('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')->where('loan_requests.id' , $id)->select('loan_requests.*' , 'loan_types.name AS typeName' ,  'employees.suspended' , 'employees.status AS emp_status' , 'employees.first_name' ,'employees.middle_name', 'employees.last_name' , 'employees.employee_id' , 'companystructures.title')->first();
        return view('Admin.request&approvals.loan_request.loan_installments.index' , get_defined_vars());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        if(request()->type == 'payment_details'){
            $payment_details = DB::table('loan_requests')->leftjoin('loan_payment_details' , 'loan_payment_details.loan_request_id' , '=' , 'loan_requests.id')
            ->leftjoin('company_banks' , 'company_banks.id' , '=' , 'loan_payment_details.company_bank_id')
            ->leftjoin('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
            ->leftjoin('users' , 'users.id' , '=' , 'loan_payment_details.created_by')
            ->where('loan_payment_details.id' , $id)
            ->first(['loan_payment_details.*' ,'company_banks.bank_name' , 'company_banks.account_name' , 'employees.first_name' , 'employees.last_name' , 'users.username']);
            return response()->json($payment_details);
        }
        else{
            $data = LoanRequest::find($id);
            return response()->json($data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id)
    {
        try{
            if(env('COMPANY') != 'RoofLine'){
                $this->validate($request , [
                    'employee_id' => 'required',
                    'loan_type' => 'required',
                    'start_date' => 'required',
                    'amount' => 'required',
                    'time_duration' => 'required',
                    'remarks' => 'required',
                ]);
            }
            $loan_request = LoanRequest::find($id);
            if(env('COMPANY') == 'RoofLine'){
                $loan_request->deduction_group = $request->deduction_group;
                $loan_request->update();
            }
            else{
                $loan_request->employee_id = $request->employee_id; 
                $loan_request->loan_type = $request->loan_type;
                $loan_request->start_date = $request->start_date;
                $loan_request->amount = $request->amount;
                $loan_request->time_duration = $request->time_duration;
                $loan_request->monthly_installment = $request->amount/$request->time_duration;
                $loan_request->remarks = $request->remarks;
                $loan_request->status = isset($request->status) ? $request->status : 'Pending';
                if($request->input('status')){
                    $loan_request['status_changed_by'] = Auth::id();
                    $loan_request['status_changed_at'] = Carbon::now();  
                }
                $loan_request->update();
                LoanInstallment::where('loan_request_id' , $id)->delete();
                if(!empty($request->status)){
                    $start_date = new Carbon($loan_request->start_date);
                    for ($i=0; $i < $loan_request->time_duration; $i++) {
                        $checkInstallment = DB::table('loan_installments')->where('loan_request_id', $id)->whereYear('month', $start_date->year)->whereMonth('month', $start_date->format('m'))->first(['id']);
                        if(empty($checkInstallment)){
                            DB::table('loan_installments')->insert([
                                'loan_request_id' => $id,
                                'month' => $start_date->startOfMonth(),
                                'amount' => $loan_request->amount/$loan_request->time_duration,
                                'status' => 'active'
                            ]);
                        }
                        $start_date->addMonth();
                    }
                }
            }
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            if(request()->type == 'installment'){
                LoanInstallment::where('id' , $id)->delete();
                return ['code'=>'200','message'=>'success'];
            }
            else{
                LoanRequest::find($id)->delete();
                LoanInstallment::where('loan_request_id' , $id)->delete();
                return ['code'=>'200','message'=>'success'];
            }
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }

    public function getCheckBookByBanks($bank_id)
    {
        try{
            $chequeBooks = getCheckBookByBanks($bank_id);
            return response()->json($chequeBooks);
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }

    public function getCheckBookByLeafs($cheque_book_id)
    {
        try{
            $chequeNo = getCheckBookByLeafs($cheque_book_id);
            return response()->json($chequeNo);
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }

    public function loanPaymentErpDetail($referenceNo)
    {
        return view('Admin.request&approvals.loan_request.paymenterpvoucher' , \get_defined_vars());
    }
}
