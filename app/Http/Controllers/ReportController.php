<?php

namespace App\Http\Controllers;

use Auth;
use Carbon;
use PDF;
use DataTables;
use App\Models\Attendance;
use App\Models\Payroll;
use App\Models\EmploymentStatus;
use App\Models\User;
use App\Models\Employee;
use App\Models\Ledger;
use App\Models\CompanyStructure;
use Carbon\CarbonPeriod;
use App\Models\LoanRequest;
use App\Models\LeaveTypes;
use App\Models\LeavePeriods;
use App\Models\PayrollData;
use App\Models\PayGrade;
use Illuminate\Http\Request;
use App\Models\PayrollColumn;
use App\Models\CalculationGroup;
use App\Exports\EobiMonthlyExport;
use App\Exports\EobiSummaryExport;
use App\Exports\SalarySheetExport;
use App\Exports\BankSalaryExport;
use App\Exports\GrossSalaryExport;
use App\Exports\LoanReportExport;
use Illuminate\Support\Facades\DB;
use App\Exports\PessiMonthlyExport;
use App\Exports\PessiSummaryExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DepartmentWiseSalaryExport;
use App\Exports\DesignationWiseReportExport;
use App\Exports\DepartmentWiseSalarySummeryExport;
use App\Exports\LeaveEncashmentReportExport;
use App\Imports\UserImport;

class ReportController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user_calculation_groups = !empty(Auth::user()->user_calculation_groups) ? json_decode(Auth::user()->user_calculation_groups) : [];
            return $next($request);
        });
    }
    public function bank_salary_sheet(Request $request)
    {
        // $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        // $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');
        if (!empty($request->fromDate)) {
            $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
            $fromDate = $date_from->format('F Y');
            $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
            $toDate = $date_to->format('F Y');
            $calculation_group_payroll = DB::table('payroll')->where([
                ['deduction_group' , $request->calculation_group],
                [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                // [DB::raw('YEAR(date_start)') , $month],
                // [DB::raw('MONTH(date_start)') , $month],
                ['status' , 'Completed']
            ])->get(['id as payroll_id','department','columns','deduction_group']);
        }
        if(!empty($calculation_group_payroll)){
            foreach ($calculation_group_payroll as $key => $calculation_group_payrolls) {
                $net_salary = DB::table('payrollcolumns')->where([
                    ['deduction_group', $calculation_group_payrolls->deduction_group],
                    ['salary_column_type', 'net_Salary']
                ])->first(['id']);
                $gross_salary = DB::table('payrollcolumns')->where([
                    ['deduction_group', $calculation_group_payrolls->deduction_group],
                    ['salary_column_type', 'gross_salary']
                ])->first(['id']);
                $deducation_column = DB::table('payrollcolumns')->where([
                    ['deduction_group', $calculation_group_payrolls->deduction_group],
                    ['salary_column_type', 'total_deductions']
                ])->first(['id']);
                $total_days = DB::table('payrollcolumns')->where([
                    ['deduction_group', 88],
                    ['salary_column_type', 'working_days']
                ])->first(['id']);
                // dd($total_days);
                $all_childs =  getChildren($calculation_group_payrolls->department);
                array_push($all_childs,$calculation_group_payrolls->department);
                $payrol_ids[] = $calculation_group_payrolls->payroll_id;
                $payrol_ids_str = implode(',', $payrol_ids);
            }
            $where = "";
            if(!empty($request->employment_status))
            {
                $employement_status = implode(',',$request->employment_status);
                $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
            }

            if($request->bank_status=='Bank')
            {
                $where .= " AND payrolldata.account_number IS NOT NULL AND payrolldata.account_number != ' '";
            }

            if($request->employeeFilter)
            {
                // $where .= " AND (payrolldata.account_number IS NULL OR payrolldata.account_number = ' ')";
                $where .= " AND employees.id = $request->employeeFilter";
            }
            if($request->bank_status=='Cash')
            {
                // $where .= " AND (payrolldata.account_number IS NULL OR payrolldata.account_number = ' ')";
                $where .= " AND payrolldata.bank_name = 'BY Cash'";
            }
            if(!empty($request->banksFilter))
            {
                $bank_ar = implode(',',$request->banksFilter);
                $where .= " AND payrolldata.bank_id IN ($bank_ar)";
            }
            if($request->departmentFilter){
                // dd($request->section);
                $department = implode(',',$request->section);
                $where .= " AND payrolldata.department_id IN ($department)";
            }
            // if($request->departmentFilter){
            //     $department = implode(',',$request->departmentFilter);
            //     $where .= " AND payrolldata.department_id IN ($department)";
            // }
            $OrderBy = '';
            if(!empty($request->order_filter)){
                $OrderBy .= " ORDER BY employees.$request->order_filter ASC";
            }
            $departments = CompanyStructure::whereIn('id', $request->section)->get(['id', 'title']);
            foreach ($departments as $key => $department) {
                $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.termination_date,employees.off_roll_date,employees.middle_name,payrolldata.job_title as job_title,employees.last_name,payrolldata.employement_status,payrolldata.department,payrolldata.bank_name,payrolldata.account_number,employees.employee_code,employees.employee_id,employees.nic_num,employees.father_name,employees.birthday,employees.joined_date,employees.eobi
                from payrolldata
                join employees on employees.id = payrolldata.employee
                where payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id $where GROUP BY payrolldata.employee $OrderBy");
                $employees[$department->id] = collect($employees[$department->id])->reject(function ($employee) {
                    if (env('COMPANY') == 'JSML') {
                        $fromDateYearMonth = date("Y-m", strtotime(request()->fromDate));

                        if (!empty($employee->off_roll_date)) {
                            // Include employees with off_roll_date same year and month or earlier
                            if (date("Y-m", strtotime($employee->off_roll_date)) >= $fromDateYearMonth) {
                                return false;
                            }
                            // Reject employees with off_roll_date later than fromDate
                            return true;
                        }

                        if (!empty($employee->termination_date)) {
                            // Include employees terminated same year and month or earlier
                            if (date("Y-m", strtotime($employee->termination_date)) >= $fromDateYearMonth) {
                                return false;
                            }
                            // Reject employees terminated later than fromDate
                            return true;
                        }

                        return false; // Include employees without termination_date or off_roll_date
                    }
                    return false; // If conditions don't match, don't reject the employee
                })->values()->all();
                if(!empty($net_salary))
                {
                    $sub_total_net_salary[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                        join employees ON employees.id = payrolldata.employee
                        WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $net_salary->id $where");
                }
                if(!empty($gross_salary))
                {
                    $sub_total_net_gross_salary[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                        join employees ON employees.id = payrolldata.employee
                        WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $gross_salary->id $where");
                }
                if(!empty($deducation_column))
                {
                    $sub_total_deducations[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                        join employees ON employees.id = payrolldata.employee
                        WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $deducation_column->id $where");
                }
            }
        }
        $bank_show='';
        if (!empty($request->banksFilter)) {
            $bank_show = DB::table('banks')->whereIn('banks.id',$request->banksFilter)->get(['id' , 'name']);
        }

        if(request()->export_type == "excel"){
            return Excel::download(new SalarySheetExport($calculation_group_payroll,$net_salary,$total_days,$gross_salary,$sub_total_net_gross_salary,$sub_total_deducations,$sub_total_net_salary,$deducation_column,$departments,$employees,$bank_show,$where,$payrol_ids_str), 'Bank Salary Export Sheet.xlsx');
        }

        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);

        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.bank_salary_sheet_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Bank Salary Export Sheet.pdf');
        }
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $childvar =( getChildren($request->departmentFilter));
        $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        $bank_name = DB::table('banks')->get(['id' , 'name']);
        return view('Admin.reports.bank_salary_sheet',get_defined_vars());
    }
    public function bank_salary_report(Request $request)
    {
        // $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        // $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');
        if (!empty($request->fromDate)) {
            $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
            $fromDate = $date_from->format('F Y');
            $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
            $toDate = $date_to->format('F Y');
            $calculation_group_payroll = DB::table('payroll')->where([
                ['deduction_group' , $request->calculation_group],
                [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                // [DB::raw('YEAR(date_start)') , $month],
                // [DB::raw('MONTH(date_start)') , $month],
                ['status' , 'Completed']
            ])->get(['id as payroll_id','department','columns','deduction_group']);
        }
        if(!empty($calculation_group_payroll)){
            foreach ($calculation_group_payroll as $key => $calculation_group_payrolls) {
                $net_salary = DB::table('payrollcolumns')->where([
                    ['deduction_group', $calculation_group_payrolls->deduction_group],
                    ['salary_column_type', 'net_Salary']
                ])->first(['id']);
                $gross_salary = DB::table('payrollcolumns')->where([
                    ['deduction_group', $calculation_group_payrolls->deduction_group],
                    ['salary_column_type', 'gross_salary']
                ])->first(['id']);
                $deducation_column = DB::table('payrollcolumns')->where([
                    ['deduction_group', $calculation_group_payrolls->deduction_group],
                    ['salary_column_type', 'total_deductions']
                ])->first(['id']);
                $total_days = DB::table('payrollcolumns')->where([
                    ['deduction_group', 88],
                    ['salary_column_type', 'working_days']
                ])->first(['id']);
                // dd($total_days);
                $all_childs =  getChildren($calculation_group_payrolls->department);
                array_push($all_childs,$calculation_group_payrolls->department);
                $payrol_ids[] = $calculation_group_payrolls->payroll_id;
                $payrol_ids_str = implode(',', $payrol_ids);
            }
            $where = "";
            if(!empty($request->employment_status))
            {
                $employement_status = implode(',',$request->employment_status);
                $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
            }

            if($request->bank_status=='Bank')
            {
                $where .= " AND payrolldata.account_number IS NOT NULL AND payrolldata.account_number != ' '";
            }

            if($request->employeeFilter)
            {
                // $where .= " AND (payrolldata.account_number IS NULL OR payrolldata.account_number = ' ')";
                $where .= " AND employees.id = $request->employeeFilter";
            }
            if($request->bank_status=='Cash')
            {
                // $where .= " AND (payrolldata.account_number IS NULL OR payrolldata.account_number = ' ')";
                $where .= " AND payrolldata.bank_name = 'BY Cash'";
            }
            if(!empty($request->banksFilter))
            {
                $bank_ar = implode(',',$request->banksFilter);
                $where .= " AND payrolldata.bank_id IN ($bank_ar)";
            }
            if($request->departmentFilter){
                // dd($request->section);
                $department = implode(',',$request->section);
                $where .= " AND payrolldata.department_id IN ($department)";
            }
            // if($request->departmentFilter){
            //     $department = implode(',',$request->departmentFilter);
            //     $where .= " AND payrolldata.department_id IN ($department)";
            // }
            $OrderBy = '';
            if(!empty($request->order_filter)){
                $OrderBy .= " ORDER BY employees.$request->order_filter ASC";
            }
            $departments = CompanyStructure::whereIn('id', $request->section)->get(['id', 'title']);
            foreach ($departments as $key => $department) {
                $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.termination_date,employees.off_roll_date,employees.middle_name,payrolldata.job_title as job_title,employees.last_name,payrolldata.employement_status,payrolldata.department,payrolldata.bank_name,payrolldata.account_number,employees.employee_code,employees.employee_id,employees.nic_num,employees.father_name,employees.birthday,employees.joined_date,employees.eobi
                from payrolldata
                join employees on employees.id = payrolldata.employee
                where payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id $where GROUP BY payrolldata.employee $OrderBy");
                $employees[$department->id] = collect($employees[$department->id])->reject(function ($employee) {
                    if (env('COMPANY') == 'JSML') {
                        $fromDateYearMonth = date("Y-m", strtotime(request()->fromDate));

                        if (!empty($employee->off_roll_date)) {
                            // Include employees with off_roll_date same year and month or earlier
                            if (date("Y-m", strtotime($employee->off_roll_date)) >= $fromDateYearMonth) {
                                return false;
                            }
                            // Reject employees with off_roll_date later than fromDate
                            return true;
                        }

                        if (!empty($employee->termination_date)) {
                            // Include employees terminated same year and month or earlier
                            if (date("Y-m", strtotime($employee->termination_date)) >= $fromDateYearMonth) {
                                return false;
                            }
                            // Reject employees terminated later than fromDate
                            return true;
                        }

                        return false; // Include employees without termination_date or off_roll_date
                    }
                    return false; // If conditions don't match, don't reject the employee
                })->values()->all();
                if(!empty($net_salary))
                {
                    $sub_total_net_salary[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                        join employees ON employees.id = payrolldata.employee
                        WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $net_salary->id $where");
                }
                if(!empty($gross_salary))
                {
                    $sub_total_net_gross_salary[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                        join employees ON employees.id = payrolldata.employee
                        WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $gross_salary->id $where");
                }
                if(!empty($deducation_column))
                {
                    $sub_total_deducations[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                        join employees ON employees.id = payrolldata.employee
                        WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $deducation_column->id $where");
                }
            }
        }
        if(request()->export_type == "excel"){
            return Excel::download(new BankSalaryExport($calculation_group_payroll,$net_salary,$departments,$employees,$columns_totals,$payrol_ids_str), 'Bank Salary Export Sheet.xlsx');
        }
        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.bank_salary_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Bank Salary Report.pdf');
        }
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $childvar =( getChildren($request->departmentFilter));
        $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        // dd($parentvar);
        $bank_name = DB::table('banks')->get(['id' , 'name']);
        return view('Admin.reports.bank_salary_report',get_defined_vars());
    }

    public function designationWiseSalary(Request $request)
    {
        $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');

        if (isset($request->departmentFilter) && count($request->departmentFilter) > 0) {
            $payrolls = DB::table('payroll')->where([
                [DB::raw('YEAR(date_start)') , $year],
                [DB::raw('MONTH(date_start)') , $month],
                ['status' , 'Completed']
                ])->whereIn('department', $request->departmentFilter)->get(['id','department','columns','deduction_group']);
            if (count($payrolls) > 0) {
                $departments = CompanyStructure::whereIn('id',$request->departmentFilter)->get(['id', 'title']);
                foreach ($payrolls as $key => $payroll) {
                    $payroll_ids[] = $payroll->id;
                    $columns = json_decode($payroll->columns);
                    $DepartmentEmployees = PayrollData::join('employees', 'employees.id', '=', 'payrolldata.employee')->where('payroll', $payroll->id);
                    if(!empty($request->designationFilter)){
                        $DepartmentEmployees->whereIn('employees.job_title', ($request->designationFilter));
                    }
                    $employees[$payroll->department] = $DepartmentEmployees->groupBy('payrolldata.employee')->get(['employees.employee_id','employees.employee_code','employees.first_name','employees.last_name','payrolldata.payroll','payrolldata.employee','payrolldata.department','payrolldata.employement_status','payrolldata.job_title','payrolldata.employment_status_id']);
                    $total_columns = PayrollData::join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                                    ->where([
                                        ['payrolldata.payroll', $payroll->id],
                                        ['payrollcolumns.is_visible', 1],
                                        ['payrollcolumns.total_column', 1]
                                    ]);
                                    // dd($employees[$payroll->department]);
                    if(!empty($request->designationFilter)){
                        $total_columns->whereIn('payrolldata.employment_status_id', $request->designationFilter);
                    }
                    $total_columns = $total_columns->groupBy('payrollcolumns.id')->orderBy('colorder', 'asc')->get(['payrollcolumns.id', 'payrollcolumns.name',DB::raw('SUM(payrolldata.amount) as net_total')]);
                    foreach ($total_columns as $key => $total) {
                        $net_total[$payroll->department][$total->id] = $total->net_total;
                    }
                }
                // dd($columns);
                $payroll_columns = DB::table('payrollcolumns')->where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                    $query->whereNull('employers_column')->orWhere('employers_column', '')->orWhere('employers_column', 0);
                })->orderBy('colorder', 'asc')->get(['id', 'name', 'comma_seprated','round_off']);
                $grand_total_columns = PayrollData::join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                                    ->whereIn('payrolldata.payroll',$payroll_ids)
                                    ->where([
                                        ['payrollcolumns.is_visible', 1],
                                        ['payrollcolumns.total_column', 1]
                                    ]);
                if(!empty($request->designationFilter)){
                    $grand_total_columns->whereIn('payrolldata.employment_status_id', $request->designationFilter);
                }
                $grand_total_columns = $grand_total_columns->groupBy('payrollcolumns.id')->orderBy('colorder', 'asc')->get(['payrollcolumns.id', 'payrollcolumns.name',DB::raw('SUM(payrolldata.amount) as net_total')]);
                foreach ($grand_total_columns as $grandCol) {
                    $grand_total[$grandCol->id] = $grandCol->net_total;
                }
            }
        }
        if(request()->export_type == "excel"){
            $date = request()->date;
            return Excel::download(new DesignationWiseReportExport($payrolls , $payroll_columns , $departments , $employees , $date), 'Designation Wise Salary Export Sheet.xlsx');
        }
        if(request()->export_type == "pdf"){
            $date = request()->date;
            // $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.designation_wise_salary_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Designation Wise Salary Report.pdf');
        }
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        // $banks = DB::table('banks')->get(['id' , 'name']);
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        return view('Admin.reports.designation_wise_salary' , get_defined_vars());
    }

    public function gross_salary_report(Request $request)
    {
        $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');
        $calculation_group_payroll = DB::table('payroll')->where([
            ['deduction_group' , $request->calculation_group],
            [DB::raw('YEAR(date_start)') , $year],
            [DB::raw('MONTH(date_start)') , $month],
            ['status' , 'Completed']
        ])->first(['id as payroll_id','department','columns','deduction_group']);
        if(!empty($calculation_group_payroll)){
            $gross_salary = DB::table('payrollcolumns')->where([
                ['deduction_group', $calculation_group_payroll->deduction_group],
                ['salary_column_type', 'gross_salary']
            ])->first(['id']);

            $total_days = DB::table('payrollcolumns')->where([
                ['deduction_group', $calculation_group_payroll->deduction_group],
                ['salary_column_type', 'working_days']
            ])->first(['id']);
            $all_childs =  getChildren($calculation_group_payroll->department);
            array_push($all_childs,$calculation_group_payroll->department);

            $where = "";
            if(!empty($request->employment_status))
            {
                $employement_status = implode(',',$request->employment_status);
                $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
            }

            if($request->bank_status=='Bank')
            {
                $where .= " AND payrolldata.account_number IS NOT NULL AND payrolldata.account_number != ' '";
            }

            if($request->bank_status=='Cash')
            {
                // $where .= " AND (payrolldata.account_number IS NULL OR payrolldata.account_number = ' ')";
                $where .= " AND payrolldata.bank_name = 'BY Cash'";
            }
            if($request->emp_status=='Active')
            {
                $where .= " AND employees.status = 'Active'";
            }
            if($request->emp_status=='Terminated')
            {
                $where .= " AND employees.status = 'Terminated'";
            }
            if($request->seasonal_status=='On Roll')
            {
                $where .= " AND employees.seasonal_status = 'On Roll'";
            }
            if($request->seasonal_status=='Off Roll')
            {
                $where .= " AND employees.seasonal_status = 'Off Roll'";
            }
            if(!empty($request->banksFilter))
            {
                $bank_ar = implode(',',$request->banksFilter);
                $where .= " AND payrolldata.bank_id IN ($bank_ar)";
            }
            if($request->departmentFilter){
                // dd($request->section);
                $department = implode(',',$request->section);
                $where .= "AND payrolldata.department_id IN ($department)";
            }
            // if($request->departmentFilter){
            //     $department = implode(',',$request->departmentFilter);
            //     $where .= " AND payrolldata.department_id IN ($department)";
            // }
            $OrderBy = '';
            if(!empty($request->order_filter)){
                $OrderBy .= " ORDER BY employees.$request->order_filter ASC";
            }
            $departments = CompanyStructure::whereIn('id', $request->section)->get(['id', 'title']);
            foreach ($departments as $key => $department) {
                $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.middle_name,payrolldata.job_title as job_title,employees.last_name,payrolldata.employement_status,payrolldata.department,payrolldata.bank_name,payrolldata.account_number,employees.employee_code,employees.employee_id,employees.nic_num,employees.father_name,employees.birthday,employees.joined_date,employees.eobi
                from payrolldata
                join employees on employees.id = payrolldata.employee
                where payrolldata.payroll = $calculation_group_payroll->payroll_id AND payrolldata.department_id = $department->id $where GROUP BY payrolldata.employee $OrderBy");
                if(!empty($gross_salary))
                {
                    $sub_total_net_gross_salary[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                        join employees ON employees.id = payrolldata.employee
                        WHERE payrolldata.payroll = '$calculation_group_payroll->payroll_id' AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $gross_salary->id $where");
                }
            }
        }
        if (!empty($request->banksFilter)) {
            $bank_show = DB::table('banks')->whereIn('banks.id',$request->banksFilter)->get(['id' , 'name']);
        }

        if(request()->export_type == "excel"){
            return Excel::download(new GrossSalaryExport($calculation_group_payroll,$gross_salary,$sub_total_net_gross_salary,$departments,$employees,$bank_show,$where), 'Gross Salary Export Sheet.xlsx');
        }

        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);

        if(request()->export_type == "pdf"){
            $date = request()->date;
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.gross_sheet_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Gross Salary Export Sheet.pdf');
        }
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $childvar =( getChildren($request->departmentFilter));
        $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        $bank_name = DB::table('banks')->get(['id' , 'name']);
        return view('Admin.reports.gross_salary_report',get_defined_vars());
    }

    public function salaryHistory(Request $request)
    {
        $payrolls = Payroll::orderBy('id' , 'desc')->get();
        $employees = Employee::where('employees.status' , 'Active')->whereIn('employees.department' , login_user_departments())->orderBy('id' , 'desc')->get(['id','employees.first_name','employees.last_name','employees.last_name']);
        return view('Admin.reports.employee_salary_history' , get_defined_vars());
    }

    public function departmentWiseSalary(Request $request)
    {
        // dd($request);
        // $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        // $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');
        if (!empty($request->calculation_group)) {
            if (!empty($request->fromDate)) {
                $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
                $fromDate = $date_from->format('F Y');
                $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
                $toDate = $date_to->format('F Y');
                $calculation_group_payroll = DB::table('payroll')->where([
                    ['deduction_group' , $request->calculation_group],
                    [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                    [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                    // [DB::raw('YEAR(date_start)') , $year],
                    // [DB::raw('MONTH(date_start)') , $month],
                    ['status' , 'Completed']
                ])->get(['id as payroll_id','department','columns','deduction_group']);
                $payrollIds = [];
                foreach ($calculation_group_payroll as $key => $payroll) {
                    $columns = json_decode($payroll->columns);
                    $payrollIds[] = $payroll->payroll_id;
                }
                $payroll_id_str = implode(',',$payrollIds);
            }
            if(!empty($calculation_group_payroll)){
                    $payroll_columns = DB::table('payrollcolumns')->where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                        $query->whereNull('employers_column')->orWhere('employers_column', '');
                    })->orderBy('colorder', 'asc')->select('id', 'name', 'comma_seprated','round_off')->get();
                    // $payroll_column_ids = DB::table('payrollcolumns')->where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                    //     $query->whereNull('employers_column')->orWhere('employers_column', '');
                    // })->orderBy('colorder', 'asc')->pluck('id');
                // $all_childs =  getChildren2($request->departmentFilter);
                // $direct_employees = DB::table('payrolldata')->where('department_id', $request->departmentFilter)->pluck('id');
                // if(count($direct_employees) > 0){
                //     array_push($all_childs,$request->departmentFilter);
                // }
                $where='';
                if(!empty($request->employment_status)){
                    $employement_status = implode(',',$request->employment_status);
                    $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
                }
                if($request->employeeFilter)
                {
                    $where .= " AND employees.id = $request->employeeFilter ";
                }
                if($request->pay_grade)
                {
                    $where .= " AND employees.pay_grade = $request->pay_grade ";
                }
                if($request->order_filter=='emp_code')
                {
                    $where .= "GROUP BY employees.employee_code";
                }else{
                    $where .= "GROUP BY employee";
                }
                // if (env('COMPANY') != 'JSML') {
                //     $departments = CompanyStructure::where('id', $request->departmentFilter)->orderBy('id', 'ASC')->get(['id', 'title']);
                // }else{
                    $departments = CompanyStructure::whereIn('id', $request->section)->orderBy('id', 'ASC')->get(['id', 'title']);
                // }
                // if($request->departmentFilter!='all'){
                // }else{
                //     $departments = CompanyStructure::orderBy('id', 'ASC')->get(['id', 'title']);
                // }
                // dd($payroll_columns);
                foreach ($departments as $key => $department) {
                    $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.off_roll_date,employees.termination_date,employees.last_name,employees.middle_name,employees.employee_id,employees.employee_code,payrolldata.department as department_name, payrolldata.job_title as job_title, payrolldata.employement_status as employment_status,employees.bank_name,paygrades.name as paygrade_name,paygrades.min_salary,paygrades.max_salary
                        from payrolldata
                        join employees on employees.id = payrolldata.employee
                        left join paygrades on paygrades.id = employees.pay_grade
                        where payrolldata.payroll IN ($payroll_id_str) AND payrolldata.department_id = $department->id $where");
                        $employees[$department->id] = collect($employees[$department->id])->reject(function ($employee) {
                            if (env('COMPANY') == 'JSML') {
                                $fromDateYearMonth = date("Y-m", strtotime(request()->fromDate));

                                if (!empty($employee->off_roll_date)) {
                                    // Include employees with off_roll_date same year and month or earlier
                                    if (date("Y-m", strtotime($employee->off_roll_date)) >= $fromDateYearMonth) {
                                        return false;
                                    }
                                    // Reject employees with off_roll_date later than fromDate
                                    return true;
                                }

                                if (!empty($employee->termination_date)) {
                                    // Include employees terminated same year and month or earlier
                                    if (date("Y-m", strtotime($employee->termination_date)) >= $fromDateYearMonth) {
                                        return false;
                                    }
                                    // Reject employees terminated later than fromDate
                                    return true;
                                }

                                return false; // Include employees without termination_date or off_roll_date
                            }
                            return false; // If conditions don't match, don't reject the employee
                        })->values()->all();
                        $columns_totals[$department->id] = PayrollData::join('employees', 'employees.id', '=', 'payrolldata.employee')
                                ->leftjoin('paygrades','paygrades.id','=','employees.pay_grade')
                                ->join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                                ->where('payrollcolumns.is_visible', 1)
                                ->whereIn('payrolldata.payroll_item', $columns)
                                ->where(function($query){
                                    $query->whereNull('payrollcolumns.employers_column')->orWhere('payrollcolumns.employers_column', '');
                                })
                                ->whereIn('payrolldata.payroll', $payrollIds)
                                ->where('payrolldata.department_id', $department->id);

                                if(!empty($request->employment_status)){
                                    $columns_totals[$department->id]->whereIn('payrolldata.employment_status_id', $request->employment_status);
                                }
                                if(!empty($request->pay_grade)){
                                    $columns_totals[$department->id]->where('employees.pay_grade', $request->pay_grade);
                                }
                                if(!empty($request->banksFilter) && $request->banksFilter !="all"){
                                    $columns_totals[$department->id]->where('payrolldata.bank_id',$request->banksFilter);
                                }
                                if(!empty($request->employeeFilter)){
                                    $columns_totals[$department->id]->where('employees.id', $request->employeeFilter);
                                }
                                $columns_totals[$department->id] = $columns_totals[$department->id]->groupBy('payrollcolumns.id')->orderBy('payrollcolumns.colorder', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item']);
                }
                // foreach ($departments as $key => $department) {
                //     $childrens =  getChildren2($department->id);
                //     // array_push($childrens,$department->id);
                //     // if($request->departmentFilter!='all'){
                //     if (env('COMPANY') != 'JSML') {
                //         $all_childrens[$department->id] = DB::table('companystructures')->whereIn('id', $childrens)->orderBy('id', 'ASC')->get(['id', 'title']);
                //         $children_ids = DB::table('companystructures')->whereIn('id', $childrens)->orderBy('title', 'ASC')->pluck('id');
                //     }else{
                //         $all_childrens[$department->id] = DB::table('companystructures')->whereIn('id', $request->section)->orderBy('id', 'ASC')->get(['id', 'title']);
                //         $children_ids = DB::table('companystructures')->whereIn('id', $request->section)->orderBy('title', 'ASC')->pluck('id');
                //     }

                //     // }else{
                //     //     $all_childrens[$department->id] = DB::table('companystructures')->whereIn('id', explode(',',$department->id))->orderBy('id', 'ASC')->get(['id', 'title']);
                //     //     $children_ids = DB::table('companystructures')->whereIn('id', explode(',',$department->id))->orderBy('title', 'ASC')->pluck('id');
                //     // }
                //     foreach ($all_childrens[$department->id] as $key2 => $child) {
                //         // $total_employees[$child->id] = DB::table('employees')->join('payrolldata', 'payrolldata.employee', '=', 'employees.id')->where('employees.department',$child->id)->where('payrolldata.payroll', $calculation_group_payroll->payroll_id)->groupBy('payrolldata.employee')->get(['employees.id']);
                //         $employees[$child->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.last_name,employees.middle_name,employees.employee_id,employees.employee_code,payrolldata.department as department_name, payrolldata.job_title as job_title, payrolldata.employement_status as employment_status,employees.bank_name
                //         from payrolldata
                //         join employees on employees.id = payrolldata.employee
                //         where payrolldata.payroll IN ($payroll_id_str) AND payrolldata.department_id = $child->id $where");
                //         $columns_totals[$child->id] = PayrollData::join('employees', 'employees.id', '=', 'payrolldata.employee')
                //                 ->join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                //                 ->where('payrollcolumns.is_visible', 1)
                //                 ->whereIn('payrolldata.payroll', $payrollIds)
                //                 ->whereIn('payrolldata.payroll_item', $payroll_column_ids)
                //                 ->where('payrolldata.department_id', $child->id);
                //                 if(!empty($request->employment_status)){
                //                     $columns_totals[$child->id]->whereIn('employees.employment_status', $request->employment_status);
                //                 }
                //                 if(!empty($request->banksFilter) && $request->banksFilter !="all"){
                //                     $columns_totals[$child->id]->where('employees.bank_id',$request->banksFilter);
                //                 }
                //                 if(!empty($request->employeeFilter)){
                //                     $columns_totals[$child->id]->where('employees.id', $request->employeeFilter);
                //                 }
                //                 $columns_totals[$child->id] = $columns_totals[$child->id]->groupBy('payrollcolumns.id')->orderBy('payrollcolumns.colorder', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item']);
                //                 // dd($columns_totals);
                //     }
                //     // $parent_totals[$department->id] = DB::table('payrolldata')
                //     //             ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                //     //             ->join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                //     //             ->where('payrollcolumns.is_visible', 1)
                //     //             ->where('payrolldata.payroll', $calculation_group_payroll->payroll_id)
                //     //             ->whereIn('payrolldata.payroll_item', $payroll_column_ids)
                //     //             ->whereIn('employees.department', $children_ids)
                //     //             ->groupBy('payrollcolumns.id')->orderBy('payrollcolumns.colorder', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item']);
                // }
                // dd($columns_totals);
            }
        }
        if(request()->export_type == "excel"){
            $date = request()->date;
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            return Excel::download(new DepartmentWiseSalaryExport($calculation_group_payroll , $payroll_columns , $departments , $employees , $columns_totals , $date , $cal_group , $payrollIds , $request), 'Department Wise Salary Export Sheet.xlsx');
        }
        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.department_wise_salary_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Department Wise Salary Report.pdf');
        }
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        // $banks = DB::table('banks')->get(['id' , 'name']);
        $childvar =( getChildren($request->departmentFilter));
        $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        $paygrades = DB::table('paygrades')->get();
        return view('Admin.reports.department_wise_salary' , get_defined_vars());
    }

    public function departmentWiseSalarySummery(Request $request)
    {
        // $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        // $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');

        if (!empty($request->calculation_group)) {
            if (!empty($request->fromDate)) {
            $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
            $fromDate = $date_from->format('F Y');
            $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
            $toDate = $date_to->format('F Y');
                $calculation_group_payroll = DB::table('payroll')->where([
                    ['deduction_group' , $request->calculation_group],
                    [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                    [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                    // [DB::raw('YEAR(date_start)') , $year],
                    // [DB::raw('MONTH(date_start)') , $month],
                    ['status' , 'Completed']
                ])->get(['id as payroll_id','department','columns','deduction_group']);
                $payrollIds = [];
                foreach ($calculation_group_payroll as $key => $payroll) {
                    $columns = json_decode($payroll->columns);
                    $payrollIds[] = $payroll->payroll_id;
                }
                $payroll_id_str = implode(',',$payrollIds);
            }
            if(!empty($calculation_group_payroll)){
                $payroll_columns = DB::table('payrollcolumns')->where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                    $query->whereNull('employers_column')->orWhere('employers_column', '');
                })->orderBy('colorder', 'asc')->get(['id', 'name', 'comma_seprated']);

                $payroll_column_ids = DB::table('payrollcolumns')->where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                    $query->whereNull('employers_column')->orWhere('employers_column', '');
                })->orderBy('colorder', 'asc')->pluck('id');
                $all_childs =  getChildren2($request->departmentFilter);
                $direct_employees = DB::table('employees')->where('department', $request->departmentFilter)->pluck('id');
                if(count($direct_employees) > 0){
                    array_push($all_childs,$request->departmentFilter);
                }
                $where='';
                if(!empty($request->employment_status)){
                    $employement_status = implode(',',$request->employment_status);
                    $where .= "AND employees.employment_status IN ('$employement_status')";
                }
                if($request->departmentFilter!='all'){
                    // if (env('COMPANY') != 'JSML') {
                        $departments = DB::table('companystructures')->where('id', $request->departmentFilter)->orderBy('id', 'ASC')->get(['id', 'title']);
                    // }else{
                    //     $departments = DB::table('companystructures')->where('id', $request->section)->orderBy('id', 'ASC')->get(['id', 'title']);
                    // }
                }else{
                    $departments = DB::table('companystructures')->orderBy('id', 'ASC')->get(['id', 'title']);
                    $all='yes';
                }
                // dd($departments);
                foreach ($departments as $key => $department) {
                    $childrens =  getChildren2($department->id);
                    // array_push($childrens,$department->id);
                    // $childrens = [];
                    // if($department->id != $request->departmentFilter){
                    //     $childrens =  getChildren2($department->id);
                    //     array_push($childrens,$department->id);
                    // }
                    // else{
                    //     $childrens[] = $department->id;
                    // }
                    // if(env('COMPANY') != 'JSML'){
                        $all_childrens[$department->id] = DB::table('companystructures')->whereIn('id', $childrens)->orderBy('id', 'ASC')->get(['id', 'title']);
                        $children_ids = DB::table('companystructures')->whereIn('id', $childrens)->orderBy('title', 'ASC')->pluck('id');
                    // }else{
                    //     $all_childrens[$department->id] = DB::table('companystructures')->whereIn('id', $request->section)->orderBy('id', 'ASC')->get(['id', 'title']);
                    //     $children_ids = DB::table('companystructures')->whereIn('id', $request->section)->orderBy('title', 'ASC')->pluck('id');
                    // }


                    foreach ($all_childrens[$department->id] as $key2 => $child) {
                        $total_employees[$child->id] = DB::table('payrolldata')->join('employees','employees.id', '=','payrolldata.employee')
                        ->where('payrolldata.department_id',$child->id)
                        ->whereIn('payrolldata.payroll', $payrollIds)
                        ->where(function ($query) use ($request) {
                            $query->where(function ($query) use ($request) {
                                $query->whereNull('termination_date')
                                    ->orWhere('termination_date', '>=', $request->fromDate.'-01');
                            })
                            ->where(function ($query) use ($request) {
                                $query->whereNull('off_roll_date')
                                    ->orWhere('off_roll_date', '>=', $request->fromDate.'-01');
                            });
                        });
                        if(!empty($request->employment_status)){
                            $total_employees[$child->id]->whereIn('payrolldata.employment_status_id', $request->employment_status);
                        }
                        $total_employees[$child->id] = $total_employees[$child->id]->select('payrolldata.employee')->groupBy('payrolldata.employee')->get();
                        $columns_totals[$child->id] = DB::table('payrolldata')
                        ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                        ->join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                        ->where('payrollcolumns.is_visible', 1)
                        ->whereIn('payrolldata.payroll', $payrollIds)
                        ->whereIn('payrolldata.payroll_item', $payroll_column_ids)
                        ->where('payrolldata.department_id', $child->id);
                        if(!empty($request->employment_status)){
                            $columns_totals[$child->id]->whereIn('payrolldata.employment_status_id', $request->employment_status);
                        }
                        $columns_totals[$child->id] = $columns_totals[$child->id]->groupBy('payrollcolumns.id')->orderBy('payrollcolumns.colorder', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item']);
                    }
                    $parent_totals[$department->id] = DB::table('payrolldata')
                                ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                                ->join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                                ->where('payrollcolumns.is_visible', 1)
                                ->whereIn('payrolldata.payroll', $payrollIds)
                                ->whereIn('payrolldata.payroll_item', $payroll_column_ids)
                                ->whereIn('payrolldata.department_id', $children_ids);
                                if (!empty($request->employment_status)) {
                                    $parent_totals[$department->id]->whereIn('payrolldata.employment_status_id', $request->employment_status);
                                }
                                $parent_totals[$department->id] = $parent_totals[$department->id]->groupBy('payrollcolumns.id')->orderBy('payrollcolumns.colorder', 'ASC')
                                ->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item']);
                }
            }
        }
        if(request()->export_type == "excel"){
            $date = request()->date;
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            return Excel::download(new DepartmentWiseSalarySummeryExport($calculation_group_payroll , $payroll_columns , $departments,$columns_totals,$date , $cal_group , $total_employees, $all_childrens, $parent_totals), 'Department Wise Salary Summery Export Sheet.xlsx');
        }
        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.department_wise_salary_summery_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Department Wise Salary Summery.pdf');
        }
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        $childvar =( getChildren($request->departmentFilter));
        $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        return view('Admin.reports.department_wise_salary_summery' , get_defined_vars());
    }

    public function eobiMonthlyReport(Request $request)
    {
        $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');

        if (!empty($request->calculation_group)) {
            if (!empty($request->fromDate)) {
                $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
                $fromDate = $date_from->format('F Y');
                $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
                $toDate = $date_to->format('F Y');
                    $calculation_group_payroll = DB::table('payroll')->where([
                        ['deduction_group' , $request->calculation_group],
                        [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                        [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                        // [DB::raw('YEAR(date_start)') , $year],
                        // [DB::raw('MONTH(date_start)') , $month],
                        ['status' , 'Completed']
                    ])->get(['id as payroll_id','department','columns','deduction_group']);
                    $payrollIds = [];
                    foreach ($calculation_group_payroll as $key => $payroll) {
                        $columns = json_decode($payroll->columns);
                        $payrollIds[] = $payroll->payroll_id;
                    }
                    $payroll_id_str = implode(',',$payrollIds);
                }
            if(!empty($calculation_group_payroll)){
                $payroll_columns = DB::table('report_columns')->where('type','LIKE','%eobi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->get(['payroll_column_id', 'name' , 'is_decimal']);

                $report_columns = DB::table('report_columns')->where('type','LIKE','%eobi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->pluck('payroll_column_id');
                // $all_childs =  getChildren($calculation_group_payroll->department);
                // array_push($all_childs,$calculation_group_payroll->department);
                $where = "";
                if(!empty($request->employment_status))
                {
                    $employement_status = implode(',',$request->employment_status);
                    $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
                }
                if($request->departmentFilter){
                    if(env('COMPANY') != 'JSML'){
                        $dept = implode(',',$request->departmentFilter);
                        $where .= "AND payrolldata.department_id IN ($dept)";
                    }else{
                        $dept = implode(',',$request->section);
                        $where .= "AND payrolldata.department_id IN ($dept)";
                    }
                }
                if($request->employeeFilter)
                {
                    $where .= " AND employees.id = $request->employeeFilter ";
                }
                if($request->order_filter=='emp_code')
                {
                    $where .= "GROUP BY employees.employee_code";
                }else{
                    $where .= "GROUP BY employee";

                }
                if(env('COMPANY') != 'JSML'){
                    $departments = DB::table('companystructures')->whereIn('id', $all_childs)->get(['id', 'title']);
                }else{
                    $departments = DB::table('companystructures')->whereIn('id', $request->section)->get(['id', 'title']);
                }
                foreach ($departments as $key => $department) {
                    $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.termination_date,employees.off_roll_date,employees.middle_name,employees.last_name,employees.employee_id,employees.employee_code,employees.nic_num,employees.father_name,employees.birthday,employees.joined_date,employees.eobi,payrolldata.employement_status
                    from payrolldata
                    join employees on employees.id = payrolldata.employee
                    where payrolldata.payroll IN ($payroll_id_str) AND payrolldata.department_id = $department->id AND employees.birthday>'1963-01-01' AND employees.eobi_status = 'yes' $where");
                    $employees[$department->id] = collect($employees[$department->id])->reject(function ($employee) {
                        if (env('COMPANY') == 'JSML') {
                            $fromDateYearMonth = date("Y-m", strtotime(request()->fromDate));

                            if (!empty($employee->off_roll_date)) {
                                // Include employees with off_roll_date same year and month or earlier
                                if (date("Y-m", strtotime($employee->off_roll_date)) >= $fromDateYearMonth) {
                                    return false;
                                }
                                // Reject employees with off_roll_date later than fromDate
                                return true;
                            }

                            if (!empty($employee->termination_date)) {
                                // Include employees terminated same year and month or earlier
                                if (date("Y-m", strtotime($employee->termination_date)) >= $fromDateYearMonth) {
                                    return false;
                                }
                                // Reject employees terminated later than fromDate
                                return true;
                            }

                            return false; // Include employees without termination_date or off_roll_date
                        }
                        return false; // If conditions don't match, don't reject the employee
                    })->values()->all();
                    $columns_totals[$department->id] = DB::table('payrolldata')
                                ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                                ->join('report_columns', 'report_columns.payroll_column_id', '=', 'payrolldata.payroll_item')
                                ->where('employees.eobi_status', 'yes')
                                ->whereIn('payrolldata.payroll', $payrollIds)
                                ->whereIn('payrolldata.payroll_item', $report_columns)
                                ->where('employees.department', $department->id);
                                if(!empty($request->employeeFilter)){
                                    $columns_totals[$department->id]->where('employees.id', $request->employeeFilter);
                                }
                                if (!empty($request->employment_status)) {
                                    $columns_totals[$department->id]->whereIn('payrolldata.employment_status_id', $request->employment_status);
                                }
                                $columns_totals[$department->id] = $columns_totals[$department->id]->groupBy('report_columns.payroll_column_id')->orderBy('report_columns.column_order', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item' , 'report_columns.is_decimal']);
                }
            }

        }
        if(request()->export_type == "excel"){
            $date = request()->date;
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;

            return Excel::download(new EobiMonthlyExport($calculation_group_payroll , $payroll_columns , $departments , $employees , $columns_totals , $date , $cal_group , $payrollIds , $request), 'Eobi Monthly Report Sheet.xlsx');
        }
        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.eobi_monthly_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Eobi Monthly Report.pdf');
        }
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        $childvar =( getChildren($request->departmentFilter));
        $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->leftjoin('employees AS e2' , 'e2.id' , '=' , 'company_setup.payroll_officer')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName' , 'e1.last_name AS gm_lName' , 'e2.first_name AS payroll_fName' , 'e2.last_name AS payroll_lName']);
        return view('Admin.reports.eobi_monthly_report' , get_defined_vars());
    }
    public function importView(Request $request){
        return view('Admin.reports.importFile' , get_defined_vars());
    }
    public function import(Request $request){
        Excel::import(new UserImport, $request->file('file')->store('files'));
        return redirect()->back();
    }


    public function uifReport(Request $request)
    {
            // $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
            // $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');
            if (!empty($request->fromDate)) {
                $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
                $fromDate = $date_from->format('F Y');
                $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
                $toDate = $date_to->format('F Y');
                $calculation_group_payroll = DB::table('payroll')->where([
                    ['deduction_group' , $request->calculation_group],
                    [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                    [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                    // [DB::raw('YEAR(date_start)') , $month],
                    // [DB::raw('MONTH(date_start)') , $month],
                    ['status' , 'Completed']
                ])->get(['id as payroll_id','department','columns','deduction_group']);
            }
            if(!empty($calculation_group_payroll)){
                foreach ($calculation_group_payroll as $key => $calculation_group_payrolls) {
                    $net_salary = DB::table('payrollcolumns')->where([
                        ['deduction_group', $calculation_group_payrolls->deduction_group],
                        ['salary_column_type', 'net_Salary']
                    ])->first(['id']);
                    $gross_salary = DB::table('payrollcolumns')->where([
                        ['deduction_group', $calculation_group_payrolls->deduction_group],
                        ['salary_column_type', 'gross_salary']
                    ])->first(['id']);
                    $deducation_column = DB::table('payrollcolumns')->where([
                        ['deduction_group', $calculation_group_payrolls->deduction_group],
                        ['salary_column_type', 'total_deductions']
                    ])->first(['id']);
                    $total_days = DB::table('payrollcolumns')->where([
                        ['deduction_group', 88],
                        ['salary_column_type', 'working_days']
                    ])->first(['id']);
                    // dd($total_days);
                    $all_childs =  getChildren($calculation_group_payrolls->department);
                    array_push($all_childs,$calculation_group_payrolls->department);
                    $payrol_ids[] = $calculation_group_payrolls->payroll_id;
                    $payrol_ids_str = implode(',', $payrol_ids);
                }
                $where = "";
                if(!empty($request->employment_status))
                {
                    $employement_status = implode(',',$request->employment_status);
                    $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
                }

                if($request->bank_status=='Bank')
                {
                    $where .= " AND payrolldata.account_number IS NOT NULL AND payrolldata.account_number != ' '";
                }

                if($request->employeeFilter)
                {
                    // $where .= " AND (payrolldata.account_number IS NULL OR payrolldata.account_number = ' ')";
                    $where .= " AND employees.id = $request->employeeFilter";
                }
                if($request->bank_status=='Cash')
                {
                    // $where .= " AND (payrolldata.account_number IS NULL OR payrolldata.account_number = ' ')";
                    $where .= " AND payrolldata.bank_name = 'BY Cash'";
                }
                if(!empty($request->banksFilter))
                {
                    $bank_ar = implode(',',$request->banksFilter);
                    $where .= " AND payrolldata.bank_id IN ($bank_ar)";
                }
                if($request->departmentFilter){
                    // dd($request->section);
                    $department = implode(',',$request->section);
                    $where .= " AND payrolldata.department_id IN ($department)";
                }
                // if($request->departmentFilter){
                //     $department = implode(',',$request->departmentFilter);
                //     $where .= " AND payrolldata.department_id IN ($department)";
                // }
                $OrderBy = '';
                if(!empty($request->order_filter)){
                    $OrderBy .= " ORDER BY employees.$request->order_filter ASC";
                }
                $departments = CompanyStructure::whereIn('id', $request->section)->get(['id', 'title']);
                foreach ($departments as $key => $department) {
                    $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.termination_date,employees.off_roll_date,employees.middle_name,payrolldata.job_title as job_title,employees.last_name,payrolldata.employement_status,payrolldata.department,payrolldata.bank_name,payrolldata.account_number,employees.employee_code,employees.employee_id,employees.nic_num,employees.father_name,employees.birthday,employees.joined_date,employees.eobi
                    from payrolldata
                    join employees on employees.id = payrolldata.employee
                    where payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id $where GROUP BY payrolldata.employee $OrderBy");
                    $employees[$department->id] = collect($employees[$department->id])->reject(function ($employee) {
                        if (env('COMPANY') == 'JSML') {
                            $fromDateYearMonth = date("Y-m", strtotime(request()->fromDate));

                            if (!empty($employee->off_roll_date)) {
                                // Include employees with off_roll_date same year and month or earlier
                                if (date("Y-m", strtotime($employee->off_roll_date)) >= $fromDateYearMonth) {
                                    return false;
                                }
                                // Reject employees with off_roll_date later than fromDate
                                return true;
                            }

                            if (!empty($employee->termination_date)) {
                                // Include employees terminated same year and month or earlier
                                if (date("Y-m", strtotime($employee->termination_date)) >= $fromDateYearMonth) {
                                    return false;
                                }
                                // Reject employees terminated later than fromDate
                                return true;
                            }

                            return false; // Include employees without termination_date or off_roll_date
                        }
                        return false; // If conditions don't match, don't reject the employee
                    })->values()->all();
                    if(!empty($net_salary))
                    {
                        $sub_total_net_salary[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                            join employees ON employees.id = payrolldata.employee
                            WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $net_salary->id $where");
                    }
                    if(!empty($gross_salary))
                    {
                        $sub_total_net_gross_salary[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                            join employees ON employees.id = payrolldata.employee
                            WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $gross_salary->id $where");
                    }
                    if(!empty($deducation_column))
                    {
                        $sub_total_deducations[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                            join employees ON employees.id = payrolldata.employee
                            WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $deducation_column->id $where");
                    }
                }
            }
            if(request()->export_type == "excel"){
                return Excel::download(new BankSalaryExport($calculation_group_payroll,$net_salary,$departments,$employees,$columns_totals,$payrol_ids_str), 'Bank Salary Export Sheet.xlsx');
            }
            $date = request()->date;
            if(request()->export_type == "pdf"){
                $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
                $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.bank_salary_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
                return $pdf->download('Bank Salary Report.pdf');
            }
            $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
            $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
            ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
            ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);
            $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
            $childvar =( getChildren($request->departmentFilter));
            $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
            // dd($parentvar);
            $bank_name = DB::table('banks')->get(['id' , 'name']);
            return view('Admin.reports.uif_report' , get_defined_vars());
    }

    public function uifFile(Request $request)
    {
            // $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
            // $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');
            if (!empty($request->fromDate)) {
                $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
                $fromDate = $date_from->format('F Y');
                $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
                $toDate = $date_to->format('F Y');
                $calculation_group_payroll = DB::table('payroll')->where([
                    ['deduction_group' , $request->calculation_group],
                    [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                    [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                    // [DB::raw('YEAR(date_start)') , $month],
                    // [DB::raw('MONTH(date_start)') , $month],
                    ['status' , 'Completed']
                ])->get(['id as payroll_id','department','columns','deduction_group']);
            }
            if(!empty($calculation_group_payroll)){
                foreach ($calculation_group_payroll as $key => $calculation_group_payrolls) {
                    $net_salary = DB::table('payrollcolumns')->where([
                        ['deduction_group', $calculation_group_payrolls->deduction_group],
                        ['salary_column_type', 'net_Salary']
                    ])->first(['id']);
                    $gross_salary = DB::table('payrollcolumns')->where([
                        ['deduction_group', $calculation_group_payrolls->deduction_group],
                        ['salary_column_type', 'gross_salary']
                    ])->first(['id']);
                    $deducation_column = DB::table('payrollcolumns')->where([
                        ['deduction_group', $calculation_group_payrolls->deduction_group],
                        ['salary_column_type', 'total_deductions']
                    ])->first(['id']);
                    $total_days = DB::table('payrollcolumns')->where([
                        ['deduction_group', 88],
                        ['salary_column_type', 'working_days']
                    ])->first(['id']);
                    // dd($total_days);
                    $all_childs =  getChildren($calculation_group_payrolls->department);
                    array_push($all_childs,$calculation_group_payrolls->department);
                    $payrol_ids[] = $calculation_group_payrolls->payroll_id;
                    $payrol_ids_str = implode(',', $payrol_ids);
                }
                $where = "";
                if(!empty($request->employment_status))
                {
                    $employement_status = implode(',',$request->employment_status);
                    $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
                }

                if($request->bank_status=='Bank')
                {
                    $where .= " AND payrolldata.account_number IS NOT NULL AND payrolldata.account_number != ' '";
                }

                if($request->employeeFilter)
                {
                    // $where .= " AND (payrolldata.account_number IS NULL OR payrolldata.account_number = ' ')";
                    $where .= " AND employees.id = $request->employeeFilter";
                }
                if($request->bank_status=='Cash')
                {
                    // $where .= " AND (payrolldata.account_number IS NULL OR payrolldata.account_number = ' ')";
                    $where .= " AND payrolldata.bank_name = 'BY Cash'";
                }
                if(!empty($request->banksFilter))
                {
                    $bank_ar = implode(',',$request->banksFilter);
                    $where .= " AND payrolldata.bank_id IN ($bank_ar)";
                }
                if($request->departmentFilter){
                    // dd($request->section);
                    $department = implode(',',$request->section);
                    $where .= " AND payrolldata.department_id IN ($department)";
                }
                // if($request->departmentFilter){
                //     $department = implode(',',$request->departmentFilter);
                //     $where .= " AND payrolldata.department_id IN ($department)";
                // }
                $OrderBy = '';
                if(!empty($request->order_filter)){
                    $OrderBy .= " ORDER BY employees.$request->order_filter ASC";
                }
                $departments = CompanyStructure::whereIn('id', $request->section)->get(['id', 'title']);
                foreach ($departments as $key => $department) {
                    $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.termination_date,employees.off_roll_date,employees.middle_name,payrolldata.job_title as job_title,employees.last_name,payrolldata.employement_status,payrolldata.department,payrolldata.bank_name,payrolldata.account_number,employees.employee_code,employees.employee_id,employees.nic_num,employees.father_name,employees.birthday,employees.joined_date,employees.eobi
                    from payrolldata
                    join employees on employees.id = payrolldata.employee
                    where payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id $where GROUP BY payrolldata.employee $OrderBy");
                    $employees[$department->id] = collect($employees[$department->id])->reject(function ($employee) {
                        if (env('COMPANY') == 'JSML') {
                            $fromDateYearMonth = date("Y-m", strtotime(request()->fromDate));

                            if (!empty($employee->off_roll_date)) {
                                // Include employees with off_roll_date same year and month or earlier
                                if (date("Y-m", strtotime($employee->off_roll_date)) >= $fromDateYearMonth) {
                                    return false;
                                }
                                // Reject employees with off_roll_date later than fromDate
                                return true;
                            }

                            if (!empty($employee->termination_date)) {
                                // Include employees terminated same year and month or earlier
                                if (date("Y-m", strtotime($employee->termination_date)) >= $fromDateYearMonth) {
                                    return false;
                                }
                                // Reject employees terminated later than fromDate
                                return true;
                            }

                            return false; // Include employees without termination_date or off_roll_date
                        }
                        return false; // If conditions don't match, don't reject the employee
                    })->values()->all();
                    if(!empty($net_salary))
                    {
                        $sub_total_net_salary[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                            join employees ON employees.id = payrolldata.employee
                            WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $net_salary->id $where");
                    }
                    if(!empty($gross_salary))
                    {
                        $sub_total_net_gross_salary[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                            join employees ON employees.id = payrolldata.employee
                            WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $gross_salary->id $where");
                    }
                    if(!empty($deducation_column))
                    {
                        $sub_total_deducations[$department->id] = DB::select("SELECT SUM(payrolldata.amount) as amount FROM payrolldata
                            join employees ON employees.id = payrolldata.employee
                            WHERE payrolldata.payroll IN ($payrol_ids_str) AND payrolldata.department_id = $department->id AND payrolldata.payroll_item = $deducation_column->id $where");
                    }
                }
            }
            if(request()->export_type == "excel"){
                return Excel::download(new BankSalaryExport($calculation_group_payroll,$net_salary,$departments,$employees,$columns_totals,$payrol_ids_str), 'Bank Salary Export Sheet.xlsx');
            }
            $date = request()->date;
            if(request()->export_type == "pdf"){
                $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
                $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.bank_salary_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
                return $pdf->download('Bank Salary Report.pdf');
            }
            $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
            $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
            ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
            ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);
            $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
            $childvar =( getChildren($request->departmentFilter));
            $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
            // dd($parentvar);
            $bank_name = DB::table('banks')->get(['id' , 'name']);
            return view('Admin.reports.uif_file' , get_defined_vars());
    }

    public function DirectiveNumberReport(Request $request)
    {

        $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');

        if (!empty($request->calculation_group)) {
            $calculation_group_payroll = DB::table('payroll')->where([
                ['deduction_group' , $request->calculation_group],
                [DB::raw('YEAR(date_start)') , $year],
                [DB::raw('MONTH(date_start)') , $month],
                ['status' , 'Completed']
            ])->first(['id as payroll_id','department','columns','deduction_group']);

            if(!empty($calculation_group_payroll)){
                $payroll_columns = DB::table('report_columns')->where('type','LIKE','%eobi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->get(['payroll_column_id', 'name' , 'is_decimal']);

                $report_columns = DB::table('report_columns')->where('type','LIKE','%eobi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->pluck('payroll_column_id');
                // $all_childs =  getChildren($calculation_group_payroll->department);
                // array_push($all_childs,$calculation_group_payroll->department);
                $where = "";
                if(!empty($request->employment_status))
                {
                    $employement_status = implode(',',$request->employment_status);
                    $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
                }
                if($request->departmentFilter){
                    if(env('COMPANY') != 'JSML'){
                        $dept = implode(',',$request->departmentFilter);
                        $where .= "AND payrolldata.department_id IN ($dept)";
                    }else{
                        $dept = implode(',',$request->section);
                        $where .= "AND payrolldata.department_id IN ($dept)";
                    }
                }
                if($request->order_filter=='emp_code')
                {
                    $where .= "GROUP BY employees.employee_code";
                }else{
                    $where .= "GROUP BY employee";

                }
                if(env('COMPANY') != 'JSML'){
                    $departments = DB::table('companystructures')->whereIn('id', $request->departmentFilter)->get(['id', 'title']);
                }else{
                    $departments = DB::table('companystructures')->whereIn('id', $request->section)->get(['id', 'title']);
                }
                foreach ($departments as $key => $department) {
                    $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.middle_name,employees.last_name,employees.employee_id,employees.employee_code,employees.nic_num,employees.father_name,employees.birthday,employees.joined_date,employees.eobi,payrolldata.employement_status
                    from payrolldata
                    join employees on employees.id = payrolldata.employee
                    where payrolldata.payroll = '$calculation_group_payroll->payroll_id' AND payrolldata.department_id = $department->id AND employees.birthday>'1963-01-01' AND employees.eobi_status = 'yes' $where");
                    $columns_totals[$department->id] = DB::table('payrolldata')
                                ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                                ->join('report_columns', 'report_columns.payroll_column_id', '=', 'payrolldata.payroll_item')
                                ->where('employees.eobi_status', 'yes')
                                ->where('payrolldata.payroll', $calculation_group_payroll->payroll_id)
                                ->whereIn('payrolldata.payroll_item', $report_columns)
                                ->where('employees.department', $department->id)
                                ->groupBy('report_columns.payroll_column_id')->orderBy('report_columns.column_order', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item' , 'report_columns.is_decimal']);
                }
            }

        }
        if(request()->export_type == "excel"){
            $date = request()->date;
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;

            return Excel::download(new EobiMonthlyExport($calculation_group_payroll , $payroll_columns , $departments , $employees , $columns_totals , $date , $cal_group), 'Eobi Monthly Report Sheet.xlsx');
        }
        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.eobi_monthly_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Eobi Monthly Report.pdf');
        }
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        // $childvar =( getChildren($request->departmentFilter));
        // $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->leftjoin('employees AS e2' , 'e2.id' , '=' , 'company_setup.payroll_officer')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName' , 'e1.last_name AS gm_lName' , 'e2.first_name AS payroll_fName' , 'e2.last_name AS payroll_lName']);
        return view('Admin.reports.Directive_Number_Report' , get_defined_vars());
    }

    public function uifMonthlyReport(Request $request)
    {

        $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');

        if (!empty($request->calculation_group)) {
            $calculation_group_payroll = DB::table('payroll')->where([
                ['deduction_group' , $request->calculation_group],
                [DB::raw('YEAR(date_start)') , $year],
                [DB::raw('MONTH(date_start)') , $month],
                ['status' , 'Completed']
            ])->first(['id as payroll_id','department','columns','deduction_group']);

            if(!empty($calculation_group_payroll)){
                $payroll_columns = DB::table('report_columns')->where('type','LIKE','%eobi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->get(['payroll_column_id', 'name' , 'is_decimal']);

                $report_columns = DB::table('report_columns')->where('type','LIKE','%eobi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->pluck('payroll_column_id');
                // $all_childs =  getChildren($calculation_group_payroll->department);
                // array_push($all_childs,$calculation_group_payroll->department);
                $where = "";
                if(!empty($request->employment_status))
                {
                    $employement_status = implode(',',$request->employment_status);
                    $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
                }
                if($request->departmentFilter){
                    if(env('COMPANY') != 'JSML'){
                        $dept = implode(',',$request->departmentFilter);
                        $where .= "AND payrolldata.department_id IN ($dept)";
                    }else{
                        $dept = implode(',',$request->section);
                        $where .= "AND payrolldata.department_id IN ($dept)";
                    }
                }
                if($request->order_filter=='emp_code')
                {
                    $where .= "GROUP BY employees.employee_code";
                }else{
                    $where .= "GROUP BY employee";

                }
                if(env('COMPANY') != 'JSML'){
                    $departments = DB::table('companystructures')->whereIn('id', $request->departmentFilter)->get(['id', 'title']);
                }else{
                    $departments = DB::table('companystructures')->whereIn('id', $request->section)->get(['id', 'title']);
                }
                foreach ($departments as $key => $department) {
                    $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.middle_name,employees.last_name,employees.employee_id,employees.employee_code,employees.nic_num,employees.father_name,employees.birthday,employees.joined_date,employees.eobi,payrolldata.employement_status
                    from payrolldata
                    join employees on employees.id = payrolldata.employee
                    where payrolldata.payroll = '$calculation_group_payroll->payroll_id' AND payrolldata.department_id = $department->id AND employees.birthday>'1963-01-01' AND employees.eobi_status = 'yes' $where");
                    $columns_totals[$department->id] = DB::table('payrolldata')
                                ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                                ->join('report_columns', 'report_columns.payroll_column_id', '=', 'payrolldata.payroll_item')
                                ->where('employees.eobi_status', 'yes')
                                ->where('payrolldata.payroll', $calculation_group_payroll->payroll_id)
                                ->whereIn('payrolldata.payroll_item', $report_columns)
                                ->where('employees.department', $department->id)
                                ->groupBy('report_columns.payroll_column_id')->orderBy('report_columns.column_order', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item' , 'report_columns.is_decimal']);
                }
            }

        }
        if(request()->export_type == "excel"){
            $date = request()->date;
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;

            return Excel::download(new EobiMonthlyExport($calculation_group_payroll , $payroll_columns , $departments , $employees , $columns_totals , $date , $cal_group), 'Eobi Monthly Report Sheet.xlsx');
        }
        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.eobi_monthly_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Eobi Monthly Report.pdf');
        }
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        // $childvar =( getChildren($request->departmentFilter));
        // $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->leftjoin('employees AS e2' , 'e2.id' , '=' , 'company_setup.payroll_officer')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName' , 'e1.last_name AS gm_lName' , 'e2.first_name AS payroll_fName' , 'e2.last_name AS payroll_lName']);
        return view('Admin.reports.uif_monthly_report' , get_defined_vars());
    }

    public function eobiMonthlySummery(Request $request)
    {
        $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');
        if (!empty($request->calculation_group)) {
            if (!empty($request->fromDate)) {
                $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
                $fromDate = $date_from->format('F Y');
                $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
                $toDate = $date_to->format('F Y');
                    $calculation_group_payroll = DB::table('payroll')->where([
                        ['deduction_group' , $request->calculation_group],
                        [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                        [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                        // [DB::raw('YEAR(date_start)') , $year],
                        // [DB::raw('MONTH(date_start)') , $month],
                        ['status' , 'Completed']
                    ])->get(['id as payroll_id','department','columns','deduction_group']);
                    $payrollIds = [];
                    foreach ($calculation_group_payroll as $key => $payroll) {
                        $columns = json_decode($payroll->columns);
                        $payrollIds[] = $payroll->payroll_id;
                    }
                    $payroll_id_str = implode(',',$payrollIds);
                }
            if(!empty($calculation_group_payroll)){
                $payroll_columns = DB::table('report_columns')->where('type','LIKE','%eobi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->get(['payroll_column_id', 'name']);
                $payroll_column_ids = DB::table('payrollcolumns')->where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                    $query->whereNull('employers_column')->orWhere('employers_column', '');
                })->orderBy('colorder', 'asc')->pluck('id');

                $report_columns = DB::table('report_columns')->where('type','LIKE','%eobi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->pluck('payroll_column_id');
                $all_childs =  getChildren2($request->departmentFilter);
                // array_push($all_childs,$calculation_group_payroll->department);
                // $departments = DB::table('companystructures')->whereIn('id', $all_childs)->get(['id', 'title']);
                // if(env('COMPANY') != 'JSML'){
                    if($request->departmentFilter != 'all'){
                        $departments = DB::table('companystructures')->where('id', $request->departmentFilter)->orderBy('id', 'ASC')->get(['id', 'title']);
                    }else{
                        $departments = DB::table('companystructures')->orderBy('id', 'ASC')->get(['id', 'title']);
                    }
                foreach ($departments as $key => $department) {
                    $childrens =  getChildren2($department->id);
                    // array_push($childrens,$department->id);
                    $all_childrens[$department->id] = DB::table('companystructures')->whereIn('id', $childrens)->orderBy('id', 'ASC')->get(['id', 'title']);
                    $children_ids = DB::table('companystructures')->whereIn('id', $childrens)->orderBy('title', 'ASC')->pluck('id');
                    foreach ($all_childrens[$department->id] as $key2 => $child) {
                        $total_employees[$child->id] = DB::table('payrolldata')
                        ->join('employees','employees.id', '=','payrolldata.employee')
                        ->whereIn('payrolldata.employment_status_id', $request->employment_status)
                        ->where('payrolldata.department_id',$child->id)
                        ->where('employees.birthday','>','1963-01-01')
                        ->whereIn('payrolldata.payroll', $payrollIds)
                        ->where(function ($query) use ($request) {
                            $query->where(function ($query) use ($request) {
                                $query->whereNull('termination_date')
                                    ->orWhere('termination_date', '>=', $request->fromDate.'-01');
                            })
                            ->where(function ($query) use ($request) {
                                $query->whereNull('off_roll_date')
                                    ->orWhere('off_roll_date', '>=', $request->fromDate.'-01');
                            });
                        })
                        ->where('employees.eobi_status', 'yes')->groupBy('payrolldata.employee')
                        ->get(['payrolldata.employee']);

                        $columns_totals[$child->id] = DB::table('payrolldata')
                        ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                        ->join('report_columns', 'report_columns.payroll_column_id', '=', 'payrolldata.payroll_item')
                        ->where('employees.eobi_status', 'yes')
                        ->whereIn('payrolldata.employment_status_id', $request->employment_status)
                        ->whereIn('payrolldata.payroll', $payrollIds)
                        ->whereIn('payrolldata.payroll_item', $report_columns)
                        ->where('employees.department', $child->id)
                        ->groupBy('report_columns.payroll_column_id')->orderBy('report_columns.column_order', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item','report_columns.is_decimal']);
                    }
                    $parent_totals[$department->id] = DB::table('payrolldata')
                                ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                                ->join('report_columns', 'report_columns.payroll_column_id', '=', 'payrolldata.payroll_item')
                                ->join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                                ->where('employees.eobi_status', 'yes')
                                ->where('payrollcolumns.is_visible', 1)
                                ->whereIn('payrolldata.employment_status_id', $request->employment_status)
                                ->whereIn('payrolldata.payroll', $payrollIds)
                                ->whereIn('payrolldata.payroll_item', $report_columns)
                                ->whereIn('payrolldata.department_id', $children_ids)
                                ->groupBy('report_columns.payroll_column_id')->orderBy('report_columns.column_order', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item','report_columns.is_decimal']);
                }
            }
        }
        if(request()->export_type == "excel"){
            $date = request()->date;
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;

            return Excel::download(new EobiSummaryExport($calculation_group_payroll , $payroll_columns , $departments , $total_employees , $columns_totals , $date , $cal_group), 'Eobi Monthly Summary Sheet.xlsx');
        }
        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.eobi_monthly_summery_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Eobi Monthly Summery Report.pdf');
        }
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        $childvar =( getChildren($request->departmentFilter));
        $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->leftjoin('employees AS e2' , 'e2.id' , '=' , 'company_setup.payroll_officer')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName' , 'e1.last_name AS gm_lName' , 'e2.first_name AS payroll_fName' , 'e2.last_name AS payroll_lName']);
        return view('Admin.reports.eobi_monthly_summery' , get_defined_vars());
    }

    public function pessiMonthlyReport(Request $request)
    {
        $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');
        if (!empty($request->calculation_group)) {
            if($request->type == 'pf_fund'){
                $calculation_group_payroll = DB::table('payroll')->where([
                    ['deduction_group' , $request->calculation_group],
                    [DB::raw('YEAR(date_start)') , $year],
                    [DB::raw('MONTH(date_start)') , $month],
                    ['status' , 'Completed']
                ])->first(['id as payroll_id','department','columns','deduction_group']);
            }
            if (!empty($request->fromDate)) {
                $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
                $fromDate = $date_from->format('F Y');
                $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
                $toDate = $date_to->format('F Y');
                    $calculation_group_payroll = DB::table('payroll')->where([
                        ['deduction_group' , $request->calculation_group],
                        [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                        [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                        // [DB::raw('YEAR(date_start)') , $year],
                        // [DB::raw('MONTH(date_start)') , $month],
                        ['status' , 'Completed']
                    ])->get(['id as payroll_id','department','columns','deduction_group']);
                    $payrollIds = [];
                    foreach ($calculation_group_payroll as $key => $payroll) {
                        $columns = json_decode($payroll->columns);
                        $payrollIds[] = $payroll->payroll_id;
                    }
                    $payroll_id_str = implode(',',$payrollIds);
                }
            if(!empty($calculation_group_payroll)){
                if($request->type == 'pessi')
                {
                    $payroll_columns = DB::table('report_columns')->where('type','LIKE','%pessi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->get(['payroll_column_id', 'name' , 'is_decimal']);

                    $report_columns = DB::table('report_columns')->where('type','LIKE','%pessi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->pluck('payroll_column_id');
                }
                else
                {
                    $payroll_columns = DB::table('report_columns')->where('type','LIKE','%pf_fund%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->get(['payroll_column_id', 'name' , 'report_columns.is_decimal']);

                    $report_columns = DB::table('report_columns')->where('type','LIKE','%pf_fund%')->where('calculation_group', request()->calculation_group)->orderBy('column_order', 'ASC')->pluck('payroll_column_id');
                }
                if($request->type == 'pf_fund'){
                    $all_childs =  getChildren($calculation_group_payroll->department);
                    array_push($all_childs,$calculation_group_payroll->department);
                }
                $where = "";
                if(!empty($request->employment_status))
                {
                    $employement_status = implode(',',$request->employment_status);
                    $where .= "AND payrolldata.employment_status_id IN ($employement_status)";
                }
                if($request->departmentFilter){
                    if(env('COMPANY') != 'JSML'){
                        $dept = implode(',',$request->departmentFilter);
                        $where .= "AND payrolldata.department_id IN ($dept)";
                    }else{
                        $dept = implode(',',$request->section);
                        $where .= "AND payrolldata.department_id IN ($dept)";
                    }
                }
                if(!empty($request->employeeFilter))
                {
                    $where .= " AND employees.id = $request->employeeFilter ";
                }
                if($request->order_filter=='emp_code')
                {
                    $where .= "GROUP BY employees.employee_code";
                }else{
                    $where .= "GROUP BY employee";

                }
                if(env('COMPANY') != 'JSML'){
                    $departments = DB::table('companystructures')->whereIn('id', $all_childs)->get(['id', 'title']);
                }else{
                    if($request->type == 'pessi'){
                        $departments = DB::table('companystructures')->whereIn('id', $request->section)->get(['id', 'title']);
                    }else{
                        $departments = DB::table('companystructures')->whereIn('id', $all_childs)->get(['id', 'title']);
                    }
                }
                foreach ($departments as $key => $department) {
                    if($request->type == 'pessi'){
                        $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.termination_date,employees.off_roll_date,employees.last_name,employees.employee_id,employees.nic_num,employees.father_name,employees.employee_code,payrolldata.job_title as desigination,payrolldata.employement_status
                        from payrolldata
                        join employees on employees.id = payrolldata.employee
                        where payrolldata.payroll IN ($payroll_id_str) AND payrolldata.department_id = $department->id AND employees.pessi = 'yes' $where");
                        $employees[$department->id] = collect($employees[$department->id])->reject(function ($employee) {
                            if (env('COMPANY') == 'JSML') {
                                $fromDateYearMonth = date("Y-m", strtotime(request()->fromDate));

                                if (!empty($employee->off_roll_date)) {
                                    // Include employees with off_roll_date same year and month or earlier
                                    if (date("Y-m", strtotime($employee->off_roll_date)) >= $fromDateYearMonth) {
                                        return false;
                                    }
                                    // Reject employees with off_roll_date later than fromDate
                                    return true;
                                }

                                if (!empty($employee->termination_date)) {
                                    // Include employees terminated same year and month or earlier
                                    if (date("Y-m", strtotime($employee->termination_date)) >= $fromDateYearMonth) {
                                        return false;
                                    }
                                    // Reject employees terminated later than fromDate
                                    return true;
                                }

                                return false; // Include employees without termination_date or off_roll_date
                            }
                            return false; // If conditions don't match, don't reject the employee
                        })->values()->all();
                    }else{
                        $employees[$department->id] = DB::select("SELECT payrolldata.employee,employees.first_name,employees.last_name,employees.employee_id,employees.nic_num,employees.father_name,employees.employee_code,payrolldata.job_title as desigination,payrolldata.employement_status
                        from payrolldata
                        join employees on employees.id = payrolldata.employee
                        where payrolldata.payroll = '$calculation_group_payroll->payroll_id' AND payrolldata.department_id = $department->id AND employees.provident_fund = 'yes' $where");
                    }
                    $columns_totals[$department->id] = DB::table('payrolldata')
                                ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                                ->join('report_columns', 'report_columns.payroll_column_id', '=', 'payrolldata.payroll_item');
                                if($request->type == 'pessi'){
                                    $columns_totals[$department->id]->where('employees.pessi', 'yes')->whereIn('payrolldata.payroll', $payrollIds)
                                    ->whereIn('payrolldata.payroll_item', $report_columns)
                                    ->where('payrolldata.department_id', $department->id);
                                }
                                if($request->type == 'pf_fund'){
                                    $columns_totals[$department->id] = $columns_totals[$department->id]->where('employees.provident_fund', 'yes')
                                    ->where('payrolldata.payroll', $calculation_group_payroll->payroll_id)
                                    ->whereIn('payrolldata.payroll_item', $report_columns)
                                    ->where('employees.department', $department->id);
                                }
                                if(!empty($request->employeeFilter)){
                                    $columns_totals[$department->id]->where('employees.id', $request->employeeFilter);
                                }
                                if(!empty($request->employment_status)){
                                    $columns_totals[$department->id]->whereIn('payrolldata.employment_status_id', $request->employment_status);
                                }
                                $columns_totals[$department->id] = $columns_totals[$department->id]->select(DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item' , 'report_columns.is_decimal')->groupBy('report_columns.payroll_column_id')->orderBy('report_columns.column_order', 'ASC')->get();
                            }
            }
        }
        if(request()->export_type == "excel"){
            $report_type = request()->type == 'pf_fund' ? 'Provident Fund Report' : 'Pessi Monthly Report';
            $report_name = request()->type == 'pf_fund' ? 'Provident Fund Export Sheet.xlsx' : 'Pessi Monthly Export Sheet.xlsx';
            $date = request()->date;
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;

            return Excel::download(new PessiMonthlyExport($calculation_group_payroll , $payroll_columns , $departments , $employees , $columns_totals , $date , $cal_group , $payrollIds , $request), 'Pessi Monthly Export Sheet.xlsx');
        }
        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            if (request()->type == 'pf_fund') {
                $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.pessi_monthly_report_pdf', get_defined_vars())->setPaper('4A0', 'portrait');
            }else{
                $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.pessi_monthly_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            }
            return $pdf->download('Pessi Monthly Report.pdf');
        }
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        $childvar =( getChildren($request->departmentFilter));
        $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->leftjoin('employees AS e2' , 'e2.id' , '=' , 'company_setup.payroll_officer')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName' , 'e1.last_name AS gm_lName' , 'e2.first_name AS payroll_fName' , 'e2.last_name AS payroll_lName']);
        if($request->type == 'pf_fund')
        {
            return view('Admin.reports.provident_fund_report' , get_defined_vars());
        }
        return view('Admin.reports.pessi_monthly_report' , get_defined_vars());
    }

    public function pessiMonthlySummery(Request $request)
    {
        $year = !empty($request->date) ? date('Y' , strtotime($request->date)) : date('Y');
        $month = !empty($request->date) ? date('m' , strtotime($request->date)) : date('m');
        if (!empty($request->calculation_group)) {
            if (!empty($request->fromDate)) {
                $date_from = \Carbon\Carbon::createFromFormat('Y-m', $request->fromDate);
                $fromDate = $date_from->format('F Y');
                $date_to = \Carbon\Carbon::createFromFormat('Y-m', $request->toDate);
                $toDate = $date_to->format('F Y');
                    $calculation_group_payroll = DB::table('payroll')->where([
                        ['deduction_group' , $request->calculation_group],
                        [DB::raw('SUBSTRING(date_start, 1, 7)'), '>=', $request->fromDate],
                        [DB::raw('SUBSTRING(date_start, 1, 7)'), '<=', $request->toDate],
                        // [DB::raw('YEAR(date_start)') , $year],
                        // [DB::raw('MONTH(date_start)') , $month],
                        ['status' , 'Completed']
                    ])->get(['id as payroll_id','department','columns','deduction_group']);
                    $payrollIds = [];
                    foreach ($calculation_group_payroll as $key => $payroll) {
                        $columns = json_decode($payroll->columns);
                        $payrollIds[] = $payroll->payroll_id;
                    }
                    $payroll_id_str = implode(',',$payrollIds);
                }

            if(!empty($calculation_group_payroll)){
                $payroll_columns = DB::table('report_columns')->where('type','LIKE','%pessi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->get(['payroll_column_id', 'name']);
                // $payroll_column_ids = DB::table('payrollcolumns')->where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                //     $query->whereNull('employers_column')->orWhere('employers_column', '');
                // })->orderBy('colorder', 'asc')->pluck('id');
                $report_columns = DB::table('report_columns')->where('type','LIKE','%pessi%')->where('calculation_group', $request->calculation_group)->orderBy('column_order', 'ASC')->pluck('payroll_column_id');
                // $all_childs =  getChildren($calculation_group_payroll->department);
                // array_push($all_childs,$calculation_group_payroll->department);
                $all_childs =  getChildren2($request->departmentFilter);
                $direct_employees = DB::table('employees')->where('department', $request->departmentFilter)->pluck('id');
                if(count($direct_employees) > 0){
                    array_push($all_childs,$request->departmentFilter);
                }
                if($request->departmentFilter != 'all'){
                    $departments = DB::table('companystructures')->where('id', $request->departmentFilter)->orderBy('id', 'ASC')->get(['id', 'title']);
                }else{
                    $departments = DB::table('companystructures')->orderBy('id', 'ASC')->get(['id', 'title']);
                }
                foreach ($departments as $key => $department) {
                    $childrens =  getChildren2($department->id);
                    // array_push($childrens,$department->id);
                    // if(env('COMPANY') != 'JSML'){
                        $all_childrens[$department->id] = DB::table('companystructures')->whereIn('id', $childrens)->orderBy('id', 'ASC')->get(['id', 'title']);
                        $children_ids = DB::table('companystructures')->whereIn('id', $childrens)->orderBy('title', 'ASC')->pluck('id');
                    // }else{
                    //     $all_childrens[$department->id] = DB::table('companystructures')->whereIn('id', $childrens)->orderBy('id', 'ASC')->get(['id', 'title']);
                    //     $children_ids = DB::table('companystructures')->whereIn('id', $request->section)->orderBy('title', 'ASC')->pluck('id');
                    // }
                    foreach ($all_childrens[$department->id] as $key2 => $child) {
                        $total_employees[$child->id] = DB::table('payrolldata')->join('employees','employees.id' , '=','payrolldata.employee')
                        ->where('payrolldata.department_id',$child->id)
                        ->whereIn('payrolldata.payroll', $payrollIds)
                        ->whereIn('payrolldata.employment_status_id', $request->employment_status)
                        ->where('employees.pessi', 'yes')
                        ->where(function ($query) use ($request) {
                            $query->where(function ($query) use ($request) {
                                $query->whereNull('termination_date')
                                    ->orWhere('termination_date', '>=', $request->fromDate.'-01');
                            })
                            ->where(function ($query) use ($request) {
                                $query->whereNull('off_roll_date')
                                    ->orWhere('off_roll_date', '>=', $request->fromDate.'-01');
                            });
                        })
                        ->groupBy('payrolldata.employee')->get(['payrolldata.employee']);
                        $columns_totals[$child->id] = DB::table('payrolldata')
                                ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                                ->join('report_columns', 'report_columns.payroll_column_id', '=', 'payrolldata.payroll_item')
                                ->where('employees.pessi', 'yes')
                                ->whereIn('payrolldata.employment_status_id', $request->employment_status)
                                ->whereIn('payrolldata.payroll', $payrollIds)
                                ->whereIn('payrolldata.payroll_item', $report_columns)
                                ->where('payrolldata.department_id', $child->id)
                                ->groupBy('report_columns.payroll_column_id')->orderBy('report_columns.column_order', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item' , 'report_columns.is_decimal']);
                    }
                            $parent_totals[$department->id] = DB::table('payrolldata')
                                ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                                ->join('report_columns', 'report_columns.payroll_column_id', '=', 'payrolldata.payroll_item')
                                ->join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                                ->where('employees.pessi', 'yes')
                                ->where('payrollcolumns.is_visible', 1)
                                ->whereIn('payrolldata.employment_status_id', $request->employment_status)
                                ->whereIn('payrolldata.payroll', $payrollIds)
                                ->whereIn('payrolldata.payroll_item', $report_columns)
                                ->whereIn('payrolldata.department_id', $children_ids)
                                ->groupBy('report_columns.payroll_column_id')->orderBy('report_columns.column_order', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount'),'payrolldata.payroll_item' , 'report_columns.is_decimal']);
                        }
            }
        }
        if(request()->export_type == "excel"){
            $date = request()->date;
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;

            return Excel::download(new PessiSummaryExport($calculation_group_payroll , $payroll_columns , $departments , $total_employees , $columns_totals , $date , $cal_group , $payrollIds), 'Pessi Monthly Summary Export Sheet.xlsx');
        }
        $date = request()->date;
        if(request()->export_type == "pdf"){
            $cal_group =  CalculationGroup::find(request()->calculation_group)->name;
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.pessi_monthly_summery_report_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
            return $pdf->download('Pessi Monthly Summery Report.pdf');
        }
        $childvar =( getChildren($request->departmentFilter));
        $parentvar = DB::table('companystructures')->whereIn('companystructures.id',$childvar)->select('id','title')->get();
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $calculation_groups = DB::table('deductiongroup')->whereIn('id',$this->user_calculation_groups)->get();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->leftjoin('employees AS e2' , 'e2.id' , '=' , 'company_setup.payroll_officer')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName' , 'e1.last_name AS gm_lName' , 'e2.first_name AS payroll_fName' , 'e2.last_name AS payroll_lName']);
        return view('Admin.reports.pessi_monthly_summery' , get_defined_vars());
    }

    public function leaveReport(Request $request){
        $periods = LeavePeriods::select('leaveperiods.*')->get();
        if(!empty(request()->departmentFilter)){
            $departmentEmployees = DB::table('employees')->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title');
            $employees = DB::table('employees')->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title');
            if(request()->departmentFilter != 'all'){
                $employees->where('employees.department' , request()->departmentFilter);
                $departmentEmployees->where('department' , request()->departmentFilter);
            }
            if(!empty(request()->employeeFilter)){
                $employees->where('employees.id' , request()->employeeFilter);
            }
            $departmentEmployees = $departmentEmployees->where('employees.status' , 'Active')->get(['employees.id' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' , 'employees.last_name' ,'employees.middle_name', 'jobtitles.name AS designation']);

            $employees = $employees->where('employees.status' , 'Active')
            ->where('suspended' , 0)
            ->get(['employees.id' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' , 'employees.last_name' ,'employees.middle_name', 'companystructures.title AS department' , 'jobtitles.name AS designation']);
        }
        $leave_groups = DB::table('leavegroupemployees')->whereNull('deleted_at')->where('employee', request()->employeeFilter)->pluck('leave_group')->toArray();
        // $leave_groups = DB::table('leavegroupemployees')->pluck('leave_group')->toArray();
        // dd($leave_groups );
        if(count($leave_groups) > 1){
            // dd($leave_groups);
            // $leave_types = LeaveTypes::whereIn('leave_group', $leave_groups)->select('leavetypes.*')->get();
            $leave_types = LeaveTypes::select('leavetypes.*')->get();
        }
        else{
            $leave_groups = DB::table('leavegroupemployees')->pluck('leave_group')->toArray();
            $leave_types = LeaveTypes::where('leave_group', $leave_groups)->select('leavetypes.*')->get();
            // dd($leave_types)

        }
        return view('Admin.reports.leave_report' , get_defined_vars());
    }

    public function annualLeaveEncashmentReport(Request $request){
        $periods = LeavePeriods::select('leaveperiods.*')->get();
        if(!empty(request()->departmentFilter)){
            $departmentEmployees = DB::table('employees')->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title');
            $employees = DB::table('employees')->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title');
            if(request()->departmentFilter != 'all'){
                $employees->where('employees.department' , request()->departmentFilter);
                $departmentEmployees->where('department' , request()->departmentFilter);
            }
            if(!empty(request()->employeeFilter)){
                $employees->where('employees.id' , request()->employeeFilter);
            }
            $departmentEmployees = $departmentEmployees->where('employees.status' , 'Active')->get(['employees.id' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' , 'employees.last_name' ,'employees.middle_name', 'jobtitles.name AS designation']);

            $employees = $employees->where('employees.status' , 'Active')
            ->where('suspended' , 0)
            ->get(['employees.id' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' , 'employees.last_name' ,'employees.middle_name', 'companystructures.title AS department' , 'jobtitles.name AS designation','companystructures.id as department_id']);
        }
        $annual_leave_type = LeaveTypes::where('name','Annual Leave')->first();
        if ($request->export_type == "pdf") {
            $page_size = 'A3';
            $pdf = PDF::loadView('Admin.reports.annual_leave_encashment_report_pdf', get_defined_vars())->setPaper($page_size, 'landscape');
            return $pdf->download('Leave Encashment Report.pdf');
        }
        if ($request->export_type == "excel") {
            return Excel::download(new LeaveEncashmentReportExport($employees, $annual_leave_type), 'Leave Encashment Report.xlsx');
        }
        return view('Admin.reports.annual_leave_encashment_report' , get_defined_vars());
    }

    public function departmentWiseSalaryReport(){
        $locations = DB::table('employees')->select('work_station_id' , 'city')
        ->where('status' , 'Active')->where('work_station_id' , '!=' , '')
        ->groupBy('work_station_id')
        ->orderBy('employees.employee_code')
        ->get(['work_station_id','city']);
        $user_calculation_groups = !empty(Auth::user()->user_calculation_groups) ? json_decode(Auth::user()->user_calculation_groups) : [];
        $payroll_types = DB::table('deductiongroup')->whereIn('id', $user_calculation_groups)->get(['id', 'name']);
        $selected_month = date('m' , strtotime(request()->monthFilter));
        $selected_year = date('Y' , strtotime(request()->monthFilter));
        $net_column = $selected_year == '2022' && $selected_month <= '05' ? 76 : 111;
        $employees = DB::table('employees')
        ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department');
        if(!empty(request()->departmentFilter)){
            $employees = $employees->whereIn('employees.department' , request()->departmentFilter);
        }
        if(!empty(request()->locationFilter)){
            $employees = $employees->whereIn('employees.work_station_id' , request()->locationFilter);
        }
        $employees = $employees->get(['employees.id' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' , 'employees.last_name' , 'companystructures.title AS department']);
        $data = [];
        $total_salary=0;
        $total_deduction=0;
        $total_net_payable=0;
        $total_loan=0;
        $total_advance=0;

        foreach($employees as $employee){
            $data[$employee->id]['employee_id'] = $employee->employee_id;
            $data[$employee->id]['employee_code'] = $employee->employee_code;
            $data[$employee->id]['name'] = $employee->first_name.' '.$employee->last_name;
            $data[$employee->id]['department'] = $employee->department;

            $payrolls = DB::table('payroll')->whereYear('date_start' , date('Y' , strtotime(request()->monthFilter)))->whereMonth('date_start', date('m', strtotime(request()->monthFilter)))->where('status', 'Completed');
            if(!empty(request()->payrollFilter)){
                $payrolls = $payrolls->where('deduction_group' , request()->payrollFilter);
            }
            $payrolls = $payrolls->get(['id', 'name']);
            foreach($payrolls as $payroll){
                $net_salary = DB::table('payrolldata')->where('payroll', $payroll->id)->where('employee', $employee->id)->where('payroll_item' , $net_column)->first();
                if(!empty($net_salary)){
                    $data[$employee->id]['payroll'] = $payroll->name;
                    $data[$employee->id]['net_payable'] = $net_salary->amount;
                    $total_net_payable = $total_net_payable + $net_salary->amount;

                    $gross_salary = DB::table('payrolldata')->where('payroll' , $payroll->id)->where('employee' , $employee->id)->where('payroll_item' , 110)->first();
                    if(!empty($gross_salary)){
                        $data[$employee->id]['gross_salary'] = $gross_salary->amount;
                        $total_salary = $total_salary + $gross_salary->amount;
                    }

                    $loan_amount = DB::table('payrolldata')->where('payroll' , $payroll->id)->where('employee' , $employee->id)->where('payroll_item' , 79)->first();
                    if(!empty($loan_amount)){
                        $data[$employee->id]['loan_amount'] = $loan_amount->amount;
                        $total_loan = $total_loan + $loan_amount->amount;
                    }

                    $advance_amount = DB::table('payrolldata')->where('payroll' , $payroll->id)->where('employee' , $employee->id)->where('payroll_item' , 15)->first();
                    if(!empty($advance_amount)){
                        $data[$employee->id]['advance_amount'] = $advance_amount->amount;
                        $total_advance = $total_advance + $advance_amount->amount;
                    }

                    $deducations = DB::table('payrolldata')->where('payroll' , $payroll->id)->where('employee' , $employee->id)->where('payroll_item' , 75)->first();
                    if(!empty($deducations)){
                        $data[$employee->id]['deducations'] = $deducations->amount;
                        $total_deduction = $total_deduction + $deducations->amount;
                    }
                }
            }
        }
        $data['total_net_payable'] = $total_net_payable;
        $data['total_deduction'] = $total_deduction;
        $data['total_salary'] = $total_salary;
        $data['total_loan'] = $total_loan;
        $data['total_advance'] = $total_advance;
        return view('Admin.finance.departmentWiseSalaryReport.index' , get_defined_vars());
    }

    public function locationWiseReport(){
        $locations = DB::table('employees')->select('work_station_id' , 'city')
        ->where('status' , 'Active')->where('work_station_id' , '!=' , '');
        if(!empty(request()->locationFilter)){
            $locations->where('employees.work_station_id' , request()->locationFilter);
        }
        $locations = $locations->groupBy('work_station_id')->get(['work_station_id','city']);

        $filter_department = '';
        if(!empty(request()->departmentFilter)){
            $filter_department = request()->departmentFilter;
        }

        $year = !empty(request()->monthFilter) ? date('Y' , strtotime(request()->monthFilter)) : date('Y');
        $month = !empty(request()->monthFilter) ? date('m' , strtotime(request()->monthFilter)) : date('m');
        $net_column = $month > '05' ? 111 : 76;

        $payrolls = Payroll::whereYear('date_start' , $year)
        ->whereMonth('date_start' , $month)
        ->where('status', 'Completed')
        ->pluck('id')->toArray();
        return view('Admin.finance.locationWiseSalaryReport.index' , get_defined_vars());
    }

    public function loan_report(Request $request)
    {
        if(!empty($request->fromMonth))
        {
            $fromMonth = strtotime($request->fromMonth);
            $toMonth = strtotime($request->toMonth);
            while ($fromMonth <= $toMonth) {
                $months[] = date('F Y',$fromMonth);
                $fromMonth = strtotime('+1 MONTH', $fromMonth);
            }
            if (env('COMPANY') != 'JSML') {
                $query = LoanRequest::join('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
                    ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                    ->where([
                        ['loan_requests.created_at', '>=', $request->fromMonth.'-01 00:00:00'],
                        ['loan_requests.created_at', '<=', $request->toMonth.'-31 23:59:59']
                    ])
                    ->whereIn('employees.department' , login_user_departments())
                    ->where('loan_requests.status' , '=' , 'Approved');
                }else{
                    $query = LoanRequest::join('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
                    ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                    ->where([
                        ['loan_requests.start_date', '>=', $request->fromMonth.'-01 00:00:00'],
                        ['loan_requests.start_date', '<=', $request->toMonth.'-31 23:59:59']
                    ])
                    ->whereIn('employees.department' , login_user_departments())
                    ->where('loan_requests.status' , '=' , 'Approved');
                }
                if(!empty($request->section))
                {
                    $query->whereIn('employees.department', $request->section);
                }
                if(!empty($request->employeeFilter))
                {
                    $query->where('loan_requests.employee_id', $request->employeeFilter);
                }
                if(!empty($request->typeFilter !='all'))
                {
                    $query->where('loan_requests.loan_type', $request->typeFilter);
                }
                if($request->statusFilter == 'completed')
                {
                    $query->where('loan_requests.remaining_amount', 0);
                }
                if ($request->statusFilter == 'settled') {
                    $query->where('loan_requests.final_satelment', 1);
                }
                if ($request->statusFilter == 'progress') {
                    $query->where([
                        ['loan_requests.remaining_amount', '>', 0],
                        ['loan_requests.final_satelment', '=', 0]
                    ]);
                }
            $loans = $query->get(['loan_requests.*','employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name', 'employees.last_name', 'companystructures.title']);
            if($request->has('pdf')){
                $pdf = PDF::loadView('Admin.request&approvals.loanReport.pdf', compact('loans', 'months'))->setPaper('legal', 'landscape');
                $currentDateTime = date('Y-m-d H:i:s');
                return $pdf->download("Loan Report-$currentDateTime.pdf");
            }
            if($request->has('excel')){
                $currentDateTime = date('Y-m-d H:i:s');
                return Excel::download(new LoanReportExport($loans, $months), "Loan Report-$currentDateTime.xlsx");
            }
        }
        $loan_types = DB::table('loan_types')->get();
        return view('Admin.request&approvals.loanReport.loan_report' , get_defined_vars());
    }

    public function deduction_report(Request $request)
    {
        $month = strtotime($request->month);
        if(isset($request->payrolls))
        {
            if($request->departmentFilter != 'all'){
                $all_childs =  getChildren($request->departmentFilter);
                array_push($all_childs,$request->departmentFilter);
            }
            $employees = DB::table('employees')->where('status', 'Active');
            if($request->departmentFilter != 'all'){
                $employees->whereIn('department', $all_childs);
            }
            $employees = $employees->get(['id', 'employee_id', 'employee_code','first_name','last_name','middle_name']);
            $payrolls = Payroll::where('status', '=', 'Completed')->whereYear('date_start', '=', date('Y', $month))->whereMonth('date_start', '=', date('m', $month))->get(['id','name']);

            $payroll_columns = PayrollColumn::whereIn('id', $request->payrollFilter)->orderBy('colorder', 'ASC')->get(['id', 'name']);
            $payroll_column_ids = PayrollColumn::whereIn('id', $request->payrollFilter)->orderBy('colorder', 'ASC')->pluck('id')->toArray();
            $query = PayrollData::join('employees', 'employees.id', '=', 'payrolldata.employee')
            ->join('payroll', 'payroll.id', '=', 'payrolldata.payroll')
            ->whereIn('payrolldata.payroll',$request->payrolls);
            if(isset($request->suppress))
            {
                // if(count($request->payrollFilter) == 1)
                // {
                    $query->whereIn('payrolldata.payroll_item',$request->payrollFilter);
                // }
            }

            $total_query = DB::table('payrolldata')
                        ->join('employees', 'employees.id', '=', 'payrolldata.employee')
                        ->join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                        ->whereIn('payrolldata.payroll', $request->payrolls)
                        ->whereIn('payrolldata.payroll_item', $payroll_column_ids);

            if($request->departmentFilter != 'all'){
                $query->whereIn('payrolldata.department_id',$all_childs);
                $total_query->whereIn('payrolldata.department_id', $all_childs);
            }
            if(!empty($request->employeeFilter))
            {
                $query->where('payrolldata.employee', '=', $request->employeeFilter);
                $total_query->where('payrolldata.employee', '=', $request->employeeFilter);
            }
            if(!empty($request->employment_status))
            {
                $query->whereIn('payrolldata.employment_status_id',$request->employment_status);
                $total_query->whereIn('payrolldata.employment_status_id',$request->employment_status);
            }
            if(isset($request->suppress))
            {
                // if(count($request->payrollFilter) == 1)
                // {
                    $query->where('payrolldata.amount', '!=',0);
                // }
            }
            $employees = $query->groupBy('payrolldata.employee')->orderBy('payroll.id', 'ASC')->get(['employees.employee_id','payrolldata.employee', 'employees.first_name', 'employees.middle_name', 'employees.employee_code', 'employees.last_name' , 'payrolldata.department' , 'payrolldata.payroll_item' , 'payrolldata.amount','payrolldata.employement_status as status','payroll.id as payroll_id','payroll.name as payroll_name']);
            $totals = $total_query->groupBy('payrollcolumns.id')->orderBy('payrollcolumns.colorder', 'ASC')->get([DB::raw('SUM(payrolldata.amount) as amount')]);
        }
        $payrollColumn = PayrollColumn::get(['id','name']);
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $employment_status_show='';
        if(!empty($request->employment_status))
        {
            $employment_status_show = DB::table('employmentstatus')->whereIn('employmentstatus.id',$request->employment_status)->get(['name']);
            // $employment_status_show=json_encode($employment_status_show);
        }
        return view('Admin.reports.deduction_report.deduction_report' , get_defined_vars());
    }

    public function getPayrollsByMonth($month)
    {
        $payrolls = DB::table('payroll')->where('status', '=', 'Completed')->whereYear('date_start', '=', date('Y', strtotime($month)))
        ->whereMonth('date_start', '=', date('m', strtotime($month)))->get(['id' , 'name']);
        return response()->json($payrolls);
    }

    public function payrollLedger(Request $request)
    {
        $employees = Employee::where('status','Active')->selectRaw('id,Concat(employee_id,"-",first_name," ",COALESCE(last_name,"")) as name')->get();

        $payrolls = Payroll::where('status', 'Completed')->where('ledgerised', 1)->get();

        $records = Ledger::query();
        if(isset($request->employeeFilter)){
            $records = $records->where('employee_id',$request->employeeFilter);
        }
        if(isset($request->categoryFilter)){
            $records = $records->where('type',$request->categoryFilter);
        }
        if(isset($request->payrollFilter)){
            $payroll_obj = Payroll::find($request->payrollFilter);
            $records = $records->where('payroll_id',$request->payrollFilter);
        }
        if(isset($request->employeeFilter) || isset($request->categoryFilter) || isset($request->payrollFilter)){
            $records = $records->get();
        }
        return view('Admin.reports.ledgers.payroll_ledger' , get_defined_vars());
    }
    public function LoginReport(Request $request)
    {
        $users = User::get(['id','username']);
        if($request->ajax()){
            $query = [];
            if(!empty($request->user)){
                $query = User::join('authentication_log','users.id','=','authentication_log.authenticatable_id')->where('users.id',$request->user);
                if(!empty($request->fromDate)){
                    $date = !empty($request->toDate) ? $request->toDate : date('Y-m-d');
                    $query->whereDate('authentication_log.login_at','>=',$request->fromDate)->whereDate('authentication_log.login_at','<=',$date);
                }
                $query->select('users.id','users.username','authentication_log.ip_address','authentication_log.login_at','authentication_log.logout_at');
            }
            return Datatables::of($query)->addIndexColumn()->make(true);
        }
        return view('Admin.reports.login_report',compact('users'));
    }

    public function time_conflict_report(Request $request)
    {
        if($request->ajax()){
            $query = [];
            if(!empty($request->department)){
                $query = Attendance::join('employees','employees.id','=','attendance.employee')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->where('employees.status','Active')
                ->where('attendance.is_manual',0);
                if (!empty($request->department)) {
                    $query->where('department_id', $request->department);
                }
                if (!empty($request->employee)) {
                    $query->where('employee', $request->employee);
                }
                if (!empty($request->fromDate)) {
                    $query->whereDate('in_time', '>=', $request->fromDate);
                }
                if (!empty($request->toDate)) {
                    $query->whereDate('in_time', '<=', $request->toDate);
                }
                $query->select('attendance.*', 'employees.first_name', 'employees.last_name', 'companystructures.title');
            }
            return Datatables::of($query)->addIndexColumn()->make(true);
        }
        return view('Admin.reports.time_conflict_report',get_defined_vars());
    }
}
