<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\WorkWeek;
use App\Models\WorkDay;
use App\Models\Holidays;
use App\Models\HolidaySetting;
use App\Models\LeaveGroup;
use App\Models\LeaveRules;
use App\Models\LeaveTypes;
use App\Models\PaidTimeOff;
use App\Models\SpecialDay;
use App\Models\LeavePeriods;
use App\Models\LeaveTypeSetting;
use App\Models\CompanyStructure;
use App\Models\EmploymentStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LeaveGroupEmployee;
use Illuminate\Validation\ValidationException;

class LeaveTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            if($request->type == 'leave_type')
            {
                return DataTables::eloquent(LeaveTypes::leftjoin('leavegroups' , 'leavegroups.id' , '=' , 'leavetypes.leave_group')->select('leavetypes.id','leavetypes.name' , 'leavetypes.abbreviation' , 'leavetypes.leave_accrue' , 'leavetypes.carried_forward' , 'leavetypes.default_per_year' , 'leavegroups.name as leave_group_name' , 'leavetypes.leave_color'))->addIndexColumn()->make(true);
            }
            elseif($request->type == 'leave_period')
            {
                return DataTables::eloquent(LeavePeriods::query())->addIndexColumn()->make(true);
            }
            elseif($request->type == 'work_week')
            {
                return DataTables::eloquent(WorkWeek::query())->addIndexColumn()->make(true);
            }
            elseif($request->type == 'holidays')
            {
                return DataTables::eloquent(Holidays::leftjoin('country' , 'country.id' , '=' , 'holidays.country')
                ->select('holidays.id','holidays.name' , 'holidays.dateh' , 'holidays.status' , 'country.namecap')->whereYear('dateh', date('Y')))
                ->addIndexColumn()->make(true);
            }
            elseif($request->type == 'leave_rules')
            {
                return DataTables::eloquent(LeaveRules::leftjoin('leavegroups' , 'leavegroups.id' , '=' , 'leaverules.leave_group')->leftjoin('leavetypes' , 'leavetypes.id' , '=' , 'leaverules.leave_type')
                ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'leaverules.job_title')
                ->leftjoin('employmentstatus' , 'employmentstatus.id' , '=' , 'leaverules.employment_status')
                ->leftjoin('employees' , 'employees.id' , '=' , 'leaverules.employee')
                ->select('leaverules.id' , 'leaverules.exp_days' , 'leaverules.default_per_year' ,  'leavegroups.name AS leaveGroupName' , 'leavetypes.name AS leave_name' , 'jobtitles.specification' , 'employmentstatus.name' , 'employees.first_name' , 'employees.last_name'))->addIndexColumn()->make(true);
            }
            elseif($request->type == 'paid_time_off')
            {
                return DataTables::eloquent(PaidTimeOff::leftJoin('leavetypes' , 'leavetypes.id' , '=' , 'leavestartingbalance.leave_type')
                ->leftJoin('employees' , 'employees.id' , '=' , 'leavestartingbalance.employee')
                ->leftJoin('leaveperiods' , 'leaveperiods.id' , 'leavestartingbalance.leave_period')
                ->select('leavestartingbalance.id','leavestartingbalance.amount' , 'leavetypes.name' , 'employees.first_name AS fname' , 'employees.last_name AS lname' ,  'leaveperiods.name AS periodName'))->addIndexColumn()->make(true);
            }
            elseif($request->type == 'edit_leave_group')
            {
                return DataTables::eloquent(LeaveGroup::query())->addIndexColumn()->make(true);
            }
            elseif($request->type == 'all_leave_employee')
            {   
                return DataTables::eloquent(LeaveGroupEmployee::leftjoin('employees' , 'leavegroupemployees.employee' , '=' , 'employees.id')
                ->leftjoin('leavegroups' , 'leavegroupemployees.leave_group' , '=' , 'leavegroups.id')
                ->select('leavegroupemployees.id','employees.first_name' , 'employees.last_name' , 'leavegroups.name'))->addIndexColumn()->make(true);
            }
            elseif($request->type == 'special_working_day')
            {   
                return DataTables::eloquent(SpecialDay::leftjoin('employees' , 'specialdays.emp_id' , '=' , 'employees.id')
                ->select('specialdays.*','employees.first_name' , 'employees.last_name'))->addIndexColumn()->make(true);
            }
            elseif($request->type == 'leave_type_setting')
            {   
                return DataTables::eloquent(LeaveTypeSetting::join('leavetypes' , 'leavetypes.id' , '=' , 'leave_type_settings.leave_type_id')
                ->leftJoin('employees' , 'employees.id' , '=' , 'leave_type_settings.employee')
                ->select('leave_type_settings.*', 'leavetypes.name', 'employees.first_name' , 'employees.last_name'))
                ->addColumn('employment_statuses_names', function ($row) {
                    return EmploymentStatus::whereIn('id', $row->employement_statuses)->pluck('name')->implode(', ');
                })
                ->addColumn('department_names', function ($row) {
                    return CompanyStructure::whereIn('id', $row->departments)->pluck('title')->implode(', ');
                })
                ->addColumn('section_names', function ($row) {
                    return CompanyStructure::whereIn('id', $row->sections)->pluck('title')->implode(', ');
                })
                ->addIndexColumn()->make(true);
            }
            elseif($request->type == 'holiday_setting')
            {   
                return DataTables::eloquent(HolidaySetting::query())
                ->addColumn('employment_statuses_names', function ($row) {
                    return EmploymentStatus::whereIn('id', $row->employement_statuses)->pluck('name')->implode(', ');
                })
                ->addColumn('department_names', function ($row) {
                    return CompanyStructure::whereIn('id', $row->departments)->pluck('title')->implode(', ');
                })
                ->addColumn('section_names', function ($row) {
                    return CompanyStructure::whereIn('id', $row->sections)->pluck('title')->implode(', ');
                })
                ->addColumn('holiday_names', function ($row) {
                    return Holidays::whereIn('id', $row->holidays)->pluck('name')->implode(', ');
                })
                ->addIndexColumn()->make(true);
            }
            else{
                return DataTables::eloquent(LeaveGroupEmployee::leftjoin('employees' , 'leavegroupemployees.employee' , '=' , 'employees.id')
                ->leftjoin('leavegroups' , 'leavegroupemployees.leave_group' , '=' , 'leavegroups.id')
                ->select('leavegroupemployees.id','employees.first_name' , 'employees.last_name' , 'leavegroups.name'))->addIndexColumn()->make(true);
            }
        }
        $leaveGroups = LeaveGroup::get(['id','name']);
        $countries = DB::table('country')->get();
        $leaveTypes = DB::table('leavetypes')->get();
        $allow = DB::table('leavetypes')->first(['allow']);
        $holidays = DB::table('holidays')->whereYear('dateh', date('Y'))->get(['id', 'name']);

        // $leaveTypesAllow = DB::table('leavetypes')->first();
        
        $jobTitles = DB::table('jobtitles')->get();
        $employmentStatus = DB::table('employmentstatus')->get();
        $leavePeriods = DB::table('leaveperiods')->get();
        $weekDays = ['1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday'];
        return view('Admin.setup.leave_settings.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Leave Types
            if($request->type == 'leave_type'){
                $request->validate([
                    'name' => 'required',
                    'abbreviation' => 'required',
                    'default_per_year' => 'required',
                    'supervisor_leave_assign' => 'required',
                    'employee_can_apply' => 'required',
                    'apply_beyond_current' => 'required',
                    'leave_accrue' => 'required',
                    'carried_forward' => 'required',
                    'more_quota' => 'required',
                    'max_carried_forward_amount' => 'required',
                    'carried_forward_leave_availability' => 'required',
                    'propotionate_on_joined_date' => 'required',
                    'send_notification_emails' => 'required',
                    'leave_group' => 'required',
                    'leave_color' => 'required',
                ]);

                $data = $request->all();
                LeaveTypes::create($data);
            }
            // Leave Periods
            elseif($request->type == 'leave_period'){
                $this->validate($request , [
                    'name' => 'required',
                    'date_start' => 'required',
                    'date_end' => 'required|after_or_equal:date_start',
                ]);
                if($request->status == 'Active'){
                    $activePeriod = LeavePeriods::where('status', 'Active')->first();
                    if(!empty($activePeriod)){
                        return ['code' => '500' , 'errorMessage' => 'Leave Period is already active, please deactivate it first!'];
                    }
                }
                $data = $request->all();
                LeavePeriods::create($data);
            }
            // Work Week
            elseif($request->type == 'work_week'){
                $weekDays = [
                    '1'=>'Monday',
                    '2'=>'Tuesday',
                    '3'=>'Wednesday',
                    '4'=>'Thursday',
                    '5'=>'Friday',
                    '6'=>'Saturday',
                    '7'=>'Sunday',
                ];
                $work_week = new WorkWeek();
                $work_week->desc = $request->work_week_name;
                if($request->status == 2){
                    $work_week->work_week_status = 0;
                }elseif($request->status == 1){
                    $work_week->work_week_status = 1;
                }
                $work_week->save();
                foreach($weekDays as $key => $day)
                {
                    if(in_array($key, $request->off_days)){
                        $dayStatus = 'Non-working Day';
                    }else{
                        $dayStatus = 'Full Day';
                    }
                    $work_day = new WorkDay();
                    $work_day->name = $day;
                    $work_day->status = $dayStatus;
                    $work_day->work_week_id = $work_week->id;
                    $work_day->save();
                }
            }
            // Holidays
            elseif($request->type == 'holiday'){
                $this->validate($request , [
                    'name' => 'required',
                    'dateh' => 'required',
                    'status' => 'required',
                    'country' => 'required',
                ]);
                $data = $request->all();
                Holidays::create($data);
            }
            // Leave Rules
            elseif($request->type == 'leave_rule'){
                $this->validate($request , [
                    'leave_type' => 'required',
                    'leave_group' => 'required',
                    'job_title' => 'required',
                    'employment_status' => 'required',
                    'employee' => 'required',
                    'exp_days' => 'required',
                    'default_per_year' => 'required',
                    'supervisor_leave_assign' => 'required',
                    'employee_can_apply' => 'required',
                    'apply_beyond_current' => 'required',
                    'leave_accrue' => 'required',
                    'carried_forward_percentage' => 'required',
                    'max_carried_forward_amount' => 'required',
                    'carried_forward_leave_availability' => 'required',
                    'propotionate_on_joined_date' => 'required',
                ]);
                $data = $request->all();
                LeaveRules::create($data);
            }
            // Paid Time Off
            elseif($request->type == 'paid_time_off'){
                $this->validate($request , [
                    'leave_type' => 'required',
                    'employee' => 'required',
                    'leave_period' => 'required',
                    'amount' => 'required',
                    'note' => 'required',
                ]);
                $data = $request->all();
                PaidTimeOff::create($data);
            }
            // Edit Leave Group
            elseif($request->type == 'edit_leave_group'){
                $this->validate($request , [
                    'name' => 'required',
                    'details' => 'required',
                ]);
                $data = $request->all();
                LeaveGroup::create($data);
            }
            // Leave Group For All Employee
            elseif($request->type == 'all_leave_group_employee'){
                $this->validate($request , [
                ]);
                $employees = DB::table('employees')->where('status','Active')->pluck('id');
                $data = $request->all_leave_group;
                foreach($employees as $emp){
                    DB::table('leavegroupemployees')->insert(['employee' => $emp, 'leave_group' => $data]);
                }
            }
            // Leave Group For All Employee
            elseif($request->type == 'special_working_day'){
                $this->validate($request , [
                ]);
                $date = $request->special_day;
                foreach($request->get_employees as $emp){
                    DB::table('specialdays')->insert(['emp_id' => $emp, 'special_day' => $date]);
                }
            }
            // Leave Setting
            elseif($request->type == 'leave_type_setting'){
                if(!empty($request->employee)){
                    LeaveTypeSetting::create([
                        'leave_type_id' => $request->leave_type_for_setting,
                        'employee' => $request->employee,
                        'amount' => $request->amount,
                    ]);
                }else{
                    LeaveTypeSetting::create([
                        'leave_type_id' => $request->leave_type_for_setting,
                        'departments' => isset($request->departments) ? json_encode($request->departments) : null,
                        'sections' => isset($request->sections) ? json_encode($request->sections) : null,
                        'employement_statuses' => isset($request->employement_statuses) ? json_encode($request->employement_statuses) : null,
                        'amount' => $request->amount,
                    ]);
                }
            }
            // Holiday Setting
            elseif($request->type == 'holiday_setting'){
                HolidaySetting::create([
                    'departments' => isset($request->departments) ? json_encode($request->departments) : null,
                    'sections' => isset($request->sections) ? json_encode($request->sections) : null,
                    'employement_statuses' => isset($request->employement_statuses) ? json_encode($request->employement_statuses) : null,
                    'holidays' => isset($request->holidays) ? json_encode($request->holidays) : null,
                ]);
            }
            // Leave Group Employee
            else{
                $this->validate($request , [
                    'employee' => 'required',
                    'leave_group' => 'required',
                ]);
                $data = $request->all();
                LeaveGroupEmployee::create($data);
            }
            return ['code' => '200' , 'message' => 'success'];
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveTypes $leaveTypes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if($request->type == 'leave_type'){
            $data = LeaveTypes::find($id);
            return response()->json($data);
        }
        // Leave Periods
        elseif($request->type == 'leave_period'){
            $data = LeavePeriods::find($id);
            return response()->json($data);
        }
        // Work Week
        elseif($request->type == 'work_week'){
            $work_week = WorkWeek::find($id);
            $work_days = WorkDay::where('work_week_id', $id)->where('status', 'Non-working Day')->pluck('name');
            return response()->json(['work_week' => $work_week, 'work_days' => $work_days]);
        }
        // Holidays
        elseif($request->type == 'holiday'){
            $data = Holidays::find($id);
            return response()->json($data);
        }
        // Leave Rules
        elseif($request->type == 'leave_rule'){
            $data = LeaveRules::find($id);
            return response()->json($data);
        }
        // Paid Time Off
        elseif($request->type == 'paid_time_off'){
            $data = PaidTimeOff::find($id);
            return response()->json($data);
        }
        // Leave Group
        elseif($request->type == 'leave_group'){
            $data = LeaveGroup::find($id);
            return response()->json($data);
        }
        // Leave Setting
        elseif($request->type == 'leave_type_setting'){
            $data = LeaveTypeSetting::find($id);
            return response()->json($data);
        }
        elseif($request->type == 'holiday_setting'){
            $data = HolidaySetting::find($id);
            return response()->json($data);
        }
        // Leave Group Employee
        else{
            $data = LeaveGroupEmployee::find($id);
            return response()->json($data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            // Leave Types
            if($request->type == 'leave_type'){
                $this->validate($request , [
                    'name' => 'required',
                    'abbreviation' => 'required',
                    'default_per_year' => 'required',
                    'supervisor_leave_assign' => 'required',
                    'employee_can_apply' => 'required',
                    'apply_beyond_current' => 'required',
                    'leave_accrue' => 'required',
                    'more_quota' => 'required',
                    'carried_forward' => 'required',
                    'max_carried_forward_amount' => 'required',
                    'carried_forward_leave_availability' => 'required',
                    'propotionate_on_joined_date' => 'required',
                    'send_notification_emails' => 'required',
                    'leave_group' => 'required',
                    'leave_color' => 'required',
                ]);
                $data = $request->all();
                // if(!isset($request->sandwich_leave))
                // {
                //    
                //  $data['sandwich_leave'] = 0;
                // }
                $data['allow']=!empty($request->allow) ? 'allow' : Null;
                $leaveTypes = LeaveTypes::find($id);
                $leaveTypes->fill($data)->save();
            }
            // Leave Periods
            elseif($request->type == 'leave_period'){
                $this->validate($request , [
                    'name' => 'required',
                    'date_start' => 'required',
                    'date_end' => 'required|after_or_equal:date_start',
                ]);
                if($request->status == 'Active'){
                    $activePeriod = LeavePeriods::where('id','!=',$id)->where('status', 'Active')->first();
                    if(!empty($activePeriod)){
                        return ['code' => '500' , 'errorMessage' => 'Leave Period is already active, please deactivate it first!'];
                    }
                }
                $leave_periods = LeavePeriods::find($id);
                $leave_periods->fill($request->all())->save();
            }
            // Work Week
            elseif($request->type == 'work_week'){
                $this->validate($request , [
                    'work_week_name' => 'required',
                ]);
                $weekDays = [
                    '1'=>'Monday',
                    '2'=>'Tuesday',
                    '3'=>'Wednesday',
                    '4'=>'Thursday',
                    '5'=>'Friday',
                    '6'=>'Saturday',
                    '7'=>'Sunday',
                ];
                $work_week = WorkWeek::find($id);
                $work_week->desc = $request->work_week_name;
                if($request->status == 2){
                    $work_week->work_week_status = 0;
                }elseif($request->status == 1){
                    $work_week->work_week_status = 1;
                }
                $work_week->save();
                WorkDay::where('work_week_id', $id)->delete();
                if($request->filled('off_days')){
                    foreach($weekDays as $key => $day)
                    {
                        if(in_array($key, $request->off_days)){
                            $dayStatus = 'Non-working Day';
                        }else{
                            $dayStatus = 'Full Day';
                        }
                        $work_day = new WorkDay();
                        $work_day->name = $day;
                        $work_day->status = $dayStatus;
                        $work_day->work_week_id = $id;
                        $work_day->save();
                    }
                }
            }
            // Holiday
            elseif($request->type == 'holiday'){
                $this->validate($request , [
                    'name' => 'required',
                    'dateh' => 'required',
                    'status' => 'required',
                    'country' => 'required',
                ]);
                $holidays = Holidays::find($id);
                $holidays->fill($request->all())->save();
            }
            // Leave Rules
            elseif($request->type == 'leave_rule'){
                $this->validate($request , [
                    'leave_type' => 'required',
                    'leave_group' => 'required',
                    'job_title' => 'required',
                    'employment_status' => 'required',
                    'employee' => 'required',
                    'exp_days' => 'required',
                    'default_per_year' => 'required',
                    'supervisor_leave_assign' => 'required',
                    'employee_can_apply' => 'required',
                    'apply_beyond_current' => 'required',
                    'leave_accrue' => 'required',
                    'carried_forward_percentage' => 'required',
                    'max_carried_forward_amount' => 'required',
                    'carried_forward_leave_availability' => 'required',
                    'propotionate_on_joined_date' => 'required',
                ]);
                $leaveRules = LeaveRules::find($id);
                $leaveRules->fill($request->all())->save();
            }
            // Paid Time Off
            elseif($request->type == 'paid_time_off'){
                $this->validate($request , [
                    'leave_type' => 'required',
                    'employee' => 'required',
                    'leave_period' => 'required',
                    'amount' => 'required',
                    'note' => 'required',
                ]);
                $paidTimeOff = PaidTimeOff::find($id);
                $paidTimeOff->fill($request->all())->save();
            }
            // Leave Group
            elseif($request->type == 'edit_leave_group'){
                $this->validate($request , [
                    'name' => 'required',
                    'details' => 'required',
                ]);
                $leaveGroup = LeaveGroup::find($id);
                $leaveGroup->fill($request->all())->save();
            }
            // Leave Setting
            elseif($request->type == 'leave_type_setting'){
                if(!empty($request->employee)){
                    LeaveTypeSetting::find($id)->update([
                        'leave_type_id' => $request->leave_type_for_setting,
                        'employee' => $request->employee,
                        'departments' => null,
                        'sections' => null,
                        'employement_statuses' => null,
                        'amount' => $request->amount,
                    ]);
                }else{
                    if($request->filled('departments') || $request->filled('employement_statuses')){
                        LeaveTypeSetting::find($id)->update([
                            'leave_type_id' => $request->leave_type_for_setting,
                            'departments' => isset($request->departments) ? json_encode($request->departments) : null,
                            'sections' => isset($request->sections) ? json_encode($request->sections) : null,
                            'employement_statuses' => isset($request->employement_statuses) ? json_encode($request->employement_statuses) : null,
                            'amount' => $request->amount,
                        ]);
                    }
                }
            }
            // Holiday Setting
            elseif($request->type == 'holiday_setting'){
                HolidaySetting::find($id)->update([
                    'departments' => isset($request->departments) ? json_encode($request->departments) : null,
                    'sections' => isset($request->sections) ? json_encode($request->sections) : null,
                    'employement_statuses' => isset($request->employement_statuses) ? json_encode($request->employement_statuses) : null,
                    'holidays' => isset($request->holidays) ? json_encode($request->holidays) : null,
                ]);
            }
            // Leave group Employee
            else{
                $this->validate($request , [
                    'employee' => 'required',
                    'leave_group' => 'required',
                ]);
                $leaveGroupEmployee = LeaveGroupEmployee::find($id);
                $leaveGroupEmployee->fill($request->all())->save();
            }
            return ['code' => '200' , 'message' => 'success'];
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        try{
            // Leave Types
            if($request->type == 'leave_type'){
                $leaveTypes = LeaveTypes::find($id);
                $leaveTypes->deleted_by = Auth::id();
                $leaveTypes->update();
                $leaveTypes->delete();
            }
            // Leave Periods
            elseif($request->type == 'leave_period'){
                $leavePeriods = LeavePeriods::find($id);
                $leavePeriods->deleted_by = Auth::id();
                $leavePeriods->update();
                $leavePeriods->delete();
            }
            // Work Week
            elseif($request->type == 'work_week'){
                $week = WorkWeek::find($id);
                $week->deleted_by = Auth::id();
                $week->update();
                $week->delete();
                DB::table('workdays')->where('work_week_id' , $id)->delete();
                return ['code'=>'200','message'=>'success'];
            }
            // Holidays
            elseif($request->type == 'holiday'){
                $holidays = Holidays::find($id);
                $holidays->deleted_by = Auth::id();
                $holidays->update();
                $holidays->delete();
            }
            // Leave Rules
            elseif($request->type == 'leave_rule'){
                $leaveRules = LeaveRules::find($id);
                $leaveRules->deleted_by = Auth::id();
                $leaveRules->update();
                $leaveRules->delete();
            }
            // Paid Time Off
            elseif($request->type == 'paid_time_off'){
                $paidTimeOff = PaidTimeOff::find($id);
                $paidTimeOff->deleted_by = Auth::id();
                $paidTimeOff->update();
                $paidTimeOff->delete();
            }
            // Leave Group
            elseif($request->type == 'edit_leave_group'){
                $leaveGroups = LeaveGroup::find($id);
                $leaveGroups->deleted_by = Auth::id();
                $leaveGroups->update();
                $leaveGroups->delete();
            }
            elseif($request->type == 'special_working_day'){
                $leaveGroups = SpecialDay::find($id);
                $leaveGroups->deleted_by = Auth::id();
                $leaveGroups->update();
                $leaveGroups->delete();
            }
            elseif($request->type == 'leave_type_setting'){
                $leaveTypeSetting = LeaveTypeSetting::find($id);
                $leaveTypeSetting->delete();
            }
            elseif($request->type == 'holiday_setting'){
                $holidaySetting = HolidaySetting::find($id);
                $holidaySetting->delete();
            }
            // Leave Group Employee
            else{
                $leaveGroupEmployee = LeaveGroupEmployee::find($id);
                $leaveGroupEmployee->deleted_by = Auth::id();
                $leaveGroupEmployee->update();
                $leaveGroupEmployee->delete();
            }
            return ['code'=>'200','message'=>'success'];
        }catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }   
    }
}
