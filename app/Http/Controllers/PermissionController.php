<?php

namespace App\Http\Controllers;

use App\Models\PermissionTitle;
use DataTables;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\ValidationException;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::eloquent(Permission::query())->make(true);
        }
        $titles = PermissionTitle::select('id', 'name')->get();
        // dd($titles);
        return view('Admin.settings.roles&permission.permission', compact('titles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                // 'permission_titles_id' => 'required:permission_titles,id'
            ]);
            $permission = new Permission();
            $permission->name = $request->name;
            $permission->permission_titles_id = $request->permission_titles_id;
            $permission->save();
            return ['code' => '200', 'message' => 'Permission created successfully'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '200', 'errors' => $e->errors()];
            } else {
                return ['code' => '200', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function edit($id)
    {
        $permission = Permission::find($id);
        return response($permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'permission_titles_id' => 'required|exists:permission_titles,id',
            ]);
            $permission = Permission::findOrFail($id);
            $permission->name = $request->name;
            $permission->permission_titles_id = $request->permission_titles_id;
            $permission->save();
            return ['code' => '200', 'message' => 'Permission updated successfully'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '200', 'errors' => $e->errors()];
            } else {
                return ['code' => '200', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Permission::find($id)->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '200', 'error_message' => $e->getMessage()];
        }
    }
}