<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DataTables;
use App\Models\FineReversal;
use App\Models\PayrollData;
use App\Models\Payroll;
use Illuminate\Http\Request;

class FineReversalRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            $month = date("m", strtotime($request->monthFilter));
            $year = date("Y", strtotime($request->monthFilter));
            $query = Payroll::join('payrolldata','payroll.id','=', 'payrolldata.payroll')
            ->join('employees' , 'employees.id' , '=' ,  'payrolldata.employee')
            ->leftjoin('fine_reversal_requests','payrolldata.id','=','fine_reversal_requests.fine_payrolldata_id')
            ->whereIn('payrolldata.department_id' , login_user_departments())
            ->whereIn('payrolldata.payroll_item' , [83,68,53,50])
            ->whereYear('payroll.date_start',$year)
            ->whereMonth('payroll.date_start',$month)
            ->where('payroll.status','completed')
            ->where('payrolldata.amount','>',0)
            ->select('employees.id','employees.employee_id','employees.employee_code',
            'employees.first_name' , 'employees.middle_name' , 'employees.last_name','payroll.id as payroll_id',
             'payrolldata.department','fine_reversal_requests.status as fine_request',
             'payrolldata.id as fine_relation_id',
             'payrolldata.payroll_item',DB::raw('SUM(payrolldata.amount) AS total_fine'))->groupBy('employees.id');
            return DataTables::of($query)->filter(function ($query) use ($request) {
                if(!empty($request->departmentFilter)){
                    if($request->departmentFilter != 'all'){
                        $query->where('payrolldata.department_id', $request->departmentFilter);
                    }
                }
            },true)->addIndexColumn()->make(true);
        }
        return view('Admin.request&approvals.fineReversal.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'amount' =>  'required',
                'remarks' => 'required'
            ]);
            $FilepathToStore=NULL;
            if ($request->hasFile('attachment_file')) {
                $fileNameExt = $request->file('attachment_file')->getClientOriginalName();
                $fileName = pathinfo($fileNameExt, PATHINFO_FILENAME);
                $fileExt = $request->file('attachment_file')->getClientOriginalExtension();
                $fileNameToStore = $fileName . '_' . time() . '.' . $fileExt;
                $FilepathToStore = $request->file('attachment_file')->storeAs('News-Images_Attachments', $fileNameToStore);
                $FilepathToStore = "../storage/app/" . $FilepathToStore;
            }   
            $fine_exist = FineReversal::where('employee_id',$request->employee_id)->where('month',$request->month)->first();
            if(empty($fine_exist)){
                $fine = new FineReversal();
                $fine->employee_id = $request->employee_id;
                $fine->month = $request->month;
                $fine->amount = $request->amount;
                $fine->remarks = $request->remarks;
                $fine->status = 'Pending';
                $fine->attachment_file = $FilepathToStore;
                $fine->submitted_by = Auth::user()->id;
                $fine->fine_payrolldata_id = $request->payrolldata_id;
                $fine->fine_payroll_id = $request->payroll_id;
                $fine->save();
                return ['code' => '200' , 'message' => 'success'];
            }else{
                return ['code' => '500' , 'customMessage' => 'exist'];
            }
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fine = PayrollData::join('payrollcolumns','payrollcolumns.id','=','payrolldata.payroll_item')->where([['payrolldata.payroll',request()->payroll_id],['payrolldata.employee',$id]])->whereIn('payrolldata.payroll_item',[83,68,53,50])->get(['payrollcolumns.id','payrollcolumns.name','payrolldata.amount']);
        return response()->json($fine);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        FineReversal::find($id)->update([
            'amount'=> $request->amount,
            'status'=>'Approved',
            'status_changed_by'=> auth()->user()->id,
            'status_changed_at'=>date('Y-m-d H:i:s',time()),
        ]);
          return ['code' => '200' , 'message' => 'success'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
