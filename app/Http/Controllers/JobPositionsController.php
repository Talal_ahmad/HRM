<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\JobPositions;
use Illuminate\Support\Facades\DB; 
use Illuminate\Validation\ValidationException;

class JobPositionsController extends Controller
{
    public function __construct(){ 
        $this->middleware('auth'); 
    }
    
    public function index(Request $request) 
    {
        if(isset($request->department)){
            $query = JobPositions::join('jobtitles','jobtitle_employees.jobtitle','=','jobtitles.id')
                ->join('companystructures','jobtitle_employees.department_id','=','companystructures.id');
                if($request->department != 'all')
                {
                    $query->where('jobtitle_employees.department_id',$request->department);
                }
                $JobPositions = $query->select('jobtitle_employees.*','jobtitles.name','companystructures.title')
                ->get()
                ->map(function($JobPositions) {
                    $job_title_employees = Employee::where([
                        ['job_title', $JobPositions->jobtitle],
                        ['department', $JobPositions->department_id],
                        ['status', 'Active'],
                        ['seasonal_status', 'On Roll'],
                    ])->count();
                    $JobPositions->remaining_persons = $JobPositions->num_of_persons-$job_title_employees;
                    $JobPositions->person_used = $job_title_employees;
                    return $JobPositions;
                });
        }
        $jobtitles = DB::table('jobtitles')->get();
        return view('Admin.setup.jobPositions.index', get_defined_vars());  
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        $jobtitles = DB::table('jobtitles')->get(); 
        return view('Admin.setup.jobPositions.add',get_defined_vars()); 
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    { 
        $this->validate($request , [
            'department_id' => 'required',
        ]);
        foreach ($request->jobtitles as $key => $job_title) {
            if (is_numeric($request->no_of_persons[$job_title])) {
                $job_position = new JobPositions();
                $job_position->jobtitle = $job_title;
                $job_position->department_id = $request->department_id;
                $job_position->num_of_persons = $request->no_of_persons[$job_title];
                $job_position->save();
            }
        }
        return redirect('job_positions')->with('success_message', 'Job Position has been Added Successfully!');
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id) 
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit(Request $request  , $id)
    {
        $job_position = JobPositions::find($id);
        return response()->json($job_position);
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'department_id' => 'required',
                'job_title' => 'required',
                'no_of_persons' => 'required',
            ]);

            $job_position = JobPositions::find($id);
            $job_position->jobtitle = $request->job_title;
            $job_position->department_id = $request->department_id;
            $job_position->num_of_persons = $request->no_of_persons;
            $job_position->update();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        try{
            JobPositions::find($id)->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
    
    public function get_department_employees(Request $request)
    {
        $data = DB::table('employees')->where('department', $request->department_id)->where('status', '=', 'Active')->select('id', 'employee_id', 'employee_code', 'first_name', 'last_name')->get();
        return response()->json($data);
    }
}
