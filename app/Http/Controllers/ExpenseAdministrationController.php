<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\EmployeeExpense;
use App\Models\PaymentMethod;
use App\Models\ExpenseCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException; 

class ExpenseAdministrationController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth'); 
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            // Expense Categories
            if($request->type == 'expenseCategories'){
                DB::statement(DB::raw('set @rownum=0'));
                $data = ExpenseCategory::select(['id', 'name',DB::raw('@rownum  := @rownum  + 1 AS rownum')]);
                return Datatables::eloquent($data)->make(true);
            }
            // Payment Methods
            elseif($request->type == 'payment_method'){
                DB::statement(DB::raw('set @rownum=0'));
                $data = PaymentMethod::select(['id', 'name',DB::raw('@rownum  := @rownum  + 1 AS rownum')]);
                return Datatables::eloquent($data)->make(true);
            }
            // Employee Expenses
            else{
                DB::statement(DB::raw('set @rownum=0'));
                $query = EmployeeExpense::leftjoin('employees','employeeexpenses.employee','=','employees.id')
                ->leftjoin('expensescategories','employeeexpenses.category','=','expensescategories.id')
                ->leftjoin('expensespaymentmethods','employeeexpenses.payment_method','=','expensespaymentmethods.id')
                ->leftjoin('currencytypes','employeeexpenses.currency','=','currencytypes.id')
                ->select('employeeexpenses.id','employeeexpenses.expense_date','employeeexpenses.payee','employeeexpenses.amount','employeeexpenses.status','expensescategories.name AS category_name','expensespaymentmethods.name AS paymentMethod_name','currencytypes.code','employees.employee_id','employees.employee_code','employees.first_name','employees.last_name',DB::raw('@rownum  := @rownum  + 1 AS rownum'));
                return DataTables::eloquent($query)->make(true);
            }
        } 
        $expenseCategory = ExpenseCategory::get();
        $paymentMethod =  PaymentMethod::get();
        $currencyTypes = DB::table('currencytypes')->get();
        return view('Admin.finance.expenseAdministration.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()  
    {
        //
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Expense Category
            if($request->type == 'expense_categories'){
                $this->validate($request, [
                    'name' => 'required',
                ]);

                $data = $request->all();
                ExpenseCategory::create($data);
            }
            // Payment Method
            elseif($request->type == 'payment_method'){
                $this->validate($request, [
                    'name' => 'required',
                    
                ]);

                $data = $request->all();
                PaymentMethod::create($data);
            }
            // Employee Expense
            else{
                $this->validate($request, [
                    'employee' => 'required',
                    'expense_date' => 'required',
                    'payment_method' => 'required',
                    'payee' => 'required',
                    'category' => 'required',
                    'notes' => 'required',
                    'currency' => 'required',
                    'amount' => 'required',
                ]);

                $data = $request->all();
                if(!empty($data['attachment1'])){
                    $data['attachment1'] = EmployeeExpense::InsertImage($data['attachment1']);
                }

                if(!empty($data['attachment2'])){
                    $data['attachment2'] = EmployeeExpense::InsertImage($data['attachment2']);
                }

                if(!empty($data['attachment3'])){
                    $data['attachment3'] = EmployeeExpense::InsertImage($data['attachment3']);
                }
                EmployeeExpense::create($data);
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // Expense Category
        if($request->type == 'expenseCategories'){
            $expenseCategories = ExpenseCategory::find($id);
            return response()->json($expenseCategories);
        }
        // Payment Method
        elseif($request->type == 'payment_method'){
            $payment_method = PaymentMethod::find($id);
            return response()->json($payment_method);
        }
        // Employee Expense
        else{
            $employeeExpenses = EmployeeExpense::find($id);
            return response()->json($employeeExpenses);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            // Expense Category
            if($request->type == 'expenseCategories'){
                $this->validate($request, [
                    'name' => 'required',
                ]);

                $expenseCategories = ExpenseCategory::find($id);
                $expenseCategories->fill($request->all())->save();
            }
            // Payment Method
            elseif($request->type == 'payment_method'){
                $this->validate($request, [
                    'name' => 'required',
                ]);
                $paymentMethods = PaymentMethod::find($id);
                $paymentMethods->fill($request->all())->save();
            }
            // Employee Expense
            else{
                $this->validate($request, [
                    'employee' => 'required',
                    'expense_date' => 'required',
                    'payment_method' => 'required',
                    'payee' => 'required',
                    'category' => 'required',
                    'notes' => 'required',
                    'currency' => 'required',
                    'amount' => 'required',
                ]);

                $data = $request->all();
                $employeeExpenses = EmployeeExpense::find($id);
                if(!empty($data['attachment1'])){
                    if(!empty($employeeExpenses->attachment1)){
                        unlink(public_path().$employeeExpenses->attachment1);
                    }
                    $data['attachment1'] = EmployeeExpense::InsertImage($data['attachment1']);
                }
                
                if(!empty($data['attachment2'])){
                    if(!empty($employeeExpenses->attachment2)){
                        unlink(public_path().$employeeExpenses->attachment2);
                    }
                    $data['attachment2'] = EmployeeExpense::InsertImage($data['attachment2']);
                }

                if(!empty($data['attachment3'])){
                    if(!empty($employeeExpenses->attachment3)){
                        unlink(public_path().$employeeExpenses->attachment3);
                    }
                    $data['attachment3'] = EmployeeExpense::InsertImage($data['attachment3']);
                }
                $employeeExpenses->fill($data)->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id) 
    {
        try{
            // Expense Category
            if($request->type == 'expenseCategories'){
                $employeeExpenses = EmployeeExpense::where('category', $id)->get();
                if(count($employeeExpenses) > 0)
                {
                    return ['code'=>'300', 'message'=>'This Category is Assigned to Some Expense Please change it first!'];
                }
                ExpenseCategory::find($id)->delete();
            }
            // Payment Method
            elseif($request->type == 'payment_method'){
                $employeeExpenses = EmployeeExpense::where('payment_method', $id)->get();
                if(count($employeeExpenses) > 0)
                {
                    return ['code'=>'300', 'message'=>'This Payment Method is Assigned to Some Expense Please change it first!'];
                }
                PaymentMethod::find($id)->delete();
            }
            // Employee Expense
            else{
                $employeeExpense = EmployeeExpense::find($id);
                $employeeExpense->deleted_by = Auth::user()->id;
                $employeeExpense->update();
                if(!empty($employeeExpense->attachment1)){
                    unlink(public_path().$employeeExpense->attachment1);
                }
                if(!empty($employeeExpense->attachment2)){
                    unlink(public_path().$employeeExpense->attachment2);
                }
                if(!empty($employeeExpense->attachment3)){
                    unlink(public_path().$employeeExpense->attachment3);
                }
                $employeeExpense->delete();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
