<?php

namespace App\Http\Controllers;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request) 
    {
        $departments = DB::table('companystructures')->get();
        $employees = DB::table('employees')->get();
        $payFrequency = DB::table('payfrequency')->get();
        $currency = DB::table('currencytypes')->get();
        $calculationGroup = DB::table('deductiongroup')->get();
        $calculation_Exemptions_Assigned = DB::table('deductions')->get(); 
        return view('Admin.finance.salarysetup.index', compact('departments','employees','payFrequency','currency','calculationGroup','calculation_Exemptions_Assigned')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
