<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\WorkWeek;
use App\Models\Employee;
use App\Models\ShiftType;
use Illuminate\Http\Request;
use App\Models\ShiftManagement;
use App\Models\CompanyStructure;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class BulkShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $work_week = WorkWeek::get();
        $shift_type = ShiftType::where('active',1)->get(); 
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        return view('Admin.timeManagement.bulkShiftManagement.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'departmentFilter' => 'required',
                'shift_id' => 'required',
                'work_week_id' => 'required',
                'shift_from_date' => 'required',
                'shift_to_date' => 'required',
                'late_time_allowed' => 'required',
            ]);
            $data = $request->all();
            if(!empty($request->departmentFilter)){
                if(env('COMPANY') == 'JSML'){
                    $employees = Employee::whereIn('department' , $request->section);
                }else{
                    $employees = Employee::where('department' , $request->departmentFilter);
                }
                if(!empty($request->employeeFilter)){
                    $employees = $employees->whereIn('id' , $request->employeeFilter);
                }
                $employees = $employees->get();
                foreach($employees as $employee){
                    $data['employee_id'] = $employee->id;
                if(env('COMPANY') == 'JSML'){
                    $data['status'] = 'Approved';
                }
                    $data['created_by'] = Auth::id();
                    ShiftManagement::create($data);
                }
            }
            if(!empty($request->departmentFilter=='all')){
                // $employees = Employee::where('department' , $request->departmentFilter);
                if(!empty($request->employeeFilter)){
                    $employees = Employee::whereIn('id' , $request->employeeFilter);
                    $employees = $employees->get();
                }else{
                    $employees = Employee::get();

                }
                foreach($employees as $employee){
                    $data['employee_id'] = $employee->id;
                    $data['created_by'] = Auth::id();
                    ShiftManagement::create($data);
                }
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
