<?php

namespace App\Http\Controllers;

use App\Models\Payroll;
use App\Models\LoanRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentWiseSalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        $year = !empty(request()->date) ? date('Y' , strtotime(request()->date)) : date('Y');
        $month = !empty(request()->date) ? date('m' , strtotime(request()->date)) : date('m');
        
        //For Department Wise Summary 
        $department_payrolls = Payroll::where(DB::raw('YEAR(date_start)') , $year)
        ->where(DB::raw('MONTH(date_start)') , $month)
        ->where('status' , 'Completed')
        ->whereIn('department' , login_user_departments())
        ->select('id' , 'name' , 'department');
        if(!empty(request()->departmentFilter) && request()->departmentFilter !== 'all'){
            $department_payrolls = $department_payrolls->where('department' , request()->departmentFilter);   
        }
        $department_payrolls = $department_payrolls->get();
        $payroll_data = [];
        foreach($department_payrolls as $department_payroll){
            $payroll = DB::table('payrolldata')->leftjoin('payroll' , 'payroll.id' , '=' , 'payrolldata.payroll')
            ->leftjoin('employees' , 'employees.id' , '=' , 'payrolldata.employee')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'payroll.department')
            ->where('payrolldata.payroll' , $department_payroll->id)
            ->select(DB::raw('SUM(CASE WHEN payrolldata.payroll_item = 73 THEN payrolldata.amount END) AS loan_deduction') , DB::raw('SUM(CASE WHEN payrolldata.payroll_item = 82 THEN payrolldata.amount END) AS incentive') , DB::raw('SUM(CASE WHEN payrolldata.payroll_item = 37 THEN payrolldata.amount END) AS gross_amount') , DB::raw('SUM(CASE WHEN payrolldata.payroll_item = 16 THEN payrolldata.amount END) AS overtime_value') , DB::raw('SUM(CASE WHEN payrolldata.payroll_item = 81 THEN payrolldata.amount END) AS tax_amount') , DB::raw('SUM(CASE WHEN payrolldata.payroll_item = 74 THEN payrolldata.amount END) AS net_salary') , 'companystructures.title' , 'payroll.id AS payroll_id')
            ->first();

            $loan = LoanRequest::join('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
            ->where('employees.department' , $department_payroll->department)
            ->where('loan_requests.status' , 'Approved')
            ->whereYear('loan_requests.start_date' , $year)
            ->whereMonth('loan_requests.start_date' , $month)
            ->sum('loan_requests.amount');
            $payroll->loan_amount = $loan;
            array_push($payroll_data , $payroll);
        }

        // For Department Wise Summary History
        $department_history_payrolls = DB::table('payroll_history')->where(DB::raw('YEAR(date_start)') , $year)
        ->where(DB::raw('MONTH(date_start)') , $month)
        ->where('status' , 'Completed')
        ->whereIn('department' , login_user_departments())
        ->select('id' , 'name' , 'department');
        if(!empty(request()->departmentFilter)){
            $department_history_payrolls = $department_history_payrolls->where('department' , request()->departmentFilter);   
        }
        $department_history_payrolls = $department_history_payrolls->get();

        $payroll_history_data = [];
        foreach($department_history_payrolls as $department_history_payroll){
            $payroll_history = DB::table('payrolldata_history')->leftjoin('payroll_history' , 'payroll_history.id' , '=' , 'payrolldata_history.payroll_history_id')
            ->leftjoin('employees' , 'employees.id' , '=' , 'payrolldata_history.employee')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'payroll_history.department')
            ->where('payrolldata_history.payroll_history_id' , $department_history_payroll->id)
            ->select(DB::raw('SUM(CASE WHEN payrolldata_history.payroll_item = 73 THEN payrolldata_history.amount END) AS loan_deduction') , DB::raw('SUM(CASE WHEN payrolldata_history.payroll_item = 82 THEN payrolldata_history.amount END) AS incentive') , DB::raw('SUM(CASE WHEN payrolldata_history.payroll_item = 37 THEN payrolldata_history.amount END) AS gross_amount') , DB::raw('SUM(CASE WHEN payrolldata_history.payroll_item = 16 THEN payrolldata_history.amount END) AS overtime_value') , DB::raw('SUM(CASE WHEN payrolldata_history.payroll_item = 81 THEN payrolldata_history.amount END) AS tax_amount') , DB::raw('SUM(CASE WHEN payrolldata_history.payroll_item = 74 THEN payrolldata_history.amount END) AS net_salary') , 'companystructures.title' , 'payroll_history.id AS payroll_history_id')
            ->first();

            $loan = LoanRequest::join('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
            ->where('employees.department' , $department_history_payroll->department)
            ->where('loan_requests.status' , 'Approved')
            ->whereYear('loan_requests.start_date' , $year)
            ->whereMonth('loan_requests.start_date' , $month)
            ->sum('loan_requests.amount');
            $payroll_history->loan_amount = $loan;
            array_push($payroll_history_data , $payroll_history);
        }
        return view('Admin.finance.department_wise_salary.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
