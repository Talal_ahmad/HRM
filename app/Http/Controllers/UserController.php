<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\CalculationGroup;
use App\Models\CompanyStructure;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            $users = User::get();
            foreach($users as $key => $user){
                $roles = $user->getRoleNames()->toArray();
                $user->role_name = !empty($roles) ? implode(",", $roles) : "";
            }
            return DataTables::of($users)->make(true);
        }
        $roles = Role::get();
        $departments = CompanyStructure::get();
        $calculationGroups = CalculationGroup::get();
        return view('Admin.settings.users.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name' =>  'required|min:3|max:100|unique:users,username',
                'email' => 'required|email|unique:users',
                'password' => 'required_with:cpassword|same:cpassword',
                'cpassword' => 'required',
                'department_name' => 'required',
                'role_name' => 'required',
            ]);
            $user = new User();
            $user->username = $request->name;
            $user->email = $request->email;
            $user->employee = $request->employee;
            $user->password = Hash::make($request->password);
            $user->user_departments = json_encode($request->department_name);
            $user->user_calculation_groups = !empty($request->calculationGroups_name) ? json_encode($request->calculationGroups_name) : NULL;
            $user->user_level = 'Admin';
            $user->mac_address = $request->mac_address;
            if($request->allow_mac==1){
                $user->allow_mac = 1;
            }else{
                $user->allow_mac = 0;
            }
            $user->assignRole($request->role_name);
            $user->save();
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $role = DB::table('model_has_roles')->where('model_id' , $id)->pluck('role_id');
        return response([
            'user' => $user,
            'role' => $role,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request , [
                'name' =>  'required|min:3|unique:users,username' . ($id ? ",$id" : ''),
                'email' => 'required|email|unique:users,email,'.$id,
                'password' => 'required_with:cpassword|same:cpassword',
                'department_name' => 'required',
                'role_name' => 'required',
            ]);
            $user = User::find($id);
            $user->username = $request->name;
            $user->email = $request->email;
            $user->employee = $request->employee;
            $user->user_departments = json_encode($request->department_name);
            $user->user_calculation_groups = !empty($request->calculationGroups_name) ? json_encode($request->calculationGroups_name) : NULL;
            $user->syncRoles($request->role_name);
            $user->mac_address = $request->mac_address;
            $user->allow_mac = $request->allow_mac;
            if($request->password){
                $user->password = Hash::make($request->password);
            }
            $user->save();
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            } 
        }
    }
    public function status(Request $request, $id)
    {
        $driver = User::find($id);
        $driver->status = $request->status;
        $driver->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
