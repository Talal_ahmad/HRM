<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\MiscellaneousRequest;
use App\Models\MiscellaneousRequestType;
use Illuminate\Validation\ValidationException;

class MiscellaneousRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            $query = MiscellaneousRequest::leftjoin('employees' , 'employees.id' , '=' , 'miscellaneous.employee_id')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->leftjoin('miscellaneous_request_type' , 'miscellaneous_request_type.id' , '=' , 'miscellaneous.request_type')
            ->whereIn('employees.department' , login_user_departments())
            ->where('employees.status' , 'Active')
            ->select('miscellaneous.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.employee_code' , 'companystructures.title','miscellaneous_request_type.name as request_type');
            if (userRoles()=="Self Service") {
                $query->where('miscellaneous.employee_id' , Auth::user()->employee);
            }
            return DataTables::of($query)->filter(function ($query) use ($request) {
                if(!empty($request->departmentFilter)){
                    $query->where('employees.department', $request->departmentFilter);
                }
                if (!empty($request->employeeFilter)) {
                    $query->where('employees.id', $request->employeeFilter);
                }
                if(!empty($request->fromDateFilter) && !empty($request->toDateFilter)){
                    $query->whereBetween(DB::raw("DATE(miscellaneous.created_at)"), [$request->fromDateFilter , $request->toDateFilter]);
                }
                else{
                    $query->whereDate(DB::raw("DATE(miscellaneous.created_at)"), date('Y-m-d'));
                }
            },true)->addIndexColumn()->make(true);
        }
        $miscellaneous_request_types = MiscellaneousRequestType::get();
        return view('Admin.request&approvals.miscellaneous_request.index',get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'description' => 'required',
                'feedback' => 'required',
            ]);

            $data = $request->all();
            if(!empty($request->status)){
                $data['status'] = 'Approved';
                $data['status_changed_by'] = Auth::id();
                $data['status_changed_at'] = Carbon::now();
            }else{
                $data['status'] = 'Pending';
            }
            if($request->hasFile('image')){
                dd($request->image);
                $data['image'] = InsertFile($request->file('image'), '/images/miscellaneous_request/');
            }
            // if($request->hasFile('image')){
            //     $data['image'] = InsertFile($request->file('image'), '/images/miscellaneous_request/');
            // }
            $data['submitted_by'] = Auth::id();
            MiscellaneousRequest::create($data);
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MiscellaneousRequest  $miscellaneousRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MiscellaneousRequest  $miscellaneousRequest
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MiscellaneousRequest::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MiscellaneousRequest  $miscellaneousRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'description' => 'required',
                'feedback' => 'required',
            ]);
            $data = $request->all();
            $miscellaneous = MiscellaneousRequest::find($id);
            if($request->status == 'Approved'){
                $miscellaneous->status = 'Approved';
                $miscellaneous['status_changed_by'] = Auth::id();
                $miscellaneous['status_changed_at'] = Carbon::now();
            }else{
                $miscellaneous->status = 'Pending';
            }
            $miscellaneous->fill($data)->save();
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MiscellaneousRequest  $miscellaneousRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $miscellaneous = MiscellaneousRequest::find($id);
            $miscellaneous->deleted_by = Auth::id();
            $miscellaneous->update();
            $miscellaneous->delete();
            return ['code' => 200 , 'message' => 'success'];
        }catch(\Exception $e){
            return ['code' => 200 , 'error_message' => $e->getMessage()];
        }
    }
}
