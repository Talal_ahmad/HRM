<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalaryComponentHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        // $year = !empty(request()->dateFilter) ? date('Y' , strtotime(request()->dateFilter)) : date('Y');
        // $month = !empty(request()->dateFilter) ? date('m' , strtotime(request()->dateFilter)) : date('m');

        if(request()->ajax()){
            $employees = Employee::join('salary_component_history' , 'employees.id' , '=' , 'salary_component_history.employee_id')
            ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
            ->join('salarycomponent' , 'salarycomponent.id' , '=' , 'salary_component_history.component_id')
            ->whereYear('salary_component_history.created_at' , date('Y' , strtotime(request()->dateFilter)))
            ->whereMonth('salary_component_history.created_at' , date('m' , strtotime(request()->dateFilter)));
            if(request()->departmentFilter != 'all'){
                $employees->where('employees.department' , request()->departmentFilter);
            }
            if(!empty(request()->salaryFilter)){
                $employees->where('salary_component_history.component_id' , request()->salaryFilter);
            }
            $employees = $employees->whereIn('employees.department' , login_user_departments())
            ->select('salary_component_history.employee_id' , 'salary_component_history.component_id' , 'salary_component_history.old_amount' ,'salary_component_history.new_amount', 'salary_component_history.remarks', 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'companystructures.title as department' , 'employees.employee_id' , 'jobtitles.name AS designation' , 'employees.employee_code' , 'salarycomponent.name AS component_name');
            return Datatables::eloquent($employees)->addIndexColumn()->make(true);
        }

        $salary_components = DB::table('salarycomponent')->get(['id' , 'name']);
        return view('Admin.finance.salary_comp_history.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
