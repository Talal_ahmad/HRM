<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\LoanRequest;
use App\Models\PayrollEmployee;
use App\Models\PayrollColumn;
use App\Models\CalculationGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class BulkAdvanceApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $loan_requests = LoanRequest::join('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
        ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
        ->join('loan_types' , 'loan_types.id' , '=' , 'loan_requests.loan_type')
        ->join('users' , 'users.id' , '=' , 'loan_requests.submitted_by')
        ->whereIn('employees.department' , login_user_departments())
        ->where('loan_requests.is_bulk' , 1);
        if(!empty($request->departmentFilter)){
            $loan_requests->where('employees.department' , $request->departmentFilter);
        }
        if (!empty($request->employeeFilter)) {
            $loan_requests->where('employees.id' , $request->employeeFilter);
        }
        if(!empty($request->month)){
            $loan_requests->whereYear('loan_requests.start_date' , date('Y' , strtotime($request->month)))
            ->whereMonth('loan_requests.start_date' , date('m' , strtotime($request->month)));
        }
        $loan_requests = $loan_requests->select('loan_requests.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.employee_id as employeeId', 'employees.employee_code', 'companystructures.title','loan_types.name as loan_type_name' , 'jobtitles.name AS designation' , 'users.username')->get();

        $loan_types = DB::table('loan_types')->get();
        return view('Admin.request&approvals.bulkAdvanceApproval.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if(!empty(request()->month)){
        //     if(date('Y-m',strtotime(request()->month)) != date('Y-m')){
        //         return redirect()->back()->with('message', 'Only Current Month can be Selected!');
        //     }
        // }
        if(!empty(request()->employeeFilter)){
            $basic_salary = DB::table('payrollcolumns')->where('id' , request()->basic_salary)->first(['salary_components']);
            if(!empty($basic_salary->salary_components))
            {
                $comp_id = json_decode($basic_salary->salary_components);
                $employees = DB::table('employeesalary')->select('employees.id','employees.employee_id','employees.employee_code','employees.first_name','employees.middle_name','employees.last_name','companystructures.title AS department','jobtitles.name AS designation','employeesalary.amount')
                ->join('employees' , 'employees.id' , '=' , 'employeesalary.employee')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->whereIn('employeesalary.employee' , request()->employeeFilter)
                ->where('employeesalary.component' , $comp_id[0])
                ->where('employees.status' , 'Active')
                ->get();
            }
            $calculation_employees = PayrollEmployee::join('employees', 'employees.id', '=', 'payrollemployees.employee')
                    ->join('companystructures', 'companystructures.id', '=', 'employees.department')
                    ->where('payrollemployees.deduction_group', request()->calculation_group)
                    ->where('employees.status', 'Active')
                    ->get(['employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.last_name','companystructures.title']);
        }
        $user_groups = !empty(Auth::user()->user_calculation_groups) ? json_decode(Auth::user()->user_calculation_groups) : [];
        $loan_types = DB::table('loan_types')->get();
        $calculations_groups = CalculationGroup::whereIn('id', $user_groups)->get(['id', 'name']);
        $payroll_columns = PayrollColumn::get(['id', 'name']);
        return view('Admin.request&approvals.bulkAdvanceApproval.create' , get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'employees' => 'required',
                'amount' => 'required',
            ]);

            foreach($request->employees as $employee){
                $loan_request = new LoanRequest();
                $loan_request->employee_id = $employee;
                $loan_request->loan_type = $request->loan_type;
                $loan_request->is_bulk = 1;
                $loan_request->amount = $request->amount[$employee];
                $loan_request->time_duration = 1;
                $loan_request->monthly_installment = $request->amount[$employee];
                $loan_request->remaining_amount = $request->amount[$employee];
                $loan_request->status = 'Pending';
                $loan_request->submitted_by =  Auth::id();
                $loan_request->loan_no =  !empty(LoanRequest::max('id')) ? LoanRequest::max('id') : 1;
                $loan_request->start_date = $request->start_date;
                if(isset($request->calculation_group))
                {
                    $loan_request->deduction_group = $request->calculation_group;
                }
                $loan_request->save();
            }
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=> '422','errors' => $e->errors()];
            }
            else{
                return ['code'=> '500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employees = PayrollEmployee::join('employees', 'employees.id', '=', 'payrollemployees.employee')
        ->join('companystructures', 'companystructures.id', '=', 'employees.department')
        ->where('payrollemployees.deduction_group', $id)
        ->where('employees.status', 'Active')
        ->get(['employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.last_name','companystructures.title']);
        return response()->json($employees);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approve_loan(){
        try{
            $loans = request()->all();
            if(empty($loans)){
                return ['code' => '500' , 'message' => 'No Record Has been Checked!'];
            }
            foreach($loans as $loan){
                $loan_request = LoanRequest::find($loan);
                $loan_request->status = 'Approved';
                $loan_request->save();

                DB::table('loan_installments')->insert([
                    'loan_request_id' => $loan_request->id,
                    'month' => $loan_request->start_date,
                    'amount' => $loan_request->monthly_installment,
                    'status' => 'active',
                ]);
            }
        }catch(Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
