<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\WorkDay;
use App\Models\Holidays;
use App\Models\Attendance;
use App\Models\CompanyStructure;
use App\Models\ShiftManagement;
use App\Models\OverTimeManagement;
use App\Models\EmployeeLeaveRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Carbon;

class OverTimeManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){ 
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            $query = OverTimeManagement::join('employees' , 'employees.id' , '=' , 'overtime_management.employee_id')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' ,  'employees.department')
            ->leftjoin('shift_type' , 'shift_type.id' , '=' , 'overtime_management.shift_id')
            ->whereIn('employees.department' , login_user_departments())
            // ->where('employees.status' , 'Active')
            ->select('overtime_management.*' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'shift_type.shift_desc' , 'companystructures.title');
            if (userRoles()=="Self Service") {
                $query->where('overtime_management.employee_id' , Auth::user()->employee);
            }
            if (userRoles()!="Self Service") {
                if (env('COMPANY') != 'JSML') {
                    if(!empty($request->departmentFilter!=='all')){
                        $query->where('employees.department', $request->departmentFilter);
                    }
                }else{
                    if (!empty($request->section)) {
                        $query->whereIn('employees.department', $request->section);
                    }
                }
            }
            if (!empty($request->employeeFilter)) {
                $query->whereIn('employees.id', $request->employeeFilter);
            }
            if(!empty($request->statusFilter)){
                $query->where('overtime_management.status', $request->statusFilter);
            }
            if(!empty($request->fromDateFilter) && !empty($request->toDateFilter)){
                $query->whereBetween('overtime_management.overtime_date', [$request->fromDateFilter , $request->toDateFilter]);
            }
            else{
                $query->where('overtime_management.overtime_date' , date('Y-m-d'));
            }
            $totalOvertime = $query->sum('overtime_management.overtime');
            return DataTables::of($query)
            ->with(['totalOvertime' => $totalOvertime])
            ->addIndexColumn()
            ->make(true);
        }

        return view('Admin.request&approvals.overtime_management.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     foreach (request()->employee_id as $emp_id) {
    //         $shift = ShiftManagement::select('shift_management.id', 'shift_management.work_week_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'work_week.desc as work_week_name','employees.first_name' , 'employees.middle_name' , 'employees.last_name')
    //             ->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
    //             ->join('work_week', 'work_week.id', '=', 'shift_management.work_week_id')
    //             ->join('employees', 'employees.id', '=', 'shift_management.employee_id')
    //             ->where([
    //                 ['shift_management.employee_id', $emp_id],   
    //                 ['shift_management.shift_from_date', '<=', request()->overtime_date],
    //                 ['shift_management.shift_to_date', '>=', request()->overtime_date]
    //             ])->orderBy('shift_management.id', 'DESC')->first();
    //         if(!empty($shift))
    //         {
    //             $attendance = Attendance::where('employee', $emp_id)->whereDate('in_time', request()->overtime_date)->first();
    //             if(!empty($attendance))
    //             {
    //                 $in_time = $attendance->in_time;
    //                 $out_time = $attendance->out_time;
    //             }
    //             else{
    //                 $employee_leave = EmployeeLeaveRequest::where([
    //                     ['employee', $emp_id],
    //                     ['date_start', '<=', request()->overtime_date],
    //                     ['date_end', '>=', request()->overtime_date],
    //                     ['status', '=', 'Approved']
    //                 ])->first();

    //                 if(!empty($employee_leave))
    //                 {
    //                     $leave = 'Yes';
    //                 }
    //             }
    //             $work_week_days = WorkDay::where([
    //                 ['work_week_id', $shift->work_week_id],
    //                 ['status', '=', 'Non-working Day']
    //             ])->get();
    //             if(count($work_week_days) > 0)
    //             {
    //                 $current_day_name = date('l', strtotime(request()->overtime_date));
    //                 foreach($work_week_days as $week_day)
    //                 {
    //                     if($week_day->name == $current_day_name)
    //                     {
    //                         $is_holiday = 'Off Day';
    //                     }
    //                 }
    //             }

    //             $holiday = Holidays::where('dateh', request()->overtime_date)->first();
    //             if(!empty($holiday))
    //             {
    //                 if(!empty($is_holiday))
    //                 {
    //                     $is_holiday = $is_holiday.' and '.$holiday->name;
    //                 }
    //                 else{
    //                     $is_holiday = $holiday->name;
    //                 }
    //             }

    //             $data['is_holiday'] = isset($is_holiday) ? $is_holiday : '-';
    //             $data['in_time'] = isset($in_time) ? $in_time : '-';
    //             $data['out_time'] = isset($out_time) ? $out_time : '-';
    //             $data['leave'] = isset($leave) ? $leave : 'No';
    //             $data['shift_name'] = $shift->shift_desc;
    //             $data['work_week'] = $shift->work_week_name;
    //             $data['shift_start_time'] = $shift->shift_start_time;
    //             $data['shift_end_time'] = $shift->shift_end_time;
    //             $data['name'] = $shift->first_name.' '.$shift->middle_name.' '.$shift->last_name;
    //             return response()->json($data);
    //         }
    //         else{
    //             return ['code' => 500,'message'=>'No Shift Assigned Please first assign shift!'];
    //         }
    //     }

    // }
    public function create()
    {
        $data = [];
        $allShiftsAssigned = true;
        $employeesWithNoShift = [];
        foreach (request()->employee_id as $emp_id) {
            $shift = ShiftManagement::select('shift_management.id', 'shift_management.work_week_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'work_week.desc as work_week_name', 'employees.first_name', 'employees.middle_name', 'employees.last_name')
                ->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
                ->join('work_week', 'work_week.id', '=', 'shift_management.work_week_id')
                ->join('employees', 'employees.id', '=', 'shift_management.employee_id')
                ->where([
                    ['shift_management.employee_id', $emp_id],
                    ['shift_management.shift_from_date', '<=', request()->overtime_date],
                    ['shift_management.shift_to_date', '>=', request()->overtime_date]
                ])->orderBy('shift_management.id', 'DESC')->first();
            if (!empty($shift)) {
                // Shift is assigned, continue processing
                $attendance = Attendance::where('employee', $emp_id)->whereDate('in_time', request()->overtime_date)->first();
                $in_time = '-';
                $out_time = '-';
                if (!empty($attendance)) {
                    $in_time = $attendance->in_time;
                    $out_time = $attendance->out_time;
                } else {
                    $employee_leave = EmployeeLeaveRequest::where([
                        ['employee', $emp_id],
                        ['date_start', '<=', request()->overtime_date],
                        ['date_end', '>=', request()->overtime_date],
                        ['status', '=', 'Approved']
                    ])->first();

                    if (!empty($employee_leave)) {
                        $leave = 'Yes';
                    } else {
                        $leave = 'No';
                    }
                }
                $work_week_days = WorkDay::where([
                    ['work_week_id', $shift->work_week_id],
                    ['status', '=', 'Non-working Day']
                ])->get();
                $is_holiday = '-';
                if (count($work_week_days) > 0) {
                    $current_day_name = date('l', strtotime(request()->overtime_date));
                    foreach ($work_week_days as $week_day) {
                        if ($week_day->name == $current_day_name) {
                            $is_holiday = 'Off Day';
                            break; // No need to continue checking
                        }
                    }
                }

                $holiday = Holidays::where('dateh', request()->overtime_date)->first();
                if (!empty($holiday)) {
                    if ($is_holiday != '-') {
                        $is_holiday .= ' and ' . $holiday->name;
                    } else {
                        $is_holiday = $holiday->name;
                    }
                }

                $data[] = [
                    'is_holiday' => isset($is_holiday) ? $is_holiday : '-',
                    'in_time' => isset($in_time) ? $in_time : '-',
                    'out_time' => isset($out_time) ? $out_time : '-',
                    'leave' => isset($leave) ? $leave : 'No',
                    'shift_name' => $shift->shift_desc,
                    'work_week' => $shift->work_week_name,
                    'shift_start_time' => $shift->shift_start_time,
                    'shift_end_time' => $shift->shift_end_time,
                    'name' => $shift->first_name . ' ' . $shift->middle_name . ' ' . $shift->last_name
                ];
            } else {
                // Shift is not assigned for at least one employee
                $allShiftsAssigned = false;
                $employeesWithNoShift[] = $emp_id;
            }
        }

        if ($allShiftsAssigned) {
            return response()->json($data);
        } else {
            $errorMessage = 'No Shift Assigned for employee ID: ' . implode(', ', $employeesWithNoShift);
            return ['code' => 500, 'message' => $errorMessage];
            // return ['code' => 500, 'message' => 'No Shift Assigned Please first assign shift!'];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     try{
    //         $this->validate($request , [
    //             'employee_id' => 'required',
    //             'overtime_date' => 'required',
    //             'start_datetime' => 'required',
    //             'end_datetime' => 'required|after:start_datetime',
    //             'remarks' => 'required',
    //         ]);
    //         foreach ($request->employee_id as $emp_id) {       
    //             $overtime_dup = OverTimeManagement::where('employee_id', $emp_id)->orderBy('id', 'desc')->whereDate('overtime_date', $request->overtime_date)->first();
    //             $shift = ShiftManagement::select('shift_management.id', 'shift_management.work_week_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'work_week.desc as work_week_name', 'shift_management.shift_id')
    //             ->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
    //             ->join('work_week', 'work_week.id', '=', 'shift_management.work_week_id')
    //             ->where([
    //                 ['shift_management.employee_id', $emp_id],   
    //                 ['shift_management.shift_from_date', '<=', $request->overtime_date],
    //                 ['shift_management.shift_to_date', '>=', $request->overtime_date]
    //             ])->orderBy('shift_management.id', 'DESC')->first();
    //             // if(!empty($shift))
    //             // {
    //             //     // Check Off Days And Holiday
    //             //     $is_holiday = '';
    //             //     $work_week_days = WorkDay::where([
    //             //         ['work_week_id', $shift->work_week_id],
    //             //         ['status', '=', 'Non-working Day']
    //             //     ])->get();
    //             //     if(count($work_week_days) > 0)
    //             //     {
    //             //         $current_day_name = date('l', strtotime($request->overtime_date));
    //             //         foreach($work_week_days as $week_day)
    //             //         {
    //             //             if($week_day->name == $current_day_name)
    //             //             {
    //             //                 $is_holiday = 'Off Day';
    //             //             }
    //             //         }
    //             //     }
        
    //             //     $holiday = Holidays::where('dateh', $request->overtime_date)->first();
    //             //     if(!empty($holiday))
    //             //     {
    //             //         $is_holiday = 'NH';
    //             //     }
    //             //     $overtime_date = $request->overtime_date;
    //                 $start_datetime = date('Y-m-d H:i:s', strtotime($request->start_datetime));
    //                 $end_datetime = date('Y-m-d H:i:s', strtotime($request->end_datetime));
    //             //     $shift_start_datetime = $overtime_date.' '.$shift->shift_start_time;
    //             //     $shift_end_datetime = $overtime_date.' '.$shift->shift_end_time;
    //             //     if($shift_end_datetime < $shift_start_datetime)
    //             //     {
    //             //         $overtime_date = date("Y-m-d", strtotime("+1 day", strtotime($overtime_date)));
    //             //     }
    //             //     $shift_end_datetime = $overtime_date.' '.$shift->shift_end_time;

    //             //     // Advance Approval
    //             //     if($request->overtime_date > date('Y-m-d'))
    //             //     {
    //             //         if(empty($is_holiday))
    //             //         {
    //             //             if($start_datetime > $shift_start_datetime && $end_datetime < $shift_end_datetime || $start_datetime < $shift_start_datetime && $end_datetime > $shift_start_datetime || $start_datetime < $shift_end_datetime && $end_datetime > $shift_end_datetime)
    //             //             {
    //             //                 return ['code' => 300, 'message' => 'Selected Overtime is not allowed because this time is in-between employee shift-time!'];
    //             //             }
    //             //         }
    //             //     }

    //             //     // Check Attendance And Leave
    //             //     $attendance = Attendance::where('employee', $request->employee_id)->whereDate('in_time', $request->overtime_date)->first();
    //             //     $employee_leave = EmployeeLeaveRequest::where([
    //             //         ['employee', $request->employee_id],
    //             //         ['date_start', '<=', $request->overtime_date],
    //             //         ['date_end', '>=', $request->overtime_date],
    //             //         ['status', '=', 'Approved']
    //             //     ])->first();

    //             //     $leave = '';
    //             //     if(!empty($employee_leave))
    //             //     {
    //             //         $leave = 'Yes';
    //             //     }

    //             //     if(empty($attendance) && !empty($leave))
    //             //     {
    //             //         return ['code' => 300, 'message' => 'Overtime not allowed because employee is on leave on this date!'];
    //             //     }

    //             //     if(empty($attendance) && empty($leave))
    //             //     {
    //             //         return ['code' => 300, 'message' => 'Overtime not allowed because employee is absent on this date!'];
    //             //     }

    //             //     // Post Approval
    //             //     if($request->overtime_date < date('Y-m-d'))
    //             //     {
    //             //         if(empty($is_holiday))
    //             //         {
    //             //             if($start_datetime > $shift_start_datetime && $end_datetime < $shift_end_datetime || $start_datetime < $shift_start_datetime && $end_datetime > $shift_start_datetime || $start_datetime < $shift_end_datetime && $end_datetime > $shift_end_datetime)
    //             //             {
    //             //                 return ['code' => 300, 'message' => 'Selected Overtime is not allowed because this time is in-between employee shift-time!'];
    //             //             }
    //             //         }

    //             //         if(isset($attendance) && !empty($attendance->out_time))
    //             //         {
    //             //             if($end_datetime > $attendance->out_time)
    //             //             {
    //             //                 return ['code' => 300, 'message' => 'Selected Overtime cannot be greater than your checkout time!'];
    //             //             }
    //             //         }
    //             //         else{
    //             //             return ['code' => 300, 'message' => 'You cannot request overtime Unless you have checked out!'];
    //             //         }
    //             //     }
    //             //     else{
    //             //         if(empty($is_holiday))
    //             //         {
    //             //             if($start_datetime > $shift_start_datetime && $end_datetime < $shift_end_datetime || $start_datetime < $shift_start_datetime && $end_datetime > $shift_start_datetime || $start_datetime < $shift_end_datetime && $end_datetime > $shift_end_datetime)
    //             //             {
    //             //                 return ['code' => 300, 'message' => 'Selected Overtime is not allowed because this time is in-between employee shift-time!'];
    //             //             }
    //             //         }
    //             //     }

    //             //     // Calculate hours
    //                 $start_overtime = date_create($start_datetime);
    //                 $end_overtime = date_create($end_datetime);
    //                 $diff = date_diff($start_overtime, $end_overtime);
    //                 $days = $diff->d;
    //                 $sec1 = $days * 24 * 3600;
    //                 $hours = $diff->h;
    //                 $sec2 = $hours * 3600;
    //                 $minutes = $diff->i;
    //                 $sec3 = $minutes * 60;
    //                 $total_sec = $sec1 + $sec2 + $sec3;
    //                 $overtime_hours = $total_sec / 3600;

    //             //     if($overtime_hours > 48)
    //             //     {
    //             //         return ['code' => 300, 'message' => 'Overtime cannot be greater than 48 hours!'];
    //             //     }

    //                 if($request->status == 'Approved')
    //                 {
    //                     $approved = 1;
    //                     $status = 'Approved';
    //                     $status_changed_by = Auth::id();
    //                     $status_changed_at = Carbon::now();
    //                 }
    //                 else{
    //                     $approved = 0;
    //                     $status = 'Pending';
    //                     $status_changed_by = Null;
    //                     $status_changed_at = Null;
    //                 }
    //             // }
    //             if(isset($overtime_dup) && $overtime_dup->status=='Approved'){
    //                 return ['code' => 300, 'message' => 'The overtime for this employee is already exist!'];
    //             }elseif (isset($overtime_dup) && $overtime_dup->status=='Pending') {
    //                 return ['code' => 300, 'message' => 'The overtime for this employee is already exist!'];
    //             }else{
    //                 $overtime = new OverTimeManagement();
    //                 $overtime->employee_id = $emp_id;
    //                 $overtime->shift_id = $shift->shift_id;
    //                 $overtime->work_week_id = $shift->work_week_id;
    //                 $overtime->overtime_start_time_stamp = $start_datetime;
    //                 $overtime->overtime_end_time_stamp = $end_datetime;
    //                 $overtime->overtime_date = $request->overtime_date;
    //                 $overtime->overtime = $overtime_hours;
    //                 $overtime->approved = $approved;
    //                 if($request->hasFile('attachment')){
    //                     $file = $request->file('attachment');
    //                     $extension = $file->getClientOriginalExtension();
    //                     $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
    //                     $file->move('images/overtime_request/' , $temp_name);
    //                     $overtime->attachment = $temp_name;
    //                 }
    //                 $overtime->status = $status;
    //                 $overtime->status_changed_by = $status_changed_by;
    //                 $overtime->status_changed_at = $status_changed_at;
    //                 $overtime->remarks = $request->remarks;
    //                 $overtime->submitted_by = Auth::id();
    //                 $overtime->save();
    //                 return ['code' => 200 , 'message' => 'success'];
    //             }
    //         }
    //     }
    //     catch(\Exception | ValidationException $e){
    //         if($e instanceof ValidationException){
    //             return ['code' => 422 , 'errors' => $e->errors()];
    //         }else{
    //             return ['code' => 500 , 'error_message' => $e->getMessage()];
    //         }
    //     }
    // }
    public function store(Request $request)
    {
        try {
            $this->validate($request , [
                'employee_id' => 'required|array',
                'employee_id.*' => 'required',
                'overtime_date' => 'required',
                'start_datetime' => 'required',
                'end_datetime' => 'required|after:start_datetime',
                'remarks' => 'required',
            ]);

            foreach ($request->employee_id as $emp_id) {
                // $overtime_dup = OverTimeManagement::where('employee_id', $emp_id)
                //     ->orderBy('id', 'desc')
                //     ->whereDate('overtime_date', $request->overtime_date)
                //     ->first();
                $overtime_dup = OverTimeManagement::where('employee_id', $emp_id)
                    ->where('overtime_start_time_stamp', $request->start_datetime)
                    ->orderBy('id', 'desc')
                    ->first();
                if ($overtime_dup && in_array($overtime_dup->status, ['Approved', 'Pending'])) {
                    return ['code' => 300, 'message' => 'Overtime request already exists for employee ID '.$emp_id.'!'];
                    // return ['code' => 300, 'message' => 'The overtime for this employee is already exist!'];
                }

                $shift = ShiftManagement::select('shift_management.id', 'shift_management.work_week_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'work_week.desc as work_week_name', 'shift_management.shift_id')
                    ->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
                    ->join('work_week', 'work_week.id', '=', 'shift_management.work_week_id')
                    ->where([
                        ['shift_management.employee_id', $emp_id],   
                        ['shift_management.shift_from_date', '<=', $request->overtime_date],
                        ['shift_management.shift_to_date', '>=', $request->overtime_date]
                    ])->orderBy('shift_management.id', 'DESC')->first();

                $start_datetime = date('Y-m-d H:i:s', strtotime($request->start_datetime));
                $end_datetime = date('Y-m-d H:i:s', strtotime($request->end_datetime));

                $start_overtime = date_create($start_datetime);
                $end_overtime = date_create($end_datetime);
                $diff = date_diff($start_overtime, $end_overtime);
                $days = $diff->d;
                $sec1 = $days * 24 * 3600;
                $hours = $diff->h;
                $sec2 = $hours * 3600;
                $minutes = $diff->i;
                $sec3 = $minutes * 60;
                $total_sec = $sec1 + $sec2 + $sec3;
                $overtime_hours = $total_sec / 3600;

                $overtime = new OverTimeManagement();
                $overtime->employee_id = $emp_id;
                $overtime->shift_id = $shift->shift_id;
                $overtime->work_week_id = $shift->work_week_id;
                $overtime->overtime_start_time_stamp = $start_datetime;
                $overtime->overtime_end_time_stamp = $end_datetime;
                $overtime->overtime_date = $request->overtime_date;
                $overtime->overtime = $overtime_hours;
                $overtime->approved = $request->status == 'Approved' ? 1 : 0;
                $overtime->status = $request->status == 'Approved' ? 'Approved' : 'Pending';
                $overtime->status_changed_by = $request->status == 'Approved' ? Auth::id() : null;
                $overtime->status_changed_at = $request->status == 'Approved' ? Carbon::now() : null;
                $overtime->remarks = $request->remarks;
                $overtime->submitted_by = Auth::id();

                if ($request->hasFile('attachment')) {
                    $file = $request->file('attachment');
                    $extension = $file->getClientOriginalExtension();
                    $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                    $file->move('images/overtime_request/', $temp_name);
                    $overtime->attachment = $temp_name;
                }

                $overtime->save();
            }

            return ['code' => 200, 'message' => 'success'];
        } catch(\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => 422, 'errors' => $e->errors()];
            } else {
                return ['code' => 500, 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OverTimeManagement  $overTimeManagement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OverTimeManagement  $overTimeManagement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OverTimeManagement  $overTimeManagement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        OverTimeManagement::find($id)->update([
            'status'=>$request->status

        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OverTimeManagement  $overTimeManagement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $OverTimeManagement = OverTimeManagement::find($id);
            $OverTimeManagement->update([
                'deleted_by' => auth()->user()->id,
            ]);
            $OverTimeManagement->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
