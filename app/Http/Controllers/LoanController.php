<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\EmployeeCompanyLoan;
use Illuminate\Support\Facades\DB; 
use Illuminate\Validation\ValidationException;

class LoanController extends Controller
{
    public function __construct(){    
        $this->middleware('auth');
    }

    public function index(Request $request) 
    {
        if($request->ajax()){
            
                return Datatables::eloquent(EmployeeCompanyLoan::leftjoin('employees','employeecompanyloans.employee','=','employees.id')->leftjoin('loan_types','employeecompanyloans.loan','=','loan_types.id')->leftjoin('currencytypes','employeecompanyloans.currency','=','currencytypes.id')->where('employees.id','=',Auth::id())->select('loan_types.name as loan','employeecompanyloans.id','employeecompanyloans.start_date','employeecompanyloans.period_months','employeecompanyloans.amount','employeecompanyloans.status','currencytypes.code as currency'))->make(true);
            }

        return view('Admin.finance.loan.index');  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    { 
        //
    }
}
