<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Auth;
use DataTables;
use PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Session;
use Carbon;
use App\Models\OnBoarding;
use App\Models\Event;
use App\Models\CompanyStructure;
use App\Models\Employee;
use Illuminate\Validation\ValidationException;
class OnBoardingController extends Controller
{
    public function cv_bank_list(Request $request)
    {   
        if($request->ajax())
        {   
            $query = OnBoarding::leftjoin('companystructures','companystructures.id','=','on_boarding.source')
                    ->leftjoin('users', 'users.id', '=', 'on_boarding.submitted_by')
                    ->select('on_boarding.*','companystructures.title','users.username');
            if(!Auth::user()->hasRole('Admin')){
                $query->where('submitted_by',Auth::user()->id);
            }
            return DataTables::of($query)->make(true); 
        }
        return view('Admin.on_boarding.index', get_defined_vars());
    }

    public function pdf_download($id)
    {   
        $boarding = DB::table('on_boarding')->find($id);
        // dd($boarding);
        if($boarding->source=='Online'){
            $boarding = DB::table('on_boarding')->where('on_boarding.id',$id)->latest('id')->first();
        }
        else{
            $boarding = DB::table('on_boarding')->join('companystructures','companystructures.id','=','on_boarding.source')
            ->join('users', 'users.id', '=', 'on_boarding.submitted_by')
            ->select('on_boarding.*','companystructures.title','users.username')->where('on_boarding.id',$id)->latest('id')->first(); 
        } 
        if(request()->export_type == "pdf"){
            // dd($boarding);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.on_boarding.application_pdf', get_defined_vars())->setPaper('4A0', 'portrait');
            return $pdf->download('Application.pdf');
        }
    }
    public function call_for_appointment(Request $request)
    {   
        try{
        $obj = OnBoarding::find($request->id);
        if($obj->status == 1){
            session::flash('exist','Appointment already exists!');
            return redirect()->back();
        }
        if (env('COMPANY') == 'CLINIX') {            
            $obj->status = 1;
            $date = date('Y-m-d', strtotime($request->adate));
            // dd($date);
            $obj->app_date = $date;
            $obj->app_time = $request->atime;
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->isSMTP(); 
            $mail->SMTPDebug = 0; // 0 = off (for production use) - 1 = client messages - 2 = client and server messages
            $mail->Host = 'clinix.com.pk'; // use $mail->Host = gethostbyname('smtp.gmail.com'); // if your network does not support SMTP over IPv6
            $mail->Port = 587; // TLS only
            $mail->SMTPSecure = 'tls'; // ssl is depracated
            $mail->SMTPAuth = true;
            $mail->Username = "alliance@clinix.com.pk";
            $mail->Password = "absbls1234";
            $mail->setFrom('alliance@clinix.com.pk', 'Clinix');
            $mail->addAddress($obj->email);
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Call for Interview!';
            $mail->Body    = '
            <head>
                <style type="text/css">
                    body {
                        margin: 0;
                        padding: 0;
                        min-width: 100% !important;
                    }
                    p{
                        margin: 11px 0px;
                    }
                    .content {
                        width: 100%;
                        max-width: 900px;
                    }
                    .header {
                        padding: 26px 30px 20px 30px;
                    }
                    .innerpadding {
                        padding: 20px 30px 10px 30px;
                    }
                    .borderbottom {
                        border-bottom: 1px solid #f2eeed;
                    }
                    .h2,
                    .bodycopy {
                        color: #153643;
                        font-family: sans-serif;
                    }
                    .h2 {
                        padding: 12px 0 22px 0;
                        font-size: 27px;
                        line-height: 28px;
                        font-weight: bold;
                        text-align: center;
                    }
                    .bodycopy {
                        font-size: 15px;
                        line-height: 22px;
                    }
                    .bodycopy1 {
                        font-size: 15px;
                        line-height: 27px;
                    }
                    .footer {
                        padding: 20px 30px 15px 30px;
                        background-color: #222222;
                    }
                    .footercopy {
                        font-family: sans-serif;
                        font-size: 14px;
                        color: #c9c6c5;
                    }
                    .footercopy a {
                        color: #ffffff;
                        text-decoration: underline;
                    }
                </style>
            </head>

            <body>
                <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="border: 1px groove rgba(0, 0, 0, .2);">
                                <tr>
                                    <td bgcolor="#0E1BBE" class="header">
                                        <table width="70" align="left" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding: 0 10px 10px 0;">
                                                    <img src="https://clinix.com.pk/logo.png" style="height: 41px; width: 195px;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="innerpadding borderbottom">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="h2">
                                                    Interview Schedule!
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 18px;padding-top: 10px">
                                                    <strong>Dear '.$obj->name . ',</strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="innerpadding borderbottom" style="padding-top: 7px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 850px;">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>We have reviewed your application for the designation of <strong>'.$obj->post .'</strong>.You have been shortlisted for the interview.</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>Following is the detail regarding your Interview:</p>
                                                                <p><strong>Date:</strong> '.$request->adate.'</p>
                                                                <p><strong>Time:</strong> '.date('h:i a', strtotime($request->atime)).'</p>
                                                                <p><strong>Venue:</strong> Head Office Clinix,Lahore</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>We wish you good luck for your Interview!</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p style="font-size: 17px;font-weight: bold;">Thanks & Best Regards,</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" >Azhar Hashmi</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" style="font-weight: 600;">Human Resource Manager</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" style="padding-bottom: 25px"><strong>Mobile:</strong> 0331-4339122</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="footer" bgcolor="#44525f">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center" class="footercopy">
                                                    Copyright &copy;2021 Clinix All Rights Reserved. Powered By Bright-line Solutions
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="padding: 20px 0 0 0;">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="https://www.facebook.com/Clinix.com.pk/">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci3.googleusercontent.com/proxy/fKRMRu5YHtOsX4g8QjulXayAWPG30Wqmxsp099ERcTznTvGDOyoGvnHKxjPvJ6UOpLlc-gNIpoYQUTH0-34TdgsDeYXyO-qJqQ8YuDUr3nqKRyXTnetzNgIhKG3JUva-YqJKbu_FxDj6skuPd_oxHBZ8w0jQAxCNA2BUIUHRi6P3vJqS=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/facebook@2x.png"
                                                                        alt="Facebook"
                                                                        title="Facebook"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="https://www.instagram.com/Clinix.com.pk/">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci3.googleusercontent.com/proxy/3Y1OfRcQFS-CU5SfUrdqV1i-H_3TWdDH0ATiTv9eb4mtzW9eFDr-yPM2aW_m7PKd5ypfHbm9Hrw6ZFheHYcb7b5Udn5qRF9MRt9SAsyf5AkjxEp_NaasZzgD-QOSolLrCaCTz1F2XDT5oRvLx8EvBwdz-WlkQNWP1LJ97TrR2zC2SPOS4g=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/instagram@2x.png"
                                                                        alt="Instagram"
                                                                        title="Instagram"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="https://twitter.com/clinix_pk">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci5.googleusercontent.com/proxy/FQX5qL7jcfdz_qAdNtRn8hjS-ZAVnV0Ou_EaOUxbN6dVErXW8GpF62I0lRauVpxgge1wEvULzs-oPMKPqRlaKWPRSmbukLrJTrBA6vu-HoGmUm3w_PUYW-SU8O_IzasjFYSDvD5Pob77dugFWdBiihoElv6Cy_gUVxCsV5VBDRSr3D0=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/twitter@2x.png"
                                                                        alt="Twitter"
                                                                        title="Twitter"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci4.googleusercontent.com/proxy/sEVHCI0qwDINTwNryZ8Yq2rc4kyRz8wbefesRVtJF5dYBB3eiGN_HoUWUW5pFzPsUcTE9-EtgvWiex341JJGp7Rq7R6oLQPwfhERDzuk2tQOg2jfmpk6aeZHO8r7_KYV2CzMa_o7UXPTR4ibvWHvcEyE_d3D-xdl4H7iob3vGf7Bijg=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/youtube@2x.png"
                                                                        alt="YouTube"
                                                                        title="YouTube"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </body>';
        }
        elseif(env('COMPANY') == 'HEALTHWISE') {
            $obj->status = 1;
            $date = date('Y-m-d', strtotime($request->adate));
            // dd($date);
            $obj->app_date = $date;
            $obj->app_time = $request->atime;
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->isSMTP(); 
            $mail->SMTPDebug = 0; // 0 = off (for production use) - 1 = client messages - 2 = client and server messages
            $mail->Host = 'clinix.com.pk'; // use $mail->Host = gethostbyname('smtp.gmail.com'); // if your network does not support SMTP over IPv6
            $mail->Port = 587; // TLS only
            $mail->SMTPSecure = 'tls'; // ssl is depracated
            $mail->SMTPAuth = true;
            $mail->Username = "alliance@clinix.com.pk";
            $mail->Password = "absbls1234";
            $mail->setFrom('alliance@clinix.com.pk', 'HEALTHWISE');
            $mail->addAddress($obj->email);
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Call for Interview!';
            $mail->Body    = '
            <head>
                <style type="text/css">
                    body {
                        margin: 0;
                        padding: 0;
                        min-width: 100% !important;
                    }
                    p{
                        margin: 11px 0px;
                    }
                    .content {
                        width: 100%;
                        max-width: 900px;
                    }
                    .header {
                        padding: 26px 30px 20px 30px;
                    }
                    .innerpadding {
                        padding: 20px 30px 10px 30px;
                    }
                    .borderbottom {
                        border-bottom: 1px solid #f2eeed;
                    }
                    .h2,
                    .bodycopy {
                        color: #153643;
                        font-family: sans-serif;
                    }
                    .h2 {
                        padding: 12px 0 22px 0;
                        font-size: 27px;
                        line-height: 28px;
                        font-weight: bold;
                        text-align: center;
                    }
                    .bodycopy {
                        font-size: 15px;
                        line-height: 22px;
                    }
                    .bodycopy1 {
                        font-size: 15px;
                        line-height: 27px;
                    }
                    .footer {
                        padding: 20px 30px 15px 30px;
                        background-color: #222222;
                    }
                    .footercopy {
                        font-family: sans-serif;
                        font-size: 14px;
                        color: #c9c6c5;
                    }
                    .footercopy a {
                        color: #ffffff;
                        text-decoration: underline;
                    }
                </style>
            </head>

            <body>
                <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="border: 1px groove rgba(0, 0, 0, .2);">
                                <tr>
                                    <td bgcolor="#0E1BBE" class="header">
                                        <table width="70" align="left" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding: 0 10px 10px 0;">
                                                    <h1>HEALTHWISE</h1>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="innerpadding borderbottom">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="h2">
                                                    Interview Schedule!
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 18px;padding-top: 10px">
                                                    <strong>Dear '.$obj->name . ',</strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="innerpadding borderbottom" style="padding-top: 7px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 850px;">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>We have reviewed your application for the designation of <strong>'.$obj->post .'</strong>.You have been shortlisted for the interview.</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>Following is the detail regarding your Interview:</p>
                                                                <p><strong>Date:</strong> '.$request->adate.'</p>
                                                                <p><strong>Time:</strong> '.date('h:i a', strtotime($request->atime)).'</p>
                                                                <p><strong>Venue:</strong> Head Office Clinix,Lahore</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>We wish you good luck for your Interview!</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p style="font-size: 17px;font-weight: bold;">Thanks & Best Regards,</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" >Hassan Javaid</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" style="font-weight: 600;">Director HR</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" style="padding-bottom: 25px"><strong>Mobile:</strong>+92 322 4817777</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="footer" bgcolor="#44525f">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center" class="footercopy">
                                                    Copyright All Rights Reserved. Powered By Bright-line Solutions
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="padding: 20px 0 0 0;">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci3.googleusercontent.com/proxy/fKRMRu5YHtOsX4g8QjulXayAWPG30Wqmxsp099ERcTznTvGDOyoGvnHKxjPvJ6UOpLlc-gNIpoYQUTH0-34TdgsDeYXyO-qJqQ8YuDUr3nqKRyXTnetzNgIhKG3JUva-YqJKbu_FxDj6skuPd_oxHBZ8w0jQAxCNA2BUIUHRi6P3vJqS=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/facebook@2x.png"
                                                                        alt="Facebook"
                                                                        title="Facebook"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci3.googleusercontent.com/proxy/3Y1OfRcQFS-CU5SfUrdqV1i-H_3TWdDH0ATiTv9eb4mtzW9eFDr-yPM2aW_m7PKd5ypfHbm9Hrw6ZFheHYcb7b5Udn5qRF9MRt9SAsyf5AkjxEp_NaasZzgD-QOSolLrCaCTz1F2XDT5oRvLx8EvBwdz-WlkQNWP1LJ97TrR2zC2SPOS4g=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/instagram@2x.png"
                                                                        alt="Instagram"
                                                                        title="Instagram"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci5.googleusercontent.com/proxy/FQX5qL7jcfdz_qAdNtRn8hjS-ZAVnV0Ou_EaOUxbN6dVErXW8GpF62I0lRauVpxgge1wEvULzs-oPMKPqRlaKWPRSmbukLrJTrBA6vu-HoGmUm3w_PUYW-SU8O_IzasjFYSDvD5Pob77dugFWdBiihoElv6Cy_gUVxCsV5VBDRSr3D0=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/twitter@2x.png"
                                                                        alt="Twitter"
                                                                        title="Twitter"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci4.googleusercontent.com/proxy/sEVHCI0qwDINTwNryZ8Yq2rc4kyRz8wbefesRVtJF5dYBB3eiGN_HoUWUW5pFzPsUcTE9-EtgvWiex341JJGp7Rq7R6oLQPwfhERDzuk2tQOg2jfmpk6aeZHO8r7_KYV2CzMa_o7UXPTR4ibvWHvcEyE_d3D-xdl4H7iob3vGf7Bijg=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/youtube@2x.png"
                                                                        alt="YouTube"
                                                                        title="YouTube"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </body>';
        }
        elseif(env('COMPANY') == 'UNICORN') {
            $obj->status = 1;
            $date = date('Y-m-d', strtotime($request->adate));
            // dd($date);
            $obj->app_date = $date;
            $obj->app_time = $request->atime;
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->isSMTP(); 
            $mail->SMTPDebug = 0; // 0 = off (for production use) - 1 = client messages - 2 = client and server messages
            $mail->Host = 'clinix.com.pk'; // use $mail->Host = gethostbyname('smtp.gmail.com'); // if your network does not support SMTP over IPv6
            $mail->Port = 587; // TLS only
            $mail->SMTPSecure = 'tls'; // ssl is depracated
            $mail->SMTPAuth = true;
            $mail->Username = "alliance@clinix.com.pk";
            $mail->Password = "absbls1234";
            $mail->setFrom('alliance@clinix.com.pk', 'UNICORN');
            $mail->addAddress($obj->email);
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Call for Interview!';
            $mail->Body    = '
            <head>
                <style type="text/css">
                    body {
                        margin: 0;
                        padding: 0;
                        min-width: 100% !important;
                    }
                    p{
                        margin: 11px 0px;
                    }
                    .content {
                        width: 100%;
                        max-width: 900px;
                    }
                    .header {
                        padding: 26px 30px 20px 30px;
                    }
                    .innerpadding {
                        padding: 20px 30px 10px 30px;
                    }
                    .borderbottom {
                        border-bottom: 1px solid #f2eeed;
                    }
                    .h2,
                    .bodycopy {
                        color: #153643;
                        font-family: sans-serif;
                    }
                    .h2 {
                        padding: 12px 0 22px 0;
                        font-size: 27px;
                        line-height: 28px;
                        font-weight: bold;
                        text-align: center;
                    }
                    .bodycopy {
                        font-size: 15px;
                        line-height: 22px;
                    }
                    .bodycopy1 {
                        font-size: 15px;
                        line-height: 27px;
                    }
                    .footer {
                        padding: 20px 30px 15px 30px;
                        background-color: #222222;
                    }
                    .footercopy {
                        font-family: sans-serif;
                        font-size: 14px;
                        color: #c9c6c5;
                    }
                    .footercopy a {
                        color: #ffffff;
                        text-decoration: underline;
                    }
                </style>
            </head>

            <body>
                <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="border: 1px groove rgba(0, 0, 0, .2);">
                                <tr>
                                    <td bgcolor="#0E1BBE" class="header">
                                        <table width="70" align="left" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding: 0 10px 10px 0;">
                                                    <h1>UNICORN</h1>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="innerpadding borderbottom">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="h2">
                                                    Interview Schedule!
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 18px;padding-top: 10px">
                                                    <strong>Dear '.$obj->name . ',</strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="innerpadding borderbottom" style="padding-top: 7px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 850px;">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>We have reviewed your application for the designation of <strong>'.$obj->post .'</strong>.You have been shortlisted for the interview.</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>Following is the detail regarding your Interview:</p>
                                                                <p><strong>Date:</strong> '.$request->adate.'</p>
                                                                <p><strong>Time:</strong> '.date('h:i a', strtotime($request->atime)).'</p>
                                                                <p><strong>Venue:</strong> Head Office Clinix,Lahore</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>We wish you good luck for your Interview!</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p style="font-size: 17px;font-weight: bold;">Thanks & Best Regards,</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" >Hassan Javaid</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" style="font-weight: 600;">Director HR</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" style="padding-bottom: 25px"><strong>Mobile:</strong>+92 322 4817777</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="footer" bgcolor="#44525f">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center" class="footercopy">
                                                    Copyright All Rights Reserved. Powered By Bright-line Solutions
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="padding: 20px 0 0 0;">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci3.googleusercontent.com/proxy/fKRMRu5YHtOsX4g8QjulXayAWPG30Wqmxsp099ERcTznTvGDOyoGvnHKxjPvJ6UOpLlc-gNIpoYQUTH0-34TdgsDeYXyO-qJqQ8YuDUr3nqKRyXTnetzNgIhKG3JUva-YqJKbu_FxDj6skuPd_oxHBZ8w0jQAxCNA2BUIUHRi6P3vJqS=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/facebook@2x.png"
                                                                        alt="Facebook"
                                                                        title="Facebook"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci3.googleusercontent.com/proxy/3Y1OfRcQFS-CU5SfUrdqV1i-H_3TWdDH0ATiTv9eb4mtzW9eFDr-yPM2aW_m7PKd5ypfHbm9Hrw6ZFheHYcb7b5Udn5qRF9MRt9SAsyf5AkjxEp_NaasZzgD-QOSolLrCaCTz1F2XDT5oRvLx8EvBwdz-WlkQNWP1LJ97TrR2zC2SPOS4g=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/instagram@2x.png"
                                                                        alt="Instagram"
                                                                        title="Instagram"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci5.googleusercontent.com/proxy/FQX5qL7jcfdz_qAdNtRn8hjS-ZAVnV0Ou_EaOUxbN6dVErXW8GpF62I0lRauVpxgge1wEvULzs-oPMKPqRlaKWPRSmbukLrJTrBA6vu-HoGmUm3w_PUYW-SU8O_IzasjFYSDvD5Pob77dugFWdBiihoElv6Cy_gUVxCsV5VBDRSr3D0=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/twitter@2x.png"
                                                                        alt="Twitter"
                                                                        title="Twitter"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci4.googleusercontent.com/proxy/sEVHCI0qwDINTwNryZ8Yq2rc4kyRz8wbefesRVtJF5dYBB3eiGN_HoUWUW5pFzPsUcTE9-EtgvWiex341JJGp7Rq7R6oLQPwfhERDzuk2tQOg2jfmpk6aeZHO8r7_KYV2CzMa_o7UXPTR4ibvWHvcEyE_d3D-xdl4H7iob3vGf7Bijg=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/youtube@2x.png"
                                                                        alt="YouTube"
                                                                        title="YouTube"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </body>';
        }
        else{
            $obj->status = 1;
            $date = date('Y-m-d', strtotime($request->adate));
            // dd($date);
            $obj->app_date = $date;
            $obj->app_time = $request->atime;
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->isSMTP(); 
            $mail->SMTPDebug = 0; // 0 = off (for production use) - 1 = client messages - 2 = client and server messages
            $mail->Host = 'clinix.com.pk'; // use $mail->Host = gethostbyname('smtp.gmail.com'); // if your network does not support SMTP over IPv6
            $mail->Port = 587; // TLS only
            $mail->SMTPSecure = 'tls'; // ssl is depracated
            $mail->SMTPAuth = true;
            $mail->Username = "alliance@clinix.com.pk";
            $mail->Password = "absbls1234";
            $mail->setFrom('alliance@clinix.com.pk', 'KITCHEN/OPUS');
            $mail->addAddress($obj->email);
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Call for Interview!';
            $mail->Body    = '
            <head>
                <style type="text/css">
                    body {
                        margin: 0;
                        padding: 0;
                        min-width: 100% !important;
                    }
                    p{
                        margin: 11px 0px;
                    }
                    .content {
                        width: 100%;
                        max-width: 900px;
                    }
                    .header {
                        padding: 26px 30px 20px 30px;
                    }
                    .innerpadding {
                        padding: 20px 30px 10px 30px;
                    }
                    .borderbottom {
                        border-bottom: 1px solid #f2eeed;
                    }
                    .h2,
                    .bodycopy {
                        color: #153643;
                        font-family: sans-serif;
                    }
                    .h2 {
                        padding: 12px 0 22px 0;
                        font-size: 27px;
                        line-height: 28px;
                        font-weight: bold;
                        text-align: center;
                    }
                    .bodycopy {
                        font-size: 15px;
                        line-height: 22px;
                    }
                    .bodycopy1 {
                        font-size: 15px;
                        line-height: 27px;
                    }
                    .footer {
                        padding: 20px 30px 15px 30px;
                        background-color: #222222;
                    }
                    .footercopy {
                        font-family: sans-serif;
                        font-size: 14px;
                        color: #c9c6c5;
                    }
                    .footercopy a {
                        color: #ffffff;
                        text-decoration: underline;
                    }
                </style>
            </head>

            <body>
                <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="border: 1px groove rgba(0, 0, 0, .2);">
                                <tr>
                                    <td bgcolor="#0E1BBE" class="header">
                                        <table width="70" align="left" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding: 0 10px 10px 0;">
                                                    <h1>KITCHEN/OPUS</h1>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="innerpadding borderbottom">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="h2">
                                                    Interview Schedule!
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 18px;padding-top: 10px">
                                                    <strong>Dear '.$obj->name . ',</strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="innerpadding borderbottom" style="padding-top: 7px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 850px;">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>We have reviewed your application for the designation of <strong>'.$obj->post .'</strong>.You have been shortlisted for the interview.</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>Following is the detail regarding your Interview:</p>
                                                                <p><strong>Date:</strong> '.$request->adate.'</p>
                                                                <p><strong>Time:</strong> '.date('h:i a', strtotime($request->atime)).'</p>
                                                                <p><strong>Venue:</strong> Head Office Clinix,Lahore</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy">
                                                                <p>We wish you good luck for your Interview!</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p style="font-size: 17px;font-weight: bold;">Thanks & Best Regards,</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" >Hassan Javaid</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" style="font-weight: 600;">Director HR</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodycopy1" style="padding-bottom: 25px"><strong>Mobile:</strong>+92 322 4817777</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="footer" bgcolor="#44525f">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center" class="footercopy">
                                                    Copyright All Rights Reserved. Powered By Bright-line Solutions
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="padding: 20px 0 0 0;">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci3.googleusercontent.com/proxy/fKRMRu5YHtOsX4g8QjulXayAWPG30Wqmxsp099ERcTznTvGDOyoGvnHKxjPvJ6UOpLlc-gNIpoYQUTH0-34TdgsDeYXyO-qJqQ8YuDUr3nqKRyXTnetzNgIhKG3JUva-YqJKbu_FxDj6skuPd_oxHBZ8w0jQAxCNA2BUIUHRi6P3vJqS=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/facebook@2x.png"
                                                                        alt="Facebook"
                                                                        title="Facebook"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci3.googleusercontent.com/proxy/3Y1OfRcQFS-CU5SfUrdqV1i-H_3TWdDH0ATiTv9eb4mtzW9eFDr-yPM2aW_m7PKd5ypfHbm9Hrw6ZFheHYcb7b5Udn5qRF9MRt9SAsyf5AkjxEp_NaasZzgD-QOSolLrCaCTz1F2XDT5oRvLx8EvBwdz-WlkQNWP1LJ97TrR2zC2SPOS4g=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/instagram@2x.png"
                                                                        alt="Instagram"
                                                                        title="Instagram"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci5.googleusercontent.com/proxy/FQX5qL7jcfdz_qAdNtRn8hjS-ZAVnV0Ou_EaOUxbN6dVErXW8GpF62I0lRauVpxgge1wEvULzs-oPMKPqRlaKWPRSmbukLrJTrBA6vu-HoGmUm3w_PUYW-SU8O_IzasjFYSDvD5Pob77dugFWdBiihoElv6Cy_gUVxCsV5VBDRSr3D0=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/twitter@2x.png"
                                                                        alt="Twitter"
                                                                        title="Twitter"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                            <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                                <a href="#">
                                                                    <img
                                                                        width="25"
                                                                        height="25"
                                                                        src="https://ci4.googleusercontent.com/proxy/sEVHCI0qwDINTwNryZ8Yq2rc4kyRz8wbefesRVtJF5dYBB3eiGN_HoUWUW5pFzPsUcTE9-EtgvWiex341JJGp7Rq7R6oLQPwfhERDzuk2tQOg2jfmpk6aeZHO8r7_KYV2CzMa_o7UXPTR4ibvWHvcEyE_d3D-xdl4H7iob3vGf7Bijg=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-dark-gray/youtube@2x.png"
                                                                        alt="YouTube"
                                                                        title="YouTube"
                                                                        style="text-decoration: none; height: auto; border: 0; display: block;"
                                                                        class="CToWUd"
                                                                    />
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </body>';
        }
        if($mail->send()){
            $obj->save();
        }
        return ['code'=>'200','message'=>'success'];

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }
    public function sch(Request $request)
    {
        if($request->ajax()) {
            
            $data = DB::select("SELECT `a`.`id`, `a`.`status`, `a`.`app_date` as `start`, CONCAT(COUNT(p.start),' Interviews') as title FROM `on_boarding` `a` LEFT JOIN `careers_appointments` `p` ON (`p`.`start` = a.app_date) WHERE `a`.`app_date` BETWEEN '$request->start' AND '$request->end' AND  `a`.`status` = '1' GROUP BY `a`.`app_date`");
            return response()->json($data);
       }
       return view('Admin.on_boarding.calendar');
    }
    
    public function appointment(Request $request)
    {
        $date = $request->date;
        // $appointments = OnBoarding::whereNotNull('status')->join('careers_appointments','careers_appointments.applicant_id','=','on_boarding.id')->get();
        // $appointments = OnBoarding::whereNotNull('status')->get();
        $appointments = OnBoarding::whereNotNull('on_boarding.status')->get(['on_boarding.*']);
        // dd($num_inter);
        // $add = DB::table('careers_appointments')
        // ->select('careers_appointments.*',DB::raw('COUNT(applicant_id) as count'))
        // ->groupBy($appointments)
        // ->orderBy('count')
        // ->get();
        return view('Admin.on_boarding.appointment',get_defined_vars());
    }

    public function quick_add(Request $request)
    {
        try{
            if (env('COMPANY') == 'HEALTHWISE') {
                $this->validate($request, [
                    'name' => 'required',
                    // 'f_name' => 'required',
                    'phone2' => 'required',
                    'post_name' => 'required',
                    // 'nic_num' => 'required',
                    'address2' => 'required',
                ]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                    'f_name' => 'required',
                    'phone2' => 'required',
                    'post_name' => 'required',
                    'nic_num' => 'required',
                    'address2' => 'required',
                ]);
            }
            $date = date('Y-m-d', strtotime($request->adate));
            if(isset($request->source) && !empty($request->source)){
                $source = $request->source;
            }
            DB::table('on_boarding')->insert([
                'submitted_by' => Auth::check() ? Auth::user()->id : 'Quick Add',
                'source' => $source,
                'post' => $request->post_name,
                'cnic' => $request->nic_num,
                'name' => $request->name,
                'f_name' => $request->f_name,
                'permanent_addr' => $request->address2,
                'phone_1st' => $request->phone2,
                'status' => 1,
                'app_date' =>$date,
                'app_time' => $request->atime,
            ]);
            return response()->json([
                'message' => 'Success',
                ], 201);
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }
    public function remarks(Request $request){
        $emp_id= new Event;
        // dd($request->stat_id);
        // $application= OnBoarding::find($request->stat_id);
        // dd($request->id);
        // $date = date('Y-m-d', strtotime($request->adate));
        $remarks = $request->remarks;
        if(!empty($remarks)){
            if(!empty($remarks == 'selected')){
                $application= OnBoarding::find($request->id);
                $application->app_remarks = 1;
                $application->save();
            }
            else if(!empty($remarks == 'rejected')){
                $emp_id->app_remarks = 2;
            }
            else if(!empty($remarks == 'waiting')){
                $emp_id->app_remarks = 3;
                if(!empty($request->adate)){
                    $emp_id->start = $date = date('Y-m-d', strtotime($request->adate));
                    $emp_id->app_date = $date = date('Y-m-d', strtotime($request->adate));
                }
                // $emp_id->app_date = $date;
                $emp_id->app_time = $request->atime;
            }
            else if(!empty($remarks == 'didnotappear')){
                $emp_id->app_remarks = 4;
            }
            $emp_id->status = 3;
            $emp_id->communication = $request->communication;
            $emp_id->cleanliness = $request->cleanliness;
            $emp_id->description = $request->description;
            $emp_id->applicant_id = $request->id;
            $emp_id->save();
            // dd($emp_id->save());
            // DB::commit();
        }
            // $date = date('Y-m-d', strtotime($request->adate));
            // dd(!empty($request->adate));
            // $obj->app_date = $date;
            // if(!empty($request->adate)){
            //     $obj2 = new Event;
            //         $obj2->applicant_id = $request->stat_id;
            //         $obj2->start = $date = date('Y-m-d', strtotime($request->adate));
            //         $obj2->save();
            // }
            session::flash('remarks','Application has been '.$remarks);
            return redirect()->back();
    }

    public function results(Request $request){
        // dd($request);
        $where = '';
        $where .= "WHERE `upload_time` IS NOT NULL";
        if(!empty($request->fd)){
            $fd = Carbon::parse($request->fd)->format('Y-m-d');
            $where .= " AND app_date >= '$fd'";
        }
        if(!empty($request->td)){
            $td = Carbon::parse($request->td)->format('Y-m-d');
            $where .= " AND app_date <= '$td'";
        }
        if(!empty($request->remarks)){
            $where .= " AND `app_remarks` = $request->remarks";
        }
        $applications = DB::select("SELECT * FROM on_boarding $where AND `app_remarks` = 1");

        // dd($applications);
        return view('admin.on_boarding.result',compact('applications'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(request()->export_type == "pdf"){
            $boarding = DB::table('on_boarding')->join('companystructures','companystructures.id','=','on_boarding.source')
            ->join('users', 'users.id', '=', 'on_boarding.submitted_by')
            ->select('on_boarding.*','companystructures.title','users.username')->latest('on_boarding.id')->first();
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.on_boarding.application_pdf', get_defined_vars())->setPaper('4A0', 'portrait');
            return $pdf->download('Application.pdf');
        }
        return view('Admin.on_boarding.cv', get_defined_vars());
    }
    public function cv_bank_online()
    {   
        $boarding = DB::table('on_boarding')->latest('id')->first();
        if(request()->export_type == "pdf"){
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.on_boarding.application_pdf', get_defined_vars())->setPaper('4A0', 'portrait');
            return $pdf->download('Application.pdf');
        }
        return view('Admin.on_boarding.cv_online', get_defined_vars());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if (env('COMPANY') == 'HEALTHWISE') {
                if(!empty($request->source) || !empty($request->login) ){
                    $this->validate($request, [
                        'agreement' => 'required',
                        'accept_salary' => 'required',
                        'source' => 'required',
                    ]);
                }else{
                    $this->validate($request, [
                        'agreement' => 'required',
                        'accept_salary' => 'required',
                    ]);
                }
            }else{
                if(!empty($request->source) || !empty($request->login)){
                    $this->validate($request, [
                        'agreement' => 'required',
                        'owns' => 'required',
                        'lang_known' => 'required',
                        'father_health_status' => 'required',
                        'mother_health_status' => 'required',
                        'agreement' => 'required',
                        'source' => 'required',
                    ]);
                }else{
                    $this->validate($request, [
                        'agreement' => 'required',
                        'owns' => 'required',
                        'lang_known' => 'required',
                        'father_health_status' => 'required',
                        'mother_health_status' => 'required',
                        'agreement' => 'required',
                    ]);
                }
            }
            $applicant_image = OnBoarding::InsertFile($request->file('applicant_image'),'/applicant_images');
            if(isset($request->source) && !empty($request->source)){
                $source = $request->source;
            }
            else{
                $source = 'Online';
            }
            if (!empty($request->owns)) {
               $owns = implode(',', $request->owns);
            }else{
                $owns = null;
            }
            if (!empty($request->lang_known)) {
                $lang_known = implode(',' ,$request->lang_known);
            }else{
                $lang_known = null;
            }
            if (!empty($request->father_health_status)) {
                $father_health_status = implode(',' ,$request->father_health_status);
            }else{
                $father_health_status = null;
            }
            if (!empty($request->mother_health_status)) {
                $mother_health_status = implode(',' ,$request->mother_health_status);
            }else{
                $mother_health_status = null;
            }
            DB::table('on_boarding')->insert([
                'submitted_by' => Auth::check() ? Auth::user()->id : 'Online',
                'source' => $source,
                'post' => $request->post_name,
                'name' => $request->name,
                'email' => $request->email,
                'doj' => $request->doj,
                'image' => $applicant_image,
                'f_name' => $request->f_name,
                'dob' => $request->birth_date,
                'blood' => $request->blood,
                'cnic' => $request->nic_num,
                'current_salary' => $request->current_salary,
                'religion' => $request->religion, 
                'marital_status' => $request->marital_status,
                'total_child' => $request->child,
                'kin_address' => $request->address1,
                'kin_phone' => $request->phone1,
                'residence_status' => $request->residence_status,
                'own_conveyance' => $owns,
                // 'status' => 'Active',
                'permanent_addr' => $request->address2,
                'phone_1st' => $request->phone2,
                'present_addr' => $request->address3,
                'phone_2nd' => $request->phone3,
                'higher' => $request->higher_education,
                'higher_college_school' => $request->higher_college_school,
                'higher_detail' => $request->higher_detail,
                'higher_year' => $request->higher_year,
                'higher_div' => $request->higher_div,
                'higher_grade' => $request->higher_grade,

                'graduation' => $request->graduation,
                'grad_college_school' => $request->grad_college_school,
                'grad_detail' => $request->grad_detail,
                'grad_year' => $request->grad_year,
                'grad_div' => $request->grad_div,
                'grad_grade' => $request->grad_grade,

                'inter' => $request->inter,
                'inter_college_school' => $request->inter_college_school,
                'inter_detail' => $request->inter_detail,
                'inter_year' => $request->inter_year,
                'inter_div' => $request->inter_div,
                'inter_grade' => $request->inter_grade,

                'matric' => $request->matric,
                'matric_college_school' => $request->matric_college_school,
                'matric_detail' => $request->matric_detail,
                'matric_year' => $request->matric_year,
                'matric_div' => $request->matric_div,
                'matric_grade' => $request->matric_grade,

                'organization_name_1' => $request->organization_name_1,
                'post_name_1' => $request->post_name_1,
                'duration_1' => $request->duration_1,
                'salary_1' => $request->salary_1,
                'leaving_reason_1' => $request->leaving_reason_1,
                
                'organization_name_2' => $request->organization_name_2,
                'post_name_2' => $request->post_name_2,
                'duration_2' => $request->duration_2,
                'salary_2' => $request->salary_2,
                'leaving_reason_2' => $request->leaving_reason_2,

                'organization_name_3' => $request->organization_name_3,
                'post_name_3' => $request->post_name_3,
                'duration_3' => $request->duration_3,
                'salary_3' => $request->salary_3,
                'leaving_reason_3' => $request->leaving_reason_3,

                'organization_name_4' => $request->organization_name_4,
                'post_name_4' => $request->post_name_4,
                'duration_4' => $request->duration_4,
                'salary_4' => $request->salary_4,
                'leaving_reason_4' => $request->leaving_reason_4,

                'accept_salary' => $request->accept_salary,
                'lang_known' => $lang_known,
                'father_health_status' => $father_health_status,
                'mother_health_status' => $mother_health_status,

                'father' => $request->father,
                'father_name' => $request->father_name,
                'f_age' => $request->f_age,
                'f_qualification' => $request->f_qualification,
                'f_occupation' => $request->f_occupation,

                'mother' => $request->mother,
                'mother_name' => $request->mother_name,
                'm_age' => $request->m_age,
                'm_qualification' => $request->m_qualification,
                'm_occupation' => $request->m_occupation,

                'brother_1' => $request->brother_1,
                'brother_1_name' => $request->brother_1_name,
                'b_1_age' => $request->b_1_age,
                'b_1_qualification' => $request->b_1_qualification,
                'b_1_occupation' => $request->b_1_occupation,

                'brother_2' => $request->brother_2,
                'brother_2_name' => $request->brother_2_name,
                'b_2_age' => $request->b_2_age,
                'b_2_qualification' => $request->b_2_qualification,
                'b_2_occupation' => $request->b_2_occupation,

                'sister_1' => $request->sister_1,
                'sister_1_name' => $request->sister_1_name,
                's_1_age' => $request->s_1_age,
                's_1_qualification' => $request->s_1_qualification,
                's_1_occupation' => $request->s_1_occupation,

                'sister_2' => $request->sister_2,
                'sister_2_name' => $request->sister_2_name,
                's_2_age' => $request->s_2_age,
                's_2_qualification' => $request->s_2_qualification,
                's_2_occupation' => $request->s_2_occupation,

                'relation_1' => $request->relation_1,
                'rel_name_1' => $request->rel_name_1,
                'rel_designation_1' => $request->rel_designation_1,
                'rel_department_1' => $request->rel_department_1,

                'relation_2' => $request->relation_2,
                'rel_name_2' => $request->rel_name_2,
                'rel_designation_2' => $request->rel_designation_2,
                'rel_department_2' => $request->rel_department_2,

                'relation_3' => $request->relation_3,
                'rel_name_3' => $request->rel_name_3,
                'rel_designation_3' => $request->rel_designation_3,
                'rel_department_3' => $request->rel_department_3,
                
                'relation_4' => $request->relation_4,
                'rel_name_4' => $request->rel_name_4,
                'rel_designation_4' => $request->rel_designation_4,
                'rel_department_4' => $request->rel_department_4,
                
                'medical_history' => $request->medical_history,
                'agreement' => $request->agreement,
                // 'date_current' => $request->date_current,                
            ]);
        $boarding = DB::table('on_boarding')->latest('id')->first();
        return response()->json([
        'data' => $boarding,
        'message' => 'Success'
        ], 201);
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appoint = OnBoarding::join('careers_appointments','careers_appointments.applicant_id','=','on_boarding.id')->where('careers_appointments.applicant_id',$id)->get(['on_boarding.*','careers_appointments.*']);
        return response()->json($appoint);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $boarding = DB::table('on_boarding')->find($id);
        return view('Admin.on_boarding.edit', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        try{
            $this->validate($request, [
                'agreement' => 'required',
                'owns' => 'required',
                'lang_known' => 'required',
                'father_health_status' => 'required',
                'mother_health_status' => 'required',
                'agreement' => 'required',
            ]);
            DB::table('on_boarding')->where('id',$id)->update([
                'post' => $request->post_name,
                'name' => $request->name,
                'email' => $request->email,
                'image' => $request->photo,
                'f_name' => $request->f_name,
                'dob' => $request->birth_date,
                'blood' => $request->blood,
                'cnic' => $request->nic_num,
                'religion' => $request->religion, 
                'marital_status' => $request->marital_status,
                'total_child' => $request->child,
                'kin_address' => $request->address1,
                'kin_phone' => $request->phone1,
                'residence_status' => $request->residence_status,
                'own_conveyance' => implode(',', $request->owns),
                // 'status' => 'Active',
                'permanent_addr' => $request->address2,
                'phone_1st' => $request->phone2,
                'present_addr' => $request->address3,
                'phone_2nd' => $request->phone3,
                'higher' => $request->higher_education,
                'higher_college_school' => $request->higher_college_school,
                'higher_detail' => $request->higher_detail,
                'higher_year' => $request->higher_year,
                'higher_div' => $request->higher_div,
                'higher_grade' => $request->higher_grade,

                'graduation' => $request->graduation,
                'grad_college_school' => $request->grad_college_school,
                'grad_detail' => $request->grad_detail,
                'grad_year' => $request->grad_year,
                'grad_div' => $request->grad_div,
                'grad_grade' => $request->grad_grade,

                'inter' => $request->inter,
                'inter_college_school' => $request->inter_college_school,
                'inter_detail' => $request->inter_detail,
                'inter_year' => $request->inter_year,
                'inter_div' => $request->inter_div,
                'inter_grade' => $request->inter_grade,

                'matric' => $request->matric,
                'matric_college_school' => $request->matric_college_school,
                'matric_detail' => $request->matric_detail,
                'matric_year' => $request->matric_year,
                'matric_div' => $request->matric_div,
                'matric_grade' => $request->matric_grade,

                'organization_name_1' => $request->organization_name_1,
                'post_name_1' => $request->post_name_1,
                'duration_1' => $request->duration_1,
                'salary_1' => $request->salary_1,
                'leaving_reason_1' => $request->leaving_reason_1,
                
                'organization_name_2' => $request->organization_name_2,
                'post_name_2' => $request->post_name_2,
                'duration_2' => $request->duration_2,
                'salary_2' => $request->salary_2,
                'leaving_reason_2' => $request->leaving_reason_2,

                'organization_name_3' => $request->organization_name_3,
                'post_name_3' => $request->post_name_3,
                'duration_3' => $request->duration_3,
                'salary_3' => $request->salary_3,
                'leaving_reason_3' => $request->leaving_reason_3,

                'organization_name_4' => $request->organization_name_4,
                'post_name_4' => $request->post_name_4,
                'duration_4' => $request->duration_4,
                'salary_4' => $request->salary_4,
                'leaving_reason_4' => $request->leaving_reason_4,

                'accept_salary' => $request->accept_salary,
                'lang_known' => implode(',' ,$request->lang_known),
                'father_health_status' => implode(',' ,$request->father_health_status),
                'mother_health_status' => implode(',' ,$request->mother_health_status),

                'father' => $request->father,
                'father_name' => $request->father_name,
                'f_age' => $request->f_age,
                'f_qualification' => $request->f_qualification,
                'f_occupation' => $request->f_occupation,

                'mother' => $request->mother,
                'mother_name' => $request->mother_name,
                'm_age' => $request->m_age,
                'm_qualification' => $request->m_qualification,
                'm_occupation' => $request->m_occupation,

                'brother_1' => $request->brother_1,
                'brother_1_name' => $request->brother_1_name,
                'b_1_age' => $request->b_1_age,
                'b_1_qualification' => $request->b_1_qualification,
                'b_1_occupation' => $request->b_1_occupation,

                'brother_2' => $request->brother_2,
                'brother_2_name' => $request->brother_2_name,
                'b_2_age' => $request->b_2_age,
                'b_2_qualification' => $request->b_2_qualification,
                'b_2_occupation' => $request->b_2_occupation,

                'sister_1' => $request->sister_1,
                'sister_1_name' => $request->sister_1_name,
                's_1_age' => $request->s_1_age,
                's_1_qualification' => $request->s_1_qualification,
                's_1_occupation' => $request->s_1_occupation,

                'sister_2' => $request->sister_2,
                'sister_2_name' => $request->sister_2_name,
                's_2_age' => $request->s_2_age,
                's_2_qualification' => $request->s_2_qualification,
                's_2_occupation' => $request->s_2_occupation,

                'relation_1' => $request->relation_1,
                'rel_name_1' => $request->rel_name_1,
                'rel_designation_1' => $request->rel_designation_1,
                'rel_department_1' => $request->rel_department_1,

                'relation_2' => $request->relation_2,
                'rel_name_2' => $request->rel_name_2,
                'rel_designation_2' => $request->rel_designation_2,
                'rel_department_2' => $request->rel_department_2,

                'relation_3' => $request->relation_3,
                'rel_name_3' => $request->rel_name_3,
                'rel_designation_3' => $request->rel_designation_3,
                'rel_department_3' => $request->rel_department_3,
                
                'relation_4' => $request->relation_4,
                'rel_name_4' => $request->rel_name_4,
                'rel_designation_4' => $request->rel_designation_4,
                'rel_department_4' => $request->rel_department_4,
                
                'medical_history' => $request->medical_history,
                'agreement' => $request->agreement,
                'date_current' => $request->date_current,                
                // 'update_at' => $request->update_at,                
            ]);
        $boarding = DB::table('on_boarding')->find($id);
        return response()->json([
        'data' => $boarding,
        'message' => 'Success'
        ], 201);
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'300','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try{
            // $boarding = DB::table('on_boarding')->where('id', $id)->get();
            // $image = OnBoarding::find($id);

            // $pathToYourFile = 'applicant_images/'.$image->image;
            // if(file_exists($pathToYourFile))  
            // {
            // unlink($pathToYourFile); // delete file/image
            // // and delete the record from database
            // }
            // $boarding = DB::table('on_boarding')->where('id', $id)->delete();
            // dd(Auth::id());
                $boarding = OnBoarding::find($id);
                $boarding->deleted_by = Auth::id();
                $boarding->update();
                $boarding->delete();
            return ['code' => 200 , 'message' => 'success'];
        }catch(\Exception $e){
            return ['code' => 200 , 'error_message' => $e->getMessage()]; 
        }
    }
}
