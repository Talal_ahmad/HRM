<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Models\TrainingSession;
use Illuminate\Support\Facades\DB;
use App\Models\EmployeeTrainingSession;
use Illuminate\Validation\ValidationException;

class TrainingSetupController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            // Course
            if($request->type == 'course'){
                return DataTables::eloquent(Course::leftjoin('employees','courses.coordinator','=','employees.id')->select('courses.id','courses.code','courses.name','courses.trainer','courses.paymentType','employees.first_name'))->make(true);
            }
            // Training Session
            elseif($request->type == 'training_session'){ 
                return DataTables::eloquent(TrainingSession::leftjoin('courses','trainingsessions.course','=','courses.id')->select('trainingsessions.id','trainingsessions.name','trainingsessions.scheduled','trainingsessions.status','trainingsessions.deliveryMethod','trainingsessions.deliveryLocation','trainingsessions.attendanceType','trainingsessions.requireProof','courses.name as cname'))->make(true);
            }
            // Employee Training Session
            else{
                return DataTables::eloquent( 
                $emp = EmployeeTrainingSession::leftJoin('employees', 'employeetrainingsessions.employee', '=', 'employees.id')
                ->select('employees.first_name', 'employeetrainingsessions.status')
                ->addSelect(DB::raw('GROUP_CONCAT(DISTINCT trainingsessions.name ORDER BY trainingsessions.name ASC) AS tnames'))
                ->leftJoin('trainingsessions', function ($join) {
                    $join->on(DB::raw('FIND_IN_SET(trainingsessions.id, employeetrainingsessions.trainingSession)'), '>', DB::raw('0'));
                })
                ->groupBy('employees.first_name', 'employeetrainingsessions.status'))
                ->make(true);
            }
        }
        $courses = Course::get();
        $training_sessions =  TrainingSession::get();
        $employee_training_sessions = EmployeeTrainingSession::get();
        $employees = DB::table('employees')->get();

        return view('Admin.training.trainingsetup.index', compact('courses', 'training_sessions', 'employee_training_sessions','employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()  
    {
        //
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Course
            if($request->type == 'course'){
                $this->validate($request, [
                    'code' => 'required',
                    'name' => 'required',
                    'coordinator' => 'required',
                    'paymentType' => 'required',
                    'currency' => 'required',
                    'cost' => 'required',
                    'status' => 'required',
                ]);

                $data = $request->all();
                Course::create($data);
                return ['code'=>'200','message'=>'success'];    
            }
            // Training Session
            elseif($request->type == 'training_session'){
                $this->validate($request, [
                    'name' => 'required',
                    'course' => 'required',
                    'scheduled' => 'required',
                    'dueDate' => 'required',
                    'deliveryMethod' => 'required',
                    'attendanceType' => 'required',
                    'requireProof' => 'required',
                ]);
                $trainingSession = new TrainingSession();
                $trainingSession->name = $request->input('name');
                $trainingSession->course = $request->input('course');
                $trainingSession->description = $request->input('description');
                $trainingSession->scheduled = $request->input('scheduled');
                $trainingSession->dueDate = $request->input('dueDate');
                $trainingSession->deliveryMethod = $request->input('deliveryMethod');
                $trainingSession->deliveryLocation	 = $request->input('deliveryLocation');
                $trainingSession->attendanceType = $request->input('attendanceType');
                $trainingSession->requireProof = $request->input('requireProof');
                if($request->hasFile('attachment')){
                    $file = $request->file('attachment');
                    $file_extension = $file->getClientOriginalExtension();
                    $file_temp = time() . "." . $file_extension;
                    $file->move('images/training_session/' , $file_temp);
                    $trainingSession->attachment = $file_temp;
                }
                $trainingSession->save();
                return ['code'=>'200','message'=>'success'];
            }
            // Employee Training Session
            else{
                $this->validate($request, [
                    'employee' => 'required',
                    'trainingSession' => 'required',
                    'status' => 'required',
                ]);
                // dd(implode(',', $request->trainingSession));
                $EmployeeTrainingSession = new EmployeeTrainingSession();
                $EmployeeTrainingSession->employee = $request->employee;
                $EmployeeTrainingSession->trainingSession = implode(',', $request->trainingSession);
                $EmployeeTrainingSession->status = $request->status;
                $EmployeeTrainingSession->save();
                return ['code' => '200', 'message' => 'success'];
            }
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        // Course
        if($request->type == 'course'){
            $course = Course::find($id);
            return response()->json($course);
        }
        // Training Session
        elseif($request->type == 'training_session'){
            $training_session = TrainingSession::find($id);
            return response()->json($training_session);
        }
        // Employee Training Session
        else{
            $employee_training_session = EmployeeTrainingSession::find($id);
            return response()->json($employee_training_session);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            // Course
            if($request->type == 'course'){
                $this->validate($request, [
                    'code' => 'required',
                    'name' => 'required',
                    'coordinator' => 'required',
                    'paymentType' => 'required',
                    'currency' => 'required',
                    'cost' => 'required',
                    'status' => 'required',
                ]);

                $course = Course::find($id);
                $course->fill($request->all())->save();
                return ['code'=>'200','message'=>'success'];
            }
            // Training Session
            elseif($request->type == 'training_session'){
                $this->validate($request, [
                    'name' => 'required',
                    'course' => 'required',
                    'scheduled' => 'required',
                    'dueDate' => 'required',
                    'deliveryMethod' => 'required',
                    'attendanceType' => 'required',
                    'requireProof' => 'required',
                ]);
                $trainingSession = TrainingSession::find($id);
                $trainingSession->name =  $request->input('name');
                $trainingSession->course = $request->input('course');
                $trainingSession->description = $request->input('description');
                $trainingSession->scheduled = $request->input('scheduled');
                $trainingSession->dueDate = $request->input('dueDate');
                $trainingSession->deliveryMethod = $request->input('deliveryMethod');
                $trainingSession->deliveryLocation	 = $request->input('deliveryLocation');
                $trainingSession->attendanceType = $request->input('attendanceType');
                $trainingSession->requireProof = $request->input('requireProof');
                if($request->hasFile('attachment')){
                    unlink('images/training_session/' , $trainingSession->attachment);
                    $file = $request->file('attachment');
                    $file_extension = $file->getClientOriginalExtension();
                    $file_temp = time() . "." . $file_extension;
                    $file->move('images/training_session/' , $file_temp);
                    $trainingSession->attachment = $file_temp;
                }
                $image = $trainingSession->attachment;
                $trainingSession->attachment = $image;
                $trainingSession->save();
                // $training_session->fill($request->all())->save();
                return ['code'=>'200','message'=>'success'];
            }
            // Employee Training Session
            else{
                $this->validate($request, [
                    'employee' => 'required',
                    'trainingSession' => 'required',
                    'status' => 'required',
                ]);

                $employee_training_session = EmployeeTrainingSession::find($id);
                $employee_training_session->fill($request->all())->save();
                return ['code'=>'200','message'=>'success'];
            }
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id) 
    {
        try{
            // Course
            if($request->type == 'course'){
                $course = Course::find($id);
                $course->deleted_by = Auth::id();
                $course->update();
                $course->delete();
                return ['code'=>'200','message'=>'success'];
            }
            // Training Session
            elseif($request->type == 'training_session'){
                $training_session = TrainingSession::find($id);
                if($training_session->attachment){
                    unlink('images/training_session/'.$training_session->attachment);
                }
                $training_session->deleted_by = Auth::id();
                $training_session->update();
                $training_session->delete();
                return ['code'=>'200','message'=>'success'];
            }
            // Employee Training Session
            else{
                $EmployeeTrainingSession = EmployeeTrainingSession::find($id);
                $EmployeeTrainingSession->deleted_by = Auth::id();
                $EmployeeTrainingSession->update();
                $EmployeeTrainingSession->delete();
                return ['code'=>'200','message'=>'success'];
            }
        }catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }    
    }
}
