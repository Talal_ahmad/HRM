<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
// use Spatie\Permission\Models\PermissionTitle;
use App\Models\PermissionTitle;
use Illuminate\Validation\ValidationException;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::eloquent(Role::query())->make(true);
        }
        $permissions = DB::table('permissions')->get();
        // $permission_titles = DB::table('permission-titles')->get();
        return view('Admin.settings.roles&permission.role', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = DB::table('permissions')->get();
        $permission_titles = DB::table('permission_titles')->get();

        return view('Admin.settings.roles&permission.create_role', compact('permissions', 'permission_titles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'roll_name' => 'required',
        ]);
        $role = new Role();
        $role->name = $request->roll_name;
        $role->syncPermissions($request->permission_name);
        $role->save();
        return redirect()->back()->with('message', 'Role Added Successfuly!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = DB::table('permissions')->get();
        $role_has_permissions = DB::table('role_has_permissions')->where('role_id', '=', $id)->pluck('permission_id')->toArray();
        // $permission_titles = DB::table('permission_titles')->get();

        // Fetching the permission titles related to the role's permissions
        // $permission_titles_ids = DB::table('permissions')->whereIn('id', $role_has_permissions)->pluck('permission_titles_id')->toArray();
        $permission_titles = PermissionTitle::get();
        // dd($permission_titles);

        return view('Admin.settings.roles&permission.edit_role', compact('role', 'permissions', 'role_has_permissions', 'permission_titles'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'roll_name' => 'required',
        ]);
        $role = Role::find($id);
        $role->name = $request->roll_name;
        $role->syncPermissions($request->permission_name);
        $role->save();
        return redirect()->to('roles')->with('message', 'Role Updated Successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $role = Role::find($id);
            $role->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}