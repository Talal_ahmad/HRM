<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\Employee;
use Illuminate\Http\Request; 
use App\Models\ShiftManagement;
use App\Models\ManualAttendance;
use App\Models\EmployeeLeaveBalance;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Spatie\Activitylog\Models\Activity;

class ManualAttendanceController extends Controller
{ 
    public function __construct(){ 
        $this->middleware('auth');
    }

    public function index(Request $request)  
    {
        if($request->ajax())
        {
            $query = ManualAttendance::join('employees','attendance.employee','=','employees.id')
            ->leftjoin('companystructures','employees.department','=','companystructures.id')
            ->leftjoin('jobtitles','employees.job_title','=','jobtitles.id')
            ->where('employees.status', 'Active')
            ->whereIn('employees.department' , login_user_departments())
            ->select('attendance.*' , 'employees.employee_id' ,'employees.employee_code' , 'employees.first_name' , 'employees.last_name' , 'employees.middle_name' , 'companystructures.title AS department' , 'jobtitles.name as designation');
            return DataTables::of($query)->filter(function ($query) use ($request) {
                if(!empty($request->departmentFilter)){
                    if (env('COMPANY') != 'JSML') {
                        $query->where('employees.department', $request->departmentFilter);
                    }else{
                        $query->whereIn('employees.department', $request->section);
                    }
                }
                if (!empty($request->employeeFilter)) {
                    $query->where('employees.id', $request->employeeFilter);
                }
                if(!empty($request->fromDateFilter) && !empty($request->toDateFilter)){
                    $query->whereBetween(DB::raw("DATE(attendance.in_time)") , [$request->fromDateFilter , $request->toDateFilter]);
                }
                if(!empty($request->dateFilter)){
                    $query->whereDate('attendance.in_time' , $request->dateFilter);
                }
                // else{
                //     $query->where(DB::raw("DATE(attendance.in_time)") , date('Y-m-d'))->orWhere(DB::raw("DATE(attendance.out_time)") , date('Y-m-d'));
                // }
            },true)->make(true);
        }
        return view('Admin.manualAttendance.index', get_defined_vars());    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'employee' => 'required',
                'in_time' => 'required',
                // 'out_time' => 'required',
                'in_date' => 'required|date|before_or_equal:today',
                'out_date' => 'nullable|date|before_or_equal:today',
            ]);
            if (!empty($request->out_time&&$request->out_time)) {
                $data = $request->all();
            // Get Employee Department
            $employeeDepartment = Employee::where('id' , $data['employee'])->first(['employee_id', 'employee_code','first_name', 'last_name', 'department','employment_status']);
            // Get Employee Shift
            $employeeShift = ShiftManagement::leftJoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
            ->where('shift_management.employee_id' , $data['employee'])
            ->where('shift_management.shift_from_date' , '<=' , $data['in_date'])
            ->where('shift_management.shift_to_date' , '>=' , $data['in_date'])
            ->select('shift_management.*' , 'shift_type.shift_start_time' , 'shift_type.shift_end_time','shift_type.id as shift_type_id')
            ->latest()->first();
            if($employeeShift)
            {   
                $data['shift_id'] = $employeeShift->shift_id;
                $data['shift_start_time'] = $employeeShift->shift_start_time;
                $data['shift_end_time'] = $employeeShift->shift_end_time;
                $data['work_week_id'] = $employeeShift->work_week_id;
                $data['late_time_allowed'] = $employeeShift->late_time_allowed;
            }
            else
            {
                $data['shift_id'] = 1;
                $data['shift_start_time'] = '09:00:00';
                $data['shift_end_time'] = '17:00:00';
                $data['work_week_id'] = 1;
                $data['late_time_allowed'] = '11';
            }
            // dd($data['shift_id']);
            // dd($employeeShift->id);
            $data['department_id'] = $employeeDepartment->department;
            $data['is_manual'] = 1;
            $data['in_time'] = $data['in_date']. ' ' .$data['in_time'];
            $data['out_time'] = $data['out_date']. ' ' .$data['out_time'];
            unset($data['in_date'] , $data['out_date']);
            $attendance = new ManualAttendance();
            $attendance->create($data)->save();
            }else{
                $data = $request->all();
            // Get Employee Department
            $employeeDepartment = Employee::where('id' , $data['employee'])->first(['employee_id', 'employee_code','first_name', 'last_name', 'department']);
            // Get Employee Shift
            $employeeShift = ShiftManagement::leftJoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
            ->where('shift_management.employee_id' , $data['employee'])
            ->where('shift_management.shift_from_date' , '<=' , $data['in_date'])
            ->where('shift_management.shift_to_date' , '>=' , $data['in_date'])
            ->select('shift_management.*' , 'shift_type.shift_start_time' , 'shift_type.shift_end_time')
            ->first();
            if($employeeShift)
            {   
                $data['shift_id'] = $employeeShift->shift_id;
                $data['shift_start_time'] = $employeeShift->shift_start_time;
                $data['shift_end_time'] = $employeeShift->shift_end_time;
                $data['work_week_id'] = $employeeShift->work_week_id;
                $data['late_time_allowed'] = $employeeShift->late_time_allowed;
            }
            else
            {
                $data['shift_id'] = 1;
                $data['shift_start_time'] = '09:00:00';
                $data['shift_end_time'] = '17:00:00';
                $data['work_week_id'] = 1;
                $data['late_time_allowed'] = '11';
            }
            $data['department_id'] = $employeeDepartment->department;
            $data['is_manual'] = 1;
            $data['in_time'] = $data['in_date']. ' ' .$data['in_time'];
            unset($data['in_date'] , $data['out_date']);
            $attendance = new ManualAttendance();
            $attendance->create($data)->save();
            }   
            $cpl_balance = DB::table('shift_management')->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
            ->join('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
            ->where([
                ['shift_management.id' , $employeeShift->id],
                // ['shift_management.status', '=', 'Approved']
            ])
            ->select('shift_management.work_week_id','shift_management.shift_id','shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc','shift_type.sandwich_absent','work_week.desc as work_week','shift_management.late_time_allowed')
            ->orderBy('shift_management.id', 'DESC')
            ->first();

            // $checkAttendance = Attendance::where('employee', $request['id'])->whereDate('in_time',$date)->orderBy('id', 'DESC')->first();
                // checking for C shift
            $checkInDateTime = Carbon::parse($request->in_time);
            $checkinTs = $checkInDateTime->format('H:i:s');
            $startTime = Carbon::parse('23:00:00')->format('H:i:s'); 
            $endTime = Carbon::parse('00:15:00')->format('H:i:s'); 

            $work_week_days = DB::table('workdays')->where([
                ['work_week_id', $cpl_balance->work_week_id],
                ['status', '=', 'Non-working Day']
                ])->pluck('name')->toArray();
                // dd($data['in_time']);
                $off_day = 0;
            if($checkinTs >= $startTime && $checkinTs <= '23:59:59' && $cpl_balance->shift_desc=="C (2400 ~ 0800)"){
                $cpl_date = date('Y-m-d' , strtotime($data['in_time'].' +1 day'));
                $holiday = DB::table('holidays')->where('dateh', "$cpl_date")->first();
                $off_day = 1;
            }else{
                $cpl_date = date('Y-m-d' , strtotime($data['in_time']));
                if(in_array(date('l', strtotime("$cpl_date")), $work_week_days)){
                    $off_day = 1;
                }
                $holiday = DB::table('holidays')->where('dateh', "$cpl_date")->first();
                // dd($holiday);
            }
            if($off_day ==1 || !empty($holiday) && $employeeDepartment->employment_status!=9){
                $cpl_value = EmployeeLeaveBalance::where('emp_id',$data['employee'])->first();
                if($cpl_value){
                    $update_cpl = EmployeeLeaveBalance::where('emp_id',$data['employee'])->first();
                    $update_cpl->balance += 1;
                    $update_cpl->update();
                }else{
                    $add_cpl = new EmployeeLeaveBalance();
                    $add_cpl->emp_id = $data['employee'];
                    $add_cpl->leave_type = 4;
                    $add_cpl->balance=1;
                    $add_cpl->save();
                }
            }
            // dd($cpl_balance);         
            // activity('Manual Attendance')
            // ->performedOn($attendance)
            // ->event('created')
            // ->withProperties(['attributes' => ['employeeId' => $data['employee_id'], 'employee_id' => $employeeDepartment->employee_id, 'employee_code' => $employeeDepartment->employee_code, 'first_name' => $employeeDepartment->first_name]])
            // ->log(Auth::user()->username.' have created manual attendance');
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        $query = DB::table('employees')->select('id' , 'first_name' , 'last_name')
        ->where('status' , '=' ,  'Active')
        ->where('department' , '=' , $id)
        ->get(); 
        return response()->json($query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data = ManualAttendance::find($id);
       return response()->json($data);     
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'employee' => 'required',
                'in_time' => 'required',
                'out_time' => 'required',
                'in_date' => 'required|date|before_or_equal:today',
                'out_date' => 'required|date|before_or_equal:today',
            ]);
            $data = $request->all();
            // Get Employee Department
            $employeeDepartment = Employee::where('id' , $data['employee'])->first(['employee_id', 'employee_code','first_name', 'last_name', 'department']);
            // Get Employee Shift
            $employeeShift = ShiftManagement::leftJoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
            ->where('shift_management.employee_id' , $data['employee'])
            ->where('shift_management.shift_from_date' , '<=' , $data['in_date'])
            ->where('shift_management.shift_to_date' , '>=' , $data['in_date'])
            ->select('shift_management.*' , 'shift_type.shift_start_time' , 'shift_type.shift_end_time')
            ->first();
            if($employeeShift)
            {   
                $data['shift_id'] = $employeeShift->shift_id;
                $data['shift_start_time'] = $employeeShift->shift_start_time;
                $data['shift_end_time'] = $employeeShift->shift_end_time;
                $data['work_week_id'] = $employeeShift->work_week_id;
                $data['late_time_allowed'] = $employeeShift->late_time_allowed;
            }
            else
            {
                $data['shift_id'] = 1;
                $data['shift_start_time'] = '09:00:00';
                $data['shift_end_time'] = '17:00:00';
                $data['work_week_id'] = 1;
                $data['late_time_allowed'] = '11';
            }
            if(!isset($data['od'])){
                $data['od'] = 0;
            }
            if(!isset($data['ret'])){
                $data['ret'] = 0;
            }
            $data['department_id'] = $employeeDepartment->department;
            $data['in_time'] = $data['in_date']. ' ' .$data['in_time'];
            $data['out_time'] = $data['out_date']. ' ' .$data['out_time'];
            unset($data['in_date'] , $data['out_date']);
            $attendance = ManualAttendance::find($id);
            $attendance->fill($data)->save();
            // activity('Manual Attendance')
            // ->performedOn($attendance)
            // ->event('updated')
            // ->withProperties(['attributes' => ['employeeId' => $data['employee_id'], 'employee_id' => $employeeDepartment->employee_id, 'employee_code' => $employeeDepartment->employee_code, 'first_name' => $employeeDepartment->first_name, 'in_time' => $data['in_time'],'out_time' => $data['out_time']],'old' => ['employeeId' => $data['employee_id'], 'employee_id' => $employeeDepartment->employee_id, 'employee_code' => $employeeDepartment->employee_code, 'first_name' => $employeeDepartment->first_name,'in_time' => $attendance->in_time,'out_time' => $attendance->out_time]])
            // ->log(Auth::user()->username.' have updated manual attendance');
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $attendance = ManualAttendance::find($id);
            $attendance->delete();
            return ['code' => 200 , 'message' => 'success'];
        }catch(\Exception $e){
            return ['code' => 200 , 'error_message' => $e->getMessage()]; 
        }
    }
}