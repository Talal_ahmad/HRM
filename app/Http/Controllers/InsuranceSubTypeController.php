<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\InsuranceSubType;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class InsuranceSubTypeController extends Controller
{ 
    public function __construct(){  
        $this->middleware('auth');
    }

    public function index(Request $request)   
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @rownum=0'));
            $data = InsuranceSubType::select(['id', 'sub_type','description',DB::raw('@rownum  := @rownum  + 1 AS rownum')]);
            return Datatables::eloquent($data)->make(true); 
        }
        return view('Admin.finance.insuranceSubTypes.index');    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'sub_type' => 'required',
            ]);

            $data = $request->all(); 
            InsuranceSubType::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = InsuranceSubType::find($id);
        return response()->json($data);     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'sub_type' => 'required',
            ]);

            $InsuranceSubType = InsuranceSubType::find($id);
            $InsuranceSubType->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $InsuranceSubType = InsuranceSubType::find($id);
            $InsuranceSubType->deleted_by = Auth::id();
            $InsuranceSubType->update();
            $InsuranceSubType->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception $e){
            return ['code' => 422 , 'error_message' => $e->getMessage()]; 
        }
    }
}