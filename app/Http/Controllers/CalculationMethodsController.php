<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\CalculationMethod;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CalculationMethodsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    } 
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = CalculationMethod::leftjoin('deductiongroup','deductions.deduction_group','=','deductiongroup.id')->select('deductions.id','deductions.name','deductiongroup.name AS deductionGroup');
            return Datatables::of($data)->addIndexColumn()->make(true);
        }
        return view('Admin.finance.calculationMethods.index');  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() 
    {
        $salaryComponentTypes = DB::table('salarycomponenttype')->get(['id', 'name']); 
        $salaryComponent = DB::table('salarycomponent')->get(['id', 'name']);
        $payrollReportColumn = DB::table('payrollcolumns')->get(['id', 'name']);
        $calculationGroups = DB::table('deductiongroup')->get(['id', 'name']);
        return view('Admin.finance.calculationMethods.create', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'componentTypes' => 'required',
            'components' => 'required',
        ]);
        $calculation = new CalculationMethod;
        $calculation->name = $request->name;
        $calculation->componentType = json_encode($request->componentTypes);
        $calculation->component = json_encode($request->components);
        $calculation->payrollColumn = $request->payrollColumn;
        $calculation->deduction_group = $request->calculation_group;
        $calculation->rangeAmounts = isset($request->rangeAmounts) ? json_encode($request->rangeAmounts) : NULL;
        $calculation->save();
        return redirect('calculationMethods')->with('success_message', 'Calculation Method has been Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $CalculationMethod = CalculationMethod::find($id);
        $salaryComponentTypes = DB::table('salarycomponenttype')->get(['id', 'name']); 
        $salaryComponent = DB::table('salarycomponent')->get(['id', 'name']);
        $payrollReportColumn = DB::table('payrollcolumns')->get(['id', 'name']);
        $calculationGroups = DB::table('deductiongroup')->get(['id', 'name']);
        return view('Admin.finance.calculationMethods.edit', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'componentTypes' => 'required',
            'components' => 'required',
        ]);
        $calculation = CalculationMethod::find($id);
        $calculation->name = $request->name;
        $calculation->componentType = json_encode($request->componentTypes);
        $calculation->component = json_encode($request->components);
        $calculation->payrollColumn = $request->payrollColumn;
        $calculation->deduction_group = $request->calculation_group;
        $calculation->update();
        return redirect('calculationMethods')->with('success_message', 'Calculation Method has been Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $CalculationMethod = CalculationMethod::find($id);
            $CalculationMethod->deleted_by = Auth::id();
            $CalculationMethod->update();
            $CalculationMethod->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }


}
