<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\PaymentCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PaymentCodesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @rownum=0'));
            $data = PaymentCode::select('id','code', 'description',DB::raw('@rownum  := @rownum  + 1 AS rownum'));
            return Datatables::eloquent($data)->make(true);
        }
        return view('Admin.finance.paymentCodes.index');  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'code' => 'required|unique:payment_codes',
            ]);
            
            $data = $request->all(); 
            PaymentCode::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $PaymentCode = PaymentCode::find($id);
        return response()->json($PaymentCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'code' => 'required|unique:payment_codes'
            ]);

            $PaymentCode = PaymentCode::find($id);
            $PaymentCode->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{ 
            $payment_code = PaymentCode::find($id);
            $payment_code->deleted_by = Auth::user()->id;
            $payment_code->update();

            $payment_code->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }


}