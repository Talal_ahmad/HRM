<?php

namespace App\Http\Controllers;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\Course;
use App\Models\TrainingSession;
use App\Models\EmployeeTrainingSession;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EmployeeTrainingSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            
            return DataTables::eloquent(EmployeeTrainingSession::leftjoin('employees','employeetrainingsessions.employee','=','employees.id')
        ->leftjoin('trainingsessions','employeetrainingsessions.trainingSession','=','trainingsessions.id')
        ->select('employeetrainingsessions.status','employees.first_name','trainingsessions.name as tname'))->make(true);
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'employee' => 'required',
                'trainingSession' => 'required',
                'status' => 'required',
            ]);

            $data = $request->all();
            EmployeeTrainingSession::create($data);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee_training_session = EmployeeTrainingSession::find($id);
        return response()->json($employee_training_session);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'employee' => 'required',
                'trainingSession' => 'required',
                'status' => 'required',
            ]);

            $employee_training_session = EmployeeTrainingSession::find($id);
            $employee_training_session->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            EmployeeTrainingSession::find($id)->delete();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
