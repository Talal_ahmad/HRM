<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\ShiftManagement;
use App\Models\ManualAttendanceRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class AttendanceApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        if(request()->ajax()){
            $query = DB::table('manual_attendance_request')->select('manual_attendance_request.id' , 'manual_attendance_request.in_time' , 'manual_attendance_request.out_time' , 'manual_attendance_request.status' , 'manual_attendance_request.note' , 'employees.employee_code' , 'employees.employee_id' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation')
            ->leftjoin('employees' , 'employees.id' , '=' , 'manual_attendance_request.employee_id')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'manual_attendance_request.department_id')
            ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
            ->where('employees.status' , 'Active')
            ->whereIn('employees.department' , login_user_departments());
            if (userRoles()=="Self Service") {
                $query->where('manual_attendance_request.employee_id' , Auth::user()->employee);
            }
            return DataTables::of($query)->filter(function ($query){
                if(!empty(request()->departmentFilter != 'all' && userRoles()!="Self Service")){
                    $query->where('manual_attendance_request.department_id' , request()->departmentFilter);
                }
                if(!empty(request()->employeeFilter)){
                    $query->where('employees.id' , request()->employeeFilter);
                }
                if(!empty(request()->statusFilter)){
                    $query->where('manual_attendance_request.status' , request()->statusFilter);
                }
                if(!empty(request()->fromDateFilter) && !empty(request()->toDateFilter)){
                    $query->where(DB::raw('DATE(manual_attendance_request.in_time)') , '>=' ,  request()->fromDateFilter)->where(DB::raw('DATE(manual_attendance_request.in_time)') , '<=' , request()->toDateFilter);
                }
            },true)->make(true);
        }
        return view('Admin.request&approvals.attendance_approvals.index');
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'employee' => 'required',
                'in_time' => 'required',
                // 'out_time' => 'required',
                'in_date' => 'required',
                // 'out_date' => 'required',
            ]);

            if (!empty($request->out_time&&$request->out_time)) {
                $department = Employee::where('id' , $request->employee)->first(['department']);

            $shift = ShiftManagement::select('shift_management.*' , 'shift_type.shift_start_time' , 'shift_type.shift_end_time')
            ->leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
            ->where([
                ['shift_management.employee_id' , $request->employee],
                ['shift_management.shift_from_date' , '<=' , $request->in_date],
                ['shift_management.shift_to_date' , '>=' , $request->in_date]
            ])->orderBy('shift_management.id' , 'desc')->first();
            if(!empty($shift)){
                $data['shift_id'] = $shift->shift_id;
                $data['shift_start_time'] = $shift->shift_start_time;
                $data['shift_end_time'] = $shift->shift_end_time;
                $data['work_week_id'] = $shift->work_week_id;
                $data['late_time_allowed'] = $shift->late_time_allowed;
            }
            else{
                $data['shift_id'] = 1;
                $data['shift_start_time'] = '09:00:00';
                $data['shift_end_time'] = '17:00:00';
                $data['work_week_id'] = 1;
                $data['late_time_allowed'] = 11;
            }

            if(!empty($request->status)){
                $attendance = DB::table('attendance')->where([
                    ['employee' , $request->employee],
                    [DB::raw('DATE(in_time)') , $request->in_date]
                    ])->first();
                    
                if(!empty($attendance)){
                    DB::table('attendance')->where([
                        ['employee' , $request->employee],
                        [DB::raw('DATE(in_time)') , $request->in_date]
                        ])->delete();
                }
                DB::table('attendance')->insert([
                    'employee' => $request->employee,
                    'in_time' => $request->in_date.' '.$request->in_time,
                    'out_time' => $request->out_date.' '.$request->out_time, 
                    'department_id' => $department->department,
                    'is_online' => 1,
                    'is_manual' => 1,
                    'shift_id' => $data['shift_id'],
                    'shift_start_time' => $data['shift_start_time'],
                    'shift_end_time' => $data['shift_end_time'],
                    'work_week_id' => $data['work_week_id'],
                    'late_time_allowed' => $data['late_time_allowed'],
                    'note' => $request->note,
                ]);
            }

            $manual_attendance = DB::table('manual_attendance_request')->where([
                ['employee_id' , $request->employee],
                [DB::raw('DATE(in_time)') , $request->in_date]
            ])->first();

            if(!empty($manual_attendance)){
                DB::table('manual_attendance_request')->where([
                    ['employee_id' , $request->employee],
                    [DB::raw('DATE(in_time)') , $request->in_date]
                ])->delete();
            }
            // dd($request->hasFile('attachment'));
            $manual_attendance = new ManualAttendanceRequest();
            $manual_attendance->employee_id = $request->employee;
            $manual_attendance->in_time = $request->in_date.' '.$request->in_time;
            $manual_attendance->out_time = $request->out_date.' '.$request->out_time;
            $manual_attendance->department_id = $department->department;
            $manual_attendance->shift_id = $data['shift_id'];
            $manual_attendance->shift_start_time = $data['shift_start_time'];
            $manual_attendance->shift_end_time = $data['shift_end_time'];
            $manual_attendance->work_week_id = $data['work_week_id'];
            $manual_attendance->late_time_allowed = $data['late_time_allowed'];
            $manual_attendance->note = $request->note;
            $manual_attendance->status = empty($request->status) ? 'Pending' : 'Approved';
            // if($request->status=='Approved'){ 
            //     $manual_attendance->status_changed_by = Auth::id();     
            //     $manual_attendance->status_changed_at = Carbon::now(); 
            // }]
            if($request->hasFile('attachment')){
                $file = $request->file('attachment');
                $extension = $file->getClientOriginalExtension();
                $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                $file->move('images/attendace_request/' , $temp_name);
                $manual_attendance->attachment = $temp_name;
            }
            $manual_attendance->submitted_by = Auth::id();        
            $manual_attendance->save();
            // DB::table('manual_attendance_request')->insert([
            //     'employee_id' => $request->employee,
            //     'in_time' => $request->in_date.' '.$request->in_time,
            //     'out_time' => $request->out_date.' '.$request->out_time, 
            //     'department_id' => $department->department,
            //     'shift_id' => $data['shift_id'],
            //     'shift_start_time' => $data['shift_start_time'],
            //     'shift_end_time' => $data['shift_end_time'],
            //     'work_week_id' => $data['work_week_id'],
            //     'late_time_allowed' => $data['late_time_allowed'],
            //     'attachment' => $request->file('attachment'),

            //     'note' => $request->note,
            //     'status' => empty($request->status) ? 'Pending' : 'Approved',
            //     'submitted_by' => Auth::id()
            // ]);
            }else{
                $department = Employee::where('id' , $request->employee)->first(['department']);

            $shift = ShiftManagement::select('shift_management.*' , 'shift_type.shift_start_time' , 'shift_type.shift_end_time')
            ->leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
            ->where([
                ['shift_management.employee_id' , $request->employee],
                ['shift_management.shift_from_date' , '<=' , $request->in_date],
                ['shift_management.shift_to_date' , '>=' , $request->in_date]
            ])->orderBy('shift_management.id' , 'desc')->first();
            if(!empty($shift)){
                $data['shift_id'] = $shift->shift_id;
                $data['shift_start_time'] = $shift->shift_start_time;
                $data['shift_end_time'] = $shift->shift_end_time;
                $data['work_week_id'] = $shift->work_week_id;
                $data['late_time_allowed'] = $shift->late_time_allowed;
            }
            else{
                $data['shift_id'] = 1;
                $data['shift_start_time'] = '09:00:00';
                $data['shift_end_time'] = '17:00:00';
                $data['work_week_id'] = 1;
                $data['late_time_allowed'] = 11;
            }

            if(!empty($request->status)){
                $attendance = DB::table('attendance')->where([
                    ['employee' , $request->employee],
                    [DB::raw('DATE(in_time)') , $request->in_date]
                    ])->first();
                    
                if(!empty($attendance)){
                    DB::table('attendance')->where([
                        ['employee' , $request->employee],
                        [DB::raw('DATE(in_time)') , $request->in_date]
                        ])->delete();
                }
                DB::table('attendance')->insert([
                    'employee' => $request->employee,
                    'in_time' => $request->in_date.' '.$request->in_time,
                    'out_time' => $request->out_date.' '.$request->out_time, 
                    'department_id' => $department->department,
                    'is_online' => 1,
                    'is_manual' => 1,
                    'shift_id' => $data['shift_id'],
                    'shift_start_time' => $data['shift_start_time'],
                    'shift_end_time' => $data['shift_end_time'],
                    'work_week_id' => $data['work_week_id'],
                    'late_time_allowed' => $data['late_time_allowed'],
                    'note' => $request->note,
                ]);
            }

            $manual_attendance = DB::table('manual_attendance_request')->where([
                ['employee_id' , $request->employee],
                [DB::raw('DATE(in_time)') , $request->in_date]
            ])->first();

            if(!empty($manual_attendance)){
                DB::table('manual_attendance_request')->where([
                    ['employee_id' , $request->employee],
                    [DB::raw('DATE(in_time)') , $request->in_date]
                ])->delete();
            }
            
            $manual_attendance = new ManualAttendanceRequest();
            $manual_attendance->employee_id = $request->employee;
            $manual_attendance->in_time = $request->in_date.' '.$request->in_time;
            $manual_attendance->out_time = $request->out_date.' '.$request->out_time;
            $manual_attendance->department_id = $department->department;
            $manual_attendance->shift_id = $data['shift_id'];
            $manual_attendance->shift_start_time = $data['shift_start_time'];
            $manual_attendance->shift_end_time = $data['shift_end_time'];
            $manual_attendance->work_week_id = $data['work_week_id'];
            $manual_attendance->late_time_allowed = $data['late_time_allowed'];
            $manual_attendance->note = $request->note;
            $manual_attendance->status = empty($request->status) ? 'Pending' : 'Approved';
            // if($request->status=='Approved'){ 
            //     $manual_attendance->status_changed_by = Auth::id();     
            //     $manual_attendance->status_changed_at = Carbon::now(); 
            // }]
            if($request->hasFile('attachment')){
                $file = $request->file('attachment');
                $extension = $file->getClientOriginalExtension();
                $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                $file->move('images/attendace_request/' , $temp_name);
                $manual_attendance->attachment = $temp_name;
            }
            $manual_attendance->submitted_by = Auth::id();        
            $manual_attendance->save();
            
            // DB::table('manual_attendance_request')->insert([
            //     'employee_id' => $request->employee,
            //     'in_time' => $request->in_date.' '.$request->in_time,
            //     // 'out_time' => $request->out_date.' '.$request->out_time, 
            //     'department_id' => $department->department,
            //     'shift_id' => $data['shift_id'],
            //     'shift_start_time' => $data['shift_start_time'],
            //     'shift_end_time' => $data['shift_end_time'],
            //     'work_week_id' => $data['work_week_id'],
            //     'late_time_allowed' => $data['late_time_allowed'],
            //     'attachment' => $request->file('attachment'),
            //     'note' => $request->note,
            //     'status' => empty($request->status) ? 'Pending' : 'Approved',
            //     'submitted_by' => Auth::id()
            // ]);
            }
            
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('manual_attendance_request')->find($id);
        // dd($data);
        return response()->json($data); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'employee' => 'required',
                'in_time' => 'required',
                'out_time' => 'required',
                'in_date' => 'required',
                'out_date' => 'required',
            ]);

            DB::table('manual_attendance_request')->where('id',$id)->update([
                'employee_id' => $request->employee,
                'in_time' => $request->in_date.' '.$request->in_time,
                'out_time' => $request->out_date.' '.$request->out_time, 
                'note' => $request->note,
                'submitted_by' => Auth::id()
            ]);

            return ['code'=>'200','message' => 'success'];
        }  
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status_change(){
        
    }
}
