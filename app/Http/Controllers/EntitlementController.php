<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\Entitlement;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EntitlementController extends Controller
{ 
    public function __construct(){ 
        $this->middleware('auth');
    }
 
    public function index(Request $request)   
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @rownum=0'));
            $data = Entitlement::leftjoin('paygrades','entitlement.paygroup','=','paygrades.id')->leftjoin('jobtitles','entitlement.designation','=','jobtitles.id')->select('entitlement.id','entitlement.name','entitlement.amount','entitlement.description','paygrades.name as paygroup','jobtitles.name as designation',DB::raw('@rownum  := @rownum  + 1 AS rownum'));
            return DataTables::eloquent($data)->make(true); 
        }

        $paygrades=DB::table('paygrades')->get();
        $designations=DB::table('jobtitles')->get(); 
        return view('Admin.finance.entitlement.index', get_defined_vars());    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'name' => 'required',
            ]);

            $data = $request->all(); 
            Entitlement::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
     $data = Entitlement::find($id);

     return response()->json($data);     
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'name' => 'required',
            ]);

            $Entitlement = Entitlement::find($id);
            $Entitlement->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $Entitlement = Entitlement::find($id);
            $Entitlement->deleted_by = Auth::id();
            $Entitlement->update();
            $Entitlement->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception $e){
            return ['code' => 500 , 'error_message' => $e->getMessage()]; 
        }
    }
}