<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\JobTitle;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class JobTitlesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return Datatables::eloquent(JobTitle::query())->addIndexColumn()->make(true);
        }
        $skills=DB::table('skills')->get(['id', 'name']);
        $pid=DB::table('jobtitles')->get(['id', 'name']);
        return view('Admin.setup.jobtitles.index', compact('skills','pid'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required |string',
            ]);

            $data = $request->all();
            if(!empty($data['attachment'])){
                $data['attachment'] = JobTitle::InsertImage($data['attachment']);
            }
            JobTitle::create($data);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $JobTitle = JobTitle::find($id);
        return response()->json($JobTitle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'name' => 'required |string',
            ]);
            $data = $request->all();
            $JobTitle = JobTitle::find($id);
            if(!empty($data['attachment'])){
                if(!empty($JobTitle->attachment)){
                    unlink(public_path().$JobTitle->attachment);
                }
                $data['attachment'] = JobTitle::InsertImage($data['attachment']);
            }
            $JobTitle->fill($data)->save();
            // dd($JobTitle);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
   
        try{
            $job_title = JobTitle::find($id);
            $job_title->deleted_by = Auth::id();
            $employee = Employee::where('job_title' , $id)->get();
            if(count($employee) > 0){
                return ['code'=>'300', 'message'=>'This JobTitle is Assigned to Some Employees Please change it first!'];
            }
            $job_title->update();
            $job_title->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }


}
