<?php

namespace App\Http\Controllers;

use App\Models\CompanyStructure;
use App\Models\Employee;
use App\Models\Kpi_Defination;
use Illuminate\Http\Request;

class KpiEvalationFormController extends Controller
{
    public function index(){
        $departments=CompanyStructure::get(['id','title']);
        $kpis= Kpi_Defination::get(['id','kpi']);
        $employees = Employee::where('status', 'Active')
        ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
        ->leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
        ->get(['employees.id', 'first_name', 'middle_name', 'last_name', 'employee_id', 'employee_code', 'companystructures.title as department', 'jobtitles.name as job_title']);
        return view('Admin.kpis.kpi_evaluation_form', get_defined_vars());
    }
}
