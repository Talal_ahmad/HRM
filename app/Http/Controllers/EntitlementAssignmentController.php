<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\Entitlement;
use App\Models\CompanyStructure;
use Illuminate\Support\Facades\DB;
use App\Models\EmployeeEntitlement;
use Illuminate\Validation\ValidationException;

class EntitlementAssignmentController extends Controller
{ 
    public function __construct(){  
        $this->middleware('auth');
    }

    public function index(Request $request)   
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @rownum=0'));
            $data = EmployeeEntitlement::leftjoin('employees','employeesentitlement.employee_id','=','employees.id')->leftjoin('companystructures','employeesentitlement.department_id','=','companystructures.id')->leftjoin('entitlement','employeesentitlement.entitlement_id','=','entitlement.id')->select('employeesentitlement.id','employeesentitlement.date_from','employeesentitlement.date_to','employeesentitlement.remarks','companystructures.title as department','entitlement.name as entitlement','employees.employee_id','employees.employee_code','employees.first_name','employees.middle_name','employees.last_name',DB::raw('@rownum  := @rownum  + 1 AS rownum'));
            return DataTables::eloquent($data)->make(true); 
        }
        $entitlements = Entitlement::get(['id', 'name']);         
        return view('Admin.finance.entitlementAssignment.index', get_defined_vars());    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'department_id' => 'required',
                'entitlement_id' => 'required',
                'date_from' => 'required',
                'date_to' => 'required',
                'remarks' => 'required'
            ]);

            $data = $request->all(); 
            EmployeeEntitlement::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        $data = EmployeeEntitlement::find($id);
        $employees = employees($data->department_id);
        return response()->json(['data' => $data, 'employees' => $employees]);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'department_id' => 'required',
                'entitlement_id' => 'required',
                'date_from' => 'required',
                'date_to' => 'required',
                'remarks' => 'required'
            ]);

            $EmployeeEntitlement = EmployeeEntitlement::find($id);
            $EmployeeEntitlement->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $EmployeeEntitlement = EmployeeEntitlement::find($id);
            $EmployeeEntitlement->deleted_by = Auth::id();
            $EmployeeEntitlement->update();
            $EmployeeEntitlement->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception $e){
            return ['code' => 500 , 'error_message' => $e->getMessage()]; 
        }
    }
}