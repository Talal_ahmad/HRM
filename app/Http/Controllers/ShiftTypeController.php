<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\ShiftType;
use Illuminate\Http\Request;
use App\Models\ShiftManagement;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ShiftTypeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return Datatables::eloquent(ShiftType::query())->addIndexColumn()->make(true);
        }

        return view('Admin.timeManagement.shiftTypes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'shift_id' => 'required',
                // 'rotating_order' => 'nullable|unique:shift_type',
                'active' => 'required',
                'shift_start_time' => 'required',
                'shift_end_time' => 'required',
                'shift_desc' => 'required',
            ]);
            $data = $request->all();
            if($request->has('open_working_hours_checkbox')){
                $data['open_working_hours_checkbox'] = 1;
            }
            $data['created_by'] = Auth::id();
            ShiftType::create($data);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=> $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ShiftType = ShiftType::find($id);
        return response()->json($ShiftType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try{
            $this->validate($request, [
                'shift_id' => 'required',
                // 'rotating_order' => 'nullable|unique:shift_type,rotating_order,'.$id,
                'active' => 'required',
                'shift_start_time' => 'required',
                'shift_end_time' => 'required',
                'shift_desc' => 'required',
            ]);
            $data  = $request->all();
            if($request->has('open_working_hours_checkbox')){
                $data['open_working_hours_checkbox'] = 1;
            }
            else{
                $data['open_working_hours_checkbox'] = 0;
            }

            if(!isset($request->sandwich_absent))
            {
                $data['sandwich_absent'] = 0;
            }
            if(!isset($request->is_24_hour_shift))
            {
                $data['is_24_hour_shift'] = 0;
            }
            $ShiftType = ShiftType::find($id);
            $ShiftType->fill($data)->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $ShiftType = ShiftType::find($id);
            $ShiftType->deleted_by = Auth::id();
            $shiftManagement = ShiftManagement::where('shift_id' , $id)->get();
            if(count($shiftManagement) > 0){
                return ['code'=>'300', 'message'=>'This Shift Type is Assigned to Some Employees Please change it first!'];
            }
            $ShiftType->update();
            $ShiftType->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
    public function get_employees_shift(Request $request)
    {
        $employees = Employee::where('status' , 'Active')->where('employees.id' , $request->emp_id)->select('employees.id' , 'employees.employee_id','employees.job_title','employees.employee_code', 'employees.first_name' , 'employees.last_name','employees.department')->get();

        if (env('COMPANY') == 'CLINIX') {
            $shift_type  = [];
            foreach ($employees as $employee) {
                $employeeId = $employee->id;
                $employeeJobTitle = $employee->job_title;
                $shift_type = ($employeeJobTitle == 39)
                    ? ShiftType::where('active', 1)
                    ->whereRaw('(TIMESTAMPDIFF(HOUR, shift_start_time, shift_end_time) >= 12
                                 OR (TIMEDIFF(shift_end_time, shift_start_time) < 0
                                     AND (24 + HOUR(shift_end_time) - HOUR(shift_start_time)) >= 12))')
                    ->get()
                    : ShiftType::where('active', 1)
                    ->where(function ($query) {
                        $query->whereRaw('IF(shift_start_time <= shift_end_time,
                                            TIMESTAMPDIFF(HOUR, shift_start_time, shift_end_time),
                                            TIMESTAMPDIFF(HOUR, shift_start_time, shift_end_time) + 24) BETWEEN 0 AND 10')
                            ->orWhere(function ($q) {
                                $q->whereRaw('TIME(shift_start_time) >= "14:00:00"')
                                    ->whereRaw('TIME(shift_end_time) = "00:00:00"');
                            });
                    })
                    ->get();
            }
        }else{
            $shift_type = ShiftType::where('active', 1)->get();
        }
        return response()->json($shift_type);
    }
}
