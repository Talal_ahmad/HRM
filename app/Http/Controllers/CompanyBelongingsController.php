<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\CompanyBelonging;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 

class CompanyBelongingsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = CompanyBelonging::select('id', 'name', 'serial_number', 'asset_tag', 'description');
            return Datatables::eloquent($data)->addIndexColumn()->make(true);
        }
        return view('Admin.finance.companyBelongings.index');  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required',
                'serial_number' => 'required',
                'b_limit' => 'required_with:end_page|integer|min:1|digits_between: 1,5',
                'asset_tag' => 'required'
            ]);

            $data = $request->all(); 
            CompanyBelonging::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $CompanyBelonging = CompanyBelonging::find($id);
        return response()->json($CompanyBelonging);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'name' => 'required',
                'serial_number' => 'required',
                'b_limit' => 'required_with:end_page|integer|min:1|digits_between: 1,5',
                'asset_tag' => 'required'
            ]);

            $CompanyBelonging = CompanyBelonging::find($id);
            $CompanyBelonging->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $company_belonging = CompanyBelonging::find($id);
            $company_belonging->deleted_by = Auth::user()->id;
            $company_belonging->update();

            $company_belonging->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message' => $e->getMessage()];
        }
    }


}