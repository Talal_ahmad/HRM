<?php

namespace App\Http\Controllers;

use DataTables;
use Carbon\Carbon;
use App\Models\Employee;
use App\Models\WorkWeek;
use App\Models\ShiftType;
use Illuminate\Http\Request;
use App\Models\CompanyStructure;
use Illuminate\Support\Facades\DB;

class AttendanceDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        if(!empty(request()->department)){
            $login_user_departs = implode(',',request()->department);
        }
        else
        {
            $login_user_departs = implode(',',login_user_departments());
        }
        $date = empty(request()->date) ? date('Y-m-d') : request()->date;
        $holiday = DB::table('holidays')->where('dateh', "$date")->first(['dateh']);

        // Total Employees
        $total_employees = Employee::where([
            ['status' , 'Active'],
            ['suspended' , 0]
        ])->whereIn('department' , login_user_departments());
        if(!empty(request()->department)){
            $total_employees->whereIn('department' , request()->department);
        }
        $total_employees = $total_employees->count();

        $on_roll = "";
        if(env('COMPANY') == 'JSML'){
            $on_roll= " AND employees.`seasonal_status` = 'On Roll'";
        }

        // Total Present
        $presentQuery = '';
        $missingCheckoutQuery = "AND DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date'";
        if(env('COMPANY') == 'JSML'){
            $previous_date = date('Y-m-d',strtotime($date.'-1 day'));
            $presentQuery = "OR attendance.id IN (SELECT MAX(id) AS a_id FROM attendance WHERE DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$previous_date' AND (shift_id = 6 || (shift_id = 10 AND time(attendance.in_time) >= '23:00:00')) GROUP BY employee )";
            $missingCheckoutQuery = "AND (
                (DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date') OR
                ((DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$previous_date' AND time(attendance.in_time) >= '23:00:00') AND time(attendance.in_time) <= '23:59:59')
            )";
        }
        $current_time = date('H:i:s');
        $total_present_employees = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,jobtitles.name AS designation,shift_type.shift_desc,employmentstatus.name AS employment,attendance.in_time,attendance.out_time,employees.first_name,employees.middle_name,employees.last_name,employees.work_phone,companystructures.title AS department
        FROM employees
        JOIN attendance ON attendance.employee = employees.id
        AND (attendance.id IN (SELECT MAX(id) AS a_id FROM attendance
        WHERE DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date' AND TIME(attendance.in_time) < '23:00:00' GROUP BY employee ) $presentQuery)
        JOIN companystructures ON companystructures.id = employees.department
        JOIN jobtitles ON jobtitles.id = employees.job_title
        JOIN employmentstatus ON employmentstatus.id = employees.employment_status
        LEFT JOIN shift_management ON shift_management.employee_id = employees.id
        AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
        WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date AND shift_management.status='Approved'
        GROUP BY employee_id))
        LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
        where employees.department IN ($login_user_departs)
        AND employees.`status` = 'Active'
        $on_roll
        AND employees.`suspended` = '0'
        ORDER BY companystructures.title ASC");

        // Total Absent
        $total_absent_employees = DB::select("SELECT employees.id
        FROM employees
        left join shift_management on shift_management.employee_id = employees.id
        AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
        WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
        GROUP BY employee_id))
        left join shift_type on shift_type.id = shift_management.shift_id
        WHERE employees.id NOT IN (SELECT employee FROM attendance where DATE_FORMAT(in_time,'%Y-%m-%d') = '$date')
        AND employees.id NOT IN (SELECT employee FROM employeeleaves
        where '$date' between date_start and date_end
        AND status = 'Approved')
        AND employees.id NOT IN (
            SELECT employee_id
            from shift_management
            join workdays on workdays.work_week_id = shift_management.work_week_id
            where workdays.status = 'Non-working Day'
            and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
            )
            and '$current_time' > IFNULL(shift_type.shift_start_time,time('$date 09:00:00'))
            AND department IN ($login_user_departs)
            AND employees.`status` = 'Active'
            $on_roll
            AND employees.`suspended` = '0'
            ");

        // Total LateCommers
        $grace_time_allowed = 11;
        $total_late_employees = DB::select("SELECT employees.id
        FROM employees
        JOIN attendance ON attendance.employee = employees.id
        AND (attendance.id IN (SELECT MIN(id) AS a_id FROM attendance
        WHERE DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date' GROUP BY employee ))
        -- JOIN companystructures ON companystructures.id = employees.department

        LEFT JOIN shift_management ON shift_management.employee_id = employees.id
        AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
        WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
        GROUP BY employee_id))
        LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id

        WHERE employees.department IN ($login_user_departs)
        AND employees.`status` = 'Active'
        AND employees.`suspended` = '0'
        AND (time(in_time) > IFNULL(shift_type.shift_start_time+INTERVAL shift_management.late_time_allowed MINUTE,time('2018-12-06 09:00:00')+INTERVAL '$grace_time_allowed' MINUTE) )");

        // Total Employees On Leave
        $total_leave_employees = DB::select("SELECT employees.id
        FROM employees
        -- join companystructures on companystructures.id = employees.department
        join employeeleaves on employeeleaves.employee = employees.id
        where '$date'  BETWEEN employeeleaves.date_start and employeeleaves.date_end
        AND employeeleaves.status = 'Approved'
        AND employees.department IN ($login_user_departs)
        AND employees.`status` = 'Active'
        AND employees.`suspended` = '0'
        group by employees.id");

        // Total Employees On Leave But Present
        $total_leave_present = DB::select("SELECT employees.id
        from employees
        join attendance on attendance.employee = employees.id
        -- join companystructures on companystructures.id = employees.department
        join employeeleaves on employeeleaves.employee = employees.id
        where '$date' BETWEEN employeeleaves.date_start and employeeleaves.date_end
        AND employeeleaves.status = 'Approved'
        and DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date'
        AND employees.department IN ($login_user_departs)
        AND employees.`status` = 'Active'
        AND employees.`suspended` = '0'
        group by employees.id");

        // Employees Non Working Day
        $total_on_off_day = DB::select("SELECT employees.id
        from employees
        -- join companystructures on companystructures.id = employees.department

        join shift_management on employees.id = shift_management.employee_id

        join workdays on workdays.work_week_id = shift_management.work_week_id
        where workdays.status = 'Non-working Day'
        and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
        AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                                GROUP BY employee_id))

        and employees.department IN ($login_user_departs)
        AND employees.`status` = 'Active'
        AND employees.`suspended` = '0'
        group by employees.id");

        // Employees Non Working Day But Present
        $total_on_off_day_present = DB::select("SELECT employees.id
                from employees
                JOIN attendance on attendance.employee = employees.id
                JOIN companystructures on companystructures.id = employees.department
                JOIN jobtitles ON jobtitles.id = employees.job_title
                JOIN employmentstatus ON employmentstatus.id = employees.employment_status
                LEFT JOIN shift_management on employees.id = shift_management.employee_id AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                    WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                    GROUP BY employee_id))
                LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                    WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                    GROUP BY employee_id))
                JOIN workdays  on workdays.work_week_id = shift_management.work_week_id
                where workdays.status = 'Non-working Day'
                and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
                and DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date'
                and employees.department IN ($login_user_departs)
                AND employees.`status` = 'Active'
                AND employees.`suspended` = '0'
                group by employees.id
                ORDER BY companystructures.title");

        // Employees Missing Checkouts
        $total_missing_checkout = DB::select("SELECT employees.id
        FROM employees
        LEFT JOIN attendance ON attendance.employee = employees.id
        WHERE attendance.out_time IS NULL
        $missingCheckoutQuery
        AND CONVERT(attendance.shift_end_time USING utf8mb4) < CONVERT('$current_time' USING utf8mb4)
        AND employees.department IN ($login_user_departs)
        AND employees.`status` = 'Active'
        AND employees.`suspended` = '0'
        group by employees.id");
        return view('Admin.attendanceDashboard.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

            if(!empty($request->departmentFilter)){
                $login_user_departs = implode(',',$request->departmentFilter);
                $dept_name = CompanyStructure::where('id' , $login_user_departs)->first()->title;
            }
            else
            {
                $login_user_departs = implode(',',login_user_departments());
                $dept_name = 'All Departments';
            }
            if(!empty($request->singleDate)){
                $date = $request->singleDate;
            }else{

                $date = empty(request()->query('date')) ? date('Y-m-d') : request()->query('date');
                // if(env('COMPANY') == 'JSML' && date('H:i',strtotime($shift_start_time)) == '00:00'){
                //     $date=date('Y-m-d',strtotime($date.'-1 day'));
                // }
            }
            $current_time = date('H:i:s');
            $filter = "";

            if(!empty(request()->employment_status)){
                $status = request()->employment_status;
                $filter = "AND employees.`employment_status` = '$status'";
            }
            if(!empty(request()->designation)){
                $job = request()->designation;
                $filter .= " AND employees.`job_title` = '$job'";
            }
            if(!empty(request()->shift_type)){
                $shift = request()->shift_type;
                $filter .= " AND shift_type.`id` = '$shift'";
            }
            $presentQuery = '';
            $missingCheckoutQuery = "AND DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date'";
            $column = 'in_time';
            if(env('COMPANY') == 'JSML'){
                $previous_date = date('Y-m-d',strtotime($date.'-1 day'));
                if(isset($shift) && request()->shift_type == 6){
                    $column = 'out_time';
                }
                else{
                    $column = 'in_time';
                }
                $presentQuery = "OR attendance.id IN (SELECT MAX(id) AS a_id FROM attendance WHERE DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$previous_date' AND (shift_id = 6 || (shift_id = 10 AND time(attendance.in_time) >= '23:00:00')) GROUP BY employee )";
                $missingCheckoutQuery = "AND (
                    (DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date') OR
                    ((DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$previous_date' AND time(attendance.in_time) >= '23:00:00') AND time(attendance.in_time) <= '23:59:59')
                )";
            }
            $holiday = DB::table('holidays')->where('dateh', "$date")->first(['dateh']);
            if(request()->query('type') == 'All Employees'){
                $query = Employee::select('employees.id','employees.employee_id','employees.employee_code','employees.first_name', 'employees.middle_name', 'employees.last_name' , 'jobtitles.name AS designation' , 'employmentstatus.name AS employment' , 'employees.work_phone','companystructures.title AS department','shift_type.shift_desc')
                ->join('companystructures', 'companystructures.id', '=', 'employees.department')
                ->join('employmentstatus' , 'employmentstatus.id' , '=' , 'employees.employment_status')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->leftjoin('shift_management', function ($join) use ($date) {
                        $join->on('employees.id', '=', 'shift_management.employee_id')
                        ->where([
                            ['shift_management.shift_from_date', '<=', $date],
                            ['shift_management.shift_to_date', '>=', $date]
                        ])
                        ->select(DB::raw('MAX(shift_management.id)'));
                    })
                ->leftjoin('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
                ->where([
                    ['employees.status' , 'Active'],
                    ['employees.suspended' , 0]
                ])->whereIn('employees.department' , login_user_departments());
                if(!empty(request()->query('departmentFilter'))){
                    $query->whereIn('employees.department' , request()->departmentFilter);
                }
                if(!empty(request()->employment_status)){
                    $query->where('employees.employment_status' , request()->employment_status);
                }
                if(!empty(request()->designation)){
                    $query->where('employees.job_title' , request()->designation);
                }
                if(!empty(request()->shift_type)){
                    $query->where('shift_type.id' , request()->shift_type);
                }
                $query->where(function ($query) use ($date) {
                    $query->where('employees.off_roll_date', '>=', $date)
                    ->orWhereNull('employees.off_roll_date');
                });
                $query->groupBy('employees.id')->orderBy('companystructures.title');
                $query = $query->get();
            }
            if(request()->query('type') == 'Present Employees'){
                $query =    DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,jobtitles.name AS designation,shift_type.shift_desc,employmentstatus.name AS employment,attendance.in_time,attendance.out_time,employees.first_name,employees.middle_name,employees.last_name,employees.work_phone,companystructures.title AS department
                FROM employees
                JOIN attendance ON attendance.employee = employees.id
                AND (attendance.id IN (SELECT MAX(id) AS a_id FROM attendance
                WHERE DATE_FORMAT(attendance.$column,'%Y-%m-%d') = '$date' AND TIME(attendance.in_time) < '23:00:00' GROUP BY employee ) $presentQuery)
                JOIN companystructures ON companystructures.id = employees.department
                JOIN jobtitles ON jobtitles.id = employees.job_title
                JOIN employmentstatus ON employmentstatus.id = employees.employment_status
                LEFT JOIN shift_management ON shift_management.employee_id = employees.id
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date AND shift_management.status='Approved'
                GROUP BY employee_id))
                LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
                where employees.department IN ($login_user_departs)
                $filter
                AND employees.`status` = 'Active'
                AND employees.`suspended` = '0'
                AND (employees.off_roll_date >= '$date' OR employees.off_roll_date IS NULL)
                ORDER BY companystructures.title ASC");
            }
            if(request()->query('type') == 'Absent Employees'){
                if(!empty($holiday))
                {
                    $query = [];
                }
                else
                {
                    $on_roll = "";
                    if(env('COMPANY') == 'JSML'){
                        $on_roll= " AND employees.`seasonal_status` = 'On Roll'";
                    }
                    $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,shift_type.shift_desc,jobtitles.name AS designation,employmentstatus.name AS employment,employees.first_name,employees.middle_name,employees.last_name,employees.work_phone,companystructures.title AS department
                    FROM employees
                    JOIN companystructures on companystructures.id = employees.department
                    JOIN jobtitles ON jobtitles.id = employees.job_title
                    JOIN employmentstatus ON employmentstatus.id = employees.employment_status
                    left join shift_management on shift_management.employee_id = employees.id
                    AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                    WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                    GROUP BY employee_id))
                    left join shift_type on shift_type.id = shift_management.shift_id
                    WHERE employees.id NOT IN (SELECT employee FROM attendance where DATE_FORMAT(in_time,'%Y-%m-%d') = '$date')
                    AND employees.id NOT IN (SELECT employee FROM employeeleaves
                    where '$date' between date_start and date_end
                    AND status = 'Approved')
                    AND (employees.off_roll_date >= '$date' OR employees.off_roll_date IS NULL)
                    AND employees.id NOT IN (
                        SELECT employee_id
                        from shift_management
                        join workdays on workdays.work_week_id = shift_management.work_week_id
                        where workdays.status = 'Non-working Day'
                        and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
                        )
                        and '$current_time' > IFNULL(shift_type.shift_start_time,time('$date 09:00:00'))
                        AND department IN ($login_user_departs)
                        $filter
                        AND employees.`status` = 'Active'
                        $on_roll
                        AND employees.`suspended` = '0'
                        ORDER BY companystructures.title");
                }
            }
            if(request()->query('type') == 'LateCommers Employees'){
                $grace_time_allowed = 11;
                $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,shift_type.shift_desc,attendance.in_time,attendance.out_time,jobtitles.name AS designation,employmentstatus.name AS employment,employees.first_name,employees.middle_name,employees.last_name,employees.work_phone,companystructures.title AS department,attendance.in_time,attendance.out_time,shift_type.shift_desc
                FROM employees
                JOIN attendance ON attendance.employee = employees.id
                JOIN jobtitles ON jobtitles.id = employees.job_title
                JOIN employmentstatus ON employmentstatus.id = employees.employment_status
                AND (attendance.id IN (SELECT MIN(id) AS a_id FROM attendance
                WHERE DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date' GROUP BY employee ))
                JOIN companystructures ON companystructures.id = employees.department

                LEFT JOIN shift_management ON shift_management.employee_id = employees.id
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                GROUP BY employee_id))
                LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id

                WHERE employees.department IN ($login_user_departs)
                $filter
                AND employees.`status` = 'Active'
                AND employees.`suspended` = '0'
                AND (time(in_time) > IFNULL(shift_type.shift_start_time+INTERVAL shift_management.late_time_allowed MINUTE,time('2018-12-06 09:00:00')+INTERVAL '$grace_time_allowed' MINUTE) )
                AND (employees.off_roll_date >= '$date' OR employees.off_roll_date IS NULL)
                ORDER BY companystructures.title");
            }
            if(request()->query('type') == 'OnLeave Employees'){
                $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,jobtitles.name AS designation,employmentstatus.name AS employment,employees.first_name,employees.middle_name,employees.last_name,employees.work_phone,companystructures.title AS department,shift_type.shift_desc
                FROM employees
                JOIN companystructures on companystructures.id = employees.department
                JOIN jobtitles ON jobtitles.id = employees.job_title
                JOIN employmentstatus ON employmentstatus.id = employees.employment_status
                join employeeleaves on employeeleaves.employee = employees.id
                JOIN shift_management ON shift_management.employee_id = employees.id
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                GROUP BY employee_id))
                JOIN shift_type ON shift_type.id = shift_management.shift_id
                where '$date'  BETWEEN employeeleaves.date_start and employeeleaves.date_end
                AND employeeleaves.status = 'Approved'
                AND employees.department IN ($login_user_departs)
                $filter
                AND employees.`status` = 'Active'
                AND employees.`suspended` = '0'
                AND (employees.off_roll_date >= '$date' OR employees.off_roll_date IS NULL)
                group by employees.id
                ORDER BY companystructures.title");
            }
            if(request()->query('type') == 'On Leave But Present Employees'){
                $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,jobtitles.name AS designation,employmentstatus.name AS employment,attendance.in_time,attendance.out_time,employees.first_name,employees.middle_name,employees.last_name,employees.work_phone,companystructures.title AS department,shift_type.shift_desc,attendance.in_time,attendance.out_time
                from employees
                join attendance on attendance.employee = employees.id
                join companystructures on companystructures.id = employees.department
                JOIN jobtitles ON jobtitles.id = employees.job_title
                JOIN employmentstatus ON employmentstatus.id = employees.employment_status
                join employeeleaves on employeeleaves.employee = employees.id
                LEFT JOIN shift_management ON shift_management.employee_id = employees.id
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                GROUP BY employee_id))
                LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
                where '$date' BETWEEN employeeleaves.date_start and employeeleaves.date_end
                AND employeeleaves.status = 'Approved'
                and DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date'
                AND employees.department IN ($login_user_departs)
                $filter
                AND employees.`status` = 'Active'
                AND employees.`suspended` = '0'
                AND (employees.off_roll_date >= '$date' OR employees.off_roll_date IS NULL)
                group by employees.id
                ORDER BY companystructures.title");
            }
            if(request()->query('type') == 'On OffDay Employees'){
                $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,jobtitles.name AS designation,employmentstatus.name AS employment,employees.first_name,employees.middle_name,employees.last_name,employees.work_phone,companystructures.title AS department,shift_type.shift_desc
                from employees
                join companystructures on companystructures.id = employees.department
                JOIN jobtitles ON jobtitles.id = employees.job_title
                JOIN employmentstatus ON employmentstatus.id = employees.employment_status
                LEFT JOIN shift_management on employees.id = shift_management.employee_id
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                GROUP BY employee_id))
                LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
                join workdays on workdays.work_week_id = shift_management.work_week_id
                where workdays.status = 'Non-working Day'
                and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                                        WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                                        GROUP BY employee_id))

                and employees.department IN ($login_user_departs)
                $filter
                AND employees.`status` = 'Active'
                AND employees.`suspended` = '0'
                AND (employees.off_roll_date >= '$date' OR employees.off_roll_date IS NULL)
                group by employees.id
                ORDER BY companystructures.title");
            }
            if(request()->query('type') == 'On OffDay Present Employees'){
                $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,jobtitles.name AS designation,attendance.in_time,attendance.out_time,employmentstatus.name AS employment,employees.first_name,employees.middle_name,employees.last_name,employees.work_phone,companystructures.title AS department,shift_type.shift_desc,attendance.in_time,attendance.out_time
                from employees
                JOIN attendance on attendance.employee = employees.id
                JOIN companystructures on companystructures.id = employees.department
                JOIN jobtitles ON jobtitles.id = employees.job_title
                JOIN employmentstatus ON employmentstatus.id = employees.employment_status
                LEFT JOIN shift_management on employees.id = shift_management.employee_id AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                    WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                    GROUP BY employee_id))
                LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                    WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                    GROUP BY employee_id))
                JOIN workdays  on workdays.work_week_id = shift_management.work_week_id
                where workdays.status = 'Non-working Day'
                and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
                and DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date'
                and employees.department IN ($login_user_departs)
                $filter
                AND employees.`status` = 'Active'
                AND employees.`suspended` = '0'
                AND (employees.off_roll_date >= '$date' OR employees.off_roll_date IS NULL)
                group by employees.id
                ORDER BY companystructures.title");
            }
            if(request()->query('type') == 'Missing Checkout Employees'){
                $query = DB::select("SELECT employees.id,employees.employee_id,
                employees.employee_code,jobtitles.name AS designation,employmentstatus.name AS employment,employees.first_name,attendance.in_time,attendance.out_time,employees.middle_name,employees.last_name,employees.work_phone,companystructures.title AS department,shift_type.shift_desc
                FROM employees
                LEFT JOIN attendance ON attendance.employee = employees.id
                JOIN companystructures on companystructures.id = employees.department
                JOIN jobtitles ON jobtitles.id = employees.job_title
                JOIN employmentstatus ON employmentstatus.id = employees.employment_status
                LEFT JOIN shift_management on employees.id = shift_management.employee_id
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                GROUP BY employee_id))
                LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
                WHERE attendance.out_time IS NULL
                $missingCheckoutQuery
                AND '$current_time' > attendance.shift_end_time
                AND employees.department IN ($login_user_departs)
                $filter
                AND employees.`status` = 'Active'
                AND employees.`suspended` = '0'
                AND (employees.off_roll_date >= '$date' OR employees.off_roll_date IS NULL)
                group by employees.id
                ORDER BY companystructures.title");
            }

        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        // $shift_types = DB::table('shift_type')->get(['id' , 'shift_desc']);
        $work_week = WorkWeek::get();
        $shift_type = ShiftType::select('id' ,'shift_desc' )->get();
        // dd($shift_type);
        return view('Admin.attendanceDashboard.dashboard_table' , get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $shift_types = DB::table('shift_type')->get(['id' , 'shift_desc']);
        return view('Admin.attendanceDashboard.dashboard_table' , get_defined_vars());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
