<?php

namespace App\Http\Controllers;

use App\Models\Disciplinarie;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Warning_rule;
use Illuminate\Support\Facades\DB;
use App\Models\News_Page;
use Auth;
use function PHPUnit\Framework\isEmpty;

class DisciplinariesController extends Controller
{
    public function index()
    {
        $rules = Warning_rule::where('status', 1)->get(); 
        $employees = Employee::where('status', 'Active')
            ->whereIn('employees.department' , login_user_departments())
            ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
            ->leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
            ->get(['employees.id', 'first_name', 'middle_name', 'last_name', 'employee_id', 'employee_code', 'companystructures.title as department', 'jobtitles.name as job_title']);
        return view('Admin.disciplinaries.index', compact('employees', 'rules'));
    }
    public function issue_disciplinaries(Request $request)
    {
        $request->validate([
            'employee_id' => 'required',
            'warning_type' => 'required',
            'warning_rule' => 'required',
        ]);
        $FilepathToStore = NULL;
        if ($request->hasFile('attachment_image')) {
            $fileNameExt = $request->file('attachment_image')->getClientOriginalName();
            $fileName = pathinfo($fileNameExt, PATHINFO_FILENAME);
            $fileExt = $request->file('attachment_image')->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '.' . $fileExt;
            $FilepathToStore = $request->file('attachment_image')->storeAs('Disciplinaries-Images_Attachments', $fileNameToStore);
            $FilepathToStore = "../storage/app/" . $FilepathToStore;
        }
        $departmentid = Employee::where('id', $request->employee_id)->pluck('department');
        $disciplinarie = new Disciplinarie;
        $disciplinarie->employee_id = $request->employee_id;
        $disciplinarie->warning_type = $request->warning_type;
        $disciplinarie->warning_rule = $request->warning_rule;
        $disciplinarie->attachment_image = $FilepathToStore;
        $disciplinarie->created_by = $request->created_by;
        $disciplinarie->message = $request->message;
        $disciplinarie->Issuer_name = $request->issuername;
        $disciplinarie->employee_department_id = $departmentid[0];
        $disciplinarie->watched_by=",";
        $disciplinarie->save();
        return redirect()->back();
    }
    public function disciplinariestable(Request $request)
    {
        $rules = Warning_rule::get();
        $employees = Employee::where('status', 'Active')->get(['id', 'first_name', 'middle_name', 'last_name']);
        $disciplinaries = Disciplinarie::leftjoin('employees', 'employees.id', '=', 'disciplinaries.employee_id')
            ->join('warning_rules', 'warning_rules.id', '=', 'disciplinaries.warning_rule')
            ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
            ->join('companystructures', 'companystructures.id', 'employees.department')
            ->select(
                'disciplinaries.*',
                'employees.employee_id',
                'jobtitles.name as designation',
                'employees.employee_code',
                'employees.first_name',
                'employees.middle_name',
                'warning_rules.rule',
                'disciplinaries.Issuer_name',
                'companystructures.title as employee_department',
                'employees.last_name'
            );
            if(userRoles()=="Self Service"){
                $disciplinaries->where('disciplinaries.employee_id',Auth::user()->employee);
            }
            $disciplinaries = $disciplinaries->get();
        return view('Admin.disciplinaries.disciplinariestable', get_defined_vars());
    }
    public function disciplinaries_edit($id)
    {
        $disciplinaries_edit = Disciplinarie::leftjoin('employees', 'employees.id', '=', 'disciplinaries.employee_id')
            ->join('warning_rules', 'warning_rules.id', '=', 'disciplinaries.warning_rule')
            ->select(
                'disciplinaries.*',
                'employees.employee_code',
                'employees.first_name',
                'employees.middle_name',
                'warning_rules.rule',
                'employees.last_name'
            )->where('disciplinaries.id', $id)->first();
        return response()->json($disciplinaries_edit);
    }
    public function update_disciplinaries(Request $request)
    {
        $request->validate([
            'employee_id' => 'required',
            'warning_type' => 'required',
            'warning_rule' => 'required',
        ]);
        $FilepathToStore = NULL;
        if ($request->hasFile('attachment_image')) {
            $fileNameExt = $request->file('attachment_image')->getClientOriginalName();
            $fileName = pathinfo($fileNameExt, PATHINFO_FILENAME);
            $fileExt = $request->file('attachment_image')->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '.' . $fileExt;
            $FilepathToStore = $request->file('attachment_image')->storeAs('Disciplinaries-Images_Attachments', $fileNameToStore);
            $FilepathToStore = "../storage/app/" . $FilepathToStore;
        }
        $disciplinarie = Disciplinarie::find($request->disciplinaries_id);
        $disciplinarie->employee_id = $request->employee_id;
        $disciplinarie->warning_type = $request->warning_type;
        $disciplinarie->warning_rule = $request->warning_rule;
        if (!empty($FilepathToStore)) {
            $disciplinarie->attachment_image = $FilepathToStore;
        } else {
            $disciplinarie->attachment_image = $disciplinarie->attachment_image;
        }
        $disciplinarie->message = $request->message;
        $disciplinarie->issuer_name = $request->issuername;
        $disciplinarie->save();
        return redirect()->back();
    }
    public function create_disciplinaries_rule(Request $request)
    {
        $request->validate([
            'rule' => 'required',
            'status' => 'required',
        ]);
        $object = new Warning_rule;
        $object->rule = $request->rule;
        $object->status = $request->status;
        $object->save();
        return redirect()->back();
    }


    public function disciplinaries_delete($id)
    {
        Disciplinarie::destroy($id);
        return redirect()->back();
    }
    public function disciplinariesRules()
    {
        $warning_rules = Warning_rule::get();
        return view('Admin.disciplinaries.rulesTable', compact('warning_rules'));
    }
    public function disciplinaries_Rules_edit($id)
    {
        $rules = Warning_rule::find($id);
        return response()->json($rules);
    }
    public function update_disciplinaries_rule(Request $request)
    {
        $object = Warning_rule::find($request->editruleid);
        $object->rule = $request->editrule;
        $object->status = $request->editstatus;
        $object->save();
        return redirect()->back();
    }
    public function disciplinaries_Rules_delete($id)
    {
        Warning_rule::destroy($id);
        return redirect()->back();
    }
    public function ChangeStatus(Request $request, $id)
    {
        $object = Disciplinarie::find($id);
        $object->watched_by = $object->watched_by.",".$request->user_id;
        $object->update();
        return redirect()->back();
    }
    public function add_reply(Request $request)
    {
        Disciplinarie::find($request->reply_disciplinaries_id)->update([
            'reply' => $request->reply
        ]);
        return redirect()->back();
    }
}
