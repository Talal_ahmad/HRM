<?php
namespace App\Http\Controllers;
use Auth;
use DataTables;
use App\Models\LoanType;
use Illuminate\Http\Request;
use App\Models\EmployeeCompanyLoan;
use Illuminate\Support\Facades\DB; 
use Illuminate\Validation\ValidationException;

class CompanyLoansController extends Controller
{
    public function __construct(){   
        $this->middleware('auth');
    }

    public function index(Request $request) 
    {
        if($request->ajax()){
            // Loan Type
            if($request->type == 'loan_type'){
                DB::statement(DB::raw('set @rownum=0'));
                $data = LoanType::select('id', 'name',DB::raw('@rownum  := @rownum  + 1 AS rownum'));
                return Datatables::eloquent($data)->addIndexColumn()->make(true); 
            }
            // Employee Loans 
            else{
                DB::statement(DB::raw('set @rownum=0'));
                $data = EmployeeCompanyLoan::leftjoin('employees','employeecompanyloans.employee','=','employees.id')->leftjoin('loan_types','employeecompanyloans.loan','=','loan_types.id')->leftjoin('currencytypes','employeecompanyloans.currency','=','currencytypes.id')->select('loan_types.name as loan','employeecompanyloans.id','employeecompanyloans.start_date','employeecompanyloans.period_months','employeecompanyloans.amount','employeecompanyloans.status','currencytypes.code as currency','employees.employee_id','employees.employee_code','employees.first_name','employees.last_name',DB::raw('@rownum  := @rownum  + 1 AS rownum'));
                return Datatables::of($data)->filter(function ($data) use ($request) {
                    if (!empty($request->employeeFilter)) {
                        $data->where('employeecompanyloans.employee', $request->employeeFilter);
                    }
                    if(!empty($request->loanTypeFilter)){
                        $data->where('employeecompanyloans.loan', $request->loanTypeFilter);
                    }
                },true)->addIndexColumn()->make(true);
            }
        } 

        $currencytypes = DB::table('currencytypes')->get(['id', 'code']);
        $loan_type = DB::table('loan_types')->get(['id', 'name']);   
        return view('Admin.finance.companyLoans.index', get_defined_vars());  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Store Loan Type
            if($request->type == 'loan_type')
            {
                $this->validate($request, [
                    'name' => 'required',
                ]);
                $LoanType = new LoanType;
                $LoanType->name = $request->name;
                $LoanType->save();
            }
            // Employee Loans
            else{
                $this->validate($request, [
                    'employee' => 'required',
                    'loan' => 'required',
                    'start_date' => 'required',
                    'period_months' => 'required',                    
                    'currency' => 'required',
                    'amount' => 'required',
                    'monthly_installment' => 'required',
                    'status' => 'required',
                ]);

                $data = $request->all();
                EmployeeCompanyLoan::create($data);
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        // Edit loan type
        if($request->type == 'loan_type'){
            $data = LoanType::find($id);
        }
        // Edit Employee Loans
        else
        {
            $data = EmployeeCompanyLoan::find($id);
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            //update loan Type
            if($request->type == 'loan_type'){
                $this->validate($request, [
                    'name' => 'required',
                ]);
                $LoanType = LoanType::find($id);
                $LoanType->fill($request->all())->save();
            }
            //update Employee Loans
            else
            {
                $this->validate($request, [
                    'employee' => 'required',
                    'loan' => 'required',
                    'start_date' => 'required',
                    'period_months' => 'required',                    
                    'currency' => 'required',
                    'amount' => 'required',
                    'monthly_installment' => 'required',
                    'status' => 'required',
                ]);
                $EmployeeCompanyLoan = EmployeeCompanyLoan::find($id);
                $EmployeeCompanyLoan->fill($request->all())->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e)
        {
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    { 
        try{
            if($request->type == 'loan_type'){
                $types = EmployeeCompanyLoan::where('loan', $id)->get();
                if(count($types) > 0)
                {
                    return ['code'=>'300', 'message'=>'This Loan Type is Assigned to Loans Please change it first!'];
                }
                $LoanType = LoanType::find($id);
                $LoanType->deleted_by = Auth::id();
                $LoanType->update();
                $LoanType->delete(); 
            }
            // Employee Loans
            else
            {
                $EmployeeCompanyLoan = EmployeeCompanyLoan::find($id);
                $EmployeeCompanyLoan->deleted_by = Auth::id();
                $EmployeeCompanyLoan->update();
                $EmployeeCompanyLoan->delete();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
