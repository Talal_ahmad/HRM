<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Auth;
use DataTables;
use Carbon\Carbon;
use App\Models\Payroll;
use App\Models\Employee;
use App\Models\CalculationGroup;
use App\Models\GlSetting;
use App\Helpers\EvalMath;
use App\Models\PayrollData;
use App\Models\CompanyStructure;
use Illuminate\Http\Request;
use App\Models\PayrollColumn;
use App\Models\PayrollHistory;
use App\Models\EmployeeSalary;
use App\Models\SalaryComponent;
use App\Models\Ledger;
use App\Models\EnvChecks;
use App\Models\PayslipTemplate;
use App\Traits\PayrollFunctions;
use App\Models\PayrollEmployee;
use App\Services\ErpIntegrationService;
use App\Exports\MultiplePayrollExport;
use App\Exports\PayrollSalarySheetExport;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\EmploymentStatus;
// use ElephantIO\Client;
// use ElephantIO\Engine\SocketIO\Version2x;

class PayrollProcessingController extends Controller
{
    use PayrollFunctions;
    public $progress_percentage = 0;

    public $socket_version;
    public $socket_client;

    public function __construct(){
        ini_set('max_execution_time', '0');
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $user_calculation_groups = !empty(Auth::user()->user_calculation_groups) ? json_decode(Auth::user()->user_calculation_groups) : [];
        if($request->ajax()){
            // Payroll Report
            if($request->type == 'payrollReports'){
                $query = Payroll::leftjoin('payfrequency','payroll.pay_period','=','payfrequency.id')
                ->leftjoin('companystructures','payroll.department','=','companystructures.id')
                ->leftjoin('users','payroll.process_by','=','users.id')
                ->whereIn('payroll.department', login_user_departments())
                ->whereIn('payroll.deduction_group', $user_calculation_groups)
                ->select('payroll.id','payroll.name','payroll.date_start','payroll.date_end','payroll.status','payfrequency.name AS payfrequency_name','companystructures.title AS department','payroll.ledgerised','payroll.erp_trans_no','payroll.payment','users.username as process_by','payroll.process_at');
                return Datatables::of($query)->addIndexColumn()->make(true);
            }
            // Payslip Template
            elseif($request->type == 'payslipTemplates'){
                return Datatables::eloquent(PayslipTemplate::query())->addIndexColumn()->make(true);
            }
            // Payroll History
            else{
                return Datatables::eloquent(PayrollHistory::join('companystructures','payroll_history.department','=','companystructures.id')
                ->whereIn('payroll_history.department' , login_user_departments())
                ->select('payroll_history.id','payroll_history.name as payroll','payroll_history.date_start','payroll_history.date_end','companystructures.title as department'))->make(true);
            }
        }
        $PayslipTemplate =  PayslipTemplate::get();
        $payrollColumns = PayrollColumn::get(['id', 'name']);
        $payFrequency = DB::table('payfrequency')->get();
        $calculationGroup = DB::table('deductiongroup')->whereIn('id', $user_calculation_groups)->get(['id', 'name']);
        $departments = CompanyStructure::get(['id', 'title']);
        return view('Admin.finance.payrollProcessing.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Payroll
            if($request->type == 'payroll')
            {
                $this->validate($request, [
                    'name' => 'required',
                    'pay_period' => 'required',
                    'deduction_group' => 'required',
                    'department' => 'required',
                    'date_start' => 'required',
                    'date_end' => 'required',
                    'columns' => 'required',
                    'status' => 'required',
                ]);

                $existing_payroll = Payroll::where('deduction_group', $request->deduction_group)->where('department', $request->department)->where('date_start', $request->date_start)->where('date_end', $request->date_end)->first();
                if(!empty($existing_payroll)){
                    return ['code'=>'500','error_message'=>"Payroll Already Exists! : #$existing_payroll->id"];
                }
                $data = $request->all();
                $data['columns'] = json_encode($request->columns);
                $payroll = Payroll::create($data);
                $employee_columns = array_keys(payrollEmployeeColumns());
                $newRequest = new Request(['employee_columns' => $employee_columns]);
                $this->storePayrollEmployeeColumns($newRequest,$payroll);
            }
            else{
                // Payslip Templates
                $this->validate($request, [
                    'name' => 'required',
                ]);
                $PayslipTemplate = new PayslipTemplate;
                $PayslipTemplate->name = $request->name;
                $PayslipTemplate->basic_salary = $request->basic_salary;
                $PayslipTemplate->gross_salary = $request->gross_salary;
                $PayslipTemplate->net_salary = $request->net_salary;
                $PayslipTemplate->add_columns = json_encode($request->allowance);
                $PayslipTemplate->deduct_columns = json_encode($request->deduction);
                $PayslipTemplate->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = request()->query('type');
        $payroll = Payroll::find($id);
        $prev_month_date = date("Y-m-d", strtotime("-1 months",strtotime($payroll->date_start)));
        $prev_payroll = Payroll::where('department', $payroll->department)->whereYear('date_start', date('Y',strtotime($prev_month_date)))->whereMonth('date_start', date('m',strtotime($prev_month_date)))->where('status', 'Completed')->first(['id']);
        if(!empty($prev_payroll)){
            $prevTotalEmployees = PayrollData::where('payroll',$prev_payroll->id)->groupBy('employee')->get(['employee']);
        }
        $columns = json_decode($payroll->columns);
        $payroll_columns = PayrollColumn::query();
        if(env('COMPANY') == 'Ajmal Dawakhana' && $type == 'pdf'){
            $payroll_columns->where('is_visible', 1);
        }
        else{
            if(env('COMPANY') != 'Ajmal Dawakhana'){
                $payroll_columns->where('is_visible', 1);
            }
        }
        if(request()->query('type') == 'employers_column'){
            $payroll_columns = $payroll_columns->whereIn('id', $columns)->where('employers_column', 1)->orderBy('colorder', 'asc')->get(['id', 'name', 'comma_seprated' , 'default_value' , 'round_off']);
        }
        else{
            $payroll_columns = $payroll_columns
            ->whereIn('id', $columns)
            ->where(function ($query) {
                $query->where('employers_column', '!=', 1)
                    ->orWhereNull('employers_column');
            })
            ->orderBy('colorder', 'asc')
            ->get(['id', 'name', 'comma_seprated', 'default_value', 'round_off']);
        }
        $all_childs =  $this->getChildren($payroll->department);
        array_push($all_childs,$payroll->department);
        $employees = Employee::join('payrolldata', 'payrolldata.employee', '=', 'employees.id')
                            ->leftjoin('paygrades','paygrades.id','=','employees.pay_grade')
                            ->join('companystructures', 'companystructures.id', '=', 'payrolldata.department_id')
                            // ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                            // ->join('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                            ->where('payrolldata.payroll', $payroll->id);
                            // if(env('COMPANY') != 'RoofLine')
                            // {
                            //     $employees->whereIn('employees.department', $all_childs);
                            // }
                            $employees = $employees->select('employees.id', 'employees.first_name' , 'employees.middle_name' , 'employees.last_name', 'employees.employee_id', 'employees.employee_code','employees.nic_num','payrolldata.department as department_name','payrolldata.job_title','payrolldata.employement_status','employees.off_roll_date','paygrades.name as paygrade_name','paygrades.min_salary','paygrades.max_salary','payrolldata.emp_active_inactive','companystructures.type')->groupBy('payrolldata.employee')->get()
                            ->reject(function($payroll_employees,$key) use ($payroll) {
                                if(env('COMPANY') == 'JSML'){
                                    if(!empty($payroll_employees->off_roll_date))
                                    {
                                        return $payroll_employees->off_roll_date <= $payroll->date_start;
                                    }
                                }
                            });
        $total_columns = PayrollData::join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                                    ->where([
                                        ['payrolldata.payroll', $payroll->id],
                                        ['payrollcolumns.is_visible', 1],
                                        ['payrollcolumns.total_column', 1]
                                    ])->groupBy('payrollcolumns.id')->orderBy('colorder', 'asc')->get(['payrollcolumns.id', 'payrollcolumns.name',DB::raw('SUM(payrolldata.amount) as net_total')]);
        foreach ($total_columns as $key => $total) {
            $net_total[$total->id] = $total->net_total;
        }
        foreach ($employees as $key => $employee) {
            foreach ($payroll_columns as $key => $column) {
                $payroll_data = PayrollData::where([
                    ['payroll', $id],
                    ['employee', $employee->id],
                    ['payroll_item', $column->id]
                ])->first(['amount',DB::raw('SUM(amount) as net_total')]);

                $col_amount = !empty($payroll_data) ? $payroll_data->amount : $column->default_value;
                if($column->comma_seprated == 1){
                    if(env('COMPANY') == 'JSML'){
                        // For Total Days as they can be in decimal
                        if(in_array($column->id, [125, 455])){
                            $column_amount[$employee->id][$column->id] = $col_amount;
                        }
                        else{
                            $column_amount[$employee->id][$column->id] = number_format($col_amount);
                        }
                    }
                    else{
                        $column_amount[$employee->id][$column->id] = number_format($col_amount);
                    }
                }
                else{
                    $column_amount[$employee->id][$column->id] = $col_amount;
                }
                if($column->round_off == 1){
                    // $col_amount = !empty($payroll_data) ? round($payroll_data->amount,2) : $column->default_value;
                    $column_amount[$employee->id][$column->id] = round($col_amount);
                }
                if($column->round_off == 2){
                    $column_amount[$employee->id][$column->id] = round($col_amount,2);
                }
            }
        }
        if($type == 'pdf')
        {   $employee_columns = $payroll->employeeColumns()->select('name', 'display_name')->get();
            if(env('COMPANY') == 'CLINIX')
            {
                if(!empty($prev_payroll)){
                    $prev_total_columns = PayrollData::join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                            ->where([
                                ['payrolldata.payroll', $prev_payroll->id],
                                ['payrollcolumns.is_visible', 1],
                                ['payrollcolumns.total_column', 1]
                            ])->groupBy('payrollcolumns.id')->orderBy('colorder', 'asc')->get(['payrollcolumns.id', 'payrollcolumns.name',DB::raw('SUM(payrolldata.amount) as net_total')]);
                    foreach ($prev_total_columns as $key => $prev_total) {
                        $prev_net_total[$prev_total->id] = $prev_total->net_total;
                    }
                }
                $pdf = PDF::loadView('Admin.finance.payrollProcessing.clinix_salary_sheet_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
                return $pdf->download($payroll->name.'.pdf');
            }
            else
            {
                $salary_font = EnvChecks::join('env_name', 'env_name.id', '=', 'meta_data.name')->where('env_name.name','Payroll Font Size PDF')->where('key',1)->first();
                $pdf = PDF::loadView('Admin.finance.payrollProcessing.salary_sheet_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
                return $pdf->download($payroll->name.'.pdf');
            }
        }
        if($type == 'excel')
        {
            $employee_columns = $payroll->employeeColumns()->select('name', 'display_name')->get();
            if(env('COMPANY') == 'CLINIX')
            {
                if(!empty($prev_payroll)){
                    $prev_total_columns = PayrollData::join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                            ->where([
                                ['payrolldata.payroll', $prev_payroll->id],
                                ['payrollcolumns.is_visible', 1],
                                ['payrollcolumns.total_column', 1]
                            ])->groupBy('payrollcolumns.id')->orderBy('colorder', 'asc')->get(['payrollcolumns.id', 'payrollcolumns.name',DB::raw('SUM(payrolldata.amount) as net_total')]);
                    foreach ($prev_total_columns as $key => $prev_total) {
                        $prev_net_total[$prev_total->id] = $prev_total->net_total;
                    }
                }
                return Excel::download(new PayrollSalarySheetExport($payroll, $column_amount, $employee_columns, $payroll_columns, $employees, $net_total, $salary_font = '', $prev_total_columns, $prev_net_total), 'Salary Sheet.xlsx');
            }
            else
            {
                $salary_font = EnvChecks::join('env_name', 'env_name.id', '=', 'meta_data.name')->where('env_name.name','Payroll Font Size PDF')->where('key',1)->first();
                return Excel::download(new PayrollSalarySheetExport($payroll, $column_amount, $employee_columns, $payroll_columns, $employees, $net_total, $salary_font), 'Salary Sheet.xlsx');
            }
        }
        return view('Admin.finance.payrollProcessing.view', get_defined_vars());
    }

    public function downloadMultiple(Request $request)
    {
        $columns = [];
        $payroll_ids = [];
        $payroll_data = [];
        $month = date('m' , strtotime($request->month));
        $year = date('Y' , strtotime($request->month));
        $payrolls = Payroll::whereMonth('date_start',$month)->whereYear('date_start',$year)->whereIn('department', $request->departments)->get(['id','name','columns']);
        foreach ($payrolls as $key => $payroll) {
            $payroll_ids[] = $payroll->id;
            $columns = json_decode($payroll->columns);
        }
        $payroll_columns = PayrollColumn::where('is_visible', 1)->whereIn('id', $columns)->orderBy('colorder', 'asc')->get(['id', 'name']);

        $employees = Employee::join('payrolldata', 'payrolldata.employee', '=', 'employees.id')
            ->join('companystructures', 'companystructures.id', '=', 'payrolldata.department_id')
            // ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
            // ->join('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
            // ->whereIn('employees.status', ['Active','Suspended'])
            ->whereIn('payrolldata.payroll', $payroll_ids)
            ->whereIn('payrolldata.department_id', $request->departments)
            ->select('employees.id', 'employees.first_name', 'employees.last_name', 'employees.employee_id', 'employees.employee_code','employees.nic_num','payrolldata.department as department_name','payrolldata.job_title','payrolldata.employement_status','payrolldata.emp_active_inactive','companystructures.type')->groupBy('payrolldata.employee')->get();
            $total_columns = PayrollData::join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                ->whereIn('payrolldata.payroll', $payroll_ids)
                ->where([
                    ['payrollcolumns.is_visible', 1],
                    ['payrollcolumns.total_column', 1]
                ])->groupBy('payrollcolumns.id')->orderBy('colorder', 'asc')->get(['payrollcolumns.id', 'payrollcolumns.name',DB::raw('SUM(payrolldata.amount) as net_total')]);
            foreach ($total_columns as $key => $total) {
                $net_total[$total->id] = $total->net_total;
            }
        foreach ($employees as $key => $employee) {
            $payroll_data = PayrollData::leftJoin('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                            ->leftJoin('payroll', 'payroll.id', '=', 'payrolldata.payroll')
                            ->leftJoin('employees', 'employees.id', '=', 'payrolldata.employee')
                            ->whereIn('payrolldata.payroll', $payroll_ids)
                            ->where([
                                ['employees.id', $employee->id],
                                ['payrollcolumns.is_visible',1]
                            ])->orderBy('colorder', 'asc')->get(['payrolldata.amount', 'payrollcolumns.comma_seprated' , 'payrollcolumns.default_value','payrollcolumns.round_off','payrollcolumns.id as column_id']);
            foreach ($payroll_data as $key => $data) {
                // if($data->comma_seprated == 1)
                // {
                //     if($data->round_off == 2)
                //     {
                //         $column_amount[$employee->id][$data->column_id] = number_format($data->amount,2);
                //     }
                //     else
                //     {
                //         $column_amount[$employee->id][$data->column_id] = number_format($data->amount);
                //     }
                // }
                // else
                // {
                    $column_amount[$employee->id][$data->column_id] = $data->amount;
                // }
            }
        }
        return Excel::download(new MultiplePayrollExport($payroll_columns,$employees,$payroll_data,$column_amount,$net_total), 'Salary Sheet.xlsx');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        // Payroll
        if($request->type == 'payroll'){
            $Payroll = Payroll::find($id);
            return response()->json($Payroll);
        }
        // Payslip Template
        else{
            $PayslipTemplate = PayslipTemplate::find($id);
            return response()->json($PayslipTemplate);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            if($request->type == 'payroll'){
                $this->validate($request, [
                    'name' => 'required',
                    'pay_period' => 'required',
                    'deduction_group' => 'required',
                    'department' => 'required',
                    'date_start' => 'required',
                    'date_end' => 'required',
                    'columns' => 'required',
                    'status' => 'required',
                ]);
                $Payroll = Payroll::find($id);
                $data = $request->all();
                if($request->status == 'Draft'){
                    Ledger::where('payroll_id',$id)->whereIn('type',['payroll_accrual', 'pf_accrual'])->delete();
                    Ledger::where('payroll_id',$id)->where('type','salary_payment')->delete();
                    $data['ledgerised'] = 0;
                    $data['payment'] = 0;
                }
                $data['columns'] = json_encode($request->columns);
                $Payroll->fill($data)->save();
            }
            else{
                $Payslip = PayslipTemplate::find($id);
                $data = $request->all();
                $data['add_columns'] = json_encode($request->allowance);
                $data['deduct_columns'] = json_encode($request->deduction);
                $Payslip->fill($data)->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        // Payroll
        if($request->type=='payroll'){
        try{
            $payroll = Payroll::find($id);
            $payroll->deleted_by = Auth::user()->id;
            $payroll->update();
            $payroll->delete();
            return ['code'=>'200','message'=>'success'];
        }

        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
        // Payslip Template
        elseif($request->type == 'payslipTemplates'){
            try{
                PayslipTemplate::find($id)->delete();

                return ['code'=>'200','message'=>'success'];
            }
            catch(\Exception $e){
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }

        // Payroll Report
        else{
            try{
                Payroll::find($id)->delete();

                return ['code'=>'200','message'=>'success'];
            }
            catch(\Exception $e){
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    public function PayrollAccrualJV(Request $request)
    {
        try {
            $payroll = Payroll::find($request->payroll_id);
            if(env('COMPANY') == 'JSML'){
                ErpIntegrationService::ProcessPayrollAccrualJvToErp($payroll);
            }
            return ['code'=>'200','message'=>'Entry Has Been Sent Successfully!'];
        } catch (\Throwable $th) {
            if (get_class($th) !== \Exception::class) {
                return ['code'=>'500','message'=> 'Please Contact Support Team!'];
            } else {
                return ['code'=>'500','message'=>$th->getMessage()];
            }
        }
    }

    public function checkPayrollAccrualJV(Request $request)
    {
        DB::beginTransaction();
        try {
            $payroll = Payroll::find($request->payroll_id);
            if($payroll->ledgerised !=1 || $request->reprocess == 1){
                $payrollemployees = DB::select("SELECT payroll.name,payrolldata.employee as employee_id,employees.first_name,employees.last_name,employees.job_title as designation_id,employees.employee_code,employees.bank_name,employees.account_title,employees.account_number,jobtitles.name as designation,payrolldata.department_id,companystructures.title as department_name,payrollcolumns.id as column_id,payrollcolumns.name as column_name,payrolldata.amount,CONCAT(payroll.date_start,' - ',payroll.date_end) as payroll_duration,employees.employment_status, employees.joined_date,employees.eobi_status,employees.pessi,employees.provident_fund,payroll.date_end
                from payroll
                join payrolldata on payrolldata.payroll = payroll.id
                join employees on payrolldata.employee = employees.id
                join companystructures on employees.department = companystructures.id
                join jobtitles on employees.job_title = jobtitles.id
                join payrollcolumns on payrolldata.payroll_item = payrollcolumns.id
                where payroll.id  = $request->payroll_id");
                $balance = 0;
                $employeeID = '';
                if(count($payrollemployees) > 0)
                {
                    Ledger::where('payroll_id',$request->payroll_id)->whereIn('type',['payroll_accrual', 'pf_accrual'])->delete();
                    foreach (array_chunk($payrollemployees, 2000) as $chunk) {
                        $records = [];
                        foreach ($chunk as $employee) {
                            $setting = GlSetting::whereRaw("(JSON_SEARCH(employment_status_id, 'one', $employee->employment_status) IS NOT NULL AND JSON_SEARCH(department_id, 'one', $employee->department_id) IS NOT NULL AND JSON_SEARCH(payroll_column_id, 'one', $employee->column_id) IS NOT NULL) AND (`category` = 'payroll_accrual' OR `category` = 'pf_accrual')")->get();
                            if(count($setting) > 0){
                                foreach ($setting as $sett) {
                                    $employeeID != $employee->employee_id ? $balance = 0 : '';
                                    $employeeID = $employee->employee_id;
                                    if($sett->column_type == 'credit'){
                                        $amount = -$employee->amount;
                                    }
                                    else{
                                        $amount = $employee->amount;
                                    }
                                    $balance += $amount;
                                    $records[] = [
                                        'gl_setting_id' => $sett->id,
                                        'payroll_id' => $request->payroll_id,
                                        'entry_of_month' => date('F Y', strtotime($employee->date_end)),
                                        'name' => $employee->name,
                                        'employee_id' => $employee->employee_id,
                                        'employee' => "{$employee->employee_code}-{$employee->first_name} {$employee->last_name}",
                                        'designation_id' => $employee->designation_id,
                                        'designation' => $employee->designation,
                                        'department_id' => $employee->department_id,
                                        'department' => $employee->department_name,
                                        'employement_status_id' => $employee->employment_status,
                                        'employee_code' => $employee->employee_code,
                                        'employee_joining_date' => $employee->joined_date,
                                        'employee_eobi_status' => $employee->eobi_status,
                                        'employee_pessi_status' => $employee->pessi,
                                        'employee_provident_fund_status' => $employee->provident_fund,
                                        'employee_bank' => $employee->bank_name,
                                        'account_title' => $employee->account_title,
                                        'account_number' => $employee->account_number,
                                        'type' => $sett->category,
                                        'setting_type' => $sett->type,
                                        'column_id' => $employee->column_id,
                                        'column_name' => $employee->column_name,
                                        'gl_account_type' => $sett->column_type,
                                        'gl_account' => $sett->gl_accounts,
                                        'amount' => $amount,
                                        'balance' => $balance,
                                        'payroll_duration' => $employee->payroll_duration,
                                        'user_id' => Auth::id(),
                                        'trans_date' => $employee->date_end,
                                        'accrual_date' => $employee->date_end,
                                        'created_at' => now(),
                                    ];
                                }
                            }
                        }
                        Ledger::insert($records);
                    }
                    $payroll->ledgerised = 1;
                    $payroll->update();
                    if($payroll->payment == 1){
                        Ledger::where('payroll_id',$payroll->id)->where('type','salary_payment')->delete();
                    }
                    DB::commit();
                }
                else
                {
                    return ['code'=>'404','message'=>'No Record Found!'];
                }
            }
            $result = $this->viewAccrualJv($payroll);
            return ['code'=>'200','message'=>'Ledger Has Been Successfully Made!', 'result' => $result];
        } catch (\Throwable $th) {
            DB::rollback();
            if (get_class($th) !== \Exception::class) {
                return ['code'=>'500','message'=> 'Please Contact Support Team!'];
            } else {
                return ['code'=>'500','message'=>$th->getMessage()];
            }
        }
    }

    private function viewAccrualJv($payroll)
    {
        $accounts_sum = Ledger::where('payroll_id', $payroll->id)
            ->groupBy('gl_account')
            ->groupBy('gl_account_type')
            ->groupBy('column_id')
            ->select(\DB::raw('COUNT(*) as total_employees'), 'gl_account_type', 'gl_account', \DB::raw('SUM(amount) as total_amount'),'column_name')
            ->get();
        return $accounts_sum;
    }

    private function viewAccrualPayment($voucher_no)
    {
        $employees = Ledger::select('employee', 'designation', 'payment_banks as bank', 'allocation', 'entry_of_month', 'voucher_date', 'payment_voucher_no')
            ->where('type', 'salary_payment')
            ->where('payment_voucher_no', $voucher_no)
            ->get();
        return $employees;
    }

    public function deletePayrollAccrualJV(Payroll $payroll){
        DB::beginTransaction();
        try {
            Ledger::where('payroll_id',$payroll->id)->whereIn('type',['payroll_accrual', 'pf_accrual'])->delete();
            if(env('COMPANY') == 'JSML'){
                ErpIntegrationService::voidPayrollAccrualJv($payroll);
            }
            $payroll->update([
                'ledgerised' => 0,
                'erp_trans_no' => null,
                'erp_reference' => null,
            ]);
            DB::commit();
            return ['code'=>'200','message'=>'Accrual Jv Has Been Deleted Successfully!'];
        }
        catch (\Throwable $th) {
            DB::rollback();
            if (get_class($th) !== \Exception::class) {
                return ['code'=>'500','message'=> 'Please Contact Support Team!'];
            } else {
                return ['code'=>'500','message'=>$th->getMessage()];
            }
        }
    }

    public function processPayroll(Request $request, $payroll_id)
    {
        set_time_limit(0);
        // DB::beginTransaction();
        try {
            $meta_info = $request->metainfo;
            $eval_math = new EvalMath();

            if (isset(request()->employee_id)) {
                PayrollData::where('payroll', $payroll_id)->where('employee', request()->employee_id)->delete();
            } else {
                if($meta_info == 'existing'){
                    $existing_employees =  PayrollData::where('payroll', $payroll_id)->pluck('employee')->toArray();
                }
                PayrollData::where('payroll', $payroll_id)->delete();
            }
            $payroll = DB::select("SELECT *,month(date_start) as payroll_month,year(date_start) as payroll_year,deduction_group from payroll where id = '$payroll_id'")[0];
            $all_childs = $this->getChildren($payroll->department);
            array_push($all_childs, $payroll->department);
            if (isset(request()->employee_id)) {
                $payroll_employees = Employee::join('companystructures', 'companystructures.id', '=', 'employees.department')
                    ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                    ->join('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                    ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.job_title', 'employees.employment_status', 'employees.joined_date', 'employees.department', 'employees.suspension_date', 'employees.restore_date', 'employees.account_title', 'employees.bank_id','employees.bank_name', 'employees.account_number', 'employees.birthday', 'employees.provident_fund', 'employees.eobi_status','employees.pessi','employees.confirmation_date','employees.job_title','employees.active','companystructures.title','jobtitles.name as jobtitlesName','employmentstatus.name as employmentstatusName')->where('employees.id', request()->employee_id)->get();
            } else {
                if(env('COMPANY') == 'RoofLine' || env('COMPANY') == 'Ajmal Dawakhana')
                {
                    if(env('COMPANY') == 'RoofLine'){
                        $payroll_employees = Employee::join('payrollemployees', 'payrollemployees.employee', '=', 'employees.id')
                        ->join('companystructures', 'companystructures.id', '=', 'employees.department')
                        ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                        ->join('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                        ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.job_title', 'employees.employment_status', 'employees.joined_date', 'employees.department', 'employees.suspension_date', 'employees.restore_date', 'employees.account_title', 'employees.bank_name','employees.bank_id', 'employees.account_number', 'employees.birthday', 'employees.active','employees.provident_fund', 'employees.eobi_status','employees.pessi','employees.termination_date','employees.confirmation_date','companystructures.title','jobtitles.name as jobtitlesName','employmentstatus.name as employmentstatusName',DB::raw('Month(employees.termination_date) as termination_month, Year(employees.termination_date) as termination_year'));
                        if($meta_info == 'existing'){
                            $payroll_employees = $payroll_employees
                            ->whereIn('employees.id', $existing_employees)->get();
                        }
                        else{
                            $payroll_employees = $payroll_employees
                            ->where('employees.joined_date', '<', $payroll->date_end)
                            ->where('payrollemployees.deduction_group', $payroll->deduction_group)
                            ->whereNull('payrollemployees.deleted_at')->get()
                            ->reject(function($payroll_employees,$key) use ($payroll) {
                                if(!empty($payroll_employees->termination_date) && $payroll_employees->termination_date != '0000-00-00')
                                {
                                    return $payroll_employees->termination_date < $payroll->date_start;
                                }
                            });
                        }
                    }
                    if(env('COMPANY') == 'Ajmal Dawakhana'){
                        $payroll_employees = Employee::join('payrollemployees', 'payrollemployees.employee', '=', 'employees.id')
                        ->join('companystructures', 'companystructures.id', '=', 'employees.department')
                        ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                        ->join('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                        ->select('employees.id', 'employees.employee_id', 'employees.active', 'employees.employee_code', 'employees.job_title', 'employees.employment_status', 'employees.joined_date', 'employees.department', 'employees.suspension_date', 'employees.restore_date', 'employees.account_title', 'employees.bank_name','employees.bank_id', 'employees.account_number', 'employees.birthday', 'employees.provident_fund', 'employees.eobi_status','employees.pessi','employees.termination_date','employees.confirmation_date','companystructures.title','jobtitles.name as jobtitlesName','employmentstatus.name as employmentstatusName',DB::raw('Month(employees.termination_date) as termination_month, Year(employees.termination_date) as termination_year'));
                        if($meta_info == 'existing'){
                            $payroll_employees = $payroll_employees
                            ->whereIn('employees.id', $existing_employees)->get();
                        }
                        else{
                            $payroll_employees = $payroll_employees->where('employees.joined_date', '<', $payroll->date_end)
                            ->where('employees.status', '!=', 'Terminated')
                            ->where('payrollemployees.deduction_group', $payroll->deduction_group)
                            ->whereNull('payrollemployees.deleted_at')
                            ->get();
                        }
                    }
                }
                else{
                    $payroll_employees = Employee::select('employees.id','employees.first_name','employees.last_name','employees.employee_id','employees.employee_code','employees.job_title','employees.employment_status','employees.joined_date','employees.department','employees.suspension_date','employees.restore_date','employees.account_title','employees.bank_id','employees.bank_name','employees.account_number','employees.birthday','employees.provident_fund','employees.eobi_status','employees.pessi','employees.confirmation_date','employees.job_title as jobtitlesName','employees.active','companystructures.title','jobtitles.name as jobtitlesName','employmentstatus.name as employmentstatusName','employees.off_roll_date','employees.on_roll_date')
                    ->join('companystructures', 'companystructures.id', '=', 'employees.department')
                    ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                    ->join('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status');
                    if($meta_info == 'existing'){
                        $payroll_employees = $payroll_employees
                        ->whereIn('employees.id', $existing_employees)->get();
                    }
                    else{
                        $payroll_employees = $payroll_employees->whereIn('employees.department', $all_childs)
                        ->where('employees.joined_date', '<=', $payroll->date_end)
                        ->where(function ($query) use ($payroll) {
                            $query->where('employees.suspension_date', '>', $payroll->date_start)
                                ->orWhereNull('employees.suspension_date');
                        });
                        if(env('COMPANY') == 'CLINIX' || env('COMPANY') == 'HEALTHWISE'){
                            $statuses = env('COMPANY') == 'CLINIX' ? ['Active', 'Suspended'] : ['Active'];
                            $payroll_employees->whereIn('employees.status', $statuses);
                        }
                        else{
                            $payroll_employees->where(function ($query) use ($payroll) {
                                $query->where('employees.termination_date', '>', $payroll->date_start)
                                    ->orWhereNull('employees.termination_date');
                            });
                            if(env('COMPANY') == 'JSML'){
                                $payroll_employees->where(function ($query) use ($payroll) {
                                    $query->where('employees.restore_date', '<=', $payroll->date_end)
                                    ->orWhereNull('employees.restore_date');
                                });
                                $payroll_employees->where(function ($query) use ($payroll) {
                                    $query->where('employees.off_roll_date', '>=', $payroll->date_start)
                                    // ->where('employees.off_roll_date', '<', $payroll->date_end)
                                    ->orWhereNull('employees.off_roll_date');
                                });
                                $payroll_employees->where(function ($query) use ($payroll) {
                                    $query->where('employees.on_roll_date', '<=', $payroll->date_end)
                                    ->orWhereNull('employees.on_roll_date');
                                });
                            }
                        }
                        $payroll_employees = $payroll_employees->get();
                    }
                }
            }
            if (count($payroll_employees) == 0) {
                return ['code'=>'404','message'=>'No Employee Found!'];
            }
            $progress_percentage_increment = 100 / count($payroll_employees);
            $columns = json_decode($payroll->columns, true);
            $single_payroll_columns = PayrollColumn::where('column_type', 'single')->whereIn('id', $columns)->get();
            $salary_component_payroll_columns = PayrollColumn::where('column_type', 'salary_component')->whereIn('id', $columns)->get();
            $recursive_payroll_columns = PayrollColumn::where('column_type', 'recursive')->whereIn('id', $columns)->orderBy('colorder', 'ASC')->get();
            $last_payroll_columns = PayrollColumn::where('column_type', 'last')->whereIn('id', $columns)->get();
            $calculation_columns = PayrollColumn::where('column_type', 'calculation')->whereIn('id', $columns)->orderBy('colorder', 'ASC')->get();

            foreach ($payroll_employees as $employee) {
                try{
                    if(env('COMPANY') == 'RoofLine' || env('COMPANY') == 'Ajmal Dawakhana'){
                        $payroll_employee = PayrollEmployee::where('employee', $employee->id)->pluck('deduction_group')->toArray();
                        if (count($payroll_employee) == 0) {
                            continue;
                        }
                        if (!in_array($payroll->deduction_group,$payroll_employee)) {
                            continue;
                        }
                    }
                    else{
                        $payroll_employee = PayrollEmployee::where('employee', $employee->id)->first();
                        if (!$payroll_employee) {
                            continue;
                        }
                        if ($payroll->deduction_group != $payroll_employee->deduction_group) {
                            continue;
                        }
                    }
                    if(count($single_payroll_columns) > 0){
                        foreach ($single_payroll_columns as $column) {
                            $payroll_data = new PayrollData;
                            $payroll_data->payroll = $payroll_id;
                            $payroll_data->employee = $employee->id;
                            $payroll_data->emp_active_inactive = $employee->active == 1 ? 'Active' : 'Inactive';
                            $payroll_data->department_id = $employee->department;
                            $payroll_data->employment_status_id = $employee->employment_status;
                            $payroll_data->job_title_id = $employee->job_title;
                            $payroll_data->department = $employee->title;
                            $payroll_data->employement_status = $employee->employmentstatusName;
                            $payroll_data->job_title = $employee->jobtitlesName;
                            $payroll_data->bank_id = $employee->bank_id;
                            $payroll_data->bank_name = $employee->bank_name;
                            $payroll_data->account_number = $employee->account_number;
                            $payroll_data->account_title = $employee->account_title;
                            $payroll_data->payroll_item = $column->id;
                            $amount = $this->{$column->function_name}($employee,$payroll);
                            $payroll_data->amount = $amount;
                            $payroll_data->save();
                        }
                    }

                    foreach ($salary_component_payroll_columns as $column) {
                        $payroll_data = new PayrollData;
                        $payroll_data->payroll = $payroll_id;
                        $payroll_data->employee = $employee->id;
                        $payroll_data->emp_active_inactive = $employee->active == 1 ? 'Active' : 'Inactive';
                        $payroll_data->department_id = $employee->department;
                        $payroll_data->employment_status_id = $employee->employment_status;
                        $payroll_data->job_title_id = $employee->job_title;
                        $payroll_data->department = $employee->title;
                        $payroll_data->employement_status = $employee->employmentstatusName;
                        $payroll_data->job_title = $employee->jobtitlesName;
                        $payroll_data->bank_id = $employee->bank_id;
                        $payroll_data->bank_name = $employee->bank_name;
                        $payroll_data->account_number = $employee->account_number;
                        $payroll_data->account_title = $employee->account_title;
                        $payroll_data->payroll_item = $column->id;

                        $salary_component_id = json_decode($column->salary_components, true)[0];
                        $main_salary_component = SalaryComponent::find($salary_component_id);
                        if ($main_salary_component->is_monthly == 1) {
                            if(env('COMPANY') == 'JSML' || env('COMPANY') == 'RoofLine' || env('COMPANY') == 'CIDEX' || env('COMPANY') == 'Ajmal Dawakhana' || env('COMPANY') == 'UNICORN' || env('COMPANY') == 'HEALTHWISE')
                            {
                                $where = '';
                                if(env('COMPANY') == 'RoofLine' || env('COMPANY') == 'Ajmal Dawakhana'){
                                    $where .= 'AND deducation_group = '.$payroll->deduction_group.'';
                                }
                                $monthly_component = DB::select("SELECT *,month(component_month) as component_month,year(component_month) as component_year from monthly_salary_component where component_id = $salary_component_id AND employee_id =  $employee->id AND year(component_month) = $payroll->payroll_year AND Month(component_month) = $payroll->payroll_month $where");
                                if(count($monthly_component) > 0)
                                {
                                    $amount = $monthly_component[0]->amount;
                                }
                                else
                                {
                                    $amount = 0.00;
                                }
                            }
                            else
                            {
                                $component_date = DB::select("SELECT *,month(applied_month) as component_month,year(applied_month) as component_year from salarycomponent where id = $salary_component_id")[0];

                                if ($component_date->component_month == $payroll->payroll_month and $component_date->component_year == $payroll->payroll_year) {
                                    $salary_component = EmployeeSalary::where('employee', $employee->id)->where('component', $salary_component_id)->first();
                                    if ($salary_component) {
                                        $amount = $salary_component->amount;
                                    } else {
                                        $amount = 0.00;
                                    }
                                } else {
                                    $amount = 0.00;
                                }
                            }
                        } else {
                            $salary_component = EmployeeSalary::where('employee', $employee->id)->where('component', $salary_component_id)->first();
                            if ($salary_component) {
                                $amount = $salary_component->amount;
                            } else {
                                $amount = 0.00;
                            }
                        }
                        if ($column->round_off == 1) {
                            $amount = round($amount);
                        } else {
                            $amount = round($amount, 2);
                        }
                        $payroll_data->amount = $amount;
                        $payroll_data->save();
                    }

                    foreach ($recursive_payroll_columns as $column) {
                        $payroll_data = new PayrollData;
                        $payroll_data->payroll = $payroll_id;
                        $payroll_data->emp_active_inactive = $employee->active == 1 ? 'Active' : 'Inactive';
                        $payroll_data->employee = $employee->id;
                        $payroll_data->department_id = $employee->department;
                        $payroll_data->employment_status_id = $employee->employment_status;
                        $payroll_data->job_title_id = $employee->job_title;
                        $payroll_data->department = $employee->title;
                        $payroll_data->employement_status = $employee->employmentstatusName;
                        $payroll_data->job_title = $employee->jobtitlesName;
                        $payroll_data->bank_id = $employee->bank_id;
                        $payroll_data->bank_name = $employee->bank_name;
                        $payroll_data->account_number = $employee->account_number;
                        $payroll_data->account_title = $employee->account_title;
                        $payroll_data->payroll_item = $column->id;
                        $amount = $this->{$column->function_name}($employee, $payroll);
                        if ($column->round_off == 1) {
                            $amount = round($amount);
                        }
                        elseif($column->round_off == 2)
                        {
                            $amount = round($amount,2);
                        }
                        else {
                            $amount = $amount;
                        }
                        $payroll_data->amount = $amount;
                        $payroll_data->save();
                    }
                    foreach ($calculation_columns as $key => $calculation_column) {
                        $cc = json_decode($calculation_column->calculation_columns);
                        $func = $calculation_column->calculation_function;

                        foreach ($cc as $c) {
                            $value = $this->getCalculationColumnValue($c->column, $payroll->id, $employee->id);
                            $func = str_replace($c->name, $value, $func);
                        }
                        $func = str_replace($c->name, $value, $func);

                        $payroll_data = new PayrollData;
                        $payroll_data->payroll = $payroll_id;
                        $payroll_data->employee = $employee->id;
                        $payroll_data->emp_active_inactive = $employee->active == 1 ? 'Active' : 'Inactive';
                        $payroll_data->department_id = $employee->department;
                        $payroll_data->employment_status_id = $employee->employment_status;
                        $payroll_data->job_title_id = $employee->job_title;
                        $payroll_data->department = $employee->title;
                        $payroll_data->employement_status = $employee->employmentstatusName;
                        $payroll_data->job_title = $employee->jobtitlesName;
                        $payroll_data->bank_id = $employee->bank_id;
                        $payroll_data->bank_name = $employee->bank_name;
                        $payroll_data->account_number = $employee->account_number;
                        $payroll_data->account_title = $employee->account_title;
                        $payroll_data->payroll_item = $calculation_column->id;
                        $amount = $eval_math->evaluate($func, $calculation_column);
                        if ($calculation_column->round_off == 1) {
                            $amount = round($amount);
                        } else {
                            $amount = round($amount, 2);
                        }
                        $payroll_data->amount = $amount;
                        $payroll_data->save();
                    }
                    if(count($last_payroll_columns) > 0){
                        foreach ($last_payroll_columns as $column) {
                            $payroll_data = new PayrollData;
                            $payroll_data->payroll = $payroll_id;
                            $payroll_data->emp_active_inactive = $employee->active == 1 ? 'Active' : 'Inactive';
                            $payroll_data->employee = $employee->id;
                            $payroll_data->department_id = $employee->department;
                            $payroll_data->employment_status_id = $employee->employment_status;
                            $payroll_data->job_title_id = $employee->job_title;
                            $payroll_data->department = $employee->title;
                            $payroll_data->employement_status = $employee->employmentstatusName;
                            $payroll_data->job_title = $employee->jobtitlesName;
                            $payroll_data->bank_id = $employee->bank_id;
                            $payroll_data->bank_name = $employee->bank_name;
                            $payroll_data->account_number = $employee->account_number;
                            $payroll_data->account_title = $employee->account_title;
                            $payroll_data->payroll_item = $column->id;
                            $amount = $this->{$column->function_name}($employee, $payroll);
                            if ($column->round_off == 1) {
                                $amount = round($amount);
                            }
                            elseif($column->round_off == 2)
                            {
                                $amount = round($amount,2);
                            }
                            else {
                                $amount = $amount;
                            }
                            $payroll_data->amount = $amount;
                            $payroll_data->save();

                        }
                    }
                    $this->progress_percentage = $this->progress_percentage + $progress_percentage_increment;
                    $this->update_process_percentage(intval($this->progress_percentage));
                }
                catch (\Throwable $th) {
                    \Log::debug('payroll processing error of employee-', [$employee, $th]);
                    return ['code'=>'500','message' => "An error occurred with the employee ($employee->employee_code-$employee->first_name $employee->last_name) during payroll processing. \n \n Details: ". $th->getMessage()];
                }
            }
            Payroll::find($payroll_id)->update([
                'status' => 'Completed',
                'process_by' => Auth::user()->id,
                'process_at' => now(),
                'process_type' => $meta_info
            ]);

            // DB::commit();
            return ['code'=>'200','message'=>'Payroll Processing has been Completed Successfully!'];
        }
        catch (\Throwable $th) {
            // DB::rollback();
            \Log::debug('payroll processing error-', $th);
            return ['code'=>'500','message'=> $th->getMessage()];
        }
    }

    public function getChildren($id, $ids = [])
    {
        $final = [];

        $query = DB::select("SELECT id FROM companystructures WHERE parent='$id'");
        if (count($query) > 0) {
            foreach ($query as $deparment) {
                array_push($ids, $deparment->id);
                array_push($final, $deparment->id);
            }
        }
        while (!empty($ids)) {
            foreach ($ids as $id) {

                $loop = $this->getChildren2($id);
                foreach ($loop as $l) {
                    array_push($ids, $l);
                    array_push($final, $l);
                }
                array_shift($ids);
            }

        }
        return $final;
    }

    public function getChildren2($id)
    {
        $loop = [];

        $query = DB::select("SELECT id FROM companystructures WHERE parent='$id'");
        if (count($query) > 0) {

            foreach ($query as $department) {
                array_push($loop, $department->id);
            }
        }
        return $loop;

    }

    public function update_process_percentage($progress_percentage){
        $app_name = env('APP_NAME');
        $user_id = Auth::user()->id;
        // dd($app_name."_progress_percentage_".$user_id);
        // $this->socket_client->emit($app_name."_progress_percentage_".$user_id,["value"=>$progress_percentage]);
    }

    public function getCalculationColumnValue($column_id,$payroll_id,$employee_id){
        $data_value = PayrollData::where('payroll',$payroll_id)->where('employee',$employee_id)->where('payroll_item',$column_id)->first();
        if($data_value){
            return $data_value->amount;
        }
        else{
            return 0.00;
        }
    }

    public function PayrollAccrualPayment(Request $request){
        $departments = CompanyStructure::all();
        $all_employees = Employee::where('status', 'Active')->get();
        $banks = getBankAccounts();
        $months = [];
        $employment_statuses = EmploymentStatus::all();
        if (!empty($request->fromDateFilter) || !empty($request->toDateFilter) || !empty($request->departmentFilter) || !empty($request->employmentStatusFilter) || !empty($request->employeeFilter)) {
            $employees = DB::table('ledgers')
            ->select('name', 'employee', 'employee_id', 'department', 'designation', 'amount', 'department_id','employement_status_id','employee_bank','payroll_id', 'accrual_date')
            ->whereIn('id', function ($query) use ($request){
                $query = $query->select(DB::raw('MAX(ledgers.id)'))
                    ->from('ledgers')
                    ->join('payrollcolumns', 'ledgers.column_id', '=', 'payrollcolumns.id');
                    if (!empty($request->fromDateFilter)) {
                        $query = $query->where('accrual_date', '>=', $request->fromDateFilter);
                    }
                    if (!empty($request->toDateFilter)) {
                        $query = $query->where('accrual_date', '<=' ,$request->toDateFilter);
                    }
                    if (!empty($request->departmentFilter)) {
                        $query = $query->whereIn('department_id', $request->departmentFilter);
                    }
                    if (!empty($request->employmentStatusFilter)) {
                        $query = $query->whereIn('employement_status_id', $request->employmentStatusFilter);
                    }
                    if (!empty($request->employeeFilter)) {
                        $query = $query->whereIn('employee_id', $request->employeeFilter);
                    }
                    $query = $query->whereIn('type', ['payroll_accrual', 'pf_accrual'])->where('payrollcolumns.salary_column_type', 'net_Salary')->whereNull('payment_voucher_no')
                    ->groupBy('payroll_id')
                    ->groupBy('employee_id');
            });
            if (!empty($request->fromDateFilter)) {
                $employees->where('accrual_date', '>=', $request->fromDateFilter);
            }
            if (!empty($request->toDateFilter)) {
                $employees->where('accrual_date', '<=' ,$request->toDateFilter);
            }
            if (!empty($request->departmentFilter)) {
                $employees->whereIn('department_id', $request->departmentFilter);
            }
            if (!empty($request->employmentStatusFilter)) {
                $employees->whereIn('employement_status_id', $request->employmentStatusFilter);
            }
            $employees = $employees->get();
            $payments = DB::table('ledgers')->select('employee_id','payroll_id','left_to_allocate')->where('type', 'salary_payment')->get();
            foreach ($employees as $key => $employee) {
                $entry_of_month = date('F Y', strtotime($employee->accrual_date));
                if(!in_array($entry_of_month, $months)){
                    array_push($months, $entry_of_month);
                }
                $employee->left_to_allocate = abs($employee->amount);
                foreach ($payments as $payment) {
                    if($employee->employee_id == $payment->employee_id){
                        if($employee->payroll_id == $payment->payroll_id && $payment->left_to_allocate == 0){
                            unset($employees[$key]);
                        }
                        else{
                            $employee->left_to_allocate = $payment->left_to_allocate;
                        }
                    }
                }
            }
        }
        return view('Admin.finance.payrollProcessing.payment', get_defined_vars());
    }

    public function storePayrollAccrualPayment(Request $request){
        DB::beginTransaction();
        try {
            if(!$request->bank_accounts || !$request->voucher_date){
                return ['code'=>'500','message'=>'You Have Not selected Payment Bank Or Voucher Date!'];
            }
            $records = [];
            $months = [];
            $voucher_no = $this->paymentVoucherNo();
            foreach ($request->employee_ids as $key => $value) {
                $employee = Ledger::join('employees', 'employees.id', '=', 'ledgers.employee_id')
                ->join('payroll', 'payroll.id', '=', 'ledgers.payroll_id')
                ->where('ledgers.employee_id',$key)->first(['ledgers.employee_id','ledgers.department_id', 'ledgers.employement_status_id','ledgers.employee','ledgers.designation_id','ledgers.designation','ledgers.department_id', 'ledgers.department', 'employees.bank_name','employees.account_title','employees.account_number','payroll.date_end']);
                if(!empty($employee)){
                    $setting = GlSetting::whereRaw("(JSON_SEARCH(department_id, 'one', $employee->department_id) IS NOT NULL AND JSON_SEARCH(employment_status_id, 'one', $employee->employement_status_id) IS NOT NULL) AND `category` = 'salary_payment'")->first();
                    if(!empty($setting) && !empty($value['allocation'])){
                        $entry_of_month = date('F Y', strtotime($employee->date_end));
                        if(!in_array($entry_of_month, $months)){
                            array_push($months, $entry_of_month);
                        }
                        $records[] = [
                            'payroll_id' => $value['payroll_id'],
                            'entry_of_month' => $entry_of_month,
                            'gl_setting_id' => $setting->id,
                            'payment_voucher_no' => $voucher_no,
                            'employee_id' => $employee->employee_id,
                            'employee' => $employee->employee,
                            'designation_id' => $employee->designation_id,
                            'designation' => $employee->designation,
                            'department_id' => $employee->department_id,
                            'department' => $employee->department,
                            'employee_bank' => $employee->bank_name,
                            'account_title' => $employee->account_title,
                            'account_number' => $employee->account_number,
                            'type' => $setting->category,
                            'setting_type' => $setting->type,
                            'payment_banks_id' => json_encode($request->bank),
                            'payment_banks' => $request->bank_accounts,
                            'net_payable' => $value['amount'],
                            'gl_account_type' => $setting->column_type,
                            'gl_account' => $setting->gl_accounts,
                            'left_to_allocate' => $value['left_to_allocate'] - $value['allocation'],
                            'amount' => $value['allocation'],
                            'allocation' => $value['allocation'],
                            'balance' => $value['left_to_allocate'] - $value['allocation'],
                            'user_id' => Auth::id(),
                            'voucher_date' => $request->voucher_date,
                            'trans_date' => $request->voucher_date,
                            'created_at' => now(),
                        ];
                    }
                }
            }
            if(count($records) < 1){
                return ['code'=>'404','message'=>'You Have not properly filled the form!'];
            }
            Ledger::insert($records);
            $months = implode(',',$months);
            $voucher_desc = "Salary Payment For Accrual $months";
            Ledger::where('payment_voucher_no', $voucher_no)->update(['payment_voucher_desc'=>$voucher_desc]);
            DB::commit();
            $result = $this->viewAccrualPayment($voucher_no);
            return ['code'=>'200','message'=>'Payment Has Been Successfully Made!', 'result' => $result];
        } catch (\Throwable $th) {
            DB::rollback();
            if (get_class($th) !== \Exception::class) {
                return ['code'=>'500','message'=> 'Please Contact Support Team!'];
            } else {
                return ['code'=>'500','message'=>$th->getMessage()];
            }
        }
    }

    private function paymentVoucherNo(){
        $max_no = Ledger::max('payment_voucher_no');
        return !empty($max_no) ? $max_no + 1 : 1;
    }

    public function accrualPaymentVouchers(Request $request){
        $banks = getBankAccounts();
        $vouchers = Ledger::select('payment_banks as bank','entry_of_month','payment_voucher_desc','payment_trans_no', 'payment_voucher_no', 'voucher_date')->selectRaw('SUM(allocation) as amount')->where('type', 'salary_payment');
        if (!empty($request->fromDateFilter)) {
            $vouchers->where('voucher_date', '>=', $request->fromDateFilter);
        }
        if (!empty($request->toDateFilter)) {
            $vouchers->where('voucher_date', '<=' ,$request->toDateFilter);
        }
        if (!empty($request->voucherFilter)) {
            $vouchers->where('payment_voucher_no', $request->voucherFilter);
        }
        if (!empty($request->bankFilter)) {
            $vouchers->where(function ($query) use ($request) {
                foreach ($request->bankFilter as $bankId) {
                    $query->orWhereJsonContains('payment_banks_id', $bankId);
                }
            });
        }
        if (!empty($request->statusFilter)) {
            if($request->statusFilter == 'paid'){
                $vouchers->whereNotNull('payment_trans_no');
            }
            if($request->statusFilter == 'unpaid'){
                $vouchers->whereNull('payment_trans_no');
            }
        }
        $vouchers = $vouchers->groupBy('payment_voucher_no')->paginate(5);
        // For Pagination
        $total = $vouchers->total();
        $currentPage = $vouchers->currentPage();
        $perPage = $vouchers->perPage();

        $from = ($currentPage - 1) * $perPage + 1;
        $to = min($currentPage * $perPage, $total);
        return view('Admin.finance.payrollProcessing.vouchers', get_defined_vars());
    }

    public function accrualPaymentVoucherView($voucher_no){
        $employees = Ledger::select('employee', 'designation','allocation', 'payment_banks as bank','entry_of_month','payment_voucher_desc', 'payment_voucher_no', 'voucher_date')->where('type', 'salary_payment')->where('payment_voucher_no', $voucher_no)->get();
        return view('Admin.finance.payrollProcessing.voucher', get_defined_vars());
    }

    public function sendAccrualPaymentVoucher(Request $request){
        try {
            if(env('COMPANY') == 'JSML'){
                $employees = Ledger::selectRaw('id,SUM(allocation) as allocation,gl_account,payment_voucher_desc, JSON_UNQUOTE(JSON_EXTRACT(payment_banks_id, "$[0]")) as payment_banks_id')
                ->where('payment_voucher_no', $request->voucher)
                ->groupBy('gl_account')
                ->get();
                if(count($employees) < 1){
                    return ['code'=>'404','message'=>'No Record Found!'];
                }
                ErpIntegrationService::accrualVoucherToErp($request, $employees);
            }
            return ['code'=>'200','message'=>'Voucher Has Been Sent Successfully!'];
        } catch (\Throwable $th) {
            if (get_class($th) !== \Exception::class) {
                return ['code'=>'500','message'=> 'Please Contact Support Team!'];
            } else {
                return ['code'=>'500','message'=>$th->getMessage()];
            }
        }
    }

    public function payrollsErpUrls(Request $request){
        $calculation_groups = CalculationGroup::get();
        $addresses = Payroll::query()
        ->whereNotNull('erp_address')
        ->groupBy('erp_address')
        ->pluck('erp_address')
        ->mapWithKeys(function ($erp_address) {
            return [$erp_address => Payroll::query()->where('erp_address',$erp_address)->pluck('deduction_group')->toArray()];
        })
        ->toArray();

        return view('Admin.finance.payrollProcessing.erpurls', get_defined_vars());
    }

    public function storePayrollsErpUrls(Request $request){
        $this->validate($request, [
            'addresses' => 'required|array',
            'addresses.*.deduction_group' => 'required|array',
            'addresses.*.address' => 'required|url',
        ]);

        Payroll::query()->update([
            'erp_address' => NULL
        ]);
        if($request->filled('addresses')){
            foreach ($request->addresses as $address) {
                Payroll::whereIn('deduction_group',$address['deduction_group'])->update([
                    'erp_address' => $address['address']
                ]);
            }
        }
        return redirect()->back()->with('message','Payroll ERP Addresses Made Successfully!');
    }

    public function payrollEmployeeColumns(Payroll $payroll){
        try {
            $columns = $payroll->employeeColumns()->pluck('name')->toArray();
            return ['code'=>'200', 'columns' => $columns];
        } catch (\Throwable $th) {
            return ['code'=>'500','message'=>$th->getMessage()];
        }
    }

    public function storePayrollEmployeeColumns(Request $request, Payroll $payroll){
        try {
            $payroll->employeeColumns()->whereNotIn('name', $request->employee_columns)->get()->map->delete();
            foreach ($request->employee_columns as $employee_column) {
                $display_name = payrollEmployeeColumns($employee_column);
                $payroll->employeeColumns()->updateOrCreate([
                    'name' => $employee_column,
                    'display_name' => $display_name,
                ],[
                    'name' => $employee_column,
                    'display_name' => $display_name,
                ]);
            }
            return ['code'=>'200', 'message' => 'Colums Saved Successfully!'];
        } catch (\Throwable $th) {
            return ['code'=>'500','message'=>$th->getMessage()];
        }
    }
}
