<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\IndustryGroup;
use Yajra\DataTables\Facades\DataTables;

class IndustryGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::eloquent(IndustryGroup::query())->addIndexColumn()->make(true);
        }
        return view('Admin.setup.IndustryGroup.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
            ]);

            $data = $request->all();
            IndustryGroup::create($data);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(IndustryGroup $industry_group)
    {
        return response()->json($industry_group);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IndustryGroup $industry_group)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
            ]);

            $data = $request->all();
            $industry_group->update($data);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndustryGroup $industry_group)
    {
        try {
            $industry_group->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
