<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\OrganogramSetup;
use App\Models\Employee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Validation\ValidationException;

class OrganogramSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = OrganogramSetup::leftJoin('organogram_setup as c2', 'c2.id', '=', 'organogram_setup.pid')
                ->join('employees', 'employees.id', '=', 'organogram_setup.user_id')
                ->select('organogram_setup.*', 'employees.first_name as user_name', 'c2.name as parent_name');
            return Datatables::eloquent($data)->addIndexColumn()->make(true);
        }
        $users = Employee::where('employees.status', '=', 'Active')->get(['id', 'first_name', 'last_name','employee_code']);
        $Organogram = OrganogramSetup::get();
        return view('Admin.organogram_setup.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try {
            $this->validate($request, [
                'name' => 'required |string',
                'title' => 'required |string',
                // 'pid' => 'required',
                'user_id' => 'required',
            ]);

            $organogram = new OrganogramSetup();
            $organogram->name = $request->name;
            $organogram->title = $request->title;
            $organogram->user_id = $request->user_id;
            $organogram->pid = $request->pid;
            $organogram->created_by = Auth::user()->id;
            $organogram->save();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrganogramSetup  $organogramSetup
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         // Fetch the image path based on the user ID
        //  $image = OrganogramSetup::where('id', $id)->value('profile_photo_path');
        //  dd($image);
         $image = OrganogramSetup::where('organogram_setup.id',$id)->join('employees', 'employees.id', '=', 'organogram_setup.user_id')
        ->first('image');
        if(!empty($image->image)){
            $path = asset('images/employees').'/'.$image->image;
        }else{
            $path = asset('default/default_employee.png');
        }
        // You can add error handling here if necessary

        return response()->json(['image' => $path]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrganogramSetup  $organogramSetup
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organogram = OrganogramSetup::find($id);
        return response()->json($organogram);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrganogramSetup  $organogramSetup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required |string',
                'title' => 'required |string',
                // 'pid' => 'required',
                'user_id' => 'required',
            ]);

            $organogram = OrganogramSetup::find($id);
            $organogram->fill($request->all())->save();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrganogramSetup  $organogramSetup
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $organogram = OrganogramSetup::find($id);
            $organogram->deleted_by = Auth::id();
            // $employee = Employee::where("department" , $id)->get();
            // if(count($employee) > 0){
            //     return ['code'=>'300', 'message'=>'This Department is Assigned to Some Employees Please change it first!'];
            // }
            $organogram->update();
            $organogram->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
