<?php

namespace App\Http\Controllers;

use App\Models\Kpi_Defination;
use Illuminate\Http\Request;
use DataTables;

class KpiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        // $kpis=Kpi_Defination::get();
        if($request ->ajax())
        {
            return DataTables::eloquent(Kpi_Defination::select('kpi_definition.*'))->addIndexColumn()->make(true);
        }
        return view('Admin.kpis.index',get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        // dd($request->kpidefination);
        try{
            $request->validate([
                    'kpidefination' =>'required',
                    'marking_criteria' =>'required',
                    'number_range' =>'required',
            ]);
            Kpi_Defination::create([
                'kpi' => $request->kpidefination,
                'marking_criteria' => $request->marking_criteria,
                'number_range' => $request->number_range,
            ]);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    //    return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kpi_Defination $kpi)
    {
        return 0;   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kpi= Kpi_Defination::find($id);
        return response()->json($kpi);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        try {
            $request->validate([
                'editkpidefination' => 'required',
                'editmarking_criteria' => 'required',
                'editnumber_range' => 'required',
            ]);
            $kpi = Kpi_Defination::find($id);
            if ($kpi) {
                $kpi->update([
                    'kpi' => $request->editkpidefination,
                    'marking_criteria' => $request->editmarking_criteria,
                    'number_range' => $request->editnumber_range,
                ]);
                return response()->json(['code' => 200, 'message' => 'success']);
            } else {
                return response()->json(['code' => 404, 'error' => 'KPI not found'], 404);
            }
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return response()->json(['code' => 422, 'errors' => $e->errors()]);
            } else {
                return response()->json(['code' => 500, 'error_message' => $e->getMessage()]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kpi_Defination $kpi)
    {
        $kpi->delete();
        return response()->json(['success' => true]);
    }
}
