<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\PayGrade;
use App\Models\Employee;
use App\Models\EmploymentStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class JobDetailsSetupController extends Controller
{
    public function __construct(){ 
        $this->middleware('auth');
    }

    public function index(Request $request) 
    {

        if($request->ajax()){

            if($request->type == 'payGrades'){
                return DataTables::eloquent(PayGrade::leftjoin('currencytypes','paygrades.currency','=','currencytypes.id')->select('paygrades.id','paygrades.min_salary', 'paygrades.max_salary' ,'currencytypes.code AS currency','paygrades.name AS paygradeName'))->addIndexColumn()->make(true);
            }
            else{
                return Datatables::eloquent(EmploymentStatus::query())->addIndexColumn()->make(true);
            }
        }    
        $currency = DB::table('currencytypes')->get();  
        return view('Admin.setup.jobDetailsSetup.index', compact('currency'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->type == 'payGrades'){
                $this->validate($request, [
                    'name' => 'required',
                    'currency' => 'required',
                    'min_salary' => 'required',
                    'max_salary' => 'required',               
                ]);
    
                $data = $request->all();
                PayGrade::create($data);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                    'description' => 'required',
                ]);
                $data = $request->all();
                EmploymentStatus::create($data);
            }
            return ['code'=>'200','message'=>'success'];  
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        if($request->type == 'payGrades'){
            $PayGrade = PayGrade::find($id);
            return response()->json($PayGrade);
        }  
        else{
            $EmploymentStatus = EmploymentStatus::find($id);
            return response()->json($EmploymentStatus);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            if($request->type == 'payGrades'){
                $this->validate($request, [
                    'name' => 'required',
                    'currency' => 'required',
                    'min_salary' => 'required',
                    'max_salary' => 'required',
                ]);
                $PayGrade = PayGrade::find($id);
                $PayGrade->fill($request->all())->save();
            }else{
                $this->validate($request, [
                    'name' => 'required',
                    'description' => 'required',
                ]);
                $EmploymentStatus = EmploymentStatus::find($id);
                $EmploymentStatus->fill($request->all())->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }    
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            if($request->type == 'payGrades'){
                $pay_grade = PayGrade::find($id);
                $employee = Employee::where('pay_grade' , $id)->get();
                if(count($employee) > 0){
                    return ['code'=>'300', 'message'=>'This Pay Grade is Assigned to Some Employees Please change it first!'];
                }
                $pay_grade->deleted_by = Auth::id();
                $pay_grade->update();  
                $pay_grade->delete();  
            }else{
                // EmploymentStatus::find($id)->delete();
                $employment_status = EmploymentStatus::find($id);
                $employee = Employee::where('employment_status' , $id)->get();
                if(count($employee) > 0){
                    return ['code'=>'300', 'message'=>'This Employement Status is Assigned to Some Employees Please change it first!'];
                }
                $employment_status->deleted_by = Auth::id();
                $employment_status->update();
                $employment_status->delete();
            }
            return ['code'=>'200','message'=>'success'];
        }catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }    
}