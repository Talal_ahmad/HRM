<?php

namespace App\Http\Controllers;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\MedicalClaim;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class MedicalClaimsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @rownum=0'));
            $data = MedicalClaim::leftjoin('employees','medical_claims.employee_id','=','employees.id')
            ->select('medical_claims.id','medical_claims.description','employees.employee_id','employees.employee_code',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
            ->selectRaw('CONCAT(employees.first_name,employees.middle_name,employees.last_name) as "employee"');
            return Datatables::eloquent($data)
            ->make(true);
        }
        $insurance_types = DB::table('insurance_types')->get(); 
        $InsuranceSubTypes = DB::table('insurance_sub_types')->get();  
        return view('Admin.finance.medicalClaims.index', compact('insurance_types','InsuranceSubTypes'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'type' => 'required',
                'sub_type' => 'required',
                'claim_date' => 'required',
                'amount' => 'required',
                
            ]);

            $data = $request->all();
            if(!empty($data['attachment'])){
                $data['attachment'] = MedicalClaim::Attachment($data['attachment']);
            } 
            MedicalClaim::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $MedicalClaim = MedicalClaim::find($id);
        return response()->json($MedicalClaim);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'type' => 'required',
                'sub_type' => 'required',
                'claim_date' => 'required',
                'amount' => 'required',
            ]);

            $data = $request->all();
            $medical_claim = MedicalClaim::find($id);
            if(!empty($data['attachment'])){
                if(!empty($medical_claim->attachment))
                {
                    unlink(public_path().$medical_claim->attachment);
                }
                $data['attachment'] = MedicalClaim::Attachment($data['attachment']);
            }
            $medical_claim->fill($data)->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            MedicalClaim::find($id)->delete(); 
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }


}
