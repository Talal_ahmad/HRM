<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\PayrollColumn;
use App\Models\SalaryComponent;
use App\Models\CalculationGroup;
use App\Models\CalculationMethod;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class PayrollColumnsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $query = PayrollColumn::leftjoin('salarycomponent','payrollcolumns.salary_components','=','salarycomponent.id')
            ->leftjoin('deductions','payrollcolumns.deductions','deductions.id')
            ->leftjoin('deductiongroup','payrollcolumns.deduction_group','deductiongroup.id')
            ->select('payrollcolumns.id','payrollcolumns.name as payroll_column','payrollcolumns.colorder','salarycomponent.name as salary_comp','deductions.name as cal_method','deductiongroup.name as cal_group');
            return DataTables::of($query)->addIndexColumn()->make(true);
        }
        $calculationGroup = CalculationGroup::get(['id', 'name']);
        $salaryComponents = SalaryComponent::get(['id', 'name']);
        $calculationMethods = CalculationMethod::get(['id', 'name']);
        $payrollColumns = PayrollColumn::get(['id', 'name']);
        return view('Admin.finance.payrollColumns.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $calculationGroup = CalculationGroup::get(['id', 'name']);
        $salaryComponents = SalaryComponent::get(['id', 'name']);
        $calculationMethods = CalculationMethod::get(['id', 'name']);
        $payrollColumns = PayrollColumn::get(['id', 'name']);
        return view('Admin.finance.payrollColumns.create',get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'deduction_group' => 'required',
            'salary_column_type' => 'required',
            'column_order' => 'required|unique:payrollcolumns,colorder',
            'default_value' => 'required'
        ]);

        $column_type = 'single';
        if(!empty($request->salary_components))
        {
            $column_type = 'salary_component';
            $salary_components = json_encode($request->salary_components);
        }
        if(!empty($request->recursive_function))
        {
            $column_type = 'recursive';
            $recursive_function = $request->recursive_function;
        }
        if(!empty($request->deductions))
        {
            $deductions = json_encode($request->deductions);
        }
        if(!empty($request->calculation_columns))
        {
            $column_type = 'calculation';
            $calculation_columns = json_encode($request->calculation_columns);
            $calculation_function = $request->calculation_function;
        }
        $payroll_column = new PayrollColumn();
        $payroll_column->name = $request->name;
        $payroll_column->column_type = $column_type;
        $payroll_column->salary_column_type = $request->salary_column_type;
        $payroll_column->deduction_group = $request->deduction_group;
        $payroll_column->colorder = $request->column_order;
        $payroll_column->default_value = $request->default_value;
        // $payroll_column->gl_account_credit = $request->gl_account_credit;
        // $payroll_column->gl_account_debit = $request->gl_account_debit;
        $payroll_column->payroll_accrual = $request->payroll_accrual;
        $payroll_column->round_off = !empty($request->round_off) ? $request->round_off : NULL;
        $payroll_column->employers_column = !empty($request->employers_column) ? $request->employers_column : NULL;
        $payroll_column->comma_seprated = !empty($request->comma_seprated) ? $request->comma_seprated : NULL;
        $payroll_column->function_name = !empty($request->recursive_function) ? $request->recursive_function : NULL;
        $payroll_column->salary_components = !empty($salary_components) ? $salary_components : NULL;
        $payroll_column->deductions = !empty($deductions) ? $deductions : NULL;
        $payroll_column->calculation_columns = !empty($calculation_columns) ? $calculation_columns : NULL;
        $payroll_column->calculation_function = !empty($calculation_function) ? $calculation_function : NULL;
        $payroll_column->total_column = !empty($request->total_column) ? $request->total_column : 0;
        $payroll_column->save();
        return redirect('payrollColumns')->with('success_message', 'Payroll Column has been Added Successfully!');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payroll_column = PayrollColumn::find($id);
        $calculationGroup = CalculationGroup::get(['id', 'name']);
        $salaryComponents = SalaryComponent::get(['id', 'name']);
        $calculationMethods = CalculationMethod::get(['id', 'name']);
        $payrollColumns = PayrollColumn::get(['id', 'name']);
        return view('Admin.finance.payrollColumns.edit',get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'deduction_group' => 'required',
            'column_order' => 'required|unique:payrollcolumns,colorder,'.$id,
            'default_value' => 'required'
        ]);

        $column_type = 'single';
        if(!empty($request->salary_components))
        {
            $column_type = 'salary_component';
            $salary_components = json_encode($request->salary_components);
        }
        if(!empty($request->recursive_function))
        {
            $column_type = 'recursive';
            $recursive_function = $request->recursive_function;
        }
        if(!empty($request->deductions))
        {
            $deductions = json_encode($request->deductions);
        }
        if(!empty($request->calculation_columns))
        {
            $column_type = 'calculation';
            $calculation_columns = json_encode($request->calculation_columns);
            $calculation_function = $request->calculation_function;
        }

        if($request->name == 'SC%' || $request->name == 'sc%')
        {
            $column_type = 'last';
        }
        $payroll_column = PayrollColumn::find($id);
        $payroll_column->name = $request->name;
        $payroll_column->column_type = $column_type;
        $payroll_column->salary_column_type = $request->salary_column_type;
        $payroll_column->deduction_group = $request->deduction_group;
        $payroll_column->colorder = $request->column_order;
        $payroll_column->default_value = $request->default_value;
        // $payroll_column->gl_account_credit = $request->gl_account_credit;
        // $payroll_column->gl_account_debit = $request->gl_account_debit;
        $payroll_column->payroll_accrual = $request->payroll_accrual;
        $payroll_column->round_off = !empty($request->round_off) ? $request->round_off : NULL;
        $payroll_column->employers_column = !empty($request->employers_column) ? $request->employers_column : NULL;
        $payroll_column->comma_seprated = !empty($request->comma_seprated) ? $request->comma_seprated : NULL;
        $payroll_column->function_name = !empty($request->recursive_function) ? $request->recursive_function : NULL;
        $payroll_column->salary_components = !empty($salary_components) ? $salary_components : NULL;
        $payroll_column->deductions = !empty($deductions) ? $deductions : NULL;
        $payroll_column->calculation_columns = !empty($calculation_columns) ? $calculation_columns : NULL;
        $payroll_column->calculation_function = !empty($calculation_function) ? $calculation_function : NULL;
        $payroll_column->total_column = !empty($request->total_column) ? $request->total_column : 0;
        $payroll_column->update();
        return redirect('payrollColumns')->with('success_message', 'Payroll Column has been Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }
}
