<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\PayslipTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class PayslipTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){  
        $this->middleware('auth');
    }
    
    public function index()
    {
        if(request()->ajax()){
            $query = PayslipTemplate::get();
            return DataTables::of($query)->make(true);
        }
        $payroll_columns = DB::table('payrollcolumns')->get();
        return view('Admin.finance.new_payslip.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'name' => 'required',
                'add_columns' => 'required',
                'deduct_columns' => 'required',
            ]);
            $paySlip = new PayslipTemplate();
            $paySlip->name = $request->name;
            $paySlip->net_salary = $request->net_column;
            $paySlip->gross_salary = $request->gross_column;
            $paySlip->basic_salary = $request->basic_column;
            $paySlip->add_columns = json_encode($request->add_columns);
            $paySlip->deduct_columns = json_encode($request->deduct_columns);
            $paySlip->save();
            return ['code' => 200, 'message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payslip_val = PayslipTemplate::find($id);
        // dd( $payslip_val);
        // implode(',', $payslip_val->add_columns);
        return response()->json($payslip_val);
        return view('Admin.finance.new_payslip.index' , get_defined_vars());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request , [
                'name' => 'required',
                'add_columns' => 'required',
                'deduct_columns' => 'required',
            ]);
            $Payroll = PayslipTemplate::find($id);
            $data = $request->all();
            $data['net_salary'] = $request->net_column;
            $data['gross_salary'] = $request->gross_column;
            $data['basic_salary'] = $request->basic_column;
            $data['add_columns'] = json_encode($request->add_columns);
            $data['deduct_columns'] = json_encode($request->deduct_columns);
                $Payroll->fill($data)->save();
            // $paySlip->name = $request->name;
            // $paySlip->add_columns = json_encode($request->add_columns);
            // $paySlip->deduct_columns = json_encode($request->deduct_columns);
            // $paySlip->save();
            return ['code' => 200, 'message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $paySlip = PayslipTemplate::find($id);
            $paySlip->delete();
            return ['code' => 200, 'message' => 'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
