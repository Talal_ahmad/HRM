<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\LoanInstallment;
use App\Models\LoanRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoanInstallmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'month' => 'required',
                'amount' => 'required',
                'description' => 'required',
            ]);
            $loanInstallment = $request->all();  
            $loanInstallment['status'] = 'active'; 
            $loanInstallment['changed_by'] = Auth::id(); 
            LoanInstallment::create($loanInstallment);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LoanInstallment  $loanInstallment
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request , $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LoanInstallment  $loanInstallment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = LoanInstallment::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LoanInstallment  $loanInstallment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            if($request->type == 'settlement'){
                $this->validate($request, [
                    'remarks' => 'required',
                ]);
                $loanRequest = LoanRequest::find($id);
                $loanRequest->final_satelment = 1;
                $loanRequest->remaining_amount = 0;
                $loanRequest->update();
                $loanInstallments = LoanInstallment::where('loan_request_id' , $id)->where('status' , 'active')->get();
                foreach($loanInstallments as $loanInstallment){
                    $loanInstallment->changed_by =  Auth::id();
                    $loanInstallment->status = 'cancelled';
                    $loanInstallment->description = $request->remarks;
                    $loanInstallment->update();
                }
            }
            else{
                $this->validate($request , [
                    'amount' => 'required',
                    'status' => 'required',
                    'remarks' => 'required',
                ]);
                $loanInstallment = LoanInstallment::find($id);
                $loanInstallment->amount = $request->amount;
                $loanInstallment->changed_by = Auth::id();
                $loanInstallment->description = $request->remarks;
                $loanInstallment->status = $request->status;
                $loanInstallment->update();
                if($request->status == 'deducted'){
                    $loanRequest = LoanRequest::find($request->id);
                    $remaining_amount = $loanRequest->remaining_amount - $request->amount;
                    $loanRequest->remaining_amount = $remaining_amount;
                    $loanRequest->update();
                }
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LoanInstallment  $loanInstallment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
