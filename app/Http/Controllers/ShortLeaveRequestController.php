<?php

namespace App\Http\Controllers;

use DataTables;
use Carbon\Carbon;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\ShortLeaveRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class ShortLeaveRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){ 
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            $query = ShortLeaveRequest::leftjoin('employees' , 'employees.id' , '=' , 'short_leave_requests.employee_id')
            ->leftjoin('leavetypes' , 'leavetypes.id' , '=' , 'short_leave_requests.leave_type')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->whereIn('employees.department' , login_user_departments())
            ->where('employees.status' , 'Active')
            ->select('short_leave_requests.*' , 'employees.employee_code' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'companystructures.title' , 'leavetypes.name');
            if (userRoles()=="Self Service") {
                $query->where('short_leave_requests.employee_id' , Auth::user()->employee);
            }
            return DataTables::of($query)->filter(function ($query) use ($request) {
                if (!empty($request->employeeFilter)) {
                    $query->where('employees.id' , $request->employeeFilter);
                }
                if(!empty($request->fromDateFilter) && !empty($request->toDateFilter)){
                    $query->whereBetween('short_leave_requests.date' , [$request->fromDateFilter , $request->toDateFilter]);
                }
                else{
                    $query->whereYear('short_leave_requests.date' , date('Y'))->whereMonth('short_leave_requests.date' , date('m'));
                }
            },true)->addIndexColumn()->make(true);
        }
        return view('Admin.request&approvals.short_leave.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'date' => 'required',
                'time_from' => 'required',
                'time_to' => 'required',
                'remarks' => 'required',
            ]);
            $short_dup = ShortLeaveRequest::where('employee_id', $request->employee_id)->orderBy('id', 'desc')->whereDate('date', $request->date)->first();
            if(isset($short_dup) && $short_dup->status=='Approved'){
                return ['code' => 300, 'message' => 'The Short Leave for this employee is already exist!'];
            }elseif (isset($short_dup) && $short_dup->status=='Pending') {
                return ['code' => 300, 'message' => 'The Short Leave for this employee is already exist!'];
            }else{
                $data = $request->all();
                if($request->status){
                    $data['status'] = $request->status;
                    $data['status_changed_by'] = Auth::id();     
                    $data['status_changed_at'] = Carbon::now();   
                }
                if($request->hasFile('image')){
                    $data['image'] = InsertFile($request->file('image'), '/images/short_leave_request/');
                }
                $data['submitted_by'] = Auth::id();
                ShortLeaveRequest::create($data);
            }
            return ['code' => 200 , 'message' => 'Short Leave Request Added Successfully!'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShortLeaveRequest  $shortLeaveRequest
     * @return \Illuminate\Http\Response
     */
    public function show(ShortLeaveRequest $shortLeaveRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShortLeaveRequest  $shortLeaveRequest
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ShortLeaveRequest::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShortLeaveRequest  $shortLeaveRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'date' => 'required',
                'time_from' => 'required',
                'time_to' => 'required',
                'remarks' => 'required',
            ]);
            $data = $request->all();
            $short_leave_request = ShortLeaveRequest::find($id);
            if(!empty($request->status)){
                $data['status'] = $request->status;
                $data['status_changed_by'] =  Auth::id();
                $data['status_changed_at'] =  Carbon::now();
            }
            $data['submitted_by'] = Auth::id();
            $short_leave_request->fill($data)->save();
            return ['code' => 200 , 'message' => 'Short Leave Request Updated Successfully!'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShortLeaveRequest  $shortLeaveRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $short_leave_request = ShortLeaveRequest::find($id);
            $short_leave_request->deleted_by = Auth::id();
            $short_leave_request->update();
            $short_leave_request->delete();
            return response(['code' => 200 , 'message' => "Success"]);
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
