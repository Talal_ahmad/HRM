<?php

namespace App\Http\Controllers;

use App\Models\AddSalary;
use App\Models\CalculationGroup;
use App\Models\SalaryComponent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use DB;

class AddSalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $query = AddSalary::leftjoin('employees' , 'employees.id' , '=' , 'employeesalary_pendings.employee')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->leftjoin('salarycomponent' , 'salarycomponent.id' , '=' , 'employeesalary_pendings.component')
            ->where('employeesalary_pendings.status','Pending')
            ->select('employeesalary_pendings.*','employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'companystructures.title' , 'employees.employee_code' , 'salarycomponent.name as component_name');
            return DataTables::eloquent($query)->addIndexColumn()->make(true);
        }
        $CalculationGroups = CalculationGroup::get(['id','name']);
        $salary_components = SalaryComponent::where('is_edit',1)->get();
        return view('Admin.request&approvals.addSalary.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'salary_id' => 'required|array',
                'salary_amount' => 'required|array',
                'employee_id_send' => 'required',
                'calculation_group' => 'required',
            ]);
            foreach ($request->salary_id as $key => $component) {
                $employee_pendings = DB::table('employeesalary_pendings')->where([
                    ['employee' , $request->employee_id_send],
                    ['component' , $component]
                ])->first();
                if(!empty($employee_pendings)){
                    DB::table('employeesalary_pendings')->where('employee' , $request->employee_id_send)->where('component' , $component)->update([
                        'amount' => $request->salary_amount[$component],
                        'status' => 'Pending',
                        'submitted_by' => Auth::id(),
                        // 'remarks' => $request->remarks[$component]
                    ]);
                }
                else{
                    DB::table('employeesalary_pendings')->insert([
                        'employee' => $request->employee_id_send,
                        'component' => $component,
                        'amount' => $request->salary_amount[$component],
                        'status' => 'Pending',
                        'submitted_by' => Auth::id(),
                        // 'remarks' => $request->remarks[$component]
                    ]);
                }
                $payrollEmployee = DB::table('payrollemployees')->where('employee',$request->employee_id_send)->first();
                if(empty($payrollEmployee)){
                    DB::table('payrollemployees')->insert([
                        'employee' => $request->employee_id_send,
                        'pay_frequency' => 4,
                        'currency' => 131,
                        'deduction_exemptions' => '[]',
                        'deduction_allowed' => '[]',
                        'deduction_group' => $request->calculation_group
                    ]);
                }
            }
            return ['code' => 200 , 'message' => 'Success'];

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AddSalary  $addSalary
     * @return \Illuminate\Http\Response
     */
    public function show(AddSalary $addSalary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AddSalary  $addSalary
     * @return \Illuminate\Http\Response
     */
    public function edit(AddSalary $addSalary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AddSalary  $addSalary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AddSalary $addSalary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AddSalary  $addSalary
     * @return \Illuminate\Http\Response
     */
    public function destroy(AddSalary $addSalary)
    {
        //
    }
}
