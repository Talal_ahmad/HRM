<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if(request()->ajax()){
            if ($request->value_emp) {
                $query = DB::table('employees_history')->where('employees_history.emp_id',$request->value_emp)->get();
                return DataTables::of($query)->make(true);
            }else{
                $query = DB::table('employees_history')->where('employees_history.id','')->get();
                return DataTables::of($query)->make(true);
            }
        }
        $employees_name = DB::table('employees')
        ->whereIn('employees.department' , login_user_departments())
        ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name' , 'employees.last_name' ,'employees.off_roll_date','employees.seasonal_status')->get();
        return view('Admin.employees.employee_history.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $date = request()->query('type');
        
        $employee_info = DB::table('employees_history')->leftjoin('country' , 'country.id' , '=' , 'employees_history.country')
        ->leftjoin('province' , 'province.id' , '=' , 'employees_history.province')
        ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees_history.department')
        ->leftjoin('employmentstatus' , 'employmentstatus.id' , '=' , 'employees_history.employment_status')
        ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees_history.job_title')
        ->leftjoin('paygrades' , 'paygrades.id' , '=' , 'employees_history.paygrade')
        ->leftjoin('city' , 'city.id' , '=' , 'employees_history.posting_station')
        ->leftjoin('employees' , 'employees.id' , '=' , 'employees_history.supervisor')
        ->leftjoin('users' , 'users.id' , '=' , 'employees_history.created_by')
        ->where('employees_history.employee_id' , $id)
        ->where('employees_history.created_at' , '<=' , $date)
        ->select('employees_history.*' , 'employees_history.restore_date as restore','employees.first_name AS super_first_name' , 'employees.last_name AS super_last_name' , 'country.name AS country_name' , 'province.name AS province_name' , 'companystructures.title' , 'employmentstatus.name AS emp_status_name' , 'jobtitles.name AS job_title_name' , 'paygrades.name AS pay_grade_name' , 'city.name AS posting_station_name' , 'users.username as modified_by')
        ->orderBy('employees_history.id' , 'desc')
        ->take(2)
        ->get();
        // dd($employee_info);

        $updated_employee = DB::table('employees')->select('employees.*' , 'jobtitles.name AS job' , 'companystructures.title AS dept' , 'employmentstatus.name AS employment')
        ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->join('employmentstatus' , 'employmentstatus.id' , '=' , 'employees.employment_status')
        ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
        ->where('employees.id' , $id)
        ->first();
        
        return view('Admin.employees.employee_history.details' , get_defined_vars());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function single_employee_history(){
        $id = request()->query('id');
        $employee_info = DB::table('employees_history')->where('employee_id' , $id)->get();
        return view('Admin.employees.list.single_employee_history' , get_defined_vars());
    }
}
