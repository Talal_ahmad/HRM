<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use DateTime;
use App\Models\WorkWeek;
use App\Models\Employee;
use App\Models\ShiftType;
use Illuminate\Http\Request;
use App\Models\ShiftManagement;
use App\Models\CompanyStructure;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ShiftManagementController extends Controller
{ 
    public function __construct(){
        $this->middleware('auth');
    }  
    public function index(Request $request) 
    {   
        if($request->ajax()){
            // Current Shift
            if($request->type == 'current_shift'){
                $query = ShiftManagement::leftjoin('employees' , 'employees.id' , '=' , 'shift_management.employee_id')
                ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                ->leftjoin('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
                ->where('employees.status' , 'Active')
                ->whereNull('shift_management.deleted_by')
                ->whereNull('shift_management.deleted_at')
                ->where('shift_management.status', 'Approved')
                // ->where('shift_management.shift_from_date' , '<=' , date('Y-m-d'))
                // ->where('shift_management.shift_to_date' , '>=' , date('Y-m-d'))
                ->whereIn('employees.department' , login_user_departments())
                ->select('shift_management.*' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' ,'employees.middle_name', 'employees.last_name' , 'shift_type.shift_desc AS shiftDesc' , 'work_week.desc AS workDesc' , 'jobtitles.name AS designation' , 'companystructures.title AS empDep');
                if (env('COMPANY') != 'JSML') {
                    $query->where('shift_management.shift_from_date' , '<=' , date('Y-m-d'))->where('shift_management.shift_to_date' , '>=' , date('Y-m-d'));
                }
                return DataTables::of($query)->filter(function ($query) use ($request) {
                    if(!empty($request->curDeptFilter)){
                        if (env('COMPANY') != 'JSML') {
                            $query->where('employees.department', $request->curDeptFilter);
                        }else{
                            $query->whereIn('employees.department', $request->section);
                        }
                    }
                },true)
                ->addColumn('added_on', function ($row) {
                    $created_at = new DateTime($row->created_at);
                    return $created_at->format('j M Y g:ia'); // Format the date as '7 Nov 2023 2:00pm'
                })
                ->addIndexColumn()->make(true);
            }
            // Previous Shift
            elseif($request->type == 'prev_shift'){
                $query = ShiftManagement::leftjoin('employees' , 'employees.id' , '=' , 'shift_management.employee_id')
                ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                ->leftjoin('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
                ->where('employees.status' , 'Active')
                ->where('shift_management.shift_from_date' , '<>' , date('Y-m-d'))
                ->whereNull('shift_management.deleted_at')
                ->where('shift_management.status', 'Approved')
                ->whereIn('employees.department' , login_user_departments())
                ->select('shift_management.*' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' ,'employees.middle_name', 'employees.last_name' , 'shift_type.shift_desc AS shiftDesc' , 'work_week.desc AS workDesc' , 'jobtitles.name AS designation' , 'companystructures.title AS empDep');
                return DataTables::of($query)->filter(function ($query) use ($request) {
                    if(!empty($request->prevDeptFilter)){
                        if (env('COMPANY') != 'JSML') {
                            $query->where('employees.department', $request->prevDeptFilter);
                        }else{
                            $query->whereIn('employees.department', $request->section1);
                        }
                    }
                },true)->make(true);
            }
            // Shift Management History
            else{
                $query = ShiftManagement::leftjoin('employees' , 'employees.id' , '=' , 'shift_management.employee_id')
                ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                ->leftjoin('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
                ->where('employees.status' , 'Active')
                ->where('shift_management.shift_from_date' , '!<' , date('Y-m-d'))
                ->where('shift_management.shift_to_date' , '!>' , date('Y-m-d'))
                ->whereNull('shift_management.deleted_at')
                ->where('shift_management.status', 'Approved')
                ->whereIn('employees.department' , login_user_departments())
                ->select('shift_management.*' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' ,'employees.middle_name', 'employees.last_name' , 'shift_type.shift_desc AS shiftDesc' , 'work_week.desc AS workDesc' , 'jobtitles.name AS designation' , 'companystructures.title AS empDep');
                return DataTables::of($query)->filter(function ($query) use ($request) {
                    if(!empty($request->historyDeptFilter)){
                        if (env('COMPANY') != 'JSML') {
                            $query->where('employees.department', $request->historyDeptFilter);
                        }else{
                            $query->whereIn('employees.department', $request->section2);
                        }
                    }
                },true)->make(true); 
            }
        }
        $work_week = WorkWeek::get();
        $shift_type = ShiftType::where('active',1)->get(); 
        return view('Admin.timeManagement.shiftManagement.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)  
    {
        $employees = [];
        if($request->departmentFilter){
            if (env('COMPANY') != 'JSML') {
                $employees = Employee::where('status' , 'Active')->where('employees.department' , $request->departmentFilter)->whereIn('department' , login_user_departments());
            }else{
                $employees = Employee::where('status' , 'Active')->whereIn('employees.department' , $request->section)->whereIn('department' , login_user_departments());
            }
            

            if($request->employeeFilter){
                $employees = $employees->where('employees.id' , $request->employeeFilter);
            }
            $employees = $employees->select('employees.id' , 'employees.employee_id','employees.job_title','employees.employee_code', 'employees.first_name' , 'employees.last_name','employees.department')->get();
            $work_week = WorkWeek::where('work_week_status',1)->get();
            // dd($shift_type);
        }
        if($request->departmentFilter=='all'){
            $employees = Employee::where('status' , 'Active')->whereIn('department' , login_user_departments());

            if($request->employeeFilter){
                $employees = $employees->where('employees.id' , $request->employeeFilter);
            }
            $employees = $employees->select('employees.id' , 'employees.job_title','employees.employee_id','employees.employee_code', 'employees.first_name' , 'employees.last_name','employees.department')->get();
            $work_week = WorkWeek::where('work_week_status',1)->get();
        }
        
        if (env('COMPANY') == 'CLINIX') {
            $shift_type  = [];
            foreach ($employees as $employee) {
                $employeeId = $employee->id;
                $employeeJobTitle = $employee->job_title;
                $shift_type [$employeeId] = ($employeeJobTitle == 39)
                    ? ShiftType::where('active', 1)
                    ->whereRaw('(TIMESTAMPDIFF(HOUR, shift_start_time, shift_end_time) >= 12 
                                 OR (TIMEDIFF(shift_end_time, shift_start_time) < 0
                                     AND (24 + HOUR(shift_end_time) - HOUR(shift_start_time)) >= 12))')
                    ->get()                
                    : ShiftType::where('active', 1)
                    ->where(function ($query) {
                        $query->whereRaw('IF(shift_start_time <= shift_end_time, 
                                            TIMESTAMPDIFF(HOUR, shift_start_time, shift_end_time), 
                                            TIMESTAMPDIFF(HOUR, shift_start_time, shift_end_time) + 24) BETWEEN 0 AND 10')
                            ->orWhere(function ($q) {
                                $q->whereRaw('TIME(shift_start_time) >= "14:00:00"')
                                    ->whereRaw('TIME(shift_end_time) = "00:00:00"');
                            });
                    })
                    ->get();
            }
        }else{
            $shift_type = ShiftType::where('active', 1)->get();
        }
        return view('Admin.timeManagement.shiftManagement.create' , get_defined_vars());
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            foreach($request->employees as $employee)
            {
                DB::table('shift_management')->insert([
                    'employee_id' => $employee,
                    'shift_id' => $request->shift_id[$employee],
                    'work_week_id' => $request->work_week_id[$employee],
                    'shift_from_date' => $request->shift_from_date[$employee],
                    'shift_to_date' => $request->shift_to_date[$employee],
                    'late_time_allowed' => $request->late_time_allowed[$employee],
                    'status' => 'Approved',
                    'created_by' => Auth::id(),
                ]);
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=> $e->getMessage()];
        }
    }
    public function shiftstore(Request $request)
    {
        try{
            // dd($request->emp_id);
            DB::table('shift_management')->insert([
                'employee_id' => $request->emp_id,
                'shift_id' => $request->shift_id,
                'work_week_id' => $request->work_week_id,
                'shift_from_date' => $request->shift_from_date,
                'shift_to_date' => $request->shift_to_date,
                'late_time_allowed' => $request->late_time_allowed,
                'status' => 'Approved',
                'created_by' => Auth::id(),
            ]);
            
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=> $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ShiftManagement::where('id' , $id)->first();
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request , [
                'shift_id' => 'required',
                'work_week_id' => 'required',
                'shift_from_date' => 'required',
                'shift_to_date' => 'required',
                'late_time_allowed' => 'required',
            ]);
            $data = $request->all();
            $current_shift = ShiftManagement::find($id);
            $current_shift->fill($data)->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | validationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=> $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}