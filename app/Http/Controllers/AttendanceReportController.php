<?php

namespace App\Http\Controllers;

use App\Exports\AttendanceExport;
use App\Models\Attendance;
use App\Models\CompanyStructure;
use App\Models\Employee;
use App\Models\Holidays;
use App\Models\JobTitle;
use App\Models\ShiftManagement;
use Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Auth;
use App\Models\EnvChecks;

class AttendanceReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except('edit');
    }

    public function index()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $dateDay = false;
        if (!empty(request()->singleDate)) {
            $start_date = date('Y-m-d', strtotime(request()->singleDate));
            $end_date = date('Y-m-d', strtotime(request()->singleDate));
            $month = !empty(request()->singleDate) ? date('m', strtotime(request()->singleDate)) : date('m');
            $year = !empty(request()->singleDate) ? date('Y', strtotime(request()->singleDate)) : date('Y');
            $dateDay = true;
        } elseif (!empty(request()->fromDate) && !empty(request()->toDate)) {
            if (date('m', strtotime(request()->fromDate)) != date('m', strtotime(request()->toDate))) {
                return redirect()->back()->with('error', 'Start Date and To date both are of same month!');
            }
            $start_date = request()->fromDate;
            $end_date = request()->toDate;
            $month = date('m', strtotime(request()->fromDate));
            $year = date('Y', strtotime(request()->fromDate));
        }elseif(userRoles()=="Self Service"){
            $start_date = date('Y-m-01');
            $end_date = date('Y-m-t');
            $month = date('m');
            $year = date('Y');
        }
         else {
            $start_date = !empty(request()->dateFilter) ? date('Y-m-01', strtotime(request()->dateFilter)) : date('Y-m-01');
            $end_date = !empty(request()->dateFilter) ? date('Y-m-t', strtotime(request()->dateFilter)) : date('Y-m-t');
            $month = !empty(request()->dateFilter) ? date('m', strtotime(request()->dateFilter)) : date('m');
            $year = !empty(request()->dateFilter) ? date('Y', strtotime(request()->dateFilter)) : date('Y');
        }
        $month_days = empty($dateDay) ? cal_days_in_month(CAL_GREGORIAN, $month, $year) : 1;
        if (!empty(request()->departmentFilter) || !empty(request()->designationFilter) || userRoles()=="Self Service") {
            $employees = Employee::join('companystructures', 'companystructures.id', '=', 'employees.department')
                ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title');

            if (env('COMPANY') != 'RoofLine' && env('COMPANY') != 'JSML') {
                $employees->where('employees.status', 'Active');
            }
            if (env('COMPANY') == 'CLINIX') {
                $employees->whereIn('employees.status', ['Active', 'Suspended']);
            }
            if (request()->type != 'multiple') {
                if (isset(request()->section)) {
                    $employees->whereIn('employees.department', request()->section);
                } else {
                    if (isset(request()->departmentFilter) && request()->departmentFilter != 'all') {
                        $employees->where('employees.department', request()->departmentFilter);
                    }
                }
            }
            if (request()->type == 'multiple') {
                $employees->whereIn('employees.department', request()->departmentFilter);
            }
            if (request()->type == 'staffWiseAttendanceReport' || (!empty(request()->designationFilter) && request()->designationFilter != "all")) {
                $employees->where('employees.job_title', request()->designationFilter);
            }
            if (!empty(request()->employeeFilter)) {
                $employees->where('employees.id', request()->employeeFilter);
            }elseif(userRoles()=="Self Service"){
                $employees->where('employees.id', Auth::user()->employee);
            }
            if (!empty(request()->employmentStatusFilter)) {
                $employees->whereIn('employees.employment_status', request()->employmentStatusFilter);
            }
            if (!empty(request()->shiftFilter)) {
                $shift_employees = ShiftManagement::where('shift_id', request()->shiftFilter)
                    ->where(function ($query) use ($year, $month) {
                        $query->where(function ($q) use ($year, $month) {
                            $q->whereYear('shift_from_date', $year);
                        })
                            ->orWhere(function ($q) use ($year, $month) {
                                $q->whereYear('shift_to_date', $year);
                            });
                    })
                    ->select('employee_id')
                    ->distinct()
                    ->pluck('employee_id')
                    ->toArray();
                $employees->whereIn('employees.id', $shift_employees);
            }
            if (env('COMPANY') == 'CIDEX') {
                $employees->orderBy(DB::raw('ISNULL(employees.sequence), employees.sequence'), 'ASC');
            } else {
                $employees->orderBy('companystructures.title', 'asc');
            }
            $employees = $employees->orderBy('companystructures.title', 'asc')->select('employees.*', 'companystructures.title', 'jobtitles.name as desigination', DB::raw('Month(employees.termination_date) as termination_month, Year(employees.termination_date) as termination_year, Month(employees.off_roll_date) as off_month, Year(employees.off_roll_date) as off_year, Month(employees.suspension_date) as suspension_month, Year(employees.suspension_date) as suspension_year'))->get(['employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name', 'employees.last_name', 'employees.joined_date', 'employees.suspension_date', 'employees.suspended', 'employees.restore_date', 'companystructures.title', 'jobtitles.name as desigination', 'employment_status'])
                ->reject(function ($employees, $key) use ($year, $month) {
                if (env('COMPANY') == 'RoofLine') {
                    if (!empty($employees->termination_date) && $employees->termination_date != '0000-00-00' && $employees->status == 'Terminated') {
                        return $employees->termination_year <= $year && $employees->termination_month . $employees->termination_year != $month . $year;
                    }
                }elseif( env('COMPANY') == 'JSML'){
                    if (!empty($employees->termination_date) && $employees->termination_date != '0000-00-00' && $employees->status == 'Terminated') {
                        return $employees->termination_year <= $year && $employees->termination_month . $employees->termination_year != $month . $year && ($employees->termination_year != $year || intval($employees->termination_month) < intval($month));
                    }
                    if (!empty($employees->off_roll_date) && $employees->off_roll_date != '0000-00-00' && $employees->seasonal_status == 'Off Roll') {
                        return $employees->off_year <= $year && $employees->off_month . $employees->off_year != $month . $year && ($employees->off_year != $year || intval($employees->off_month) < intval($month));
                    }
                    if (!empty($employees->suspension_date) && $employees->suspension_date != '0000-00-00' && $employees->status == 'Suspended' && $employees->suspended == 1) {
                        return $employees->suspension_year <= $year && $employees->suspension_month . $employees->suspension_year != $month . $year && ($employees->suspension_year != $year || intval($employees->suspension_month) < intval($month));
                    }
                }
            });
            if(!empty(request()->old_attendance)){
                $attendance_table = 'attendance_history';
            }else{
                $attendance_table = 'attendance';
            }
            if (env('COMPANY') == 'UNICORN'){
                $unicornDoubleAttendanceShifts = getUnicornDoubleAttendanceShifts()->pluck('id')->toArray();
            }
            else{
                $unicornDoubleAttendanceShifts = [];
            }
            foreach ($employees as $key => $employe) {
                $empTotalLeaves = 0;
                $total_working_hours = 0;
                $total_days_present = 0;
                $total_manual_attendance = 0;
                $total_days_absent = 0;
                $total_days_late = 0;
                $total_overtime_hours = 0;
                $total_off_days = 0;
                $total_missing_checkout = 0;
                $total_working_days = 0;
                $over_all_working_days = 0;
                $total_late_hours = 0;
                for ($date = $start_date; $date <= $end_date; $date++) {
                    $shift_data = DB::table('shift_management')->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
                        ->join('work_week', 'work_week.id', '=', 'shift_management.work_week_id')
                        ->where([
                            ['shift_management.employee_id', $employe->id],
                            ['shift_management.shift_from_date', '<=', $date],
                            ['shift_management.shift_to_date', '>=', $date],
                            ['shift_management.status', '=', 'Approved'],
                            ['shift_management.deleted_at', '=', null],
                        ])
                        ->select('shift_management.work_week_id', 'shift_management.shift_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'shift_type.sandwich_absent', 'work_week.desc as work_week', 'shift_management.late_time_allowed', 'open_working_hours','shift_type.is_24_hour_shift')
                        ->orderBy('shift_management.id', 'DESC')
                        ->first();
                        if(!empty(request()->shiftFilter) && $shift_data?->shift_id != request()->shiftFilter) {
                            continue 2;
                        }
                    if (!empty(request()->shiftFilter)) {
                        if (empty($shift_data) || $shift_data->shift_id != request()->shiftFilter) {
                            continue;
                        }
                    }
                    if (!empty($shift_data)) {
                        $shift_desc = $shift_data->shift_desc;
                        $work_week_id = $shift_data->work_week_id;
                        $shift_start_time = $shift_data->shift_start_time;
                        $shift_end_time = $shift_data->shift_end_time;
                        $late_time_allowed = $shift_data->late_time_allowed;
                        $open_working_hours = $shift_data->open_working_hours ?? 0;
                    } else {
                        $shift_desc = 'Nil';
                        $shift_start_time = '09:00:00';
                        $shift_end_time = '17:00:00';
                        $late_time_allowed = 11;
                        $work_week_id = 1;
                        $open_working_hours = 0;
                    }
                    $shift_start_datetime = "$date " . $shift_start_time;
                    $shift_end_datetime = "$date " . $shift_end_time;
                    $night_shift = false;
                    if (date('H', strtotime($shift_start_time)) > date('H', strtotime($shift_end_time))) {
                        $night_shift = true;
                        $shift_end_datetime = date('Y-m-d H:i:s', strtotime($shift_end_datetime . ' +1 day'));
                    }
                    if (env('COMPANY') == 'JSML') {
                        $short_leave = $employe->shortLeaves()->where([
                            ['date', "$date"],
                            ['status', "Approved"],
                            ['deduction_leave_type_id', '!=', null],
                        ])->first();
                        if (!empty($short_leave)) {
                            $employe_attendance[$employe->id][$date]['short_leave'] = "$short_leave->time_from - $short_leave->time_to";
                            $empTotalLeaves += 0.5;
                        }
                    }
                    $attendance_date = $date;
                    if (env('COMPANY') == 'JSML' && (date('H:i', strtotime($shift_start_time)) == '00:00' || date('H:i', strtotime($shift_start_time)) == '00:01')) {
                        $attendance_date2 = date('Y-m-d', strtotime($date . ' -1 day'));
                        // $shift_start_datetime = date('Y-m-d', strtotime($shift_start_datetime . ' -1 day'));
                        $currentDayattendance = DB::table($attendance_table)
                            ->where('employee', $employe->id)
                            ->whereDate('in_time', "$attendance_date")
                            ->whereRaw('TIME(in_time) < ?', ['23:00:00'])
                            ->first();
                        $previousDayAttendance = DB::table($attendance_table)->where('employee', $employe->id)->whereDate('in_time', "$attendance_date2")->whereRaw('TIME(in_time) > ?', ['23:00:00'])->first();
                        $attendance = null;
                        if (!empty($currentDayattendance)) {
                            $attendance = $currentDayattendance;
                        }
                        if (!empty($previousDayAttendance)) {
                            $attendance = $previousDayAttendance;
                        }
                    } else {
                        if (env('COMPANY') == 'JSML') {
                            $attendance = DB::table($attendance_table)->where('employee', $employe->id)->whereDate('in_time', "$attendance_date")->whereRaw('TIME(in_time) < ?', ['23:00:00'])->orderBy('id', 'ASC')->first();
                        } else {
                            if (env('COMPANY') == 'UNICORN' && $shift_data && in_array($shift_data->shift_id, $unicornDoubleAttendanceShifts)) {
                                $attendance = DB::table($attendance_table)->where('employee', $employe->id)->whereDate('in_time', "$attendance_date")->orderBy('id', 'ASC')->get();
                            }
                            else{
                                $attendance = DB::table($attendance_table)->where('employee', $employe->id)->whereDate('in_time', "$attendance_date")->orderBy('id', 'ASC')->first();
                            }
                        }
                    }
                    $absentMessageGuide = '';
                    if ((env('COMPANY') == 'UNICORN' && $shift_data && in_array($shift_data->shift_id, $unicornDoubleAttendanceShifts) && count($attendance) > 0) || (env('COMPANY') == 'UNICORN' && !empty($attendance) && !is_countable($attendance)) || (env('COMPANY') != 'UNICORN' && !empty($attendance))) {
                        if (env('COMPANY') == 'UNICORN' && $shift_data && in_array($shift_data->shift_id, $unicornDoubleAttendanceShifts)) {
                            $total_working_days += 1;
                            $over_all_working_days += 1;
                            $working_hours = 0.00;
                            $total_days_present = $total_days_present + 1;
                            $overtime_hours = '0.00';
                            $in_time_desc = [];
                            $out_time_desc = [];
                            $distance_in_desc = [];
                            $distance_out_desc = [];
                            $in_datetime_desc = [];
                            $out_datetime_desc = [];
                            foreach ($attendance as $attendanceRecord) {
                                $attend_time_in_carbon = Carbon::parse($attendanceRecord->in_time);
                                if(!empty($attendanceRecord->out_time)){
                                    $attend_time_out_carbon = Carbon::parse($attendanceRecord->out_time);
                                    $out_time_desc[] = $attend_time_out_carbon->format('H:i:s');
                                    $out_datetime_desc[] = $attend_time_out_carbon->format('Y-m-d');
                                }
                                else{
                                    $attend_time_out_carbon = Carbon::parse($shift_end_datetime);
                                    $out_time_desc[] = "Nil";
                                    $out_datetime_desc[] = "Nil";
                                }
                                $working_hours += $attend_time_in_carbon->floatDiffInHours($attend_time_out_carbon);
                                $in_time_desc[] = $attend_time_in_carbon->format('H:i:s');
                                $distance_in_desc[] = $attendanceRecord->distance_in;
                                $distance_out_desc[] = $attendanceRecord->distance_out;
                                $in_datetime_desc[] = $attend_time_in_carbon->format('Y-m-d');
                            }
                            $working_hours = $working_hours <= $open_working_hours ? $working_hours : $open_working_hours;
                            $overtime = DB::table('overtime_management')->where('employee_id', $employe->id)->where('overtime_date', "$date")->where('status', '=', 'Approved')->first(['overtime']);
                            if (!empty($overtime)) {
                                $overtime_hours = $overtime->overtime;
                                $total_overtime_hours = $total_overtime_hours + $overtime_hours;
                            }
                            if (empty($attendance[0]->out_time)) {
                                $total_missing_checkout = $total_missing_checkout + 1;
                            }
                            if ($attendance[0]->is_manual == 1) {
                                $total_manual_attendance = $total_manual_attendance + 1;
                            }
                            $total_working_hours = $total_working_hours + $working_hours;
                            $employe_attendance[$employe->id][$date]['in_time'] = $in_time_desc;
                            $employe_attendance[$employe->id][$date]['out_time'] = $out_time_desc;
                            $employe_attendance[$employe->id][$date]['distance_in'] = $distance_in_desc;
                            $employe_attendance[$employe->id][$date]['distance_out'] = $distance_out_desc;
                            $employe_attendance[$employe->id][$date]['in_datetime'] = $in_datetime_desc;
                            $employe_attendance[$employe->id][$date]['out_datetime'] = $out_datetime_desc;
                            $employe_attendance[$employe->id][$date]['shift_start_time'] = date('H:i', strtotime($shift_start_time));
                            $employe_attendance[$employe->id][$date]['shift_end_time'] = date('H:i', strtotime($shift_end_time));
                            $employe_attendance[$employe->id][$date]['shift_desc'] = $shift_desc;
                            $employe_attendance[$employe->id][$date]['is_manual'] = $attendance[0]->is_manual;
                            $employe_attendance[$employe->id][$date]['department_id'] = $attendance[0]->department_id;
                            $employe_attendance[$employe->id][$date]['grace_period'] = '-';
                            $employe_attendance[$employe->id][$date]['working_hours'] = $working_hours;
                            $employe_attendance[$employe->id][$date]['total_working_hours'] = $total_working_hours;
                            $employe_attendance[$employe->id][$date]['overtime_hours'] = $overtime_hours;
                        }
                        else{
                            if (empty($attendance->out_time)) {
                                $total_missing_checkout = $total_missing_checkout + 1;
                            }
                            if ($attendance->is_manual == 1) {
                                $total_manual_attendance = $total_manual_attendance + 1;
                            }
                            if(env('COMPANY') == 'JSML'){
                                if(checkHalfLeave($employe->id, $date)){
                                    $total_days_present = $total_days_present + 0.5;
                                    $over_all_working_days += 0.5;
                                    $empTotalLeaves = $empTotalLeaves + 0.5;
                                    $employe_attendance[$employe->id][$date]['half_leave_flag'] = 1;
                                }
                                else{
                                    $total_days_present = $total_days_present + 1;
                                    $over_all_working_days += 1;
                                }
                            }
                            else{
                                $total_days_present = $total_days_present + 1;
                                $over_all_working_days += 1;
                            }
                            $overtime_hours = '0.00';
                            $working_hours = '0.00';
                            $overtime = '';
                            $grace_period = date("H:i", strtotime("+$late_time_allowed minutes", strtotime($shift_start_time)));
                            if (!empty($attendance->in_time) && !empty($attendance->out_time)) {
                                $attend_time_in = $attendance->in_time;
                                $attend_time_out = $attendance->out_time;
                                $e_grace_period = date("Y-m-d H:i:s", strtotime("+$late_time_allowed minutes", strtotime($shift_start_datetime)));
                                // because JSML want to calculate hours according to in time
                                if (strtotime($attend_time_in) < strtotime($e_grace_period) && env('COMPANY') != 'JSML') {
                                    $attend_time_in = $shift_start_datetime;
                                }
                                // because JSML want to calculate hours according to out time
                                if (strtotime($attend_time_out) > strtotime($shift_end_datetime) && env('COMPANY') != 'JSML') {
                                    $attend_time_out = $shift_end_datetime;
                                }
                                $working_hours = Carbon::parse($attend_time_in)->floatDiffInHours($attend_time_out);
                                $shift_working_hours = Carbon::parse($shift_start_datetime)->floatDiffInHours($shift_end_datetime);
                                if(!empty($shift_data) && $shift_data->is_24_hour_shift == 1){
                                    $shift_working_hours = 8;
                                }
                                if(env('COMPANY') == 'HEALTHWISE'){
                                    $work_week_days = DB::table('workdays')->where([
                                        ['work_week_id', $work_week_id],
                                        ['status', '=', 'Non-working Day'],
                                    ])->pluck('name')->toArray();
                                    if (in_array(date('l', strtotime("$date")), $work_week_days)) {
                                        $working_hours = 0;
                                    }
                                }
                                if(env('COMPANY') == 'JSML'){
                                    $carbonDate = Carbon::parse($date);
                                    $shift_working_hours = $shift_working_hours - 0.5;
                                    if($working_hours == ($shift_working_hours / 2) && !empty($attendance->ret)){
                                        $total_working_days += 0.5;
                                    }
                                    // Exclude Half Hour From Shift Time
                                    elseif (($working_hours >= ($shift_working_hours / 2) && $working_hours < $shift_working_hours) && (!empty($shift_data) && in_array($shift_data->shift_id, [4, 5, 6]) || (!$carbonDate->isFriday()))) {
                                        $total_working_days += 0.5;
                                    }
                                    else{
                                        if(checkHalfLeave($employe->id, $date)){
                                            $total_working_days += 0.5;
                                        }
                                        else{
                                            if($working_hours >= ($shift_working_hours / 2)){
                                                $total_working_days += 1;
                                            }
                                        }
                                    }
                                }
                                else{
                                    $total_working_days += 1;
                                }
                                $overtime = DB::table('overtime_management')->where('employee_id', $employe->id)->where('overtime_date', "$date")->where('status', '=', 'Approved')->first(['overtime']);
                                if (!empty($overtime)) {
                                    $overtime_hours = $overtime->overtime;
                                    $total_overtime_hours = $total_overtime_hours + $overtime_hours;
                                }
                            }
                            else{
                                $total_working_days += 1;
                            }
                            $grace_period = date("H:i:s", strtotime("+$late_time_allowed minutes", strtotime($shift_start_time)));
                            if (date('H:i:s', strtotime($attendance->in_time)) > $grace_period && date('H:i:s', strtotime($attendance->in_time)) < '23:00:00') {
                                $total_days_late = $total_days_late + 1;
                            }
                            $employe_attendance[$employe->id][$date]['in_time'] = date('H:i', strtotime($attendance->in_time));
                            if (env('COMPANY') == 'CLINIX') {
                                if ($date >= '2023-02-01') {
                                    $shiftEnd = date('Y-m-d H:i:s', strtotime($shift_end_datetime . ' + 20 minute'));
                                    if (!empty($attendance->out_time) && $attendance->out_time <= $shiftEnd) {
                                        $employe_attendance[$employe->id][$date]['out_time'] = date('H:i', strtotime($attendance->out_time));
                                        $total_working_hours = $total_working_hours + $working_hours;
                                    } else {
                                        if (!empty($attendance->out_time) && !empty($overtime)) {
                                            $employe_attendance[$employe->id][$date]['out_time'] = date('H:i', strtotime($attendance->out_time));
                                            $total_working_hours = $total_working_hours + $working_hours;
                                        } else {
                                            $working_hours = 0;
                                            $total_working_hours = $total_working_hours + $working_hours;
                                            if (!empty($attendance->out_time)) {
                                                $employe_attendance[$employe->id][$date]['out_time'] = date('H:i', strtotime($attendance->out_time));
                                                $employe_attendance[$employe->id][$date]['color'] = 'orange';
                                            }
                                            else {
                                                $employe_attendance[$employe->id][$date]['out_time'] = 'Nil';
                                            }
                                        }
                                    }
                                } else {
                                    $total_working_hours = $total_working_hours + $working_hours;
                                    $employe_attendance[$employe->id][$date]['out_time'] = !empty($attendance->out_time) ? date('H:i', strtotime($attendance->out_time)) : 'Nil';
                                }
                            } else {
                                $total_working_hours = $total_working_hours + $working_hours;
                                $employe_attendance[$employe->id][$date]['out_time'] = !empty($attendance->out_time) ? date('H:i', strtotime($attendance->out_time)) : 'Nil';
                            }
                            $employe_attendance[$employe->id][$date]['distance_in'] = $attendance->distance_in;
                            $employe_attendance[$employe->id][$date]['distance_out'] = $attendance->distance_out;
                            $employe_attendance[$employe->id][$date]['in_datetime'] = $attendance->in_time;
                            $employe_attendance[$employe->id][$date]['out_datetime'] = $attendance->out_time;
                            $employe_attendance[$employe->id][$date]['shift_start_time'] = date('H:i', strtotime($shift_start_time));
                            $employe_attendance[$employe->id][$date]['shift_end_time'] = date('H:i', strtotime($shift_end_time));
                            $employe_attendance[$employe->id][$date]['shift_desc'] = $shift_desc;
                            if(env('COMPANY') == 'JSML' && !empty($attendance->od)){
                                $employe_attendance[$employe->id][$date]['od'] = $attendance->od;
                            }
                            if(env('COMPANY') == 'JSML' && !empty($attendance->ret)){
                                $employe_attendance[$employe->id][$date]['ret'] = $attendance->ret;
                            }
                            $employe_attendance[$employe->id][$date]['is_manual'] = $attendance->is_manual;
                            $employe_attendance[$employe->id][$date]['department_id'] = $attendance->department_id;
                            $employe_attendance[$employe->id][$date]['grace_period'] = $grace_period;
                            $employe_attendance[$employe->id][$date]['working_hours'] = $working_hours;
                            $employe_attendance[$employe->id][$date]['total_working_hours'] = $total_working_hours;
                            $employe_attendance[$employe->id][$date]['overtime_hours'] = $overtime_hours;
                            if(env('COMPANY') == 'HEALTHWISE'){
                                $total_late_hours += employeeLateHoursInADay($employe->id, $date);
                            }
                        }
                    }
                    else {
                        absentScript:
                        if(!empty($shift_data)){
                            $absentMessageGuide = "Employee Has Been Assigned $shift_data->shift_desc Shift On This Date. In case of Any Issue Cross Check Shift Management";
                        }
                        else{
                            $absentMessageGuide = "No Shift Has Been Assigned To Employee On This Date. In case of Any Issue Cross Check Shift Management";
                        }
                        $absent_value = 'A';
                        $absent_value_color = 'red';
                        $employee_leave = DB::table('employeeleaves')
                            ->join('leavetypes', 'leavetypes.id', '=', 'employeeleaves.leave_type')
                            ->where([
                                ['employee', $employe->id],
                                ['date_start', '<=', "$date"],
                                ['date_end', '>=', "$date"],
                                ['status', '=', 'Approved'],
                            ])->first(['leavetypes.name as leave_type', 'leavetypes.abbreviation', 'leavetypes.leave_color']);
                        $work_week_days = DB::table('workdays')->where([
                            ['work_week_id', $work_week_id],
                            ['status', '=', 'Non-working Day'],
                        ])->pluck('name')->toArray();
                        $previous_date = date('Y-m-d', strtotime('' . $date . ' -1 day'));
                        $next_date = date('Y-m-d', strtotime('' . $date . ' +1 day'));
                        if (!empty($shift_data) && $shift_data->sandwich_absent == 1) {
                            $previous_attendance = DB::table($attendance_table)->where('employee', $employe->id)->whereDate('in_time', "$previous_date")->first(['id']);
                            $next_attendance = DB::table($attendance_table)->where('employee', $employe->id)->whereDate('in_time', "$next_date")->first(['id']);
                            $employee_previous_leave = DB::table('employeeleaves')
                                ->join('leavetypes', 'leavetypes.id', '=', 'employeeleaves.leave_type')
                                ->where([
                                    ['employee', $employe->id],
                                    ['date_start', '<=', "$previous_date"],
                                    ['date_end', '>=', "$previous_date"],
                                    ['status', '=', 'Approved'],
                                ])->first(['leavetypes.name as leave_type', 'leavetypes.abbreviation', 'leavetypes.leave_color']);
                            $employee_next_leave = DB::table('employeeleaves')
                                ->join('leavetypes', 'leavetypes.id', '=', 'employeeleaves.leave_type')
                                ->where([
                                    ['employee', $employe->id],
                                    ['date_start', '<=', "$next_date"],
                                    ['date_end', '>=', "$next_date"],
                                    ['status', '=', 'Approved'],
                                ])->first(['leavetypes.name as leave_type', 'leavetypes.abbreviation', 'leavetypes.leave_color']);
                        }
                        if (in_array(date('l', strtotime("$date")), $work_week_days)) {
                            $absent_value = 'OFF';
                            $absent_value_color = '#28c76f';
                            // if (!empty($shift_data) && $shift_data->sandwich_absent == 1) {
                            //     if (empty($employee_previous_leave) && empty($employee_next_leave) && empty($previous_attendance) && empty($next_attendance)) {
                            //         $absent_value = 'A';
                            //         $absent_value_color = 'red';
                            //     }
                            //     else{
                            //         $absentMessageGuide = '';
                            //     }
                            // }
                        }
                        // if (!empty($employee_leave) && (env('COMPANY') != 'JSML' || $absent_value != 'OFF')) {
                        if (!empty($employee_leave)) {
                            $absent_value = !empty($employee_leave->abbreviation) ? $employee_leave->abbreviation : 'L';
                            $absent_value_color = !empty($employee_leave->leave_color) ? $employee_leave->leave_color : 'green';
                            $empTotalLeaves = $empTotalLeaves + 1;
                            if ($date <= date('Y-m-d')) {
                                $total_working_days += 1;
                            }
                            $over_all_working_days += 1;
                            $absentMessageGuide = '';
                        }
                        $holiday = DB::table('holidays')->where('dateh', "$date")->first();
                        $holidaySetting = holidaySettingForEmployee($employe, $holiday);
                        if (!empty($holiday) && $holidaySetting) {
                            $attendanceQuery = DB::table($attendance_table)->where('employee', $employe->id)->whereDate('in_time', "$date")->orderBy('id', 'ASC')->first();
                            $absent_value = (env('COMPANY') == 'JSML' && $employe->employment_status == 9)  ? $absent_value : (!empty($attendanceQuery) || !empty($employee_leave) ? $absent_value : 'NH');
                            $absent_value_color = (env('COMPANY') == 'JSML' && $employe->employment_status == 9) ? $absent_value_color : (!empty($employee_leave) ? $absent_value_color : 'skyblue');
                            $absentMessageGuide = '';
                        }
                        if (!empty($shift_data) && $shift_data->sandwich_absent == 1) {
                            if (empty($employee_leave) && empty($employee_previous_leave) && empty($employee_next_leave) && empty($previous_attendance) && empty($next_attendance)) {
                                $absent_value = 'A';
                                $absent_value_color = 'red';
                            }
                            else{
                                $absentMessageGuide = '';
                            }
                        }
                        $checked_date = "$date";
                        if (strtotime($employe->joined_date) > strtotime($checked_date)) {
                            $absent_value = '-';
                            $absent_value_color = 'grey';
                            $absentMessageGuide = '';
                        }
                        if (!empty($employe->suspension_date) && $employe->suspension_date != '0000-00-00' && $employe->suspended == 1) {
                            if (strtotime($employe->suspension_date) < strtotime($checked_date)) {
                                $absent_value = '-';
                                $absent_value_color = 'grey';
                                $absentMessageGuide = '';
                            }
                        }
                        if (!empty($employe->restore_date) && $employe->restore_date != '0000-00-00') {
                            if (strtotime($checked_date) < strtotime($employe->restore_date)) {
                                $absent_value = '-';
                                $absent_value_color = 'grey';
                                $absentMessageGuide = '';
                            }
                        }
                        if ($date > date('Y-m-d')) {
                            $absent_value = '-';
                            $absent_value_color = 'grey';
                            $absentMessageGuide = '';
                        }
                        if ($absent_value == 'A') {
                            $total_days_absent = $total_days_absent + 1;
                        }
                        if ($absent_value == 'OFF' || $absent_value == 'NH') {
                            $total_off_days = $total_off_days + 1;
                            if ($date <= date('Y-m-d')) {
                                $total_working_days += 1;
                            }
                            $over_all_working_days += 1;
                        }

                        $employe_attendance[$employe->id][$date]['absent_value'] = $absent_value;
                        $employe_attendance[$employe->id][$date]['absent_value_color'] = $absent_value_color;
                        $employe_attendance[$employe->id][$date]['absentMessageGuide'] = $absentMessageGuide;
                    }
                }
                $employe_attendance[$employe->id]['out_time'] = $total_missing_checkout;
                $employe_attendance[$employe->id]['total_overtime_hours'] = $total_overtime_hours;
                $employe_attendance[$employe->id]['total_overtime_hours'] = $total_overtime_hours;
                $employe_attendance[$employe->id]['total_days_late'] = $total_days_late;
                $employe_attendance[$employe->id]['total_working_hours'] = $total_working_hours;
                $employe_attendance[$employe->id]['total_leaves'] = $empTotalLeaves;
                $employe_attendance[$employe->id]['total_days_present'] = $total_days_present;
                $employe_attendance[$employe->id]['total_days_absent'] = $total_days_absent;
                $employe_attendance[$employe->id]['total_off_days'] = $total_off_days;
                $employe_attendance[$employe->id]['total_working_days'] = $total_working_days;
                $employe_attendance[$employe->id]['over_all_working_days'] = $over_all_working_days;
                $employe_attendance[$employe->id]['total_manual_attendance'] = $total_manual_attendance;
                if(env('COMPANY') == 'HEALTHWISE'){
                    $employe_attendance[$employe->id]['total_late_hours'] = $total_late_hours;
                }
            }
        }
        if (request()->export_type == 'pdf' || request()->export_type == 'mini_pdf') {
            if (!empty(request()->departmentFilter) && request()->type == 'single') {
                $dept = request()->departmentFilter != 'all' ? CompanyStructure::find(request()->departmentFilter)->title : 'All Departments';
            }
            if (!empty(request()->designationFilter) && request()->designationFilter != "all") {
                $desg = JobTitle::find(request()->designationFilter)->name;
            }
            $orientation = !empty($dateDay) ? 'portrait' : 'landscape';
            if (request()->type == 'Minimized' or request()->query('type') == 'Minimized_multiple') {
                $page_size = 'A2';
                $pdf = PDF::loadView('Admin.attendanceReport.single_page_attendance_report_pdf', get_defined_vars())->setPaper($page_size, $orientation);
            }
            if (request()->type == 'day_wise_hours') {
                $page_size = !empty($dateDay) ? 'A3' : '2A0';
                $pdf = PDF::loadView('Admin.attendanceReport.day_wise_hour_pdf', get_defined_vars())->setPaper($page_size, $orientation);
            } else {
                $page_size = !empty($dateDay) ? 'A3' : '2A0';
                $pdf = PDF::loadView('Admin.attendanceReport.attendance_report_pdf', get_defined_vars())->setPaper($page_size, $orientation);
            }
            if (request()->size_pdf == 'cidpdf') {
                $pdf = PDF::loadView('Admin.attendanceReport.single_date_attendance_report_pdf', get_defined_vars())->setPaper($page_size, $orientation);
            }
            return $pdf->download('Attendance Sheet.pdf');
        }
        if (request()->export_type == 'excel') {
            $dept = '';
            $desg = '';
            $dateFilter = !empty(request()->dateFilter) ? request()->dateFilter : '';
            $singleDate = !empty(request()->singleDate) ? request()->singleDate : '';
            if (!empty(request()->departmentFilter) && request()->type == 'single') {
                $dept = request()->departmentFilter != 'all' ? CompanyStructure::find(request()->departmentFilter)->title : 'All Departments';
            }
            if (!empty(request()->designationFilter) && request()->designationFilter != "all") {
                $desg = JobTitle::find(request()->designationFilter)->name;
            }
            $minimized_undetail = EnvChecks::join('env_name', 'env_name.id', '=', 'meta_data.name')->where('env_name.name','minimized_undetail')->where('key',1)->first();

            return Excel::download(new AttendanceExport($start_date, $end_date, $employees, $employe_attendance, $dept, $desg, $dateFilter, $singleDate, $minimized_undetail), 'Attendance Sheet.xlsx');
        }
        if (request()->type == 'absentees') {
            return view('Admin.attendanceReport.absentess_report', get_defined_vars());
        }
        if (request()->type == 'missing_checkout_report') {
            return view('Admin.attendanceReport.missing_checkout_report', get_defined_vars());
        }
        if (request()->type == 'multiple') {
            return view('Admin.attendanceReport.attendance_report_multiple', get_defined_vars());
        } elseif (request()->type == 'staffWiseAttendanceReport') {
            return view('Admin.attendanceReport.staff_wise_report', get_defined_vars());
        } elseif (request()->type == 'Minimized') {
            return view('Admin.attendanceReport.minimized_report', get_defined_vars());
        } elseif (request()->type == 'from_to_date') {
            return view('Admin.attendanceReport.from_to_date_report', get_defined_vars());
        } elseif (request()->type == 'day_wise_hours') {
            return view('Admin.attendanceReport.day_wise_hour', get_defined_vars());
        } else {
            return view('Admin.attendanceReport.index', get_defined_vars());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('Admin.attendanceReport.absentess_report', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        $year = date('Y', strtotime(request()->date));
        $month = date('m', strtotime(request()->date));

        $leaves = DB::table('employeeleaves')->join('employees', 'employees.id', '=', 'employeeleaves.employee')
            ->join('leavetypes', 'leavetypes.id', '=', 'employeeleaves.leave_type')
            ->join('users', 'users.id', '=', 'employeeleaves.submitted_by')
            ->where('employeeleaves.employee', $id)->where('employeeleaves.status', 'Approved');

        $date = explode('-', request()->date);
        if (count($date) == 2) {
            $leaves = $leaves->whereYear('employeeleaves.date_start', $year)->whereMonth('employeeleaves.date_start', $month)->whereYear('employeeleaves.date_end', $year)->whereMonth('employeeleaves.date_end', $month);
        } else {
            $leaves = $leaves->where('employeeleaves.date_start', '<=', request()->date)->where('employeeleaves.date_end', '>=', request()->date);
        }
        $leaves = $leaves->get(['employeeleaves.date_start', 'employeeleaves.date_end', 'employeeleaves.status', 'employees.first_name', 'employees.last_name', 'users.username', 'leavetypes.name AS leave']);
        $short_leaves = $employee->shortLeaves()->with(['leaveType', 'employee', 'user'])->whereNotNull('deduction_leave_type_id')->whereMonth('date', $month)->whereYear('date', $year)->where('status', 'Approved')->get();

        return response()->json([
            'leaves' => $leaves,
            'short_leaves' => $short_leaves,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($date)
    {
        $attendances = DB::table('attendance')->whereDate('in_time', '>=', "$date")->where('is_manual', 0)->get(['id', 'employee', 'department_id', 'in_time', 'out_time', 'note', 'shift_id', 'shift_start_time', 'shift_end_time', 'work_week_id', 'late_time_allowed', 'is_manual', 'is_online']);
        return response()->json($attendances);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function employeesPresentOnHolidays()
    {
        $startDate = Carbon::parse('2023-10-01'); // Set your start date
        $endDate = Carbon::now(); // You can set an end date if needed

        $dates = [];

        while ($startDate <= $endDate) {
            $dates[] = [
                'name' => $startDate->format('l'),
                'date' => $startDate->toDateString(),
            ];
            $startDate->addDay(); // Increment the date by one day
        }
        // Checking Holiday
        $holidays = Holidays::whereYear('dateh', now()->year)->where('dateh', '>=', '2023-10-01')->pluck('dateh')->toArray();

        $employees = Employee::with(['companystructure', 'jobtitle'])->where('seasonal_status', 'On Roll')->where('status', 'Active')->get();

        $data = [];
        foreach ($employees as $employee) {
            $holidaysDates = $holidays;
            $shiftManagements = $employee->shiftManagements()->whereHas('workWeek.days')->with('workWeek')->where('shift_from_date', '>=', '2023-10-01')->get();
            foreach ($shiftManagements as $shiftManagement) {
                $nonWorkingDays = $shiftManagement->workWeek->days()->where('status', 'Non-working Day')->get();
                foreach ($nonWorkingDays as $nonWorkingDay) {
                    foreach ($dates as $date) {
                        if ($nonWorkingDay->name == $date['name'] && !in_array($date['date'], $holidaysDates)) {
                            array_push($holidaysDates, $date['date']);
                        }
                    }
                }
            }
            $entries = $employee->attendance()
                ->with('employeeDetail.companystructure')
                ->where(function ($query) use ($holidaysDates) {
                    // Check if in_time falls within the first time window
                    $query->whereIn(DB::raw('DATE(in_time)'), $holidaysDates)
                        ->whereBetween(DB::raw('TIME(in_time)'), ['23:00:00', '23:59:59']);
                })
                ->orWhere(function ($query) use ($holidaysDates) {
                    // Check if in_time falls within the second time window
                    $query->whereIn(DB::raw('DATE(in_time)'), $holidaysDates)
                        ->whereBetween(DB::raw('TIME(in_time)'), ['00:00:00', '00:59:59']);
                })
                ->select(['employee', 'in_time', 'out_time'])
                ->get();
            if ($entries->count() > 0) {
                $data[$employee->id]['employee'] = $employee;
                $data[$employee->id]['entries'] = $entries;
            }
        }

        return view('admin.reports.employees_present_on_holidays', compact('data'));
    }
}
