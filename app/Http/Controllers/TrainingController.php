<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use App\Models\TrainingSession;
use App\Models\EmployeeTrainingSession;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            if($request->type == 'my_training'){
                return DataTables::eloquent(TrainingSession::query())->make(true);
            }
            elseif($request->type == 'all_training'){
                $query = TrainingSession::leftjoin('courses' , 'trainingsessions.course' , '=' , 'courses.id')->select('trainingsessions.*' , 'courses.name AS cName')->get();
                return DataTables::of($query)->make(true);
            }
            elseif($request->type == 'training_direct'){
                $query = EmployeeTrainingSession::leftjoin('employees' , 'employees.id' , '=' , 'employeetrainingsessions.employee')->leftjoin('trainingsessions' , 'trainingsessions.id' , '=' , 'employeetrainingsessions.trainingSession')->select('employeetrainingsessions.*' , 'employees.first_name' , 'employees.last_name' , 'trainingsessions.name AS tName')->get();
                return DataTables::of($query)->make(true);
            }
            else{
                $query = TrainingSession::leftjoin('courses' , 'courses.id' , '=' , 'trainingsessions.course')->select('trainingsessions.*' , 'courses.name AS cName')->get();
                return DataTables::of($query)->make(true);
            }
        }
        return view('Admin.training.main_training.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
