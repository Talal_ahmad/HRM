<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\Country;
use App\Models\Province;
use App\Models\Ethnicity;
use App\Models\Nationality;
use Illuminate\Http\Request;
use App\Models\CurrencyType;
use App\Models\ImmigrationStatus;
use Illuminate\Support\Facades\DB; 
use Illuminate\Validation\ValidationException; 

class ManageMetadataController extends Controller
{
    public function __construct(){  
        $this->middleware('auth');
    }

    public function index(Request $request) 
    {
        if($request->ajax()){
            // countries
            if($request->type == 'countries'){
                return Datatables::eloquent(Country::query())->addIndexColumn()->make(true); 
            }
            // provinces
            elseif($request->type == 'provinces'){
                return Datatables::eloquent(Province::leftjoin('country','province.country','=','country.code')->select('province.id','province.code','province.name','country.name AS country'))->addIndexColumn()->make(true); 
            }
            // currency types
            elseif($request->type == 'currency_types'){
                return Datatables::eloquent(CurrencyType::query())->addIndexColumn()->make(true); 

            }
            // nationality
            elseif($request->type == 'nationality'){
                return Datatables::eloquent(Nationality::query())->addIndexColumn()->make(true); 

            }
            // ethnicity
            elseif($request->type == 'ethnicity'){
                return Datatables::eloquent(Ethnicity::query())->addIndexColumn()->make(true); 

            }
            // immigration status 
            else{
                return Datatables::eloquent(ImmigrationStatus::query())->addIndexColumn()->make(true); 

            }
        }

        $countries = Country::get(); 
        return view('Admin.setup.manageMetadata.index', get_defined_vars());  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        try{
            // Store countries
            if($request->type == 'countries'){
                $this->validate($request, [
                    'code' => 'required|unique:country',
                    'name' => 'required',
                ]);
                
                Country::create($request->all());
            }
            // Store provinces
            elseif($request->type == 'provinces'){
                $this->validate($request, [
                    'code' => 'required',
                    'name' => 'required',
                    'country' => 'required',
                ]);
                $Province = new Province();
                $Province->code = $request->code;
                $Province->name = $request->name;
                $Province->country = $request->country; 
                $Province->save();
            }
            // Store currency types
            elseif($request->type == 'currency_types'){
                $this->validate($request, [
                    'code' => 'required',
                    'name' => 'required',
                ]);
                CurrencyType::create($request->all());
            }
            // Store nationality
            elseif($request->type == 'nationality'){
                $this->validate($request, [
                    'name' => 'required',
                ]);
                Nationality::create($request->all());
            }
            // Store ethnicity
            elseif($request->type == 'ethnicity'){
                $this->validate($request, [
                    'name' => 'required',
                ]);
                Ethnicity::create($request->all());
            }
            // immigration status
            else
            {
                $this->validate($request, [
                    'name' => 'required',
                ]);
                ImmigrationStatus::create($request->all());
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        // Edit countries
        if($request->type == 'countries'){
            $Country = Country::find($id);
            return response()->json($Country);
        }
        // Edit provinces
        elseif($request->type == 'provinces'){
            $Province = Province::find($id);
            return response()->json($Province);
        }
        // Edit currency types
        elseif($request->type == 'currency_types'){
            $CurrencyType = CurrencyType::find($id);
            return response()->json($CurrencyType);
        }
        // Edit nationality
        elseif($request->type == 'nationality'){
            $Nationality = Nationality::find($id);
            return response()->json($Nationality);
        }
        // Edit ethnicity
        elseif($request->type == 'ethnicity'){
            $Ethnicity = Ethnicity::find($id);
            return response()->json($Ethnicity);
        }
        // Edit immigration status
        else{
            $ImmigrationStatus = ImmigrationStatus::find($id);
            return response()->json($ImmigrationStatus);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            //update countries
            if($request->type == 'countries'){
                $this->validate($request, [
                    'code' => 'required',
                    'name' => 'required',
                ]);

                $Country = Country::find($id);
                $Country->fill($request->all())->save();
            }
            //update provinces
            elseif($request->type == 'provinces'){
                $this->validate($request, [
                    'code' => 'required',
                    'name' => 'required',
                    'country' => 'required',
                ]);

                $Province = Province::find($id);
                $Province->fill($request->all())->save();
            }
            //update currency types
            elseif($request->type == 'currency_types'){
                $this->validate($request, [
                    'code' => 'required',
                    'name' => 'required',
                ]);

                $CurrencyType = CurrencyType::find($id);
                $CurrencyType->fill($request->all())->save();
            }
            //update nationality
            elseif($request->type == 'nationality'){
                $this->validate($request, [
                    'name' => 'required',
                ]);

                $Nationality = Nationality::find($id);
                $Nationality->fill($request->all())->save();
            }
            //update ethnicity
            elseif($request->type == 'ethnicity'){
                $this->validate($request, [
                    'name' => 'required',
                ]);

                $Ethnicity = Ethnicity::find($id);
                $Ethnicity->fill($request->all())->save();
            }
            //update immigration status
            else
            {
                $this->validate($request, [
                    'name' => 'required',
                ]);
                $ImmigrationStatus = ImmigrationStatus::find($id);
                $ImmigrationStatus->fill($request->all())->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e)
        {
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            // countries
            if($request->type == 'countries'){
                $country =  Country::find($id);
                $country->deleted_by = Auth::id();
                $country->update();
                $country->delete();
            }
            // provinces
            elseif ($request->type == 'provinces') {
                $province = Province::find($id);
                $province->deleted_by = Auth::id();
                $province->update();
                $province->delete();
            }
            // currency types
            elseif ($request->type == 'currency_types') {
                $currency_type = CurrencyType::find($id);
                $currency_type->deleted_by = Auth::id();
                $currency_type->update();
                $currency_type->delete();
            }
            // nationality
            elseif ($request->type == 'nationality') {
                $nationality = Nationality::find($id);
                $nationality->deleted_by = Auth::id();
                $nationality->update();
                $nationality->delete();
            }
            // ethnicity
            elseif ($request->type == 'ethnicity') {
                $ethnicity = Ethnicity::find($id);
                $ethnicity->deleted_by = Auth::id();
                $ethnicity->update();
                $ethnicity->delete();
            }
            // immigration status
            else
            {
                $imigration_status = ImmigrationStatus::find($id);
                $imigration_status->deleted_by = Auth::id();
                $imigration_status->update();
                $imigration_status->delete();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
