<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\Payroll;
use App\Models\PayrollColumn;
use App\Models\CompanyStructure;
// use NumberToWords\NumberToWords;
use App\Models\PayslipTemplate;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class SalarySlipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        $payroll = Payroll::where('id', request()->payroll)->first(['id','name', 'deduction_group','date_start','payslipTemplate','department']);
        if(!empty($payroll))
        {   
            // $payroll_columns = DB::table('payrollcolumns')->where('is_visible', 1)->whereIn('id', json_decode($calculation_group_payroll->columns))->where(function($query){
            //         $query->whereNull('employers_column')->orWhere('employers_column', '');
            //     })->orderBy('colorder', 'asc')->get(['id', 'name', 'comma_seprated','round_off']);
            $total_days = DB::table('payrollcolumns')->where([
                ['deduction_group', $payroll->deduction_group],
                ['salary_column_type', 'working_days']
                ])->first(['id','round_off']);
            if(!empty($payroll->payslipTemplate))
            {
                $payslip = PayslipTemplate::where('id',$payroll->payslipTemplate)->first();
            }
            else
            {
                return redirect()->to('salary_slip')->with('message', 'No Employee found in the given Data!');
            }
            $employees = DB::table('payrolldata')->join('employees', 'employees.id', '=', 'payrolldata.employee')
                ->join('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->where('payrolldata.payroll' , $payroll->id);
                if(!empty(request()->employee)){
                    $employees->where('employees.id' , request()->employee);
                } 
            $employees = $employees->groupBy('employees.id')
                ->get(['employees.id', 'employees.first_name', 'employees.middle_name', 'employees.last_name', 'employees.employee_id','employees.department as dept', 'employees.employee_code', 'companystructures.title AS department', 'jobtitles.name as desigination','employees.nic_num','employees.mobile_phone','employees.image','employees.account_number','employees.bank_name']);
                $allownaces_columns = PayrollColumn::whereIn('id', json_decode($payslip->add_columns))->get(['id', 'name']);
                $deductions_columns = PayrollColumn::whereIn('id', json_decode($payslip->deduct_columns))->orderBy('name', 'ASC')->get(['id', 'name']);
        }
        // $NumberToWords = new NumberToWords();
        // $NumberTransformer = $NumberToWords->getNumberTransformer('en');  

        if (env('COMPANY') == 'RoofLine') {
            if (!empty($payroll->department)) {
                $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
                ->where('company_setup.dept',$payroll->department)
                ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
                ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);
            }
        }
        else{
            $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
                ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
                ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName']);
        }
        $all_payrolls = Payroll::where('status', 'Completed')->get(['id', 'name']);
        return view('Admin.finance.salarySlip.salarySlip' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request , [
            'salary' => 'required',
            'date' => 'required',
            'add_col_name' => 'required',
            'deduct_col_name' => 'required',
        ]);
        $add_cols = $request->add_col_name;
        if(!empty($request->employee)){
            $employee_list = DB::table('payrolldata')->join('payroll' , 'payroll.id' , '=' , 'payrolldata.payroll')
            ->where([
                ['payroll.date_start' , $request->date],
                ['payrolldata.payroll' , $request->salary],
                ['payrolldata.employee' , $request->employee]
            ])
            ->groupBy('payrolldata.employee')
            ->get('payrolldata.*' , 'payroll.department');

            if(count($employee_list) > 0){
                foreach($employee_list as $employee){
                    $all_emp[] = $employee->employee;
                    $emp_dept = $employee->department;
                    $emp_payroll = $employee->payroll;
                }
            }
            else{
                return redirect()->to('salary_slip')->with('message', 'No Employee found in the given Data!');
            }
        }
        else{
            $employee_list = DB::table('payrolldata')->join('payroll' , 'payroll.id' , '=' , 'payrolldata.payroll')
            ->where([
                ['payroll.date_start' , $request->date],
                ['payrolldata.payroll' , $request->salary]
            ])
            ->groupBy('payrolldata.employee')
            ->get(['payrolldata.*' , 'payroll.department']);

            if(count($employee_list) > 0){
                foreach($employee_list as $employee){
                    $all_emp[] = $employee->employee;
                    $emp_dept = $employee->department;
                    $emp_payroll = $employee->payroll;
                }
            }
            else{
                return redirect()->to('salary_slip')->with('message', 'No Employee found in the given Data!');
            }
        }
        
        if(!in_array(71 , $add_cols) && $emp_dept == 4){
            array_push($add_cols , 71);
        }
        elseif(!in_array(74 , $add_cols)){
            array_push($add_cols , 74);
        }

        $employee_info = DB::table('payrolldata')->join('payroll' , 'payroll.id' , '=' , 'payrolldata.payroll')
        ->join('employees' , 'employees.id' , '=' , 'payrolldata.employee')
        ->join('payrollcolumns' , 'payrollcolumns.id' , '=' , 'payrolldata.payroll_item')
        ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
        ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->where([
            ['payrolldata.payroll' , $emp_payroll],
            ['payroll.date_start' , $request->date]
        ])
        ->whereIn('payrolldata.employee' , $all_emp)
        ->whereIn('payrolldata.payroll_item' , $add_cols)
        ->groupBy('payrolldata.employee')
        ->get(['payrolldata.*' , 'employees.first_name' , 'employees.last_name' , 'employees.employee_id AS employee_code' , 'employees.department as dept_id' , 'payrollcolumns.name' , 'jobtitles.name AS designation' , 'payroll.name AS month_salary' , 'companystructures.title AS department']);

        $added_cols = DB::table('payrolldata')->leftjoin('payrollcolumns' , 'payrollcolumns.id' , '=' , 'payrolldata.payroll_item')
        ->whereIn('payrolldata.employee' , $all_emp)
        ->where('payrolldata.payroll' , $request->salary)
        ->whereIn('payrolldata.payroll_item' , $add_cols)
        ->select('payrolldata.*' , 'payrollcolumns.name')
        ->get(); 
        $add_cols_arr = [];
        foreach($added_cols as $add_columns){
            $add_cols_arr[$add_columns->employee][$add_columns->name] = $add_columns->amount;
        }

        $deduct_cols = DB::table('payrolldata')->leftjoin('payrollcolumns' , 'payrollcolumns.id' , '=' , 'payrolldata.payroll_item')
        ->whereIn('payrolldata.employee' , $all_emp)
        ->where('payrolldata.payroll' , $request->salary)
        ->whereIn('payrolldata.payroll_item' , $request->deduct_col_name)
        ->select('payrolldata.*' , 'payrollcolumns.name')
        ->get();
        
        $deduct_cols_arr = [];
        foreach($deduct_cols as $deduct_columns){
            $deduct_cols_arr[$deduct_columns->employee][$deduct_columns->name] = $deduct_columns->amount;
        }

        return view('Admin.finance.salarySlip.slip' , get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $deduction_group = DB::table('payroll')->where('id' , $id)->first(['deduction_group']); 
        $employees = DB::table('payrolldata')
        ->join('employees','employees.id', '=','payrolldata.employee')
        ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
        ->where('payrolldata.payroll', $id)
        ->groupBy('employees.id')
        ->get(['employees.id', 'employees.first_name', 'employees.middle_name', 'employees.last_name', 'employees.employee_id', 'employees.employee_code' ,'jobtitles.name as desigination',]);
        return response()->json($employees);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
