<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\CompanyStructure;
use App\Models\MachineManagement;
use Illuminate\Support\Facades\DB;

class MachineManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {   
        // dd($query);
        if($request->ajax())
        {   
            $query = MachineManagement::leftJoin('companystructures' , 'companystructures.id' , '=' , 'machine_details.machine_department')
            ->select('machine_details.*','companystructures.title');
            return Datatables::of($query)->addIndexColumn()->make(true);
        }
        return view('Admin.setup.maschine_management.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'machine_id' => 'required',
                // 'machine_imei' => 'required',
                'machine_sr_no' => 'required',
                // 'machine_gps' => 'required',
                // 'machine_sim' => 'required',
                'machine_department' => 'required',
                // 'machine_model_no' => 'required',
                'machine_desc' => 'required',
                'machine_type' => 'required',
                'machine_rec_type' => 'required',
                // 'machine_score' => 'required',
                // 'machine_key' => 'required',
                // 'master_key' => 'required',
                // 'tab_imei_number' => 'required',
            ]);
            $data = $request->all();
            MachineManagement::create($data);
        }
        catch(\Exception | ValidationException $e)
        {
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MachineManagement  $machineManagement
     * @return \Illuminate\Http\Response
     */
    public function show(MachineManagement $machineManagement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MachineManagement  $machineManagement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $maschine_management = MachineManagement::find($id);
        return response()->json($maschine_management);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MachineManagement  $machineManagement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'machine_id' => 'required',
                // 'machine_imei' => 'required',
                'machine_sr_no' => 'required',
                // 'machine_gps' => 'required',
                // 'machine_sim' => 'required',
                'machine_department' => 'required',
                // 'machine_model_no' => 'required',
                'machine_desc' => 'required',
                'machine_type' => 'required',
                'machine_rec_type' => 'required',
                // 'machine_score' => 'required',
                // 'machine_key' => 'required',
                // 'master_key' => 'required',
                // 'tab_imei_number' => 'required',
            ]);

            $data = MachineManagement::find($id);
            $data->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MachineManagement  $machineManagement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        {
            try{
                $maschine_management = MachineManagement::find($id);
                $maschine_management->deleted_by = Auth::id();
                $maschine_management->update();
                $maschine_management->delete();
    
                return ['code'=>'200','message'=>'success'];
            }
            catch(\Exception $e){
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }
}
