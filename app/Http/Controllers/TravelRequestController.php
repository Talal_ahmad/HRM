<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\CustomField;
use App\Models\TravelRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class TravelRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {  
        if($request->ajax()){
            if($request->case == 'travel_request' && userRoles()!="Self Service"){
                return Datatables::eloquent(TravelRequest::join('employees', 'employees.id', '=', 'employeetravelrecords.employee')
                ->select('employeetravelrecords.id', 'employeetravelrecords.type', 'employeetravelrecords.travel_from','employeetravelrecords.travel_to','employeetravelrecords.travel_date', 'employeetravelrecords.purpose','employees.employee_id', 'employees.employee_code', 'employees.first_name' , 'employees.middle_name' , 'employees.last_name','employeetravelrecords.status'))->addIndexColumn()->make(true);
            }elseif(userRoles()=="Self Service"){
                return Datatables::eloquent(TravelRequest::join('employees', 'employees.id', '=', 'employeetravelrecords.employee')
                ->select('employeetravelrecords.id', 'employeetravelrecords.type', 'employeetravelrecords.travel_from','employeetravelrecords.travel_to','employeetravelrecords.travel_date', 'employeetravelrecords.purpose','employees.employee_id', 'employees.employee_code', 'employees.first_name' , 'employees.middle_name' , 'employees.last_name','employeetravelrecords.status')->where('employeetravelrecords.employee',Auth::user()->employee))->addIndexColumn()->make(true);
            }
            else{
                return Datatables::eloquent(CustomField::query())->make(true);
            }
        }
        $currencies = DB::table('currencytypes')->get();
        return view('Admin.request&approvals.travel_request.index' , \get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->case == 'travel_request'){
                $this->validate($request , [
                    'employee' => 'required',
                    'type' => 'required',
                    'purpose' => 'required',
                    'travel_from' => 'required',
                    'travel_to' => 'required',
                    'travel_date' => 'required',
                    'return_date' => 'required',
                    'currency' => 'required',
                    'funding' => 'required',
                ]);
                $travelRequest = $request->all();
                if($request->hasFile('attachment1')){
                    $travelRequest['attachment1'] = InsertFile($request->file('attachment1'), '/images/travel_requests/attachment1/');
                }
                if($request->hasFile('attachment2')){
                    $travelRequest['attachment2'] = InsertFile($request->file('attachment2'), '/images/travel_requests/attachment2/');
                }
                if($request->hasFile('attachment3')){
                    $travelRequest['attachment3'] = InsertFile($request->file('attachment3'), '/images/travel_requests/attachment3/');
                }
                TravelRequest::create($travelRequest);
            }
            else{
                $this->validate($request , [
                    'name' => 'required',
                    'display' => 'required',
                    'field_type' => 'required',
                    'field_label' => 'required',
                    'field_validation' => 'required',
                    'display_order' => 'required',
                    'display_section' => 'required',
                ]);
                $customField = new CustomField();
                $customField = $request->all();
                $customField['type'] = 'EmployeeTravelRecord';
                $data = [
                    'label' => $request->field_label,
                    'type' => $request->field_type,
                    'validation' => $request->field_validation,
                ];
                $data_store = array();
                array_push($data_store , $request->name);
                array_push($data_store , $data);
                $customField['data'] = json_encode($data_store);
                $customField['display'] = 'Form';
                if($request->input('add_column_label') || $request->input('add_column_value')){
                    $field_options = [
                        'label' => $request->add_column_label,
                        'value' => $request->add_column_value,
                        'id' => 'field_options_1',
                    ];
                    $field_options_store = array();
                    array_push($field_options_store , $field_options);
                    $customField['field_options'] = json_encode($field_options);
                }                
                CustomField::create($customField);
            }
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TravelRequest  $travelRequest
     * @return \Illuminate\Http\Response
     */
    public function show(TravelRequest $travelRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TravelRequest  $travelRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(TravelRequest $travelRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TravelRequest  $travelRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TravelRequest $travelRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TravelRequest  $travelRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(TravelRequest $travelRequest)
    {
        //
    }
}
