<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\PayrollColumn;
use App\Models\CalculationGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CalculationGroupController extends Controller 
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = CalculationGroup::leftjoin('companystructures','deductiongroup.department','=','companystructures.id')->select('deductiongroup.id','deductiongroup.name','deductiongroup.description','companystructures.title');
            return Datatables::of($data)->addIndexColumn()->make(true);
        }
        return view('Admin.finance.calculationGroups.index');  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required',
            ]);

            $data = $request->all(); 
            CalculationGroup::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $calculationGroups = CalculationGroup::find($id);
        return response()->json($calculationGroups);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'name' => 'required',
            ]);

            $calculationGroups = CalculationGroup::find($id);
            $calculationGroups->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $payroll_columns = PayrollColumn::where('deduction_group', $id)->get();
            if(count($payroll_columns) > 0)
            {
                return ['code'=>'300', 'message'=>'This Calculation Group is Assigned to Some Payroll Columns Please change it first!'];
            }
            $calculation_group = CalculationGroup::find($id);
            $calculation_group->deleted_by = Auth::user()->id;
            $calculation_group->update();

            $calculation_group->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }


}
