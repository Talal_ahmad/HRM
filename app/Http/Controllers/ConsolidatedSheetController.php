<?php

namespace App\Http\Controllers;

use App\Models\Payroll;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConsolidatedSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        $employees = [];
        
        if(!empty(request()->payrollFilter)){
            $employees = Employee::join('payrolldata' , 'payrolldata.employee' , '=' , 'employees.id')
            ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->whereIn('payrolldata.payroll' , request()->payrollFilter)
            ->where('payrolldata.payroll_item' , 74)
            ->whereNotNull('employees.account_number')
            ->where('employees.account_number','<>','')
            ->select('employees.id' , 'employees.employee_id' ,
             'employees.employee_code' , 'employees.first_name' ,
              'employees.last_name' ,'employees.middle_name', 
              'employees.account_number' ,
               'employees.account_title' , 
               'employees.nic_num'
             , 'employees.mobile_phone' ,
              'payrolldata.amount' , 
              'jobtitles.name AS designation',
              'companystructures.title as employee_Department')
            ->get();
            // dd($employees);
            // foreach($employees as $employee){
            //     $gross_amount = DB::table('payrolldata')->whereIn('payroll' , request()->payrollFilter)->where('employee' , $employee->id)->where('payroll_item' , 37)->select('payrolldata.amount')->first();

            //     $bank_details = DB::table('payrolldata')->whereIn('payroll' , request()->payrollFilter)->where('employee' , $employee->id)->where('payroll_item' , 75)->select('amount')->first();

            //     $account_details = explode('-' , $bank_details->amount);
            //     if(isset($account_details[3]))
            //     {
            //         $account_details[3] = '-'.$account_details[3];
            //     }
            //     else{
            //         $account_details[3] = '';
            //     }
            //     $employee->gross_amount = $gross_amount->amount;
            //     $employee->accountName = $account_details[0]; 
            //     $employee->bankName = $account_details[1];
            //     $employee->accountNum = $account_details[2].$account_details[3];
            // }
            $payrolls = DB::table('payroll')->where([
                // [DB::raw('YEAR(date_start)') , $year],
                // [DB::raw('MONTH(date_start)') , $month],
                ['status' , 'Completed']
                ])->whereIn('payroll.id', request()->payrollFilter)->get(['id','department','columns','deduction_group']);
                if (count($payrolls) > 0) {
                    foreach ($payrolls as $key => $payroll) {
                        $payroll_ids[] = $payroll->id;
                        $columns = json_decode($payroll->columns);
                    }
                    $payroll_columns = DB::table('payrollcolumns')->where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                        $query->whereNull('employers_column')->orWhere('employers_column', '');
                    })->orderBy('colorder', 'asc')->get(['id', 'name', 'comma_seprated','round_off']);
                }
        }
        $payrolls = Payroll::orderBy('id' , 'desc')->get();
        return view('Admin.finance.consolidated_bank_sheet.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
