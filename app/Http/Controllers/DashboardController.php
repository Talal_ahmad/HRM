<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Models\Employee;
use App\Models\EmployeesHistory;
use Illuminate\Http\Request;
use App\Models\CompanyStructure;
use Illuminate\Support\Facades\DB;
use App\Models\EmployeeLeaveRequest;
use Spatie\DbDumper\Databases\MySql;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        //TurnOver Report
        $in_emp = Employee::select('id','joined_date')
        ->where('employees.status', 'Active')
        ->whereIn('employees.department', login_user_departments())
        ->where('employees.suspended' , 0)
        ->get()->groupBy(function($in_emp){
            return Carbon::parse($in_emp->joined_date)->format('M');
        });
        $months=[];
        $Countmonths=[];
        foreach ($in_emp as $month => $value) {
            $months[]=$month;
            $Countmonths[]=count($value);
        }
        $out_emp = Employee::select('id','termination_date')
        ->where('employees.suspended' , 0)
        ->where('employees.status', 'Terminated')
        ->whereIn('employees.department', login_user_departments())->get()->groupBy(function($out_emp){
            return Carbon::parse($out_emp->termination_date)->format('M');
        });
        $out_months=[];
        $out_Countmonths=[];
        foreach ($out_emp as $month => $value) {
            $out_months[]=$month;
            $out_Countmonths[]=count($value);
        }
        //Annual Turnover Report
        $in_emp_1 = Employee::select('id','joined_date')
        ->where('employees.status', 'Active')
        ->whereIn('employees.department', login_user_departments())
        ->where('employees.suspended' , 0)
        ->get()->groupBy(function($in_emp_1){
            return Carbon::parse($in_emp_1->joined_date)->format('Y');
        });
        $months_1=[];
        $Countmonths_1=[];
        foreach ($in_emp_1 as $month => $value) {
            $months_1[]=$month;
            $Countmonths_1[]=count($value);
        }
        $out_emp_1 = Employee::select('id','termination_date')
        ->where('employees.suspended' , 0)
        ->where('employees.status', 'Terminated')
        ->whereIn('employees.department', login_user_departments())->get()->groupBy(function($out_emp_1){
            return Carbon::parse($out_emp_1->termination_date)->format('Y');
        });
        $out_months_1=[];
        $out_Countmonths_1=[];
        foreach ($out_emp_1 as $month => $value) {
            $out_months_1[]=$month;
            $out_Countmonths_1[]=count($value);
        }
        // $file = 'attendance_form_data-'.date('Y-m-d').'.sql';
        // $folder = 'D:\BSCS\backup\attendance_form_data_backups/'.env('DB_DATABASE');
        // if (!file_exists($folder)) {
        //     mkdir($folder, 0777, true);
        // }
        // $path = 'D:\BSCS\backup\attendance_form_data_backups/'.env('DB_DATABASE').'/'.$file;
        // \Spatie\DbDumper\Databases\MySql::create()
        // ->setDbName(env('DB_DATABASE'))
        // ->setUserName(env('DB_USERNAME'))
        // ->setPassword(env('DB_PASSWORD'))
        // ->includeTables('attendance_form_data')
        // ->dumpToFile($path);
        // dd('dfsf');
        // $cur_date = new Carbon(date('Y-m-d' , strtotime('-7 days')));
        // $week_start_date = $cur_date->startOfWeek()->format('Y-m-d');
        // $week_end_date = $cur_date->endOfWeek()->format('Y-m-d');
        // $ranges = CarbonPeriod::create($week_start_date, $week_end_date);
        // foreach($ranges as $range){
        //     $list_of_dates[] = $range->format('Y-m-d');
        // }
        // $attendance_last_week = DB::table('attendance')->whereIn(DB::raw("DATE(in_time)"), $list_of_dates)->count();
        if(env('COMPANY') == 'JSML'){
        $off_employees = Employee::leftjoin('companystructures', 'companystructures.id', '=', 'employees.department')
        ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
        ->where('employees.suspended' , 0)
        ->where('employees.status', 'Active')
        ->whereIn('employees.department', login_user_departments())
        ->where('employees.seasonal_status' , 'Off Roll')->count();
        $reg_employees = Employee::leftjoin('companystructures', 'companystructures.id', '=', 'employees.department')
        ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
        // ->where('employees.suspended' , 0)
        // ->where('employees.status', 'Active')
        ->whereIn('employees.department', login_user_departments())->count();
        $ter_employees = Employee::leftjoin('companystructures', 'companystructures.id', '=', 'employees.department')
        ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
        ->where('employees.suspended' , 0)
        ->where('employees.status', 'Terminated')
        ->whereIn('employees.department', login_user_departments())->count();
        }
        $employees_on_leave = EmployeeLeaveRequest::leftjoin('employees' , 'employees.id' , '=' , 'employeeleaves.employee')
        ->where([
            ['employeeleaves.date_start' , '<=' , date('Y-m-d')],
            ['employeeleaves.date_end' , '>=' , date('Y-m-d')],
            ['employeeleaves.status' , 'Approved'],
            ['employees.status' , 'Active'],
            ['employees.suspended' , 0],
        ])->whereIn('employees.department' , login_user_departments())->count();
        $departments = CompanyStructure::whereIn('id',login_user_departments())->where('parent',1)->first(['id', 'title','parent']);
        if(!empty($departments)){
            $parent = count(getchildren2($departments->id));        
        }  

        $currentYear = date('Y');
        $employees_turnover = EmployeesHistory::selectRaw("DATE_FORMAT(created_at, '%m') AS month_number,
            DATE_FORMAT(created_at, '%b') AS month_name,
            SUM(CASE WHEN status = 'Suspended' THEN 1 ELSE 0 END) AS suspended,
            SUM(CASE WHEN status = 'Terminated' THEN 1 ELSE 0 END) AS 'terminated'")
        ->whereYear('created_at', $currentYear)
        ->groupBy('month_number', 'month_name')
        ->orderBy('month_number')
        ->get();

        $suspended = '';
        $terminated = '';
        $lastIndex = count($employees_turnover) - 1;

        foreach ($employees_turnover as $key => $record) {
            $monthNumber = intval($record->month_number);
            $formattedDate = "new Date($currentYear, " . ($monthNumber - 1).")";
            
            $suspended .= "{x: $formattedDate, y: $record->suspended}";
            $terminated .= "{x: $formattedDate, y: $record->terminated}";
        
            if ($key !== $lastIndex) {
                $suspended .= ",";
                $terminated .= ",";
            }
        }
        $dataPoints['suspended'] = $suspended;
        $dataPoints['terminated'] = $terminated;

        return view('Admin.index' , get_defined_vars());
    }
}
