<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocalizationController extends Controller
{
    public function changeLanguage($locale)
    {
        // dd($locale);
        session(['locale' => $locale]);
        return redirect()->back(); // Redirect back to the previous page or any other page
    } 
}
