<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\CompanySetup;
use App\Models\JobTitle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class companySetupController extends Controller
{
    public function __construct(){   
        $this->middleware('auth');
    }

    public function index()
    {
        if(request()->ajax()){
            $query = CompanySetup::leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
            
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'company_setup.dept')
            ->leftjoin('employees as e1' , 'e1.id' , '=' , 'company_setup.gm')
            ->leftjoin('employees as e2' , 'e2.id' , '=' , 'company_setup.payroll_officer')
            ->get(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName' , 'e1.last_name AS gm_lName' , 'e2.first_name AS payroll_fName' , 'e2.last_name AS payroll_lName','companystructures.title']);
            return DataTables::of($query)->make(true);
        }
        return view('Admin.setup.companySetup.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'company_name' => 'required',
                'logo' => 'required',
            ]);
            $company_setup = new CompanySetup();
            $company_setup->company_name = $request->company_name;
            $company_setup->hr = $request->hr;
            $company_setup->gm = $request->gm;
            $company_setup->dept = $request->departmentFilter;
            $company_setup->payroll_officer = $request->payroll_officer;
            if($request->hasFile('logo')){
                $file = $request->file('logo');
                $extension = $file->getClientOriginalExtension();
                $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                $file->move('images/company_logo/' , $temp_name);
                $company_setup->logo = $temp_name;
            }
            $company_setup->save();
            return ['code' => 200 , 'message' => 'Success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company_setup = CompanySetup::find($id);
        return response()->json($company_setup);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request , [
                'company_name' => 'required',
            ]);
            $company_setup = CompanySetup::find($id);
            $company_setup->company_name = $request->company_name;
            $company_setup->hr = $request->hr;
            $company_setup->gm = $request->gm;
            $company_setup->dept = $request->departmentFilter;
            $company_setup->payroll_officer = $request->payroll_officer;
            if($request->hasFile('logo')){
                unlink('images/company_logo/'.$company_setup->logo);
                $file = $request->file('logo');
                $extension = $file->getClientOriginalExtension();
                $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                $file->move('images/company_logo/' , $temp_name);
                $company_setup->logo = $temp_name;
            }
            $company_setup->save();
            return ['code' => 200 , 'message' => 'Success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $company_setup = CompanySetup::find($id);
            if($company_setup->logo){
                unlink('images/company_logo/'.$company_setup->logo);
            }
            $company_setup->deleted_by = Auth::id();
            $company_setup->update();
            $company_setup->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'422','error_message'=>$e->getMessage()];
        }
    }
}
