<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Carbon\Carbon;
use App\Models\Employee;
use App\Models\PayFrequency;
use App\Models\Deduction;
use App\Models\DeductionGroup;
use App\Models\CurrencyType;
use Illuminate\Http\Request;
use App\Models\PayrollEmployee;
use Illuminate\Validation\ValidationException;

class CompanyPayrollController extends Controller
{
    public function __construct(){  
        $this->middleware('auth');
    }

    public function index(Request $request) 
    {
        $user_calculation_groups = !empty(Auth::user()->user_calculation_groups) ? json_decode(Auth::user()->user_calculation_groups) : [];
        if($request->ajax()){
            $data = PayrollEmployee::leftjoin('employees','payrollemployees.employee','=','employees.id')->leftjoin('payfrequency','payrollemployees.pay_frequency','=','payfrequency.id')->leftjoin('deductiongroup','payrollemployees.deduction_group','=','deductiongroup.id')->leftjoin('currencytypes','payrollemployees.currency','=','currencytypes.id')
            ->where('employees.status', 'Active');
            if(env('COMPANY') == 'JSML'){
                $data->where('employees.seasonal_status' , 'On Roll');
            }
            $data = $data->whereIn('employees.department', login_user_departments())
            // ->whereIn('payrollemployees.deduction_group', $user_calculation_groups)
            ->select('payrollemployees.id','payrollemployees.employee','payrollemployees.pay_frequency','payrollemployees.deduction_group','payrollemployees.currency','payfrequency.name as payFrequency','deductiongroup.name AS calculationGroup','currencytypes.code AS currency','employees.employee_id','employees.employee_code','employees.first_name','employees.middle_name','employees.last_name', 'payrollemployees.created_at');
            return Datatables::of($data)
            ->addColumn('formatted_created_at', function($row) {
                $createdAt = Carbon::parse($row->created_at);
                return $createdAt->format('d F Y h:i A');
            })
            ->addIndexColumn()
            ->make(true);
        }
        
        $payFrequency = PayFrequency::get(['id', 'name']);
        $currencies = CurrencyType::get(['id', 'code']);
        $calculationGroup = DeductionGroup::get(['id', 'name']);
        $calculation_Exemptions_Assigned = Deduction::get(['id', 'name']);
        return view('Admin.finance.companyPayroll.index', get_defined_vars());   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'employee' => 'required',
                'pay_frequency' => 'required',
                'currency' => 'required',     
                'deduction_group' => 'required',
            ]);

            $employee = Employee::where('id' , $request->employee)->first();
            $deduct_group = PayrollEmployee::where([
                ['employee' , $request->employee],
                ['deduction_group' , $request->deduction_group]
            ])->first();
            if(!empty($deduct_group)){
                return ['code'=>'419','message'=>'This Calculation Group already Assigned to '.$employee->first_name." ".$employee->last_name];
            }

            $data = $request->all();
            PayrollEmployee::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        $PayrollEmployee = PayrollEmployee::find($id);
        return response()->json($PayrollEmployee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'employee' => 'required',
                'pay_frequency' => 'required',
                'currency' => 'required',
                'deduction_group' => 'required',
            ]);

            $PayrollEmployee = PayrollEmployee::find($id);

            $employee = Employee::where('id' , $request->employee)->first();
            $deduct_group = PayrollEmployee::where([
                ['employee' , $request->employee],
                ['deduction_group' , $request->deduction_group]
            ])->first();
            if(!empty($deduct_group)){
                return ['code'=>'419','message'=>'This Calculation Group already Assigned to '.$employee->first_name." ".$employee->last_name];
            }

            $PayrollEmployee->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $PayrollEmployee = PayrollEmployee::find($id);
            $PayrollEmployee->deleted_by = Auth::user()->id;
            $PayrollEmployee->update();
            $PayrollEmployee->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
