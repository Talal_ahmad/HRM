<?php

namespace App\Http\Controllers;

use App\Models\Headline;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\News_Page;
use App\Models\CompanyStructure;
use Illuminate\Validation\ValidationException;

class NewsPageController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function news_page()
    {
        
        $users = User::get(['id', 'username']);
        $departments = CompanyStructure::get(['title', 'id']);
        $news = News_Page::get();
        return view('Admin.newsPage.index', get_defined_vars());
    }
    public function news_create(Request $request)
    {

        $request->validate([
            'user' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'attachment_image' => 'max:10240',
            'attachment_video' => 'max:30240',
            'message' => '',
            'noticetype' => 'required',
            'designation' => 'required',
            'name' => 'required',
            'department' => 'required'

        ]);

        $VideopathToStore = NULL;
        $FilepathToStore = NULL;
        if ($request->hasFile('attachment_image')) {
            $fileNameExt = $request->file('attachment_image')->getClientOriginalName();
            $fileName = pathinfo($fileNameExt, PATHINFO_FILENAME);
            $fileExt = $request->file('attachment_image')->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '.' . $fileExt;
            $FilepathToStore = $request->file('attachment_image')->storeAs('News-Images_Attachments', $fileNameToStore);
            $FilepathToStore = "../storage/app/" . $FilepathToStore;
        }
        if ($request->hasFile('attachment_video')) {
            $fileNameExt = $request->file('attachment_video')->getClientOriginalName();
            $fileName = pathinfo($fileNameExt, PATHINFO_FILENAME);
            $fileExt = $request->file('attachment_video')->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '.' . $fileExt;
            $VideopathToStore = $request->file('attachment_video')->storeAs('News-Videos_Attachments', $fileNameToStore);
            $VideopathToStore = "../storage/app/" . $VideopathToStore;
        }
        $newsObject = new News_Page;
        $newsObject->user_id = $request->user;
        $newsObject->attachment_image = $FilepathToStore;
        $newsObject->designation = $request->designation;
        $newsObject->name = $request->name;
        $newsObject->attachment_vedio = $VideopathToStore;
        $newsObject->from_date = $request->from_date;
        $newsObject->to_date = $request->to_date;
        $newsObject->type = $request->noticetype;
        $newsObject->message = $request->message;
        $newsObject->created_by = $request->created_by;
        $newsObject->department_ids = implode(',', $request->department);
        $newsObject->status = 1;
        $newsObject->watched_by=",";
        $newsObject->save();
        return redirect()->back();
    }
    public function news_dashboard()
    {
        dd(listPayrollFunctionsNames());
        $currentdatetime = date('Y-m-d H:i:s');
        $news = News_Page::orderBy('created_at', 'DESC')->where('to_date', '>=', $currentdatetime)->get();
        $headlines = Headline::where('to_datetime', '>=', $currentdatetime)->get();
        $newsheadlines = '';
        foreach ($headlines as $currentheadline) {
            $newsheadlines .= $currentheadline->headline . " ";
        }
        return view('Admin.newsPage.newsdashboard', compact('news', 'newsheadlines'));
    }
    public function deletenews($id)
    {
        News_Page::destroy($id);
        return redirect()->back();
    }
    public function updatenewsdeuration(Request $request)
    {
        $updateValue = News_Page::find($request->newsId);
        $updateValue->from_date = $request->from_date;
        $updateValue->to_date = $request->to_date;
        $updateValue->update();
        return redirect()->back();
    }
    public function createheadline(Request $request)
    {
        Headline::create([
            'created_by' => $request->created_by,
            'name' => $request->name,
            'designation' => $request->designation,
            'headline' => $request->headline,
            'from_datetime' => $request->from_date,
            'to_datetime' => $request->to_date,
        ]);

        return redirect()->back();
    }
    public function deleteheadline($id)
    {
        Headline::destroy($id);
        return redirect()->back();
    }
    public function newslist_items_delete($id)
    {
        News_Page::destroy($id);
        return redirect()->back();
    }
    public function newslist_items_edit($id)
    {
        $news = News_Page::find($id);
        $news->department_ids = explode(',', $news->department_ids);
        return response()->json($news);
    }
    public function newslist_items_update(Request $request)
    {

        $request->validate([
            'edituser' => 'required',
            'editfrom_date' => 'required',
            'editto_date' => 'required',
            'editattachment_video' => 'max:30240',
            'editattachment_image' => 'max:10240',
            'editmessage' => '',
            'editnoticetype' => 'required',
            'editdesignation' => 'required',
            'editname' => 'required',
            'editdepartment' => 'required',
            'editnoticetype' => 'required',

        ]);

        $VideopathToStore = NULL;
        $FilepathToStore = NULL;
        if ($request->hasFile('editattachment_image')) {
            $fileNameExt = $request->file('editattachment_image')->getClientOriginalName();
            $fileName = pathinfo($fileNameExt, PATHINFO_FILENAME);
            $fileExt = $request->file('editattachment_image')->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '.' . $fileExt;
            $FilepathToStore = $request->file('editattachment_image')->storeAs('News-Images_Attachments', $fileNameToStore);
            $FilepathToStore = "../storage/app/" . $FilepathToStore;
        }
        if ($request->hasFile('editattachment_video')) {
            $fileNameExt = $request->file('editattachment_video')->getClientOriginalName();
            $fileName = pathinfo($fileNameExt, PATHINFO_FILENAME);
            $fileExt = $request->file('editattachment_video')->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '.' . $fileExt;
            $VideopathToStore = $request->file('editattachment_video')->storeAs('News-Videos_Attachments', $fileNameToStore);
            $VideopathToStore = "../storage/app/" . $VideopathToStore;
        }
        News_Page::find($request->toupdate)->update([
            'user_id' => $request->edituser,
            'created_by' => $request->created_by,
            'name'  => $request->editname,
            'designation'  => $request->editdesignation,
            'attachment_vedio'  => $VideopathToStore,
            'attachment_image'  =>  $FilepathToStore,
            'from_date'  => $request->editfrom_date,
            'to_date'  => $request->editto_date,
            'type'  => $request->editnoticetype,
            'message'  => $request->editmessage,
            'department_ids'  => implode(',', $request->editdepartment),
        ]);

        return redirect()->back();
    }
    public function  ChangeNewsStatus(Request $request, $id)
    {
        $news = News_Page::find($id);
        $news->watched_by = $news->watched_by.",".$request->user_id;
        $news->update();
        return redirect()->back();
    }
    public function headlines_items_edit($id)
    {
        $headlines = Headline::find($id);
        return response()->json($headlines);
    }
    public function add_headlines()
    {
        $headlines = Headline::get();
        return view('Admin.newsPage.add_headlines', get_defined_vars());
    }
}
