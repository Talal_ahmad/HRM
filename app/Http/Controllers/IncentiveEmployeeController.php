<?php

namespace App\Http\Controllers;

use DB;
use DataTables;
use Illuminate\Http\Request;
use App\Models\IncentiveEmployee;
use Illuminate\Validation\ValidationException;

class IncentiveEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = DB::table('incentive_employees')
            ->join('employees', 'employees.id', '=', 'incentive_employees.employee_id')
            ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
            ->leftjoin('companystructures', 'companystructures.id', '=', 'employees.department')
            ->select('incentive_employees.id','incentive_employees.status','companystructures.title','jobtitles.name as desigination',DB::raw("CONCAT(employees.employee_id,' - ',employees.employee_code,' - ',employees.first_name,' ',employees.middle_name,' ',employees.last_name) as fullname"));
            $datatables = Datatables::of($data);
            if ($keyword = $request->get('search')['value']) {
                $datatables->filterColumn('fullname', function($query, $keyword) {
                    $sql = "CONCAT(employees.employee_id,' - ',employees.employee_code,' - ',employees.first_name,' ',employees.last_name) like ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
                // $datatables->filterColumn('DT_RowIndex', function($query, $keyword) {
                //     $sql = "@DT_RowIndex  + 1 like ?";
                //     $query->whereRaw($sql, ["%{$keyword}%"]);
                // });
            }
            return $datatables->addIndexColumn()->make(true);
        }
        return view('Admin.finance.IncentiveEmployees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'department' => 'required',
                'employee_id' => 'required|unique:incentive_employees',
            ]);

            foreach($request->employee_id as $employee)
            {
                $incentive_employee = new IncentiveEmployee();
                $incentive_employee->employee_id = $employee;
                $incentive_employee->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
