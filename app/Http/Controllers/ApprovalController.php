<?php

namespace App\Http\Controllers;

use App\Models\Approval;
use App\Models\Employee;
use App\Models\LoanRequest;
use App\Models\JobTitle;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\ShiftManagement;
use App\Models\DepartmentChange;
use App\Models\ManualAttendanceRequest;
use App\Models\ShortLeaveRequest;
use App\Models\OverTimeManagement;
use Illuminate\Support\Facades\DB;
use App\Models\EmployeeLeaveRequest;
use App\Models\FineReversal;
use App\Models\MiscellaneousRequest;
use App\Models\EmployeeSalary;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        $type = request()->query('type');
        // dd($type);

        // Department Change
        $department_change_requests = DepartmentChange::leftjoin('employees' , 'employees.id' , '=' , 'department_requests.employee_id')
        ->leftjoin('companystructures AS cs' , 'cs.id' , '=' , 'department_requests.dept_id')
        ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'department_requests.dept_from')
        ->leftjoin('requests' , 'requests.id' , '=' , 'department_requests.request_type')
        ->leftjoin('leavetypes' , 'leavetypes.id' , '=' , 'department_requests.request_type')
        ->join('users' , 'users.id' , '=' , 'department_requests.submitted_by')
        // ->leftjoin('users as test' , 'test.id' , '=' , 'department_requests.status_changed_by')
        ->where('employees.status' , 'Active')
        ->whereIn('employees.department' , login_user_departments());
        if(!empty(request()->departmentFilter)){
            if (request()->departmentFilter!='all') {
                $department_change_requests->where('employees.department' , request()->departmentFilter);
            }
        }
        if(!empty(request()->employeeFilter)){
            $department_change_requests->where('department_requests.employee_id' , request()->employeeFilter);
        }
        if (userRoles()=="Self Service") {
            $department_change_requests->where('department_requests.employee_id' , Auth::user()->employee);
        }
        if(!empty(request()->designationFilter)){
            $department_change_requests->where('employees.job_title' , request()->designationFilter);
        }
        if(!empty(request()->statusFilter)){
            if (request()->statusFilter!='all') {
                $department_change_requests->where('department_requests.status' , request()->statusFilter);
            }
        }else{
            $department_change_requests->where('department_requests.status' , 'Pending');
        }
        if (userRoles()!="Self Service") {
            if(!empty(request()->selectFromDate)){
                $from = request()->selectFromDate;
                if(empty(request()->selectToDate)){
                    $to = Carbon::now();
                }else {
                    $to = request()->selectToDate;
                    // $to >= $to ;
                }
                // $department_change_requests->whereBetween('department_requests.created_at', [$from, $to]);
                // $department_change_requests->where('department_requests.created_at', '<=' , $to)->where('department_requests.created_at', '>=' , $from);
                $department_change_requests->whereDate('department_requests.created_at', '>=' , $from)->whereDate('department_requests.created_at', '<=' , $to);
                // dd($department_change_requests);
            }else{
                $department_change_requests->whereYear('department_requests.created_at', Carbon::now()->year);
                $department_change_requests->whereMonth('department_requests.created_at', Carbon::now()->month);
            }
        }
        // $department_change_requests = $department_change_requests->select('department_requests.*' ,'test.username as approvedBy', 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.image' , 'cs.title AS dept_to' , 'companystructures.title' , 'requests.id AS request_id' , 'requests.title AS request_title' , 'users.username AS creator')
        // ->orderBy('department_requests.created_at' , 'desc')
        if ($type == null) {
            $department_change_requests = $department_change_requests->select('department_requests.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.image','employees.employee_id','employees.employee_code' ,'cs.title AS dept_to' , 'companystructures.title' , 'requests.id AS request_id' , 'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(department_requests.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            ->get();

        } else {
            $department_change_requests = $department_change_requests->select('department_requests.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.image','employees.employee_id' ,'employees.employee_code' , 'cs.title AS dept_to' , 'companystructures.title' , 'requests.id AS request_id' , 'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(department_requests.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            ->paginate(10);
        }

        // dd($department_change_requests);

        // Leave Request
        $leave_requests = EmployeeLeaveRequest::leftjoin('employees' , 'employees.id' , '=' , 'employeeleaves.employee')
        ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->leftjoin('leavetypes' , 'leavetypes.id' , '=' , 'employeeleaves.leave_type')
        ->leftjoin('requests' , 'requests.id' , '=' , 'employeeleaves.request_type')
        ->join('users' , 'users.id' , '=' , 'employeeleaves.submitted_by')
        ->where('employees.status' , 'Active')
        ->whereIn('employees.department' , login_user_departments());
        if(!empty(request()->departmentFilter)){
            if (request()->departmentFilter!='all') {
                $leave_requests->where('employees.department' , request()->departmentFilter);
            }
        }
        if(!empty(request()->designationFilter)){
            $leave_requests->where('employees.job_title' , request()->designationFilter);
        }
        if(!empty(request()->employeeFilter)){
            $leave_requests->where('employeeleaves.employee' , request()->employeeFilter);
        }
        if (userRoles()=="Self Service") {
            $leave_requests->where('employeeleaves.employee' , Auth::user()->employee);
        }
        if(!empty(request()->statusFilter)){
            if (request()->statusFilter!='all') {
                $leave_requests->where('employeeleaves.status' , request()->statusFilter);
            }
        }else{
            $leave_requests->where('employeeleaves.status' , 'Pending');
        }
        if (userRoles()!="Self Service") {
            if(!empty(request()->selectFromDate)){
                $from = request()->selectFromDate;
                if(empty(request()->selectToDate)){
                    $to = Carbon::now();
                }else {
                    $to = request()->selectToDate;
                }
                $leave_requests->whereDate('employeeleaves.created_at', '>=' , $from)->whereDate('employeeleaves.created_at', '<=' , $to);
            }else{
                $leave_requests->whereYear('employeeleaves.created_at', Carbon::now()->year);
                $leave_requests->whereMonth('employeeleaves.created_at', Carbon::now()->month);
            }
        }
        if ($type == null) {
            $leave_requests = $leave_requests->select('employeeleaves.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.image' ,'employees.employee_id' ,'employees.employee_code' , 'leavetypes.name AS lt_name' , 'companystructures.title' , 'requests.id AS request_id' ,  'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(employeeleaves.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            ->get();
        } else {
            $leave_requests = $leave_requests->select('employeeleaves.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.image' ,'employees.employee_id' ,'employees.employee_code' , 'leavetypes.name AS lt_name' , 'companystructures.title' , 'requests.id AS request_id' ,  'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(employeeleaves.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            // ->orderBy('employeeleaves.created_at' , 'desc')
            ->paginate(10);

        }

        // Short Leave Request
        $short_leave_requests = ShortLeaveRequest::leftjoin('employees' , 'employees.id' , '=' , 'short_leave_requests.employee_id')
        ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->join('users' , 'users.id' , '=' , 'short_leave_requests.submitted_by')
        ->where('employees.status' , 'Active')
        ->whereIn('employees.department' , login_user_departments());
        if(!empty(request()->departmentFilter)){
            if (request()->departmentFilter!='all') {
                $short_leave_requests->where('employees.department' , request()->departmentFilter);
            }
        }
        if(!empty(request()->employeeFilter)){
            $short_leave_requests->where('short_leave_requests.employee_id' , request()->employeeFilter);
        }
        if (userRoles()=="Self Service") {
            $short_leave_requests->where('short_leave_requests.employee_id' , Auth::user()->employee);
        }
        if(!empty(request()->designationFilter)){
            $short_leave_requests->where('employees.job_title' , request()->designationFilter);
        }
        if(!empty(request()->statusFilter)){
            if (request()->statusFilter!='all') {
                $short_leave_requests->where('short_leave_requests.status' , request()->statusFilter);
            }
        }else{
            $short_leave_requests->where('short_leave_requests.status' , 'Pending');
        }
        if (userRoles()!="Self Service") {
            if(!empty(request()->selectFromDate)){
                $from = request()->selectFromDate;
                if(empty(request()->selectToDate)){
                    $to = Carbon::now();
                }else {
                    $to = request()->selectToDate;
                }
                $short_leave_requests->whereDate('short_leave_requests.created_at', '>=' , $from)->whereDate('short_leave_requests.created_at', '<=' , $to);
            }else{
                $short_leave_requests->whereYear('short_leave_requests.created_at', Carbon::now()->year);
                $short_leave_requests->whereMonth('short_leave_requests.created_at', Carbon::now()->month);
            }
        }
        if ($type == null) {
            $short_leave_requests = $short_leave_requests->select('short_leave_requests.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.image' , 'companystructures.title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(short_leave_requests.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            ->get();
        } else {
            $short_leave_requests = $short_leave_requests->select('short_leave_requests.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.image' , 'companystructures.title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(short_leave_requests.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            // ->orderBy('short_leave_requests.created_at' , 'desc')
            ->paginate(10);
        }

        // Overtime Request
        $overtime_requests = OverTimeManagement::leftjoin('employees' , 'employees.id' , '=' , 'overtime_management.employee_id')
        ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->leftjoin('requests' , 'requests.id' , '=' , 'overtime_management.request_type')
        ->join('users' , 'users.id' , '=' , 'overtime_management.submitted_by')
        ->select('overtime_management.overtime')
        ->where('employees.status' , 'Active')
        ->whereIn('employees.department' , login_user_departments());
        if(!empty(request()->departmentFilter)){
            if (request()->departmentFilter!='all') {
                $overtime_requests->where('employees.department' , request()->departmentFilter);
            }
        }
        if(!empty(request()->employeeFilter)){
            $overtime_requests->where('overtime_management.employee_id' , request()->employeeFilter);
        }
        if (userRoles()=="Self Service") {
            $overtime_requests->where('overtime_management.employee_id' , Auth::user()->employee);
        }
        if(!empty(request()->designationFilter)){
            $overtime_requests->where('employees.job_title' , request()->designationFilter);
        }
        if(!empty(request()->statusFilter)){
            if (request()->statusFilter!='all') {
                $overtime_requests->where('overtime_management.status' , request()->statusFilter);
            }
        }else{
            $overtime_requests->where('overtime_management.status' , 'Pending');
        }
        if (userRoles()!="Self Service") {
            if(!empty(request()->selectFromDate)){
                $from = request()->selectFromDate;
                if(empty(request()->selectToDate)){
                    $to = Carbon::now();
                }else {
                    $to = request()->selectToDate;
                }
                $overtime_requests->whereDate('overtime_management.created_at', '>=' , $from)->whereDate('overtime_management.created_at', '<=' , $to);
            }else{
                $overtime_requests->whereYear('overtime_management.created_at', Carbon::now()->year);
                $overtime_requests->whereMonth('overtime_management.created_at', Carbon::now()->month);
            }
        }
        if ($type == null) {
            $overtime_requests = $overtime_requests->select('overtime_management.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.image' , 'companystructures.title' , 'requests.id AS request_id' , 'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(overtime_management.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            ->get();
        } else {
            $overtime_requests = $overtime_requests->select('overtime_management.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.image' , 'companystructures.title' , 'requests.id AS request_id' , 'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(overtime_management.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            // ->orderBy('overtime_management.created_at' , 'desc')
            ->paginate(10);
        }


        // Shift Change Request
        $shift_change_requests = ShiftManagement::leftjoin('employees' , 'employees.id' , '=' , 'shift_management.employee_id')
        ->leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
        ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->leftjoin('requests' , 'requests.id' , '=' , 'shift_management.request_type')
        // ->join('users' , 'users.id' , '=' , 'shift_management.submitted_by')
        ->where('employees.status' , 'Active')
        ->whereIn('employees.department' , login_user_departments());
        if(!empty(request()->departmentFilter)){
            if (request()->departmentFilter!='all') {
                $shift_change_requests->where('employees.department' , request()->departmentFilter);
            }
        }
        if(!empty(request()->employeeFilter)){
            $shift_change_requests->where('shift_management.employee_id' , request()->employeeFilter);
        }
        if (userRoles()=="Self Service") {
            $shift_change_requests->where('shift_management.employee_id' , Auth::user()->employee);
        }
        if(!empty(request()->designationFilter)){
            $shift_change_requests->where('employees.job_title' , request()->designationFilter);
        }
        if(!empty(request()->statusFilter)){
            if (request()->statusFilter!='all') {
                $shift_change_requests->where('shift_management.status' , request()->statusFilter);
            }
        }else{
            $shift_change_requests->where('shift_management.status' , 'Pending');
        }
        if(userRoles()!="Self Service"){
            if(!empty(request()->selectFromDate)){
                $from = request()->selectFromDate;
                if(empty(request()->selectToDate)){
                    $to = Carbon::now();
                }else {
                    $to = request()->selectToDate;
                }
                $shift_change_requests->whereDate('shift_management.created_at', '>=' , $from)->whereDate('shift_management.created_at', '<=' , $to);
            }else{
                $shift_change_requests->whereYear('shift_management.created_at', Carbon::now()->year);
                $shift_change_requests->whereMonth('shift_management.created_at', Carbon::now()->month);
            }
        }
        if ($type == null) {
            $shift_change_requests = $shift_change_requests->select('shift_management.*' , 'employees.first_name' , 'employees.last_name' , 'employees.middle_name' , 'employees.image' , 'companystructures.title' , 'shift_type.shift_desc' , 'requests.id AS request_id' ,'requests.title AS request_title')
            ->orderBy(DB::raw('FIELD(shift_management.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            ->get();
        } else {
            $shift_change_requests = $shift_change_requests->select('shift_management.*' , 'employees.first_name' , 'employees.last_name' , 'employees.middle_name' , 'employees.image' , 'companystructures.title' , 'shift_type.shift_desc' , 'requests.id AS request_id' ,'requests.title AS request_title')
            ->orderBy(DB::raw('FIELD(shift_management.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            // ->orderBy('shift_management.created_at' , 'desc')
            ->paginate(10);
        }


        // Loan Requests
        $loan_requests = LoanRequest::leftjoin('employees' , 'employees.id', '=' , 'loan_requests.employee_id')
        ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->leftjoin('requests' , 'requests.id' , '=' , 'loan_requests.request_type')
        ->leftjoin('loan_payment_details' , 'loan_payment_details.loan_request_id' , '=' , 'loan_requests.id')
        ->join('users' , 'users.id' , '=' , 'loan_requests.submitted_by')
        ->where('employees.status' , 'Active')
        ->whereIn('employees.department' , login_user_departments());
        if(!empty(request()->departmentFilter)){
            if (request()->departmentFilter!='all') {
                $loan_requests->where('employees.department' , request()->departmentFilter);
            }
        }
        if(!empty(request()->designationFilter)){
            $loan_requests->where('employees.job_title' , request()->designationFilter);
        }
        if(!empty(request()->employeeFilter)){
            $loan_requests->where('loan_requests.employee_id' , request()->employeeFilter);
        }
        if (userRoles()=="Self Service") {
            $loan_requests->where('loan_requests.employee_id' , Auth::user()->employee);
        }
        if(!empty(request()->statusFilter)){
            if (request()->statusFilter!='all') {
                $loan_requests->where('loan_requests.status' , request()->statusFilter);
            }
        }else{
            $loan_requests->where('loan_requests.status' , 'Pending');
        }
        if (userRoles()!="Self Service") {
            if(!empty(request()->selectFromDate)){
                $from = request()->selectFromDate;
                if(empty(request()->selectToDate)){
                    $to = Carbon::now();
                }else {
                    $to = request()->selectToDate;
                }
                $loan_requests->whereDate('loan_requests.created_at', '>=' , $from)->whereDate('loan_requests.created_at', '<=' , $to);
            }else{
                $loan_requests->whereYear('loan_requests.created_at', Carbon::now()->year);
                $loan_requests->whereMonth('loan_requests.created_at', Carbon::now()->month);
            }
        }
        if ($type == null) {
            $loan_requests = $loan_requests->select('loan_requests.*' , 'employees.first_name' ,'employees.middle_name', 'employees.last_name', 'employees.image' , 'companystructures.title' ,'loan_payment_details.id as payment_id', 'requests.id AS request_id' , 'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(loan_requests.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            ->get();
        } else {
            $loan_requests = $loan_requests->select('loan_requests.*' , 'employees.first_name' ,'employees.middle_name', 'employees.last_name', 'employees.image' , 'companystructures.title' ,'loan_payment_details.id as payment_id', 'requests.id AS request_id' , 'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(loan_requests.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            // ->orderBy('loan_requests.created_at' , 'desc')
            ->paginate(10);
        }


        // Miscelleneous Request
        $miscellaneous_requests = MiscellaneousRequest::leftjoin('employees' , 'employees.id', '=' , 'miscellaneous.employee_id')
        ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->leftjoin('requests' , 'requests.id' , '=' , 'miscellaneous.request_type')
        ->join('users' , 'users.id' , '=' , 'miscellaneous.submitted_by')
        ->where('employees.status' , 'Active')
        ->whereIn('employees.department' , login_user_departments());
        if(!empty(request()->departmentFilter)){
            if (request()->departmentFilter!='all') {
                $miscellaneous_requests->where('employees.department' , request()->departmentFilter);
            }
        }
        if(!empty(request()->employeeFilter)){
            $miscellaneous_requests->where('miscellaneous.employee_id' , request()->employeeFilter);
        }
        if (userRoles()=="Self Service") {
            $miscellaneous_requests->where('miscellaneous.employee_id' , Auth::user()->employee);
        }
        if(!empty(request()->designationFilter)){
            $miscellaneous_requests->where('employees.job_title' , request()->designationFilter);
        }
        if(!empty(request()->statusFilter)){
            if (request()->statusFilter!='all') {
                $miscellaneous_requests->where('miscellaneous.status' , request()->statusFilter);
            }
        }else{
            $miscellaneous_requests->where('miscellaneous.status' , 'Pending');
        }
        if (userRoles()!="Self Service") {
            if(!empty(request()->selectFromDate)){
                $from = request()->selectFromDate;
                if(empty(request()->selectToDate)){
                    $to = Carbon::now();
                }else {
                    $to = request()->selectToDate;
                }
                $miscellaneous_requests->whereDate('miscellaneous.created_at', '>=' , $from)->whereDate('miscellaneous.created_at', '<=' , $to);
            }else{
                $miscellaneous_requests->whereYear('miscellaneous.created_at', Carbon::now()->year);
                $miscellaneous_requests->whereMonth('miscellaneous.created_at', Carbon::now()->month);
            }
        }
        if ($type == null) {
            $miscellaneous_requests = $miscellaneous_requests->select('miscellaneous.*' , 'employees.first_name' , 'employees.last_name' ,'employees.middle_name', 'employees.image' , 'companystructures.title' , 'requests.id AS request_id' , 'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(miscellaneous.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            ->get();
        } else {
            $miscellaneous_requests = $miscellaneous_requests->select('miscellaneous.*' , 'employees.first_name' , 'employees.last_name' ,'employees.middle_name', 'employees.image' , 'companystructures.title' , 'requests.id AS request_id' , 'requests.title AS request_title' , 'users.username AS creator')
            ->orderBy(DB::raw('FIELD(miscellaneous.status, "Pending", "Rejected", "Approved" , "Cancelled")'))
            // ->orderBy('miscellaneous.created_at' , 'desc')
            ->paginate(10);
        }
           $fines= FineReversal::join('employees','fine_reversal_requests.employee_id','=','employees.id')
           ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
           ->leftjoin('users' , 'fine_reversal_requests.submitted_by' , '=' , 'users.id')
        //    ->leftjoin('users as users_changed', 'fine_reversal_requests.status_changed_by', '=', 'users_changed.id')
           ->select('fine_reversal_requests.*','employees.first_name',
           'employees.middle_name','employees.last_name',
           'companystructures.title','users.username','employees.department');
           if(!empty(request()->departmentFilter)){
            if (request()->departmentFilter!='all') {
                $fines->where('employees.department' , request()->departmentFilter);
            }
        }
        if(!empty(request()->employeeFilter)){
            $fines->where('fine_reversal_requests.employee_id' , request()->employeeFilter);
        }
        if (userRoles()=="Self Service") {
            $fines->where('fine_reversal_requests.employee_id' , Auth::user()->employee);
        }
        if(!empty(request()->designationFilter)){
            $fines->where('employees.job_title' , request()->designationFilter);
        }
        if(!empty(request()->statusFilter)){
            if (request()->statusFilter!='all') {
            $fines->where('fine_reversal_requests.status' , request()->statusFilter);
        }
    }
        if (userRoles()!="Self Service") {
            if(!empty(request()->selectFromDate)){
                $from = request()->selectFromDate;
                if(empty(request()->selectToDate)){
                    $to = Carbon::now();
                }else {
                    $to = request()->selectToDate;
                    // $to >= $to ;
                }
                $fines->whereDate('fine_reversal_requests.created_at', '>=' , $from)->whereDate('fine_reversal_requests.created_at', '<=' , $to);

            }
        }
        $fines= $fines->paginate(10);

        $salaryAllownaces = DB::table('employeesalary_pendings')->select('employeesalary_pendings.id','employeesalary_pendings.status','employeesalary_pendings.employee','employeesalary_pendings.updated_at','employees.first_name','employees.middle_name', 'employees.last_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation' , 'users.username AS submitted_by' , 'employees.employee_code' , 'employees.job_title' , 'users.username AS creator')
                ->selectRaw('DATE(employeesalary_pendings.updated_at) AS created_at')
                ->leftjoin('employees' , 'employees.id' , '=' , 'employeesalary_pendings.employee')
                ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->leftjoin('users' , 'users.id' , '=' , 'employeesalary_pendings.submitted_by')
                ->where('employees.status' , 'Active')
                ->where('employeesalary_pendings.status', request()->statusFilter)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty(request()->departmentFilter) && request()->departmentFilter != 'all'){
                    $salaryAllownaces->where('employees.department' , request()->departmentFilter);
                }
                if(!empty(request()->designationFilter) && request()->designationFilter != 'all'){
                    $salaryAllownaces->where('employees.job_title', request()->designationFilter);
                }
                if(!empty($from) && !empty($to)){
                    $salaryAllownaces->whereDate('employeesalary_pendings.updated_at', '>=',$from)->whereDate('employeesalary_pendings.updated_at', '<=',$to);
                }
                $salaryAllownaces = $salaryAllownaces->groupBy('employeesalary_pendings.employee')->get()
                ->map(function($salaryAllownace, $key) {
                    $components = DB::table('employeesalary_pendings')->select('employeesalary_pendings.id','employeesalary_pendings.amount','employeesalary_pendings.remarks','employeesalary_pendings.status','salarycomponent.name')
                    ->join('salarycomponent' , 'salarycomponent.id' , '=' , 'employeesalary_pendings.component')
                    ->where('is_edit',1)
                    ->where('employeesalary_pendings.employee',$salaryAllownace->employee)->get();
                    foreach ($components as $key => $component) {
                        $salaryAllownace->components[] = $component;
                    }

                    return $salaryAllownace;
                });


        $job_titles = JobTitle::get();
        return view('Admin.request&approvals.approvals.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Approval  $approval
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request , $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Approval  $approval
     * @return \Illuminate\Http\Response
     */
    public function edit(Approval $approval)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Approval  $approval
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        try{
            if($request->type == 'department_change')
            {
                $data = DepartmentChange::find($id);
                $data->print_status = 'Printed';
                $data->update();
            }
            elseif ($request->type == 'leave_request') {
                $data = EmployeeLeaveRequest::find($id);
                $data->print_status = 'Printed';
                $data->update();
            }
            elseif ($request->type == 'short_leave') {
                $data = ShortLeaveRequest::find($id);
                $data->print_status = 'Printed';
                $data->update();
            }
            elseif ($request->type == 'overtime_request') {
                $data = OverTimeManagement::find($id);
                $data->print_status = 'Printed';
                $data->update();
            }
            elseif ($request->type == 'loan_request') {
                $data = LoanRequest::find($id);
                $data->print_status = 'Printed';
                $data->update();
            }
            elseif ($request->type == 'shift_change') {
                $data = ShiftManagement::find($id);
                $data->print_status = 'Printed';
                $data->update();
            }
            elseif ($request->type == 'fineconcession') {
                $data = FineReversal::find($id);
                $data->print_status = 'Printed';
                $data->update();
            }
            else {
                $data = MiscellaneousRequest::find($id);
                $data->print_status = 'Printed';
                $data->update();
            }

            return ['code'=>'200','message'=>'success','data' => $data];
        }
        catch(\Exception $e){
            return response(['error_message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Approval  $approval
     * @return \Illuminate\Http\Response
     */
    public function destroy(Approval $approval)
    {
        //
    }

    public function statusUpdate(Request $request , $id){
        try{
            if($request->ajax()){
                // dd($id);
                if($request->type == 'Approved'){
                    if($request->data_type == 'department_change'){
                        $department_change = DepartmentChange::find($id);
                        $department_change->status = 'Approved';
                        $department_change->status_changed_by = Auth::id();
                        $department_change->status_changed_at = Carbon::now();
                        $department_change->update();
                        $employee = Employee::where('id' , $department_change->employee_id)->first();
                        $employee->department = $department_change->dept_id;
                        $employee->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'manual_attendance'){
                        $leave_requests = ManualAttendanceRequest::find($id);
                        $leave_requests->status = 'Approved';
                        $leave_requests->status_changed_by = Auth::id();
                        $leave_requests->status_changed_at = Carbon::now();
                        $leave_requests->update();
                        DB::table('attendance')->insert([
                            'employee' => $leave_requests->employee_id,
                            'in_time' => $leave_requests->in_date.' '.$leave_requests->in_time,
                            'out_time' => $leave_requests->out_date.' '.$leave_requests->out_time,
                            'department_id' => $leave_requests->department_id,
                            'is_online' => 1,
                            'is_manual' => 1,
                            'shift_id' => $leave_requests->shift_id,
                            'shift_start_time' => $leave_requests->shift_start_time,
                            'shift_end_time' => $leave_requests->shift_end_time,
                            'work_week_id' => $leave_requests->work_week_id,
                            'late_time_allowed' => $leave_requests->late_time_allowed,
                            'note' => $leave_requests->note,
                        ]);
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'leave_request'){
                        $leave_requests = EmployeeLeaveRequest::find($id);
                        $leave_requests->status = 'Approved';
                        $leave_requests->status_changed_by = Auth::id();
                        $leave_requests->status_changed_at = Carbon::now();
                        $leave_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'short_leave'){
                        $short_leave_requests = ShortLeaveRequest::find($id);
                        $short_leave_requests->status = 'Approved';
                        $short_leave_requests->status_changed_by = Auth::id();
                        $short_leave_requests->status_changed_at = Carbon::now();
                        $short_leave_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'overtime_request'){
                        $overtime_requests = OverTimeManagement::find($id);
                        $overtime_requests->approved = 1;
                        $overtime_requests->status = 'Approved';
                        $overtime_requests->status_changed_by = Auth::id();
                        $overtime_requests->status_changed_at = Carbon::now();
                        $overtime_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'shift_change'){
                        $shift_change = ShiftManagement::find($id);
                        $shift_change->status = 'Approved';
                        $shift_change->status_changed_by = Auth::id();
                        $shift_change->status_changed_at = Carbon::now();
                        $shift_change->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'loan_request'){
                        $loan_requests = LoanRequest::find($id);
                        $loan_requests->status = 'Approved';
                        $loan_requests['status_changed_by'] = Auth::id();
                        $loan_requests['status_changed_at'] = Carbon::now();
                        $loan_requests->update();
                        $start_date = new Carbon($loan_requests->start_date);
                        for($i = 0 ; $i < $loan_requests->time_duration ; $i++){
                            $checkInstallment = DB::table('loan_installments')->where('loan_request_id', $id)->whereYear('month', $start_date->year)->whereMonth('month', $start_date->format('m'))->first(['id']);
                            if(empty($checkInstallment)){
                                DB::table('loan_installments')->insert([
                                    'loan_request_id' => $loan_requests->id,
                                    'month' => $start_date,
                                    'amount' => $loan_requests->monthly_installment,
                                    'status' => 'active',
                                ]);
                            }
                            $start_date = $start_date->addMonths(1);
                        }
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'miscellaneous_request'){
                        $miscellaneous = MiscellaneousRequest::find($id);
                        $miscellaneous->status = 'Approved';
                        $miscellaneous->status_changed_by = Auth::id();
                        $miscellaneous->status_changed_at = Carbon::now();
                        $miscellaneous->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'fineconcession'){
                        FineReversal::find($id)->update([
                            'status' => 'Approved',
                            'status_changed_by' => auth()->user()->id,
                            'status_changed_at' => date('Y-m-d H:i:s',time())
                        ]);
                        return ['code'=>'200','message'=>'success'];

                    }
                    if($request->data_type == 'salaryComponentRequest'){
                        DB::table('employeesalary_pendings')->where('id', $id)->update(['status' => 'Approved']);
                        $salaryAllowances = DB::table('employeesalary_pendings')->join('salarycomponent','salarycomponent.id','=','employeesalary_pendings.component')->where('salarycomponent.is_edit',1)->where('employeesalary_pendings.id', $id)->get(['employeesalary_pendings.*']);
                        if(count($salaryAllowances) > 0){
                            foreach ($salaryAllowances as $key => $allowance) {
                                $salary = EmployeeSalary::where('employee',$allowance->employee)->where('component',$allowance->component)->first();
                                if(!empty($salary)){
                                    $employeeSalary = EmployeeSalary::find($salary->id);
                                    $employeeSalary->amount = $allowance->amount;
                                    $employeeSalary->status = 'Approved';
                                    $employeeSalary->submitted_by = $allowance->submitted_by;
                                    $employeeSalary->details = $allowance->remarks;
                                    $employeeSalary->update();
                                }
                                else{
                                    $employeeSalary = new EmployeeSalary();
                                    $employeeSalary->employee = $allowance->employee;
                                    $employeeSalary->component = $allowance->component;
                                    $employeeSalary->amount = $allowance->amount;
                                    $employeeSalary->status = 'Approved';
                                    $employeeSalary->submitted_by = $allowance->submitted_by;
                                    $employeeSalary->details = $allowance->remarks;
                                    $employeeSalary->save();
                                }
                                $calculation_group = NULL;
                                insertInSalaryComponentHistory($allowance->employee,$allowance->component,$allowance->amount,$calculation_group,$allowance->remarks);
                            }
                        }
                        return ['code'=>'200','message'=>'success'];
                    }

                }
                elseif($request->type == 'Rejected'){
                    if($request->data_type == 'manual_attendance'){
                        $manual_attendance = ManualAttendanceRequest::find($id);
                        $manual_attendance->status = 'Rejected';
                        $manual_attendance->rejection_remarks = $request->rejection_remarks;
                        $manual_attendance->status_changed_by = Auth::id();
                        $manual_attendance->status_changed_at = Carbon::now();
                        $manual_attendance->update();
                    }
                    if($request->data_type == 'department_change'){
                        $department_change = DepartmentChange::find($id);
                        $department_change->status = 'Rejected';
                        $department_change->rejection_remarks = $request->rejection_remarks;
                        $department_change->status_changed_by = Auth::id();
                        $department_change->status_changed_at = Carbon::now();
                        $department_change->update();
                    }
                    if($request->data_type == 'leave_request'){
                        $leave_requests = EmployeeLeaveRequest::find($id);
                        $leave_requests->status = 'Rejected';
                        $leave_requests->rejection_remarks = $request->rejection_remarks;
                        $leave_requests->status_changed_by = Auth::id();
                        $leave_requests->status_changed_at = Carbon::now();
                        $leave_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'short_leave'){
                        $short_leave_requests = ShortLeaveRequest::find($id);
                        $short_leave_requests->status = 'Rejected';
                        $short_leave_requests->rejection_remarks = $request->rejection_remarks;
                        $short_leave_requests->status_changed_by = Auth::id();
                        $short_leave_requests->status_changed_at = Carbon::now();
                        $short_leave_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'overtime_request'){
                        $overtime_requests = OverTimeManagement::find($id);
                        $overtime_requests->status = 'Rejected';
                        $overtime_requests->rejection_remarks = $request->rejection_remarks;
                        $overtime_requests->status_changed_by = Auth::id();
                        $overtime_requests->status_changed_at = Carbon::now();
                        $overtime_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'shift_change'){
                        $shift_change = ShiftManagement::find($id);
                        $shift_change->status = 'Rejected';
                        $shift_change->rejection_remarks = $request->rejection_remarks;
                        $shift_change->status_changed_by = Auth::id();
                        $shift_change->status_changed_at = Carbon::now();
                        $shift_change->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'loan_request'){
                        $loan_requests = LoanRequest::find($id);
                        $loan_requests->status = 'Rejected';
                        $loan_requests->rejection_remarks = $request->rejection_remarks;
                        $loan_requests['status_changed_by'] = Auth::id();
                        $loan_requests['status_changed_at'] = Carbon::now();
                        $loan_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'miscellaneous_request'){
                        $miscellaneous = MiscellaneousRequest::find($id);
                        $miscellaneous->status = 'Rejected';
                        $miscellaneous->rejection_remarks = $request->rejection_remarks;
                        $miscellaneous->status_changed_by = Auth::id();
                        $miscellaneous->status_changed_at = Carbon::now();
                        $miscellaneous->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'fineconcession'){
                        FineReversal::find($id)->update([
                            'status' => 'Rejected',
                            'status_changed_by' => auth()->user()->id,
                            'status_changed_at' => date('Y-m-d H:i:s',time())
                        ]);
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'salaryComponentRequest'){
                        DB::table('employeesalary_pendings')->where('id', $id)->update(['status' => 'Rejected']);
                        return ['code'=>'200','message'=>'success'];
                    }
                }
                else{
                    if($request->data_type == 'manual_attendance'){
                        $department_change = ManualAttendanceRequest::find($id);
                        $department_change->status = 'Cancelled';
                        $department_change->status_changed_by = Auth::id();
                        $department_change->status_changed_at = Carbon::now();
                        $department_change->update();
                    }
                    if($request->data_type == 'department_change'){
                        $department_change = DepartmentChange::find($id);
                        $department_change->status = 'Cancelled';
                        $department_change->status_changed_by = Auth::id();
                        $department_change->status_changed_at = Carbon::now();
                        $department_change->update();
                    }
                    if($request->data_type == 'leave_request'){
                        $leave_requests = EmployeeLeaveRequest::find($id);
                        $leave_requests->status = 'Cancelled';
                        $leave_requests->status_changed_by = Auth::id();
                        $leave_requests->status_changed_at = Carbon::now();
                        $leave_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'short_leave'){
                        $short_leave_requests = ShortLeaveRequest::find($id);
                        $short_leave_requests->status = 'Cancelled';
                        $short_leave_requests->status_changed_by = Auth::id();
                        $short_leave_requests->status_changed_at = Carbon::now();
                        $short_leave_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'overtime_request'){
                        $overtime_requests = OverTimeManagement::find($id);
                        $overtime_requests->status = 'Cancelled';
                        $overtime_requests->status_changed_by = Auth::id();
                        $overtime_requests->status_changed_at = Carbon::now();
                        $overtime_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'shift_change'){
                        $shift_change = ShiftManagement::find($id);
                        $shift_change->status = 'Cancelled';
                        $shift_change->status_changed_by = Auth::id();
                        $shift_change->status_changed_at = Carbon::now();
                        $shift_change->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'loan_request'){
                        $loan_requests = LoanRequest::find($id);
                        $loan_requests->status = 'Cancelled';
                        $loan_requests['status_changed_by'] = Auth::id();
                        $loan_requests['status_changed_at'] = Carbon::now();
                        $loan_requests->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'miscellaneous_request'){
                        $miscellaneous = MiscellaneousRequest::find($id);
                        $miscellaneous->status_changed_by = Auth::id();
                        $miscellaneous->status_changed_at = Carbon::now();
                        $miscellaneous->status = 'Cancelled';
                        $miscellaneous->update();
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'fineconcession'){
                        FineReversal::find($id)->update([
                            'status' => 'Cancelled',
                            'status_changed_by' => auth()->user()->id,
                            'status_changed_at' => date('Y-m-d H:i:s',time())
                        ]);
                        return ['code'=>'200','message'=>'success'];
                    }
                    if($request->data_type == 'salaryComponentRequest'){
                        DB::table('employeesalary_pendings')->where('id', $id)->update(['status' => 'Cancelled']);
                        return ['code'=>'200','message'=>'success'];
                    }
                }
            }
        }catch(\Exception $e){
            return response(['error_message' => $e->getMessage()]);
        }
    }
}
