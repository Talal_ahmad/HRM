<?php

namespace App\Http\Controllers;
use App\Models\FinalSettlementSetup;
use App\Models\PayrollColumn;
use DataTables;
use DB;
use Illuminate\Http\Request;

class FinalSettlementSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return DataTables::eloquent(
                FinalSettlementSetup::select('final_settlement_setup.*')
            )->addIndexColumn()->make(true);
        }
        $payrollColumns = PayrollColumn::get(['id', 'name']);
        return view('Admin.setup.final_settlement_setup.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // $this->validate($request, [
            //     'name' => 'required|unique:final_settlement_setup,name',
            //     'status' => 'required',
            // ]);

            $data = $request->all();
            FinalSettlementSetup::create($data);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $final_settlement_edit = FinalSettlementSetup::find($id);
        return response()->json($final_settlement_edit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            // $this->validate($request, [
            //     'name' => 'required',
            //     'status' => 'required',
            // ]);

            $coulmn_name = FinalSettlementSetup::find($id);
            $requestData = $request->all();
            foreach ($coulmn_name->getAttributes() as $column => $value) {
                if (!isset($requestData[$column])) {
                    $requestData[$column] = 0;
                }
            }
            $coulmn_name->fill($requestData)->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $column_name = FinalSettlementSetup::find($id);
            $column_name->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
