<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\Document;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\CompanyDocument;
use App\Models\CompanyStructure;
use App\Models\EmployeeDocument;
use App\Models\DocumentManagement;
use Illuminate\Validation\ValidationException;


class DocumentManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            // Company Documents
            if($request->type == 'company_documents'){
                return DataTables::eloquent(CompanyDocument::query())->addIndexColumn()->make(true);
            }
            // Document Types
            elseif($request->type == 'document_types'){
                return Datatables::eloquent(Document::query())->addIndexColumn()->make(true);
            }
            // Employee Documents
            else{
                $query = EmployeeDocument::leftjoin('employees', 'employeedocuments.employee' , '=' , 'employees.id')
                ->leftjoin('documents' , 'employeedocuments.document' , '=' ,'documents.id')
                ->select('employeedocuments.*' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'documents.name')->get();
                
                return Datatables::of($query)->addIndexColumn()->make(true);
            }
        }
        $document_type = Document::get();
        return view('Admin.setup.document_management.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Company Documents
            if($request->type == 'company_documents'){
                $this->validate($request, [
                    'name' => 'required',
                    'status' => 'required',
                    'details' => 'required',
                ]);
                
                $attachment = InsertFile($request->file('attachment'), '/company_documents');
                $comoany_document = new CompanyDocument();
                $comoany_document->name = $request->name;
                $comoany_document->details = $request->details;
                $comoany_document->status = $request->status;
                $comoany_document->share_departments = json_encode($request->share_departments);
                $comoany_document->share_employees = json_encode($request->share_employees);
                $comoany_document->attachment = $attachment;
                $comoany_document->save();
            }
            // Document Types
            elseif($request->type == 'document_types'){
                $this->validate($request, [
                    'name' => 'required |string',
                    'details' => 'required',
                ]);
                $data = $request->all();
                Document::create($data);
            }
            // Employee Documents
            else{
                $this->validate($request, [
                    'status' => 'required',
                ]);
                $data = $request->all();
                if($request->hasFile('attachment')){
                    $data['attachment'] = InsertFile($request->file('attachment'), '/employee_documents');
                }
                EmployeeDocument::create($data);
            }
            return ['code'=>'200','message'=>'success'];
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $id)
    {
        // Company Documents
        if($request->type == 'company_documents'){
            $data = CompanyDocument::find($id);
        }
        // Document Types
        elseif($request->type == 'document_types'){
            $data = Document::find($id);
        }
        // Employee Documents
        else{
            $data = EmployeeDocument::find($id);
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            // Company Documents
            if($request->type == 'company_documents'){
                $this->validate($request, [
                    'name' => 'required |string',
                    'status' => 'required',
                    'details' => 'required',
                ]);

                $comoany_document = CompanyDocument::find($id);
                if($request->hasFile('attachment')){
                    if(!empty($comoany_document->attachment)){
                        unlink(public_path().$comoany_document->attachment);
                    }
                    $attachment = InsertFile($request->file('attachment'), '/company_documents');
                }
                $comoany_document->name = $request->name;
                $comoany_document->details = $request->details;
                $comoany_document->status = $request->status;
                $comoany_document->share_departments = json_encode($request->share_departments);
                $comoany_document->share_employees = json_encode($request->share_employees);
                $comoany_document->attachment = isset($attachment) ? $attachment : $comoany_document->attachment;
                $comoany_document->update();
            }
            // Document Types
            elseif($request->type == 'document_types'){
                $this->validate($request, [
                    'name' => 'required |string',
                    'details' => 'required',
                ]);
                $data = $request->all();
                $document_type = Document::find($id);
                $document_type->fill($data)->save();
            }
            // Employee Documents
            else{
                $this->validate($request, [
                    'status' => 'required',
                ]);
                $data = $request->all();
                $employee_document = EmployeeDocument::find($id);
                if($request->hasFile('attachment')){
                    if(!empty($employee_document->attachment)){
                        unlink(public_path().$employee_document->attachment);
                    }
                    $data['attachment'] = InsertFile($request->file('attachment'), '/employee_documents');
                }
                $employee_document->fill($data)->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {   
        try{
            // Company Documents
            if($request->type == 'company_documents'){
                $data = CompanyDocument::find($id);
                $data->deleted_by = Auth::id();
                $data->update();
                $data->delete();
            }
            // Document Types
            elseif($request->type == 'document_types'){
                $data = Document::find($id);
                $data->deleted_by = Auth::id();
                $data->update();
                $data->delete();
            }
            // Employee Documents
            else{
                $data = EmployeeDocument::find($id);
                $data->deleted_by = Auth::id();
                $data->update();
                $data->delete();
            }
            return ['code'=>'200','message'=>'success'];
        }catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
        
    }
}
