<?php

namespace App\Http\Controllers;
use PDF;
use Auth;
use DateTime;
use DataTables;
use App\Models\Legal;
use App\Models\Document;
use Carbon\CarbonPeriod;
use App\Models\Employee;
use App\Models\LeaveTypes;
use App\Models\LeavePeriods;
use App\Models\LoanRequest;
use App\Models\EnvChecks;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\TravelRequest;
use App\Models\AssetIssuance;
use App\Models\Disciplinarie;
use App\Models\EmployeeSkills;
use App\Models\ShiftManagement;
use App\Models\LoanInstallment;
use App\Models\Payroll;
use App\Models\PayrollColumn;
use App\Models\EmployeeDocument;
use App\Models\EmployeeEducation;
use App\Models\EmployeeLanguages;
use App\Models\EmergencyContacts;
use App\Models\EmployeeEncashment;
use App\Models\EmployeeDepandents;
use Illuminate\Support\Facades\DB;
use App\Models\InsuranceAllocation;
use App\Models\EmployeeEntitlement;
use App\Models\EmployeeLeaveRequest;
use Illuminate\Support\Facades\File;
use App\Models\EmployeeLeaveBalance;
use App\Models\IssueBelonging;
use App\Models\EmployeeTrainingSession;
use App\Models\EmployeeDisciplinaryAction;
use Illuminate\Validation\ValidationException;
use Http;

class StaffDirectoryController extends Controller
{
    public function __construct(){ 
        $this->middleware('auth');
        $this->middleware(['permission:Show Staff Detail'],['only' => ['show']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::where('employees.status' , 'Active')->whereIn('employees.department' , login_user_departments());
        if(!empty(request()->department)){
            $employees = $employees->where('employees.department' , request()->department);
        }
        if(!empty(request()->employee)){
            $employees = $employees->where('employees.id' , request()->employee);
        }
        $employees = $employees->select('id','first_name','middle_name','last_name','image','work_email','mobile_phone','job_title','department','employee_id')->paginate(12);
        return view('Admin.staff_directory.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = [];
        $payroll_columns = [];
        $payrolls = Payroll::query();
        if(!empty($request->payroll_id)){
            $payrolls->where('payroll.id',$request->payroll_id);
        }
        if(!empty($request->fromdate) && !empty($request->todate)){
            $payrolls->join('payrolldata','payrolldata.payroll', '=', 'payroll.id')->whereDate('payroll.date_start', '>=', $request->fromdate)->whereDate('payroll.date_start', '<=', $request->todate)->where('payrolldata.employee',$request->employee)->groupBy('payroll.id');
            
        }
        $payrolls = $payrolls->get(['payroll.id','payroll.name','payroll.columns']);
        // dd($payrolls);
        if(count($payrolls) > 0){
            $columns = json_decode($payrolls[0]->columns);
            $payroll_columns = PayrollColumn::where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                $query->whereNull('employers_column')->orWhere('employers_column', '');
            })->orderBy('colorder', 'asc')->get(['id', 'name', 'comma_seprated' , 'default_value','round_off']);
            foreach ($payrolls as $key => $payroll) {
                foreach($payroll_columns as $column){
                    $column_amount = DB::table('payrolldata')->where([
                        ['payroll' , $payroll->id],
                        ['payroll_item' , $column->id],
                        ['employee' , $request->employee]
                        ])->first(['amount']);
                    $col_amount = !empty($column_amount) ? $column_amount->amount : $column->default_value;
                    if(is_numeric($col_amount)){
                        $col_amount = number_format($col_amount,2); 
                    }
                    $data[$payroll->name][] = $col_amount;
                }
            }
        }
        // if(request()->export_type == "pdf"){
        //     $date = request()->date;
        //     $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('Admin.reports.employee_salary_history_pdf', get_defined_vars())->setPaper('4A0', 'landscape');
        //     return $pdf->download('Employee Salary History Report.pdf');
        // }
        return response(['columns' => $payroll_columns, 'data' => $data]);
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            // Adding Employee Image
            if($request->type == 'employee_image'){
                $this->validate($request , [
                    'employee_image' => 'required',
                ]);
                $employee = Employee::find($request->id);
                if($request->hasFile('employee_image')){
                    if(File::exists(public_path('images/employees/'.$employee->image))){
                        unlink('images/employees/'.$employee->image);
                    }
                    $file = $request->file('employee_image');
                    $extension = $file->getClientOriginalExtension();
                    $temp_name = time(). "." . rand(000 , 999) . '.' .$extension;
                    $file->move('images/employees/' , $temp_name);
                    $employee->image = $temp_name;
                    $employee->save();
                }
                return ['code'=>'200','message'=>'success'];
            }

            // Adding Employee Language 
            if($request->type == 'language'){
                $this->validate($request , [
                    'language_id' => 'required',
                    'reading' => 'required',
                    'speaking' => 'required',
                    'writing' => 'required',
                    'understanding' => 'required',
                ]);
                $data = $request->all();
                $data['employee'] = $request->employee;
                EmployeeLanguages::create($data);
            }

            // Adding Employee Skill
            if($request->type == 'skill'){
                $this->validate($request , [
                    'file' => 'required',
                    'details' => 'required',
                ]);
                $data = $request->all();
                if($request->hasFile('file')){
                    $file = $request->file;
                    $extension = $file->getClientOriginalExtension();
                    $tem_name = time(). '.' . rand(000 , 999) . '.' .$extension;
                    $file->move('images/employees/employeeSkills' , $tem_name);
                    $data['file'] = $tem_name;
                }
                $data['employee'] = $request->employee;
                EmployeeSkills::create($data);
                
            }

            // Adding Employee Education
            if($request->type == 'education'){
                $this->validate($request , [
                    'institute' => 'required',
                    'date_start' => 'required',
                    'date_end' => 'required',
                    'grade' => 'required',
                    'marks' => 'required',
                    'file' => 'required',
                ]);   
                $data = $request->all();
                if($request->hasFile('file')){
                    $file = $request->file;
                    $extension = $file->getClientOriginalExtension();
                    $tem_name = time(). '.' .$extension;
                    $file->move('images/employees/employeeEducation' , $tem_name);
                    $data['file'] = $tem_name;
                }
                $data['employee'] = $request->employee;
                EmployeeEducation::create($data);
            }

            // Adding Employee Emergency Contacts
            if($request->type == 'emergency_contacts'){
                $this->validate($request , [
                    'name' => 'required',
                    'relationship' => 'required',
                    'mobile_phone' => 'required',
                ]);
                $data = $request->all();
                $data['employee'] = $request->employee;
                EmergencyContacts::create($data);
            }

            // Adding Employee Dependants
            if($request->type == 'employee_depandents'){
                $this->validate($request , [
                    'name' => 'required',
                    'relationship' => 'required',
                    'dob' => 'required',
                ]);
                $data = $request->all();
                $data['employee'] = $request->employee;
                EmployeeDepandents::create($data);
            }

            // Adding Employee Documents
            if($request->type == 'employee_documents'){
                $this->validate($request, [
                    'document' => 'required',
                    'date_added' => 'required',
                    'valid_until' => 'required',
                    'status' => 'required',
                    'attachment' => 'required',
                    'details' => 'required',
                ]);
                $data = $request->all();
                if($request->hasfile('attachment')){
                    $file = $request->file('attachment');
                    $extension = $file->getClientOriginalExtension();
                    $temp_name = time(). "." . rand(000 , 999) . '.' .$extension;
                    $file->move('images/employee_documents/' , $temp_name);
                    $data['attachment'] = $temp_name;
                }
                $data['employee'] = $request->employee;
                EmployeeDocument::create($data);
                return ['code'=>'200','message'=>'success'];
            }

            // Adding Employee Encashment
            if($request->type == 'employee_encashment'){
                $this->validate($request, [
                    'leave_type' => 'required',
                    'year' => 'required',
                    'amount' => 'required',
                ]);
                $data = $request->all();
                $data['emp_id'] = $request->employee;
                EmployeeEncashment::create($data);
                return ['code'=>'200','message'=>'success'];
            }

            // Adding Employee Leave Balance
            if($request->type == 'employee_leave_balance'){
                $this->validate($request, [
                    'leave_type' => 'required',
                    'balance' => 'required',
                ]);
                $data = $request->all();
                $data['emp_id'] = $request->employee;
                EmployeeLeaveBalance::updateOrCreate([
                    'leave_type' => $request->leave_type,
                ],$data);
                return ['code'=>'200','message'=>'success'];
            }

            // Adding Employee leave
            if($request->type == 'employee_leave'){
                $this->validate($request, [
                    'leave_type' => 'required',
                    'date_start' => 'required',
                    'date_end' => 'required',
                ]);
                $data = $request->all();
                $shift = ShiftManagement::where('employee_id' , $request->employee)->orderBy('id' , 'desc')->first(['shift_id']);
                $data['employee'] = $request->employee;
                $data['status'] = 'Approved';
                $data['is_paid'] = !empty($request->is_paid) ? 1 : 0;
                $data['shift_id'] = $shift->shift_id;
                $data['submitted_by'] = Auth::id();
                EmployeeLeaveRequest::create($data);
                return ['code'=>'200','message'=>'success'];
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request , $id)
    {
       
        if($request->ajax()){
            // Skill Query
            if($request->type == 'skills'){
                $query = EmployeeSkills::select('employeeskills.*' , 'skills.name AS skill')
                ->leftjoin('skills' , 'skills.id' , '=' , 'employeeskills.skill_id')
                ->where('employeeskills.employee' , $id);
                return DataTables::of($query)->make(true);
            }
            
            // Education Query
            if($request->type == 'education'){
                $query = EmployeeEducation::select('employeeeducations.*' , 'educations.name AS education')
                ->leftjoin('educations' , 'educations.id' , '=' , 'employeeeducations.education_id')
                ->where('employeeeducations.employee' , $id);
                return DataTables::of($query)->make(true);
            }

            // Language Query
            if($request->type == 'languages'){
                $query = EmployeeLanguages::select('employeelanguages.*' , 'languages.name AS language')
                ->leftjoin('languages' , 'languages.id' , '=' , 'employeelanguages.language_id')
                ->where('employeelanguages.employee' , $id);
                return DataTables::of($query)->make(true);
            }

            // Emergency Contact Query
            if($request->type == 'employee_contacts'){
                $query = EmergencyContacts::where('employee' , $id);
                return DataTables::of($query)->make(true);
            }

            // Employee Dependant Query
            if($request->type == 'employee_depandents'){
                $query = EmployeeDepandents::where('employee' , $id);
                return DataTables::of($query)->make(true);
            }

            // Employee Document Query
            if($request->type == 'employee_documents'){
                $query = EmployeeDocument::select('employeedocuments.*' , 'documents.name AS docName')
                ->leftjoin('documents' , 'documents.id' , '=' , 'employeedocuments.document')
                ->where('employeedocuments.employee' , $id);
                return DataTables::of($query)->make(true);
            }

            // Employee Legals Query
            if($request->type == 'employee_legals'){
                $query = Legal::select('legals.*' , 'employees.first_name' , 'employees.last_name')
                ->leftjoin('employees' , 'employees.id' , '=' , 'legals.employee_id')
                ->where('legals.employee_id' , $id);
                return DataTables::of($query)->make(true);
            }

            // Employee Training Query
            if($request->type == 'employee_training'){
                $query = EmployeeTrainingSession::select('employeetrainingsessions.*' , 'trainingsessions.name AS tName')
                ->leftjoin('trainingsessions' , 'trainingsessions.id' , '=' , 'employeetrainingsessions.trainingSession')
                ->where('employeetrainingsessions.employee' , $id);
                return DataTables::of($query)->make(true);
            }

            // Employee Travel Query
            if($request->type == 'employee_travel'){
                $query = TravelRequest::select('employeetravelrecords.*' , 'currencytypes.code AS money')
                ->leftjoin('currencytypes' , 'currencytypes.id' , '=' , 'employeetravelrecords.currency')
                ->where('employeetravelrecords.employee' , $id);
                return DataTables::of($query)->make(true);
            }

            // Employee Entitlement Query
            if($request->type == 'employee_entitlement'){
                $query = EmployeeEntitlement::select('employeesentitlement.*' , 'entitlement.name AS entitle' , 'companystructures.title AS department')
                ->leftjoin('entitlement' , 'entitlement.id' , '=' , 'employeesentitlement.entitlement_id')
                ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employeesentitlement.department_id')
                ->where('employeesentitlement.employee_id' , $id);
                return DataTables::of($query)->make(true);
            }

            // Employee Insurance Query
            if($request->type == 'employee_insurance'){
                $query = InsuranceAllocation::select('insurance_allocation.*' , 'insurance_sub_types.sub_type AS subName' , 'insurance_types.type AS insuranceType')
                ->leftjoin('insurance_sub_types' , 'insurance_sub_types.id' , '=' , 'insurance_allocation.sub_type')
                ->leftjoin('insurance_types' , 'insurance_types.id' , '=' , 'insurance_allocation.type')
                ->where('insurance_allocation.employee_id' , $id);
                return DataTables::of($query)->make(true);
            }

            // Employee Insurance Query
            if($request->type == 'employee_inquiry'){
                return DataTables::eloquent(EmployeeDisciplinaryAction::query())->make(true);
            }

            // Employee Loan Query
            if($request->type == 'employee_loan'){
                $query = LoanRequest::where([
                    ['employee_id' , $id],
                    ['loan_type' , 3],
                    ['remaining_amount' , '>' , 0]
                ]);
                return DataTables::of($query)->make(true);
            }

            // Employee Advance Query
            if($request->type == 'employee_advance'){
                $query = LoanRequest::where('employee_id' , $id)
                ->where('loan_type' , 4)
                ->where('remaining_amount' , '>' , 0);
                return DataTables::of($query)->make(true);
            }

            // Employee Belongings Query
            if($request->type == 'employee_belongings'){
                $query = IssueBelonging::leftjoin('companybelongings' , 'companybelongings.id' , '=' , 'issue_belongings.company_belonging_id')
                ->where('issue_belongings.mul_employees' , $id)
                ->select('issue_belongings.*' , 'companybelongings.name AS asset_name');
                return DataTables::of($query)->make(true);
            }

            // Employee Leaves Query
            if($request->type == 'employee_leaves'){
                if(env('COMPANY') == 'JSML'){
                    $query = getEmployeeLeaveTypesBySettingsForStaffDirectory($id);
                    $emloyeeObj = Employee::find($id);
                    return DataTables::of($query)
                        ->addColumn('default_per_year', function($leaveType) use ($emloyeeObj) {
                            $defaultAmount = getLeaveTypeDefaultYearBalanceBySetting($leaveType, $emloyeeObj);
                            return $leaveType->default_per_year = $defaultAmount;
                        })
                        ->make(true);
                }
                else{
                    $query = getEmployeeLeaveTypesForStaffDirectory($id);
                    return DataTables::of($query)->make(true);
                }
            }

            // Medical Claims
            if($request->type == 'medical_claims'){
                $query = DB::table('medical_claims')->select('medical_claims.*')
                ->leftjoin('employees' , 'employees.id' , '=' , 'medical_claims.employee_id')
                ->where([
                    ['employees.status' , 'Active'],
                    ['employees.id' , $id]
                ]);
                return DataTables::of($query)->make(true);
            }
            if($request->type == 'disciplinaries'){
             $query = Disciplinarie::leftjoin('employees' , 'employees.id' , '=' , 'disciplinaries.employee_id')
            ->join('warning_rules','warning_rules.id' ,'=','disciplinaries.warning_rule')
            ->join('jobtitles','jobtitles.id','=', 'employees.job_title')
            ->join('companystructures' ,'companystructures.id','employees.department')
            ->where('disciplinaries.employee_id',$id)
            ->select('disciplinaries.*' , 'employees.employee_id' ,'jobtitles.name as designation','companystructures.title as employee_department', 'employees.employee_code' ,'employees.first_name' ,'employees.middle_name',
             'warning_rules.rule' ,'employees.last_name');
            return DataTables::of($query)->make(true);
            }
        }

        $leaves = [];
        $encashment = [];
        $balance = [];
        $employeeObj = Employee::find($id);
        $employeement_status = $employeeObj->employment_status;

        $leave_types = LeaveTypes::get();
        $leavePeriod = LeavePeriods::where('status', 'Active')->first();
        foreach($leave_types as $leave_type){
            $count = 0;
            $leaves_query = EmployeeLeaveRequest::where('status' , 'Approved')
            ->where('employee' , $id)
            ->where('leave_type' , $leave_type->id)
            ->where('leave_period' , $leavePeriod->id)
            ->get();
            if(env('COMPANY') == 'JSML'){
                $short_leave_total = $employeeObj
                ->shortLeaves()
                ->where('status', 'Approved')
                ->where('deduction_leave_type_id', $leave_type->id)
                ->selectRaw('SUM(0.5) AS total')
                ->value('total');
                $count += $short_leave_total;
            }
            $encashment_query = EmployeeEncashment::where('emp_id' , $id)
            ->where('leave_type' , $leave_type->id)
            ->get();
            $balance_query = EmployeeLeaveBalance::where('emp_id' , $id)
            ->where('leave_type' , $leave_type->id)
            ->get();

            foreach($leaves_query as $item){
                if(isset($item->is_half) && $item->is_half == 1){
                    $count += 0.5;
                }
                else{
                    $begin = new DateTime($item->date_start);
                    $end = new DateTime($item->date_end);
                    for($i = $begin ; $i <= $end ; $i->modify('+1 day')){
                        $count += 1;
                    }
                }
            }

            $leaves = Arr::add($leaves , $leave_type->name , $count);
            foreach($encashment_query as $item){
                $encashment = Arr::add($encashment , $leave_type->name , $item->amount);
            }

            foreach($balance_query as $item){
                $balance = Arr::add($balance , $leave_type->name , $item->balance);
            }
        }

        // Required Tables Data
        $document_type = Document::get();
        $languages = DB::table('languages')->get();
        $educations = DB::table('educations')->get();

        // Salary History
        $payrolls = Payroll::join('payrolldata','payrolldata.payroll', '=', 'payroll.id')->where('payrolldata.employee',$id)->groupBy('payroll.id')->get(['payroll.id', 'payroll.name']);

        // Required Employee Data 
        $employee = Employee::leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
        ->leftjoin('nationality' , 'nationality.id' , '=' , 'employees.nationality')
        ->leftjoin('employmentstatus' , 'employmentstatus.id' , '=' , 'employees.employment_status')
        ->leftjoin('employees AS super_employee' , 'super_employee.id' , '=' , 'employees.supervisor')
        ->leftjoin('country' , 'country.id' , '=' , 'employees.country')
        ->where('employees.id' , $id)
        ->select('employees.*' , 'companystructures.title AS empDept' , 'jobtitles.name AS designation' , 'nationality.name AS nation' , 'employmentstatus.name AS employment' , 'super_employee.first_name AS super_first_name' , 'super_employee.last_name AS super_last_name' , 'country.code AS country_code')
        ->first();
        // dd($id);
        $id = (int) $id;
        // if(env('COMPANY') != 'Ajmal Dawakhana'){
        //     if(env('COMPANY') == 'CLINIX'){
        //         $base_image_url = Http::get("http://207.180.216.40:3001/get_preview?user_id={$id}");
        //     }elseif(env('COMPANY') == 'Roofline'){
        //         $base_image_url = Http::get("http://207.180.216.40:3000/get_preview?user_id={$id}");
        //     }

        //     if(!empty($jsonData_for_base64['image_base64'])){
        //         $jsonData_for_base64 = $base_image_url->json();
        //         $jsonData_for_base64_image = 'data:image/jpeg;base64,' .$jsonData_for_base64['image_base64'];
        //     }
        // }
        $face_img = EnvChecks::join('env_name', 'env_name.id', '=', 'meta_data.name')->where('env_name.name','Face On Staff Driectory')->where('key',1)->first();

        return view('Admin.staff_directory.staffMember' , get_defined_vars());
    }
    public function attachment_download($id)
    {
        $data = EmployeeDocument::find($id);
        // dd($data->attachment);
    	$myFile = public_path("images/employee_documents/$data->attachment");
        // dd($myFile);


    	return response()->download($myFile);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $id)
    {
        // Edit Skill
        if($request->type == 'skill'){
            $data = EmployeeSkills::find($id);
            return response()->json($data);
        }

        // Edit Education
        if($request->type == 'education'){
            $data = EmployeeEducation::find($id);
            return response()->json($data);
        }

        // Edit Language
        if($request->type == 'language'){
            $data = EmployeeLanguages::find($id);
            return response()->json($data);
        }

        // Edit Emergency Contacts
        if($request->type == 'emergency_contacts'){
            $data = EmergencyContacts::find($id);
            return response()->json($data);
        }

        // Edit Employee Dependants
        if($request->type == 'employee_depandents'){
            $data = EmployeeDepandents::find($id);
            return response()->json($data);
        }

        // Edit employee Documents
        if($request->type == 'employee_documents'){
            $data = EmployeeDocument::find($id);
            return response()->json($data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            // Update Skill
            if($request->type == 'skill'){
                $this->validate($request , [
                    'details' => 'required',
                ]);
                $skill = EmployeeSkills::find($id);
                $data = $request->all();
                if($request->hasFile('file')){
                    unlink('images/employees/employeeSkills/'.$skill->file);
                    $file = $request->file;
                    $extension = $file->getClientOriginalExtension();
                    $tem_name = time(). '.' . rand(000 , 999) . '.' .$extension;
                    $file->move('images/employees/employeeSkills' , $tem_name);
                    $data['file'] = $tem_name;
                }
                $data['employee'] = $request->employee;
                $skill->fill($data);
                $skill->save();
            }

            // Update Education
            if($request->type == 'education'){
                $this->validate($request , [
                    'institute' => 'required',
                    'date_start' => 'required',
                    'date_end' => 'required',
                    'grade' => 'required',
                    'marks' => 'required',
                ]);
                $education = EmployeeEducation::find($id);
                $data = $request->all();
                if($request->hasFile('file')){
                    unlink('images/employees/employeeEducation/'.$education->file);
                    $file = $request->file;
                    $extension = $file->getClientOriginalExtension();
                    $tem_name = time(). '.' . rand(000 , 999) . '.' .$extension;
                    $file->move('images/employees/employeeEducation' , $tem_name);
                    $data['file'] = $tem_name;
                }
                $data['employee'] = $request->employee;
                $education->fill($data);
                $education->save(); 
            }

            // Update Language
            if($request->type == 'language'){
                $this->validate($request , [
                    'language_id' => 'required',
                    'reading' => 'required',
                    'speaking' => 'required',
                    'writing' => 'required',
                    'understanding' => 'required',
                ]);
                $employee_languages = EmployeeLanguages::find($id);
                $data = $request->all();
                $data['employee'] = $request->employee;
                $employee_languages->fill($data);
                $employee_languages->save();   
            }

            // Update Emergency Contacts
            if($request->type == 'emergency_contacts'){
                $this->validate($request , [
                    'name' => 'required',
                    'relationship' => 'required',
                    'mobile_phone' => 'required',
                ]);
                $emergency_contact = EmergencyContacts::find($id);
                $data = $request->all();
                $data['employee'] = $request->employee;
                $emergency_contact->fill($data);
                $emergency_contact->save();
            }

            // Update Employee Dependants
            if($request->type == 'employee_depandents'){
                $this->validate($request , [
                    'name' => 'required',
                    'relationship' => 'required',
                    'dob' => 'required',
                ]);
                $depandents = EmployeeDepandents::find($id);
                $data = $request->all();
                $data['employee'] = $request->employee;
                $depandents->fill($data);
                $depandents->save();
            }

            // Update Employee Documents
            if($request->type == 'employee_documents'){
                $this->validate($request, [
                    'document' => 'required',
                    'date_added' => 'required',
                    'valid_until' => 'required',
                    'status' => 'required',
                    'details' => 'required',
                ]);
                $data = $request->all();
                $employee = EmployeeDocument::find($id);
                if($request->hasfile('attachment')){
                    if(!empty($employee->attachment)){
                        unlink('images/employee_documents/'.$employee->attachment);
                    }
                    $file = $request->file('attachment');
                    $extension = $file->getClientOriginalExtension();
                    $temp_name = time(). "." . rand(000 , 999) . '.' .$extension;
                    $file->move('images/employee_documents/' , $temp_name);
                    $data['attachment'] = $temp_name;
                }
                $employee->fill($data)->save();
                return ['code'=>'200','message'=>'success'];
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        try{
            // Skill Delete
            if($request->type == 'skill'){
                $employeeSkill = EmployeeSkills::find($id);
                if(!empty($employeeSkill->file)){
                    $image = $employeeSkill->file;
                    unlink('images/employees/employeeSkills/'.$image);
                }
                $employeeSkill->deleted_by = Auth::id();
                $employeeSkill->update();
                $employeeSkill->delete();
                return ['code' => 200 , 'message' => 'success'];
            }

            // Education Delete
            if($request->type == 'education'){
                $employeeEducation = EmployeeEducation::find($id);
                if(!empty($employeeEducation->file)){
                    $image = $employeeEducation->file;
                    unlink('images/employees/employeeEducation/'.$image);
                }
                $employeeEducation->deleted_by = Auth::id();
                $employeeEducation->update();
                $employeeEducation->delete();
                return ['code' => 200 , 'message' => 'success'];
            }

            // Language Delete
            if($request->type == 'language'){
                $employeeLanguage = EmployeeLanguages::find($id);
                $employeeLanguage->deleted_by = Auth::id();
                $employeeLanguage->update();
                $employeeLanguage->delete();
                return ['code' => 200 , 'message' => 'success'];
            }

            // Emergency Contacts Delete
            if($request->type == 'emergency_contacts'){
                $emergencyContact = EmergencyContacts::find($id);
                $emergencyContact->deleted_by = Auth::id();
                $emergencyContact->update();
                $emergencyContact->delete();
                return ['code' => 200 , 'message' => 'success'];
            }

            // Employee Dependants Delete
            if($request->type == 'employee_depandents'){
                $depandents = EmployeeDepandents::find($id);
                $depandents->deleted_by = Auth::id();
                $depandents->update();
                $depandents->delete();
                return ['code' => 200 , 'message' => 'success'];
            }

            // Employee Documents Delete
            if($request->type == 'employee_documents'){
                $data = EmployeeDocument::find($id);
                if(!empty($data->attachment)){
                    unlink('images/employee_documents/'.$data->attachment);
                }
                $data->deleted_by = Auth::id();
                $data->update();
                $data->delete();
                return ['code'=>'200','message'=>'success'];
            }
        }catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
