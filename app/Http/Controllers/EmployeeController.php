<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Bank;
use App\Models\City;
use App\Models\JobTitle;
use App\Models\Employee;
use App\Models\PayGrade;
use App\Models\PayrollColumn;
use App\Models\EmployeeSalary;
use App\Models\FinalRecord;
use Illuminate\Http\Request;
use App\Models\EmploymentStatus;
use App\Models\FinalSettlementSetup;
use App\Models\EmployeesHistory;
use App\Models\LeaveGroup;
use App\Models\Nationality;
use App\Models\Ethnicity;
use App\Models\Province;
use App\Models\WorkWeek;
use App\Models\ShiftType;
use App\Models\ShiftManagement;
use App\Models\PayFrequency;
use App\Models\CurrencyType;
use App\Models\DeductionGroup;
use App\Models\Deduction;
use App\Models\PayrollEmployee;
use App\Models\SalaryComponent;
use App\Models\EmployeeSalaryComponent;
use App\Models\SalaryComponentType;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            if($request->type == 'active')
            {
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('nationality', 'nationality.id', '=', 'employees.nationality')
                ->leftJoin('city', 'city.id', '=', 'employees.ethnicity')
                ->leftJoin('province', 'province.id', '=', 'employees.province')
                ->leftJoin('companystructures as t2', 't2.id', '=', 'companystructures.parent')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                ->where('employees.status', '=', 'Active');
                if(env('COMPANY') == 'JSML'){
                    $employees->where('employees.seasonal_status' , 'On Roll');
                }
                $employees = $employees->where('employees.suspended', '=', 0)
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id' , 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','employees.ssn_num', 'employees.father_cnic' ,'employees.city','employees.mobile_phone','employees.driving_license','employees.other_id','employees.birthday','employees.pessi','employees.blood_group','employees.eobi_status','employees.provident_fund','employees.covid_vaccinated','employees.department','employees.pay_grade','employees.work_station_id','employees.confirmation_date','employees.joined_date','employees.work_email','employees.private_email','employees.work_phone','employees.home_phone','employees.postal_code','province.name as provinceName','employees.country','city.name as cityName','employees.eobi','employees.marital_status','employees.gender','nationality.name as nationalityName','employees.bank_name','employees.account_title','employees.account_number' , 'employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title','t2.title as parent', 'employmentstatus.name as employment_status', 'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name','employees.address1','employees.address2')->groupBy('employees.id');
                return DataTables::eloquent($employees)->filter(function ($employees) use ($request) {
                    if(!empty($request->departmentFilter)){
                        $all_childs = getChildren($request->departmentFilter);
                        array_push($all_childs , $request->departmentFilter);
                        $employees->whereIn('employees.department', $all_childs);
                    }
                    if(!empty($request->employeeFilter)){
                        $employees->where('employees.id', $request->employeeFilter);
                    }
                    if(!empty($request->empCityFilter)){
                        $employees->where('employees.city', $request->empCityFilter);
                    }
                    if(!empty($request->empJobTitleFilter)){
                        $employees->where('employees.job_title', $request->empJobTitleFilter);
                    }
                    if(!empty($request->empJoinFromDateFilter) && !empty($request->empJoinToDateFilter)){
                        $employees->whereBetween('employees.joined_date' , [$request->empJoinFromDateFilter , $request->empJoinToDateFilter]);
                    }
                    if(!empty($request->empConFromDateFilter) && !empty($request->empConToDateFilter)){
                        $employees->whereBetween('employees.confirmation_date' , [$request->empConFromDateFilter , $request->empConToDateFilter]);
                    }
                    if(!empty($request->empPayFilter)){
                        $employees->where('employees.pay_grade', $request->empPayFilter);
                    }
                    if(!empty($request->empBloodFilter)){
                        $employees->where('employees.blood_group', $request->empBloodFilter);
                    }
                },true)->addIndexColumn()->make(true);
            }
            elseif($request->type == 'suspended')
            {
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('companystructures as t2', 't2.id', '=', 'companystructures.parent')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                ->where('employees.suspended', '=', 1)
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name', 'employees.last_name','employees.father_name','employees.father_cnic','employees.city','employees.mobile_phone','employees.bank_name','employees.account_title','employees.account_number','employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title', 'employmentstatus.name as employment_status','t2.title as parent', 'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name', 'employees.suspension_date');
                return DataTables::eloquent($employees)->addIndexColumn()->make(true);
            }
            elseif($request->type == 'off_roll'){
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                ->where('employees.status', '=', 'Active')
                ->where('employees.suspended', '=', 0)
                ->where('employees.seasonal_status', '=', 'Off Roll')
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','employees.city','employees.mobile_phone','employees.bank_name','employees.account_title','employees.account_number','employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title', 'employmentstatus.name as employment_status', 'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name', 'employees.off_roll_date');
                // return DataTables::eloquent($employees)->addIndexColumn()->make(true);
                return DataTables::eloquent($employees)->filter(function ($employees) use ($request) {
                    if(!empty($request->departmentFilter)){
                        $all_childs = getChildren($request->departmentFilter);
                        array_push($all_childs , $request->departmentFilter);
                        $employees->whereIn('employees.department', $all_childs);
                    }
                    if(!empty($request->employeeFilter)){
                        $employees->where('employees.id', $request->employeeFilter);
                    }
                    if(!empty($request->empCityFilter)){
                        $employees->where('employees.city', $request->empCityFilter);
                    }
                    if(!empty($request->empJobTitleFilter)){
                        $employees->where('employees.job_title', $request->empJobTitleFilter);
                    }
                    if(!empty($request->empJoinFromDateFilter) && !empty($request->empJoinToDateFilter)){
                        $employees->whereBetween('employees.joined_date' , [$request->empJoinFromDateFilter , $request->empJoinToDateFilter]);
                    }
                    if(!empty($request->empConFromDateFilter) && !empty($request->empConToDateFilter)){
                        $employees->whereBetween('employees.confirmation_date' , [$request->empConFromDateFilter , $request->empConToDateFilter]);
                    }
                    if(!empty($request->empPayFilter)){
                        $employees->where('employees.pay_grade', $request->empPayFilter);
                    }
                    if(!empty($request->empBloodFilter)){
                        $employees->where('employees.blood_group', $request->empBloodFilter);
                    }
                },true)->addIndexColumn()->make(true);
            }
            elseif($request->type == 'reg_emp'){
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('nationality', 'nationality.id', '=', 'employees.nationality')
                ->leftJoin('city', 'city.id', '=', 'employees.ethnicity')
                ->leftJoin('country', 'country.code', '=', 'employees.country')
                ->leftJoin('province', 'province.id', '=', 'employees.province')
                ->leftJoin('companystructures as t2', 't2.id', '=', 'companystructures.parent')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id' , 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','employees.ssn_num', 'employees.father_cnic' ,'employees.city','employees.mobile_phone','employees.driving_license','employees.other_id','employees.birthday','employees.passcode','employees.pessi','employees.eobi_status','employees.provident_fund','employees.covid_vaccinated','employees.department','employees.pay_grade','employees.work_station_id','employees.confirmation_date','employees.joined_date','employees.work_email','employees.private_email','employees.work_phone','employees.home_phone','employees.postal_code','province.name as provinceName','country.namecap','city.name as cityName','employees.eobi','employees.marital_status','employees.gender','nationality.name as nationalityName','employees.bank_name','employees.account_title','employees.account_number' , 'employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title','t2.title as parent', 'employmentstatus.name as employment_status', 'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name','employees.address1','employees.address2', 'employees.suspension_date');
                return DataTables::eloquent($employees)->addIndexColumn()->make(true);
            }
            else{
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('companystructures as t2', 't2.id', '=', 'companystructures.parent')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                ->where('employees.status', '=', 'Terminated')
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','employees.father_cnic','employees.city','employees.mobile_phone','employees.bank_name','employees.account_title','employees.account_number','employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title', 'employmentstatus.name as employment_status','t2.title as parent',  'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name', 'employees.suspension_date', 'employees.termination_date');
                // return DataTables::eloquent($employees)->addIndexColumn()->make(true);
                return DataTables::eloquent($employees)->filter(function ($employees) use ($request) {
                    if(!empty($request->departmentFilter)){
                        $all_childs = getChildren($request->departmentFilter);
                        array_push($all_childs , $request->departmentFilter);
                        $employees->whereIn('employees.department', $all_childs);
                    }
                    if(!empty($request->employeeFilter)){
                        $employees->where('employees.id', $request->employeeFilter);
                    }
                    if(!empty($request->empCityFilter)){
                        $employees->where('employees.city', $request->empCityFilter);
                    }
                    if(!empty($request->empJobTitleFilter)){
                        $employees->where('employees.job_title', $request->empJobTitleFilter);
                    }
                    if(!empty($request->empJoinFromDateFilter) && !empty($request->empJoinToDateFilter)){
                        $employees->whereBetween('employees.joined_date' , [$request->empJoinFromDateFilter , $request->empJoinToDateFilter]);
                    }
                    if(!empty($request->empConFromDateFilter) && !empty($request->empConToDateFilter)){
                        $employees->whereBetween('employees.confirmation_date' , [$request->empConFromDateFilter , $request->empConToDateFilter]);
                    }
                    if(!empty($request->empPayFilter)){
                        $employees->where('employees.pay_grade', $request->empPayFilter);
                    }
                    if(!empty($request->empBloodFilter)){
                        $employees->where('employees.blood_group', $request->empBloodFilter);
                    }
                },true)->addIndexColumn()->make(true);
            }
        }
        $cities = City::get();
        $jobtitles = JobTitle::get();
        $employmentstatus = EmploymentStatus::get();
        $payGrades = PayGrade::get();
        $env = env('COMPANY');
        return view('Admin.employees.list.index' , get_defined_vars());
    }
    public function getEmployees(Request $request)
    {
        if($request->ajax()){
            if($request->type == 'active')
            {
                //  DB::enableQueryLog();
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('nationality', 'nationality.id', '=', 'employees.nationality')
                ->leftJoin('city', 'city.id', '=', 'employees.ethnicity')
                ->leftJoin('country', 'country.code', '=', 'employees.country')
                ->leftJoin('province', 'province.id', '=', 'employees.province')
                ->leftJoin('companystructures as t2', 't2.id', '=', 'companystructures.parent')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                ->where('employees.status', '=', 'Active');
                if(env('COMPANY') == 'JSML'){
                    $employees->where('employees.seasonal_status' , 'On Roll');
                }
                $employees = $employees->where('employees.suspended', '=', 0)
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id' , 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','employees.ssn_num', 'employees.father_cnic' ,'employees.city','employees.mobile_phone','employees.driving_license','employees.other_id','employees.birthday','employees.passcode','employees.blood_group','employees.pessi','employees.eobi_status','employees.ppc_reg_no','employees.provident_fund','employees.covid_vaccinated','employees.department','employees.pay_grade','employees.work_station_id','employees.confirmation_date','employees.joined_date','employees.work_email','employees.private_email','employees.work_phone','employees.home_phone','employees.postal_code','province.name as provinceName','country.namecap','city.name as cityName','employees.eobi','employees.marital_status','employees.gender','nationality.name as nationalityName','employees.bank_name','employees.account_title','employees.account_number' , 'employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title','companystructures.type','t2.title as parent', 'employmentstatus.name as employment_status', 'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name','employees.address1','employees.address2','employees.active')->groupBy('employees.id');
                // dd(DB::getQueryLog());
                // dd($employees);
                return DataTables::eloquent($employees)->filter(function ($employees) use ($request) {
                    if(!empty($request->departmentFilter)){
                        if(env('COMPANY') != 'JSML'){
                            $all_childs = getChildren($request->departmentFilter);
                            array_push($all_childs , $request->departmentFilter);
                            $employees->whereIn('employees.department', $all_childs);
                        }else{
                            $employees->whereIn('employees.department', $request->section);
                        }
                    }
                    if(!empty($request->employeeFilter)){
                        $employees->where('employees.id', $request->employeeFilter);
                    }
                    if(!empty($request->empCityFilter)){
                        $employees->where('employees.city', $request->empCityFilter);
                    }
                    if(!empty($request->empJobTitleFilter)){
                        $employees->where('employees.job_title', $request->empJobTitleFilter);
                    }
                    if(!empty($request->empJoinFromDateFilter) && !empty($request->empJoinToDateFilter)){
                        $employees->whereBetween('employees.joined_date' , [$request->empJoinFromDateFilter , $request->empJoinToDateFilter]);
                    }
                    if(!empty($request->empConFromDateFilter) && !empty($request->empConToDateFilter)){
                        $employees->whereBetween('employees.confirmation_date' , [$request->empConFromDateFilter , $request->empConToDateFilter]);
                    }
                    if(!empty($request->empPayFilter)){
                        $employees->where('employees.pay_grade', $request->empPayFilter);
                    }
                    if(!empty($request->empBloodFilter)){
                        $employees->where('employees.blood_group', $request->empBloodFilter);
                    }
                }, true)
                ->addColumn('employees_gross', function ($row) {
                    return getColumn_empValue($row->id);
                })
                ->rawColumns(['employees.id'])
                ->make(true);
            }
            elseif($request->type == 'suspended')
            {
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('nationality', 'nationality.id', '=', 'employees.nationality')
                ->leftJoin('city', 'city.id', '=', 'employees.ethnicity')
                ->leftJoin('country', 'country.code', '=', 'employees.country')
                ->leftJoin('province', 'province.id', '=', 'employees.province')
                ->leftJoin('companystructures as t2', 't2.id', '=', 'companystructures.parent')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                ->where('employees.suspended', '=', 1)
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id' , 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','employees.ssn_num', 'employees.father_cnic' ,'employees.city','employees.mobile_phone','employees.driving_license','employees.other_id','employees.birthday','employees.passcode','employees.pessi','employees.eobi_status','employees.provident_fund','employees.covid_vaccinated','employees.department','employees.pay_grade','employees.work_station_id','employees.confirmation_date','employees.joined_date','employees.work_email','employees.private_email','employees.work_phone','employees.home_phone','employees.postal_code','province.name as provinceName','country.namecap','city.name as cityName','employees.eobi','employees.marital_status','employees.gender','nationality.name as nationalityName','employees.bank_name','employees.account_title','employees.account_number' , 'employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title','t2.title as parent', 'employmentstatus.name as employment_status', 'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name','employees.address1','employees.address2', 'employees.suspension_date');
                return DataTables::eloquent($employees)->addColumn('employees_gross', function ($row) {
                    return getColumn_empValue($row->id);
                })
                ->rawColumns(['employees.id'])->make(true);
            }
            elseif($request->type == 'off_roll'){
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                ->where('employees.status', '=', 'Active')
                ->where('employees.suspended', '=', 0)
                ->where('employees.seasonal_status', '=', 'Off Roll')
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','employees.city','employees.mobile_phone','employees.bank_name','employees.account_title','employees.account_number','employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title', 'employmentstatus.name as employment_status', 'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name', 'employees.off_roll_date');
                // return DataTables::eloquent($employees)->addIndexColumn()->make(true);
                return DataTables::eloquent($employees)->filter(function ($employees) use ($request) {
                    if(!empty($request->departmentFilter)){
                        if(env('COMPANY') != 'JSML'){
                            $all_childs = getChildren($request->departmentFilter);
                            array_push($all_childs , $request->departmentFilter);
                            $employees->whereIn('employees.department', $all_childs);
                        }else{
                            $employees->whereIn('employees.department', $request->section);
                        }
                    }
                    if(!empty($request->employeeFilter)){
                        $employees->where('employees.id', $request->employeeFilter);
                    }
                    if(!empty($request->empCityFilter)){
                        $employees->where('employees.city', $request->empCityFilter);
                    }
                    if(!empty($request->empJobTitleFilter)){
                        $employees->where('employees.job_title', $request->empJobTitleFilter);
                    }
                    if(!empty($request->empJoinFromDateFilter) && !empty($request->empJoinToDateFilter)){
                        $employees->whereBetween('employees.joined_date' , [$request->empJoinFromDateFilter , $request->empJoinToDateFilter]);
                    }
                    if(!empty($request->empConFromDateFilter) && !empty($request->empConToDateFilter)){
                        $employees->whereBetween('employees.confirmation_date' , [$request->empConFromDateFilter , $request->empConToDateFilter]);
                    }
                    if(!empty($request->empPayFilter)){
                        $employees->where('employees.pay_grade', $request->empPayFilter);
                    }
                    if(!empty($request->empBloodFilter)){
                        $employees->where('employees.blood_group', $request->empBloodFilter);
                    }
                }, true)
                ->addColumn('employees_gross', function ($row) {
                    return getColumn_empValue($row->id);
                })
                ->rawColumns(['employees.id'])
                ->make(true);
            }
            elseif($request->type == 'reg_emp'){
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('nationality', 'nationality.id', '=', 'employees.nationality')
                ->leftJoin('city', 'city.id', '=', 'employees.ethnicity')
                ->leftJoin('country', 'country.code', '=', 'employees.country')
                ->leftJoin('province', 'province.id', '=', 'employees.province')
                ->leftJoin('companystructures as t2', 't2.id', '=', 'companystructures.parent')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                // ->where('employees.suspended', '=', 1)
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id' , 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','employees.ssn_num', 'employees.father_cnic' ,'employees.city','employees.mobile_phone','employees.driving_license','employees.other_id','employees.birthday','employees.passcode','employees.pessi','employees.eobi_status','employees.provident_fund','employees.covid_vaccinated','employees.department','employees.pay_grade','employees.work_station_id','employees.confirmation_date','employees.joined_date','employees.work_email','employees.private_email','employees.work_phone','employees.home_phone','employees.postal_code','province.name as provinceName','country.namecap','city.name as cityName','employees.eobi','employees.marital_status','employees.gender','nationality.name as nationalityName','employees.bank_name','employees.account_title','employees.account_number' , 'employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title','t2.title as parent', 'employmentstatus.name as employment_status', 'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name','employees.address1','employees.address2', 'employees.suspension_date');
                return DataTables::eloquent($employees)->addColumn('employees_gross', function ($row) {
                    return getColumn_empValue($row->id);
                })
                ->rawColumns(['employees.id'])->make(true);
            }
            else{
                $employees = Employee::leftJoin('companystructures', 'companystructures.id', '=', 'employees.department')
                ->leftJoin('nationality', 'nationality.id', '=', 'employees.nationality')
                ->leftJoin('city', 'city.id', '=', 'employees.ethnicity')
                ->leftJoin('country', 'country.code', '=', 'employees.country')
                ->leftJoin('province', 'province.id', '=', 'employees.province')
                ->leftJoin('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                ->leftJoin('companystructures as t2', 't2.id', '=', 'companystructures.parent')
                ->leftJoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                ->leftJoin('employees as e1', 'e1.id', '=', 'employees.supervisor')
                ->where('employees.status', '=', 'Terminated')
                ->whereIn('employees.department', login_user_departments())
                ->select('employees.id', 'employees.employee_id' , 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','employees.ssn_num', 'employees.father_cnic' ,'employees.city','employees.mobile_phone','employees.driving_license','employees.other_id','employees.birthday','employees.passcode','employees.pessi','employees.eobi_status','employees.provident_fund','employees.covid_vaccinated','employees.department','employees.pay_grade','employees.work_station_id','employees.confirmation_date','employees.joined_date','employees.work_email','employees.private_email','employees.work_phone','employees.home_phone','employees.postal_code','province.name as provinceName','country.namecap','city.name as cityName','employees.eobi','employees.marital_status','employees.gender','nationality.name as nationalityName','employees.bank_name','employees.account_title','employees.account_number' , 'employees.nic_num' , 'employees.cnic_expiry_date' , 'companystructures.title','t2.title as parent', 'employmentstatus.name as employment_status', 'jobtitles.name as job_title', 'e1.first_name as super_first_name', 'e1.last_name as super_last_name','employees.address1','employees.address2', 'employees.suspension_date', 'employees.termination_date');
                // return DataTables::eloquent($employees)->addIndexColumn()->make(true);
                return DataTables::eloquent($employees)->filter(function ($employees) use ($request) {
                    if(!empty($request->departmentFilter)){
                        if(env('COMPANY') != 'JSML'){
                            $all_childs = getChildren($request->departmentFilter);
                            array_push($all_childs , $request->departmentFilter);
                            $employees->whereIn('employees.department', $all_childs);
                        }else{
                            $employees->whereIn('employees.department', $request->section);
                        }
                    }
                    if(!empty($request->employeeFilter)){
                        $employees->where('employees.id', $request->employeeFilter);
                    }
                    if(!empty($request->empCityFilter)){
                        $employees->where('employees.city', $request->empCityFilter);
                    }
                    if(!empty($request->empJobTitleFilter)){
                        $employees->where('employees.job_title', $request->empJobTitleFilter);
                    }
                    if(!empty($request->empJoinFromDateFilter) && !empty($request->empJoinToDateFilter)){
                        $employees->whereBetween('employees.joined_date' , [$request->empJoinFromDateFilter , $request->empJoinToDateFilter]);
                    }
                    if(!empty($request->empConFromDateFilter) && !empty($request->empConToDateFilter)){
                        $employees->whereBetween('employees.confirmation_date' , [$request->empConFromDateFilter , $request->empConToDateFilter]);
                    }
                    if(!empty($request->empPayFilter)){
                        $employees->where('employees.pay_grade', $request->empPayFilter);
                    }
                    if(!empty($request->empBloodFilter)){
                        $employees->where('employees.blood_group', $request->empBloodFilter);
                    }
                }, true)
                ->addColumn('employees_gross', function ($row) {
                    return getColumn_empValue($row->id);
                })
                ->rawColumns(['employees.id'])
                ->make(true);
            }
        }
        $cities = City::get();
        $jobtitles = JobTitle::get();
        $employmentstatus = EmploymentStatus::get();
        $payGrades = PayGrade::get();
        $env = env('COMPANY');
        return view('Admin.employees.list.jsmlindex' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = DB::table('country')->get();
        $provinces = Province::get();
        $nationality = Nationality::get();
        $ethnicity = Ethnicity::get();
        $cities = DB::table('city')->get();
        $banks = DB::table('banks')->get();
        $employmentstatus = DB::table('employmentstatus')->get();
        $jobtitles = DB::table('jobtitles')->get();
        $companystructures = DB::table('companystructures')->get();
        $paygrades = DB::table('paygrades')->get();
        $leaveGroups = LeaveGroup::all(['id', 'name']);
        $shift_types = ShiftType::get();
        $work_weeks = WorkWeek::get();
        $payFrequency = PayFrequency::get(['id', 'name']);
        $currencies = CurrencyType::get(['id', 'code']);
        $calculationGroup = DeductionGroup::get(['id', 'name']);
        $calculation_Exemptions_Assigned = Deduction::get(['id', 'name']);
        $SalaryComponentTypes = SalaryComponentType::get();
        foreach ($SalaryComponentTypes as $SalaryComponentType) {
            $salary_components_result = SalaryComponent::where('is_monthly', 0)->where('componentType', $SalaryComponentType->id)->get();
            if(count($salary_components_result) > 0){
                $salary_components[$SalaryComponentType->id] = $salary_components_result;
            }
            $monthly_salary_components_result = SalaryComponent::where('is_monthly', 1)->where('componentType', $SalaryComponentType->id)->get();
            if(count($monthly_salary_components_result) > 0){
                $monthly_salary_components[$SalaryComponentType->id] = $monthly_salary_components_result;
            }
        }
        return view('Admin.employees.list.create' , \get_defined_vars() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $employee_id = Employee::where('nic_num',$request->nic_num)->first();
        $employee_unique = Employee::where('employee_id',$request->employee_id)->first();
        try{
            if(env('COMPANY') == 'CIDEX')
            {
                $this->validate($request, [
                    'employee_id' => 'required|unique:employees',
                    'sequence' => 'required|unique:employees',
                    // 'first_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    // 'last_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    'father_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                ]);
            }
            else
            {
                $this->validate($request, [
                    'employee_id' => 'required|unique:employees',
                    'nic_num' => 'required|unique:employees',
                    // 'employee_code' => 'required|unique:employees',
                    'first_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    // 'last_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    'father_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    // 'birthday' => 'required',
                    // 'province' => 'required',
                    // 'postal_code' => 'required',
                    // 'city' => 'required',
                    // 'address1' => 'required|max:255',
                    // 'mobile_phone' => 'required',
                    // 'private_email' => 'required|email',
                    // 'joined_date' => 'required|date',
                    'employment_status' => 'required',
                    'job_title' => 'required',
                    'department' => 'required',
                ], [
                    'employee_id.unique' => 'The Employee ID has already been taken on Employee ID"' . ($employee_unique ? $employee_unique->employee_id : 'NULL') . '" and on Employee Code "' . ($employee_unique ? $employee_unique->employee_code : 'NULL') . '"',
                    'nic_num.unique' => 'The CNIC number has already been taken on Employee ID "' . ($employee_id ? $employee_id->employee_id : 'NULL') . '" and on Employee Code "' . ($employee_id ? $employee_id->employee_code : 'NULL') . '"',
                    // Other custom validation messages...
                ]);
            }
            $this->validate($request, [
                'shift_id' => 'required_with:work_week_id,shift_from_date,shift_to_date',
                'work_week_id' => 'required_with:shift_id,shift_from_date,shift_to_date',
                'shift_from_date' => 'required_with:shift_id,work_week_id,shift_to_date',
                'shift_to_date' => 'required_with:shift_id,work_week_id,shift_from_date',
            ]);
            $employee = new Employee();
            $employee->blood_group =  $request->blood_group;
            $data = $request->all();
            unset($data['leave_groups']);
            unset($data['new_leave_settings']);
            unset($data['shift_id']);
            unset($data['work_week_id']);
            unset($data['shift_from_date']);
            unset($data['shift_to_date']);
            unset($data['late_time_allowed']);
            unset($data['calculationGroups']);
            unset($data['components']);
            unset($data['sc_amount']);
            unset($data['monthly_components']);
            unset($data['monthly_sc_amount']);
            unset($data['applied_month']);
            if(!empty($data['bank_id'])){
                $bank = Bank::find($data['bank_id']);
                $data['bank_name'] = $bank->name;
            }
            $employee = $employee->create($data);
            $last_emp_id = Employee::latest('id')->first();
            DB::table('employees_history')->insert([
                'emp_id' => $request->employee_id,
                'first_name' => $request->first_name,
                'middle_name' => !empty($request->middle_name) ? $request->middle_name : '',
                'last_name' => $request->last_name,
                'religion' => $request->religion,
                'marital_status' => $request->marital_status,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'city' => $request->city,
                'tax_no' => $request->tax_no,
                'country' => $request->country,
                'province' => $request->province,
                'mobile_phone' => $request->mobile_phone,
                'work_email' => $request->work_email,
                'department' => $request->department,
                'status' => 'Active',
                'employment_status' => $request->employment_status,
                'job_title' => $request->job_title,
                'joining_status' => 1,
                'paygrade' => $request->paygrade,
                'home_phone' => $request->home_phone,
                'work_phone' => $request->work_phone,
                'private_email' => $request->private_email,
                'supervisor' => $request->supervisor,
                'posting_station' => $request->posting_station,
                'employee_id' => $last_emp_id->id,
                'created_by' => Auth::id(),
            ]);
            if($request->has('leave_groups')){
                $employee->leaveGroups()->attach($request->leave_groups);
            }
            if($request->has('new_leave_settings')){
                $leave_settings = json_decode($request->new_leave_settings);
                $lastDayOfYear = Carbon::create(now()->year, 12, 31)->toDateString();
                foreach ($leave_settings as $leave_setting) {
                    $employee->newEmployeesLeavesSettings()->create([
                        'leave_type_id' => $leave_setting->leaveTypeID,
                        'amount' => $leave_setting->allowed,
                        'expiry' => $lastDayOfYear,
                    ]);
                }
            }
            if ($request->filled('shift_id') && $request->filled('work_week_id') && $request->filled('shift_from_date') && $request->filled('shift_to_date') && $request->filled('late_time_allowed')) {
                ShiftManagement::create([
                    'employee_id' => $employee->id,
                    'shift_id' => $request->shift_id,
                    'work_week_id' => $request->work_week_id,
                    'shift_from_date' => $request->shift_from_date,
                    'shift_to_date' => $request->shift_to_date,
                    'late_time_allowed' => $request->late_time_allowed,
                    'status' => 'Approved',
                    'created_by' => Auth::id(),
                ]);
            }
            if ($request->filled('calculationGroups')) {
                foreach ($request->calculationGroups as $calculation_group) {
                    PayrollEmployee::create([
                        'employee' => $employee->id,
                        'deduction_group' => $calculation_group['calculation_group'],
                        'pay_frequency' => $calculation_group['pay_frequency'],
                        'currency' => $calculation_group['currency'],
                        'deduction_exemptions' => $calculation_group['deduction_exemptions'] ?? null,
                        'deduction_allowed' => $calculation_group['deduction_allowed'] ?? null,
                    ]);
                }
            }
            if ($request->filled('components')) {
                foreach ($request->components as $component) {
                    $this->makeSalaryComponent($employee, $component, $request->sc_amount);
                }
            }
            if ($request->filled('monthly_components')) {
                foreach ($request->monthly_components as $component) {
                    $this->makeSalaryComponent($employee, $component, $request->monthly_sc_amount);

                    DB::table('monthly_salary_component')->insert([
                        'employee_id' => $employee->id,
                        'component_id' => $component,
                        'component_month' => !empty($request->applied_month) ? $request->applied_month.'-01' : null,
                        'amount' => $request->monthly_sc_amount[$component],
                        'created_by' => Auth::id(),
                    ]);
                }
            }
            DB::commit();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            DB::rollback();
            if($e instanceof ValidationException){
                return ['code'=>'300','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $all_childs = getChildren($id);
        array_push($all_childs , $id);

        $query = DB::table('employees')->join('jobtitles' , 'jobtitles.id' , 'employees.job_title')
        ->join('companystructures' , 'companystructures.id' , 'employees.department')
        ->where('employees.status' , 'Active')
        ->whereIn('employees.department' , $all_childs)
        ->select('employees.id' , 'employees.first_name' , 'employees.last_name' , 'employees.employee_id' , 'employees.employee_code' , 'jobtitles.name AS designation' , 'companystructures.title AS department')
        ->get();
        return response()->json($query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::with(['leaveGroups', 'calculationGroups'])->find($id);
        $countries = DB::table('country')->get();
        // $nationality = DB::table('nationality')->get();
        // $provinces = DB::table('province')->get();
        // $ethnicity = DB::table('ethnicity')->get();
        $provinces = Province::get();
        $nationality = Nationality::get();
        $ethnicity = Ethnicity::get();
        $cities = DB::table('city')->get();
        $banks = DB::table('banks')->get();
        $employmentstatus = DB::table('employmentstatus')->get();
        $jobtitles = DB::table('jobtitles')->get();
        $companystructures = DB::table('companystructures')->get();
        $paygrades = DB::table('paygrades')->get();
        $leaveGroups = LeaveGroup::all(['id', 'name']);
        $shift_types = ShiftType::get();
        $work_weeks = WorkWeek::get();
        $payFrequency = PayFrequency::get(['id', 'name']);
        $currencies = CurrencyType::get(['id', 'code']);
        $calculationGroup = DeductionGroup::get(['id', 'name']);
        $calculation_Exemptions_Assigned = Deduction::get(['id', 'name']);
        $SalaryComponentTypes = SalaryComponentType::get();
        foreach ($SalaryComponentTypes as $SalaryComponentType) {
            $salary_components_result = SalaryComponent::where('is_monthly', 0)->where('componentType', $SalaryComponentType->id)->get();
            if(count($salary_components_result) > 0){
                $salary_components[$SalaryComponentType->id] = $salary_components_result;
            }
            foreach($salary_components_result as $component)
            {
                $employee_component = DB::table('employeesalary')->where('employee', $id)->where('component', $component->id)->select('amount' , 'details')->first();
                $comp[$component->id] = !empty($employee_component) ? $employee_component->amount : 0.00;
                $remarks[$component->id] = !empty($employee_component) && !empty($employee_component->details) ? $employee_component->details : '';
            }
            $monthly_salary_components_result = SalaryComponent::where('is_monthly', 1)->where('componentType', $SalaryComponentType->id)->get();
            if(count($monthly_salary_components_result) > 0){
                $monthly_salary_components[$SalaryComponentType->id] = $monthly_salary_components_result;
                $monthly_salary_component = $monthly_salary_components_result[0];
            }
            foreach($monthly_salary_components_result as $component)
            {
                $employee_component = DB::table('employeesalary')->where('employee', $id)->where('component', $component->id)->select('amount' , 'details')->first();
                $month_comp[$component->id] = !empty($employee_component) ? $employee_component->amount : 0.00;
                $month_remarks[$component->id] = !empty($employee_component) && !empty($employee_component->details) ? $employee_component->details : '';
            }
        }
        if (!empty($monthly_salary_component)) {
            $monthly_salary_component_applied_month = DB::table('monthly_salary_component')->where('employee_id', $id)->where('component_id', $monthly_salary_component->id)->first();
        }
        if(!empty($monthly_salary_component_applied_month)){
            $applied_month = $monthly_salary_component_applied_month->component_month;
        }
        else{
            $applied_month = '';
        }
        return view('Admin.employees.list.edit', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        $employee_id = Employee::where('nic_num',$request->nic_num)->first();
        try{
            if(env('COMPANY') == 'CIDEX')
            {
                $this->validate($request, [
                    'employee_id' => 'required|unique:employees,employee_id,'.$id,
                    'sequence' => 'required|unique:employees,sequence,'.$id,
                    'first_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    // 'last_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    // 'father_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                ]);
            }
            else
            {
                $this->validate($request, [
                    // 'employee_id' => 'required|unique:employees,employee_id,'.$id,
                    // 'employee_code' => 'required|unique:employees',
                    'first_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    'nic_num' => 'unique:employees,nic_num,'.$id.'id',
                    // 'last_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    // 'father_name' => 'required|string|max:255|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                    // 'birthday' => 'required',
                    // 'province' => 'required',
                    // 'postal_code' => 'required',
                    // 'city' => 'required',
                    // 'address1' => 'required|max:255',
                    // 'mobile_phone' => 'required',
                    // 'private_email' => 'required|email',
                    // 'joined_date' => 'required|date',
                    'employment_status' => 'required',
                    'job_title' => 'required',
                    // 'department' => 'required',
                ], [
                    'nic_num.unique' => 'The CNIC number has already been taken on Employee ID "' . ($employee_id ? $employee_id->employee_id : 'NULL') . '" and on Employee Code "' . ($employee_id ? $employee_id->employee_code : 'NULL') . '"',
                ]);
            }
            $this->validate($request, [
                'shift_id' => 'required_with:work_week_id,shift_from_date,shift_to_date',
                'work_week_id' => 'required_with:shift_id,shift_from_date,shift_to_date',
                'shift_from_date' => 'required_with:shift_id,work_week_id,shift_to_date',
                'shift_to_date' => 'required_with:shift_id,work_week_id,shift_from_date',
                'calculationGroups.*.calculation_group' => [
                    function ($attribute, $value, $fail) use ($id) {
                        // Get the index of the current item being validated
                        $index = explode('.', $attribute)[1];

                        // Check if the current item has a non-null id (indicating an update)
                        if(isset(request('calculationGroups')[$index]['id'])){
                            if (!is_null(request('calculationGroups')[$index]['id'])) {
                                return;
                            }
                        }

                        // Check for uniqueness within the current employee's records
                        $existingRecords = PayrollEmployee::where('employee', $id)
                            ->where('deduction_group', $value)
                            ->count();

                        if ($existingRecords > 0) {
                            $fail('The calculation group must be unique for the current employee.');
                        }
                    },
                ],
            ]);

            $data = $request->all();
            unset($data['leave_groups']);
            unset($data['new_leave_settings']);
            unset($data['shift_id']);
            unset($data['work_week_id']);
            unset($data['shift_from_date']);
            unset($data['shift_to_date']);
            unset($data['late_time_allowed']);
            unset($data['calculationGroups']);
            unset($data['components']);
            unset($data['sc_amount']);
            unset($data['monthly_components']);
            unset($data['monthly_sc_amount']);
            unset($data['applied_month']);
            unset($data['remarks']);
            unset($data['monthly_remarks']);
            if(!empty($data['bank_id'])){
                $bank = Bank::find($data['bank_id']);
                $data['bank_name'] = $bank->name;
            }
            $employee = Employee::find($id);
            $employee->fill($data)->save();

            DB::table('employees_history')->insert([
                'emp_id' => $request->employee_id,
                'first_name' => $request->first_name,
                'middle_name' => !empty($request->middle_name) ? $request->middle_name : '',
                'last_name' => $request->last_name,
                'religion' => $request->religion,
                'marital_status' => $request->marital_status,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'city' => $request->city,
                'country' => $request->country,
                'province' => $request->province,
                'mobile_phone' => $request->mobile_phone,
                'work_email' => $request->work_email,
                'department' => $request->department,
                'status' => $employee->status,
                'employment_status' => $request->employment_status,
                'job_title' => $request->job_title,
                'paygrade' => $request->paygrade,
                'home_phone' => $request->home_phone,
                'work_phone' => $request->work_phone,
                'private_email' => $request->private_email,
                'supervisor' => $request->supervisor,
                'posting_station' => $request->posting_station,
                'employee_id' => $employee->id,
                'created_by' => Auth::id(),
            ]);
            if($request->has('leave_groups')){
                $employee->leaveGroups()->sync($request->leave_groups);
            }
            if($request->has('new_leave_settings')){
                $employee->newEmployeesLeavesSettings->map->delete();
                $leave_settings = json_decode($request->new_leave_settings);
                $lastDayOfYear = Carbon::create(now()->year, 12, 31)->toDateString();
                foreach ($leave_settings as $leave_setting) {
                    $employee->newEmployeesLeavesSettings()->create([
                        'leave_type_id' => $leave_setting->leaveTypeID,
                        'amount' => $leave_setting->allowed,
                        'expiry' => $lastDayOfYear,
                    ]);
                }
            }
            if ($request->filled('shift_id') && $request->filled('work_week_id') && $request->filled('shift_from_date') && $request->filled('shift_to_date') && $request->filled('late_time_allowed')) {
                ShiftManagement::create([
                    'employee_id' => $employee->id,
                    'shift_id' => $request->shift_id,
                    'work_week_id' => $request->work_week_id,
                    'shift_from_date' => $request->shift_from_date,
                    'shift_to_date' => $request->shift_to_date,
                    'late_time_allowed' => $request->late_time_allowed,
                    'status' => 'Approved',
                    'created_by' => Auth::id(),
                ]);
            }
            if ($request->filled('calculationGroups')) {
                foreach ($request->calculationGroups as $calculation_group) {
                    PayrollEmployee::updateOrCreate([
                        'id' => isset($calculation_group['id']) ? $calculation_group['id'] : 0,
                    ],[
                        'employee' => $employee->id,
                        'deduction_group' => $calculation_group['calculation_group'],
                        'pay_frequency' => $calculation_group['pay_frequency'],
                        'currency' => $calculation_group['currency'],
                        'deduction_exemptions' => $calculation_group['deduction_exemptions'] ?? null,
                        'deduction_allowed' => $calculation_group['deduction_allowed'] ?? null,
                    ]);
                }
            }
            if ($request->filled('components')) {
                foreach ($request->components as $key => $component) {
                    $this->updateSalaryComponent($component, $request->sc_amount, $request->remarks, $id);
                }
            }
            if ($request->filled('monthly_components')) {
                foreach ($request->monthly_components as $component) {
                    $this->updateSalaryComponent($component, $request->monthly_sc_amount, $request->monthly_remarks, $id);

                    DB::table('monthly_salary_component')->updateOrInsert(
                        ['employee_id' => $id,'component_id' =>$component,'component_month' => !empty($request->applied_month) ? $request->applied_month.'-01' : null],
                        [
                            'employee_id' => $id,
                            'component_id' => $component,
                            'component_month' => !empty($request->applied_month) ? $request->applied_month.'-01' : null,
                            'amount' => $request->monthly_sc_amount[$component],
                            'remarks' => $request->monthly_remarks[$component],
                            'created_by' => Auth::id(),
                        ]
                    );
                }
            }
            DB::commit();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            DB::rollback();
            if($e instanceof ValidationException){
                return ['code'=>'300','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function suspend(Request $request)
    {
        try{
            $request->validate([
                'suspension_date' => 'required'
            ]);
            $employee = Employee::find($request->employee_id);
            $employee->suspension_date = $request->suspension_date;
            $employee->restore_date = NULL;
            $employee->suspended = 1;
            $employee->status = 'Suspended';
            $employee->update();

            DB::table('employees_history')->insert([
                'emp_id' => $employee->employee_id,
                'first_name' => $employee->first_name,
                'middle_name' => !empty($employee->middle_name) ? $employee->middle_name : '',
                'last_name' => $employee->last_name,
                'marital_status' => $employee->marital_status,
                'address1' => $employee->address1,
                'address2' => $employee->address2,
                'city' => $employee->city,
                'country' => $employee->country,
                'province' => $employee->province,
                'mobile_phone' => $employee->mobile_phone,
                'work_email' => $employee->work_email,
                'department' => $employee->department,
                'status' => 'Suspended',
                'employment_status' => $employee->employment_status,
                'job_title' => $employee->job_title,
                'paygrade' => $employee->paygrade,
                'home_phone' => $employee->home_phone,
                'work_phone' => $employee->work_phone,
                'private_email' => $employee->private_email,
                'supervisor' => $employee->supervisor,
                'posting_station' => $employee->posting_station,
                'employee_id' => $employee->id,
                'created_by' => Auth::id(),
            ]);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'300','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    public function act_inact_employee(Request $request)
    {
        try{
            $request->validate([
                'active_inactive' => 'required'
            ]);
            $employee = Employee::find($request->employee_id);
            $employee->active = $request->active_inactive;
            $employee->update();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'300','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    public function terminate(Request $request)
    {
        try{
            $request->validate([
                'termination_date' => 'required'
            ]);
            $employee = Employee::find($request->employee_id);
            $employee->termination_date = $request->termination_date;
            $employee->terminate_reason = $request->terminate_reason;
            $employee->separation_type = $request->separation_type;
            $employee->restore_date = NULL;
            $employee->employee_status_start_date = $request->employee_status_start_date;
            $employee->notice_period_status_end_date = $request->notice_period_status_end_date;
            if($request->hasFile('termination_file')){
                $termination_file = Employee::InsertFile($request->file('termination_file'),'/termination_files');
            }
            $employee->termination_file = !empty($termination_file) ? $termination_file : NULL;
            $employee->status = 'Terminated';
            $employee->suspended = 0;
            $employee->thumb_print1 = NULL;
            $employee->thumb_print2 = NULL;
            $employee->thumb_print3 = NULL;
            $employee->update();

            DB::table('employee_termination_history')->insert([
                'employee_id' => $employee->id,
                'termination_date' => $request->termination_date,
                'termination_reason' => $request->terminate_reason,
                'separation_type' => $request->separation_type,
                'notice_start_date' => $request->employee_status_start_date,
                'notice_end_date' => $request->notice_period_status_end_date,
                'attachment' => !empty($termination_file) ? $termination_file : NULL
            ]);

            DB::table('employees_history')->insert([
                'emp_id' => $employee->employee_id,
                'first_name' => $employee->first_name,
                'middle_name' => !empty($employee->middle_name) ? $employee->middle_name : '',
                'last_name' => $employee->last_name,
                'marital_status' => $employee->marital_status,
                'address1' => $employee->address1,
                'address2' => $employee->address2,
                'city' => $employee->city,
                'country' => $employee->country,
                'province' => $employee->province,
                'mobile_phone' => $employee->mobile_phone,
                'work_email' => $employee->work_email,
                'department' => $employee->department,
                'status' => 'Terminated',
                'employment_status' => $employee->employment_status,
                'job_title' => $employee->job_title,
                'paygrade' => $employee->paygrade,
                'home_phone' => $employee->home_phone,
                'work_phone' => $employee->work_phone,
                'private_email' => $employee->private_email,
                'supervisor' => $employee->supervisor,
                'posting_station' => $employee->posting_station,
                'employee_id' => $employee->id,
                'created_by' => Auth::id(),
            ]);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'300','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    public function restore(Request $request)
    {
        try{
            $request->validate([
                'restore_date' => 'required'
            ]);
            $employee = Employee::find($request->employee_id);
            $employee->restore_date = $request->restore_date;
            $employee->termination_date = NULL;
            $employee->suspension_date = NULL;
            $employee->status = 'Active';
            $employee->suspended = 0;
            $employee->update();

            DB::table('employees_history')->insert([
                'emp_id' => $employee->employee_id,
                'first_name' => $employee->first_name,
                'middle_name' => !empty($employee->middle_name) ? $employee->middle_name : '',
                'last_name' => $employee->last_name,
                'marital_status' => $employee->marital_status,
                'restore_date' => $request->restore_date,
                'address1' => $employee->address1,
                'address2' => $employee->address2,
                'city' => $employee->city,
                'country' => $employee->country,
                'province' => $employee->province,
                'mobile_phone' => $employee->mobile_phone,
                'work_email' => $employee->work_email,
                'department' => $employee->department,
                'status' => 'Active',
                'employment_status' => $employee->employment_status,
                'job_title' => $employee->job_title,
                'paygrade' => $employee->paygrade,
                'home_phone' => $employee->home_phone,
                'work_phone' => $employee->work_phone,
                'private_email' => $employee->private_email,
                'supervisor' => $employee->supervisor,
                'posting_station' => $employee->posting_station,
                'employee_id' => $employee->id,
                'created_by' => Auth::id(),
            ]);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'300','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    public function off_roll(Request $request){
        try{
            $request->validate([
                'off_roll_date' => 'required'
            ]);
            $employee = Employee::find($request->employee_id);
            $employee->off_roll_date = $request->off_roll_date;
            $employee->on_roll_date = NULL;
            $employee->seasonal_status = 'Off Roll';
            $employee->update();

            DB::table('employees_history')->insert([
                'emp_id' => $employee->employee_id,
                'first_name' => $employee->first_name,
                'middle_name' => !empty($employee->middle_name) ? $employee->middle_name : '',
                'last_name' => $employee->last_name,
                'marital_status' => $employee->marital_status,
                'address1' => $employee->address1,
                'address2' => $employee->address2,
                'city' => $employee->city,
                'country' => $employee->country,
                'province' => $employee->province,
                'mobile_phone' => $employee->mobile_phone,
                'work_email' => $employee->work_email,
                'department' => $employee->department,
                'status' => 'Active',
                'off_roll_status' => 1,
                'employment_status' => $employee->employment_status,
                'job_title' => $employee->job_title,
                'paygrade' => $employee->paygrade,
                'home_phone' => $employee->home_phone,
                'work_phone' => $employee->work_phone,
                'private_email' => $employee->private_email,
                'supervisor' => $employee->supervisor,
                'posting_station' => $employee->posting_station,
                'employee_id' => $employee->id,
                'created_by' => Auth::id(),
            ]);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'300','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    public function on_roll(Request $request){
        try{
            $request->validate([
                'on_roll_date' => 'required'
            ]);
            $employee = Employee::find($request->employee_id);
            $employee->on_roll_date = $request->on_roll_date;
            $employee->off_roll_date = NULL;
            $employee->seasonal_status = 'On Roll';
            $employee->update();

            DB::table('employees_history')->insert([
                'emp_id' => $employee->employee_id,
                'first_name' => $employee->first_name,
                'middle_name' => !empty($employee->middle_name) ? $employee->middle_name : '',
                'last_name' => $employee->last_name,
                'marital_status' => $employee->marital_status,
                'address1' => $employee->address1,
                'address2' => $employee->address2,
                'city' => $employee->city,
                'country' => $employee->country,
                'province' => $employee->province,
                'mobile_phone' => $employee->mobile_phone,
                'work_email' => $employee->work_email,
                'department' => $employee->department,
                'status' => 'Active',
                'on_roll_status' => 1,
                'employment_status' => $employee->employment_status,
                'job_title' => $employee->job_title,
                'paygrade' => $employee->paygrade,
                'home_phone' => $employee->home_phone,
                'work_phone' => $employee->work_phone,
                'private_email' => $employee->private_email,
                'supervisor' => $employee->supervisor,
                'posting_station' => $employee->posting_station,
                'employee_id' => $employee->id,
                'created_by' => Auth::id(),
            ]);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'300','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    public function finalSettlementLetter(Request $request){
        if(!empty($request->employee_id)){
            $basic_salary_column = PayrollColumn::where('salary_column_type','basic_salary')->first();
            $final_columns = FinalSettlementSetup::first();
            if(!empty($basic_salary_column)){
                $basic_salary_column = $basic_salary_column->salary_components;
                if (env('COMPANY') != 'RoofLine'){
                    $basic_salary = EmployeeSalary::where('employee',$request->employee_id)->where('component',json_decode($basic_salary_column))->first();
                    $basic_salary = $basic_salary->amount ?? 0;
                }
            }
            else{
                $basic_salary = 0;
            }
            $data = Employee::leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')->where('employees.id',$request->employee_id)
            ->leftjoin('employmentstatus' , 'employmentstatus.id' , '=' , 'employees.employment_status')
            ->leftjoin('companystructures', 'companystructures.id', '=', 'employees.department')
            ->select('employees.id','employees.employee_id','employees.employee_code', 'employees.first_name', 'employees.last_name' , 'employees.department' , 'employees.nic_num','employees.image','employees.terminate_reason' , 'employees.termination_date' ,'companystructures.title','employees.joined_date','employees.notice_period_status_end_date','jobtitles.name as job_title' , 'employmentstatus.name as employment')->first();

            $days_payable = DB::table('payroll')->join('payrolldata' , 'payrolldata.payroll' , '=' , 'payroll.id')->where([
                ['payrolldata.employee' , $data->id],
                ['payroll.department' , $data->department],
                ['payrolldata.payroll_item' , '52'],
            ])->orderBy('payroll.id' , 'desc')->first(['payroll.date_start' , 'payrolldata.amount']);
        }
        return view('Admin.employees.employee_settlement_letter.index',get_defined_vars());
    }
    public function settlement_history(Request $request)
    {
        if($request->ajax())
        {
            $data = FinalRecord::leftjoin('employees' , 'employees.id' , '=' , 'final_settlement_records.emp_id')
            ->leftjoin('companystructures', 'companystructures.id', '=', 'employees.department')
            ->select('final_settlement_records.*','employees.employee_id','employees.employee_code', 'employees.first_name', 'employees.last_name' , 'employees.department' , 'employees.nic_num','employees.image','employees.terminate_reason' , 'employees.termination_date' ,'employees.joined_date','companystructures.title','employees.notice_period_status_end_date');
            return Datatables::eloquent($data)->addIndexColumn()->make(true);
        }
        return view('Admin.employees.employee_settlement_letter.final_history.index',get_defined_vars());
    }
    public function settlement_history_print(Request $request,$id)
    {
        $final_columns = FinalSettlementSetup::first();
        $data = FinalRecord::where('final_settlement_records.id',$id)->leftjoin('employees' , 'employees.id' , '=' , 'final_settlement_records.emp_id')
        ->leftjoin('companystructures', 'companystructures.id', '=', 'employees.department')
        ->select('final_settlement_records.*','employees.id','employees.employee_id','employees.employee_code', 'employees.first_name', 'employees.last_name' , 'employees.department' , 'employees.nic_num','employees.image','employees.terminate_reason' , 'employees.termination_date' ,'employees.joined_date','companystructures.title','employees.notice_period_status_end_date')->first();
        return view('Admin.employees.employee_settlement_letter.final_history_print.index',get_defined_vars());
    }
    public function final_record(Request $request)
    {
        try{
            $this->validate($request , [

            ]);
            $final_rec = new FinalRecord();
            $final_rec->emp_id = $request->emp_id;
            $final_rec->resignation = $request->resignation;
            $final_rec->termination = $request->termination;
            $final_rec->layoff = $request->layoff;
            $final_rec->day_payable = $request->day_payable;
            $final_rec->days_payable_remarks = $request->days_payable_remarks;
            $final_rec->unpaid = $request->unpaid;
            $final_rec->unpaid_remarks = $request->unpaid_remarks;
            $final_rec->leave_encash = $request->leave_encash;
            $final_rec->leave_encash_remarks = $request->leave_encash_remarks;
            $final_rec->extra_leaves = $request->extra_leaves;
            $final_rec->extra_leaves_remarks = $request->extra_leaves_remarks;
            $final_rec->overtime_1 = $request->overtime_1;
            $final_rec->overtime_1_remarks = $request->overtime_1_remarks;
            $final_rec->notice_period = $request->notice_period;
            $final_rec->notice_period_remarks = $request->notice_period_remarks;
            $final_rec->overtime_2 = $request->overtime_2;
            $final_rec->overtime_2_remarks = $request->overtime_2_remarks;
            $final_rec->eobi = $request->eobi;
            $final_rec->eobi_remarks = $request->eobi_remarks;
            $final_rec->food = $request->food;
            $final_rec->food_remarks = $request->food_remarks;
            $final_rec->tardiness = $request->tardiness;
            $final_rec->tardiness_remarks = $request->tardiness_remarks;
            $final_rec->arrears = $request->arrears;
            $final_rec->arrears_remarks = $request->arrears_remarks;
            $final_rec->loan_advance = $request->loan_advance;
            $final_rec->loan_advance_remarks = $request->loan_advance_remarks;
            $final_rec->others_1 = $request->others_1;
            $final_rec->others_1_remarks = $request->others_1_remarks;
            $final_rec->others_2 = $request->others_2;
            $final_rec->others_2_remarks = $request->others_2_remarks;
            $final_rec->entitlement_net = $request->entitlement_net;
            $final_rec->deduction_net = $request->deduction_net;
            $final_rec->net_all = $request->net_all;
            $final_rec->basic_salary = $request->basic_salary;
            $final_rec->notice_period_value = $request->notice_period_value;
            $final_rec->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    public function getDeptTerminatedEmployees(Request $request){
        $data = DB::table('employees')
        ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
        ->where('department', $request->department_id)->where('status', '=', 'Terminated')->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name' , 'employees.last_name','jobtitles.name as desigination')
        ->get();
        return response()->json($data);
    }

    private function makeSalaryComponent($employee, $component, $sc_amount){
        $employee_component = new EmployeeSalaryComponent();
        $employee_component->employee = $employee->id;
        $employee_component->component = $component;
        if(env('COMPANY') == 'CLINIX'){
            $status = 'Pending';
        }
        else{
            $status = 'Approved';
        }
        $employee_component->amount = $sc_amount[$component];
        $employee_component->status = $status;
        $employee_component->submitted_by = Auth::user()->id;
        $employee_component->save();
        $calculation_group = NULL;
        insertInSalaryComponentHistory($employee->id,$component,$sc_amount[$component],$calculation_group);
    }

    private function updateSalaryComponent($component, $sc_amount, $remarks, $id){
        $emp_component = EmployeeSalaryComponent::where('employee', $id)->where('component', $component)->first();
        if(!empty($emp_component)){
            $emp_component->amount = $sc_amount[$component];
            $emp_component->details = $remarks[$component];
            $emp_component->update();
        }
        else{
            $employee_component = new EmployeeSalaryComponent();
            $employee_component->employee = $id;
            $employee_component->component = $component;
            $employee_component->amount = $sc_amount[$component];
            $employee_component->details = $remarks[$component];
            $employee_component->save();
        }

        $calculation_group = NULL;
        $remarks = !empty($remarks[$component]) ? $remarks[$component] : NULL;
        insertInSalaryComponentHistory($id,$component,$sc_amount[$component],$calculation_group,$remarks);
    }
}
