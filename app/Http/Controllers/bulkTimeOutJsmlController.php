<?php
namespace App\Http\Controllers;

use DataTables;
use Carbon\Carbon;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\CompanyStructure;
use Illuminate\Support\Facades\DB;
class bulkTimeOutJsmlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $employees = [];
        if(!empty($request->departmentFilter)){
            $employees =  Employee::join('attendance' , 'attendance.employee' , '=' , 'employees.id')
            ->where('employees.status' , 'Active')
            ->whereIn('employees.department' , login_user_departments());
            if (isset(request()->section)) {
                $employees->whereIn('attendance.department_id', request()->section);
            } else {
                if (isset(request()->departmentFilter) && request()->departmentFilter != 'all') {
                    $employees->where('attendance.department_id', request()->departmentFilter);
                }
            }
            if(!empty($request->employeeFilter)){
                $employees = $employees->where('attendance.employee' , $request->employeeFilter);
            }
            if(!empty($request->fromDate)){
                // $month =  Carbon::Parse($request->date)->format('m');
                // $year =  Carbon::Parse($request->date)->format('Y');
                
                // $employees = $employees->whereYear('attendance.in_time' , $year)->whereMonth('attendance.in_time', $month);
                $employees = $employees->whereDate('attendance.in_time' , '>=', $request->fromDate)->whereDate('attendance.in_time', '<=', $request->toDate);
            }
            if(env('COMPANY') == 'JSML'){
                // $employees = $employees->whereNull('attendance.out_time');  
            }
            $employees = $employees->select('attendance.*' , 'employees.first_name' , 'employees.middle_name' ,  'employees.last_name' , 'employees.employee_id' , 'employees.employee_code')
            ->orderBy(DB::raw('DATE(attendance.in_time)') , 'asc')->get();
        }
        return view('Admin.timeManagement.bulkTimeOut.jsmlindex' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            foreach($request->attendance_ids as $key => $value){
                // dd($request->all());
                if (!empty($request->out_time[$value])) {
                    $in_time = date('Y-m-d H:i:s', strtotime($request->in_time[$value]));
                    $out_time = date('Y-m-d H:i:s', strtotime($request->out_time[$value]));
                    if(!empty($out_time)){
                        DB::table('attendance')->where('id' , $value)->update([
                            'in_time' => $in_time,
                            'out_time' => $out_time,
                        ]);
                    }
                }
            }
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request , $id)
    {
        if($request->ajax()){
            $query = DB::table('employees')->select('id' , 'first_name' , 'last_name')
                ->where('status' , '=' ,  'Active')
                ->where('department' , '=' , $id)
                ->get(); 
            return response()->json($query);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
