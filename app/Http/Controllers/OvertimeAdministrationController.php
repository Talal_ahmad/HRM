<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use Illuminate\Http\Request; 
use App\Models\OvertimeRequest;
use App\Models\OvertimeCategory;
use Illuminate\Support\Facades\DB;

class OvertimeAdministrationController extends Controller
{
    public function __construct(){ 
        $this->middleware('auth');
    }

    public function index(Request $request) 
    {

        if($request->ajax()){

            if($request->type == 'overtimeCategories'){
                return Datatables::eloquent(OvertimeCategory::query())->addIndexColumn()->make(true);
            }
            else{
                return DataTables::eloquent(OvertimeRequest::leftjoin('employees','employeeovertime.employee','=','employees.id')->leftjoin('overtimecategories','employeeovertime.category','=','overtimecategories.id')->leftjoin('projects','employeeovertime.project','=','projects.id')->select('employeeovertime.id','employeeovertime.start_time','employeeovertime.end_time','employeeovertime.project','employeeovertime.status','overtimecategories.name AS category','projects.name AS project' )->selectRaw('CONCAT(employees.first_name,employees.last_name) as "employeeName"'))->addIndexColumn()->make(true); 
            }
        }
        $category = DB::table('overtimecategories')->get();
        $project = DB::table('projects')->get();  
        return view('Admin.setup.overtimeAdministration.index', compact('category','project'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            if($request->type == 'overtimeCategories'){
                $this->validate($request, [
                    'name' => 'required',               
                ]);
                $data = $request->all();
                OvertimeCategory::create($data);
            }
            else{
                $this->validate($request, [
                    'employee' => 'required',
                    'category' => 'required',
                    'start_time' => 'required',               
                    'end_time' => 'required',
                ]);
                $data = $request->all();
                $data['start_time'] = date('Y-m-d').' '.$data['start_time'];
                $data['end_time'] = date('Y-m-d').' '.$data['end_time'];
                OvertimeRequest::create($data);
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        if($request->type == 'overtimeCategories')
        {
            $OvertimeCategory = OvertimeCategory::find($id);
            return response()->json($OvertimeCategory);
        }       
        else{
            $OvertimeRequest = OvertimeRequest::find($id);
            return response()->json($OvertimeRequest);
        }   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            if($request->type == 'overtimeCategories'){
                $this->validate($request, [
                    'name' => 'required',
                ]);
    
                $OvertimeCategory = OvertimeCategory::find($id);
                $OvertimeCategory->fill($request->all())->save();
            }
            else{
                $this->validate($request, [
                    'employee' => 'required',
                    'category' => 'required',
                    'start_time' => 'required',               
                    'end_time' => 'required',
                ]);
                
                $data = $request->all();
                $data['start_time'] = date('Y-m-d').' '.$data['start_time'];
                $data['end_time'] = date('Y-m-d').' '.$data['end_time'];
                $OvertimeRequest = OvertimeRequest::find($id);
                $OvertimeRequest->fill($data)->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            if($request->type == 'overtimeCategories'){    
                $OvertimeCategory = OvertimeCategory::find($id);
                $OvertimeCategory->deleted_by = Auth::id();
                $OvertimeCategory->update();
                $OvertimeCategory->delete();
            }else{
                $OvertimeRequest = OvertimeRequest::find($id);
                $OvertimeRequest->deleted_by = Auth::id();
                $OvertimeRequest->update();
                $OvertimeRequest->delete();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }  
    }
}