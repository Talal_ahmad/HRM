<?php

namespace App\Http\Controllers;

use DataTables;
use Carbon\Carbon;
use App\Models\WorkWeek;
use App\Models\Employee;
use App\Models\ShiftType;
use App\Models\ShiftChange;
use App\Models\EmploymentStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class ShiftChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            $query = ShiftChange::join('employees' , 'employees.id' , '=' , 'shift_management.employee_id')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->leftjoin('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
            ->leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
            ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
            ->leftjoin('users' , 'users.id' , '=' , 'shift_management.created_by')
            ->leftjoin('users as users2' , 'users2.id' , '=' , 'shift_management.status_changed_by')
            ->whereIn('employees.department' , login_user_departments())
            ->where('employees.status' , 'Active')
            ->select('shift_management.*', 'employees.employee_id', 'employees.employee_code' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employees.job_title' , 'work_week.desc' , 'shift_type.shift_desc' , 'companystructures.title' , 'jobtitles.name as job_title_name', 'users.username as requested_by', 'users2.username as action_taken_by');
            if (userRoles()=="Self Service") {
                $query->where('shift_management.employee_id' , Auth::user()->employee);
            }
            return DataTables::of($query)->filter(function ($query) use ($request) {
                if(!empty($request->departmentFilter) && $request->departmentFilter!='all'){
                    $query->where('employees.department', $request->departmentFilter);
                }
                if (!empty($request->employeeFilter)) {
                    $query->where('employees.id', $request->employeeFilter);
                }
                if(!empty($request->designationFilter) && $request->designationFilter[0]){
                    $query->whereIn('employees.job_title', $request->designationFilter);
                }
                if(!empty($request->statusFilter)){
                    $query->where('shift_management.status', $request->statusFilter);
                }
                if(!empty($request->shift_filter)){
                    $query->where('shift_management.shift_id', $request->shift_filter);
                }
                if(!empty($request->fromDateFilter) && !empty($request->toDateFilter)){
                    $query->whereBetween('shift_management.shift_from_date', [$request->fromDateFilter , $request->toDateFilter]);
                }
            },true)->addIndexColumn()->make(true);
        }
        $workWeeks = WorkWeek::get();
        $shiftTypes = ShiftType::where('active',1)->get();
        $employment_status = EmploymentStatus::get(); 
        return view('Admin.request&approvals.shift_change.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'work_week_id' => 'required',
                'shift_id' => 'required',
                'shift_from_date' => 'required',
                'shift_to_date' => 'required',
                'remarks' => 'required',
            ]);

            $data = $request->all();
            if($request->status){
                $data['status'] = $request->status;
                $data['status_changed_by'] = Auth::id();
                $data['status_changed_at'] = Carbon::now();
            }else{
                $data['status'] = 'Pending';
            }
            if($request->hasFile('image')){
                $data['image'] = InsertFile($request->file('image'), '/images/shift_change_request/');
            }
            if(env('COMPANY') == 'CLINIX'){
                $data['late_time_allowed'] = 11;
            }else{
                $data['late_time_allowed'] = 15;
            }
            $data['created_by'] = Auth::id();
            if(env('COMPANY') == 'JSML'){
                // Check for duplicate records
                $employeesHaveExistingRecordsQuery = ShiftChange::with(['shiftType', 'employee'])
                ->whereIn('employee_id', $request->employee_id)
                ->where(function($query) use ($request) {
                    $query->where(function($query) use ($request) {
                        $query->where('shift_from_date', '<=', $request->shift_to_date)
                            ->where('shift_to_date', '>=', $request->shift_from_date);
                    })
                    ->orWhere(function($query) use ($request) {
                        $query->where('shift_from_date', '>=', $request->shift_from_date)
                            ->where('shift_to_date', '<=', $request->shift_to_date);
                    });
                })
                ->where('status', '!=', 'Rejected');
                $employeesHaveExistingRecords = $employeesHaveExistingRecordsQuery->distinct('employee_id')->pluck('employee_id')->toArray();
                $employeesHaveExistingRecordsData = $employeesHaveExistingRecordsQuery->get();
                $response = $this->storeForBulkEmployees($data, $employeesHaveExistingRecords);
                return ['code' => 200 , 'message' => 'success', 'records' => $employeesHaveExistingRecordsData, 'response_message' => $response];
            }
            else{
                ShiftChange::create($data);
                return ['code' => 200 , 'message' => 'success'];
            } 
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShiftChange  $shiftChange
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShiftChange  $shiftChange
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ShiftChange::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShiftChange  $shiftChange
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'work_week_id' => 'required',
                'shift_id' => 'required',
                'shift_from_date' => 'required',
                'shift_to_date' => 'required',
                'remarks' => 'required',
            ]);
            $data = $request->all();
            $shift_change = ShiftChange::find($id);
            if($request->input('status')){
                $data['status'] = $request->status;
                $data['status_changed_by'] = Auth::id();
                $data['status_changed_at'] = Carbon::now();
                
            }else{
                $data['status'] = 'Pending';
            }
            $shift_change->fill($data)->save();
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShiftChange  $shiftChange
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $shift_change = ShiftChange::find($id);
            $shift_change->deleted_by = Auth::id();
            $shift_change->update();
            $shift_change->delete();
            return ['code' => 200 , 'message' => 'success'];
        }catch(\Exception $e){
            return ['code' => 500 , 'error_message' => $e->getMessage()];
        }
    }

    public function storeForBulkEmployees($request, $employeesHaveExistingRecords){
        $employees = array_diff(array_map('intval', $request['employee_id']), $employeesHaveExistingRecords);
        unset($request['department']);
        unset($request['sections']);
        unset($request['employment_status']);
        foreach ($employees as $employee) {
            $request['employee_id'] = $employee;
            ShiftChange::create($request);
        }
        return count($employees) > 0 ? 'Shift Change Added For The Employees Successfully!' : 'No Employee Shift Change Added As All have Existing Records';
    }
}
