<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\PerformanceIndicator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PerformanceIndicatorController extends Controller
{ 
    public function __construct(){  
        $this->middleware('auth');
    }

    public function index(Request $request)   
    {
        if($request->ajax())
        {
            return Datatables::eloquent(PerformanceIndicator::query())->make(true); 
        }
        
        return view('Admin.finance.keyPerformanceIndicator.index');    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'indicator' => 'required',
                'max_score' => 'required',
            ]);
            $data = $request->all(); 
            
            PerformanceIndicator::create($data);
            

            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
     $data = PerformanceIndicator::find($id);

     return response()->json($data);     
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
             'indicator' => 'required',
             'max_score' => 'required',
         ]);

            $PerformanceIndicator = PerformanceIndicator::find($id);
            $PerformanceIndicator->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $PerformanceIndicator = PerformanceIndicator::find($id);
            $PerformanceIndicator->deleted_by = Auth::id();
            $PerformanceIndicator->update();
            $PerformanceIndicator->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }catch(\Exception $e){
            return ['code' => 200 , 'error_message' => $e->getMessage()]; 
        }
    }
}