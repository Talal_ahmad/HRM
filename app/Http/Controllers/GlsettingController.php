<?php

namespace App\Http\Controllers;

use App\Models\CompanyStructure;
use App\Models\EmploymentStatus;
use App\Models\GlSetting;
use App\Models\Payroll;
use App\Models\PayrollColumn;
use DataTables;
use Illuminate\Http\Request;

class GlsettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $departments = CompanyStructure::all();
        $payroll_columns = PayrollColumn::all();
        if ($request->ajax()) {
            $data = GlSetting::query();
            if (!empty($request->departmentFilter)) {
                $data->whereRaw("JSON_SEARCH(department_id, 'one', '$request->departmentFilter') IS NOT NULL");
            }
            if (!empty($request->columnFilter)) {
                $data->whereRaw("JSON_SEARCH(payroll_column_id, 'one', '$request->columnFilter') IS NOT NULL");
            }
            if (!empty($request->columnTypeFilter)) {
                $data->where('column_type', $request->columnTypeFilter);
            }

            return DataTables::eloquent($data)
                ->addColumn('employment_statuses', function ($row) {
                    return EmploymentStatus::whereIn('id', $row->employment_status_id)->pluck('name')->implode(', ');
                })
                ->addColumn('departments', function ($row) {
                    return CompanyStructure::whereIn('id', $row->department_id)->pluck('title')->implode(', ');
                })
                ->addColumn('columns', function ($row) {
                    if($row->payroll_column_id){
                        return PayrollColumn::whereIn('id', $row->payroll_column_id)->pluck('name')[0];
                    }
                    else{
                        return '';
                    }
                })
                ->make(true);
        }
        return view('Admin.settings.gl_settings.index', get_defined_vars());
    }

    public function loanAdvanceIndex()
    {
        $settings = GlSetting::where('loan_advance', 1)->get();
        $employment_statuses = EmploymentStatus::all();
        $departments = CompanyStructure::all();
        return view('Admin.settings.gl_settings.loan_advance', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settings = GlSetting::all();
        $employment_statuses = EmploymentStatus::all();
        $payroll_columns = PayrollColumn::all();
        $departments = CompanyStructure::all();
        $banks = getBankAccounts();
        return view('Admin.settings.gl_settings.create', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->settings)) {
            foreach ($request->settings as $setting) {
                $gl_obj = GlSetting::create([
                    'employment_status_id' => json_encode($setting['employment_status']),
                    'department_id' => json_encode($setting['department']),
                    'payroll_column_id' => isset($setting['payroll_column']) ? json_encode($setting['payroll_column']) : null,
                    'column_type' => $setting['column_type'],
                    'category' => $setting['category'],
                    'gl_account_id' => isset($setting['gl_account']) ? json_encode($setting['gl_account']) : null,
                    'gl_accounts' => $setting['gl_accounts'],
                    'bank_accounts_id' => isset($setting['bank_accounts_id']) ? json_encode($setting['bank_accounts_id']) : null,
                    'bank_accounts' => isset($setting['bank_accounts']) ? $setting['bank_accounts'] : null,
                    'type' => $setting['type'],
                ]);
            }
        }
        return redirect('glSettings')->with('message', 'Gl Setting Made Successfully!');
    }

    public function StoreLoanAdvance(Request $request)
    {
        $ids = [];
        if (isset($request->settings)) {
            foreach ($request->settings as $setting) {
                $gl_obj = GlSetting::updateOrCreate([
                    'id' => $setting['setting_id'] ?? 0,
                ], [
                    'employment_status_id' => json_encode($setting['employment_status']),
                    'department_id' => json_encode($setting['department']),
                    'column_type' => $setting['column_type'],
                    'category' => $setting['category'],
                    'gl_account_id' => json_encode($setting['gl_account']),
                    'gl_accounts' => $setting['gl_accounts'],
                    'loan_advance' => 1,
                ]);
                $ids[] = $gl_obj->id;
            }
        }
        GlSetting::whereNotIn('id', $ids)->where('loan_advance', 1)->delete();
        return redirect()->back()->with('message', 'Gl Settings Made Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $setting = GlSetting::find($id);
        $payroll_column = $setting->payroll_column_id[0];
        $employment_statuses = EmploymentStatus::whereIn('id', $setting->employment_status_id)->pluck('name')->implode(', ');
        $payroll_columns = PayrollColumn::where('id', $payroll_column)->pluck('name')[0];
        $departments = CompanyStructure::whereIn('id', $setting->department_id)->pluck('title')->implode(', ');
        $payrolls = Payroll::where('status', 'Completed')->get();
        $amount_found = 0;
        $amount_not_found = 0;
        $employees_found = [];

        if ($request->has('payroll')) {
            $payrollemployees = \DB::select("SELECT employees.id,employees.department as department_id,payrollcolumns.id as column_id,payrolldata.amount,employees.employment_status
            from payroll
            join payrolldata on payrolldata.payroll = payroll.id
            join employees on payrolldata.employee = employees.id
            join jobtitles on employees.job_title = jobtitles.id
            join payrollcolumns on payrolldata.payroll_item = payrollcolumns.id
            where payroll.id  = $request->payroll 
            AND payrollcolumns.id  = $payroll_column
            AND employees.employment_status IN ($setting->employment_status_id_string)
            AND employees.department IN ($setting->department_id_string)");
            foreach ($payrollemployees as $employee) {
                $glsetting = GlSetting::whereRaw("(JSON_SEARCH(employment_status_id, 'one', $employee->employment_status) IS NOT NULL AND JSON_SEARCH(department_id, 'one', $employee->department_id) IS NOT NULL AND JSON_SEARCH(payroll_column_id, 'one', $employee->column_id) IS NOT NULL) AND (`category` = 'payroll_accrual' OR `category` = 'pf_accrual')")->first();

                if (!empty($glsetting)) {
                    $amount_found += $employee->amount;
                } else {
                    $amount_not_found += $employee->amount;
                }
                if(!in_array($employee->id, $employees_found)){
                    array_push($employees_found, $employee->id);
                }
            }
        }
        return view('Admin.settings.gl_settings.show', get_defined_vars());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = GlSetting::find($id);
        if($setting->locked == 1){
            abort(401);
        }
        $employment_statuses = EmploymentStatus::all();
        $payroll_columns = PayrollColumn::all();
        $departments = CompanyStructure::all();
        $banks = getBankAccounts();
        return view('Admin.settings.gl_settings.edit', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $setting = GlSetting::find($id);
        if($setting->locked == 1){
            abort(401);
        }
        $setting->update([
            'employment_status_id' => json_encode($request['employment_status']),
            'department_id' => json_encode($request['department']),
            'payroll_column_id' => isset($request['payroll_column']) ? json_encode($request['payroll_column']) : null,
            'column_type' => $request['column_type'],
            'category' => $request['category'],
            'gl_account_id' => json_encode($request['gl_account']),
            'gl_accounts' => $request['gl_accounts'],
            'bank_accounts_id' => isset($request['bank_accounts_id']) ? json_encode($request['bank_accounts_id']) : null,
            'bank_accounts' => isset($request['bank_accounts']) ? $request['bank_accounts'] : null,
            'type' => $request['type'],
        ]);

        return redirect('glSettings')->with('message', 'Gl Setting Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            GlSetting::find($id)->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
    
    public function lockUnlock(Request $request, $id)
    {
        try{
            GlSetting::find($id)->update([
                'locked' => $request->action
            ]);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
