<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\User;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\DepartmentChange;
use App\Models\DepartmentHistory;
use App\Models\CompanyStructure;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DepartmentChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth'); 
    }
    
    public function index(Request $request)
    {
        if($request->ajax()){
            $query = DepartmentChange::select('department_requests.*' , 'companystructures.title' , 'cs.title AS toTitle' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'users.username AS user' , 'employees.employee_id' , 'employees.employee_code')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'department_requests.dept_from')
            ->leftjoin('companystructures AS cs' , 'cs.id' , '=' , 'department_requests.dept_id')
            ->leftjoin('employees' , 'employees.id' , '=' , 'department_requests.employee_id' )
            ->leftjoin('users' , 'users.id' , '=' , 'department_requests.submitted_by')
            ->whereIn('employees.department', login_user_departments())
            ->where('employees.status', 'Active');
            if (userRoles()=="Self Service") {
                $query->where('department_requests.employee_id' , Auth::user()->employee);
            }
            return DataTables::of($query)->filter(function ($query) use ($request) {
                if (!empty($request->employeeFilter)) {
                    $query->where('employees.id' , $request->employeeFilter);
                }
            },true)->addIndexColumn()->make(true);
        }
        $departments = CompanyStructure::get(['id', 'title']);

        return view('Admin.request&approvals.department_change.index',get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'dept_from' => 'required',
                'employee_id' => 'required',
                'dept_id' => 'required',
                'remarks' => 'required',
            ]);
            $department_dup = DepartmentChange::where('employee_id', $request->employee_id)->orderBy('id', 'desc')->whereDate('affective_date', $request->effected_date)->first();
            if(isset($department_dup) && $department_dup->status=='Approved'){
                return ['code' => 300, 'message' => 'The Request of Department Change for this employee is already exist!'];
            }elseif (isset($department_dup) && $department_dup->status=='Pending') {
                return ['code' => 300, 'message' => 'The Request of Department Change for this employee is already exist!'];
            }else{
                if(isset($request->status) && !empty($request->status)){ 
                    $status = 'Approved';
                    $employee = Employee::where('id' , $request->employee_id)->first();
                    $employee->department = $request->dept_id;
                    $employee->update();
    
                    $prev_depart_history = DepartmentHistory::whereNull('to_date')->where('employee_id', $request->employee_id)->orderByDesc('id')->first();
                    if(!empty($prev_depart_history))
                    {
                        $prev_depart_history->to_date = date('Y-m-d', strtotime(date('Y-m-d') .' -1 day'));
                        $prev_depart_history->update();
                    }
                    $department_history = new DepartmentHistory();
                    $department_history->employee_id = $request->employee_id;
                    $department_history->from_date = date('Y-m-d');
                    $department_history->department = $request->dept_id;
                    $department_history->save();
                }
                else{
                    $status = 'Pending';
                }
                $parent = CompanyStructure::where('id', $request->dept_id)->first();
                $changeDepartment = new DepartmentChange();
                $changeDepartment->employee_id = $request->employee_id;
                $changeDepartment->dept_from = $request->dept_from;
                $changeDepartment->dept_id = $request->dept_id;
                $changeDepartment->parent_id = !empty($parent) ? $parent->parent : NULL;
                $changeDepartment->remarks = $request->remarks;
                $changeDepartment->status = $status;
                $changeDepartment->affective_date = $request->effected_date;
                if($request->status=='Approved'){ 
                    $changeDepartment->status_changed_by = Auth::id();     
                    $changeDepartment->status_changed_at = Carbon::now(); 
                }
                if($request->hasFile('attachment')){
                    $file = $request->file('attachment');
                    $extension = $file->getClientOriginalExtension();
                    $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                    $file->move('images/department_request/' , $temp_name);
                    $changeDepartment->attachment = $temp_name;
                }
                $changeDepartment->submitted_by = Auth::id();        
                $changeDepartment->save();
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DepartmentChange  $departmentChange
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request , $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DepartmentChange  $departmentChange
     * @return \Illuminate\Http\Response
     */
    public function edit(DepartmentChange $departmentChange)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DepartmentChange  $departmentChange
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DepartmentChange $departmentChange)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DepartmentChange  $departmentChange
     * @return \Illuminate\Http\Response
     */
    public function destroy(DepartmentChange $departmentChange)
    {
        //
    }
}
