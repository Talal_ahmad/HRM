<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\InsuranceType;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class InsuranceTypeController extends Controller
{ 
    public function __construct(){  
        $this->middleware('auth');
    }

    public function index(Request $request)   
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @rownum=0'));
            $data = InsuranceType::select(['id', 'type','description',DB::raw('@rownum  := @rownum  + 1 AS rownum')]);
            return Datatables::eloquent($data)->make(true); 
        }
        return view('Admin.finance.insuranceTypes.index');    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'type' => 'required',
            ]);

            $data = $request->all(); 
            InsuranceType::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        $data = InsuranceType::find($id);
        return response()->json($data);     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'type' => 'required',
            ]);

            $InsuranceType = InsuranceType::find($id);
            $InsuranceType->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $InsuranceType = InsuranceType::find($id);
            $InsuranceType->deleted_by = Auth::id();
            $InsuranceType->update();
            $InsuranceType->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception $e){
            return ['code' => 500 , 'error_message' => $e->getMessage()]; 
        }
    }
}