<?php
namespace App\Http\Controllers;

use Auth;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\EmployeeDisciplinaryAction;
use Illuminate\Validation\ValidationException;

class EmployeeDisciplinaryActionController extends Controller
{ 
    public function __construct(){ 
        $this->middleware('auth');
    } 

    public function index(Request $request)   
    {
        if($request->ajax())
        {
            $query = EmployeeDisciplinaryAction::leftjoin('employees' , 'employees.id' , '=' , 'disciplinary_inquiry.employee_id')
            ->select('disciplinary_inquiry.*' , 'employees.employee_id' , 'employees.employee_code' ,'employees.first_name' ,'employees.middle_name', 'employees.last_name');
            return DataTables::of($query)->make(true); 
        }

        return view('Admin.employees.employee_disciplinary_actions.index', get_defined_vars());    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'title' => 'required',
                'employee_id' => 'required',
                'start_date' => 'required',
            ]);
            $data = $request->all();
            if($request->hasFile('attachment1')){
                $data['attachment1'] = EmployeeDisciplinaryAction::petitionInsert($request->attachment1);
            }
            if($request->hasFile('employee_statement')){
                $data['employee_statement'] = EmployeeDisciplinaryAction::statementInsert($request->employee_statement);
            }
            $action = new EmployeeDisciplinaryAction();
            $action->create($data)->save();
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
       $data = EmployeeDisciplinaryAction::find($id);
       return response()->json($data);     
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'title' => 'required',
                'employee_id' => 'required',
                'start_date' => 'required',
           ]);
            $data = $request->all();
            $action = EmployeeDisciplinaryAction::find($id);
            if($request->hasFile('attachment1')){
                $data['attachment1'] = EmployeeDisciplinaryAction::petitionUpdate($action->attachment1 , $request->attachment1);
            }
            if($request->hasFile('employee_statement')){
                $data['employee_statement'] = EmployeeDisciplinaryAction::statementUpdate($action->employee_statement , $request->employee_statement);
            }
            $action->fill($data)->save();
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $action = EmployeeDisciplinaryAction::find($id);
            $action->deleted_by = Auth::id();
            $action->update();
            $action->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }catch(\Exception $e){
            return ['code' => 200 , 'error_message' => $e->getMessage()]; 
        }
    }
}