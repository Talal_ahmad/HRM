<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CurrencyType;
use DataTables;

class CurrencyTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::eloquent(CurrencyType::query())->addIndexColumn()->make(true);
        }
        return view('Admin.setup.currencyType.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'code' => 'required',
                'name' => 'required',
            ]);

            $data = $request->all();
            CurrencyType::create($data);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CurrencyType $currency_type)
    {
        return response()->json($currency_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CurrencyType $currency_type)
    {
        try {
            $this->validate($request, [
                'code' => 'required',
                'name' => 'required',
            ]);

            $data = $request->all();
            $currency_type->update($data);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CurrencyType $currency_type)
    {
        try {
            $currency_type->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
