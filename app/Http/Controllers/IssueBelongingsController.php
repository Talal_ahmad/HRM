<?php

namespace App\Http\Controllers;
use DataTables;
use Auth;
use Illuminate\Http\Request;
use App\Models\IssueBelonging;
use App\Models\Employee;
use App\Models\CompanyStructure;
use App\Models\CompanyBelonging;
class IssueBelongingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = IssueBelonging::leftjoin('employees' , 'employees.id' , '=' , 'issue_belongings.mul_employees')
            ->leftjoin('companybelongings' , 'companybelongings.id' , '=' , 'issue_belongings.company_belonging_id');
            if(!empty($request->departmentFilter)){
                $data = $data->where('departments',$request->departmentFilter);
            }
            if(!empty($request->employeeFilter)){
                $data = $data->where('mul_employees',$request->employeeFilter);
            }
            if(!empty($request->assetFilter)){
                $data = $data->where('company_belonging_id',$request->assetFilter);
            }
            if(!empty($request->serialFilter)){
                $data = $data->where('company_belonging_id',$request->serialFilter);
            }
            $data->select('companybelongings.name','companybelongings.serial_number','issue_belongings.id', 'departments', 'department_title' , 'mul_employees', 'bel_conditions', 'sp_instruction', 'attachs', 'remarks', 'issue_date','employees.first_name','employees.middle_name','employees.last_name','return_date');
            return Datatables::eloquent($data)->addIndexColumn()->make(true);
        }
        $issue_b = Employee::whereIn('employees.department', login_user_departments())->get();
        $belonging = CompanyBelonging::get(['id', 'name', 'serial_number']);
        return view('Admin.finance.companyBelongings.issueBelongings.index' , get_defined_vars());  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'departments' => 'required',
                'mul_employees' => 'required',
                'issue_date' => 'required',
            ]);
            $issue_b = IssueBelonging::where('company_belonging_id',$request->company_belonging_id)->whereNull('status_return')->count();
            $belonging = CompanyBelonging::where('id',$request->company_belonging_id)->first();
            // dd($issue_b < $belonging->b_limit);
            if($issue_b < $belonging->b_limit){
                $departments = CompanyStructure::where('id',$request->departments)->first();
                $issue = new IssueBelonging();
                $issue->assign_by = Auth::id();
                $issue->departments = $request->departments;
                $issue->department_title = $departments->title;
                $issue->mul_employees = $request->mul_employees;
                $issue->company_belonging_id = $request->company_belonging_id;
                // $issue->mul_employees = implode(',',$request->mul_employees);
                $issue->bel_conditions = $request->condition;
                $issue->sp_instruction = $request->sp_instruction;
                $issue->remarks = $request->remarks;
                $issue->issue_date = $request->issue_date;
                $issue->return_date = $request->return_date;
                if($request->hasFile('attachs')){
                    $file = $request->file('attachs');
                    $extension = $file->getClientOriginalExtension();
                    $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                    $file->move('images/belongings/' , $temp_name);
                    $issue->attachs = $temp_name;
                }
                $issue->save();
                return ['code' => 202 , 'message' => 'Success'];
            }
            return ['code' => 200 , 'message' => 'err'];
            
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $CompanyBelonging = IssueBelonging::find($id);
        return response()->json($CompanyBelonging);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request , [
                // 'departments' => 'required',
                'mul_employees' => 'required',
                'issue_date' => 'required',
            ]);
                // $issue_b = IssueBelonging::where('company_belonging_id',$request->company_belonging_id)->whereNull('status_return')->count();
                // $belonging = CompanyBelonging::where('id',$request->company_belonging_id)->first();
                $emp_departments = Employee::where('id',$request->mul_employees)->first('department');
                $departments = CompanyStructure::where('id',$emp_departments->department)->first();
                $issue = IssueBelonging::find($id);
                $issue->assign_by = Auth::id();
                $issue->departments = $emp_departments->department;
                $issue->department_title = $departments->title;
                $issue->mul_employees = $request->mul_employees;
                $issue->company_belonging_id = $request->company_belonging_id;
                $issue->bel_conditions = $request->condition;
                $issue->sp_instruction = $request->sp_instruction;
                $issue->remarks = $request->remarks;
                $issue->return_date = $request->return_date;
                $issue->issue_date = $request->issue_date;
            $issue->update();
            if($request->hasFile('attachs')){
                // unlink('images/belongings/'.$issue->attachs);
                $file = $request->file('attachs');
                $allowedExtensions = ['jpg', 'jpeg', 'png', 'gif', 'pdf']; 

                $extension = $file->getClientOriginalExtension();
                if (!in_array($extension, $allowedExtensions)) {
                    return response()->json(['code' => 400, 'message' => 'Only JPG, JPEG, PNG, GIF, and PDF files are allowed.']);
                }

                $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                $file->move('images/belongings/' , $temp_name);
                $issue->attachs = $temp_name;
            }
            $issue->save();
            return ['code' => 200 , 'message' => 'Success'];

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $company_belonging = IssueBelonging::find($id);
            if($company_belonging->attachs){
                unlink('images/belongings/'.$company_belonging->attachs);
            }
            // $company_belonging->deleted_by = Auth::user()->id;
            $company_belonging->update();

            $company_belonging->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message' => $e->getMessage()];
        }
    }
    
    public function returning_belonging(Request $request,$id)
    {
        try{
            $this->validate($request , [
                // 'company_name' => 'required',
            ]);
            $issue = IssueBelonging::find($id);
            $issue->confirm_return_by = Auth::id();
            $issue->person_order = $request->person_order;
            $issue->status_return = $request->status_return;
            $issue->condition_return = $request->condition_return;
            $issue->remarks_return = $request->remarks_return;
            $issue->save();
            return ['code' => 200 , 'message' => 'Success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }
    // public function return_belonging(Request $request,$id)
    // {
    //     try{
    //         $this->validate($request , [
    //             // 'company_name' => 'required',
    //         ]);
    //         $issue = IssueBelonging::find($id);
    //         $issue->return_date = $request->return_date;
    //         $issue->save();
    //         return ['code' => 200 , 'message' => 'Success'];
    //     }
    //     catch(\Exception | ValidationException $e){
    //         if($e instanceof ValidationException){
    //             return ['code'=>'422','errors' => $e->errors()];
    //         }
    //         else{
    //             return ['code'=>'500','error_message'=>$e->getMessage()];
    //         }
    //     }
    // }
}
