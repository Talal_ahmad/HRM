<?php

namespace App\Http\Controllers;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\EmployeePayment;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EmployeePaymentsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = EmployeePayment::leftjoin('employees','employee_payments.employee_id','=','employees.id')
            ->leftjoin('payment_codes','employee_payments.payment_code','=','payment_codes.id')
            ->where('employees.status', 'Active')
            ->whereIn('employees.department', login_user_departments())
            ->select('employee_payments.id','employee_payments.amount','employee_payments.payment_type','employee_payments.amount_month','employee_payments.amount_year','employee_payments.billing_period_from','employee_payments.billing_period_to','payment_codes.description', 'employees.first_name','employees.last_name', 'employees.employee_code','employees.employee_id');
            return Datatables::eloquent($data)->addIndexColumn()->make(true);
        }
        $payment_codes = DB::table('payment_codes')->get(); 
        return view('Admin.finance.employeePayments.index', compact('payment_codes'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'amount' => 'required',
                'payment_type' => 'required',
                'payment_code' => 'required',
                'amount_month' => 'required',
                'amount_year' => 'required',
                'billing_period_from' => 'required',
                'billing_period_to' => 'required',
            ]);

            $data = $request->all(); 
            EmployeePayment::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $EmployeePayment = EmployeePayment::find($id);
        return response()->json($EmployeePayment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'amount' => 'required',
                'payment_type' => 'required',
                'payment_code' => 'required',
                'amount_month' => 'required',
                'amount_year' => 'required',
                'billing_period_from' => 'required',
                'billing_period_to' => 'required',
            ]);

            $EmployeePayment = EmployeePayment::find($id);
            $EmployeePayment->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            EmployeePayment::find($id)->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
