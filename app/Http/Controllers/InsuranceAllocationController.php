<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\InsuranceAllocation;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class InsuranceAllocationController extends Controller
{ 
    public function __construct(){  
        $this->middleware('auth');
    }

    public function index(Request $request)   
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @rownum=0'));
            $data = InsuranceAllocation::leftjoin('employees','insurance_allocation.employee_id','=','employees.id')->leftjoin('insurance_types','insurance_allocation.type','=','insurance_types.id')->leftjoin('insurance_sub_types','insurance_allocation.sub_type','=','insurance_sub_types.id')->select('insurance_allocation.id','insurance_allocation.policy_number','insurance_allocation.start_date','insurance_allocation.end_date','insurance_allocation.amount','insurance_types.type','insurance_sub_types.sub_type','employees.employee_id','employees.employee_code','employees.middle_name','employees.first_name','employees.last_name',DB::raw('@rownum  := @rownum  + 1 AS rownum'));
            return DataTables::eloquent($data)->make(true); 
        }
        
        $employees=DB::table('employees')->select('employees.*')->selectRaw('CONCAT(employees.first_name,employees.last_name) as "employee"')->get();
        $insurance_types=DB::table('insurance_types')->get();
        $insurance_sub_types=DB::table('insurance_sub_types')->get();
        return view('Admin.finance.insurance.index', get_defined_vars());    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'type' => 'required',
                'sub_type' => 'required',
                'policy_number' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'amount' => 'required',
            ]);
            $data = $request->all(); 
            if(!empty($data['attachment'])){
                $data['attachment'] = InsuranceAllocation::Attachment($data['attachment']);
            }
            InsuranceAllocation::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
       $data = InsuranceAllocation::find($id);

       return response()->json($data);     
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'type' => 'required',
                'sub_type' => 'required',
                'policy_number' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'amount' => 'required',
            ]);

            $data = $request->all();
            $InsuranceAllocation = InsuranceAllocation::find($id);
            if(!empty($data['attachment'])){
                if(!empty($InsuranceAllocation->attachment)){
                    unlink(public_path().$InsuranceAllocation->attachment);
                }
                $data['attachment'] = InsuranceAllocation::Attachment($data['attachment']);
            }
            $InsuranceAllocation->fill($data)->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $InsuranceAllocation = InsuranceAllocation::find($id);
            $InsuranceAllocation->deleted_by = Auth::id();
            $InsuranceAllocation->update();
            $InsuranceAllocation->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }
        catch(\Exception $e){
            return ['code' => 500 , 'error_message' => $e->getMessage()]; 
        }
    }
}