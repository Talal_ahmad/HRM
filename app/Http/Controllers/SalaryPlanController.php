<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class SalaryPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        $salary_components = DB::table('salarycomponent')->get(['id','name']);
        $packages = DB::table('salary_plains')->get();
        
        foreach($packages as $key => $package){
            foreach ($salary_components as $component) {
                $package_comp = DB::table('salary_plains_setup')->where('salary_plains_id' , $package->id)->where('component_id', $component->id)->first();
                $data[$package->id][$component->id] = !empty($package_comp) ? $package_comp->amount : 0.00;
            }
        }
        return view('Admin.finance.salary_plans.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $package = DB::table('salary_plains')->where('id' , $id)->first(['id' , 'name']);
        $components = DB::table('salary_plains_setup')->leftjoin('salarycomponent' , 'salarycomponent.id' , '=' , 'salary_plains_setup.component_id')
        ->where('salary_plains_setup.salary_plains_id' , $id)
        ->select('salarycomponent.id AS component_id' , 'salarycomponent.name AS component_name' , 'salary_plains_setup.amount')
        ->get();
        return view('Admin.finance.salary_plans.edit' , get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::table('salary_plains')->where('id' , $id)->update(['name' => $request->package_name]);
            foreach($request->component as $key => $comp){
                DB::table('salary_plains_setup')->where('salary_plains_id' , $id)->where('component_id' , $key)->update(['amount' => $comp]);
            }
            return redirect('/salary_plan');
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::table('salary_plains')->where('id' , $id)->delete();
            DB::table('salary_plains_setup')->where('salary_plains_id' , $id)->delete();
            return redirect('/salary_plan');
        }catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
