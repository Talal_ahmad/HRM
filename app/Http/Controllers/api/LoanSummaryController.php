<?php

namespace App\Http\Controllers\api;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Models\LoanRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class LoanSummaryController extends Controller
{
    public function __construct(){ 
        $this->middleware('auth:sanctum');
    }

    public function loanSummary(Request $request){

        $range = CarbonPeriod::create($request->date_from , '1 month' , $request->date_to);

        foreach($range as $data){
            $list_of_dates[] = $data->format('Y-m');
        }
        foreach($list_of_dates as $key => $date){
            
            $opening_balance = LoanRequest::join('loan_installments' , 'loan_installments.loan_request_id' , '=' , 'loan_requests.id')
            ->join('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
            ->whereYear('loan_requests.start_date' , date('Y' , strtotime($date)))
            ->whereMonth('loan_requests.start_date' , date('m' , strtotime($date)))
            ->where('loan_requests.status' , 'Approved')
            ->where('loan_installments.status' , 'active')
            ->orWhere(function($query){
                $query->where('loan_installments.status' , 'deducted')
                ->where('loan_installments.system_deducted' , 1);
            })
            ->sum('loan_installments.amount');
            $loan_summary[$key]['opening_balance'] = $opening_balance;

            $new_loan = LoanRequest::select(DB::raw('SUM(loan_requests.amount) AS new_loan') , DB::raw('SUM(loan_requests.pending) AS pending'))
            ->join('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
            ->whereYear('loan_requests.start_date' , date('Y' , strtotime($date)))
            ->whereMonth('loan_requests.start_date' , date('m' , strtotime($date)))
            ->where('loan_requests.status' , 'Approved')
            ->groupBy('loan_requests.status');
            $loan_summary[$key]['new_loan'] = $new_loan->new_loan + $new_loan->pending;
        }
        dd($new_loan);
    }
}
