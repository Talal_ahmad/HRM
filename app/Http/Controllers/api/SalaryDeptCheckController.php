<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SalaryDeptCheckController extends Controller
{
    public function __construct(){ 
        $this->middleware('auth:sanctum');
        
    }

    public function salary_dept_check(){
        if(!empty(request()->department_id) && !empty(request()->user_id)){
            DB::table('dept_attendence_confirm')->insert([
                'submitted_by' => request()->user_id,
                'dept_id' => request()->department_id,
                'salary_month' => request()->date,
            ]);
            return response(['message' => 'Success' , 'code' => 200]);
        }
        else{
            $query = DB::table('dept_attendence_confirm')->select(DB::raw('DATE(dept_attendence_confirm.created_at) AS created_at') , 'users.username')
            ->join('users' , 'users.id' , '=' , 'dept_attendence_confirm.submitted_by')
            ->where([
                ['dept_attendence_confirm.dept_id' , request()->department_id],
                ['dept_attendence_confirm.salary_month' , request()->date]
            ])->get();
            if(count($query) > 0){
                return response([   
                    'message' => 'Exsist' , 'code' => 200 , 'data' => $query
                ]);
            }
            else{
                return response(['message' => 'Not Exsist' , 'code' => 404]);
            }
        }
    }

    public function dept_salary_check(){
        $payroll = DB::table('payroll')->where([
            ['department' , request()->department],
            ['status' , 'Completed'],
            ['date_start' , '>=' , request()->start_date],
            ['date_end' , '<=' , request()->end_date]
        ])->get(['id']);

        if(count($payroll) > 0){
            return response(['message' => 'Success' , 'code' => 200 , 'data' => $payroll]);
        }
        else{
            return response(['message' => 'Failed' , 'code' => 404]);
        }
    }
}
