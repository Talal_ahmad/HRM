<?php

namespace App\Http\Controllers\api;

use Auth;
use Illuminate\Http\Request;
use App\Models\ShiftManagement;
use App\Models\ShiftType;
use App\Models\Employee;
use App\Models\JobTitle;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class ShiftChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){ 
        $this->middleware('auth:sanctum');
    }

    public function index()
    {
        $shift_types = ShiftType::where('active',1)->get(['id' , 'shift_desc']);
        $work_weeks = DB::table('work_week')->get(['id' , 'desc']);
        $designation = JobTitle::get(['id' , 'name']);
        return response(['code' => 200, 'status' => 'success','data' => ['shift_types' => $shift_types,'work_weeks' => $work_weeks ,'designation' => $designation]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'work_week_id' => 'required',
                'shift_id' => 'required',
                'shift_from_date' => 'required',
                'shift_to_date' => 'required',
                'remarks' => 'required',
            ]);

            $data = $request->all();
            if(isset($request->approved_admin) && $request->approved_admin == 'true'){
                $data['status'] = 'Approved';
            }
            else{
                $data['status'] = 'Pending';
            }
            if(env('COMPANY') == 'CLINIX'){
                $data['late_time_allowed'] = 11;
            }else{
                $data['late_time_allowed'] = 15;
            }
            unset($data['approved_admin']);
            $data['created_by'] = Auth::id();
            ShiftManagement::create($data);
            return response(['code' => 200,'status' => "success",'message' => "Shift Request Submitted Successfully!"]);

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure", 'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function get_employees_shift(Request $request)
    {
        $employees = Employee::where('status' , 'Active')->where('employees.id' , $request->emp_id)->select('employees.id' , 'employees.employee_id','employees.job_title','employees.employee_code', 'employees.first_name' , 'employees.last_name','employees.department')->get();

        if (env('COMPANY') == 'CLINIX') {
            $shift_type  = [];
            foreach ($employees as $employee) {
                $employeeId = $employee->id;
                $employeeJobTitle = $employee->job_title;
                $shift_type = ($employeeJobTitle == 39)
                    ? ShiftType::where('active', 1)
                    ->whereRaw('(TIMESTAMPDIFF(HOUR, shift_start_time, shift_end_time) >= 12 
                                 OR (TIMEDIFF(shift_end_time, shift_start_time) < 0
                                     AND (24 + HOUR(shift_end_time) - HOUR(shift_start_time)) >= 12))')
                    ->get(['id','shift_desc'])                
                    : ShiftType::where('active', 1)
                    ->where(function ($query) {
                        $query->whereRaw('IF(shift_start_time <= shift_end_time, 
                                            TIMESTAMPDIFF(HOUR, shift_start_time, shift_end_time), 
                                            TIMESTAMPDIFF(HOUR, shift_start_time, shift_end_time) + 24) BETWEEN 0 AND 10')
                            ->orWhere(function ($q) {
                                $q->whereRaw('TIME(shift_start_time) >= "14:00:00"')
                                    ->whereRaw('TIME(shift_end_time) = "00:00:00"');
                            });
                    })->get(['id','shift_desc']);
            }
        }else{
            $shift_type = ShiftType::where('active', 1)->get();
        }
        return response(['code' => 200, 'status' => 'success','data' => ['shift_types' => $shift_type]]);

    }
}
