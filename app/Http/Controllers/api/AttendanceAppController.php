<?php

namespace App\Http\Controllers\Api;

use DB;
use Carbon\Carbon;
use App\Models\Employee;
use App\Models\Attendance;
use App\Models\ShiftManagement;
use App\Models\MachineManagement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AttendanceAppController extends Controller
{
    public function SearchUser(Request $request)
    {
        if($request->searchType == 0){
            $employees = Employee::where('first_name', 'like', '%' . $request->name . '%')->orWhere('last_name','LIKE','%'.$request->name.'%')->get(['id','employee_id','employee_code','first_name','last_name']);
        }
        else{
            $employees = Employee::where('employee_id', '=',$request->name)->get(['id','employee_id','employee_code','first_name','last_name']);
        }
        return ['code' => '200' , 'status' => 'Success','result' => $employees];
    }

    public function GetEmployeesList(Request $request)
    {
        $employees = Employee::leftJoin('companystructures','companystructures.id','=','employees.department')->where('status', 'Active');
        if(isset($request->department_id) && !empty($request->department_id)){
            $employees->where('employees.department',$request->department_id);
        }
        $employees = $employees->get(['employees.id','employees.employee_id','employees.employee_code','employees.first_name','employees.last_name','employees.image','employees.passcode','employees.nic_num','employees.department','employees.thumb_print1','employees.thumb_print2','employees.thumb_print3','employees.thumb_print4','employees.thumb_print5','employees.thumb_print6','employees.width','employees.height','employees.format','companystructures.title']);
        return ['code' => '200' , 'status' => 'Success','result' => $employees];
    }

    public function RegisterUser(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required'
            ]);
            $employee = Employee::where('employee_id',$request->employee_id)->first();
            if(!empty($employee)){
                if (isset($request->thumb_print4)) {
                    $employee->thumb_print4 = $request->thumb_print4;
                    $employee->thumb_print5 = $request->thumb_print5;
                    $employee->thumb_print6 = $request->thumb_print6;
                    $employee->format = $request->format;
                    $employee->width = $request->width;
                    $employee->height = $request->height;
                    $employee->update();
                }
                else {
                    $employee->thumb_print1 = $request->thumb_print1;
                    $employee->thumb_print2 = $request->thumb_print2;
                    $employee->thumb_print3 = $request->thumb_print3;
                    $employee->update();
                }
                return ['code' => '200' , 'status' => 'Success','message' => 'Record updated successfully!'];
            }
            return ['code' => '404' , 'status' => 'failure','message' => 'No Record Found!'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => '422','status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => '500','status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function GetStatus(Request $request)
    {
        try{
            $this->validate($request , [
                'department_id' => 'required',
                'device_id' => 'required'
            ]);
            DB::table('get_status')->insert([
                'department_id' => $request->department_id,
                'device_id' => $request->device_id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            return ['code' => 200 , 'status' => 'Success','message' => 'Record created successfully!'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function MachineDetails(Request $request)
    {
        $machine = MachineManagement::where('machine_sr_no',$request->machine_serial)->first();
        return ['code' => '200' , 'status' => 'Success','result' => $machine];
    }

    public function CheckInApp(Request $request)
    {
        $employee = null;
        try {
            DB::table('attendance_form_data')->insert([
                'employee_id' => $request['id'],
                'form_data' => json_encode($request->all()),
                'attendance_type' => 1,
            ]);
            $is_manual = 0;
            if (isset($request['is_manual'])) {
                $is_manual = $request['is_manual'];
            }
            $employee = Employee::where('id', $request['id'])->first(['id', 'department']);
            if (!empty($employee)) {
                $employee_department = $employee->department;
                $is_online = 0;
                $is_online = $request['is_online'];
                $timestamp = $request['checkin_ts'];
                $date = date('Y-m-d', strtotime($request['checkin_ts']));
                $allow_checkin = DB::table('allow_checkin')->first();
                if ($allow_checkin->allow == '1') {
                    $check_emp = Attendance::where('employee', $request['id'])->whereDate('in_time', $date)->orderBy('id', 'DESC')->first();
                    if (!empty($check_emp)) {
                        // if (!empty($check_emp->out_time)) {
                            return response()->json(['code' => '401', 'status' => 'failure', 'message' => 'Already Checked In!']);
                        // }
                    }
                }
                $shift_data = ShiftManagement::join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
                    ->where([
                        ['shift_management.employee_id', $request['id']],
                        ['shift_management.shift_from_date', '<=', $date],
                        ['shift_management.shift_to_date', '>=', $date],
                    ])
                    ->select('shift_management.id', 'shift_management.work_week_id', 'shift_management.shift_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_management.late_time_allowed')
                    ->orderBy('shift_management.id', 'DESC')
                    ->first();
                if (!empty($shift_data)) {
                    $shift_id = $shift_data->shift_id;
                    $shift_start_time = $shift_data->shift_start_time;
                    $shift_end_time = $shift_data->shift_end_time;
                    $late_time_allowed = $shift_data->late_time_allowed;
                    $work_week_id = $shift_data->work_week_id;
                } else {
                    $shift_id = 1;
                    $shift_start_time = '09:00:00';
                    $shift_end_time = '17:00:00';
                    $late_time_allowed = 11;
                    $work_week_id = 1;
                }
                // $thumb_matched = 'Yes';
                // if (isset($request['NotMatched']) && empty($request['NotMatched'])) {
                //     $thumb_matched = 'No';
                // }
                $checkAttendance = Attendance::where('employee', $request['id'])->whereDate('in_time', $date)->orderBy('id', 'DESC')->first();
                if (empty($checkAttendance)) {
                    $attendance = new Attendance;
                    $attendance->employee = $request['id'];
                    $attendance->in_time = $timestamp;
                    $attendance->note = 0;
                    $attendance->shift_id = $shift_id;
                    $attendance->shift_start_time = $shift_start_time;
                    $attendance->shift_end_time = $shift_end_time;
                    $attendance->late_time_allowed = $late_time_allowed;
                    $attendance->work_week_id = $work_week_id;
                    $attendance->is_manual = $is_manual;
                    $attendance->department_id = $employee_department;
                    $attendance->is_online = $is_online;
                    $attendance->thumb_matched_insert = 'Yes';
                    $attendance->save();
                }
                return response()->json(['code' => '200', 'status' => 'success', 'message' => 'CheckIn successfully!']);
            } else {
                \Log::debug('checkIn not found exception-', [$request['id']]);
                return response()->json(['code' => '404', 'status' => 'failure', 'message' => 'Employee not found!']);
            }
        } catch (\Exception $e) {
            \Log::error('checkIn exception-', [$e, $employee, $request->all()]);
            return response()->json(['code' => '500', 'status' => 'failure', 'message' => $e->getMessage()]);
        }
    }

    public function CheckOutApp(Request $request)
    {
        try {
            DB::table('attendance_form_data')->insert([
                'employee_id' => $request['id'],
                'form_data' => json_encode($request->all()),
                'attendance_type' => 2,
            ]);
            $is_online = $request['is_online'];
            $is_manual = 0;
            if (isset($request['is_manual'])) {
                $is_manual = $request['is_manual'];
            }
            $timestamp = $request['checkout_ts'];
            $date = date('Y-m-d', strtotime($request['checkout_ts']));
            $previous_date = date("Y-m-d", strtotime("-1 day", strtotime($date)));
            $check_employee_attend = Attendance::where('employee', $request['id'])->whereDate('in_time', $date)->first(['id','in_time','out_time']);
            $thumb_matched = 'Yes';
            if (isset($request['NotMatched']) && !empty($request['NotMatched'])) {
                $thumb_matched = 'No';
            }
            if (!empty($check_employee_attend) && empty($check_employee_attend->out_time)) {
                $checkOut = Attendance::find($check_employee_attend->id);
                $checkOut->out_time = $timestamp;
                $checkOut->thumb_matched_checkout = 'Yes';
                $checkOut->update();
                return response()->json(['code' => '200', 'status' => 'success', 'message' => 'Checkout successfully!']);
            }
            $check_emp_pre = Attendance::where('employee', $request['id'])->whereDate('in_time', $previous_date)->whereNull('out_time')->first(['id']);
            if (!empty($check_emp_pre)) {
                $prevCheckOut = Attendance::find($check_emp_pre->id);
                $prevCheckOut->out_time = $timestamp;
                $prevCheckOut->thumb_matched_checkout = 'Yes';
                $prevCheckOut->update();
                return response()->json(['code' => '200', 'status' => 'success', 'message' => 'Checkout successfully!']);
            }
            if(!empty($check_employee_attend) && !empty($check_employee_attend->out_time)){
                return response()->json(['code' => '401', 'status' => 'failure', 'message' => 'Already Checked Out!']);
            }
            else{
                return response()->json(['code' => '401', 'status' => 'failure', 'message' => 'Not Checked In!']);
            }
        } catch (\Exception $e) {
            \Log::error('checkout exception-', [$e, $request->all()]);
            return response()->json(['code' => '500', 'status' => 'failure', 'message' => $e->getMessage()]);
        }
    }
}
