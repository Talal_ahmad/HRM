<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Attendance;
use App\Models\Employee;
use App\Models\Holidays;
use App\Models\ShiftManagement;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function checkIn(Request $request)
    {
        $checkInDateFormatted = date('Y-m-d', strtotime($request['checkin_ts']));
        $today = date('Y-m-d');

        if ($checkInDateFormatted > $today) {
            \Log::error('Check-in date cannot be greater than today-', [$request->all()]);
            return response()->json(['message' => 'Check-in date cannot be greater than today'], 422);
        }

        $employee = null;
        try {
            DB::table('attendance_form_data')->insert([
                'employee_id' => $request['id'],
                'form_data' => json_encode($request->all()),
                'attendance_type' => 1,
            ]);
            $is_manual = 0;
            if (isset($request['is_manual'])) {
                $is_manual = $request['is_manual'];
            }
            $employee = Employee::where('id', $request['id'])->first(['id', 'department']);
            if (!empty($employee)) {
                $employee_department = $employee->department;
                $is_online = $request['is_online'];
                if ($is_online == 0) {
                    $timestamp = $request['checkin_ts'];
                    $date = date('Y-m-d', strtotime($request['checkin_ts']));
                }
                $allow_checkin = DB::table('allow_checkin')->first();
                if ($allow_checkin && $allow_checkin->allow == '1') {
                    $check_emp = Attendance::where('employee', $request['id'])->where('in_time', $timestamp)->orderBy('id', 'DESC')->first();
                    if (!empty($check_emp)) {
                        if (!empty($check_emp->out_time)) {
                            return response()->json(['code' => '401', 'status' => 'failure', 'message' => 'Already Checked In!']);
                        }
                    }
                }
                $c_shift_rotating_employee = false;
                $shift_data = ShiftManagement::join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
                    ->where([
                        ['shift_management.employee_id', $request['id']],
                        ['shift_management.shift_from_date', '<=', $date],
                        ['shift_management.shift_to_date', '>=', $date],
                        ['shift_management.status', '=', 'Approved'],
                    ])
                    ->select('shift_management.id', 'shift_management.work_week_id', 'shift_management.shift_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_management.late_time_allowed')
                    ->orderBy('shift_management.id', 'DESC')
                    ->first();
                if (!empty($shift_data)) {
                    $shift_id = $shift_data->shift_id;
                    $shift_start_time = $shift_data->shift_start_time;
                    $shift_end_time = $shift_data->shift_end_time;
                    $late_time_allowed = $shift_data->late_time_allowed;
                    $work_week_id = $shift_data->work_week_id;
                } else {
                    $shift_id = 1;
                    $shift_start_time = '09:00:00';
                    $shift_end_time = '17:00:00';
                    $late_time_allowed = 11;
                    $work_week_id = 1;
                }
                $thumb_matched = 'Yes';
                if (isset($request['NotMatched']) && empty($request['NotMatched'])) {
                    $thumb_matched = 'No';
                }
                if(env('COMPANY') == 'UNICORN'){
                    $unicornDoubleAttendanceShifts = getUnicornDoubleAttendanceShifts()->pluck('id')->toArray();
                    if(in_array($shift_data->shift_id, $unicornDoubleAttendanceShifts)){
                        $checkAttendance = false;
                    }
                }
                else{
                    $checkAttendance = Attendance::where('employee', $request['id'])->whereDate('in_time', $date)->orderBy('id', 'DESC')->first();
                }
                // checking for C shift
                $checkInDateTime = Carbon::parse($request['checkin_ts']);
                $checkinTs = $checkInDateTime->format('H:i:s');
                // Define the start and end times for the allowed range
                $startTime = Carbon::parse('23:00:00')->format('H:i:s'); // 11 PM
                $endTime = Carbon::parse('00:15:00')->format('H:i:s'); // 12:15 AM
                // For today because time is < 23:59:59
                if ($checkinTs >= $startTime && $checkinTs <= '23:59:59') {
                    $checkCshiftAttendance = Attendance::where('employee', $request['id'])
                        ->where(function ($query) use ($checkInDateTime) {
                            $query->where(function ($subQuery) use ($checkInDateTime) {
                                $subQuery->where('in_time', '>=', $checkInDateTime->setTime(23, 0, 0)->format('Y-m-d H:i:s'))->where('in_time', '<=', $checkInDateTime->setTime(23, 59, 59)->format('Y-m-d H:i:s'));
                            })->orWhere(function ($subQuery) use ($checkInDateTime) {
                                $subQuery->where('in_time', '>=', $checkInDateTime->addDay()->setTime(0, 0, 0)->format('Y-m-d H:i:s'))->where('in_time', '<=', $checkInDateTime->setTime(0, 15, 0)->format('Y-m-d H:i:s'));
                            });
                        })
                        ->first();
                }
                // For next day because time is now > 00:00:00
                if ($checkinTs >= '00:00:00' && $checkinTs <= '00:15:00') {
                    $checkCshiftAttendance = Attendance::where('employee', $request['id'])
                        ->where(function ($query) use ($checkInDateTime) {
                            $query->where(function ($subQuery) use ($checkInDateTime) {
                                $subQuery->where('in_time', '>=', $checkInDateTime->setTime(0, 0, 0)->format('Y-m-d H:i:s'))->where('in_time', '<=', $checkInDateTime->setTime(0, 15, 0)->format('Y-m-d H:i:s'));
                            })->orWhere(function ($subQuery) use ($checkInDateTime) {
                                $subQuery->where('in_time', '>=', $checkInDateTime->subDay()->setTime(23, 0, 0)->format('Y-m-d H:i:s'))->where('in_time', '<=', $checkInDateTime->setTime(23, 59, 59)->format('Y-m-d H:i:s'));
                            });
                        })
                        ->first();
                    // because next day $checkAttendance will be empty but its the c shift
                    $checkAttendance = true;
                }
                if ((($checkinTs >= $startTime && $checkinTs <= '23:59:59') || ($checkinTs >= '00:00:00' && $checkinTs <= $endTime)) && empty($checkCshiftAttendance)) {
                    $c_shift_rotating_employee = true;
                } // Static Check of C shift For Now
                if (empty($checkAttendance) || $c_shift_rotating_employee) {
                    $this->incrementCplLeaveBalance($employee, $request['checkin_ts']);
                    $attendance = new Attendance;
                    $attendance->employee = $request['id'];
                    $attendance->in_time = $timestamp;
                    $attendance->note = 0;
                    $attendance->shift_id = $shift_id;
                    $attendance->shift_start_time = $shift_start_time;
                    $attendance->shift_end_time = $shift_end_time;
                    $attendance->late_time_allowed = $late_time_allowed;
                    $attendance->work_week_id = $work_week_id;
                    $attendance->is_manual = $is_manual;
                    $attendance->department_id = $employee_department;
                    $attendance->is_online = $is_online;
                    $attendance->thumb_matched_insert = $thumb_matched;
                    $attendance->save();
                }
                return response()->json(['code' => '200', 'status' => 'success', 'message' => 'CheckIn successfully!']);
            } else {
                \Log::debug('checkIn not found exception-', [$request['id']]);
                return response()->json(['code' => '404', 'status' => 'failure', 'message' => 'Employee not found!']);
            }
        } catch (\Exception $e) {
            \Log::error('checkIn exception-', [$e, $employee, $request->all()]);
            return response()->json(['code' => '500', 'status' => 'failure', 'message' => $e->getMessage()]);
        }
    }

    public function checkOut(Request $request)
    {
        $checkOutDateFormatted = date('Y-m-d', strtotime($request['checkout_ts']));
        $today = date('Y-m-d');

        if ($checkOutDateFormatted > $today) {
            \Log::error('Check-out date cannot be greater than today-', [$request->all()]);
            return response()->json(['message' => 'Check-out date cannot be greater than today'], 422);
        }

        try {
            DB::table('attendance_form_data')->insert([
                'employee_id' => $request['id'],
                'form_data' => json_encode($request->all()),
                'attendance_type' => 2,
            ]);
            $is_online = $request['is_online'];
            $is_manual = 0;
            if (isset($request['is_manual'])) {
                $is_manual = $request['is_manual'];
            }
            if ($is_online == 0) {
                $timestamp = $request['checkout_ts'];
                $date = date('Y-m-d', strtotime($request['checkout_ts']));
            }
            $previous_date = date("Y-m-d", strtotime("-1 day", strtotime($date)));
            $check_employee = Attendance::where('employee', $request['id'])->whereDate('in_time', $date)->whereNull('out_time')->first(['id']);
            $thumb_matched = ',thumb_matched_checkout=Yes';
            if (isset($request['NotMatched']) && !empty($request['NotMatched'])) {
                $thumb_matched = ',thumb_matched_checkout=No';
            }
            if (!empty($check_employee)) {
                $checkOut = Attendance::find($check_employee->id);
                $checkOut->out_time = $timestamp;
                $checkOut->thumb_matched_checkout = $thumb_matched;
                $checkOut->update();
                return response()->json(['code' => '200', 'status' => 'success', 'message' => 'Checkout successfully!']);
            }
            // for C Shift in JSML
            if(env('COMPANY') == 'JSML'){
                $employeeShift = getEmployeeShift($request['id'], $previous_date);
                if(!empty($employeeShift) && $employeeShift?->shift_id == 6){
                    $check_emp_pre = Attendance::where('employee', $request['id'])->whereDate('in_time', $previous_date)->whereNull('out_time')->first(['id']);
                    if (!empty($check_emp_pre)) {
                        $prevCheckOut = Attendance::find($check_emp_pre->id);
                        $prevCheckOut->out_time = $timestamp;
                        $prevCheckOut->thumb_matched_checkout = $thumb_matched;
                        $prevCheckOut->update();
                        return response()->json(['code' => '200', 'status' => 'success', 'message' => 'Checkout successfully!']);
                    }
                }
            }
            return response()->json(['code' => '402', 'status' => 'failure', 'message' => 'Not Checked In!']);
        } catch (\Exception $e) {
            \Log::error('checkout exception-', [$e, $request->all()]);
            return response()->json(['code' => '500', 'status' => 'failure', 'message' => $e->getMessage()]);
        }
    }

    public function incrementCplLeaveBalance($employee, $inTime)
    {
        if (env('COMPANY') == 'JSML' && $employee->employment_status != 9) {
            $shiftManagement = ShiftManagement::with('workWeek')->where('employee_id', $employee->id)->orderBy('id', 'DESC')->first();
            $checkInDateTime = Carbon::parse($inTime);
            $checkinTs = $checkInDateTime->format('H:i:s');
            $currentDate = $checkInDateTime;

            // checking for C shift
            // For today because time is < 23:59:59
            if ($checkinTs >= '23:00:00' && $checkinTs <= '23:59:59') {
                $currentDate = $checkInDateTime->addDay();
            }

            // Checking Off Day
            $off_day = $shiftManagement->workWeek->days()->where('name', $currentDate->dayName)->where('status', 'Non-working Day')->count();
            // Checking Holiday
            $holiday = Holidays::where('dateh', $currentDate->format('Y-m-d'))->count();

            if ($off_day || $holiday) {
                $employee->leaveTypeBalances()->updateOrCreate([
                    'leave_type' => 4,
                ], [
                    'leave_type' => 4,
                    'balance' => DB::raw('IFNULL(balance,0) + 1'),
                ]);
            }
        }
        return true;
    }
}
