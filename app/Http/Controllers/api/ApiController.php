<?php

namespace App\Http\Controllers\api;

use DB;
use PDF;
use Auth;
use Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ShiftManagement;
use App\Models\MachineManagement;
use App\Models\LeavePeriods;
use App\Models\PayrollData;
use App\Models\Employee;
use App\Models\EmployeeLeaveRequest;
use App\Models\OverTimeManagement;
use App\Models\LoanRequest;
use App\Models\LoanType;
use App\Models\LeaveTypes;
use App\Models\ShortLeaveRequest;
use App\Models\DepartmentChange;
use App\Models\MiscellaneousRequest;
use App\Models\CompanyStructure;
use App\Models\DepartmentHistory;
use App\Models\SalaryComponent;
use App\Models\EmployeeSalary;
use App\Models\CalculationGroup;
use App\Models\EnvChecks;
use App\Models\Payroll;
use App\Models\PayrollColumn;
use App\Models\ManualAttendanceRequest;
use Illuminate\Validation\ValidationException;

class ApiController extends Controller
{
    public function __construct(){   
        $this->middleware('auth:sanctum')->except('updateLeavePeriod','updatePayrollData','getDepartments', 'getEmployees','markAttendance');
    }

    public function getDepartments(){
        $departments = CompanyStructure::get(['id','title', 'parent']);
        return response(['code' => 200, 'status' => 'success','data' => ['departments' => $departments]]);
    }

    public function attendance_check_handle(){
        $attendance_long_lat_button = EnvChecks::join('env_name', 'env_name.id', '=', 'meta_data.name')->where('env_name.name','Attendance Long Lat Button')->where('key',1)->first();
        // dd($attendance_long_lat_button);
        return response(['code' => 200, 'status' => 'success','data' => ['attendance_check_handle' => $attendance_long_lat_button]]);
    }

    public function getEmployees(){
        $months = DB::table('month_manual_attend_permission')->where('department' , request()->department_id)->first(['month']);
        $months = !empty($months) ? $months->month : "[]";
        $employees = Employee::select('id','employee_id','employee_code',DB::raw("CONCAT(ifnull(first_name,''),' ',ifnull(last_name,'')) as name"),'department as department_id','parent as department_parent_id')
        ->where([
            'department' => request()->department_id,
            'status' => 'Active'
        ])->get();

        return response(['code' => 200, 'status' => 'success','data' => ['employees' => $employees,'month_code' => $months]]);
    }

    public function getEmployeeDetails(){
        $employee = Employee::Join('companystructures', 'companystructures.id', '=', 'employees.department')
        ->Join('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
        ->Join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
        ->where('employees.id', request()->employee_id)->first(['employees.id', 'employees.employee_id' , 'employees.employee_code', 'employees.first_name', 'employees.last_name', 'employees.middle_name','employees.father_name','companystructures.title as department','employmentstatus.name as employment_status','jobtitles.name as job_title']);
        if(!empty($employee)){
            return response(['code' => 200, 'status' => 'success','data' => ['employee' => $employee]]);
        }
        return response(['code' => 404, 'status' => 'failed','message' => 'Employee not found!']);
    }
    
    public function markAttendance(Request $request)
    {
        $date = date('Y-m-d');
        $time = date('H:i:s');
        $department = Employee::where('id' , $request->employee_id)->first(['department']);
        if(!empty($request->nearest_department)){
            $machine_dpt = MachineManagement::where('machine_department' , $request->nearest_department)->first(['latitude','longitude','id']);
        }else{
            $machine_dpt = MachineManagement::where('machine_department' , $department->department)->first(['latitude','longitude','id']);
        }
        if(!empty($machine_dpt)){
            $lat_long_app_in = degreesToMeters($request->lat_in,$request->long_in);
            $lat_long_app_out = degreesToMeters($request->lat_out,$request->long_out);
            $lat_long_machine = degreesToMeters($machine_dpt->latitude,$machine_dpt->longitude);
            $distance_in = calculateDistance($lat_long_app_in['latitude_meters'], $lat_long_app_in['longitude_meters'], $lat_long_machine['latitude_meters'], $lat_long_machine['longitude_meters']);
            $distance_out = calculateDistance($lat_long_app_out['latitude_meters'], $lat_long_app_out['longitude_meters'], $lat_long_machine['latitude_meters'], $lat_long_machine['longitude_meters']);
        }else{
            return response(['code' => 500,'status' => 'faliure','message' => 'Machine Department is missing!']);
        }

        // Checking Shift
        $shift = ShiftManagement::select('shift_management.*' , 'shift_type.shift_start_time' , 'shift_type.shift_end_time')
        ->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
        ->where([
            ['shift_management.employee_id' , $request->employee_id],
            ['shift_management.shift_from_date' , '<=' , $date],
            ['shift_management.shift_to_date' , '>=' , $date],
            ['shift_management.status' , '=' , 'Approved'],
        ])->orderBy('shift_management.id', 'desc')->first();

        if(!empty($shift)){
            $shift_id = $shift->shift_id;
            $shift_start_time = $shift->shift_start_time;
            $shift_end_time = $shift->shift_end_time;
            $work_week_id = $shift->work_week_id;
            if($shift->late_time_allowed==Null){
                $late_time_allowed = 0;
            }else{
                $late_time_allowed = $shift->late_time_allowed;
            }
        }
        else{
            $shift_id = 1;
            $shift_start_time = '09:00:00';
            $shift_end_time = '17:00:00';
            $work_week_id = 1;
            $late_time_allowed = 12;
        }
        $status = $request->status == 'true' ? 'Approved' : 'Pending';
        $mac_address = DB::table('machine_details')->where('mac_address',$request->mac_addr)->first();
        if($request->attendance_type == 'check_in'){
            if(env('COMPANY') == 'UNICORN'){
                $unicornDoubleAttendanceShifts = getUnicornDoubleAttendanceShifts()->pluck('id')->toArray();
            }
            else{
                $unicornDoubleAttendanceShifts = []; 
            }
            $attendance = DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)
                ->where(DB::raw('DATE(in_time)') , $date)->first();

            if(!empty($attendance)){
                if(env('COMPANY') == 'UNICORN' && in_array($shift_id, $unicornDoubleAttendanceShifts)){
                    // do nothing
                }
                else{
                    DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)
                    ->where(DB::raw('DATE(in_time)') , $date)->delete();
                }
            }
            DB::table('manual_attendance_request')->insert([
                'employee_id' => $request->employee_id,
                'in_time' => $date. ' ' .$time,
                'department_id' => $department->department,
                'shift_id' => $shift_id,
                'shift_start_time' => $shift_start_time,
                'shift_end_time' => $shift_end_time,
                'work_week_id' => $work_week_id,
                'late_time_allowed' => $late_time_allowed,
                'note' => $request->remarks,
                'status' => $status,
                'submitted_by' => $request->submitted_by
            ]);
            if($status == 'Approved'){
                $attendance = DB::table('attendance')->where('employee', $request->employee_id)->where(DB::raw('DATE(in_time)'), $request->in_date)->first();
                $sameAttendanceInAnHourFlag = false;
                if(!empty($attendance)){
                    if(env('COMPANY') == 'UNICORN' && in_array($shift_id, $unicornDoubleAttendanceShifts)){
                        $currentHour = date('H');
                        $sameAttendanceInAnHourFlag = DB::table('attendance')
                        ->where('employee', $request->employee_id)
                        ->where(DB::raw('DATE(in_time)'), $date)
                        ->whereRaw("HOUR(in_time) = ?", [$currentHour])
                        ->exists();
                    }
                    else{
                        DB::table('attendance')->where('employee', $request->employee_id)->where(DB::raw('DATE(in_time)'), $request->in_date)->delete();
                    }
                }

                if(!$sameAttendanceInAnHourFlag){
                    DB::table('attendance')->insert([
                        'employee' => $request->employee_id,
                        'in_time' => $date. ' ' .$time,
                        'department_id' => $department->department,
                        'is_online' => 1,
                        'is_manual' => 0,
                        'shift_id' => $shift_id,
                        'shift_start_time' => $shift_start_time,
                        'shift_end_time' => $shift_end_time,
                        'work_week_id' => $work_week_id,
                        'late_time_allowed' => $late_time_allowed,
                        'distance_in' => $distance_in,
                        'mac' => $request->mac_addr,
                        'note' => $request->remarks,
                    ]);
                }
            }
        }
        elseif($request->attendance_type == 'check_out'){
            $attendance = DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)->where(DB::raw('DATE(in_time)'), $date)->whereNull('out_time')->first();
            if(!empty($attendance)){
                DB::table('manual_attendance_request')->where('id', $attendance->id)->update([
                    'out_time' => $date. ' ' .$time,
                    'status' => $status,
                ]);
            }
            else{
                $prevAttendance = DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)
                ->where(DB::raw('DATE(in_time)') , date ("Y-m-d", strtotime("-1 day", strtotime($date))))->whereNull('out_time')->first();

                if(!empty($prevAttendance)){
                    DB::table('manual_attendance_request')->where('id', $prevAttendance->id)->update([
                        'out_time' => $date. ' ' .$time,
                        'status' => $status,
                        'submitted_by' => $request->submitted_by
                    ]);
                }
            }
            $attendance = DB::table('attendance')->where('employee', $request->employee_id)->where(DB::raw('DATE(in_time)'),date('Y-m-d', strtotime($date)))->whereNull('out_time')->latest()->first();
            if(!empty($attendance)){
                if($status == 'Approved'){
                    DB::table('attendance')->where('id', $attendance->id)->update([
                        'out_time' => $date. ' ' .$time,
                        'distance_out' => $distance_out
                    ]);
                }
            }
            else{
                $prevAttendance = DB::table('attendance')->where('employee' , $request->employee_id)
                ->where(DB::raw('DATE(in_time)') , date ("Y-m-d", strtotime("-1 day", strtotime($date))))->whereNull('out_time')
                ->latest()
                ->first();

                if(!empty($prevAttendance)){
                    if($status == 'Approved'){
                        DB::table('attendance')->where('id' , $prevAttendance->id)->update([
                            'out_time' => $date. ' ' .$time,
                        ]);
                    }
                }
                else{
                    return response(['code' => 500,'status' => 'faliure','message' => 'Employee not Checked In!']);
                }
            }
        }
        return response(['code' => 200,'status' => 'success','message' => 'Employee Attendance Has been added Sucessfuly']);
    }

    public function shortLeaveRequest(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'date' => 'required|date|after_or_equal:'. date('Y-m-d'),
                'time_from' => 'required',
                'time_to' => 'required',
                'remarks' => 'required',
            ]);
            $short_dup = ShortLeaveRequest::where('employee_id', $request->employee_id)->orderBy('id', 'desc')->whereDate('date', $request->date)->first();
            if(isset($short_dup) && $short_dup->status=='Approved'){
                return ['code' => 500,'status' => "failure",'message' => 'The Short Leave for this employee is already exist!'];

            }elseif (isset($short_dup) && $short_dup->status=='Pending') {
                return ['code' => 500,'status' => "failure",'message' => 'The Short Leave for this employee is already exist!'];
            }else{
                $data = $request->all();
                $short_leave = new ShortLeaveRequest();
                if(!empty($data['image'])){
                    $data['image'] = InsertBase64Image($data['image'],'/images/short_leave_request/');
                }
                $data['status'] = $request->status == 'true' ? 'Approved' : 'Pending';
                $data['submitted_by'] = Auth::id();
                $short_leave->create($data)->save();
                return response(['code' => 200,'status' => "success",'message' => "Short Leave Request Submitted Successfully!"]);
            }
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function getLeaveTypes(){
        $leaveTypes = LeaveTypes::get(['id','name']);
        return response(['code' => 200, 'status' => 'success','data' => ['leaveTypes' => $leaveTypes]]);
    }

    public function leaveRequest(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'leave_type_id' => 'required',
                'from_date' => 'required|date',
                'to_date' => 'required|date|after_or_equal:from_date',
                'remarks' => 'required', 
            ]);
            $data = $request->all();
            $leavePeriod = LeavePeriods::where([
                ['status','=','Active'],
                ['date_start','<=',$request->from_date],
                ['date_end','>=',$request->to_date],
            ])->first(['id']);
            if(empty($leavePeriod)){
                return ['code'=>'500','message'=>'Date does not match with leave period!'];
            }
            $shift = ShiftManagement::where('employee_id' , $request->employee_id)->orderBy('id' , 'DESC')->first(['shift_id']);
            if($request->hasFile('image')){
                $data['image'] = InsertBase64Image($data['image'],'/images/leave_request/');
            }
            if(isset($request->approved_admin) && $request->approved_admin == 'true'){
                $status = 'Approved';
            }
            else{
                $status = 'Pending';
            }
            $leave_dup = EmployeeLeaverequest::where('employee', $request->employee_id)->orderBy('id', 'desc')->whereDate('date_start', $request->from_date)->first();
            if(isset($leave_dup) && $leave_dup->status=='Approved'){
                return ['code' => 500, 'message' => 'The Leave Request for this employee is already exist!'];
            }elseif (isset($leave_dup) && $leave_dup->status=='Pending') {
                return ['code' => 500, 'message' => 'The Leave Request for this employee is already exist!'];
            }else{
                $leave_request = new EmployeeLeaveRequest();
                $leave_request->employee = $request->employee_id;
                $leave_request->leave_type = $request->leave_type_id;
                $leave_request->leave_period = $leavePeriod->id;
                $leave_request->date_start = $request->from_date;
                $leave_request->date_end = $request->to_date;
                $leave_request->submitted_by = $request->submitted_by;
                $leave_request->status = $status;
                $leave_request->remarks = $request->remarks;
                $leave_request->image = isset($image) ? $image : NULL;
                $leave_request->shift_id = !empty($shift) ? $shift->shift_id : 1;
                $leave_request->save();
                return response(['code' => 200,'status' => "success",'message' => "Leave Request Submitted Successfully!"]);
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function holiday_check(Request $request)
    {
        $shift = ShiftManagement::select('shift_management.id', 'shift_management.work_week_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'work_week.desc as work_week_name', 'shift_management.shift_id')
        ->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
        ->join('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
        ->where([
            ['shift_management.employee_id' , $request->employee_id],
            ['shift_management.shift_from_date' , '<=' , $request->overtime_date],
            ['shift_management.shift_to_date' , '>=' , $request->overtime_date],
            ])
        ->orderBy('shift_management.id', 'DESC')->first();
        
        if(!empty($shift)){
            $attendance = DB::table('attendance')->where([
                ['employee' , $request->employee_id],
                [DB::raw('DATE(in_time)') , $request->overtime_date]
            ])->first();
            if(empty($attendance)){
                $leave = DB::table('employeeleaves')->where([
                    ['employee' , $request->employee_id],
                    ['date_start' , '<=' , $request->overtime_date],
                    ['date_end' , '>=' , $request->overtime_date],
                    ['status' , 'Approved']
                ])->first();
                if(!empty($leave)){
                    $is_leave = 'Yes';
                }
            }

            $work_week_days = DB::table('workdays')->where([
                ['work_week_id' , $shift->work_week_id],
                ['status' , 'Non-working Day']
            ])->get();
            if(count($work_week_days) > 0){
                $requested_day = date('l' , strtotime($request->overtime_date));
                foreach($work_week_days as $current_day){
                    if($current_day->name == $requested_day){
                        $is_holiday = 'Off Day';
                    }
                }
            }

            $holiday = DB::table('holidays')->where('dateh' , $request->overtime_date)->first();
            if(!empty($holiday)){
                if(!empty($is_holiday)){
                    $is_holiday = $holiday->name . 'and' . $is_holiday;
                }
                else{
                    $is_holiday = $holiday->name;
                }
            }

            $data['is_holiday'] = isset($is_holiday) ? $is_holiday : '-';
            $data['in_time'] = !empty($attendance) ? $attendance->in_time : '-';
            $data['out_time'] = !empty($attendance) ? $attendance->out_time : '-';
            $data['leave'] = isset($is_leave) ? $is_leave : 'No';
            $data['shift_name'] = $shift->shift_desc;
            $data['work_week'] = $shift->work_week_name;
            $data['shift_start_time'] = $shift->shift_start_time;
            $data['shift_end_time'] = $shift->shift_end_time;
            return response(['code' => 200,'status' => 'success','data' => $data]);
        }
        else{
            return response(['code' => 500,'status' => 'faliure','message' => 'No Shift assign on this date, Please assign Shift first!']);
        }
    }

    public function overtimeRequest(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'overtime_date' => 'required',
                'overtime_start_time_stamp' => 'required',
                'overtime_end_time_stamp' => 'required|after:overtime_start_time_stamp',
                'remarks' => 'required',
            ]);

            $shift = ShiftManagement::select('shift_management.id', 'shift_management.work_week_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_management.shift_id')
            ->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
            ->where([
                ['shift_management.employee_id' , $request->employee_id],
                ['shift_management.shift_from_date' , '<=' , $request->overtime_date],
                ['shift_management.shift_to_date' , '>=' , $request->overtime_date],
            ])
            ->orderBy('shift_management.id', 'DESC')->first();
            $start_datetime = $request->overtime_start_time_stamp;
            $end_datetime = $request->overtime_end_time_stamp;
            $is_holiday = '';
            if(!empty($shift)){
                $work_week_days = DB::table('workdays')->where([
                    ['work_week_id' , $shift->work_week_id],
                    ['status' , 'Non-working Day']
                ])->get();
                $current_day_name = date('l' , strtotime($request->overtime_date));
                foreach($work_week_days as $work_week_day){
                    if($work_week_day->name == $current_day_name){
                        $is_holiday = 'Off Day';
                    }
                }

                $holiday = DB::table('holidays')->where('dateh' , $request->overtime_date)->first();
                if(!empty($holiday)){
                    if(!empty($is_holiday)){
                        $is_holiday = $holiday->name . 'and' . 'Off Day';
                    }
                    else{
                        $is_holiday = $holiday->name;
                    }
                }

                $overtime_date = new Carbon($request->overtime_date);
                $shift_start_time = $request->overtime_date. ' ' .$shift->shift_start_time;
                $shift_end_time = $request->overtime_date. ' ' .$shift->shift_end_time;
                if($shift_end_time < $shift_start_time){
                    $overtime_date = $overtime_date->addDays(1);
                    $overtime_date = $overtime_date->format('Y-m-d');
                }
                $shift_end_time = $overtime_date. ' ' .$shift->shift_end_time;

                // Advance Approval
                if($overtime_date > date('Y-m-d')){
                    if(empty($is_holiday)){
                        if(($start_datetime > $shift_start_time && $end_datetime < $shift_end_time) || ($start_datetime < $shift_end_time && $shift_end_time > $shift_end_time) || ($start_datetime < $shift_start_time && $end_datetime > $shift_start_time)){
                            return response(['code' => 500,'status' => 'faliure','message' => "Selected Overtime is not allowed, Because this time is in between Employee's Shift Time"]);
                        }
                    }
                }

                // Checking Employee's attendance && leave
                if(empty($is_holiday)){
                    $attendance = DB::table('attendance')->where([
                        ['employee' , $request->employee_id],
                        [DB::raw('DATE(in_time)') , $overtime_date]
                    ])->first();

                    if(empty($attendance)){
                        $leave = DB::table('employeeleaves')->where([
                            ['employee' , $request->employee_id],
                            ['date_start' , '<=' , $overtime_date],
                            ['date_end' , '>=' , $overtime_date],
                            ['status' , 'Approved'],
                            ])->first();
                            if(!empty($leave)){
                            $is_leave = 'Yes';
                            } 
                    }
                    if(empty($attendance) && isset($is_leave)){
                        return response(['code' => 500,'status' => 'faliure','message' => 'Overtime is not allowed, Because employee is on leave']);
                    }
                    if(empty($attendance) && !isset($is_leave)){
                        return response(['code' => 500,'status' => 'faliure','message' => 'Overtime is not allowed, Because employee is absent']);
                    }
                }

                // Post Approval
                if($overtime_date < date('Y-m-d')){
                    if(empty($is_holiday)){
                        if(($start_datetime > $shift_start_time && $end_datetime < $shift_end_time) || ($start_datetime < $shift_end_time && $end_datetime > $shift_end_time) || ($start_datetime < $shift_start_time && $end_datetime > $shift_start_time)){
                            return response(['code' => 500,'status' => 'faliure','message' => "Selected Overtime is not allowed, Because this time is in between Employee's Shift Time"]);
                        }
                    }

                    if(isset($attendance) && !empty($attendance->out_time)){
                        if($attendance->out_time < $end_datetime){
                            return response(['code' => 500,'status' => 'faliure','message' => 'Selected Overtime can not be greater then your checkout time']);
                        }
                    }
                    else{
                        return response(['code' => 500,'status' => 'faliure','message' => 'You cannot request overtime unless you checked out']);
                    }    
                }

                // Current Approval
                if($overtime_date == date('Y-m-d')){
                    if(empty($is_holiday)){
                        if(($start_datetime > $shift_start_time && $end_datetime < $shift_end_time) || ($start_datetime < $shift_end_time && $end_datetime > $shift_end_time) || ($start_datetime < $shift_start_time && $end_datetime > $shift_start_time)){
                            return response(['code' => 500,'status' => 'faliure','message' => "Selected Overtime is not allowed, Because this time is in between Employee's Shift Time"]);
                        }
                    }
                }

            }
            else{
                return response(['code' => 500,'status' => 'faliure','message' => 'No Shift assigned to this Employee, Please assign Shift first']);
            }

            // Calculating Hours
            $start_overtime = date_create($start_datetime);
            $end_overtime = date_create($end_datetime);
            $diff = date_diff($start_overtime, $end_overtime);
            $days = $diff->d;
            $sec1 = $days * 24 * 3600;
            $hours = $diff->h;
            $sec2 = $hours * 3600;
            $minutes = $diff->i;
            $sec3 = $minutes * 60;
            $total_sec = $sec1 + $sec2 + $sec3;
            $overtime_hours = $total_sec / 3600;

            if($overtime_hours > 48){
                return response(['code' => 500,'status' => 'faliure','message' => "Overtime can not be greater then 48 hours"]);
            }
            $overtime_dup = OverTimeManagement::where('employee_id', $request->employee_id)->orderBy('id', 'desc')->whereDate('overtime_date', $request->overtime_date)->first();
            if(isset($overtime_dup) && $overtime_dup->status=='Approved'){
                return ['code' => 500,'status' => 'faliure', 'message' => 'The overtime for this employee is already exist!'];
            }elseif (isset($overtime_dup) && $overtime_dup->status=='Pending') {
                return ['code' => 500,'status' => 'faliure', 'message' => 'The overtime for this employee is already exist!'];
            }else{
                $overtime = new OverTimeManagement();
                $overtime->employee_id = $request->employee_id;
                $overtime->shift_id = $shift->shift_id;
                $overtime->work_week_id = $shift->work_week_id;
                $overtime->overtime_start_time_stamp = $start_datetime;
                $overtime->overtime_end_time_stamp = $end_datetime;
                $overtime->overtime_date = $request->overtime_date;
                $overtime->overtime = $overtime_hours;
                if(isset($request->approved_admin) && $request->approved_admin == 'true'){
                    $overtime->approved = 1;
                    $overtime->status = 'Approved';
                }
                $overtime->remarks = $request->remarks;
                $overtime->submitted_by = Auth::id();
                $overtime->save();
                return response(['code' => 200,'status' => "success",'message' => "Overtime Request Submitted Successfully!"]);
            }

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function departmentChange(Request $request)
    {
        try {
            $this->validate($request, [
                'employee_id' => 'required',
                'department_from' => 'required',
                'department_to' => 'required',
                'remarks' => 'required',
            ]);
            $DepartmentRequest = new DepartmentChange();
            $DepartmentRequest->employee_id = $request->employee_id;
            $DepartmentRequest->dept_from = $request->department_from;
            $DepartmentRequest->dept_id = $request->department_to;
            $DepartmentRequest->remarks = $request->remarks;
            if (isset($request->approved_admin) && $request->approved_admin == "true") {
                $status = 'Approved';
                $DepartmentsHistory = DepartmentHistory::where('employee_id', $request->employee_id)->orderBy('id', 'DESC')->first();
                if (!empty($DepartmentsHistory)) {
                    $to_date = date('Y-m-d', time() - 60 * 60 * 24);
                    $DepartmentsHistory->update([
                        'to_date' => $to_date
                    ]);
                }
                $DepartmentHistory = new DepartmentHistory();
                $DepartmentHistory->employee_id = $request->employee_id;
                $DepartmentHistory->department = $request->department_to;
                $DepartmentHistory->from_date = date('Y-m-d');
                $DepartmentHistory->save();
                $employee = Employee::find($request->employee_id);
                $employee->department = $request->department_to;
                $employee->update();
            } else {
                $status = 'Pending';
            }
            $DepartmentRequest->status = $status;
            $DepartmentRequest->submitted_by = Auth::id();
            $DepartmentRequest->save();
            return response(['code' => 200,'status' => "success",'message' => "Department Change Request Submitted Successfully!"]);
        } catch (\Exception | ValidationException $e) {
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function getLoanTypes(){
        $loanTypes = LoanType::get(['id','name']);
        return response(['code' => 200, 'status' => 'success','data' => ['loanTypes' => $loanTypes]]);
    }

    public function loanRequest(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'loan_type' => 'required',
                'start_date' => 'required',
                'amount' => 'required',
                'monthly_installment' => 'required',
                'time_duration' => 'required',
                'remarks' => 'required',
            ]);

            $data = $request->all();
            $status = $request->status == 'true' ? 'Approved' : 'Pending';
            $data['monthly_installment'] = $request->amount/$request->time_duration;
            $data['remaining_amount'] = $request->amount;
            $data['status'] = $status;
            $data['submitted_by'] = Auth::id();
            $data['loan_no'] = LoanRequest::max('id');
            if(!empty($data['image'])){
                $data['image'] = InsertBase64Image($data['image'],'/images/loan_requests/');
            }
            $loan_request = LoanRequest::create($data);

            if($status == 'Approved'){
                $start_date = new Carbon($request->start_date);
                for ($i=0; $i < $request->time_duration; $i++) { 
                    $checkInstallment = DB::table('loan_installments')->where('loan_request_id', $loan['id'])->whereYear('month', $start_date->year)->whereMonth('month', $start_date->format('m'))->first(['id']);
                    if(empty($checkInstallment)){
                        DB::table('loan_installments')->insert([
                            'loan_request_id' => $loan_request['id'],
                            'month' => $start_date,
                            'amount' => $data['monthly_installment'],
                            'status' => 'active'
                        ]);
                    }
                    $start_date = $start_date->addMonths(1);
                }
            }
            return response(['code' => 200,'status' => "success",'message' => "Loan Request Submitted Successfully!"]);
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function getMiscellaneousRequestType()
    {
        $types = DB::table('miscellaneous_request_type')->get(['id', 'name']);
        return response(['code' => 200, 'message' => 'success', 'types' => $types]);
    }

    public function miscellaneousRequest(Request $request)
    {
        try{
            $this->validate($request , [
                'employee_id' => 'required',
                'description' => 'required',
                // 'request_type' => 'required'
            ]);
            $data = $request->all();
            $miscellaneous = new MiscellaneousRequest();
            if($request->hasFile('image')){
                $data['image'] = InsertBase64Image($data['image'],'/images/miscellaneous_request/');
            }
            $data['status'] = $request->status == 'true' ? 'Approved' : 'Pending';
            $data['submitted_by'] = Auth::id();
            $miscellaneous->create($data)->save();
            return response(['code' => 200,'status' => "success",'message' => "Miscellaneous Request Submitted Successfully!"]);          

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function ManualAttendance(Request $request){
        $department = Employee::where('id' , $request->employee_id)->first(['department']);
        // Checking Shift
        $shift = ShiftManagement::select('shift_management.*' , 'shift_type.shift_start_time' , 'shift_type.shift_end_time')
        ->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
        ->where([
            ['shift_management.employee_id' , $request->employee_id],
            ['shift_management.shift_from_date' , '<=' , $request->in_date],
            ['shift_management.shift_to_date' , '>=' , $request->in_date],
        ])->orderBy('shift_management.id', 'desc')->first();

        if(!empty($shift)){
            $shift_id = $shift->shift_id;
            $shift_start_time = $shift->shift_start_time;
            $shift_end_time = $shift->shift_end_time;
            $work_week_id = $shift->work_week_id;
            $late_time_allowed = $shift->late_time_allowed;
        }
        else{
            $shift_id = 1;
            $shift_start_time = '09:00:00';
            $shift_end_time = '17:00:00';
            $work_week_id = 1;
            $late_time_allowed = 12;
        }
        $status = $request->status == 'true' ? 'Approved' : 'Pending';
        if($request->attendance_type == 'check_in'){
            $attendance = DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)
                ->where(DB::raw('DATE(in_time)') , $request->in_date)->first();

            if(!empty($attendance)){
                DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)
                ->where(DB::raw('DATE(in_time)') , $request->in_date)->delete();
            }

            DB::table('manual_attendance_request')->insert([
                'employee_id' => $request->employee_id,
                'in_time' => $request->in_date. ' ' .$request->in_time,
                'department_id' => $department->department,
                'shift_id' => $shift_id,
                'shift_start_time' => $shift_start_time,
                'shift_end_time' => $shift_end_time,
                'work_week_id' => $work_week_id,
                'late_time_allowed' => $late_time_allowed,
                'note' => $request->remarks,
                'status' => $status,
                'submitted_by' => $request->submitted_by
            ]);
            if($status == 'Approved'){
                $attendance = DB::table('attendance')->where('employee', $request->employee_id)->where(DB::raw('DATE(in_time)'), $request->in_date)->first();

                if(!empty($attendance)){
                    DB::table('attendance')->where('employee', $request->employee_id)->where(DB::raw('DATE(in_time)'), $request->in_date)->delete();
                }

                DB::table('attendance')->insert([
                    'employee' => $request->employee_id,
                    'in_time' => $request->in_date. ' ' .$request->in_time,
                    'department_id' => $department->department,
                    'is_online' => 1,
                    'is_manual' => 1,
                    'shift_id' => $shift_id,
                    'shift_start_time' => $shift_start_time,
                    'shift_end_time' => $shift_end_time,
                    'work_week_id' => $work_week_id,
                    'late_time_allowed' => $late_time_allowed,
                    'note' => $request->remarks,
                ]);
            }
        }
        elseif($request->attendance_type == 'check_out'){
            $attendance = DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)->where(DB::raw('DATE(in_time)'), $request->out_date)->whereNull('out_time')->first();
            if(!empty($attendance)){
                DB::table('manual_attendance_request')->where('id', $attendance->id)->update([
                    'out_time' => $request->out_date. ' ' .$request->out_time,
                    'status' => $status,
                ]);
            }
            else{
                $prevAttendance = DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)
                ->where(DB::raw('DATE(in_time)') , date ("Y-m-d", strtotime("-1 day", strtotime($request->out_date))))->whereNull('out_time')->first();

                if(!empty($prevAttendance)){
                    DB::table('manual_attendance_request')->where('id', $prevAttendance->id)->update([
                        'out_time' => $request->out_date. ' ' .$request->out_time,
                        'status' => $status,
                        'submitted_by' => $request->submitted_by
                    ]);
                }
            }

            $attendance = DB::table('attendance')->where('employee', $request->employee_id)->where(DB::raw('DATE(in_time)'), $request->out_date)->whereNull('out_time')->first();
            if(!empty($attendance)){
                if($status == 'Approved'){
                    DB::table('attendance')->where('id', $attendance->id)->update([
                        'out_time' => $request->out_date. ' ' .$request->out_time
                    ]);
                }
            }
            else{
                $prevAttendance = DB::table('attendance')->where('employee' , $request->employee_id)
                ->where(DB::raw('DATE(in_time)') , date ("Y-m-d", strtotime("-1 day", strtotime($request->out_date))))->whereNull('out_time')
                ->first();

                if(!empty($prevAttendance)){
                    if($status == 'Approved'){
                        DB::table('attendance')->where('id' , $prevAttendance->id)->update([
                            'out_time' => $request->out_date. ' ' .$request->out_time
                        ]);
                    }
                }
                else{
                    return response(['code' => 500,'status' => 'faliure','message' => 'Employee not Checked In!']);
                }
            }
        }
        else{
            $attendance = DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)->where(DB::raw('DATE(in_time)') , $request->in_date)->first();

            if(!empty($attendance)){
                DB::table('manual_attendance_request')->where('employee_id', $request->employee_id)->where(DB::raw('DATE(in_time)') , $request->in_date)->delete();   
            }

            DB::table('manual_attendance_request')->insert([
                'employee_id' => $request->employee_id,
                'in_time' => $request->in_date. ' ' .$request->in_time,
                'out_time' => $request->out_date. ' ' .$request->out_time,
                'department_id' => $department->department,
                'shift_id' => $shift_id,
                'shift_start_time' => $shift_start_time,
                'shift_end_time' => $shift_end_time,
                'work_week_id' => $work_week_id,
                'late_time_allowed' => $late_time_allowed,
                'note' => $request->remarks,
                'status' => $status,
                'submitted_by' => $request->submitted_by
            ]);

            if($status == 'Approved'){
                $attendance = DB::table('attendance')->where('employee' , $request->employee_id)->where(DB::raw('DATE(in_time)') , $request->in_date)->first();

                if(!empty($attendance)){
                    DB::table('attendance')->where('employee' , $request->employee_id)->where(DB::raw('DATE(in_time)') , $request->in_date)->delete();
                }

                DB::table('attendance')->insert([
                    'employee' => $request->employee_id,
                    'in_time' => $request->in_date. ' ' .$request->in_time,
                    'out_time' => $request->out_date. ' ' .$request->out_time,
                    'department_id' => $department->department,
                    'is_online' => 1,
                    'is_manual' => 1,
                    'shift_id' => $shift_id,
                    'shift_start_time' => $shift_start_time,
                    'shift_end_time' => $shift_end_time,
                    'work_week_id' => $work_week_id,
                    'late_time_allowed' => $late_time_allowed,
                    'note' => $request->remarks,
                ]);
            }
        }
        return response(['code' => 200,'status' => 'success','message' => 'Employee Attendance Has been added Sucessfuly']);
    }

    public function getEmployeeSalaryComponents(Request $request)
    {
        try{
            $salaryComponents = SalaryComponent::where('is_edit',1)->get();
            $components = [];
            if(count($salaryComponents) > 0){
                foreach ($salaryComponents as $key => $component) {
                    $employeeComponent = EmployeeSalary::where('employee',$request->employee_id)->where('component',$component->id)->first();
                    if(!empty($employeeComponent)){
                        $components[] = array(
                            "id"       =>  $component->id,
                            "name"     =>  $component->name,
                            "is_editable" => $component->is_edit,
                            "amount"   =>  $employeeComponent->amount
                        );
                    }
                    else{
                        $components[] = array(
                            "id"       =>  $component->id,
                            "name"     =>  $component->name,
                            "is_editable" => $component->is_edit,
                            "amount"   =>  "0"
                        );
                    }
                }
            }
            return response(['code' => 200, 'status' => 'success','data' => ['salary_component' => $components]]);
        }
        catch(\Exception $e){
            return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
        }
    }

    public function getCalculationGroups()
    {
        $CalculationGroups = CalculationGroup::get(['id','name']);
        return response(['code' => 200, 'message' => 'success', 'data' => ['CalculationGroups' => $CalculationGroups]]);
    }

    public function storeEmployeeComponents(Request $request)
    {
        try{
            $this->validate($request , [
                'salary_id' => 'required|array',
                'salary_amount' => 'required|array',
                'employee_id_send' => 'required',
                'calculation_group' => 'required',
            ]);

            foreach ($request->salary_id as $key => $component) {
                $employee_pendings = DB::table('employeesalary_pendings')->where([
                    ['employee' , $request->employee_id_send],
                    ['component' , $component]
                ])->first();
                if(!empty($employee_pendings)){
                    DB::table('employeesalary_pendings')->where('employee' , $request->employee_id_send)->where('component' , $component)->update([
                        'amount' => $request->salary_amount[$key],
                        'status' => 'Pending',
                        'submitted_by' => $request->submitted_by,
                        'remarks' => $request->remarks[$key]
                    ]);
                }
                else{
                    DB::table('employeesalary_pendings')->insert([
                        'employee' => $request->employee_id_send,
                        'component' => $component,
                        'amount' => $request->salary_amount[$key],
                        'status' => 'Pending',
                        'submitted_by' => $request->submitted_by,
                        'remarks' => $request->remarks[$key]
                    ]);
                }
                $payrollEmployee = DB::table('payrollemployees')->where('employee',$request->employee_id_send)->first();
                if(empty($payrollEmployee)){
                    DB::table('payrollemployees')->insert([
                        'employee' => $request->employee_id_send,
                        'pay_frequency' => 4,
                        'currency' => 131,
                        'deduction_exemptions' => '[]',
                        'deduction_allowed' => '[]',
                        'deduction_group' => $request->calculation_group
                    ]);
                }
            }
            return response(['code' => 200,'status' => "success",'message' => "Salary Component Added Successfully!"]);          

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function monthWiseManual(Request $request){
        try{
            $this->validate($request , [
                'department_id' => 'required',
                'month' => 'required'
            ]);
            
            $query = DB::table('month_manual_attend_permission')->where('department' , $request->department_id)->first();
            if(!empty($query)){
                DB::table('month_manual_attend_permission')->where('id', $query->id)->update([
                    'month' => $request->month,
                ]);
            }
            else{
                DB::table('month_manual_attend_permission')->insert([
                    'department' => $request->department_id,
                    'month' => $request->month,
                ]);
            }
            return response(['code' => 200,'status' => "success",'message' => "Assign Month To Department Successfully!"]);          

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }


    public function Approvals(Request $request)
    {
        try{
            $this->validate($request , [
                'department_id' => 'required',
                'status' => 'required',
                'R_type' => 'required'
            ]);
            
            // For Leave Requests
            if($request->R_type == 1){
                $leave_requests = EmployeeLeaveRequest::select('employeeleaves.*' ,'employeeleaves.id as row_id','employees.first_name' , 'employees.last_name' , 'leavetypes.name AS lt_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation' , 'users.username AS submitted_by' , 'employees.employee_code' , 'employees.job_title')
                ->join('employees' , 'employees.id' , '=' , 'employeeleaves.employee')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->join('leavetypes' , 'leavetypes.id' , '=' , 'employeeleaves.leave_type')
                ->join('users' , 'users.id' , '=' , 'employeeleaves.submitted_by')
                ->where('employees.status' , 'Active')
                ->where('employeeleaves.status' , $request->status)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty($request->department_id) && $request->department_id != 'all'){
                    $leave_requests->where('employees.department', $request->department_id);
                }
                if(!empty($request->designationfilter) && $request->designationfilter != 'all'){
                    $leave_requests->where('employees.job_title', $request->designationfilter);
                }
                if(!empty($request->date_from) && !empty($request->date_to)){
                    // $leave_requests->whereBetween(DB::raw('DATE(employeeleaves.created_at)'), [$request->date_from,$request->date_to]);
                    $leave_requests->whereDate('employeeleaves.created_at', '>=',$request->date_from)->whereDate('employeeleaves.created_at', '<=',$request->date_to);
                }
                $leave_requests = $leave_requests->orderBy('employeeleaves.created_at' , 'desc')->get();
                return response(['code' => 200,'status' => "success",'data' => ['leave_request' => $leave_requests]]); 
            }
            // For Shift Change Request
            elseif($request->R_type == 2){
                $shift_change_requests = ShiftManagement::select('shift_management.*','shift_management.id as row_id', 'employees.first_name' , 'employees.last_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation' , 'users.username AS submitted_by' , 'shift_type.shift_desc AS shift_name' , 'work_week.desc AS work_week_name', 'employees.employee_code' , 'employees.job_title')
                ->join('employees' , 'employees.id' , '=' , 'shift_management.employee_id')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->join('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
                ->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                ->join('users' , 'users.id' , '=' , 'shift_management.created_by')
                ->where('employees.status' , 'Active')
                ->where('shift_management.status' , $request->status)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty($request->department_id) && $request->department_id != 'all'){
                    $shift_change_requests->where('employees.department' , $request->department_id);
                }
                if(!empty($request->designationfilter) && $request->designationfilter != 'all'){
                    $shift_change_requests->where('employees.job_title', $request->designationfilter);
                }
                if(!empty($request->shift_type_filter) && $request->shift_type_filter != 'all'){
                    $shift_change_requests->where('shift_management.shift_id', $request->shift_type_filter);
                }
                if(!empty($request->date_from) && !empty($request->date_to)){
                    // $shift_change_requests->whereBetween(DB::raw('DATE(shift_management.created_at)'), [$request->date_from,$request->date_to]);
                    $shift_change_requests->whereDate('shift_management.created_at', '>=',$request->date_from)->whereDate('shift_management.created_at', '<=',$request->date_to);
                }
                $shift_change_requests = $shift_change_requests->get();
                return response(['code' => 200,'status' => "success",'data' => ['shift_request' => $shift_change_requests]]); 
            }
            // For Overtime Requests
            elseif($request->R_type == 3){
                $overtime_requests = OverTimeManagement::select('overtime_management.*' ,'overtime_management.id as row_id', 'work_week.desc AS work_week_name' , 'shift_type.shift_desc AS shift_name' , 'shift_type.shift_start_time' , 'shift_type.shift_end_time' , 'employees.first_name' , 'employees.last_name' , 'jobtitles.name AS designation' , 'companystructures.title AS department_name' , 'users.username AS submitted_by' , 'employees.employee_code')
                ->join('employees' , 'employees.id' , '=' , 'overtime_management.employee_id')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->join('users', 'users.id', '=' , 'overtime_management.submitted_by')
                ->join('work_week', 'work_week.id', '=' , 'overtime_management.work_week_id')
                ->join('shift_type', 'shift_type.id', '=' , 'overtime_management.shift_id')
                ->where('employees.status', 'Active')
                ->where('overtime_management.status', $request->status)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty($request->department_id) && $request->department_id !== 'all'){
                    $overtime_requests->where('employees.department' , $request->department_id);
                }
                if(!empty($request->date_from) && !empty($request->date_to)){
                    // $overtime_requests->whereBetween(DB::raw('DATE(overtime_management.created_at)'), [$request->date_from,$request->date_to]);
                    $overtime_requests->whereDate('overtime_management.created_at', '>=',$request->date_from)->whereDate('overtime_management.created_at', '<=',$request->date_to);
                }
                $overtime_requests = $overtime_requests->get();
                if(count($overtime_requests) > 0){
                    foreach($overtime_requests as $overtime_request){
                        $attendance = DB::table('attendance')->where([
                            ['employee' , $overtime_request->employee_id],
                            [DB::raw('DATE(in_time)') , $overtime_request->overtime_date]
                        ])->first(['in_time' , 'out_time']);

                        if(empty($attendance)){
                            $leave = DB::table('employeeleaves')->where([
                                ['employee' , $overtime_request->employeee_id],
                                ['date_start' , '<=' , $overtime_request->overtime_date],
                                ['date_end' , '>=' , $overtime_request->overtime_date],
                                ['status' , 'Approved'],
                            ])->first();
                            if(!empty($leave)){
                                $is_leave = 'Yes';
                            }
                        }
                        $work_week_days = DB::table('workdays')->where([
                            ['work_week_id' , $overtime_request->work_week_id],
                            ['status' , 'Non-working Day']
                        ])->get();
                        if(count($work_week_days) > 0){
                            foreach($work_week_days as $work_week_day){
                                $requested_day = date('l' , strtotime($overtime_request->overtime_date));
                                if($work_week_day->name == $requested_day){
                                    $is_holiday = 'Off Day';
                                }
                            }
                        }
                        $holiday = DB::table('holidays')->where('dateh' , $overtime_request->overtime_date)->first();
                        if(!empty($holiday)){
                            if(!empty($is_holiday)){
                                $is_holiday = $holiday->name . 'and' . $is_holiday;
                            }
                            else{
                                $is_holiday = $holiday->name;
                            }
                        }
                        $overtime_request->in_time = !empty($attendance) ? $attendance->in_time : '';
                        $overtime_request->out_time = !empty($attendance) ? $attendance->out_time : '';
                        $overtime_request->leave = isset($is_leave) ? $is_leave : 'NO';
                        $overtime_request->is_holiday = isset($is_holiday) ? $is_holiday : '';
                    }
                }
                return response(['code' => 200,'status' => "success",'data' => ['overtime_request' => $overtime_requests]]);
            }
            // Misclleaneous Requests
            elseif($request->R_type == 4){
                $miscellaneous = MiscellaneousRequest::select('miscellaneous.*','miscellaneous.id as row_id', 'employees.first_name', 'employees.last_name', 'companystructures.title as department_name', 'jobtitles.name AS designation', 'users.username as submitted_by', 'miscellaneous_request_type.name as request_type' , 'employees.employee_code')
                ->join('employees' , 'employees.id' , '=' , 'miscellaneous.employee_id')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->leftjoin('miscellaneous_request_type' , 'miscellaneous_request_type.id' , '=' , 'miscellaneous.request_type')
                ->join('users' , 'users.id' , '=' , 'miscellaneous.submitted_by')
                ->where('employees.status' , 'Active')
                ->where('miscellaneous.status' , $request->status)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty($request->department_id) && $request->department_id != 'all'){
                    $miscellaneous->where('employees.department' , $request->department_id);
                }
                if(!empty($request->date_from) && !empty($request->date_to)){
                    // $miscellaneous->whereBetween(DB::raw('DATE(miscellaneous.created_at)'), [$request->date_from,$request->date_to]);
                    $miscellaneous->whereDate('miscellaneous.created_at', '>=',$request->date_from)->whereDate('miscellaneous.created_at', '<=',$request->date_to);
                }
                $miscellaneous = $miscellaneous->get();
                return response(['code' => 200,'status' => "success",'data' => ['miscellaneous' => $miscellaneous]]);
            }
            // For Short Leave Requests
            elseif($request->R_type == 5){
                $short_leave_requests = ShortLeaveRequest::select('short_leave_requests.*' ,'short_leave_requests.id as row_id', 'employees.first_name' , 'employees.last_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation' , 'users.username AS submitted_by' , 'employees.employee_code')
                ->join('employees' , 'employees.id' , '=' , 'short_leave_requests.employee_id')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->join('users' , 'users.id' , '=' , 'short_leave_requests.submitted_by')
                ->where('employees.status' , 'Active')
                ->where('short_leave_requests.status' , $request->status)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty($request->department_id) && $request->department_id != 'all'){
                    $short_leave_requests->where('employees.department' , $request->department_id);
                }
                if(!empty($request->date_from) && !empty($request->date_to)){
                    // $short_leave_requests->whereBetween(DB::raw('DATE(short_leave_requests.created_at)'), [$request->date_from,$request->date_to]);
                    $short_leave_requests->whereDate('short_leave_requests.created_at', '>=',$request->date_from)->whereDate('short_leave_requests.created_at', '<=',$request->date_to);
                }
                $short_leave_requests = $short_leave_requests->get();
                return response(['code' => 200,'status' => "success",'data' => ['short_leavel_request' => $short_leave_requests]]);
            }
            // Loan Requests
            elseif($request->R_type == 6){
                $loan_requests = LoanRequest::select('loan_requests.*' , 'employees.first_name' ,'loan_requests.id as row_id', 'employees.last_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation','users.username AS submitted_by' , 'employees.employee_code')
                ->join('employees' , 'employees.id' , '=' , 'loan_requests.employee_id')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->join('users' , 'users.id' , '=' , 'loan_requests.submitted_by')
                ->where('employees.status' , 'Active')
                ->where('loan_requests.status' , $request->status)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty($request->department_id) && $request->department_id != 'all'){
                    $loan_requests->where('employees.department' , $request->department_id);
                }
                if(!empty($request->date_from) && !empty($request->date_to)){
                    // $loan_requests->whereBetween(DB::raw('DATE(loan_requests.created_at)'), [$request->date_from,$request->date_to]);
                    $loan_requests->whereDate('loan_requests.created_at', '>=',$request->date_from)->whereDate('loan_requests.created_at', '<=',$request->date_to);
                }
                $loan_requests = $loan_requests->get();
                return response(['code' => 200,'status' => "success",'data' => ['loan_requests' => $loan_requests]]);
            }
            // For Department Change Requests
            elseif($request->R_type == 7){
                $department_change_requests = DepartmentChange::select('department_requests.*' ,'department_requests.id as row_id', 'jobtitles.name AS designation' , 'employees.first_name' , 'employees.last_name' , 'cs.title AS dept_to' , 'companystructures.title AS dept_from' , 'users.username AS submitted_by', 'employees.employee_code')
                ->join('employees' , 'employees.id' , '=' , 'department_requests.employee_id')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->join('companystructures AS cs' , 'cs.id' , '=' , 'department_requests.dept_id')
                ->join('companystructures' , 'companystructures.id' , '=' , 'department_requests.dept_from')
                ->join('users' , 'users.id' , '=' , 'department_requests.submitted_by')
                ->where('employees.status' , 'Active')
                ->where('department_requests.status' , $request->status)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty($request->department_id) && $request->department_id != 'all'){
                    $department_change_requests->where('employees.department' , $request->department_id);
                }
                if(!empty($request->date_from) && !empty($request->date_to)){
                    // $department_change_requests->whereBetween(DB::raw('DATE(department_requests.created_at)'), [$request->date_from,$request->date_to]);
                    $department_change_requests->whereDate('department_requests.created_at', '>=',$request->date_from)->whereDate('department_requests.created_at', '<=',$request->date_to);
                }
                $department_change_requests = $department_change_requests->get();
                return response(['code' => 200,'status' => "success",'data' => ['department_request' => $department_change_requests]]);
            }
            // Salary Allowance Requests
            elseif($request->R_type == 8){
                $salaryAllownaces = DB::table('employeesalary_pendings')->select('employeesalary_pendings.id','employeesalary_pendings.employee','employeesalary_pendings.updated_at','employees.first_name' , 'employees.last_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation' , 'users.username AS submitted_by' , 'employees.employee_code' , 'employees.job_title')
                ->selectRaw('DATE(employeesalary_pendings.updated_at) AS created_at')
                ->join('employees' , 'employees.id' , '=' , 'employeesalary_pendings.employee')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->join('users' , 'users.id' , '=' , 'employeesalary_pendings.submitted_by')
                ->where('employees.status' , 'Active')
                ->where('employeesalary_pendings.status', $request->status)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty($request->department_id) && $request->department_id != 'all'){
                    $salaryAllownaces->where('employees.department' , $request->department_id);
                }
                if(!empty($request->designationfilter) && $request->designationfilter != 'all'){
                    $salaryAllownaces->where('employees.job_title', $request->designationfilter);
                }
                if(!empty($request->date_from) && !empty($request->date_to)){
                    $salaryAllownaces->whereDate('employeesalary_pendings.updated_at', '>=',$request->date_from)->whereDate('employeesalary_pendings.updated_at', '<=',$request->date_to);
                }
                $salaryAllownaces = $salaryAllownaces->groupBy('employeesalary_pendings.employee')->get()
                ->map(function($salaryAllownace, $key) {
                    $components = DB::table('employeesalary_pendings')->select('employeesalary_pendings.id','employeesalary_pendings.amount','employeesalary_pendings.remarks','employeesalary_pendings.status','salarycomponent.name')
                    ->join('salarycomponent' , 'salarycomponent.id' , '=' , 'employeesalary_pendings.component')
                    ->where('is_edit',1)
                    ->where('employeesalary_pendings.employee',$salaryAllownace->employee)->get();
                    foreach ($components as $key => $component) {
                        $salaryAllownace->components[] = $component;
                    }

                    return $salaryAllownace;
                });

                return response(['code' => 200, 'status' => 'success','data' => ['componant_allowance' => $salaryAllownaces]]);
            }
            // For Mannual Attendance Request
            else{
                $mannual_attendance = DB::table('manual_attendance_request')->select('manual_attendance_request.*' ,'manual_attendance_request.id as row_id', 'employees.first_name' , 'employees.last_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation' , 'users.username AS submitted_by' , 'employees.employee_code' , 'employees.job_title')
                ->join('employees' , 'employees.id' , '=' , 'manual_attendance_request.employee_id')
                ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
                ->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
                ->join('users' , 'users.id' , '=' , 'manual_attendance_request.submitted_by')
                ->where('employees.status' , 'Active')
                ->where('manual_attendance_request.status', $request->status)
                ->whereIn('employees.department' , login_user_departments());
                if(!empty($request->department_id) && $request->department_id != 'all'){
                    $mannual_attendance->where('employees.department' , $request->department_id);
                }
                if(!empty($request->designationfilter) && $request->designationfilter != 'all'){
                    $mannual_attendance->where('employees.job_title', $request->designationfilter);
                }
                if(!empty($request->date_from) && !empty($request->date_to)){
                    // $mannual_attendance->whereBetween(DB::raw('DATE(manual_attendance_request.created_at)'), [$request->date_from,$request->date_to]);
                    $mannual_attendance->whereDate('manual_attendance_request.created_at', '>=',$request->date_from)->whereDate('manual_attendance_request.created_at', '<=',$request->date_to);
                }
                $mannual_attendance = $mannual_attendance->get();
                return response(['code' => 200,'status' => "success",'data' => ['manual_attendance_request' => $mannual_attendance]]);
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function approvalStatus(Request $request)
    {
        try{
            $this->validate($request , [
                'row_id' => 'required',
                'request_type' => 'required',
                'status_change' => 'required'
            ]);
            // For Leave Request
            if($request->request_type == 'leave_request'){
                $leave_request = EmployeeLeaveRequest::find($request->row_id);
                $leave_request->status = $request->status_change;
                $leave_request->status_changed_by = Auth::user()->id;
                $leave_request->status_changed_at = Carbon::now();
                $leave_request->update();
                return response(['code' => 200,'status' => "success",'message' => "Leave Request ".$request->status_change." Successfully!"]);
            }
            // For Short Leave Request
            elseif($request->request_type == 'short_leave'){
                $short_leave_request = ShortLeaveRequest::find($request->row_id);
                $short_leave_request->status = $request->status_change;
                $short_leave_request->status_changed_by = Auth::user()->id;
                $short_leave_request->status_changed_at = Carbon::now();
                $short_leave_request->update();
                return response(['code' => 200,'status' => "success",'message' => "Short Leave Request ".$request->status_change." Successfully!"]);
            }
            elseif($request->request_type == 'department_change'){
                $department_change = DepartmentChange::find($request->row_id);
                $department_change->status = $request->status_change;
                $department_change->status_changed_by = Auth::user()->id;
                $department_change->status_changed_at = Carbon::now();
                $department_change->update();
                $employee = Employee::where('id' , $department_change->employee_id)->first();
                $employee->department = $department_change->dept_id;
                $employee->update();
                return response(['code' => 200,'status' => "success",'message' => "Department Request ".$request->status_change." Successfully!"]);
            }
            // For Overtime Request
            elseif($request->request_type == 'overtime_request'){
                $overtime_request = OverTimeManagement::find($request->row_id);
                $overtime_request->status = $request->status_change;
                $overtime_request->status_changed_by = Auth::user()->id;
                $overtime_request->status_changed_at = Carbon::now();
                $overtime_request->approved = 1;
                $overtime_request->update();
                return response(['code' => 200,'status' => "success",'message' => "Overtime Request ".$request->status_change." Successfully!"]);
            }
            // For Shift Chnage
            elseif($request->request_type == 'shift_change'){
                $shift_change = ShiftManagement::find($request->row_id);
                $shift_change->status = $request->status_change;
                $shift_change->status_changed_by = Auth::user()->id;
                $shift_change->status_changed_at = Carbon::now();
                $shift_change->update();
                return response(['code' => 200,'status' => "success",'message' => "Shift Request ".$request->status_change." Successfully!"]);
            }
            // For Loan Request
            elseif($request->request_type == 'loan'){
                $loan_request = LoanRequest::find($request->row_id);
                $loan_request->status = $request->status_change;
                $loan_request->status_changed_by = Auth::user()->id;
                $loan_request->status_changed_at = Carbon::now();
                $loan_request->update();
                if($request->status_change == 'Approved'){
                    $start_date = new Carbon($loan_request->start_date);
                    for($i = 0 ; $i < $loan_request->time_duration ; $i++){
                        $checkInstallment = DB::table('loan_installments')->where('loan_request_id', $request->row_id)->whereYear('month', $start_date->year)->whereMonth('month', $start_date->format('m'))->first(['id']);
                        if(empty($checkInstallment)){
                            DB::table('loan_installments')->insert([
                                'loan_request_id' => $loan_request->id,
                                'month' => $start_date,
                                'amount' => $loan_request->monthly_installment,
                                'status' => 'active',
                            ]);
                        }
                        $start_date = $start_date->addMonths(1);
                    }
                }
                return response(['code' => 200,'status' => "success",'message' => "Loan Request ".$request->status_change." Successfully!"]);
            }
            // For Miscellanous Request
            elseif($request->request_type == 'miscellaneous'){
                $miscellaneous = MiscellaneousRequest::find($request->row_id);
                $miscellaneous->status = $request->status_change;
                $miscellaneous->status_changed_by = Auth::user()->id;
                $miscellaneous->status_changed_at = Carbon::now();
                $miscellaneous->update();
                return response(['code' => 200,'status' => "success",'message' => "Miscellaneous Request ".$request->status_change." Successfully!"]);
            }
            // For Salary Allowances Request
            elseif($request->request_type == 'allowance'){
                DB::table('employeesalary_pendings')->where('employee', $request->row_id)->update(['status' => $request->status_change]);
                if($request->status_change == 'Approved'){
                    $salaryAllowances = DB::table('employeesalary_pendings')->join('salarycomponent','salarycomponent.id','=','employeesalary_pendings.component')->where('salarycomponent.is_edit',1)->where('employeesalary_pendings.employee', $request->row_id)->get(['employeesalary_pendings.*']);
                    if(count($salaryAllowances) > 0){
                        foreach ($salaryAllowances as $key => $allowance) {
                            $salary = EmployeeSalary::where('employee',$request->row_id)->where('component',$allowance->component)->first();
                            if(!empty($salary)){
                                $employeeSalary = EmployeeSalary::find($salary->id);
                                $employeeSalary->amount = $allowance->amount;
                                $employeeSalary->status = 'Approved';
                                $employeeSalary->submitted_by = $allowance->submitted_by;
                                $employeeSalary->details = $allowance->remarks;
                                $employeeSalary->update();
                            }
                            else{
                                $employeeSalary = new EmployeeSalary();
                                $employeeSalary->employee = $allowance->employee;
                                $employeeSalary->component = $allowance->component;
                                $employeeSalary->amount = $allowance->amount;
                                $employeeSalary->status = 'Approved';
                                $employeeSalary->submitted_by = $allowance->submitted_by;
                                $employeeSalary->details = $allowance->remarks;
                                $employeeSalary->save();
                            }
                        }
                    }
                }
                return response(['code' => 200,'status' => "success",'message' => "Salary Allowance Request ".$request->status_change." Successfully!"]);
            }
            // For Mannual Attendance
            else{
                $manual_attendance = ManualAttendanceRequest::find($request->row_id);
                $manual_attendance->status = $request->status_change;
                $manual_attendance->status_changed_by = Auth::user()->id;
                $manual_attendance->status_changed_at = Carbon::now();
                $manual_attendance->update();
                if($request->status_change == 'Approved'){
                    $attendance = DB::table('attendance')->where([
                        ['employee' , $manual_attendance->employee_id],
                        [DB::raw('DATE(in_time)') , date('Y-m-d' , strtotime($manual_attendance->in_time))]
                    ])->first();
                    if(!empty($attendance)){
                        DB::table('attendance')->where([
                            ['employee' , $manual_attendance->employee_id],
                            [DB::raw('DATE(in_time)') , date('Y-m-d' , strtotime($manual_attendance->in_time))]
                        ])->delete();
                    }
                    DB::table('attendance')->insert([
                        'employee' => $manual_attendance->employee_id,
                        'is_manual' => 1,
                        'is_online' => 1,
                        'in_time' => $manual_attendance->in_time,
                        'out_time' => $manual_attendance->out_time,
                        'department_id' => $manual_attendance->department_id,
                        'shift_id' => $manual_attendance->shift_id,
                        'shift_start_time' => $manual_attendance->shift_start_time,
                        'shift_end_time' => $manual_attendance->shift_end_time,
                        'work_week_id' => $manual_attendance->work_week_id,
                        'late_time_allowed' => $manual_attendance->late_time_allowed,
                        'note' => $manual_attendance->note
                    ]);
                }
                return response(['code' => 200,'status' => "success",'message' => "Mannual Attendance Request ".$request->status_change." Successfully!"]);
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422,'status' => "failure",'errors' => $e->errors()];
            }else{
                return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
            }
        }
    }

    public function attendanceReport(Request $request)
    {
        try{
            if(!empty($request->department)){
                $login_user_departs = $request->department;
            }
            else{
                $login_user_departs = implode(',',login_user_departments());
            }
            $date = empty($request->date) ? date('Y-m-d') : $request->date;
            // Total Employees
            $total_employees = Employee::where([
                ['status' , 'Active'],
                ['suspended' , 0]
            ])->whereIn('department' , login_user_departments());
            if(!empty($request->department)){
                $total_employees->where('department' , $request->department);
            }
            $total_employees = $total_employees->count();
    
            // Total Present
            $current_time = date('H:i:s');
            $total_present_employees = DB::select("SELECT employees.id
            FROM employees 
            JOIN attendance ON attendance.employee = employees.id
            -- JOIN companystructures ON companystructures.id = employees.department
            -- LEFT JOIN shift_management ON shift_management.employee_id = employees.id
            -- LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
            where DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date' 
            AND employees.department IN ($login_user_departs)
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id");
    
            // Total Absent
            $total_absent_employees = DB::select("SELECT employees.id
            FROM employees
            left join shift_management on shift_management.employee_id = employees.id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
            WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
            GROUP BY employee_id))
            left join shift_type on shift_type.id = shift_management.shift_id	
            WHERE employees.id NOT IN (SELECT employee FROM attendance where DATE_FORMAT(in_time,'%Y-%m-%d') = '$date')
            AND employees.id NOT IN (SELECT employee FROM employeeleaves 
            where '$date' between date_start and date_end
            AND status = 'Approved')
            AND employees.id NOT IN (
                SELECT employee_id
                from shift_management
                join workdays on workdays.work_week_id = shift_management.work_week_id
                where workdays.status = 'Non-working Day'
                and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
                )
                and '$current_time' > IFNULL(shift_type.shift_start_time,time('$date 09:00:00'))
                AND department IN ($login_user_departs)
                AND employees.`status` = 'Active'
                AND employees.`suspended` = '0'
                ");
    
            // Total LateCommers
            $grace_time_allowed = 11;
            $total_late_employees = DB::select("SELECT employees.id
            FROM employees 
            JOIN attendance ON attendance.employee = employees.id
            AND (attendance.id IN (SELECT MIN(id) AS a_id FROM attendance 
            WHERE DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date' GROUP BY employee ))
            -- JOIN companystructures ON companystructures.id = employees.department
            
            LEFT JOIN shift_management ON shift_management.employee_id = employees.id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
            WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
            GROUP BY employee_id))
            LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
            
            WHERE employees.department IN ($login_user_departs)
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            AND (time(in_time) > IFNULL(shift_type.shift_start_time+INTERVAL shift_management.late_time_allowed MINUTE,time('2018-12-06 09:00:00')+INTERVAL '$grace_time_allowed' MINUTE) )");
    
            // Total Employees On Leave
            $total_leave_employees = DB::select("SELECT employees.id
            FROM employees
            -- join companystructures on companystructures.id = employees.department 
            join employeeleaves on employeeleaves.employee = employees.id
            where '$date'  BETWEEN employeeleaves.date_start and employeeleaves.date_end
            AND employeeleaves.status = 'Approved'
            AND employees.department IN ($login_user_departs)
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id");
            
            // Total Employees On Leave But Present
            $total_leave_present = DB::select("SELECT employees.id
            from employees 
            join attendance on attendance.employee = employees.id
            -- join companystructures on companystructures.id = employees.department
            join employeeleaves on employeeleaves.employee = employees.id
            where '$date' BETWEEN employeeleaves.date_start and employeeleaves.date_end
            AND employeeleaves.status = 'Approved'
            and DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date'
            AND employees.department IN ($login_user_departs)
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id");
    
            // Employees Non Working Day
            $total_on_off_day = DB::select("SELECT employees.id
            from employees 
            -- join companystructures on companystructures.id = employees.department
            
            join shift_management on employees.id = shift_management.employee_id
        
            join workdays on workdays.work_week_id = shift_management.work_week_id
            where workdays.status = 'Non-working Day'
            and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                                    WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                                    GROUP BY employee_id)) 
            
            and employees.department IN ($login_user_departs)
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id");
    
            // Employees Non Working Day But Present
            $total_on_off_day_present = DB::select("SELECT employees.id
            from employees 
            join attendance on attendance.employee = employees.id
            -- join companystructures on companystructures.id = employees.department
            join shift_management on employees.id = shift_management.employee_id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                GROUP BY employee_id))
            join workdays  on workdays.work_week_id = shift_management.work_week_id
            where workdays.status = 'Non-working Day'
            and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
            and DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = CURDATE()
            -- AND CURDATE() between shift_management.shift_from_date and shift_management.shift_to_date
            and employees.department IN ($login_user_departs)
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id");
    
            // Employees Missing Checkouts
            $total_missing_checkout = DB::select("SELECT employees.id
            FROM employees
            LEFT JOIN attendance ON attendance.employee = employees.id
            -- join companystructures on companystructures.id = employees.department
            WHERE attendance.out_time IS NULL AND DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date' - INTERVAL 1 DAY 
            AND employees.department IN ($login_user_departs)
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id");
            return response(['code' => 200,'status' => "success",'data' => ['total_employees' => $total_employees,'total_present' => count($total_present_employees),'total_absent' => count($total_absent_employees),'total_late' => count($total_late_employees),'total_no_chackout' => count($total_missing_checkout),'total_on_leave' => count($total_leave_employees),'total_on_off_day' => count($total_on_off_day),'total_on_off_day_but_present' => count($total_on_off_day_present),'total_on_leave_but_present' => count($total_leave_present)]]);
        }
        catch(\Exception $e){
            return ['code' => 500,'status' => "failure",'message' => $e->getMessage()];
        }
    }

    public function getAttendanceReportEmployees(Request $request)
    {
        $date = $request->date;
        $current_time = date('H:i:s');
        $filter = "";
        if(!empty($request->department)){
            $login_user_departs = $request->department;
            $department_id = $request->department;
            $login_user_ids ="= ($login_user_departs)";
            $filter = "AND employees.`department` = '$department_id'";
        }
        else{
            $login_user_departs = implode(',',login_user_departments());
            $login_user_ids ="IN ($login_user_departs)";
        }
        $holiday = DB::table('holidays')->where('dateh', "$date")->first(['dateh']);
        if($request->event == 'total_employees'){
            $query = Employee::select('employees.id','employees.employee_id','employees.employee_code','employees.mobile_phone','companystructures.title AS dep_title','shift_type.shift_desc','shift_type.shift_start_time','shift_management.shift_from_date','shift_management.shift_to_date',DB::raw("CONCAT(COALESCE(NULLIF(employees.first_name, ''), ''), ' ', COALESCE(NULLIF(employees.last_name, ''), '')) AS 'name'"))
            ->join('companystructures', 'companystructures.id', '=', 'employees.department')
            // ->join('employmentstatus' , 'employmentstatus.id' , '=' , 'employees.employment_status')
            // ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
            ->leftjoin('shift_management', function ($join) use ($date) {
                    $join->on('employees.id', '=', 'shift_management.employee_id')
                    ->where([
                        ['shift_management.shift_from_date', '<=', $date],
                        ['shift_management.shift_to_date', '>=', $date]
                    ])
                    ->select(DB::raw('MAX(shift_management.id)'));
                })
            ->leftjoin('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
            ->where([
                ['employees.status' , 'Active'],
                ['employees.suspended' , 0]
            ])->whereIn('employees.department' , login_user_departments());
            if(!empty($request->department)){
                $query->where('employees.department' , $request->department);
            }
            $query->groupBy('employees.id')->orderBy('companystructures.title');
            $query = $query->get();
        }
        if($request->event == 'total_present'){
            $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,attendance.in_time,attendance.out_time,employees.mobile_phone,companystructures.title AS dep_title,CONCAT(COALESCE(NULLIF(employees.first_name, ''), ''), ' ', COALESCE(NULLIF(employees.last_name, ''), '')) AS 'name',IFNULL(shift_type.shift_start_time,'09:00:00') as shift_start_time,
            shift_type.shift_desc,
            IFNULL(shift_management.late_time_allowed,11) as late_time_allowed
            FROM employees 
            JOIN attendance ON attendance.employee = employees.id
            AND (attendance.id IN (SELECT MAX(id) AS a_id FROM attendance 
            WHERE DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date' GROUP BY employee ))
            JOIN companystructures ON companystructures.id = employees.department
            LEFT JOIN shift_management ON shift_management.employee_id = employees.id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
            WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date AND shift_management.status='Approved'
            GROUP BY employee_id))
            LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
            where employees.department $login_user_ids
            $filter
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            ORDER BY companystructures.title ASC");
        }
        if($request->event == 'total_absent'){
            if(!empty($holiday))
            {
                $query = [];
            }
            else
            {
                $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,shift_type.shift_desc,employees.mobile_phone,companystructures.title AS dep_title,CONCAT(COALESCE(NULLIF(employees.first_name, ''), ''), ' ', COALESCE(NULLIF(employees.last_name, ''), '')) AS 'name',IFNULL(shift_type.shift_start_time,'09:00:00') as shift_start_time
                FROM employees
                JOIN companystructures on companystructures.id = employees.department
                left join shift_management on shift_management.employee_id = employees.id
                AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                GROUP BY employee_id))
                left join shift_type on shift_type.id = shift_management.shift_id	
                WHERE employees.id NOT IN (SELECT employee FROM attendance where DATE_FORMAT(in_time,'%Y-%m-%d') = '$date')
                AND employees.id NOT IN (SELECT employee FROM employeeleaves 
                where '$date' between date_start and date_end
                AND status = 'Approved')
                AND employees.id NOT IN (
                    SELECT employee_id
                    from shift_management
                    join workdays on workdays.work_week_id = shift_management.work_week_id
                    where workdays.status = 'Non-working Day'
                    and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
                    )
                    and '$current_time' > IFNULL(shift_type.shift_start_time,time('$date 09:00:00'))
                    AND department $login_user_ids
                    $filter
                    AND employees.`status` = 'Active'
                    AND employees.`suspended` = '0'
                    ORDER BY companystructures.title");
            }
        }
        if($request->event == 'late_commers'){
            $grace_time_allowed = 11;
            $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,attendance.in_time,attendance.out_time,employees.mobile_phone,companystructures.title AS dep_title,attendance.in_time,attendance.out_time,shift_type.shift_desc,CONCAT(COALESCE(NULLIF(employees.first_name, ''), ''), ' ', COALESCE(NULLIF(employees.last_name, ''), '')) AS 'name',IFNULL(shift_type.shift_start_time,'09:00:00') as shift_start_time,
            shift_type.shift_desc,
            IFNULL(shift_management.late_time_allowed,11) as late_time_allowed
            FROM employees 
            JOIN attendance ON attendance.employee = employees.id
            AND (attendance.id IN (SELECT MIN(id) AS a_id FROM attendance 
            WHERE DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date' GROUP BY employee ))
            JOIN companystructures ON companystructures.id = employees.department
            
            LEFT JOIN shift_management ON shift_management.employee_id = employees.id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
            WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
            GROUP BY employee_id))
            LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
            
            WHERE employees.department $login_user_ids
            $filter
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            AND (time(in_time) > IFNULL(shift_type.shift_start_time+INTERVAL shift_management.late_time_allowed MINUTE,time('2018-12-06 09:00:00')+INTERVAL '$grace_time_allowed' MINUTE) )ORDER BY companystructures.title");
        }
        if($request->event == 'total_on_leave'){
            $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,employees.mobile_phone,companystructures.title AS dep_title,shift_type.shift_desc,CONCAT(COALESCE(NULLIF(employees.first_name, ''), ''), ' ', COALESCE(NULLIF(employees.last_name, ''), '')) AS 'name'
            FROM employees
            JOIN companystructures on companystructures.id = employees.department 
            join employeeleaves on employeeleaves.employee = employees.id
            JOIN shift_management ON shift_management.employee_id = employees.id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
            WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
            GROUP BY employee_id))
            JOIN shift_type ON shift_type.id = shift_management.shift_id
            where '$date'  BETWEEN employeeleaves.date_start and employeeleaves.date_end
            AND employeeleaves.status = 'Approved'
            AND employees.department $login_user_ids
            $filter
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id
            ORDER BY companystructures.title");
        }
        if($request->event == 'total_on_leave_but_present'){
            $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,employees.mobile_phone,companystructures.title AS dep_title,shift_type.shift_desc,attendance.in_time,attendance.out_time,CONCAT(COALESCE(NULLIF(employees.first_name, ''), ''), ' ', COALESCE(NULLIF(employees.last_name, ''), '')) AS 'name',employeeleaves.leave_type
            from employees 
            join attendance on attendance.employee = employees.id
            join companystructures on companystructures.id = employees.department
            join employeeleaves on employeeleaves.employee = employees.id
            LEFT JOIN shift_management ON shift_management.employee_id = employees.id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
            WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
            GROUP BY employee_id))
            LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
            where '$date' BETWEEN employeeleaves.date_start and employeeleaves.date_end
            AND employeeleaves.status = 'Approved'
            and DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date'
            AND employees.department $login_user_ids
            $filter
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id
            ORDER BY companystructures.title");
        }
        if($request->event == 'total_on_off_day'){
            $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,employees.mobile_phone,companystructures.title AS dep_title,shift_type.shift_desc,CONCAT(COALESCE(NULLIF(employees.first_name, ''), ''), ' ', COALESCE(NULLIF(employees.last_name, ''), '')) AS 'name'
            from employees 
            join companystructures on companystructures.id = employees.department
            -- JOIN jobtitles ON jobtitles.id = employees.job_title
            -- JOIN employmentstatus ON employmentstatus.id = employees.employment_status
            LEFT JOIN shift_management on employees.id = shift_management.employee_id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
            WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
            GROUP BY employee_id))
            LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
            join workdays on workdays.work_week_id = shift_management.work_week_id
            where workdays.status = 'Non-working Day'
            and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                                    WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                                    GROUP BY employee_id)) 
            
            and employees.department $login_user_ids
            $filter
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id
            ORDER BY companystructures.title");
        }
        if($request->event == 'total_on_off_day_but_present'){
            $query = DB::select("SELECT employees.id,employees.employee_id,employees.employee_code,employees.mobile_phone,companystructures.title AS dep_title,shift_type.shift_desc,attendance.in_time,attendance.out_time,CONCAT(COALESCE(NULLIF(employees.first_name, ''), ''), ' ', COALESCE(NULLIF(employees.last_name, ''), '')) AS 'name',shift_management.work_week_id,LEFT(workdays.`name`,3) as off_day_name
            from employees 
            JOIN attendance on attendance.employee = employees.id
            JOIN companystructures on companystructures.id = employees.department
            LEFT JOIN shift_management on employees.id = shift_management.employee_id
            LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
                WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
                GROUP BY employee_id))
            JOIN workdays  on workdays.work_week_id = shift_management.work_week_id
            where workdays.status = 'Non-working Day'
            and LEFT(workdays.`name`,3) = '".date("D", strtotime($date))."'
            and DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = CURDATE()
            -- AND CURDATE() between shift_management.shift_from_date and shift_management.shift_to_date
            and employees.department $login_user_ids
            $filter
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id
            ORDER BY companystructures.title");
        }
        if($request->event == 'missing_checkouts'){
            $query = DB::select("SELECT employees.id,employees.employee_id,
            employees.employee_code,attendance.in_time,attendance.out_time,employees.mobile_phone,companystructures.title AS dep_title,shift_type.shift_desc,CONCAT(COALESCE(NULLIF(employees.first_name, ''), ''), ' ', COALESCE(NULLIF(employees.last_name, ''), '')) AS 'name'
            FROM employees
            LEFT JOIN attendance ON attendance.employee = employees.id
            JOIN companystructures on companystructures.id = employees.department
            -- JOIN jobtitles ON jobtitles.id = employees.job_title
            -- JOIN employmentstatus ON employmentstatus.id = employees.employment_status
            LEFT JOIN shift_management on employees.id = shift_management.employee_id
            AND (shift_management.id IN (SELECT MAX(id) AS id FROM shift_management
            WHERE '$date' between shift_management.shift_from_date AND shift_management.shift_to_date
            GROUP BY employee_id))
            LEFT JOIN shift_type ON shift_type.id = shift_management.shift_id
            WHERE attendance.out_time IS NULL AND DATE_FORMAT(attendance.in_time,'%Y-%m-%d') = '$date' - INTERVAL 1 DAY 
            AND employees.department $login_user_ids
            $filter
            AND employees.`status` = 'Active'
            AND employees.`suspended` = '0' 
            group by employees.id
            ORDER BY companystructures.title");
        }
        return response(['code' => 200, 'status' => 'success','data' =>  $query]);
    }

    public function downloadSalarySheet(Request $request)
    {
        $payroll = Payroll::where('department',$request->filter_department_id)->whereMonth('date_start', $request->month_id)->whereYear('date_start', $request->year_id)->where('status', 'Completed')->first(['id','name','columns','date_start']);
        if(!empty($payroll)){
            $prev_month_date = date("Y-m-d", strtotime("-1 months",strtotime($payroll->date_start)));
            $prev_payroll = Payroll::where('department', $payroll->department)->whereYear('date_start', date('Y',strtotime($prev_month_date)))->whereMonth('date_start', date('m',strtotime($prev_month_date)))->where('status', 'Completed')->first(['id']);
            if(!empty($prev_payroll)){
                $prevTotalEmployees = PayrollData::where('payroll',$prev_payroll->id)->groupBy('employee')->get(['employee']);
            }
            $columns = json_decode($payroll->columns);
            $payroll_columns = PayrollColumn::where('is_visible', 1)->whereIn('id', $columns)->where(function($query){
                    $query->whereNull('employers_column')->orWhere('employers_column', '');
                })->orderBy('colorder', 'asc')->get(['id', 'name', 'comma_seprated' , 'default_value','round_off']);
            $all_childs =  getChildren($payroll->department);
            array_push($all_childs,$payroll->department);
            $employees = Employee::join('companystructures', 'companystructures.id', '=', 'employees.department')
                                ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                                ->join('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
                                ->join('payrolldata', 'payrolldata.employee', '=', 'employees.id')
                                ->where('payrolldata.payroll', $payroll->id);
                                // if(env('COMPANY') != 'RoofLine')
                                // {
                                //     $employees->whereIn('employees.department', $all_childs);
                                // }
                                $employees = $employees->select('employees.id', 'employees.first_name' , 'employees.middle_name' , 'employees.last_name', 'employees.employee_id', 'employees.employee_code','employees.nic_num','companystructures.title as department_name','jobtitles.name as job_title','employmentstatus.name as employment_status','employees.off_roll_date')->groupBy('payrolldata.employee')->get()
                                ->reject(function($payroll_employees,$key) use ($payroll) {
                                    if(env('COMPANY') == 'JSML'){
                                        if(!empty($payroll_employees->off_roll_date))
                                        {
                                            return $payroll_employees->off_roll_date <= $payroll->date_start;
                                        }
                                    }
                                });
            $total_columns = PayrollData::join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                                        ->where([
                                            ['payrolldata.payroll', $payroll->id],
                                            ['payrollcolumns.is_visible', 1],
                                            ['payrollcolumns.total_column', 1]
                                        ])->groupBy('payrollcolumns.id')->orderBy('colorder', 'asc')->get(['payrollcolumns.id', 'payrollcolumns.name',DB::raw('SUM(CASE WHEN payrolldata.amount > 0 THEN payrolldata.amount ELSE 0 END) as net_total')]);
            foreach ($total_columns as $key => $total) {
                $net_total[$total->id] = $total->net_total;
            }
            foreach ($employees as $key => $employee) {
                foreach ($payroll_columns as $key => $column) {
                    $payroll_data = PayrollData::where([
                        ['payroll', $payroll->id],
                        ['employee', $employee->id],
                        ['payroll_item', $column->id]
                    ])->first(['amount',DB::raw('SUM(amount) as net_total')]);
                    
                    $col_amount = !empty($payroll_data) ? $payroll_data->amount : $column->default_value;
                    if($column->comma_seprated == 1){
                        $column_amount[$employee->id][$column->id] = number_format($col_amount);
                    }
                    else{
                        $column_amount[$employee->id][$column->id] = $col_amount;
                    }
                    if($column->round_off == 1){
                        // $col_amount = !empty($payroll_data) ? round($payroll_data->amount,2) : $column->default_value;
                        $column_amount[$employee->id][$column->id] = round($col_amount);
                    }
                    if($column->round_off == 2){
                        $column_amount[$employee->id][$column->id] = round($col_amount,2);
                    }
                }
            }
            if (!file_exists(public_path().'/salarySheet')) {
                mkdir(public_path().'/salarySheet', 0777, true);
            }
            if(env('COMPANY') == 'CLINIX')
            {
                if(!empty($prev_payroll)){
                    $prev_total_columns = PayrollData::join('payrollcolumns', 'payrollcolumns.id', '=', 'payrolldata.payroll_item')
                            ->where([
                                ['payrolldata.payroll', $prev_payroll->id],
                                ['payrollcolumns.is_visible', 1],
                                ['payrollcolumns.total_column', 1]
                            ])->groupBy('payrollcolumns.id')->orderBy('colorder', 'asc')->get(['payrollcolumns.id', 'payrollcolumns.name',DB::raw('SUM(CASE WHEN payrolldata.amount > 0 THEN payrolldata.amount ELSE 0 END) as net_total')]);
                    foreach ($prev_total_columns as $key => $prev_total) {
                        $prev_net_total[$prev_total->id] = $prev_total->net_total;
                    }
                }
                $pdf = PDF::loadView('Admin.finance.payrollProcessing.clinix_salary_sheet_pdf', get_defined_vars())->setPaper('4A0', 'landscape')->save(public_path().'/salarySheet/salarySheet.pdf');
            }
            else
            {
                $pdf = PDF::loadView('Admin.finance.payrollProcessing.salary_sheet_pdf', get_defined_vars())->setPaper('4A0', 'landscape')->save(public_path().'/salarySheet/salarySheet.pdf');
            }
            return response(['code' => 200,'status' => "success",'filelink' => '/salarySheet/salarySheet.pdf']);
        }
        else{
            return response(['code' => 401,'status' => "failed",'message' => 'Salary Sheet InCompleted Or Not Found!']);
        }
    }

    public function downloadAttendanceSheet(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $month = $request->month_id;
        $year = $request->year_id;
        $start_date  = date('Y-m-01' , strtotime($year.'-'.$month));
        $end_date  = date('Y-m-t' , strtotime($year.'-'.$month));
        $dept = CompanyStructure::find($request->filter_department_id)->title;
        $month_days = cal_days_in_month(CAL_GREGORIAN, $request->month_id, $request->year_id);
        $employees = Employee::join('companystructures', 'companystructures.id', '=', 'employees.department')
                        ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                        ->where('employees.department' , $request->filter_department_id)->where('employees.status', 'Active');
                        if(env('COMPANY') == 'JSML'){
                            $employees->where('employees.seasonal_status' , 'On Roll');
                        }
                        $employees = $employees->orderBy('companystructures.title' , 'asc')->get(['employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name','employees.middle_name','employees.last_name','employees.joined_date','employees.suspension_date','employees.suspended','employees.restore_date','companystructures.title', 'jobtitles.name as desigination']);
        foreach ($employees as $key => $employe) {
            $empTotalLeaves = 0;
            $total_working_hours = 0;
            $total_days_present = 0;
            $total_manual_attendance = 0;
            $total_days_absent = 0;
            $total_days_late = 0;
            $total_overtime_hours = 0;
            $total_off_days = 0;
            $total_missing_checkout = 0;
            for ($date=$start_date; $date <= $end_date; $date++) {
                $shift_data = DB::table('shift_management')->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                    ->join('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
                    ->where([
                        ['shift_management.employee_id' , $employe->id],
                        ['shift_management.shift_from_date', '<=', $date],
                        ['shift_management.shift_to_date', '>=', $date]
                    ])
                    ->select('shift_management.work_week_id','shift_management.shift_id','shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc','shift_type.sandwich_absent','work_week.desc as work_week','shift_management.late_time_allowed')
                    ->orderBy('shift_management.id', 'DESC')
                    ->first();
                if(!empty($shift_data))
                {
                    $shift_desc = $shift_data->shift_desc;
                    $work_week_id = $shift_data->work_week_id;
                    $shift_start_time = $shift_data->shift_start_time;
                    $shift_end_time = $shift_data->shift_end_time;
                    $late_time_allowed = $shift_data->late_time_allowed;
                }
                else
                {
                    $shift_desc = 'Nil';
                    $shift_start_time = '09:00:00';
                    $shift_end_time = '17:00:00';
                    $late_time_allowed = 11;
                    $work_week_id = 1;
                }
                $shift_start_datetime = "$date ".$shift_start_time;
                $shift_end_datetime = "$date ".$shift_end_time;
                if (date('H', strtotime($shift_start_time)) > date('H', strtotime($shift_end_time))) {
                    $shift_end_datetime = date('Y-m-d H:i:s', strtotime($shift_end_datetime . ' +1 day'));
                }
                $short_leave = DB::table('short_leave_requests')->where([
                    ['employee_id', $employe->id],
                    ['date', "$date"],
                    ['status', "Approved"]
                ])->first();
                // if(!empty($short_leave))
                // {
                //     $employe_attendance[$employe->id][$date]['short_leave_time_from'] =  $short_leave->time_from;
                //     $employe_attendance[$employe->id][$date]['short_leave_time_to'] =  $short_leave->time_to;
                // }
                $attendance = DB::table('attendance')->where('employee', $employe->id)->whereDate('in_time', "$date")->orderBy('id', 'ASC')->first();
                
                if(!empty($attendance))
                {
                    if(empty($attendance->out_time)){
                        $total_missing_checkout = $total_missing_checkout+1;
                    }
                    if($attendance->is_manual == 1){
                        $total_manual_attendance = $total_manual_attendance + 1;
                    }
                    $total_days_present = $total_days_present+1;
                    $overtime_hours = '0.00';
                    $working_hours  =  '0.00';
                    $grace_period = date("H:i",strtotime("+$late_time_allowed minutes", strtotime($shift_start_time)));
                    if(!empty($attendance->in_time) && !empty($attendance->out_time))
                    {
                        $attend_time_in = $attendance->in_time;
                        $attend_time_out = $attendance->out_time;
                        $e_grace_period = date("Y-m-d H:i:s",strtotime("+$late_time_allowed minutes", strtotime($shift_start_datetime)));
                        if(strtotime($attend_time_in) < strtotime($e_grace_period)){
                            $attend_time_in = $shift_start_datetime;
                        }
                        if(strtotime($attend_time_out) > strtotime($shift_end_datetime)){
                            $attend_time_out = $shift_end_datetime;
                        }
                        $working_hours = Carbon::parse($attend_time_in)->floatDiffInHours($attend_time_out);
                        $overtime = DB::table('overtime_management')->where('employee_id', $employe->id)->where('overtime_date', "$date")->where('status', '=', 'Approved')->first(['overtime']);
                        if(!empty($overtime))
                        {
                            $overtime_hours = $overtime->overtime;
                            $total_overtime_hours = $total_overtime_hours + $overtime_hours;
                        }
                    }
                    $grace_period = date("H:i:s",strtotime("+$late_time_allowed minutes", strtotime($shift_start_time)));
                    if(date('H:i:s',strtotime($attendance->in_time)) > $grace_period)
                    {
                        $total_days_late = $total_days_late+1;
                    }
                    $employe_attendance[$employe->id][$date]['in_time'] =  date('H:i',strtotime($attendance->in_time));
                    if(env('COMPANY') == 'CLINIX'){
                        if($date >= '2023-02-01'){
                            $shiftEnd = date('Y-m-d H:i:s',strtotime($shift_end_datetime. ' + 20 minute'));
                            if(!empty($attendance->out_time) && $attendance->out_time <= $shiftEnd){
                                $employe_attendance[$employe->id][$date]['out_time'] = date('H:i',strtotime($attendance->out_time));
                                $total_working_hours = $total_working_hours + $working_hours;
                            }
                            else{
                                $working_hours = 0;
                                if(!empty($attendance->out_time)){
                                    $total_missing_checkout = $total_missing_checkout+1;
                                }
                                $total_working_hours = $total_working_hours + $working_hours;
                                $employe_attendance[$employe->id][$date]['out_time'] = 'Nil';
                            }
                            // $employe_attendance[$employe->id][$date]['out_time'] =  !empty($attendance->out_time) && $attendance->out_time <= $shiftEnd ? date('H:i',strtotime($attendance->out_time)) : 'Nil';
                        }
                        else{
                            $total_working_hours = $total_working_hours + $working_hours;
                            $employe_attendance[$employe->id][$date]['out_time'] =  !empty($attendance->out_time) ? date('H:i',strtotime($attendance->out_time)) : 'Nil';
                        }
                    }
                    else{
                        $total_working_hours = $total_working_hours + $working_hours;
                        $employe_attendance[$employe->id][$date]['out_time'] =  !empty($attendance->out_time) ? date('H:i',strtotime($attendance->out_time)) : 'Nil';
                    }
                    $employe_attendance[$employe->id][$date]['in_datetime'] =  $attendance->in_time;
                    $employe_attendance[$employe->id][$date]['out_datetime'] =  $attendance->out_time;
                    $employe_attendance[$employe->id][$date]['shift_start_time'] =  date('H:i',strtotime($shift_start_time));
                    $employe_attendance[$employe->id][$date]['shift_end_time'] =  date('H:i',strtotime($shift_end_time));
                    $employe_attendance[$employe->id][$date]['shift_desc'] =  $shift_desc;
                    $employe_attendance[$employe->id][$date]['is_manual'] =  $attendance->is_manual;
                    $employe_attendance[$employe->id][$date]['department_id'] =  $attendance->department_id;
                    $employe_attendance[$employe->id][$date]['grace_period'] =  $grace_period;
                    $employe_attendance[$employe->id][$date]['working_hours'] =  $working_hours;
                    $employe_attendance[$employe->id][$date]['total_working_hours'] =  $total_working_hours;
                    $employe_attendance[$employe->id][$date]['overtime_hours'] =  $overtime_hours;
                }
                else{
                    $absent_value = 'A';
                    $absent_value_color = 'red';
                    if($date > date('Y-m-d')){
                        $absent_value = '-';
                        $absent_value_color = 'grey';
                    }
                    $employee_leave = DB::table('employeeleaves')
                        ->join('leavetypes', 'leavetypes.id', '=','employeeleaves.leave_type')
                        ->where([
                        ['employee', $employe->id],
                        ['date_start', '<=', "$date"],
                        ['date_end', '>=', "$date"],
                        ['status', '=', 'Approved']
                    ])->first(['leavetypes.name as leave_type' , 'leavetypes.abbreviation' , 'leavetypes.leave_color']);
                    $work_week_days = DB::table('workdays')->where([
                        ['work_week_id', $work_week_id],
                        ['status', '=', 'Non-working Day']
                        ])->pluck('name')->toArray();
                    $previous_date = date('Y-m-d', strtotime(''.$date.' -1 day'));
                    $next_date = date('Y-m-d', strtotime(''.$date.' +1 day'));
                    if(!empty($shift_data) && $shift_data->sandwich_absent == 1){
                        $previous_attendance = DB::table('attendance')->where('employee', $employe->id)->whereDate('in_time', "$previous_date")->first(['id']);
                        // if($next_date <= date('Y-m-d'))
                        // {
                            $next_attendance = DB::table('attendance')->where('employee', $employe->id)->whereDate('in_time', "$next_date")->first(['id']);
                        // }
                        $employee_previous_leave = DB::table('employeeleaves')
                            ->join('leavetypes', 'leavetypes.id', '=','employeeleaves.leave_type')
                            ->where([
                            ['employee', $employe->id],
                            ['date_start', '<=', "$previous_date"],
                            ['date_end', '>=', "$previous_date"],
                            ['status', '=', 'Approved']
                        ])->first(['leavetypes.name as leave_type' , 'leavetypes.abbreviation' , 'leavetypes.leave_color']);
                        $employee_next_leave = DB::table('employeeleaves')
                            ->join('leavetypes', 'leavetypes.id', '=','employeeleaves.leave_type')
                            ->where([
                            ['employee', $employe->id],
                            ['date_start', '<=', "$next_date"],
                            ['date_end', '>=', "$next_date"],
                            ['status', '=', 'Approved']
                        ])->first(['leavetypes.name as leave_type' , 'leavetypes.abbreviation' , 'leavetypes.leave_color']);
                    }
                    if(in_array(date('l', strtotime("$date")), $work_week_days))
                    {
                        $absent_value = 'OFF';
                        $absent_value_color = '#28c76f';
                        if(!empty($shift_data) && $shift_data->sandwich_absent == 1)
                        {
                            if(empty($employee_previous_leave) && empty($employee_next_leave) && empty($previous_attendance) && empty($next_attendance))
                            {
                                $absent_value = 'A';
                                $absent_value_color = 'red';
                            }
                        }
                    }
                    if(!empty($employee_leave))
                    {
                        $absent_value = !empty($employee_leave->abbreviation) ? $employee_leave->abbreviation : 'L';
                        $absent_value_color = !empty($employee_leave->leave_color) ? $employee_leave->leave_color : 'green';
                        $empTotalLeaves = $empTotalLeaves + 1;
                    }
                    $holiday = DB::table('holidays')->where('dateh', "$date")->first();
                    if(!empty($holiday))
                    {
                        $absent_value = 'NH';
                        $absent_value_color = 'skyblue';
                        if(!empty($shift_data) && $shift_data->sandwich_absent == 1){
                            if(empty($employee_previous_leave) && empty($employee_next_leave) && empty($previous_attendance) && empty($next_attendance))
                            {
                                $absent_value = 'A';
                                $absent_value_color = 'red';
                            }
                        }
                    }
                    $checked_date = "$date";
                    if(strtotime($employe->joined_date) > strtotime($checked_date)){
                        $absent_value = '-';
                        $absent_value_color = 'grey';
                    }
                    if(!empty($employe->suspension_date) && $employe->suspension_date != '0000-00-00' && $employe->suspended == 1){
                        if(strtotime($employe->suspension_date) < strtotime($checked_date)){
                            $absent_value = '-' ;
                            $absent_value_color = 'grey';
                        }
                    }
                    if(!empty($employe->restore_date) && $employe->restore_date != '0000-00-00'){
                        if(strtotime($checked_date) < strtotime($employe->restore_date)){
                            $absent_value = '-' ;
                            $absent_value_color = 'grey';
                        }
                    }
                    if($absent_value == 'A')
                    {
                        $total_days_absent = $total_days_absent+1;
                    }
                    if($absent_value == 'OFF' || $absent_value == 'NH')
                    {
                        $total_off_days = $total_off_days+1;
                    }
                    $employe_attendance[$employe->id][$date]['absent_value'] =  $absent_value;
                    $employe_attendance[$employe->id][$date]['absent_value_color'] =  $absent_value_color;
                }
            }
            $employe_attendance[$employe->id]['out_time'] = $total_missing_checkout;
            $employe_attendance[$employe->id]['total_overtime_hours'] =  $total_overtime_hours;
            $employe_attendance[$employe->id]['total_overtime_hours'] =  $total_overtime_hours;
            $employe_attendance[$employe->id]['total_days_late'] =  $total_days_late;
            $employe_attendance[$employe->id]['total_working_hours'] =  $total_working_hours;
            $employe_attendance[$employe->id]['total_leaves'] =  $empTotalLeaves;
            $employe_attendance[$employe->id]['total_days_present'] =  $total_days_present;
            $employe_attendance[$employe->id]['total_days_absent'] =  $total_days_absent;
            $employe_attendance[$employe->id]['total_off_days'] =  $total_off_days;
            $employe_attendance[$employe->id]['total_manual_attendance'] =  $total_manual_attendance;
        }
        if (!file_exists(public_path().'/AttendanceSheet')) {
            mkdir(public_path().'/AttendanceSheet', 0777, true);
        }
        $pdf = PDF::loadView('Admin.attendanceReport.single_page_attendance_report_pdf', get_defined_vars())->setPaper('A2', 'landscape')->save(public_path().'/AttendanceSheet/AttendanceSheet.pdf');
        return response(['code' => 200,'status' => "success",'filelink' => '/AttendanceSheet/AttendanceSheet.pdf']);
    }

    public function updateLeavePeriod()
    {
        $employeeLeaves = EmployeeLeaverequest::get(['id', 'employee','leave_period','date_start','date_end']);
        foreach ($employeeLeaves as $key => $leave) {
            $leavePeriod = LeavePeriods::where([
                ['date_start','<=',$leave->date_start],
                ['date_end','>=',$leave->date_start],
            ])->first(['id']);
            if(!empty($leavePeriod)){
                $leave->leave_period = $leavePeriod->id;
                $leave->update();
            }
        }
        return response([
            'message' => 'Success',
            'code' => 200,
        ]);
    }

    public function updatePayrollData()
    {
        ini_set('max_execution_time', 0);
        $employees = Employee::join('payrolldata', 'payrolldata.employee', '=', 'employees.id')
                    ->join('companystructures', 'companystructures.id', '=', 'employees.department')
                    ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
                    ->join('employmentstatus', 'employmentstatus.id', '=', 'employees.employment_status')
            // ->where('employees.status', 'Active')
            // ->whereIn('payrolldata.payroll', $payroll_ids)
            // ->whereIn('employees.department', $request->departments)
            ->select('payrolldata.id as payrolldataId','employees.id','employees.department', 'employees.employment_status', 'employees.job_title', 'employees.bank_id','employees.bank_name','employees.account_number','employees.account_title','companystructures.title','jobtitles.name as jobtitlesName','employmentstatus.name as employmentstatusName')->groupBy('payrolldata.employee')->get();
        foreach ($employees as $key => $employee) {
            // $payrollData = PayrollData::find($employee->payrolldataId);
            // $payrollData->department_id = $employee->department;
            // $payrollData->employment_status = $employee->employment_status;
            // $payrollData->job_title = $employee->job_title;
            // $payrollData->bank_id = $employee->bank_id;
            // $payrollData->bank_name = $employee->bank_name;
            // $payrollData->account_number = $employee->account_number;
            // $payrollData->account_title = $employee->account_title;
            // $payrollData->update();
            DB::table('payrolldata')->where('employee', $employee->id)
            ->update(
                [
                    'department_id' => $employee->department,
                    'employment_status_id' => $employee->employment_status,
                    'job_title_id' => $employee->job_title,
                    'department' => $employee->title,
                    'employement_status' => $employee->employmentstatusName,
                    'job_title' => $employee->jobtitlesName,
                    'bank_id' => !empty($employee->bank_id) ? $employee->bank_id: NULL,
                    'bank_name' => !empty($employee->bank_name) ? $employee->bank_name : NULL,
                    'account_number' => !empty($employee->account_number) ? $employee->account_number : NULL,
                    'account_title' => !empty($employee->account_title) ? $employee->account_title : NULL
                ]
            );
        }
        return response([
            'message' => 'Success',
            'code' => 200,
        ]);
    }
}
