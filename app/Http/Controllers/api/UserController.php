<?php

namespace App\Http\Controllers\api;

use DB;
use Auth;
use App\Models\User;
use App\Models\CompanyStructure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function login(Request $request){
        try{
            $this->validate($request , [
                'email' => 'required|email',
                'password' => 'required',
            ]);
            $credentials = $request->only('email', 'password');
            $version = DB::table('app_version')->orderBy('id', 'DESC')->first();
            $allow_mac = DB::table('users')->where('email', $request->email)->where('allow_mac',1)->first();
            if($allow_mac==null){
                if(!empty($request->mac_address)){
                    $mac_addr = DB::table('users')->where('email', $request->email)->where('mac_address',$request->mac_address)->first();
                }
            }
            if($version->version == $request->version_name){
                if (Auth::attempt($credentials)) {
                    if(!empty($mac_addr) || $allow_mac){
                        $token = Auth::user()->createToken('Bearer Token')->plainTextToken;
                        $user = Auth::user();
                        if($user->hasRole('Admin')){
                            $permissions['all_permissions'] = Permission::pluck('name')->toArray();
                        }
                        else{
                            $permissions['all_permissions'] = Auth::user()->getAllPermissions()->pluck('name')->toArray();
                        }
                        
                        // $permissions['all_permissions'] = DB::table('permission')->join('role_permission', 'role_permission.permission_id','=','permission.id')
                        // ->join('role', 'role.id','=','role_permission.role_id')
                        // ->join('pivot', 'pivot.role_id','=','role.id')
                        // ->join('users', 'users.id','=','pivot.user_id')
                        // ->where('users.id', 1)->pluck('permission.key')->toArray();
                        $permissions['allowed_departments'] = CompanyStructure::whereIn('id', json_decode($user->user_departments))->get(['id', 'title']);
                        return ['code'=>'200','message' => 'Login Successfully','token' => $token,'data' => ['user'=>$user,'permissions' => $permissions]];
                    
                }else{
                    return ['code'=>'500','message' => 'Your Device is not Registered!'];
                }
            }
                return ['code'=>'500','message' => 'Invalid Credentials'];
            }
            else{
                return ['code'=>'500','message' => 'Please use '.$version->company_name.' HRM version '.$version->version];
            }
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','message'=>$e->getMessage()];
            }
        }
    }

    public function logout(){
        try{
            $user = Auth::user();
            $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
            return response([
                'message' => "Logout Successfuly",
            ]);
        }catch(Exception $e){
            return ['code'=>'500','message' => $e->getMessage()];
        }
    }
}
