<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaxReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        $data = [];
        $year = !empty(request()->date) ? date('Y' , strtotime(request()->date)) : date('Y');
        $month = !empty(request()->date) ? date('m' , strtotime(request()->date)) : date('m');

        $payrolls = DB::table('payroll')->whereYear('date_start' , $year)
        ->whereMonth('date_start' , $month)
        ->where('status' , 'completed')
        ->whereIn('department' , login_user_departments())
        ->select('id' , 'name' , 'department')
        ->orderBy('id' , 'asc')
        ->get();
        foreach($payrolls as $key => $payroll){
            $employees = DB::table('payrolldata')->join('payroll' , 'payroll.id' , '=' , 'payrolldata.payroll')
            ->join('employees' , 'employees.id' , '=' , 'payrolldata.employee')
            ->join('companystructures' , 'companystructures.id' , '=' , 'payroll.department')
            ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
            ->where('payrolldata.payroll' , $payroll->id)
            ->where('payrolldata.payroll_item' , 81)
            ->select('employees.id' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' , 'employees.last_name' , 'employees.nic_num' , 'companystructures.title' , 'payroll.id AS payroll_id' , 'payrolldata.amount AS tax_amount' , 'jobtitles.name AS designation')
            ->get();
            foreach($employees as $employee){
                if(!empty($employee->tax_amount)){
                    $gross_amount = DB::table('payrolldata')->where('payroll' , $employee->payroll_id)
                    ->where('employee' , $employee->id)
                    ->where('payroll_item' , 37)
                    ->select('amount AS gross_amount')
                    ->first();

                    $bank_details = DB::table('payrolldata')->where('payroll' , $employee->payroll_id)
                    ->where('employee' , $employee->id)
                    ->where('payroll_item' , 75)
                    ->select('amount AS account_details')
                    ->first();

                    $account_details = explode('-' , $bank_details->account_details);
    
                    if(isset($account_details[3]))
                    {
                        $account_details[3] = '-'.$account_details[3];
                    }
                    else{
                        $account_details[3] = '';
                    }

                    $employee->gross_amount = !empty($gross_amount) ? $gross_amount->gross_amount : 0;
                    $employee->accountName = $account_details[0]; 
                    $employee->bankName = $account_details[1];
                    $employee->accountNum = $account_details[2].$account_details[3];
                    $data[] = $employee;
                }
            }
        }
        return view('Admin.finance.tax_report.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
