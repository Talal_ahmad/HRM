<?php

namespace App\Http\Controllers;

use DataTables;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Models\Employee;
use App\Models\PaidLeave;
use App\Models\LeaveTypes;
use App\Models\LeavePeriods;
use App\Models\EmployeeLeaveBalance;
use Illuminate\Http\Request;
use App\Models\ShiftManagement;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\EmployeeLeaveRequest;
use Illuminate\Validation\ValidationException;

class EmployeeLeaveRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){ 
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        if($request->ajax()){
            $query = EmployeeLeaveRequest::leftjoin('employees' , 'employees.id' , '=' ,  'employeeleaves.employee')
            ->leftjoin('leavetypes' , 'leavetypes.id' , '=' , 'employeeleaves.leave_type')
            ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
            ->leftjoin('companystructures' , 'companystructures.id' , '=' , 'employees.department')
            ->whereIn('employees.department' , login_user_departments())
            ->select('employeeleaves.id','employeeleaves.date_start' , 'employeeleaves.date_end' , 'employeeleaves.created_at' , 'employeeleaves.status' , 'employeeleaves.remarks' , 'leavetypes.name AS leave_name' ,'employees.employee_id','employees.employee_code', 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'companystructures.title' , 'jobtitles.name as job_title_name');
            if (userRoles()=="Self Service") {
                $query->where('employeeleaves.employee' , Auth::user()->employee);
            }
            return DataTables::of($query)->filter(function ($query) use ($request) {
                if(!empty($request->departmentFilter)){
                    if($request->departmentFilter != 'all'){
                        if (env('COMPANY') != 'JSML') {
                            $query->where('employees.department', $request->departmentFilter);
                        }else{
                            $query->whereIn('employees.department', $request->section);
                        }
                    }
                }
                if (!empty($request->employeeFilter)) {
                    $query->where('employees.id', $request->employeeFilter);
                }
                if(!empty($request->leaveTypeFilter)){
                    $query->where('employeeleaves.leave_type', $request->leaveTypeFilter);
                }
                if(!empty($request->designationFilter)){
                    $query->whereIn('employees.job_title', $request->designationFilter);
                }
                if(!empty($request->statusFilter)){
                    $query->where('employeeleaves.status', $request->statusFilter);
                }
                if(env('COMPANY') == 'JSML' && !empty($request->is_half)){
                    $query->where('employeeleaves.is_half', $request->is_half);
                }
                if(!empty($request->fromDateFilter) && !empty($request->toDateFilter)){
                    $query->where([
                        ['employeeleaves.date_start', '>=', $request->fromDateFilter],
                        ['employeeleaves.date_end', '<=', $request->toDateFilter],
                    ]);
                }
                else{
                    $query->whereMonth('employeeleaves.date_start' , date('m'));
                }
            },true)->addIndexColumn()->make(true);
        }
        $leaveTypes = LeaveTypes::where('employee_can_apply' , 'Yes')->get();
        return view('Admin.request&approvals.leave_request.index' , compact('leaveTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'employee' => 'required',
                'leave_type' => 'required',
                'date_start' => 'required|date',
                'date_end' => 'required|date|after_or_equal:date_start',
                'remarks' => 'required',
            ]);
            if(env('COMPANY') == 'JSML'){
                if($request->filled('is_half')){
                    $employeeObj = Employee::find($request->employee);
                    if($employeeObj->employement_status == 9){
                        return ['code'=>'500','customMessage'=>'Half Leave Cannot Be Given To Employee Having Employement Status Daily Wages!'];
                    }
                }
            }
            $leavePeriod = LeavePeriods::where([
                ['status','=','Active'],
                ['date_start','<=',$request->date_start],
                ['date_end','>=',$request->date_end],
            ])->first(['id']);
            if(empty($leavePeriod)){
                return ['code'=>'500','customMessage'=>'Date does not match with leave period!'];
            }
            // $cpl_b = EmployeeLeaveBalance::where('emp_id',$request->input('employee'))->where('leave_type',$request->leave_type)->first();
            // if(!empty($cpl_b)){
            //     $balance = EmployeeLeaveBalance::where('emp_id',$request->input('employee'))->first('balance');
            // }else{
                $balance = checkLeaveBalance($request->employee,$request->leave_type,$leavePeriod->id);
            // }
            $leave_name = LeaveTypes::first('allow');
            $more_quote = LeaveTypes::first('more_quota');
            $leave_dup = EmployeeLeaverequest::where('employee', $request->employee)->orderBy('id', 'desc')->whereDate('date_start', $request->date_start)->first();
            if(!empty($balance['balance'])){
                $requesting_days = Carbon::parse(request('date_start'))->diffInDays(Carbon::parse(request('date_end'))) + 1;
                if($requesting_days > $balance['balance'] && $leave_name=='allow'){
                    return ['code' => 400 , 'customMessage' => 'This Employee Has '.$balance['balance'].' Leaves as Balance And Requesting Leaves are '.$requesting_days.''];
                }
            }
            else if($more_quote=='No'){
                return ['code' => 404 , 'customMessage' => 'This Employee Have 0 Leave Balance'];
            }
            $employee_shift = ShiftManagement::where('employee_id' , $request->employee)->select('shift_id')->first();
            if(empty($employee_shift)){
                return ['code' => 400 , 'customMessage' => 'Shift is not assigned to this Employee'];   
            }
            if(isset($leave_dup) && $leave_dup->status=='Approved'){
                return ['code' => 400, 'customMessage' => 'The Leave Request for this employee is already exist!'];
            }elseif (isset($leave_dup) && $leave_dup->status=='Pending') {
                return ['code' => 400, 'customMessage' => 'The Leave Request for this employee is already exist!'];
            }else{
                if ($more_quote->more_quota=='No') {
                    // if (env('COMPANY') == 'Ajmal Dawakhana') {
                    //     $employee_join = Employee::where('id',$request->input('employee'))->first('joined_date');
                    //     if (DATE_FORMAT(NOW(),'Y')==date('Y' , strtotime($employee_join->joined_date))) {
                    //         $this_month = Carbon::parse(DATE_FORMAT(NOW(),'Y-00'))->floorMonth(); // returns 2019-07-01
                    //         $start_month = Carbon::parse(date('Y-m' , strtotime($employee_join->joined_date)))->floorMonth(); // returns 2019-06-01
                    //         $diff = $this_month->diffInMonths($start_month);
                    //         $res_def = $diff;
                    //     }else{
                    //         $res_def=0;
                    //     }
                    // }else{
                    //     $res_def=0;
                    // }
                    // $final_balance = $balance['balance']-$res_def; commented because we are using global function
                    $balance = checkLeaveBalance($request->input('employee'), $request->input('leave_type'), $leavePeriod->id)['balance'];
                    if($balance>=$requesting_days){
                        $leave_request = new EmployeeLeaverequest();
                        $leave_request->employee = $request->input('employee');
                        $leave_request->leave_type = $request->input('leave_type');
                        // dd($request->input('leave_type'));
                        $leave_request->leave_period = $leavePeriod->id;
                        $leave_request->date_start = $request->input('date_start');
                        $leave_request->date_end = $request->input('date_end');
                        $leave_request->remarks = $request->input('remarks');
                        if($request->status){
                            $leave_request->status = $request->input('status');
                            $leave_request->status_changed_by = Auth::id();     
                            $leave_request->status_changed_at = Carbon::now();                 
                        }
                        if($request->filled('is_half')){
                            $leave_request->is_half = 1;
                        }
                        $leave_request->submitted_by = Auth::id();
                        $leave_request->shift_id = $employee_shift->shift_id;
                        $leave_request->save();
                        return ['code'=>'200','message'=>'success'];
                    }else{
                        return ['code' => 422 , 'error' => 'error'];
                    }
                }else{
                    $leave_request = new EmployeeLeaverequest();
                        $leave_request->employee = $request->input('employee');
                        $leave_request->leave_type = $request->input('leave_type');
                        $leave_request->leave_period = $leavePeriod->id;
                        $leave_request->date_start = $request->input('date_start');
                        $leave_request->date_end = $request->input('date_end');
                        $leave_request->remarks = $request->input('remarks');
                        if($request->status){
                            $leave_request->status = $request->input('status');
                            $leave_request->status_changed_by = Auth::id();     
                            $leave_request->status_changed_at = Carbon::now();                 
                        }
                        if($request->filled('is_half')){
                            $leave_request->is_half = 1;
                        }
                        if($request->hasFile('attachment')){
                            $file = $request->file('attachment');
                            $extension = $file->getClientOriginalExtension();
                            $temp_name = time(). "." . rand(000 , 999) . "." .$extension;
                            $file->move('images/leaves_request/' , $temp_name);
                            $leave_request->attachment = $temp_name;
                        }
                        $leave_request->submitted_by = Auth::id();
                        $leave_request->shift_id = $employee_shift->shift_id;
                        $leave_request->save();
                        return ['code'=>'200','message'=>'success'];
                }
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeLeaveRequest  $employeeLeaveRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $leave = EmployeeLeaveRequest::find($id);
        $leave_name = LeaveTypes::find($leave->leave_type)->name;
        $leave_history = checkLeaveHistory($leave->employee , $leave->leave_type , $leave->date_start , $leave->date_end);

        return response([
            'leave_name' => $leave_name,
            'leave_history' => $leave_history
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeLeaveRequest  $employeeLeaveRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeLeaveRequest $employeeLeaveRequest)
    {
        if(request()->type == 'status'){
            return response()->json($employeeLeaveRequest);
        }
        else
        {
            $employee = Employee::where('id', $employeeLeaveRequest->employee)->select('first_name', 'last_name')->first();
            $period = CarbonPeriod::create($employeeLeaveRequest->date_start,  $employeeLeaveRequest->date_end);
            foreach ($period as $date) {
                $listOfDates[] = $date->format('Y-m-d');
            }
            $paid_leaves = PaidLeave::leftjoin('employeeleaves' , 'employeeleaves.id' , '=' , 'paid_leave.leave_id')
            ->where('paid_leave.leave_id' , $employeeLeaveRequest->id)
            ->pluck('paid_leave.leave_date');
            return response()->json([
                'employee' => $employee,
                'listOfDates' => $listOfDates,
                'paid_leaves' => $paid_leaves
            ]);
        }
    }

    /**
     * Update the specified reso-+urce in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeLeaveRequest  $employeeLeaveRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeLeaveRequest $employeeLeaveRequest)
    {
        try{
            if($request->type == 'status'){
                $this->validate($request , [
                    'status' => 'required',
                ]);

                $employeeLeaveRequest->status = $request->status;
                $employeeLeaveRequest->status_changed_by = Auth::id();     
                $employeeLeaveRequest->status_changed_at = Carbon::now();   
                $employeeLeaveRequest->update();
            }
            else{
                if(!empty($request->leave_date))
                {
                    foreach($request->leave_date as $date){
                        $leave = PaidLeave::where('leave_id', $employeeLeaveRequest->id)->where('leave_date', $date)->first();
                        if(empty($leave))
                        {
                            $paid_leave = new PaidLeave();
                            $paid_leave->leave_id = $employeeLeaveRequest->id;
                            $paid_leave->leave_date = $date;
                            $paid_leave->save();
                        }
                    }
                    $employeeLeaveRequest->is_paid = 1;
                    $employeeLeaveRequest->update();
                }
                else
                {
                    $employeeLeaveRequest->is_paid = 0;
                    $employeeLeaveRequest->update();
                    PaidLeave::where('leave_id', $employeeLeaveRequest->id)->delete();
                }
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeLeaveRequest  $employeeLeaveRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeLeaveRequest $employeeLeaveRequest)
    {
        //
    }

    public function getLeaveBalance(Request $request){
        $leave_quota = LeaveTypes::first('more_quota');
        $activePeriod = LeavePeriods::where('status', 'Active')->first();
        $leave_name = LeaveTypes::first('allow','more_quota');
        $employee_join = Employee::where('id',$request->input('employee'))->first('joined_date');
        $joined_date = DB::table('employees')
        ->where('id', $request->employee)
        ->pluck('joined_date')
        ->first(); 
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', date("Y-m-d"));
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $joined_date);
        $diff_in_days = $to->diffInDays($from);
        $joined_days_allow = LeaveTypes::where('id',$request->leave_type)->first('joining_date_allow');

        // dd($leave_name->joining_date_allow<$diff_in_days);
        if (env('COMPANY') == 'Ajmal Dawakhana') {
            if (DATE_FORMAT(NOW(),'Y')==date('Y' , strtotime($employee_join->joined_date))) {
                $this_month = Carbon::parse(DATE_FORMAT(NOW(),'Y-00'))->floorMonth(); // returns 2019-07-01
                $start_month = Carbon::parse(date('Y-m' , strtotime($employee_join->joined_date)))->floorMonth(); // returns 2019-06-01
                $diff = $this_month->diffInMonths($start_month);
                $res_def = $diff;
            }else{
                $res_def='';
            }
        }else{
            $res_def='';
        }

        if(empty($activePeriod)){
            return ['code' => '500' , 'errorMessage' => 'Leave Period is not active!'];
        }
        return ['code' => 200 ,'quota' => $leave_quota,'difference' => $res_def,'leave'=>$leave_name,'diff_in_days'=>$diff_in_days,'joined_days_allow'=>$joined_days_allow,'data' => checkLeaveBalance($request->employee,$request->leave_type,$activePeriod->id)];
    }

    public function getLeaveBalanceLogs(Request $request){
        $employee = Employee::find($request->employee);
        $settingsIds = $employee->newEmployeesLeavesSettings()->where('leave_type_id', $request->leaveType)->pluck('id')->toArray();
        return DB::table('audits')
            ->join('users', 'audits.user_id', '=', 'users.id')
            ->select('audits.*', 'users.username', 'users.email', 'users.user_level')
            ->where('audits.auditable_type', 'like', '%NewEmployeesLeavesSetting%')
            ->whereIn('audits.auditable_id', $settingsIds)
            ->get();
    }

    public function editLeaveBalance(Request $request){
        return DB::table('employee_leaves_balance')
            ->where('emp_id', $request->employee)
            ->where('leave_type', $request->leaveType)
            ->whereNull('deleted_at')
            ->latest()
            ->first();
    }

    public function updateLeaveBalance(Request $request){
        try{
            if($request->filled('balance')){
                EmployeeLeaveBalance::where('emp_id', $request->employee)
                    ->where('leave_type', $request->leaveType)
                    ->update([
                        'balance' => $request->balance,
                    ]);
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception){
            return ['code' => 500 , 'error_message' => $e->getMessage()];
        }
    }

    public function newEmployeeLeaveBalance(Request $request){
        return calculateLeavesForNewEmployee($request->leave_groups, $request->calculation_date);
    }
}
