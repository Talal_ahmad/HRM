<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\JobTitle;
use App\Models\Employee;
use App\Models\Holidays;
use App\Models\WorkDay;
use App\Models\Attendance;
use Illuminate\Http\Request;
use App\Models\ShiftManagement;
use Illuminate\Support\Facades\DB;
use App\Models\EmployeeLeaveRequest;
use Illuminate\Validation\ValidationException;

class BulkAttendanceJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        
        return view('Admin.timeManagement.bulkAttendanceJobWise.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dates = [];
        $from_month = date('m', strtotime(request()->from_date));
        $to_month = date('m', strtotime(request()->to_date));
        $employ = Employee::join('companystructures', 'companystructures.id', '=', 'employees.department')->where('employees.id', request()->employee_id)->select('employees.id','employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name' ,'employees.last_name', 'companystructures.title')->first();
        if($to_month > $from_month)
        {
            return redirect('bulk_attendance_job_wise/create')->with(['message' => 'You can select dates only in the same month']);
        }
        if(!empty(request()->from_date) && !empty(request()->to_date))
        {
            for($i = request()->from_date; $i <= request()->to_date; $i++){
                $obj = new \stdclass();
                $obj->date = $i;
                $shift_data = ShiftManagement::join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                    ->join('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
                    ->where([
                        ['shift_management.employee_id' , request()->employee_id],
                        ['shift_management.shift_from_date', '<=', $i],
                        ['shift_management.shift_to_date', '>=', $i]
                    ])
                    ->select('shift_management.work_week_id','shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc','work_week.desc')
                    ->orderBy('shift_management.id', 'DESC')
                    ->first();
                if(!empty($shift_data))
                {
                    $employee_leave = EmployeeLeaveRequest::where([
                        ['employee', request()->employee_id],
                        ['date_start', '<=', $i],
                        ['date_end', '>=', $i],
                        ['status', '=', 'Approved'],
                    ])->first();
                    if(!empty($employee_leave))
                    {
                        continue;
                    }
                    // Check Off Days
                    $off_day = '';
                    $leave_days = WorkDay::where('work_week_id', $shift_data->work_week_id)->where('status', '=', 'Non-working Day')->get();
                    if(count($leave_days) > 0)
                    {
                        $current_day_name = date('l', strtotime("$i"));
                        foreach ($leave_days as $key => $value) {
                            if($value->name == $current_day_name)
                            {
                                $off_day = 'Off Day';
                            }
                        }
                        if(!empty($off_day))
                        {
                            continue;
                        }
                    }
                    $holiday = Holidays::where('dateh', '=', $i)->first();
                    if(!empty($holiday))
                    {
                        continue;
                    }
                    $obj->shift_start_time = $shift_data->shift_start_time;
                    $obj->shift_end_time = $shift_data->shift_end_time;
                    $obj->shift_name = $shift_data->shift_desc;
                    $obj->work_week = $shift_data->desc;
                    $dates[] = $obj;
                }
                else{
                    return redirect('bulk_attendance_job_wise/create')->with(['message' => 'No shift assigned to this employee Please first assign shift']);
                }
            }
        }
        return view('Admin.timeManagement.bulk_attendance', compact('dates', 'employ'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'job' => 'required',
                'date' => 'required',
                'time_in' => 'required',
                'time_out' => 'required',
            ]);
            $employees = Employee::where('job_title' , $request->job)->get();
            foreach($employees as $employee){
                $attendance = DB::table('attendance')->where('employee', $employee->id)->whereDate('in_time', $request->date)->first();
                if(empty($attendance))
                {
                    $shift_data = ShiftManagement::leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                    ->where([
                        ['shift_management.employee_id' , $employee->id],
                        ['shift_management.shift_from_date', '<=', $request->date],
                        ['shift_management.shift_to_date', '>=', $request->date]
                    ])
                    ->select('shift_management.*','shift_type.shift_start_time', 'shift_type.shift_end_time')
                    ->orderBy('shift_management.id', 'DESC')
                    ->first();
                    if(!empty($shift_data))
                    {
                        $shift_id = $shift_data->shift_id;
                        $shift_start_time = $shift_data->shift_start_time;
                        $shift_end_time = $shift_data->shift_end_time;
                        $work_week_id = $shift_data->work_week_id;
                        $late_time_allowed = $shift_data->late_time_allowed;
                    }
                    else
                    {
                        $shift_id = 1;
                        $shift_start_time = '09:00:00';
                        $shift_end_time = '17:00:00';
                        $work_week_id = 1;
                        $late_time_allowed = 11;
                    }
                    DB::table('attendance')->insert([
                        'employee' => $employee->id,
                        'department_id' => $employee->department,
                        'shift_id' => $shift_id,
                        'shift_start_time' => $shift_start_time,
                        'shift_end_time' => $shift_end_time,
                        'work_week_id' => $work_week_id,
                        'late_time_allowed' => $late_time_allowed,
                        'in_time' => $request->date.' '.$request->time_in,
                        'out_time' => $request->date.' '.$request->time_out,
                        'is_manual' => 1,
                        'is_online' => 0
                    ]);
                }
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::where('id', $id)->select('department')->first();
        foreach ($request->dates as $key => $date) {
            $shift_data = ShiftManagement::leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                ->where([
                    ['shift_management.employee_id' , $id],
                    ['shift_management.shift_from_date', '<=', $date],
                    ['shift_management.shift_to_date', '>=', $date]
                ])
                ->select('shift_management.*','shift_type.shift_start_time', 'shift_type.shift_end_time')
                ->orderBy('shift_management.id', 'DESC')
                ->first();
            if(!empty($shift_data))
            {
                $shift_id = $shift_data->shift_id;
                $shift_start_time = $shift_data->shift_start_time;
                $shift_end_time = $shift_data->shift_end_time;
                $work_week_id = $shift_data->work_week_id;
                $late_time_allowed = $shift_data->late_time_allowed;
            }
            else
            {
                $shift_id = 1;
                $shift_start_time = '09:00:00';
                $shift_end_time = '17:00:00';
                $work_week_id = 1;
                $late_time_allowed = 11;
            }
            $attendance = Attendance::where('employee', $id)->whereDate('in_time', $date)->first();
            if(!empty($attendance))
            {
                $attendance->delete();
            }
            if(!isset($request->od)){
                $request->od = 0;
            }
            if(!isset($request->ret)){
                $request->ret = 0;
            }
            $in_datetime = $request->time_in[$date];
            $out_datetime = $request->time_out[$date];
            $attendance = new Attendance();
            $attendance->employee = $id;
            $attendance->in_time = $in_datetime;
            $attendance->out_time = $out_datetime;
            $attendance->department_id = $employee->department;
            $attendance->is_online = 1;
            $attendance->is_manual = 1;
            $attendance->shift_id = $shift_id;
            $attendance->shift_start_time = $shift_start_time;
            $attendance->shift_end_time = $shift_end_time;
            $attendance->work_week_id = $work_week_id;
            $attendance->late_time_allowed = $late_time_allowed;
            $attendance->od = $request->od;
            $attendance->ret = $request->ret;
            $attendance->note = 0;
            $attendance->save();
        }
        return redirect('bulk_attendance_job_wise/create')->with(['success_message' => 'Attendance Marked Successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
