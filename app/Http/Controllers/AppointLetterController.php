<?php

namespace App\Http\Controllers;
use App\Models\Employee;
use App\Models\AppointmentLetters;
use App\Models\CompanyStructure;
use App\Models\EnvChecks;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use DB;
class AppointLetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return Datatables::eloquent(AppointmentLetters::query()->where('status',null))->addIndexColumn()->make(true);
        }
        return view('Admin.employees.employee_appoint_letter.index');
    }
    public function print_letter(Request $request)
    {
        $template_check_1 = EnvChecks::where('name','Template 1')->where('key',1)->first();
        $template_check_2 = EnvChecks::where('name','Template 2')->where('key',1)->first();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName' , 'e1.last_name AS gm_lName']);
        return view('Admin.employees.employee_appoint_letter.create', get_defined_vars());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $print = AppointmentLetters::where('id',$id)->first();
        $company_setup = DB::table('company_setup')->leftjoin('employees' , 'employees.id' , '=' , 'company_setup.hr')
        ->leftjoin('employees AS e1' , 'e1.id' , '=' , 'company_setup.gm')
        ->first(['company_setup.*' , 'employees.first_name' , 'employees.last_name' , 'e1.first_name AS gm_fName' , 'e1.last_name AS gm_lName']);
        return view('Admin.employees.employee_appoint_letter.print', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                // 'employee' => 'unique:appointment_letter_records',
                'name' => 'required',
                'department' => 'required',
                'date_of_print' => 'required',
                'salary' => 'required',
                'note' => 'required',
            ]);
            $department = CompanyStructure::where('id',$request->department)->first('title');
            $appoint_records = new AppointmentLetters();
            $appoint_records->employee = $request->employee;
            $appoint_records->name = $request->name;
            $appoint_records->f_name = $request->f_name;
            $appoint_records->department = $department->title;
            $appoint_records->desigination = $request->desigination;
            $appoint_records->date_of_record = $request->date_of_print;
            $appoint_records->salary = $request->salary;
            $appoint_records->note = $request->note;
            $appoint_records->save();
            return ['code' => 200 , 'message' => 'Success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employees = Employee::where('employees.id',$id)->whereIn('employees.department', login_user_departments())
        ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')->first(['employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name' , 'employees.last_name', 'employees.father_name','jobtitles.name as desigination']);
        return response()->json(['employees_data' => $employees]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $letter = AppointmentLetters::find($id);
            $letter->status = "Cancelled";
            $letter->remarks = $request->remarks;
            $letter->cancelled_by = Auth::id();
            $letter->update();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
