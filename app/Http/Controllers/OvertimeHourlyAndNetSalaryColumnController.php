<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\OvertimeHourlyAndNetSalaryColumn;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class OvertimeHourlyAndNetSalaryColumnController extends Controller
{ 
    public function __construct(){ 
        $this->middleware('auth');
    }

    public function index(Request $request)   
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @rownum=0'));
            $data = OvertimeHourlyAndNetSalaryColumn::leftjoin('salarycomponent', 'salarycomponent.id', '=', 'overtime_hourly_paycolumn.overtime_salary_component_id')->leftjoin('payrollcolumns', 'payrollcolumns.id', '=', 'overtime_hourly_paycolumn.net_salary_column')->select('overtime_hourly_paycolumn.id', 'salarycomponent.name as overtime_salary_component_id', 'payrollcolumns.name as net_salary_column',DB::raw('@rownum  := @rownum  + 1 AS rownum'));
            return DataTables::eloquent($data)->make(true); 
        }

        $salary_component = DB::table('salarycomponent')->get();
        $payroll_columns = DB::table('payrollcolumns')->select('id', 'name')->get();
        return view('Admin.finance.OvertimeHourlyAndNetSalaryColumn.index', get_defined_vars());    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'overtime_salary_component_id' => 'required',
                'net_salary_column' => 'required',
            ]);
            
            $OvertimeHourlyAndNetSalaryColumn = new OvertimeHourlyAndNetSalaryColumn;
            $OvertimeHourlyAndNetSalaryColumn->overtime_salary_component_id = $request->overtime_salary_component_id;
            $OvertimeHourlyAndNetSalaryColumn->net_salary_column = $request->net_salary_column;
            $OvertimeHourlyAndNetSalaryColumn->save();
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        $data = OvertimeHourlyAndNetSalaryColumn::find($id);
        return response()->json($data);     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'overtime_salary_component_id' => 'required',
                'net_salary_column' => 'required',
            ]);

            $OvertimeHourlyAndNetSalaryColumn = OvertimeHourlyAndNetSalaryColumn::find($id);
            $OvertimeHourlyAndNetSalaryColumn->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $OvertimeHourlyAndNetSalaryColumn = OvertimeHourlyAndNetSalaryColumn::find($id)->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }catch(\Exception $e){
            return ['code' => 500 , 'error_message' => $e->getMessage()]; 
        }
    }
}