<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\DataImporter;
use Illuminate\Http\Request;
use App\Models\DataImportFile;
use Illuminate\Support\Facades\DB; 
use Illuminate\Validation\ValidationException;

class DataImportController extends Controller
{
    public function __construct(){   
        $this->middleware('auth');
    }

    public function index(Request $request) 
    {
        if($request->ajax()){
            // data importers
            if($request->type == 'data_importers'){
                return Datatables::eloquent(DataImporter::query())->make(true); 
            }
            // data import files 
            else{
                return Datatables::eloquent(DataImportFile::leftjoin('dataimport','dataimportfiles.data_import_definition','=','dataimport.id')->select('dataimportfiles.name','dataimportfiles.status','dataimport.name AS dataImportDefinition'))->make(true);
            }
        }

        $data_importer = DataImporter::get(); 
        return view('Admin.setup.dataImport.index', get_defined_vars());  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Store Data Importers
            if($request->type == 'data_importers'){
                try{
                    $this->validate($request, [
                        'name' => 'required',
                        'dataType' => 'required',


                    ]);
                    $name                                       = ($request->name);
                    $data_type                                  = ($request->dataType);
                    $details                                    = ($request->details);
                    $add_column_name                            = ($request->add_column_name);
                    $add_column_title                           = ($request->add_column_title);
                    $add_column_type                            = ($request->add_column_type);
                    $add_column_depends_on                      = ($request->add_column_depends_on);
                    $add_column_depends_on_field                = ($request->add_column_depends_on_field);
                    $add_column_key_field                       = ($request->add_column_key_field);
                    $add_column_id_field                        = ($request->add_column_id_field);
                    $limit_array = array();
                    array_push($limit_array, $add_column_name);
                    array_push($limit_array, $add_column_title);
                    array_push($limit_array, $add_column_type);
                    array_push($limit_array, $add_column_depends_on);
                    array_push($limit_array, $add_column_depends_on_field);
                    array_push($limit_array, $add_column_key_field);
                    array_push($limit_array, $add_column_id_field);
                    $limit_array = json_encode($limit_array); 


                    $DataImporter = new DataImporter;
                    $DataImporter->name = $name;
                    $DataImporter->dataType = $data_type;
                    $DataImporter->details = $details;
                    $DataImporter->columns = $limit_array;
                    $DataImporter->save();
                    return ['code'=>'200','message'=>'success'];
                } 
                catch(\Exception | ValidationException $e){
                    if($e instanceof ValidationException){
                        return ['code'=>'200','errors' => $e->errors()]; 
                    }
                    else{
                        return ['code'=>'200','error_message'=>$e->getMessage()];
                    }
                }
            }
            // Data Import Files
            else
            {
                $this->validate($request, [
                    'name' => 'required',
                    'data_import_definition' => 'required',
                    'file' => 'required',                    
                ]);
                DataImportFile::create($request->all());
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        // $salary_components = SalaryComponent::get();
        // $employees = Employee::where('department', $id)->select('id', 'employee_id', 'employee_code', 'first_name', 'last_name')->get();
        // foreach ($employees as $key => $employee) {
        //     foreach ($salary_components as $component) {
        //         $employee_component = EmployeeSalaryComponent::where('employee', $employee->id)->where('component', $component->id)->select('employee','component', 'amount')->first();
        //         $data[$employee->id][$component->id]['component'] = $employee_component;
        //     }
        //     // $employee_component = EmployeeSalaryComponent::where('employee', $employee->id)->orderBy('component', 'ASC')->select('id', 'employee', 'component', 'amount')->get();
        //     // dd($data);
        // }
        // return response()->json(['employees' => $employees, 'data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        // Edit Data Importers
        if($request->type == 'data_importers'){
            $DataImporter = DataImporter::find($id);
            return response()->json($DataImporter);
        }
        // Edit Data Import Files
        else{
            $DataImportFile = DataImportFile::find($id);
            return response()->json($DataImportFile);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            //update data importers
            if($request->type == 'data_importers'){
                $this->validate($request, [
                    'name' => 'required',
                    'dataType' => 'required',
                ]);

                $DataImporter = DataImporter::find($id);
                $DataImporter->fill($request->all())->save();
            }
            //update data import files
            else
            {
                $this->validate($request, [
                    'name' => 'required',
                    'data_import_definition' => 'required',
                    'file' => 'required',
                ]);
                $DataImportFile = DataImportFile::find($id);
                $DataImportFile->fill($request->all())->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e)
        {
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            if($request->type == 'data_importers'){
                $data_importer = DataImporter::find($id);
                $data_importer->deleted_by = Auth::id();
                $data_importer->update();
                $data_importer->delete();
            }
            // data import files
            else
            {
                $data_importer_file = DataImporter::find($id);
                $data_importer_file->deleted_by = Auth::id();
                $data_importer_file->update();
                $data_importer_file->delete();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
