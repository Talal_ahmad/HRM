<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\Legal;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class LegalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            $query = DB::table('legals')
            ->join('employees' , 'employees.id' , '=' , 'legals.employee_id')
            ->select('legals.legal_action' , 'legals.title' , 'legals.legal_counsel_company' , 'legals.legal_counsel_employee' , 'legals.case_filed_on' , 'legals.court' , 'legals.case_type' , 'legals.petition_case_copy' , 'employees.first_name' , 'employees.last_name')
            ->get();
            return DataTables::of($query)
            ->make(true);
        }
        $employees = DB::table('employees')->get();
        return view('Admin.setup.legal.index' , compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'legal_action' => 'required',
                'title' => 'required',
                'legal_counsel_company' => 'required',
                'employee_id' => 'required',
                'legal_counsel_employee' => 'required',
                'case_filed_on' => 'required',
                'court' => 'required',
                'case_type' => 'required',
                'petition_case_copy' => 'required',
            ]);
            $data = new Legal();
            $data->legal_action = $request->input('legal_action');
            $data->title = $request->input('title');
            $data->legal_counsel_company = $request->input('legal_counsel_company');
            $data->employee_id = $request->input('employee_id');
            $data->legal_counsel_employee = $request->input('legal_counsel_employee');
            $data->case_filed_on = $request->input('case_filed_on');
            $data->court = $request->input('court');
            $data->case_type = $request->input('case_type');
            
            if($request->hasFile('petition_case_copy')){
                $file = $request->file('petition_case_copy');
                $extension = $file->getClientOriginalExtension();
                $temp_name = time(). "." .$extension;
                $file->move('images/legals/' , $temp_name);
                $data->petition_case_copy = $temp_name;
            }
            $data->save();
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Legal  $legal
     * @return \Illuminate\Http\Response
     */
    public function show(Legal $legal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Legal  $legal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Legal::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Legal  $legal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        try{
            $this->validate($request , [
                'legal_action' => 'required',
                'title' => 'required',
                'legal_counsel_company' => 'required',
                'employee_id' => 'required',
                'legal_counsel_employee' => 'required',
                'case_filed_on' => 'required',
                'court' => 'required',
                'case_type' => 'required',
            ]);
            $data = Legal::find($id);
            $data->legal_action = $request->input('legal_action');
            $data->title = $request->input('title');
            $data->legal_counsel_company = $request->input('legal_counsel_company');
            $data->employee_id = $request->input('employee_id');
            $data->legal_counsel_employee = $request->input('legal_counsel_employee');
            $data->case_filed_on = $request->input('case_filed_on');
            $data->court = $request->input('court');
            $data->case_type = $request->input('case_type');
            if($request->hasFile('petition_case_copy')){
                unlink('images/legals/'.$data->petition_case_copy);
                $file = $request->file('petition_case_copy');
                $extension = $file->getClientOriginalExtension();
                $temp_name = time(). "." .$extension;
                $file->move('images/legals/' , $temp_name);
                $data->petition_case_copy = $temp_name;
            }
            $image = $data->petition_case_copy;
            $data->petition_case_copy = $image;
            $data->save();
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Legal  $legal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $legal = Legal::find($id);
            if($legal->petition_case_copy){
                $image = $legal->petition_case_copy;
                unlink('images/legals/'.$image);
            }
            $legal->deleted_by = Auth::id();
            $legal->update();
            $legal->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
