<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\PayrollEmployee;
use App\Models\SalaryComponent;
use App\Models\CalculationGroup;
use App\Models\LeaveTypes;
use App\Models\CompanyStructure;
use Illuminate\Support\Facades\DB;
use App\Models\SalaryComponentType;
use App\Models\EmployeeSalaryComponent;
use App\Models\Tax;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class SalarySetupController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user_calculation_groups = !empty(Auth::user()->user_calculation_groups) ? json_decode(Auth::user()->user_calculation_groups) : [];
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            // Salary Component Types
            if($request->type == 'salaryComponentTypes'){
                return Datatables::eloquent(SalaryComponentType::query())->addIndexColumn()->make(true);
            }
            // Salary Components
            else
            {
                return DataTables::eloquent(SalaryComponent::leftjoin('salarycomponenttype','salarycomponent.componentType','=','salarycomponenttype.id')->leftjoin('taxes','salarycomponent.tax_id','=','taxes.id')->select('salarycomponent.id','salarycomponent.name','salarycomponent.details','salarycomponenttype.name as type_name' , 'salarycomponent.is_monthly','taxes.code as tax_code' ))->addIndexColumn()->make(true);
            }
        }
        $salary_components = SalaryComponent::get();
        if(env('COMPANY') != 'CLINIX' || isset($request->departmentFilter)){
            $query = PayrollEmployee::join('employees', 'payrollemployees.employee', '=', 'employees.id')
            ->leftjoin('companystructures', 'companystructures.id', '=', 'employees.department')
            ->leftjoin('jobtitles', 'jobtitles.id', '=', 'employees.job_title')
            ->leftjoin('deductiongroup', 'deductiongroup.id', '=', 'payrollemployees.deduction_group');
            if(env('COMPANY') == 'JSML'){
                $query->whereIn('employees.status', ['Active','Terminated'])->whereIn('employees.suspended', [0,1])->whereIn('employees.seasonal_status' , ['On Roll', 'Off Roll']);
            }
            else{
                $query->where('employees.status', 'Active');
            }
            $query = $query->groupBy('employees.id')->whereIn('employees.department', login_user_departments())
            ->whereIn('payrollemployees.deduction_group', $this->user_calculation_groups)
            ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.joined_date', 'employees.first_name', 'employees.last_name' , 'employees.middle_name' , 'payrollemployees.deduction_group', 'companystructures.title', 'companystructures.type', 'jobtitles.name as desigination', 'deductiongroup.name as deduction_group','employees.off_roll_date', DB::raw('GROUP_CONCAT(deductiongroup.name) as deduction_group_name'));
            if(!empty($request->employeeFilter)){
                $query->where('employees.id' , $request->employeeFilter);
            }
            if(isset($request->departmentFilter) && $request->departmentFilter != 'all'){
                if (env('COMPANY') != 'JSML') {
                    $query->where('employees.department' , $request->departmentFilter);
                }else{
                    $query->whereIn('employees.department' , $request->section);
                }
            }
            if(!empty($request->designationFilter)){
                $query->where('employees.job_title' , $request->designationFilter);
            }
            $employees = $query->get()
            ->reject(function($employees) {
                if(env('COMPANY') == 'JSML'){
                    if(!empty($employees->off_roll_date)){
                        return $employees->off_roll_date < date("Y-m-d", strtotime("first day of previous month"));
                    }
                }
            });
            foreach ($employees as $employee) {
                foreach ($salary_components as $component) {
                    $employee_component = EmployeeSalaryComponent::where('employee', $employee->id)->where('component', $component->id)->first();
                    $data[$employee->id][$component->id] = !empty($employee_component) ? $employee_component->amount : 0.00;
                }
            }
            $filterEmployees = Employee::select('id','employee_id', 'employee_code', 'first_name','last_name');
            if(isset($request->departmentFilter) && $request->departmentFilter != 'all'){
                $filterEmployees->where('department', $request->departmentFilter);
            }
            $filterEmployees = $filterEmployees->get();
        }
        $salary_component_types = SalaryComponentType::get();
        $calculationGroups = CalculationGroup::get(['id', 'name']);
        $employment_status = DB::table('employmentstatus')->get(['id' , 'name']);
        $monthly_salary_components = DB::table('salarycomponent')->where('is_monthly' , 1)->get();
        $taxes = Tax::get(['id', 'code']);
        return view('Admin.finance.salarysetup.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $salary_components = SalaryComponent::get();
        $payFrequency = DB::table('payfrequency')->get();
        $currency = DB::table('currencytypes')->get();
        $calculationGroups = DB::table('deductiongroup')->whereIn('id', $this->user_calculation_groups)->get();
        $calculation_Exemptions_Assigned = DB::table('deductions')->get();
        return view('Admin.finance.salarysetup.create_employe_comp', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Store Employee Salary Components
            if($request->type == 'employeeSalaryComponents')
            {
                $this->validate($request, [
                    'department_id' => 'required',
                    'employee' => 'required|unique:payrollemployees|unique:employeesalary',
                    'pay_frequency' => 'required',
                    'currency' => 'required',
                    // 'calculation_group' => 'required',
                ]);
                dd($request);
                foreach ($request->components as $key => $component) {
                    $employee_component = new EmployeeSalaryComponent();
                    $employee_component->employee = $request->employee;
                    $employee_component->component = $component;
                    if(env('COMPANY') == 'CLINIX'){
                        $status = 'Pending';
                    }
                    else{
                        $status = 'Approved';
                    }
                    $employee_component->amount = $request->sc_amount[$component];
                    $employee_component->status = $status;
                    $employee_component->submitted_by = Auth::user()->id;
                    $employee_component->save();

                    $monthly_component = DB::table('salarycomponent')->where([
                        ['id', $component],
                        ['is_monthly', 1]
                    ])->first(['applied_month']);
                    if(!empty($monthly_component)){
                        DB::table('monthly_salary_component')->insert([
                            'employee_id' => $request->employee,
                            'component_id' => $component,
                            'component_month' => $monthly_component->applied_month,
                            'amount' => $request->sc_amount[$component],
                            'created_by' => Auth::id(),
                        ]);
                    }
                    if(env('COMPANY') != 'CLINIX'){
                        $calculation_group = isset($request->calculation_group) && !empty($request->calculation_group) ? $request->calculation_group : NULL;
                        insertInSalaryComponentHistory($request->employee,$component,$request->sc_amount[$component],$calculation_group);
                    }
                }
            }
            // Salary Component Types
            elseif($request->type == 'salary_component_type')
            {
                $this->validate($request, [
                    'code' => 'required',
                    'name' => 'required',
                ]);
                SalaryComponentType::create($request->all());
            }
            // Salary Component
            elseif($request->type == 'salary_component')
            {
                $this->validate($request, [
                    'name' => 'required',
                    'componentType' => 'required',
                ]);
                $data = $request->all();
                $data['is_edit'] = !empty($request->is_edit) ? $request->is_edit : 0;
                $data['is_monthly'] = !empty($request->is_monthly) ? $request->is_monthly : 0;
                SalaryComponent::create($data);
            }
            // Multiple Monthly Component
            elseif($request->type == 'multiple_component_by_month'){
                foreach ($request->employees as $key => $employee) {
                    foreach ($request->salary_components as $component) {
                            $amount = $request->sc_amount[$employee][$component][0];
                            $employee_component = EmployeeSalaryComponent::where('employee', $employee)->where('component', $component)->first();
                            if(!empty($employee_component))
                            {
                                $employee_component->amount = $amount;
                                $employee_component->update();
                            }
                            else{
                                $emp_component = new EmployeeSalaryComponent();
                                $emp_component->employee = $employee;
                                $emp_component->component = $component;
                                $emp_component->amount = $amount;
                                $emp_component->save();
                            }
                            $month = $request->month.'-01';
                            if(!empty($month))
                            {
                                DB::table('monthly_salary_component')->updateOrInsert(
                                    ['employee_id' => $employee,'component_id' =>$component,'component_month' => $month],
                                    [
                                        'employee_id' => $employee,
                                        'component_id' => $component,
                                        'deducation_group' => $request->deducationGroup,
                                        'component_month' => $month,
                                        'amount' => $amount,
                                        'created_by' => Auth::id()
                                    ]
                                );
                            }
                            $calculation_group = isset($request->deducationGroup) && !empty($request->deducationGroup) ? $request->deducationGroup : NULL;
                            insertInSalaryComponentHistory($employee,$component,$amount,$calculation_group);
                    }
                }
            }
            elseif($request->type == 'department_wise' || $request->type == 'incentive'){
                foreach ($request->employees as $key => $employee) {
                    foreach ($request->salary_components as $component) {
                        // foreach($request->sc_amount[$employee][$component] as $amount)
                        // {
                            $amount = $request->sc_amount[$employee][$component][0];
                            $remarks = isset($request->remarks) ? $request->remarks[$employee][$component][0] : NULL;
                            $employee_component = EmployeeSalaryComponent::where('employee', $employee)->where('component', $component)->first();
                            if(!empty($employee_component)){
                                $employee_component->details = $remarks;
                                $employee_component->amount = $amount;
                                $employee_component->update();
                            }
                            else{
                                $emp_component = new EmployeeSalaryComponent();
                                $emp_component->employee = $employee;
                                $emp_component->component = $component;
                                $emp_component->amount = $amount;
                                $emp_component->details = $remarks;
                                $emp_component->save();
                            }
                            $monthly_component = DB::table('salarycomponent')->where([
                                ['id', $component],
                                ['is_monthly', 1]
                            ])->first(['applied_month']);
                            if(!empty($monthly_component))
                            {
                                DB::table('monthly_salary_component')->updateOrInsert(
                                    ['employee_id' => $employee,'component_id' =>$component,'component_month' => $monthly_component->applied_month],
                                    [
                                        'employee_id' => $employee,
                                        'component_id' => $component,
                                        'component_month' => $monthly_component->applied_month,
                                        'amount' => $amount,
                                        'created_by' => Auth::id()
                                    ]
                                );
                            }
                            $calculation_group = NULL;
                            insertInSalaryComponentHistory($employee,$component,$amount,$calculation_group,$remarks);
                        // }
                    }
                }
            }
            // Employee Monthly Components
            else{
                $this->validate($request , [
                    'salary_component' => 'required',
                    // 'department' => 'required',
                ]);
                foreach($request->employee_id as $key => $employee){
                    $amount = isset($request->amount[$employee]) ? $request->amount[$employee][0] : 0.00;
                    if($request->type != 'component_by_month')
                    {
                        DB::table('employeesalary')->updateOrInsert(
                            ['employee' => $employee,'component' =>$request->salary_component],
                            [
                                'employee' => $employee,
                                'component' => $request->salary_component,
                                'status' => 'Approved',
                                'amount' => $amount
                            ]
                        );
                    }
                    $monthly_component = DB::table('salarycomponent')->where([
                        ['id', $request->salary_component],
                        ['is_monthly', 1]
                    ])->first(['applied_month']);
                    if(!empty($monthly_component))
                    {
                        if($request->type == 'component_by_month')
                        {
                            $month = $request->month.'-01';
                        }
                        else
                        {
                            $month = $monthly_component->applied_month;
                        }
                        DB::table('monthly_salary_component')->updateOrInsert(
                            ['employee_id' => $employee,'component_id' =>$request->salary_component,'component_month' => $month],
                            [
                                'employee_id' => $employee,
                                'component_id' => $request->salary_component,
                                'deducation_group' => $request->deducationGroup,
                                'component_month' => $month,
                                'amount' => $amount,
                                'created_by' => Auth::id()
                            ]
                        );
                    }
                    $calculation_group = isset($request->deducationGroup) && !empty($request->deducationGroup) ? $request->deducationGroup : NULL;
                    insertInSalaryComponentHistory($employee,$request->salary_component,$amount,$calculation_group);
                }
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $salary_components = SalaryComponent::query();
        if(request()->type == 'multiple_component_by_month'){
            $salary_components->whereIn('id', $request->salary_component);
        }
        $salary_components = $salary_components->get();
        $dept_salary_comp = request()->dept_salary_comp;
        if(request()->type == 'department' OR request()->type == 'component_by_month' OR request()->type == 'multiple_component_by_month')
        {
            $employees = DB::table('employees')->join('payrollemployees', 'payrollemployees.employee', '=', 'employees.id')
            ->leftjoin('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title');
            if(env('COMPANY') != 'JSML'){
                $employees->where('employees.status', '=', 'Active');
            }
                if($request->suppress_zero)
                {
                    $employees->join('monthly_salary_component' , 'monthly_salary_component.employee_id' , '=' , 'employees.id')
                    ->whereYear('component_month', date('Y', strtotime(request()->month)))->whereMonth('component_month', date('m', strtotime(request()->month)))
                    ->where('component_id', request()->salary_component)
                    ->where('monthly_salary_component.amount','!=',0.00);
                }
            // if(env('COMPANY') == 'JSML'){
            //     $employees->where('employees.seasonal_status' , 'On Roll');
            // }
            if(request()->type == 'component_by_month' || request()->type == 'multiple_component_by_month')
            {
                // if(env('COMPANY') == 'RoofLine'){
                //     $employees->where('payrollemployees.deduction_group' , request()->calculationGroup);
                // }
                if(isset($request->department_id) && count($request->department_id) > 0)
                {
                    // // dd()
                    // $dep_id = explode(',',$id);
                    // $all_childs = getChildren($id);
                    // array_push($all_childs , $id);
                    // // dd($all_childs);
                    $employees->whereIn('employees.department', $request->department_id);
                }
                if(!empty($request->employee_Filter)){
                    $employees->where('employees.id' , $request->employee_Filter);
                }
                if(!empty($request->calculationGroup)){
                    $employees->where('payrollemployees.deduction_group' , $request->calculationGroup);
                }
                if(!empty($request->employment_status) && $request->employment_status !="all")
                {
                    $employees = $employees->whereIn('employees.employment_status' , request()->employment_status);
                }
            }
            else
            {
                if($id != 'all')
                {
                    $id=explode(',',$id);
                    $employees->whereIn('employees.department', $id);
                }
            }
            $employees = $employees->groupBy('employees.id')->whereNull('payrollemployees.deleted_at')->whereIn('payrollemployees.deduction_group', $this->user_calculation_groups)->orderBy('employees.employee_code', 'ASC')
            ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.last_name' ,'employees.middle_name', 'jobtitles.name AS designation','employees.termination_date','employees.off_roll_date','employees.seasonal_status')->get()
            ->reject(function($employees) {
                if(env('COMPANY') == 'JSML'){
                    if(!empty($employees->off_roll_date)){
                        return date("Y-m", strtotime($employees->off_roll_date)) < date("Y-m", strtotime(request()->month));
                    }elseif (!empty($employees->termination_date)) {
                        return date("Y-m", strtotime($employees->termination_date)) < date("Y-m", strtotime(request()->month));

                    }
                }
            });
        }
        else{
            $employees = DB::table('incentive_employees')->join('employees', 'employees.id', '=', 'incentive_employees.employee_id')
            ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title')
            ->join('payrollemployees', 'payrollemployees.employee', '=', 'employees.id')
            ->where('department', $id)
            ->where('incentive_employees.status', '=', 1);
            // if(env('COMPANY') == 'JSML'){
            //     $employees->where('employees.seasonal_status' , 'On Roll');
            // }
            $employees = $employees->whereIn('payrollemployees.deduction_group', $this->user_calculation_groups)
            ->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.last_name' ,'employees.middle_name', 'jobtitles.name AS designation','employees.off_roll_date')->get()
            ->reject(function($employees) {
                if(env('COMPANY') == 'JSML'){
                    if(!empty($employees->off_roll_date)){
                        return $employees->off_roll_date < date("Y-m-d", strtotime("first day of previous month"));
                    }
                }
            });
        }
        $data = [];
        $component_name = '';
        foreach ($employees as $key => $employee) {
            if(request()->type == 'component_by_month')
            {
                $salary_component = SalaryComponent::where('id', request()->salary_component)->first();
                $employee_component = DB::table('monthly_salary_component')->where('employee_id', $employee->id)->where('component_id', request()->salary_component)->whereYear('component_month', date('Y', strtotime(request()->month)))->whereMonth('component_month', date('m', strtotime(request()->month)));
                if(env('COMPANY') == 'RoofLine'){
                    $employee_component->where('deducation_group',request()->calculationGroup);
                }
                $employee_component = $employee_component->select('employee_id','component_id', 'amount')->first();
                $employee->component_name = $salary_component->name;
                $employee->component_id = $salary_component->id;
                if(!empty($employee_component))
                {
                    $employee->amount = $employee_component->amount;
                }
                else
                {
                    $employee->amount = 0;
                }
            }
            else
            {
                if(!empty($dept_salary_comp)){
                    $employee_component = EmployeeSalaryComponent::where('employee', $employee->id)->where('component', $dept_salary_comp)->select('employee','component', 'amount')->first();
                    $component_name = SalaryComponent::where('id', $dept_salary_comp)->first(['id', 'name']);
                    $data[$employee->id][$dept_salary_comp] = $employee_component;
                }
                else{
                    foreach ($salary_components as $component) {
                        if(request()->type == 'multiple_component_by_month'){
                            $employee_component = DB::table('monthly_salary_component')->where('employee_id', $employee->id)->where('component_id', $component->id)->whereYear('component_month', date('Y', strtotime(request()->month)))->whereMonth('component_month', date('m', strtotime(request()->month)));
                            if(env('COMPANY') == 'RoofLine'){
                                $employee_component->where('deducation_group',request()->calculationGroup);
                            }
                            $employee_component = $employee_component->select('employee_id','component_id', 'amount')->first();
                        }
                        else{
                            $employee_component = EmployeeSalaryComponent::where('employee', $employee->id)->where('component', $component->id)->select('employee','component', 'amount')->first();
                        }
                        $data[$employee->id][$component->id]['component'] = $employee_component;
                    }
                }
            }
        }
        return response()->json(['employees' => $employees, 'data' => $data, 'component_name' => $component_name,'salary_components' => $salary_components]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        // Edit Salary Component Types
        if($request->type == 'salary_component_type'){
            $salaryComponentTypes = SalaryComponentType::find($id);
            return response()->json($salaryComponentTypes);
        }
        // Edit Salary Component
        elseif($request->type == 'salary_component'){
            $salaryComponents = SalaryComponent::find($id);
            return response()->json($salaryComponents);
        }
        // Edit Employee Salary Components
        else{
            $salary_components = SalaryComponent::get();
            $payFrequency = DB::table('payfrequency')->get();
            $currency = DB::table('currencytypes')->get();
            $calculationGroups = DB::table('deductiongroup')->get();
            $calculation_Exemptions_Assigned = DB::table('deductions')->get();
            $payroll_employee = DB::table('employees')->leftJoin('payrollemployees', 'employees.id', '=', 'payrollemployees.employee')->where('employees.id', $id)->select('payrollemployees.*','employees.id', 'employees.department', 'employees.first_name', 'employees.last_name', 'employees.employee_id','employees.employee_code')->first();
            $comp = [];
            foreach($salary_components as $component)
            {
                $employee_component = DB::table('employeesalary')->where('employee', $id)->where('component', $component->id)->select('amount' , 'details')->first();
                $comp[$component->id] = !empty($employee_component) ? $employee_component->amount : 0.00;
                $remarks[$component->id] = !empty($employee_component) && !empty($employee_component->details) ? $employee_component->details : '';
            }
            return view('Admin.finance.salarysetup.edit_employee_comp', get_defined_vars());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            //update Employee Salary Components
            if($request->type == 'employeeSalaryComponents'){
                if(env('COMPANY') != 'RoofLine'){
                    $this->validate($request, [
                        'pay_frequency' => 'required',
                        'currency' => 'required',
                        // 'calculation_group' => 'required',
                    ]);
                }
                foreach ($request->components as $key => $component) {
                    $emp_component = EmployeeSalaryComponent::where('employee', $id)->where('component', $component)->first();
                    if(!empty($emp_component))
                    {
                        $emp_component->amount = $request->sc_amount[$component];
                        $emp_component->details = $request->remarks[$component];
                        $emp_component->update();
                    }
                    else{
                        $employee_component = new EmployeeSalaryComponent();
                        $employee_component->employee = $id;
                        $employee_component->component = $component;
                        $employee_component->amount = $request->sc_amount[$component];
                        $employee_component->details = $request->remarks[$component];
                        $employee_component->save();
                    }

                    $monthly_component = DB::table('salarycomponent')->where([
                        ['id', $component],
                        ['is_monthly', 1]
                    ])->first(['applied_month']);
                    if(!empty($monthly_component))
                    {
                        DB::table('monthly_salary_component')->updateOrInsert(
                            ['employee_id' => $id,'component_id' =>$component,'component_month' => $monthly_component->applied_month],
                            [
                                'employee_id' => $id,
                                'component_id' => $component,
                                'component_month' => $monthly_component->applied_month,
                                'amount' => $request->sc_amount[$component],
                                'remarks' => $request->remarks[$component],
                                'created_by' => Auth::id(),
                            ]
                        );
                    }
                    $calculation_group = isset($request->calculation_group) && !empty($request->calculation_group) ? $request->calculation_group : NULL;
                    $remarks = !empty($request->remarks[$component]) ? $request->remarks[$component] : NULL;
                    insertInSalaryComponentHistory($id,$component,$request->sc_amount[$component],$calculation_group,$remarks);
                }
            }
            //update Salary Component Types
            elseif($request->type == 'salary_component_type'){
                $this->validate($request, [
                    'code' => 'required',
                    'name' => 'required',
                ]);
                $salaryComponentTypes = SalaryComponentType::find($id);
                $salaryComponentTypes->fill($request->all())->save();
            }
            // Clear Salary Component
            elseif(request()->type == 'clear_salary_component'){
                dd(request()->type);
            }
            //update Salary Components
            else
            {
                $this->validate($request, [
                    'name' => 'required',
                    'componentType' => 'required',
                ]);
                $data = $request->all();
                if(!isset($data['is_monthly']))
                {
                    $data['is_monthly'] = 0;
                    $data['applied_month'] = NULL;
                }
                if(!isset($data['is_edit'])){
                    $data['is_edit'] = 0;
                }
                $salaryComponents = SalaryComponent::find($id);
                $salaryComponents->fill($data)->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e)
        {
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            if($request->type == 'employeeSalaryComponents'){
                //
            }
            // Salary Component Type
            elseif($request->type == 'salary_component_type')
            {
                $component_type = SalaryComponent::where('componentType', $id)->get();
                if(count($component_type) > 0)
                {
                    return ['code'=>'300', 'message'=>'This Component Type is Assigned to Some Components Please change it first!'];
                }
                $component_type = SalaryComponentType::find($id);
                $component_type->deleted_by = Auth::user()->id;
                $component_type->update();

                $component_type->delete();
            }
            // Salary Component
            else
            {
                $component = EmployeeSalaryComponent::where('component', $id)->get();
                if(count($component) > 0)
                {
                    return ['code'=>'300', 'message'=>'This Component is Assigned to Some Employees Please change it first!'];
                }
                $component = SalaryComponent::find($id);
                $component->deleted_by = Auth::user()->id;
                $component->update();

                $component->delete();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }

    public function get_department_child(Request $request)
    {
        $child =(getChildren($request->department_id, [], $request->filled('leave_type_setting')));
        $data = DB::table('companystructures')->whereIn('companystructures.id',$child)->select('id','title')->get();
        return response()->json($data);
    }
    public function get_department_by_type(Request $request)
    {
        $data = DB::table('companystructures')->where('companystructures.type',$request->typeFilter)->select('id','title')->get();
        return response()->json($data);
    }
    public function get_department_employees(Request $request)
    {
        $data = DB::table('employees')->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->whereIn('employees.department' , login_user_departments())
        ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title');
        if(!empty($request->employment_status)){
            $data->whereIn('employment_status', $request->employment_status);
        }
        if(userRoles()=="Self Service"){
            $data->where('employees.id', Auth::user()->employee);
        }
        if($request->type == 'deducation_report' || $request->type == 'loan_request' || $request->type == 'leave_request' || $request->type == 'salary_component'|| $request->type == 'attendance_report' || $request->type == 'component_by_month'){
            if($request->department_id != 'all'){
                if($request->type == 'component_by_month'){
                    $data->whereIn('department', $request->department_id);
                }
                else{
                    $all_childs =  getChildren($request->department_id);
                    array_push($all_childs,$request->department_id);
                    $data->whereIn('department', $all_childs);
                }
            }
        }
        else{
            if(env('COMPANY') != 'JSML'){
                if($request->department_id != 'all'){
                    $data->where('department', $request->department_id);
                }
            }else{
                if($request->department_id != 'all' && !empty($request->department_id)){
                    $data->where('department', $request->department_id);
                }
            }
        }
        if(env('COMPANY') == 'CLINIX'){
            if($request->type == 'loan_request'){
                $status = ['Active', 'Suspended', 'Terminated'];
            }
            else{
                $status = ['Active', 'Suspended'];
            }
            $data->whereIn('employees.status', $status);
        }
        else{
            if(env('COMPANY') != 'JSML'){
                $data->where('employees.status', 'Active');
            }
        }
        $data = $data->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name' , 'employees.last_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation','employees.off_roll_date','employees.seasonal_status')
        ->get()
        ->map(function($data) {
            if ($data->middle_name == null) {
                $data->middle_name = '';
            }
            if($data->employee_code == null){
                $data->employee_code = '';
            }
            return $data;
            })
        ->reject(function($data) use ($request) {
            if(env('COMPANY') == 'JSML' && $request->type != 'employee-salary-component'){
                if($data->seasonal_status == 'Off Roll' && !empty($data->off_roll_date)){
                    return $data->off_roll_date < date("Y-m-d", strtotime("first day of previous month"));
                }
            }
        });
        return response()->json($data);
    }

    public function get_designation_employees(Request $request)
{
    $data = DB::table('employees')
        ->join('companystructures', 'companystructures.id', '=', 'employees.department')
        ->whereIn('employees.department', login_user_departments())
        ->join('jobtitles', 'jobtitles.id', '=', 'employees.job_title');

    if (!empty($request->employment_status)) {
        $data->whereIn('employment_status', $request->employment_status);
    }

    if ($request->type == 'deducation_report' || $request->type == 'loan_request' || $request->type == 'leave_request' || $request->type == 'salary_component' || $request->type == 'attendance_report' || $request->type == 'component_by_month') {
        if ($request->designation_id != 'all') {
            if ($request->type == 'component_by_month') {
                $data->whereIn('job_title', $request->designation_id);
            } else {
                $data->whereIn('job_title', $request->designation_id);
            }
        }
    } else {
        if (env('COMPANY') != 'JSML') {
            if ($request->designation_id != 'all') {
                $data->where('job_title', $request->designation_id);
            }
        } else {
            if ($request->designation_id != 'all' && !empty($request->designation_id)) {
                $data->where('job_title', $request->designation_id);
            }
        }
    }

    if (env('COMPANY') == 'CLINIX') {
        if ($request->type == 'loan_request') {
            $status = ['Active', 'Suspended', 'Terminated'];
        } else {
            $status = ['Active', 'Suspended'];
        }
        $data->whereIn('employees.status', $status);
    } else {
        if (env('COMPANY') != 'JSML') {
            $data->where('employees.status', 'Active');
        }
    }

    $data = $data->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name', 'employees.last_name', 'companystructures.title AS department', 'jobtitles.name AS designation', 'employees.off_roll_date', 'employees.seasonal_status')
        ->get()
        ->map(function ($data) {
            if ($data->middle_name == null) {
                $data->middle_name = '';
            }
            if ($data->employee_code == null) {
                $data->employee_code = '';
            }
            return $data;
        })
        ->reject(function ($data) use ($request) {
            if (env('COMPANY') == 'JSML' && $request->type != 'employee-salary-component') {
                if ($data->seasonal_status == 'Off Roll' && !empty($data->off_roll_date)) {
                    return $data->off_roll_date < date("Y-m-d", strtotime("first day of previous month"));
                }
            }
        });

    return response()->json($data);
}


    public function get_department_employees_from_section(Request $request)
    {
        $data = DB::table('employees')->join('companystructures' , 'companystructures.id' , '=' , 'employees.department')
        ->whereIn('employees.department' , login_user_departments())
        ->join('jobtitles' , 'jobtitles.id' , '=' , 'employees.job_title');
        if(empty($request->overtime_request)){
            $data->where('employees.status', 'Active');
        }
        if(!empty($request->employment_status)){
            $data->whereIn('employment_status', $request->employment_status);
        }
        $data = $data->whereIn('department', $request->department_id)->select('employees.id', 'employees.employee_id', 'employees.employee_code', 'employees.first_name', 'employees.middle_name' , 'employees.last_name' , 'companystructures.title AS department' , 'jobtitles.name AS designation','employees.off_roll_date','employees.seasonal_status')
        ->get()
        ->map(function($data) {
            if ($data->middle_name == null) {
                $data->middle_name = '';
            }
            if($data->employee_code == null){
                $data->employee_code = '';
            }
            return $data;
            })
            ->reject(function($data) use ($request) {
                if(env('COMPANY') == 'JSML' && $request->type != 'employee-salary-component' && empty($request->overtime_request)){
                    if($data->seasonal_status == 'Off Roll' && !empty($data->off_roll_date)){
                        return $data->off_roll_date < date("Y-m-d", strtotime("first day of previous month"));
                    }
                }
            });
        return response()->json($data);
    }

    public function get_emp_monthly_comp(Request $request)
    {
        // if(!empty($request->department) && !empty($request->employment_status) && !empty($request->salary_component)){
            $all_childs = getChildren($request->department);
            array_push($all_childs , $request->department);
            $employees = DB::table('employees')->select('employees.id' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' , 'employees.middle_name' , 'employees.last_name' , 'employmentstatus.name AS employment','employees.off_roll_date','employees.seasonal_status')
            ->join('employmentstatus' , 'employmentstatus.id' , '=' , 'employees.employment_status')
            ->where('employees.status' , 'Active');
            // if(env('COMPANY') == 'JSML'){
            //     $employees->where('employees.seasonal_status' , 'On Roll');
            // }
            if($request->employment_status !='all'){
                // $employement_status = implode(',',$request->employment_status);
                $employees = $employees->where('employees.employment_status' , $request->employment_status);
            }
            $employees = $employees->whereIn('employees.department' , $all_childs)->get()
            ->reject(function($employee,$key) {
                    if(env('COMPANY') == 'JSML'){
                    if($employee->seasonal_status == 'Off Roll' && !empty($employee->off_roll_date)){
                        return $employee->off_roll_date < date("Y-m-d", strtotime("first day of previous month"));
                    }
                }
            });

            if(count($employees) > 0){
                foreach($employees as $key => $employee){
                    $employee_component = DB::table('employeesalary')->where([
                        ['employee' , $employee->id],
                        ['component' , $request->salary_component]
                        ])->first();

                        if(!empty($employee_component)){
                            $employee->amount = $employee_component->amount;
                        }else{
                            $employee->amount = 0.00;
                        }
                }
                return response([
                    'code' => 200 , 'data' => $employees
                ]);
            }
            else{
                return response([
                    'code' => 404 , 'message' => 'Not Found'
                ]);
            }
        // }
        // else{
        //     return response(['code' => 522 , 'message' => 'All Fields are Required!']);
        // }
    }

    public function get_employees_leave(Request $request)
    {
        if(env('COMPANY') == 'JSML'){
            return getEmployeeLeaveTypesBySettings($request->employee);
        }
        else{
            return response()->json(getEmployeeLeaveTypes($request->employee));
        }
    }

    public function clear_salary_comp($id){
        DB::table('employeesalary')->where('component' , request()->id)->update([
            'amount' => 0,
        ]);
        return redirect()->back()->with('message', 'Cleared Successfuly');;
    }
}
