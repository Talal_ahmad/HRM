<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\AssetIssuance;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class AssetIssuanceController extends Controller
{ 
    public function __construct(){ 
        $this->middleware('auth');
    }

    public function index(Request $request)   
    {
        if($request->ajax())
        {
            $data = AssetIssuance::leftjoin('employees','asset_issuance.employee_id','=','employees.id')
            ->leftjoin('companystructures','asset_issuance.department_id','=','companystructures.id')
            ->leftjoin('companybelongings','asset_issuance.asset_id','=','companybelongings.id')
            ->select('asset_issuance.*','companybelongings.name as asset','companystructures.title as department','employees.first_name','employees.middle_name','employees.last_name','employees.employee_id' , 'employees.employee_code');
            return DataTables::of($data)->filter(function ($data) use ($request) {
                if(!empty($request->departmentFilter)){
                    $data->where('asset_issuance.department_id'
                    , $request->departmentFilter);
                }
            },true)->addIndexColumn()->make(true);; 
        }
        $assets = DB::table('companybelongings')->get(); 
        return view('Admin.finance.assetIssuance.index', get_defined_vars());    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'department_id' => 'required',
                'asset_id' => 'required',
                'from_date' => 'required',
                'received_back' => 'required',                
            ]);
            $data = $request->all(); 
            AssetIssuance::create($data);
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request , $id) 
    {
        if($request->ajax()){
            $query = DB::table('employees')->select('id' , 'first_name' , 'last_name')
                ->where('status' , '=' ,  'Active')
                ->where('department' , '=' , $id)
                ->get(); 
            return response()->json($query);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = AssetIssuance::find($id);
        $employees = employees($data->department_id);
        return response()->json(['data' => $data, 'employees' => $employees]);     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'employee_id' => 'required',
                'department_id' => 'required',
                'asset_id' => 'required',
                'from_date' => 'required',
                'received_back' => 'required',
            ]);

            $AssetIssuance = AssetIssuance::find($id);
            $AssetIssuance->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $AssetIssuance = AssetIssuance::find($id);
            $AssetIssuance->deleted_by = Auth::id();
            $AssetIssuance->update();
            $AssetIssuance->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }catch(\Exception $e){
            return ['code' => 500 , 'error_message' => $e->getMessage()]; 
        }
    }
}