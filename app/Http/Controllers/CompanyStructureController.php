<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\Country;
use App\Models\Employee;
use App\Models\TimeZone;
use Illuminate\Http\Request;
use App\Models\CompanyStructure;
use App\Models\OrganogramSetup;

use Illuminate\Validation\ValidationException;

class CompanyStructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = CompanyStructure::leftjoin('companystructures as c2','c2.id','=','companystructures.parent')
                        ->whereIn('companystructures.id', login_user_departments())
                        ->select('companystructures.id','companystructures.title','companystructures.type', 'c2.title as parent_name');
            return Datatables::eloquent($data)->addIndexColumn()->make(true);
        }
        $countries = Country::get(['code', 'name']);
        $company_structures = CompanyStructure::get(['id', 'title']);
        $timezone = TimeZone::get();
        return view('Admin.setup.company_structures.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rows = \DB::select("SELECT id,title,title as `name`,parent from companystructures");
        $tree = $this->buildTree($rows);
        return view('Admin.setup.company_structures.company_graph', compact('tree'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'title' => 'required |string',
                'type' => 'required',
                'country' => 'required',
            ]);

            $data = $request->all();
            CompanyStructure::create($data);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company_structure = CompanyStructure::find($id);
        return response()->json($company_structure);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'title' => 'required |string',
                'type' => 'required',
                'country' => 'required',
                // 'timezone' => 'required',
                // 'description' => 'required',
            ]);

            $company_structure = CompanyStructure::find($id);
            $company_structure->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $company_structure = CompanyStructure::find($id);
            $company_structure->deleted_by = Auth::id();
            $employee = Employee::where("department" , $id)->get();
            if(count($employee) > 0){
                return ['code'=>'300', 'message'=>'This Department is Assigned to Some Employees Please change it first!'];
            }
            $company_structure->update();
            $company_structure->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
    public function organogram()
    {
        {
            // $Result = '';
            // $rows = CompanyStructure::select('id','parent as pid','type as name','title')->get();
            // foreach ($rows as $key => $value) {
            //     $Result .= $value .',';
            // }
            $Result = "";
            $rows = OrganogramSetup::select('id', 'pid', 'name', 'title')->get();

            // Create a temporary associative array to map IDs to their respective data
            $tempArray = [];
            foreach ($rows as $key => $value) {
                $tempArray[$value->id] = [
                    'id' => $value->id,
                    'name' => $value->name,
                    'title' => $value->title,
                    'children' => [] // Initialize an empty children array
                ];
            }

            // Loop through the rows again to build the hierarchical structure
            foreach ($rows as $key => $value) {
                if ($value->pid && isset($tempArray[$value->pid])) {
                    $tempArray[$value->pid]['children'][] = &$tempArray[$value->id];
                } else {
                    // If there's no parent, it means it's a top-level element
                    $Result = &$tempArray[$value->id];
                }
            }

            // Convert the Result to a JSON object
            // $Result = json_encode($Result);
            return view('admin.organogram.index', get_defined_vars());
        }
    }
    public function buildTree(array $elements, $parentId = 0) {    
        $branch = array();
        foreach ($elements as $element) {
            if ($element->parent == $parentId) {
                $children = $this->buildTree($elements, $element->id);
                if ($children) {
                    $element->children = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }
}
