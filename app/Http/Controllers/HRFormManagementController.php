<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\HRForm;
use App\Models\EmployeeForm;
use Illuminate\Support\Facades\DB; 
use Illuminate\Validation\ValidationException; 

class HRFormManagementController extends Controller
{
    public function __construct(){  
        $this->middleware('auth');
    }

    public function index(Request $request) 
    {
        if($request->ajax()){
            // hr forms
            if($request->type == 'hr_forms'){
                return Datatables::eloquent(HRForm::query())->make(true); 
            }
            // employee forms 
            else{
                return Datatables::eloquent(EmployeeForm::leftjoin('employees','employeeforms.employee','=','employees.id')->leftjoin('forms','employeeforms.form','=','forms.id')->select('employeeforms.status','forms.name')->selectRaw('CONCAT(employees.first_name,employees.last_name) as "employee"'))->make(true);
            }
        }

        $employees=DB::table('employees')
        ->select('employees.*')
        ->select("*", DB::raw('CONCAT(employees.first_name,employees.last_name) AS "employee"'))
        ->get();
        $forms=DB::table('forms')->get(); 
        return view('Admin.setup.HRFormManagement.index', get_defined_vars());  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Store Hr fORMS
            if($request->type == 'hr_forms'){
                try{
                    $this->validate($request, [
                        'name' => 'required',
                        'description' => 'required',


                    ]);
                    $name                                      = ($request->name);
                    $description                               = ($request->description);
                    $add_field_name                            = ($request->add_field_name);
                    $add_field_label                           = ($request->add_field_label);
                    $add_field_type                            = ($request->add_field_type);
                    $add_field_validation                      = ($request->add_field_validation);
                    $add_field_options                         = ($request->add_field_options);
                    $add_field_help                            = ($request->add_field_help);
                    $limit_array = array();
                    array_push($limit_array, $add_field_name);
                    array_push($limit_array, $add_field_label);
                    array_push($limit_array, $add_field_type);
                    array_push($limit_array, $add_field_validation);
                    array_push($limit_array, $add_field_options);
                    array_push($limit_array, $add_field_help);
                    $limit_array = json_encode($limit_array); 


                    $HRForm = new HRForm;
                    $HRForm->name = $name;
                    $HRForm->description = $description;
                    $HRForm->items = $limit_array;
                    $HRForm->save();

                    return ['code'=>'200','message'=>'success'];
                } 
                catch(\Exception | ValidationException $e){
                    if($e instanceof ValidationException){
                        return ['code'=>'200','errors' => $e->errors()]; 
                    }
                    else{
                        return ['code'=>'200','error_message'=>$e->getMessage()];
                    }
                }
            }
            // Employee Forms
            else
            {
                $this->validate($request, [
                    'employee' => 'required',
                    'form' => 'required',
                    'status' => 'required',                    
                ]);
                EmployeeForm::create($request->all());
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        // $salary_components = SalaryComponent::get();
        // $employees = Employee::where('department', $id)->select('id', 'employee_id', 'employee_code', 'first_name', 'last_name')->get();
        // foreach ($employees as $key => $employee) {
        //     foreach ($salary_components as $component) {
        //         $employee_component = EmployeeSalaryComponent::where('employee', $employee->id)->where('component', $component->id)->select('employee','component', 'amount')->first();
        //         $data[$employee->id][$component->id]['component'] = $employee_component;
        //     }
        //     // $employee_component = EmployeeSalaryComponent::where('employee', $employee->id)->orderBy('component', 'ASC')->select('id', 'employee', 'component', 'amount')->get();
        //     // dd($data);
        // }
        // return response()->json(['employees' => $employees, 'data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        // Edit HR FOrms
        if($request->type == 'hr_forms'){
            $HRForm = HRForm::find($id);
            return response()->json($HRForm);
        }
        // Edit EMployee Form
        else{
            $EmployeeForm = EmployeeForm::find($id);
            return response()->json($EmployeeForm);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            //update hr forms
            if($request->type == 'hr_forms'){
                $this->validate($request, [
                 'name' => 'required',
                 'description' => 'required',
             ]);

                $HRForm = HRForm::find($id);
                $HRForm->fill($request->all())->save(); 
            }
            //update Employee FOrm
            else
            {
                $this->validate($request, [
                    'employee' => 'required',
                    'form' => 'required',
                    'status' => 'required',
                ]);
                $EmployeeForm = EmployeeForm::find($id);
                $EmployeeForm->fill($request->all())->save();
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e)
        {
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            if($request->type == 'hr_forms'){
                $HRForm = EmployeeForm::where('form',$id)->get();
                if(count($HRForm) > 0)
                {
                    return ['code'=>'300', 'message'=>'This Form is Assigned to Some Employee in Employee Forms. Please change that first!'];
                }
                $HRForm = HRForm::find($id);
                $HRForm->deleted_by = Auth::user()->id;
                $HRForm->update();

                $HRForm->delete();
            }
            // employee forms
            else
            {
             $EmployeeForm = EmployeeForm::find($id);
             $EmployeeForm->deleted_by = Auth::user()->id;
             $EmployeeForm->update();
             $EmployeeForm->delete();
         }
         return ['code'=>'200','message'=>'success'];
     }
     catch(\Exception $e){
        return ['code'=>'500','error_message'=>$e->getMessage()];
    }
}
}
