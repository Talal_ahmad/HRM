<?php

namespace App\Http\Controllers;

use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Models\OvertimeManagement;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class BulkOverTimeManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.request&approvals.bulk_overtime_management.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(request()->fromDate > request()->toDate){
            return ['code' => 300 , 'message' => 'From Date cannot be greater then To Date'];
        }
        if(request()->toDate > date('Y-m-d')){
            return ['code' => 300 , 'message' => 'To Date cannot be greater then '. date('Y-m-d') .'Date'];
        }

        $data = [];
        $period = CarbonPeriod::create(request()->fromDate, request()->toDate);
        foreach ($period as $date) {
            $listOfDates[] = $date->format('Y-m-d');
        }

        if(!empty($listOfDates)){
            foreach($listOfDates as $listed_date){
                $shift = DB::table('shift_management')->select('shift_management.id', 'shift_management.work_week_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'work_week.desc as work_week_name')
                ->join('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                ->join('work_week' , 'work_week.id' , '=' , 'shift_management.work_week_id')
                ->where('shift_management.employee_id' , request()->employee)
                ->where('shift_management.shift_from_date', '<=', $listed_date)
                ->where('shift_management.shift_to_date', '>=', $listed_date)
                ->orderBy('shift_management.id', 'DESC')->first();
                if(!empty($shift)){
                    $attendance = DB::table('attendance')->where('employee' , request()->employee)->whereDate('in_time' , $listed_date)->first();

                    if(!empty($attendance)){
                        $in_time = $attendance->in_time;
                        $out_time = $attendance->out_time;
                    }
                    else{
                        $employee_leave = DB::table('employeeleaves')->where([
                            ['employee', request()->employee],
                            ['date_start', '<=', $listed_date],
                            ['date_end', '>=', $listed_date],
                            ['status', '=', 'Approved']
                        ])->first();

                        if(!empty($employee_leave))
                        {
                            $leave = 'Yes';
                        }
                    }

                    $work_week_days = DB::table('workdays')->where([
                        ['work_week_id', $shift->work_week_id],
                        ['status', '=', 'Non-working Day']
                    ])->get();
                    if(count($work_week_days) > 0)
                    {
                        $current_day_name = date('l', strtotime($listed_date));
                        foreach($work_week_days as $week_day)
                        {
                            if($week_day->name == $current_day_name)
                            {
                                $is_holiday = 'Off Day';
                            }
                        }
                    }

                    $holiday = DB::table('holidays')->where('dateh', $listed_date)->first();
                    if(!empty($holiday))
                    {
                        if(!empty($is_holiday))
                        {
                            $is_holiday = $is_holiday.' and '.$holiday->name;
                        }
                        else{
                            $is_holiday = $holiday->name;
                        }
                    }

                    $overtime_date = $listed_date;
                    $shift_start_datetime = $overtime_date.' '.$shift->shift_start_time;
                    $shift_end_datetime = $overtime_date.' '.$shift->shift_end_time;
                    if($shift_end_datetime < $shift_start_datetime)
                    {
                        $overtime_date = date("Y-m-d", strtotime("+1 day", strtotime($overtime_date)));
                    }
                    $shift_end_datetime = $overtime_date.' '.$shift->shift_end_time;

                    if(!empty($out_time)){
                        // Calculate Overtime Hours
                        $start_overtime = date_create($shift_end_datetime);
                        $end_overtime = date_create($out_time);
                        $diff = date_diff($start_overtime, $end_overtime);
                        $days = $diff->d;
                        $sec1 = $days * 24 * 3600;
                        $hours = $diff->h;
                        $sec2 = $hours * 3600;
                        $minutes = $diff->i;
                        $sec3 = $minutes * 60;
                        $total_sec = $sec1 + $sec2 + $sec3;
                        $overtime_hours = $total_sec / 3600;
                    }

                    if(!empty($overtime_hours) && $overtime_hours >= 0.50){
                        $data[$listed_date]['is_holiday'] = isset($is_holiday) ? $is_holiday : '-';
                        $data[$listed_date]['in_time'] = isset($in_time) ? $in_time : '';
                        $data[$listed_date]['out_time'] = isset($out_time) ? $out_time : '';
                        $data[$listed_date]['leave'] = isset($leave) ? $leave : 'No';
                        $data[$listed_date]['shift_name'] = $shift->shift_desc;
                        $data[$listed_date]['work_week'] = $shift->work_week_name;
                        $data[$listed_date]['shift_start_time'] = $shift->shift_start_time;
                        $data[$listed_date]['shift_end_time'] = $shift->shift_end_time;
                        $data[$listed_date]['shift_end_datetime'] = $shift_end_datetime;
                        $data[$listed_date]['overtime'] = number_format((float)$overtime_hours , 2, '.', '');
                        
                        $is_holiday = '';
                        $in_time = '';
                        $out_time = '';
                        $leave = '';
                        $overtime_hours = '';
                    }
                }
                else{
                    return ['code' => 500,'message'=>'No Shift Assgined on' . $listed_date . ' to Employee!'];
                }
            }
            return response()->json($data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'employee' => 'required'
            ]);

            $period = CarbonPeriod::create(request()->fromDate, request()->toDate);
            foreach ($period as $date) {
                $listOfDates[] = $date->format('Y-m-d');
            }

            if(!empty($listOfDates)){
                foreach($listOfDates as $listed_date){
                    if($request->has($listed_date) && !empty($request->$listed_date)){
                        $data = $request->$listed_date;
                        $shift = DB::table('shift_management')->select('shift_management.id', 'shift_management.work_week_id', 'shift_type.shift_start_time', 'shift_type.shift_end_time', 'shift_type.shift_desc', 'work_week.desc as work_week_name', 'shift_management.shift_id')
                        ->join('shift_type', 'shift_type.id', '=', 'shift_management.shift_id')
                        ->join('work_week', 'work_week.id', '=', 'shift_management.work_week_id')
                        ->where([
                            ['shift_management.employee_id', $request->employee],   
                            ['shift_management.shift_from_date', '<=', $listed_date],
                            ['shift_management.shift_to_date', '>=', $listed_date]
                        ])->orderBy('shift_management.id', 'DESC')->first();

                        if(!empty($shift)){
                            $overtime_date = $listed_date;
                            $start_datetime = date('Y-m-d H:i:s', strtotime($data['start_datetime']));
                            $end_datetime = date('Y-m-d H:i:s', strtotime($data['end_datetime']));
                            $shift_start_datetime = $listed_date.' '.$shift->shift_start_time;
                            $shift_end_datetime = $listed_date.' '.$shift->shift_end_time;
                            if($shift_end_datetime < $shift_start_datetime)
                            {
                                $overtime_date = date("Y-m-d", strtotime("+1 day", strtotime($listed_date)));
                            }
                            $shift_end_datetime = $overtime_date.' '.$shift->shift_end_time;

                            // Calculate hours
                            $start_overtime = date_create($start_datetime);
                            $end_overtime = date_create($end_datetime);
                            $diff = date_diff($start_overtime, $end_overtime);
                            $days = $diff->d;
                            $sec1 = $days * 24 * 3600;
                            $hours = $diff->h;
                            $sec2 = $hours * 3600;
                            $minutes = $diff->i;
                            $sec3 = $minutes * 60;
                            $total_sec = $sec1 + $sec2 + $sec3;
                            $overtime_hours = $total_sec / 3600;

                            if($overtime_hours > 48)
                            {
                                return ['code' => 300, 'message' => 'Overtime on '.$listed_date.' cannot be greater than 48 hours!'];
                            }

                            if(!empty($data['status']))
                            {
                                $approved = 1;
                                $status = 'Approved';
                            }
                            else{
                                $approved = 0;
                                $status = 'Pending';
                            }

                            $overtime = new OverTimeManagement();
                            $overtime->employee_id = $request->employee;
                            $overtime->shift_id = $shift->shift_id;
                            $overtime->work_week_id = $shift->work_week_id;
                            $overtime->overtime_start_time_stamp = $start_datetime;
                            $overtime->overtime_end_time_stamp = $end_datetime;
                            $overtime->overtime_date = $listed_date;
                            $overtime->overtime = $overtime_hours;
                            $overtime->approved = $approved;
                            $overtime->status = $status;
                            $overtime->remarks = $data['remarks'];
                            $overtime->submitted_by = Auth::id();
                            $overtime->save();
                        }
                    }        
                }
                return ['code' => 200 , 'message' => 'success'];
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code' => 422 , 'errors' => $e->errors()];
            }else{
                return ['code' => 500 , 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkOvertime(){
        $overtime_date = date('Y-m-d' , strtotime(request()->start_datetime));
        $start_datetime = date('Y-m-d H:i:s', strtotime(request()->start_datetime));
        $end_datetime = date('Y-m-d H:i:s', strtotime(request()->end_datetime));
        $shift_start_datetime = $overtime_date.' '.request()->shift_start_time;
        $shift_end_datetime = $overtime_date.' '.request()->shift_end_time;
        if($shift_end_datetime < $shift_start_datetime)
        {
            $overtime_date = date("Y-m-d", strtotime("+1 day", strtotime($overtime_date)));
        }
        $shift_end_datetime = $overtime_date.' '.request()->shift_end_time;

        if($start_datetime > $shift_start_datetime && $end_datetime < $shift_end_datetime || $start_datetime < $shift_start_datetime && $end_datetime > $shift_start_datetime || $start_datetime < $shift_end_datetime && $end_datetime > $shift_end_datetime)
        {
            return ['code' => 300, 'message' => 'Selected Overtime is not allowed because this time is in-between employee shift-time!'];
        }

        if(!empty(request()->out_time))
        {
            if($end_datetime > request()->out_time)
            {
                return ['code' => 300, 'message' => 'Selected Overtime cannot be greater than your checkout time!'];
            }
        }
        
        // Calculate hours
        $start_overtime = date_create($start_datetime);
        $end_overtime = date_create($end_datetime);
        $diff = date_diff($start_overtime, $end_overtime);
        $days = $diff->d;
        $sec1 = $days * 24 * 3600;
        $hours = $diff->h;
        $sec2 = $hours * 3600;
        $minutes = $diff->i;
        $sec3 = $minutes * 60;
        $total_sec = $sec1 + $sec2 + $sec3;
        $overtime_hours = $total_sec / 3600;

        if($overtime_hours > 48)
        {
            return ['code' => 300, 'message' => 'Overtime cannot be greater than 48 hours!'];
        }
        return ['code' => 200 , 'message' => 'success'];
    }
}
