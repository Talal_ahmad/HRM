<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Validation\ValidationException;
use App\Models\MonthlyKPI;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class YearlyKPIController extends Controller
{ 
    public function __construct(){ 
        $this->middleware('auth');
    }
 
    public function index(Request $request)   
    {
        if($request->ajax())
        {
            // $employee = DB::table('monthly_kpi')->select('monthly_kpi.*')->groupBy('monthly_kpi.employee_id')->get();
            // $employee_id = $employee->employee_id;
            return DataTables::eloquent(MonthlyKPI::leftjoin('employees','monthly_kpi.employee_id','=','employees.id')->selectRaw('COUNT(monthly_kpi.employee_id) as emp_count')->selectRaw('SUM(monthly_kpi.marks) AS marks_sum')->selectRaw('marks_sum/emp_count AS average')->select('employees.employee_id')->selectRaw('CONCAT(employees.first_name,employees.last_name) as "employeeName"')->groupBy('monthly_kpi.employee_id'))->make(true); 
        }

        return view('Admin.MISReports.yearlyKPIReport.index');     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //       
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
      //     
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }
}