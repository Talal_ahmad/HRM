<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use App\Models\FuelRate;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class FuelRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        if(request()->ajax()){
            $query = FuelRate::get();
            return DataTables::of($query)->make(true);
        }
        return view('Admin.finance.fuelRate.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'fuel_type' => 'required',
                'rate' => 'required',
                'from_date' => 'required',
                'to_date' => 'required',
            ]);
            $data = $request->all();
            FuelRate::create($data);
            return ['code'=>'200','message'=>'success'];
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FuelRate  $fuelRate
     * @return \Illuminate\Http\Response
     */
    public function show(FuelRate $fuelRate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FuelRate  $fuelRate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = FuelRate::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FuelRate  $fuelRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id)
    {
        try{
            $this->validate($request , [
                'fuel_type' => 'required',
                'rate' => 'required',
                'from_date' => 'required',
                'to_date' => 'required',
            ]);  
            $fuelRate = FuelRate::find($id);
            $fuelRate->fill($request->all())->save();
            return ['code'=>'200','message'=>'success'];
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FuelRate  $fuelRate
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $fuelRate = FuelRate::find($id);
            $fuelRate->deleted_by = Auth::id();
            $fuelRate->update();
            $fuelRate->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
