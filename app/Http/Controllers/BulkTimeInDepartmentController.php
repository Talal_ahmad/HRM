<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\ShiftManagement;
use App\Models\CompanyStructure;
use Illuminate\Support\Facades\DB;
use App\Models\bulkTimeInDepartment;

class BulkTimeInDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $employees = [];
        if($request->departmentFilter){
            $employees = Employee::where('employees.status' , 'Active')
            ->whereIn('employees.department' , login_user_departments())
            ->where('employees.department' , $request->departmentFilter);
            if($request->employeeFilter){
                $employees = $employees->where('employees.id' , $request->employeeFilter);
            }
            $employees = $employees->select('employees.id' , 'employees.employee_id' , 'employees.employee_code' , 'employees.first_name' ,'employees.middle_name', 'employees.last_name','employees.department')
            ->get();
        }
        return view('Admin.timeManagement.bulkTimeInDepartment.index' , get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            foreach($request->employees as $key => $value){
                if(!empty($request->date_time[$value])){
                    $date = date('Y-m-d',strtotime($request->date_time[$value]));
                    $shift_data = ShiftManagement::leftjoin('shift_type' , 'shift_type.id' , '=' , 'shift_management.shift_id')
                    ->where([
                        ['shift_management.employee_id' , $value],
                        ['shift_management.shift_from_date', '<=', $date],
                        ['shift_management.shift_from_date', '>=', $date]
                    ])
                    ->select('shift_management.*','shift_type.shift_start_time', 'shift_type.shift_end_time')
                    ->orderBy('shift_management.id', 'DESC')
                    ->first();
                    $attendance = DB::table('attendance')->where('employee', $value)->whereDate('in_time', $date)->first();
                    if(empty($attendance))
                    {
                        if(!empty($shift_data)){
                            DB::table('attendance')->insert([
                                'employee' => $value,
                                'department_id' => $request->department,
                                'shift_id' => $shift_data->shift_id,
                                'shift_start_time' => $shift_data->shift_start_time,
                                'shift_end_time' => $shift_data->shift_end_time,
                                'work_week_id' => $shift_data->work_week_id,
                                'late_time_allowed' => $shift_data->late_time_allowed,
                                'in_time' => $request->date_time[$value],
                                'added_using' => 1,
                                'is_manual' => 1,
                            ]);
                        }
                        else{
                            DB::table('attendance')->insert([
                                'employee' => $value,
                                'department_id' => $request->department,
                                'shift_id' => 0,
                                'shift_start_time' => '09:00:00',
                                'shift_end_time' => '17:00:00',
                                'work_week_id' => 0,
                                'in_time' => $request->date_time[$value],
                                'added_using' => 1,
                                'is_manual' => 1,
                            ]);   
                        }
                    }
                }
            }
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\bulkTimeInDepartment  $bulkTimeInDepartment
     * @return \Illuminate\Http\Response
     */
    public function show(bulkTimeInDepartment $bulkTimeInDepartment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\bulkTimeInDepartment  $bulkTimeInDepartment
     * @return \Illuminate\Http\Response
     */
    public function edit(bulkTimeInDepartment $bulkTimeInDepartment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\bulkTimeInDepartment  $bulkTimeInDepartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, bulkTimeInDepartment $bulkTimeInDepartment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\bulkTimeInDepartment  $bulkTimeInDepartment
     * @return \Illuminate\Http\Response
     */
    public function destroy(bulkTimeInDepartment $bulkTimeInDepartment)
    {
        //
    }
}
