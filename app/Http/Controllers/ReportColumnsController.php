<?php

namespace App\Http\Controllers;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use App\Models\ReportColumn;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ReportColumnsController extends Controller
{ 
    public function __construct(){ 
        $this->middleware('auth');
    }

    public function index(Request $request)   
    {
        if($request->ajax())
        {
            return DataTables::eloquent(ReportColumn::leftjoin('payrollcolumns','report_columns.payroll_column_id','=','payrollcolumns.id')
            ->join('deductiongroup', 'deductiongroup.id', '=', 'report_columns.calculation_group')->select('report_columns.id','report_columns.name','report_columns.column_order','payrollcolumns.name as payroll_column', 'deductiongroup.name as calculation_group'))->addIndexColumn()->make(true); 
        }
        $payroll_columns=DB::table('payrollcolumns')->get();
        return view('Admin.finance.reportColumns.index', get_defined_vars());    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{
            $this->validate($request, [
                'name' => 'required',
                'column_order' => 'required|unique:report_columns',
                'type' => 'required',
                'payroll_column' => 'required',
            ]);
            $calculation_group = DB::table('payrollcolumns')->where('id', $request->payroll_column)->first();
            $ReportColumn = new ReportColumn;
            $ReportColumn->name = $request->name;
            $ReportColumn->column_order = $request->column_order;
            $ReportColumn->type = implode(',',$request->type);
            $ReportColumn->payroll_column_id = $request->payroll_column;
            $ReportColumn->calculation_group = $calculation_group->deduction_group;
            $ReportColumn->is_decimal = !empty($request->is_decimal) ? 1 : 0;
            $ReportColumn->save();
            return ['code'=>'200','message'=>'success'];
        } 
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request  , $id)
    {
        $data = ReportColumn::find($id);
        return response()->json($data);     
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'name' => 'required',
                'column_order' => 'required|unique:report_columns,column_order,'.$id,
                'type' => 'required',
                'payroll_column' => 'required',
            ]);

            $calculation_group = DB::table('payrollcolumns')->where('id', $request->payroll_column)->first();
            $ReportColumn = ReportColumn::find($id);
            $ReportColumn->name = $request->name;
            $ReportColumn->column_order = $request->column_order;
            $ReportColumn->type = implode(',',$request->type);
            $ReportColumn->payroll_column_id = $request->payroll_column;
            $ReportColumn->calculation_group = $calculation_group->deduction_group;
            $ReportColumn->is_decimal = !empty($request->is_decimal) ? 1 : 0;
            $ReportColumn->update();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            ReportColumn::find($id)->delete(); 
            return ['code' => 200 , 'message' => 'success'];
        }catch(\Exception $e){
            return ['code' => 500 , 'error_message' => $e->getMessage()]; 
        }
    }
}